#include "ExuiProgram\\ExuiProgram_Main.h"

#include "ExuiProgram\\ExuiProgram_IcoList.h"

/*


使用方法 
1.将压缩包里的  文件 解压到 C 盘  (C:\ExuiApi)
2.运行 ExuiVSIXProject.vsix  文件 安装vs插件 并等待安装完毕
3.vs里新建项目后  在解决方案管理器里  头文件里(可选新建个名为ExuiApi的筛选器) 右键-添加-现有项-选择目录 C:\ExuiApi\include  将里面的文件全部选则 确定 
4.在 你需要使用界面的cpp文件里 输入如下代码 class ExuiProgram_Main;  其中 ExuiProgram_Main 为你希望定义的 界面窗口 的类名 推荐为 ExuiProgram_ 开头 后面跟功能名
5.鼠标点击 刚输入的代码 在工具菜单里 选择 ExuiEdit 选项 或 Ctrl+E+E快捷键  此时会弹出Exui界面编辑器(如果没有检查插件是否安装成功,以及插件启用状态) 设计你的界面 点击确定
7.首次定义完成后 你需要  在解决方案管理器里 头文件里(可选新建个名为ExuiProgram的筛选器)  右键-添加-现有项-选择目录 你的项目目录 里的  ExuiProgram 文件夹里的ExuiForCplusplus.h 和 ExuiForCplusplus.c 文件
8.首次定义完成后 你需要  在使用界面的cpp文件里 右键粘贴 会出现  #include "ExuiProgram\\xxx.h:" 和 界面类名 Window_xxx;  的变量定义  代码 将他们放在合适的位置
9.完成以上步骤 你就可以使用 你先前定义的 界面类了 
10. 大概用法 参考 本例程. api 命令和事件  参考  Exui.h 的定义



*/



class ExuiProgram_Main;
ExuiProgram_Main Window_Main;

class ExuiProgram_IcoList;
ExuiProgram_IcoList Window_IcoList;


int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    Window_Main.LoadMainWindow ();
    return 0;
}

AutoInt ExuiProgram_Main::OnWindowBoxExEvent_Create(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam) {
    //PopUpTimePickEx(100, 100, 200, 200, TEXT("确定&&取消"), 0, TEXT(""), TEXT(""), TEXT(""), FALSE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    return NULL; 
}
AutoInt ExuiProgram_Main::OnMinutesboxExEvent_MouseWheel(MinutesboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2)
{
    return NULL;
}


#include <string>

AutoInt	ExuiProgram_Main::OnButtonExEvent_L_Click(Control_P CtrHandle, AutoInt EventType, AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4)
{


    if (CtrHandle == 按钮EX1)
    {
        Window_IcoList.LoadDialogWindow(Window_Main.Exui主窗口);
        return 0;
    }

    if (CtrHandle == 按钮EX2)
    {

        //mybutton.Create(窗口EX1, 50, 100, 300, 200, true, false, 255, 0, 0, NULL, NULL, 按钮EX1.Skin(), "动态创建的按钮", NULL, 0, 0, NULL, NULL);
        //mybutton.Create(Exui主窗口, 50, 100, 300, 200, true, false, 255, 0, 0, NULL, NULL, 按钮EX1.Skin(), "动态创建的按钮", NULL, 0, 0, NULL, NULL);
        mybutton.Create(Window_Main, 50, 100, 300, 200, true, false, 255, 0, 0, NULL, NULL, 按钮EX1.Skin(), "动态创建的按钮", NULL, 0, 0, NULL, NULL);


        return 0;
    }

    if (CtrHandle == 按钮EX3)
    {
        exui::StringEx title1 = 按钮EX3.Title();
       wchar_t * t=  title1.GetStr();

      
     
        exui::StringEx title = TEXT("测试文本");
        title = title + "55555";
        按钮EX3.Title(title);
        按钮EX5.Title("你好Exui");
        列表框EX_1.InsertItem(-1, 1, 0, 多功能按钮EX1.Icon(), "测试项目c++", 0, 35);
        图片框EX1.Picture(多功能按钮EX1.Icon());
        PopUpTimePickEx(100, 100, 200, 200, TEXT("确定&&取消"), 0, TEXT(""), TEXT(""), TEXT(""), FALSE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        return 0;
    }

    if (CtrHandle == mybutton)
    {
        MessageBoxA(((HWND)0), "动态按钮被单击", "警告", 0);
    }
    return 0;
}


AutoInt	ExuiProgram_Main::OnWindowExEvent_L_Click(Control_P CtrHandle, AutoInt EventType, AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4)
{


    return 0;
}











