/*--------界面类声明开始--------*/
#pragma once
#include "C:\\ExuiApi\\include\\Exui.h"
#include "ExuiForCplusplus.h"
class ExuiProgram_Main_Base : public  exui::ExuiProgramEx
{
public:
AutoInt ExuiProgramRc = IDB_ExuiProgram_Main;//Exui界面数据,若项目位置迁移或从其它地方下载界面不能打开或编译时请确认文件引用位置是否正确
exui::WindowBoxEx Exui主窗口;//exui::WindowBoxEx,由Exui界面编辑器生成请勿修改
exui::WindowEx 窗口EX1;//exui::WindowEx,由Exui界面编辑器生成请勿修改
exui::PictureBoxEx 图片框EX1;//exui::PictureBoxEx,由Exui界面编辑器生成请勿修改
exui::FilterEx 滤镜EX3;//exui::FilterEx,由Exui界面编辑器生成请勿修改
exui::ButtonEx 按钮EX1;//exui::ButtonEx,由Exui界面编辑器生成请勿修改
exui::ButtonEx 按钮EX2;//exui::ButtonEx,由Exui界面编辑器生成请勿修改
exui::ButtonEx 按钮EX3;//exui::ButtonEx,由Exui界面编辑器生成请勿修改
exui::ButtonEx 按钮EX4;//exui::ButtonEx,由Exui界面编辑器生成请勿修改
exui::ButtonEx 按钮EX5;//exui::ButtonEx,由Exui界面编辑器生成请勿修改
exui::ButtonEx 测试提示框按钮EX6;//exui::ButtonEx,由Exui界面编辑器生成请勿修改
exui::ButtonEx 按钮EX7;//exui::ButtonEx,由Exui界面编辑器生成请勿修改
exui::ButtonEx 按钮EX8;//exui::ButtonEx,由Exui界面编辑器生成请勿修改
exui::ButtonEx 按钮EX9;//exui::ButtonEx,由Exui界面编辑器生成请勿修改
exui::EditboxEx 编辑框EX1;//exui::EditboxEx,由Exui界面编辑器生成请勿修改
exui::ColorPickEx 选色板EX1;//exui::ColorPickEx,由Exui界面编辑器生成请勿修改
exui::ColorPickEx 选色板EX2;//exui::ColorPickEx,由Exui界面编辑器生成请勿修改
exui::ComboboxEx 组合框EX1;//exui::ComboboxEx,由Exui界面编辑器生成请勿修改
exui::RichEditEx 富文本框EX1;//exui::RichEditEx,由Exui界面编辑器生成请勿修改
exui::MultifunctionButtonEx 多功能按钮EX1;//exui::MultifunctionButtonEx,由Exui界面编辑器生成请勿修改
exui::MultifunctionButtonEx 多功能按钮EX2;//exui::MultifunctionButtonEx,由Exui界面编辑器生成请勿修改
exui::AnimationbuttonEx 动画按钮EX2;//exui::AnimationbuttonEx,由Exui界面编辑器生成请勿修改
exui::CalendarBoxEx 日历框EX1;//exui::CalendarBoxEx,由Exui界面编辑器生成请勿修改
exui::FilterEx 滤镜EX1;//exui::FilterEx,由Exui界面编辑器生成请勿修改
exui::AnimationbuttonEx 动画按钮EX1;//exui::AnimationbuttonEx,由Exui界面编辑器生成请勿修改
exui::FilterEx 滤镜EX2;//exui::FilterEx,由Exui界面编辑器生成请勿修改
exui::ListboxEx 列表框EX_1;//exui::ListboxEx,由Exui界面编辑器生成请勿修改
exui::ButtonEx 按钮EX_1;//exui::ButtonEx,由Exui界面编辑器生成请勿修改
exui::MultifunctionButtonEx 多功能按钮EX_1;//exui::MultifunctionButtonEx,由Exui界面编辑器生成请勿修改
exui::MultifunctionButtonEx MultifunctionButtonEx_1;//exui::MultifunctionButtonEx,由Exui界面编辑器生成请勿修改
exui::LabelEx LabelEx_1;//exui::LabelEx,由Exui界面编辑器生成请勿修改
exui::MinutesboxEx MinutesboxEx_1;//exui::MinutesboxEx,由Exui界面编辑器生成请勿修改
};

/*--------界面类声明结束--------*/



/*--------请在下方写你自己的业务代码--------*/


class ExuiProgram_Main : public  ExuiProgram_Main_Base
{


public:

    AutoInt	OnWindowExEvent_L_Click(Control_P CtrHandle, AutoInt EventType, AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4);
    AutoInt	OnButtonExEvent_L_Click(Control_P CtrHandle, AutoInt EventType, AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4);
    AutoInt OnMinutesboxExEvent_MouseWheel(MinutesboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2);
    AutoInt OnWindowBoxExEvent_Create(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam);
    exui::ButtonEx mybutton;


};


