#pragma once
#include "ExuiApi.h"

//BindataEx
namespace exui
{
	class BindataEx
	{
	private:
		BinEx_P BinEx = NULL;
	public:

		BindataEx();
		BindataEx(const byte* data, int datalong);
		BindataEx(const BinEx_P data);
		BindataEx(const BindataEx& data);
		~BindataEx();

		BindataEx& operator=(const BinEx_P data);
		BindataEx& operator=(const BindataEx& data);
		BindataEx operator+(const BinEx_P data);
		BindataEx operator+(const BindataEx& data);
		BindataEx& operator+=(const BinEx_P data);
		BindataEx& operator+=(const BindataEx& data);
		bool operator!=(const BinEx_P data);
		bool operator!=(const BindataEx& data);
		bool operator==(const BinEx_P data);
		bool operator==(const BindataEx& data);


		int GetDataLong();
		operator BinEx_P() const { return BinEx; }

	};
}
//StringEx
namespace exui
{


	class StringEx
	{

	private:
		StrEx_P StrEx = NULL;
	public:
		StringEx();
		StringEx(const char* str);
		StringEx(const wchar_t* str);
		StringEx(const StrEx_P str);
		StringEx(const StringEx& str);
		~StringEx();

		operator StrEx_P() const { return StrEx; }


		StringEx& operator=(const char* str);
		StringEx& operator=(const wchar_t* str);
		StringEx& operator=(const StrEx_P str);
		StringEx& operator=(const StringEx& str);

		StringEx operator+(const char* str);
		StringEx operator+(const wchar_t* str);
		StringEx operator+(const StrEx_P str);
		StringEx operator+(const StringEx& str);

		StringEx& operator+=(const char* str);
		StringEx& operator+=(const wchar_t* str);
		StringEx& operator+=(const StrEx_P str);
		StringEx& operator+=(const StringEx& str);



		bool operator>(const char* str);
		bool operator>(const  wchar_t* str);
		bool operator>(const  StrEx_P str);
		bool operator>(const StringEx& str);


		bool operator<(const char* str);
		bool operator<(const  wchar_t* str);
		bool operator<(const  StrEx_P str);
		bool operator<(const StringEx& str);

		bool operator!=(const char* str);
		bool operator!=(const  wchar_t* str);
		bool operator!=(const  StrEx_P str);
		bool operator!=(const StringEx& str);


		bool operator==(const char* str);
		bool operator==(const  wchar_t* str);
		bool operator==(const  StrEx_P str);
		bool operator==(const StringEx& str);


		bool operator>=(const char* str);
		bool operator>=(const  wchar_t* str);
		bool operator>=(const  StrEx_P str);
		bool operator>=(const StringEx& str);


		bool operator<=(const char* str);
		bool operator<=(const  wchar_t* str);
		bool operator<=(const  StrEx_P str);
		bool operator<=(const StringEx& str);






		int Cmp(const char* str);
		int Cmp(const  wchar_t* str);
		int Cmp(const  StrEx_P str);
		int Cmp(const StringEx& str);
		/*
		//尺寸
		int size();
		BOOL empty();
		void clear();
		char* c_str();
		//添加文本
		append();
		//删除文本
		erase();
		//插入文本
		insert();
		//替换文本
		replace();
		//取指定文本
		substr();
		//寻找文本
		find();
		//字符
		*/
		wchar_t* GetStr();
		StrEx_P GetStrEx();

	};


}
//BitmapEx
namespace exui
{
	class BitmapEx
	{
	private:
		ImageEx_P ImageHandle = NULL;
	public:

		BitmapEx();

		BitmapEx(const BindataEx& data, int Mode = 0, int Width = 0, int Height = 0, int Angle = 0, int LineClour = 0, int LineWidth = 0, int Alignment = 0);
		BitmapEx(const StringEx& data, int Mode = 0, int Width = 0, int Height = 0, int Angle = 0, int LineClour = 0, int LineWidth = 0, int Alignment = 0);
		BitmapEx(const byte* data, int datalong, int Mode = 0, int Width = 0, int Height = 0, int Angle = 0, int LineClour = 0, int LineWidth = 0, int Alignment = 0);
		BitmapEx(const ImageEx_P data);
		BitmapEx(const BitmapEx& data);
		~BitmapEx();

		operator ImageEx_P() const { return ImageHandle; }

		BitmapEx& operator=(const BitmapEx& data);
		BitmapEx& operator=(const ImageEx_P& data);
		bool operator==(const BitmapEx& data);
		bool operator==(const ImageEx_P& data);



		/*--------装载位图--------*/
		ImageEx_P Load(BindataEx data, int Mode, int Width, int Height, int Angle, int LineClour, int LineWidth, int Alignment);
		/*--------装载位图--------*/
		ImageEx_P Load(StringEx data, int Mode, int Width, int Height, int Angle, int LineClour, int LineWidth, int Alignment);
		/*--------装载位图--------*/
		ImageEx_P Load(const byte* data, int datalong, int Mode, int Width, int Height, int Angle, int LineClour, int LineWidth, int Alignment);

		/*--------销毁位图--------*/
		void Destroy();

		/*--------取位图属性--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------取位图数据--------*/
		BindataEx GetData();


	};
}
//BaseEx
namespace exui
{

	class BaseEx
	{

	public:
		Control_P control = NULL;
		AutoInt controltype = NULL;
	public:
		BaseEx();
		BaseEx(const Control_P controlhandle);
		BaseEx(const BaseEx& Struct);
		~BaseEx();
		operator Control_P() const { return control; }

	public:

		void SetHandle(Control_P ControlHamdle);
		Control_P GetHandle();

		//取句柄
		//取所在窗口

		virtual AutoInt GetType();
		virtual void SetType(AutoInt type);

		bool operator==(const Control_P controlhandle);
		bool operator==(const BaseEx& baseex);

		bool operator!=(const Control_P controlhandle);
		bool operator!=(const BaseEx& baseex);


		BaseEx& operator=(const StructEx_P controlhandle);
		BaseEx& operator=(const BaseEx& baseex);

	};


}
//StructEx
namespace exui
{

	class StructEx
	{
	private:
		StructEx_P Struct = NULL;
	public:

		StructEx();
		StructEx(const StructEx_P Struct);
		StructEx(const StructEx& Struct);
		~StructEx();


		operator StructEx_P() const { return Struct; }


		bool operator==(const  StructEx_P StructHandle);

		bool operator==(const StructEx& structex);

		bool operator!=(const  StructEx_P StructHandle);
		bool operator!=(const StructEx& structex);



		StructEx& operator=(const StructEx_P StructHandle);
		StructEx& operator=(const StructEx& structex);



	public:

		/*--------创建--------*/
		StructEx_P Create();

		/*--------销毁--------*/
		void Destroy();

		/*--------载入--------*/
		BOOL Load(BindataEx Data, AutoInt DataType, AutoInt Parameter1, AutoInt Parameter2, AutoInt Callback, AutoInt CallbackParameter);

		/*--------打包--------*/
		BindataEx Pack(AutoInt DataType, AutoInt Parameter1, AutoInt Parameter2, AutoInt Callback, AutoInt CallbackParameter);

		/*--------插入--------*/
		AutoInt InsertMember(AutoInt Member, AutoInt addnum);

		/*--------删除--------*/
		void DeleteMember(AutoInt delMember, AutoInt deletenum);

		/*--------取数量--------*/
		AutoInt GetMemberCount();

		/*--------取类型--------*/
		int GetMemberType(AutoInt Member);

		/*--------置名称--------*/
		void SetMemberName(AutoInt Member, StringEx Name);

		/*--------取名称--------*/
		StringEx GetMemberName(AutoInt Member);

		/*--------置逻辑--------*/
		void SetMemberBool(AutoInt Member, BOOL logic);

		/*--------取逻辑--------*/
		BOOL GetMemberBool(AutoInt Member);

		/*--------置整数--------*/
		void SetMemberInt(AutoInt Member, int integer);

		/*--------取整数--------*/
		int GetMemberInt(AutoInt Member);

		/*--------置长整数--------*/
		void SetMemberLongInt(AutoInt Member, INT64 longinteger);

		/*--------取长整数--------*/
		INT64 GetMemberLongInt(AutoInt Member);

		/*--------置小数--------*/
		void SetMemberFloat(AutoInt Member, float decimal);

		/*--------取小数--------*/
		float GetMemberFloat(AutoInt Member);

		/*--------置双精度小数--------*/
		void SetMemberDouble(AutoInt Member, double DoubleDecimal);

		/*--------取双精度小数--------*/
		double GetMemberDouble(AutoInt Member);

		/*--------置文本--------*/
		void SetMemberText(AutoInt Member, StringEx Text);

		/*--------取文本--------*/
		StringEx GetMemberText(AutoInt Member);

		/*--------置数据--------*/
		void SetMemberBin(AutoInt Member, BindataEx Bin);

		/*--------取数据--------*/
		BindataEx GetMemberBin(AutoInt Member);

		/*--------置结构体--------*/
		void SetMemberStruct(AutoInt Member, StructEx StructInfo);

		/*--------取结构体--------*/
		StructEx GetMemberStruct(AutoInt Member);

		/*--------插入--------*/
		AutoInt InsertAttr(AutoInt Attr, AutoInt addnum);

		/*--------删除--------*/
		void DeleteAttr(AutoInt delAttr, AutoInt deletenum);

		/*--------取数量--------*/
		AutoInt GetAttrCount();

		/*--------取类型--------*/
		int GetAttrType(AutoInt Attr);

		/*--------置名称--------*/
		void SetAttrName(AutoInt Attr, StringEx Name);

		/*--------取名称--------*/
		StringEx GetAttrName(AutoInt Attr);

		/*--------置逻辑--------*/
		void SetAttrBool(AutoInt Attr, BOOL logic);

		/*--------取逻辑--------*/
		BOOL GetAttrBool(AutoInt Attr);

		/*--------置整数--------*/
		void SetAttrInt(AutoInt Attr, int integer);

		/*--------取整数--------*/
		int GetAttrInt(AutoInt Attr);

		/*--------置长整数--------*/
		void SetAttrLongInt(AutoInt Attr, INT64 longinteger);

		/*--------取长整数--------*/
		INT64 GetAttrLongInt(AutoInt Attr);

		/*--------置小数--------*/
		void SetAttrFloat(AutoInt Attr, float decimal);

		/*--------取小数--------*/
		float GetAttrFloat(AutoInt Attr);

		/*--------置双精度小数--------*/
		void SetAttrDouble(AutoInt Attr, double DoubleDecimal);

		/*--------取双精度小数--------*/
		double GetAttrDouble(AutoInt Attr);

		/*--------置文本--------*/
		void SetAttrText(AutoInt Attr, StringEx Text);

		/*--------取文本--------*/
		StringEx GetAttrText(AutoInt Attr);

		/*--------置数据--------*/
		void SetAttrBin(AutoInt Attr, BindataEx Bin);

		/*--------取数据--------*/
		BindataEx GetAttrBin(AutoInt Attr);

		/*--------置结构体--------*/
		void SetAttrStruct(AutoInt Attr, StructEx StructInfo);

		/*--------取结构体--------*/
		StructEx GetAttrStruct(AutoInt Attr);
	};

};
//ControlEx
namespace exui
{

	class ControlEx : public BaseEx
	{

	public:
		ControlEx();
		~ControlEx();
		AutoInt GetType() { return ControlGetcontrolType(control); };

	public:

		// friend class ExuiProgram;

		/*--------组件销毁--------*/
		void Destroy();

		/*--------组件是否已创建--------*/
		BOOL IsCreate();

		/*--------组件发送消息--------*/
		AutoInt SendMessage(AutoInt msg, AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4);

		/*--------组件发送子级消息--------*/
		void SendChildMessage(AutoInt msg, AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4);

		/*--------组件置左边--------*/
		void Left(int Left);

		/*--------组件取左边--------*/
		int Left();

		/*--------组件置顶边--------*/
		void Top(int Top);

		/*--------组件取顶边--------*/
		int Top();

		/*--------组件置宽度--------*/
		void Width(int Width);

		/*--------组件取宽度--------*/
		int Width();

		/*--------组件置高度--------*/
		void Height(int Height);

		/*--------组件取高度--------*/
		int Height();

		/*--------组件取可视--------*/
		BOOL Visual();

		/*--------组件置可视--------*/
		void Visual(BOOL Visual);

		/*--------组件取禁止--------*/
		BOOL Disabled();

		/*--------组件置禁止--------*/
		void Disabled(BOOL UpdateDisabled);

		/*--------组件置光标--------*/
		void Cursor(BindataEx Cursor);

		/*--------组件取光标--------*/
		BindataEx Cursor();

		/*--------组件置光标id--------*/
		void CursorId(int CursorId);

		/*--------组件取光标id--------*/
		int CursorId();

		/*--------组件取鼠标穿透--------*/
		int Penetrate();

		/*--------组件置鼠标穿透--------*/
		void Penetrate(int Penetrate);

		/*--------组件取透明度--------*/
		int Transparency();

		/*--------组件置透明度--------*/
		void Transparency(int Transparency);

		/*--------组件置焦点权重--------*/
		void FocusWeight(int FocusWeight);

		/*--------组件取焦点权重--------*/
		int FocusWeight();

		/*--------组件置标识--------*/
		void Sign(AutoInt sign);

		/*--------组件取标识--------*/
		AutoInt Sign();

		/*--------组件取矩形--------*/
		void GetRect(int* Left, int* Top, int* Width, int* Height);

		/*--------组件置矩形--------*/
		void SetRect(int Left, int Top, int Width, int Height);

		/*--------组件取真实可视--------*/
		BOOL GetTrueVisual();

		/*--------组件取真实禁止--------*/
		BOOL GetTrueDisabled();

		/*--------组件更新缓存--------*/
		BOOL UpdateCache(int Mode);

		/*--------组件取距窗口顶边--------*/
		int GetWindowTop();

		/*--------组件取距窗口左边--------*/
		int GetWindowLeft();

		/*--------组件置焦点组件--------*/
		void SetFocusControl();

		/*--------组件取焦点组件--------*/
		Control_P GetFocusControl();

		/*--------组件取上一焦点组件--------*/
		Control_P GetMaxFocusWeightControl();

		/*--------组件取下一焦点组件--------*/
		Control_P GetNextFocusControl();

		/*--------组件取鼠标捕获组件--------*/
		Control_P GetCaptureControl();

		/*--------组件置鼠标捕获组件--------*/
		void SetCaptureControl();

		/*--------组件取热点组件--------*/
		Control_P GetHotControl();

		/*--------组件取左键按下组件--------*/
		Control_P GetLeftPressControl();

		/*--------组件取右键按下组件--------*/
		Control_P GetRightPressControl();

		/*--------组件置类型--------*/
		void SetcontrolType(int controlType);

		/*--------组件取类型--------*/
		int GetcontrolType();

		/*--------组件置回调--------*/
		ExuiCallback SetCallback(int mode, ExuiCallback Callback);

		/*--------组件取回调--------*/
		ExuiCallback GetCallback(int Mode);

		/*--------组件调用回调--------*/
		AutoInt InvokeCallback(AutoInt msg, AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4, ExuiCallback ExuiCallback);

		/*--------组件取所在窗口句柄--------*/
		HWND GetWindow();

		/*--------组件取组件绑定窗口--------*/
		HWND GetBindWin();

		/*--------组件取窗口绑定组件--------*/
		Control_P GetBindControl(HWND WindowHandle);

		/*--------组件取设备上下文--------*/
		HDC GetDc();

		/*--------组件取图形--------*/
		AutoInt GetGraphics();

		/*--------组件取位图--------*/
		AutoInt GetHBITMAP();

		/*--------组件添加重绘区--------*/
		void AddRedrawRect(int Left, int Top, int Width, int Height);

		/*--------组件请求重绘--------*/
		void Redraw();

		/*--------组件暂停重画--------*/
		int LockUpdate();

		/*--------组件容许重画--------*/
		int UnlockUpdate();

		/*--------组件取重画锁定计数--------*/
		int GetLockUpdateCount();

		/*--------组件取父组件--------*/
		Control_P GetParentControl();

		/*--------组件置父组件--------*/
		BOOL SetParentControl(Control_P Parentcontrol, BOOL mode, int NewLeft, int NewTop);

		/*--------组件可有子级--------*/
		BOOL HaveChild();

		/*--------组件取子级组件数--------*/
		int GetChildCount(BOOL Mode);

		/*--------组件枚举子级组件--------*/
		BindataEx EnumerateChild(BOOL Mode);

		/*--------组件取上一组件--------*/
		Control_P GetLastcontrol();

		/*--------组件取下一组件--------*/
		Control_P GetNextcontrol();

		/*--------组件取嵌套层次--------*/
		int GetNestingLevel();

		/*--------组件取层次组件--------*/
		Control_P Getlevelcontrol(AutoInt level);

		/*--------组件置层次--------*/
		void Setlevel(int mode, Control_P RelativeControl);

		/*--------组件取层次--------*/
		int Getlevel();

		/*--------组件置所在窗口分层透明--------*/
		void SetWinLayered(int Layered);

		/*--------组件取所在窗口分层透明--------*/
		AutoInt GetWinLayered();

		/*--------组件取所在窗口图形--------*/
		AutoInt GetWinGraphics();

		/*--------组件取所在窗口位图--------*/
		HBITMAP GetWinHBITMAP();

		/*--------组件取所在窗口设备上下文--------*/
		HDC GetWinDc();

		/*--------组件置所在窗口刷新回调--------*/
		AutoInt SetWinRefreshCallBack(AutoInt CallBack);

		/*--------组件取所在窗口刷新回调--------*/
		AutoInt GetWinRefreshCallBack();

		/*--------组件刷新所在窗口--------*/
		void RefreshWin();

		/*--------组件暂停重画所在窗口--------*/
		int LockWinUpdate();

		/*--------组件容许重画所在窗口--------*/
		int UnlockWinUpdate();

		/*--------组件取所在窗口重画锁定状态--------*/
		int GetWinLockUpdateCount();

		/*--------组件取所在页面--------*/
		int GetPage();

		/*--------组件置所在页面--------*/
		void SetPage(int Page);

		/*--------组件取当前页面--------*/
		int GetCurrentPage();

		/*--------组件置当前页面--------*/
		void SetCurrentPage(int Page);

		/*--------组件取布局器--------*/
		BindataEx GetLayoutConfig();

		/*--------组件置布局器--------*/
		void SetLayoutConfig(BindataEx LayoutConfig);

		/*--------组件提交任务--------*/
		AutoInt SubmitTask(AutoInt type, AutoInt cmd, AutoInt sign, AutoInt Parame);

		/*--------组件执行元素命令--------*/
		AutoInt RunElemCmd(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt Cmd, AutoInt parameter1, AutoInt parameter2, AutoInt parameter3, AutoInt parameter4, AutoInt parameter5, AutoInt parameter6, AutoInt parameter7, AutoInt parameter8, AutoInt parameter9, AutoInt parameter10);

		/*--------置底层元素数量--------*/
		void SetUnderlyElemCount(AutoInt Type, AutoInt cloid, AutoInt Num);

		/*--------取底层元素数量--------*/
		AutoInt GetUnderlyElemCount(AutoInt Type, AutoInt cloid);

		/*--------组件插入元素--------*/
		AutoInt InsertElem(AutoInt Type, AutoInt cloid, AutoInt ctrlindex, AutoInt addnum);

		/*--------组件删除元素--------*/
		void DeleteElem(AutoInt Type, AutoInt cloid, AutoInt delindex, AutoInt deletenum);

		/*--------组件取元素数量--------*/
		AutoInt GetElemCount(AutoInt Type, AutoInt cloid);

		/*--------组件重置元素--------*/
		void ResetElem(AutoInt Type, AutoInt cloid, AutoInt delindex, AutoInt deletenum);

		/*--------组件置元素属性_图片--------*/
		void SetElemAttribute_Imsge(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle);

		/*--------组件取元素属性_图片--------*/
		AutoInt GetElemAttribute_Imsge(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId);

		/*--------组件置元素内容_图片--------*/
		void SetElemData_Imsge(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, BitmapEx IcoData, int Align, int PlayAnimation);

		/*--------组件取元素内容_图片--------*/
		AutoInt GetElemData_Imsge(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId);

		/*--------组件置元素属性_文本--------*/
		void SetElemAttribute_Text(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle, BindataEx Font, int Align);

		/*--------组件取元素属性_文本--------*/
		AutoInt GetElemAttribute_Text(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId);

		/*--------组件置元素内容_文本--------*/
		void SetElemData_Text(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, StringEx Title, int FontColor);

		/*--------组件取元素内容_文本--------*/
		AutoInt GetElemData_Text(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId);

		/*--------组件置元素属性_按钮--------*/
		void SetElemAttribute_Button(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle, BindataEx Skin, BindataEx Font);

		/*--------组件取元素属性_按钮--------*/
		AutoInt GetElemAttribute_Button(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId);

		/*--------组件置元素内容_按钮--------*/
		void SetElemData_Button(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, StringEx Title, int FontColor, BOOL Select);

		/*--------组件取元素内容_按钮--------*/
		AutoInt GetElemData_Button(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId);

		/*--------组件置元素属性_选择框--------*/
		void SetElemAttribute_Select(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle, BindataEx Skin, BindataEx Font, int IconWidth, int IconHeight, int SelectMode);

		/*--------组件取元素属性_选择框--------*/
		AutoInt GetElemAttribute_Select(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId);

		/*--------组件置元素内容_选择框--------*/
		void SetElemData_Select(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, StringEx Title, int FontColor, int Select);

		/*--------组件取元素内容_选择框--------*/
		AutoInt GetElemData_Select(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId);

		/*--------组件置元素属性_编辑文本--------*/
		void SetElemAttribute_EditText(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle, int ShowMode, BindataEx Skin, BindataEx Font, int Align, StringEx PasswordSubstitution, BindataEx Cursor, BOOL Multiline, int InputMode, int MaxInput, BindataEx EditFont, int EditFontColor, int CursorColor, int SelectedFontColor, int SelectedColor, int MenuTableWidth, int MenuTableHeight, BindataEx MenuFont, int MenuFontClour, int MenuDisabledFontClour, int MenuLanguage);

		/*--------组件取元素属性_编辑文本--------*/
		AutoInt GetElemAttribute_EditText(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId);

		/*--------组件置元素内容_编辑文本--------*/
		void SetElemData_EditText(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, StringEx Content, int FontClour);

		/*--------组件取元素内容_编辑文本--------*/
		AutoInt GetElemData_EditText(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId);

		/*--------组件置元素属性_组合按钮--------*/
		void SetElemAttribute_ComboButton(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle, BindataEx Skin, BindataEx Font, int IconWidth, int IconHeight, int Align);

		/*--------组件取元素属性_组合按钮--------*/
		AutoInt GetElemAttribute_ComboButton(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId);

		/*--------组件置元素内容_组合按钮--------*/
		void SetElemData_ComboButton(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, int BackColor, BitmapEx image, StringEx Title, int FontColor, int PartnerStay);

		/*--------组件取元素内容_组合按钮--------*/
		AutoInt GetElemData_ComboButton(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId);

		/*--------组件置元素属性_进度条--------*/
		void SetElemAttribute_Progressbar(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle, BindataEx Skin, int Style);

		/*--------组件取元素属性_进度条--------*/
		AutoInt GetElemAttribute_Progressbar(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId);

		/*--------组件置元素内容_进度条--------*/
		void SetElemData_Progressbar(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, int Position);

		/*--------组件取元素内容_进度条--------*/
		AutoInt GetElemData_Progressbar(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId);

		/*--------组件置元素属性_滑块条--------*/
		void SetElemAttribute_Sliderbar(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle, BindataEx Skin, int Style);

		/*--------组件取元素属性_滑块条--------*/
		AutoInt GetElemAttribute_Sliderbar(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId);

		/*--------组件置元素内容_滑块条--------*/
		void SetElemData_Sliderbar(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, int Position);

		/*--------组件取元素内容_滑块条--------*/
		AutoInt GetElemData_Sliderbar(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId);

		/*--------组件置元素属性_颜色--------*/
		void SetElemAttribute_Colour(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle);

		/*--------组件取元素属性_颜色--------*/
		AutoInt GetElemAttribute_Colour(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId);

		/*--------组件置元素内容_颜色--------*/
		void SetElemData_Colour(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, int style, int Colour);

		/*--------组件取元素内容_颜色--------*/
		AutoInt GetElemData_Colour(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId);

		/*--------组件置元素属性_直线--------*/
		void SetElemAttribute_Line(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle, int LineWidth);

		/*--------组件取元素属性_直线--------*/
		AutoInt GetElemAttribute_Line(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId);

		/*--------组件置元素内容_直线--------*/
		void SetElemData_Line(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, int style, int Colour);

		/*--------组件取元素内容_直线--------*/
		AutoInt GetElemData_Line(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId);

		/*--------组件置元素属性_自定义--------*/
		void SetElemAttribute_Custom(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle, BindataEx CustomTag, BindataEx CustomElemAttr9, BindataEx CustomElemAttr10, BindataEx CustomElemAttr11, BindataEx CustomElemAttr12, BindataEx CustomElemAttr13, BindataEx CustomElemAttr14, BindataEx CustomElemAttr15, BindataEx CustomElemAttr16, BindataEx CustomElemAttr17, BindataEx CustomElemAttr18, BindataEx CustomElemAttr19, BindataEx CustomElemAttr20, BindataEx CustomElemAttr21, BindataEx CustomElemAttr22, BindataEx CustomElemAttr23, BindataEx CustomElemAttr24, BindataEx CustomElemAttr25, BindataEx CustomElemAttr26, BindataEx CustomElemAttr27, BindataEx CustomElemAttr28, BindataEx CustomElemAttr29, BindataEx CustomElemAttr30, BindataEx CustomElemAttr31, BindataEx CustomElemAttr32);

		/*--------组件取元素属性_自定义--------*/
		AutoInt GetElemAttribute_Custom(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId);

		/*--------组件置元素内容_自定义--------*/
		void SetElemData_Custom(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, BindataEx CustomElemData3, BindataEx CustomElemData4, BindataEx CustomElemData5, BindataEx CustomElemData6, BindataEx CustomElemData7, BindataEx CustomElemData8, BindataEx CustomElemData9, BindataEx CustomElemData10, BindataEx CustomElemData11, BindataEx CustomElemData12, BindataEx CustomElemData13, BindataEx CustomElemData14, BindataEx CustomElemData15, BindataEx CustomElemData16, BindataEx CustomElemData17, BindataEx CustomElemData18, BindataEx CustomElemData19, BindataEx CustomElemData20, BindataEx CustomElemData21, BindataEx CustomElemData22, BindataEx CustomElemData23, BindataEx CustomElemData24, BindataEx CustomElemData25, BindataEx CustomElemData26, BindataEx CustomElemData27, BindataEx CustomElemData28, BindataEx CustomElemData29, BindataEx CustomElemData30, BindataEx CustomElemData31, BindataEx CustomElemData32);

		/*--------组件取元素内容_自定义--------*/
		AutoInt GetElemData_Custom(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId);

		/*--------组件置元素属性_组件--------*/
		void SetElemAttribute_Control(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle);

		/*--------组件取元素属性_组件--------*/
		AutoInt GetElemAttribute_Control(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId);

		/*--------组件置元素内容_组件--------*/
		void SetElemData_Control(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, BindataEx UserData, int Mode, Control_P Handle, BindataEx  Data);

		/*--------组件取元素内容_组件--------*/
		AutoInt GetElemData_Control(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId);


	};


}
//TimerEx
namespace exui
{
	class TimerEx : public BaseEx
	{

	public:
		TimerEx();
		~TimerEx();

	private:
	public:
		/*--------创建定时器--------*/
		TimeEx_P Create(BaseEx& Parent, AutoInt Period, AutoInt Parameter, BOOL Reentry, AutoInt Flags);

		/*--------销毁定时器--------*/
		void Delete();

		/*--------置定时器配置--------*/
		void SetConfig(AutoInt type, AutoInt value);

		/*--------取定时器配置--------*/
		AutoInt GetConfig(AutoInt type);
	};
}
//SlowMotionTaskEx
namespace exui
{
	class SlowMotionTaskEx : public BaseEx
	{
	public:
		SlowMotionTaskEx();
		~SlowMotionTaskEx();

	private:
	public:
		/*--------创建缓动任务--------*/
		SlowMotionTaskEx_P Create(BaseEx& Parent, AutoInt Num, AutoInt Mode, AutoInt Star, AutoInt End, AutoInt FrameDelay, AutoInt Step, AutoInt parameter, BOOL Wait, BOOL ManualDestroy);

		/*--------置缓动任务配置--------*/
		void SetTaskConfigEx(AutoInt index, AutoInt Mode, AutoInt Star, AutoInt End);

		/*--------执行缓动任务命令--------*/
		AutoInt RunCmd(AutoInt Cmd, AutoInt index, AutoInt param1, AutoInt param2, AutoInt param3, AutoInt param4);

		/*--------销毁缓动队列任务--------*/
		void Destroy();
	};
}
//MenuEx
namespace exui
{
	class MenuEx : public ControlEx
	{
	public:
		MenuEx();
		~MenuEx();
		AutoInt GetType() { return ControlType_MenuEx; };
	private:


	public:

		/*--------创建_菜单Ex--------*/
		MenuEx_P Create(BaseEx& Parent, int IconWidth, int IconHeight, BindataEx Skin, BindataEx Font, BindataEx Cursor, int Transparency, AutoInt MenuTag);

		/*--------置属性_菜单Ex--------*/
		void SetAttribute(AutoInt AttributeId, int IconWidth, int IconHeight, BindataEx Skin, BindataEx Font, BindataEx Cursor, int Transparency, AutoInt MenuTag);

		/*--------取属性_菜单Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------销毁_菜单Ex--------*/
		void Destroy();

		/*--------是否已弹出_菜单Ex--------*/
		BOOL IsPopUp(MenuInfoEx_P MenuId);

		/*--------弹出_菜单Ex--------*/
		void PopUp(MenuInfoEx_P MenuId, int left, int top, HWND hWndParent, BOOL Modal, Control_P ExceptionControl, int ReferType, Control_P ReferControl);

		/*--------关闭_菜单Ex--------*/
		void Close(MenuInfoEx_P MenuIdl, BOOL Mode);

		/*--------插入_菜单Ex--------*/
		AutoInt InsertItem(MenuInfoEx_P ParentMenu, AutoInt index, AutoInt Data, BitmapEx Ico, StringEx Title, int FontColor, int DisabledFontColor, int Width, int Height, int Type, BOOL Disabled);

		/*--------删除_菜单Ex--------*/
		void DeleteItem(MenuInfoEx_P MenuId);

		/*--------取数量_菜单Ex--------*/
		AutoInt GetItemCount();

		/*--------置图标_菜单Ex--------*/
		void SetItemIco(MenuInfoEx_P MenuId, BitmapEx Ico);

		/*--------取图标_菜单Ex--------*/
		BitmapEx GetItemIco(MenuInfoEx_P MenuId);

		/*--------置标题_菜单Ex--------*/
		void SetItemTitle(MenuInfoEx_P MenuId, StringEx Title);

		/*--------取标题_菜单Ex--------*/
		StringEx GetItemTitle(MenuInfoEx_P MenuId);

		/*--------取附加值_菜单Ex--------*/
		AutoInt GetItemData(MenuInfoEx_P MenuId);

		/*--------置附加值_菜单Ex--------*/
		void SetItemData(MenuInfoEx_P MenuId, AutoInt Data);

		/*--------取字体色_菜单Ex--------*/
		int GetItemFontColor(MenuInfoEx_P MenuId);

		/*--------置字体色_菜单Ex--------*/
		void SetItemFontColor(MenuInfoEx_P MenuId, int FontColor);

		/*--------取禁止字体色_菜单Ex--------*/
		int GetItemDisabledFontColor(MenuInfoEx_P MenuId);

		/*--------置禁止字体色_菜单Ex--------*/
		void SetItemDisabledFontColor(MenuInfoEx_P MenuId, int DisabledFontColor);

		/*--------置项目信息_菜单Ex--------*/
		void SetItemInfo(MenuInfoEx_P MenuId, int Type, AutoInt Data);

		/*--------取项目信息_菜单Ex--------*/
		AutoInt GetItemInfo(MenuInfoEx_P MenuId, int Type);

		/*--------置类型_菜单Ex--------*/
		void SetItemType(MenuInfoEx_P MenuId, int Type);

		/*--------取类型_菜单Ex--------*/
		int GetItemType(MenuInfoEx_P MenuId);

		/*--------取禁止_菜单Ex--------*/
		BOOL GetItemDisabled(MenuInfoEx_P MenuId);

		/*--------置禁止_菜单Ex--------*/
		void SetItemDisabled(MenuInfoEx_P MenuId, BOOL Disabled);

		/*--------置项目宽度_菜单Ex--------*/
		void SetItemWidth(MenuInfoEx_P MenuId, int Width);

		/*--------取项目宽度_菜单Ex--------*/
		int GetItemWidth(MenuInfoEx_P MenuId);

		/*--------置项目高度_菜单Ex--------*/
		void SetItemHeight(MenuInfoEx_P MenuId, int Height);

		/*--------取项目高度_菜单Ex--------*/
		int GetItemHeight(MenuInfoEx_P MenuId);

		/*--------取子菜单数量_菜单Ex--------*/
		int GetSubItemCount(MenuInfoEx_P MenuId);

		/*--------取子菜单_菜单Ex--------*/
		MenuInfoEx_P GetSubItem(MenuInfoEx_P MenuId, AutoInt index);

	};

};
//DownlistEx
namespace exui
{
	class DownlistEx : public ControlEx
	{
	public:
		DownlistEx();
		~DownlistEx();
		AutoInt GetType() { return ControlType_DownlistEx; };
	private:


	public:

		/*--------创建_下拉列表Ex--------*/
		DownlistEx_P Create(BaseEx& Parent, int IconWidth, int IconHeight, BindataEx Skin, BindataEx Font, BindataEx Cursor, BOOL AlternateColor, int ScrollBarMode, AutoInt CurrentSelection, int Transparency, AutoInt ListTag);

		/*--------销毁_下拉列表Ex--------*/
		void Destroy();

		/*--------置属性_下拉列表Ex--------*/
		void SetAttribute(AutoInt AttributeId, int IconWidth, int IconHeight, BindataEx Skin, BindataEx Font, BindataEx Cursor, BOOL AlternateColor, int ScrollBarMode, int CurrentSelection, int Transparency, int ListTag);

		/*--------取属性_下拉列表Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------弹出_下拉列表Ex--------*/
		void PopUp(int X, int Y, int nWidth, int nHeight, HWND hWndParent, BOOL Modal, Control_P ExceptionControl, int ReferType, Control_P ReferControl);

		/*--------关闭_下拉列表Ex--------*/
		void Close();

		/*--------是否已弹出_下拉列表Ex--------*/
		BOOL IsPopUp();

		/*--------插入_下拉列表Ex--------*/
		AutoInt InsertItem(AutoInt index, AutoInt addnum, AutoInt Data, BitmapEx Ico, StringEx Title, int FontColor, int Size);

		/*--------删除_下拉列表Ex--------*/
		void DeleteItem(AutoInt index, AutoInt deletenum);

		/*--------取数量_下拉列表Ex--------*/
		AutoInt GetItemCount();

		/*--------置附加值_下拉列表Ex--------*/
		void SetItemData(AutoInt index, AutoInt Data);

		/*--------取附加值_下拉列表Ex--------*/
		AutoInt GetItemData(AutoInt index);

		/*--------置图标_下拉列表Ex--------*/
		void SetItemIco(AutoInt index, BitmapEx Ico);

		/*--------取图标_下拉列表Ex--------*/
		BitmapEx GetItemIco(AutoInt index);

		/*--------置标题_下拉列表Ex--------*/
		void SetItemTitle(AutoInt index, StringEx Title);

		/*--------取标题_下拉列表Ex--------*/
		StringEx GetItemTitle(AutoInt index);

		/*--------置字体色_下拉列表Ex--------*/
		void SetItemFontColor(AutoInt index, int FontColor);

		/*--------取字体色_下拉列表Ex--------*/
		int GetItemFontColor(AutoInt index);

		/*--------置项目高度_下拉列表Ex--------*/
		void SetItemSize(AutoInt index, int Size);

		/*--------取项目高度_下拉列表Ex--------*/
		int GetItemSize(AutoInt index);

		/*--------保证显示_下拉列表Ex--------*/
		void GuaranteeVisible(AutoInt index, int mode);
	};

};
//WindowBoxEx
namespace exui
{
	class WindowBoxEx : public BaseEx
	{
	public:
		WindowBoxEx();
		~WindowBoxEx();
		AutoInt GetType() { return ControlType_WindowBoxEx; };
	private:


	public:

		/*--------创建窗口--------*/
		HWND Create(BaseEx& Parent, int mode, int Location, int X, int Y, int nWidth, int nHeight, BOOL Visual, BOOL Disabled, BindataEx Icon, StringEx Title, BOOL AlwaysTop, BOOL Taskbar, BOOL EscClose, StringEx ClassName, int classstyle, int dwExStyle, int dwStyle, AutoInt Sign);

		/*--------是否已创建--------*/
		BOOL IsWindow();

		/*--------销毁窗口--------*/
		void Destroy();

		/*--------窗口置左边--------*/
		void Left(int Left);

		/*--------窗口取左边--------*/
		int Left();

		/*--------窗口置顶边--------*/
		void Top(int Top);

		/*--------窗口取顶边--------*/
		int Top();

		/*--------窗口置宽度--------*/
		void Width(int Width);

		/*--------窗口取宽度--------*/
		int Width();

		/*--------窗口置高度--------*/
		void Height(int Height);

		/*--------窗口取高度--------*/
		int Height();

		/*--------置矩形--------*/
		void SetRect(int left, int top, int Width, int Height);

		/*--------取矩形--------*/
		void GetRect(int* Left, int* Top, int* Width, int* Height);

		/*--------置位置--------*/
		void Location(int Location);

		/*--------取位置--------*/
		int Location();

		/*--------取可视--------*/
		BOOL Visual();

		/*--------置可视--------*/
		void Visual(BOOL Visual);

		/*--------取禁止--------*/
		BOOL Disabled();

		/*--------置禁止--------*/
		void Disabled(BOOL Disabled);

		/*--------窗口置标识--------*/
		void Sign(AutoInt Sign);

		/*--------窗口取标识--------*/
		AutoInt Sign();

		/*--------置父窗口--------*/
		void SetParent(HWND Parent);

		/*--------取父窗口--------*/
		HWND GetParent();

		/*--------取图标--------*/
		BindataEx Ico();

		/*--------置图标--------*/
		void Ico(BindataEx icon);

		/*--------取标题--------*/
		StringEx Title();

		/*--------置标题--------*/
		void Title(StringEx Title);

		/*--------取风格--------*/
		int GetStyle();

		/*--------置风格--------*/
		void Setstyle(int style);

		/*--------取扩展风格--------*/
		int GetExStyle();

		/*--------置扩展风格--------*/
		void SetExStyle(int ExStyle);

		/*--------置任务栏显示--------*/
		void Taskbar(BOOL show);

		/*--------取任务栏显示--------*/
		BOOL Taskbar();

		/*--------置总在最前--------*/
		void AlwaysTop(BOOL AlwaysTop);

		/*--------取总在最前--------*/
		BOOL AlwaysTop();

		/*--------置退出键关闭--------*/
		void EscClose(BOOL EscClose);

		/*--------取退出键关闭--------*/
		BOOL EscClose();

		/*--------启用文件拖放--------*/
		void EnableDragDrop();

		/*--------关闭文件拖放--------*/
		void CloseDragDrop();

		/*--------置焦点--------*/
		void SetFocus();

		/*--------可有焦点--------*/
		BOOL GetFocus();

		/*--------调整层次--------*/
		void SetLevel(int Level);

		/*--------置托盘图标--------*/
		void SetNotifyIcon(BindataEx Icon, StringEx Tip);

		/*--------发送消息--------*/
		AutoInt SendMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);

		/*--------投递消息--------*/
		AutoInt PostMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);

		/*--------消息循环--------*/
		void MessageLoop();

	};

};
//WindowEx
namespace exui
{
	class WindowEx : public ControlEx
	{
	public:
		WindowEx();
		~WindowEx();

	private:


	public:

		/*--------创建组件_窗口EX--------*/
		WindowEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, BindataEx Icon, StringEx Text, BindataEx Font, int FontClour, int LayeredTransparency, int DragPositionMode, int DragSizeMode, int MaxMode, BindataEx ButtonData, int AnimationParam, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_窗口EX--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_窗口EX--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------插入控制纽_窗口EX--------*/
		AutoInt InsertButton(AutoInt index, AutoInt Data, int Type, BindataEx skin, BOOL Visual, BOOL Disabled, BOOL Selected);

		/*--------删除_窗口EX--------*/
		void DeleteButton(AutoInt index);

		/*--------置类型_窗口EX--------*/
		void SetButtonType(AutoInt index, int Type);

		/*--------取类型_窗口EX--------*/
		int GetButtonType(AutoInt index);

		/*--------置按钮皮肤_窗口EX--------*/
		void SetButtonskin(AutoInt index, BindataEx skin);

		/*--------取按钮皮肤_窗口EX--------*/
		BindataEx GetButtonskin(AutoInt index);

		/*--------置可视_窗口EX--------*/
		void SetButtonVisual(AutoInt index, BOOL Show);

		/*--------取可视_窗口EX--------*/
		BOOL GetButtonVisual(AutoInt index);

		/*--------置禁止_窗口EX--------*/
		void SetButtonDisabled(AutoInt index, BOOL Disabled);

		/*--------取禁止_窗口EX--------*/
		BOOL GetButtonDisabled(AutoInt index);

		/*--------置选中_窗口EX--------*/
		void SetButtonSelected(AutoInt index, BOOL Selected);

		/*--------取选中_窗口EX--------*/
		BOOL GetButtonSelected(AutoInt index);

		/*--------取数量_窗口EX--------*/
		AutoInt GetButtonCount();

		/*--------取数值_窗口EX--------*/
		AutoInt GetButtonData(AutoInt index);

		/*--------置数值_窗口EX--------*/
		void SetButtonData(AutoInt index, AutoInt Data);

		/*--------添加组件_窗口EX--------*/
		BOOL InsertControl(Control_P ControlHand, int left, int top);

		/*--------移除组件_窗口EX--------*/
		BOOL RemoveControl(Control_P ControlHand, Control_P Parentcontrol, int left, int top);

		/*--------调用反馈事件_窗口EX--------*/
		AutoInt CallFeedBackEvent(AutoInt Relevant1, AutoInt Relevant2, BOOL Mode);

		/*--------播放窗口动画--------*/
		BOOL PlayWindowAnimation(AutoInt scale, AutoInt AnimationType, AutoInt AlgorithmMode, AutoInt FrameDelay, AutoInt Step, BOOL FadeInAndOut, BOOL Wait, BOOL DestroyControl);


		/*----------组件窗口EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取图标--------*/
		BindataEx Icon();
		/*--------置图标--------*/
		void Icon(BindataEx Icon);
		/*--------取标题--------*/
		StringEx Title();
		/*--------置标题--------*/
		void Title(StringEx Title);
		/*--------取字体--------*/
		BindataEx Font();
		/*--------置字体--------*/
		void Font(BindataEx Font);
		/*--------取字体色--------*/
		int FontClour();
		/*--------置字体色--------*/
		void FontClour(int FontClour);
		/*--------取分层透明--------*/
		int LayeredTransparency();
		/*--------置分层透明--------*/
		void LayeredTransparency(int LayeredTransparency);
		/*--------取拖动模式--------*/
		int DragPositionMode();
		/*--------置拖动模式--------*/
		void DragPositionMode(int DragPositionMode);
		/*--------取尺寸调整模式--------*/
		int DragSizeMode();
		/*--------置尺寸调整模式--------*/
		void DragSizeMode(int DragSizeMode);
		/*--------取最大化模式--------*/
		int MaxMode();
		/*--------置最大化模式--------*/
		void MaxMode(int MaxMode);
		/*--------取控制钮--------*/
		BindataEx ButtonData();
		/*--------置控制钮--------*/
		void ButtonData(BindataEx ButtonData);
		/*--------取控制钮动画参数--------*/
		int AnimationParam();
		/*--------置控制钮动画参数--------*/
		void AnimationParam(int AnimationParam);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件窗口EX属性读写函数声明结束--------*/
	};
}
//LabelEx
namespace exui
{
	class LabelEx : public ControlEx
	{
	public:
		LabelEx();
		~LabelEx();

	private:


	public:

		/*--------创建组件_标签EX--------*/
		LabelEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, StringEx Text, int Align, int BackColor, BindataEx Font, int FontClour, int Rotate, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_标签Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_标签Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*----------组件标签EX属性读写函数声明开始--------*/


/*--------取标题--------*/
		StringEx Title();
		/*--------置标题--------*/
		void Title(StringEx Title);
		/*--------取对齐方式--------*/
		int Align();
		/*--------置对齐方式--------*/
		void Align(int Align);
		/*--------取背景色--------*/
		int BackColor();
		/*--------置背景色--------*/
		void BackColor(int BackColor);
		/*--------取字体--------*/
		BindataEx Font();
		/*--------置字体--------*/
		void Font(BindataEx Font);
		/*--------取字体色--------*/
		int FontClour();
		/*--------置字体色--------*/
		void FontClour(int FontClour);
		/*--------取旋转角度--------*/
		int Rotate();
		/*--------置旋转角度--------*/
		void Rotate(int Rotate);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件标签EX属性读写函数声明结束--------*/
	};
}
//FilterEx
namespace exui
{
	class FilterEx : public ControlEx
	{
	public:
		FilterEx();
		~FilterEx();

	private:


	public:

		/*--------创建组件_滤镜EX--------*/
		FilterEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, int FilterExMode, int Parameter1, int Parameter2, int Parameter3, int Parameter4, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_滤镜Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_滤镜Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*----------组件滤镜EX属性读写函数声明开始--------*/


/*--------取滤镜类型--------*/
		int FilterExMode();
		/*--------置滤镜类型--------*/
		void FilterExMode(int FilterExMode);
		/*--------取滤镜参数1--------*/
		int Parameter1();
		/*--------置滤镜参数1--------*/
		void Parameter1(int Parameter1);
		/*--------取滤镜参数2--------*/
		int Parameter2();
		/*--------置滤镜参数2--------*/
		void Parameter2(int Parameter2);
		/*--------取滤镜参数3--------*/
		int Parameter3();
		/*--------置滤镜参数3--------*/
		void Parameter3(int Parameter3);
		/*--------取滤镜参数4--------*/
		int Parameter4();
		/*--------置滤镜参数4--------*/
		void Parameter4(int Parameter4);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件滤镜EX属性读写函数声明结束--------*/
	};
}
//ButtonEx
namespace exui
{
	class ButtonEx : public ControlEx
	{
	public:
		ButtonEx();
		~ButtonEx();
	private:


	public:

		/*--------创建组件_按钮EX--------*/
		ButtonEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, StringEx Text, BindataEx Font, int FontClour, int AnimationParam, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_按钮Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_按钮Ex--------*/
		AutoInt GetAttribute(AutoInt index);
		/*----------组件按钮EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取标题--------*/
		StringEx Title();
		/*--------置标题--------*/
		void Title(StringEx Title);
		/*--------取字体--------*/
		BindataEx Font();
		/*--------置字体--------*/
		void Font(BindataEx Font);
		/*--------取字体色--------*/
		int FontClour();
		/*--------置字体色--------*/
		void FontClour(int FontClour);
		/*--------取动画参数--------*/
		int AnimationParam();
		/*--------置动画参数--------*/
		void AnimationParam(int AnimationParam);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件按钮EX属性读写函数声明结束--------*/
	};
}
//ImagebuttonEx
namespace exui
{
	class ImagebuttonEx : public ControlEx
	{
	public:
		ImagebuttonEx();
		~ImagebuttonEx();

	private:


	public:

		/*--------创建组件_图片按钮EX--------*/
		ImagebuttonEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, BOOL Selected, int AnimationParam, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_图片按钮Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_图片按钮Ex--------*/
		AutoInt GetAttribute(AutoInt index);



		/*----------组件图片按钮EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取选中--------*/
		BOOL Selected();
		/*--------置选中--------*/
		void Selected(BOOL Selected);
		/*--------取动画参数--------*/
		int AnimationParam();
		/*--------置动画参数--------*/
		void AnimationParam(int AnimationParam);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件图片按钮EX属性读写函数声明结束--------*/


	};
}
//SuperbuttonEx
namespace exui
{
	class SuperbuttonEx : public ControlEx
	{
	public:
		SuperbuttonEx();
		~SuperbuttonEx();

	private:


	public:

		/*--------创建组件_超级按钮EX--------*/
		SuperbuttonEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, BOOL Selected, BindataEx Icon, int IconWidth, int IconHeight, int  Align, StringEx Text, BindataEx Font, int FontClour, int AnimationParam, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_超级按钮Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_超级按钮Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*----------组件超级按钮EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取选中--------*/
		BOOL Selected();
		/*--------置选中--------*/
		void Selected(BOOL Selected);
		/*--------取图标--------*/
		BindataEx Icon();
		/*--------置图标--------*/
		void Icon(BindataEx Icon);
		/*--------取图标宽--------*/
		int IconWidth();
		/*--------置图标宽--------*/
		void IconWidth(int IconWidth);
		/*--------取图标高--------*/
		int IconHeight();
		/*--------置图标高--------*/
		void IconHeight(int IconHeight);
		/*--------取对齐方式--------*/
		int Align();
		/*--------置对齐方式--------*/
		void Align(int Align);
		/*--------取标题--------*/
		StringEx Title();
		/*--------置标题--------*/
		void Title(StringEx Title);
		/*--------取字体--------*/
		BindataEx Font();
		/*--------置字体--------*/
		void Font(BindataEx Font);
		/*--------取字体色--------*/
		int FontClour();
		/*--------置字体色--------*/
		void FontClour(int FontClour);
		/*--------取动画参数--------*/
		int AnimationParam();
		/*--------置动画参数--------*/
		void AnimationParam(int AnimationParam);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件超级按钮EX属性读写函数声明结束--------*/


	};
}
//ChoiceboxEx
namespace exui
{
	class ChoiceboxEx : public ControlEx
	{
	public:
		ChoiceboxEx();
		~ChoiceboxEx();

	private:


	public:

		/*--------创建组件_选择框EX--------*/
		ChoiceboxEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, BOOL Selected, StringEx Text, BindataEx Font, int FontClour, int AnimationParam, int SelectedMode, int SelectedState, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_选择框Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_选择框Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*----------组件选择框EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取选中--------*/
		BOOL Selected();
		/*--------置选中--------*/
		void Selected(BOOL Selected);
		/*--------取标题--------*/
		StringEx Title();
		/*--------置标题--------*/
		void Title(StringEx Title);
		/*--------取字体--------*/
		BindataEx Font();
		/*--------置字体--------*/
		void Font(BindataEx Font);
		/*--------取字体色--------*/
		int FontClour();
		/*--------置字体色--------*/
		void FontClour(int FontClour);
		/*--------取动画参数--------*/
		int AnimationParam();
		/*--------置动画参数--------*/
		void AnimationParam(int AnimationParam);
		/*--------取选中模式--------*/
		int SelectMode();
		/*--------置选中模式--------*/
		void SelectMode(int SelectMode);
		/*--------取选中状态--------*/
		int SelectState();
		/*--------置选中状态--------*/
		void SelectState(int SelectState);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件选择框EX属性读写函数声明结束--------*/


	};
}
//RadiobuttonEx
namespace exui
{
	class RadiobuttonEx : public ControlEx
	{
	public:
		RadiobuttonEx();
		~RadiobuttonEx();

	private:


	public:

		/*--------创建组件_单选框EX--------*/
		RadiobuttonEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, BOOL Selected, StringEx Text, BindataEx Font, int FontClour, int AnimationParam, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_单选框Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_单选框Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*----------组件单选框EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取选中--------*/
		BOOL Selected();
		/*--------置选中--------*/
		void Selected(BOOL Selected);
		/*--------取标题--------*/
		StringEx Title();
		/*--------置标题--------*/
		void Title(StringEx Title);
		/*--------取字体--------*/
		BindataEx Font();
		/*--------置字体--------*/
		void Font(BindataEx Font);
		/*--------取字体色--------*/
		int FontClour();
		/*--------置字体色--------*/
		void FontClour(int FontClour);
		/*--------取动画参数--------*/
		int AnimationParam();
		/*--------置动画参数--------*/
		void AnimationParam(int AnimationParam);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件单选框EX属性读写函数声明结束--------*/


	};
}
//EditboxEx
namespace exui
{
	class EditboxEx : public ControlEx
	{
	public:
		EditboxEx();
		~EditboxEx();

	private:


	public:

		/*--------创建组件_编辑框EX--------*/
		EditboxEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, StringEx Content, int Align, int InputMode, int MaxInput, StringEx PasswordSubstitution, int CursorColor, BindataEx Font, int FontClour, int SelectedFontColor, int SelectedColor, int LeftReservation, int RightReservation, BOOL Multiline, BOOL Wrapped, int ScrollBarMode, int MenuTableWidth, int MenuTableHeight, BindataEx MenuFont, int MenuFontClour, int MenuDisabledFontClour, int MenuTransparency, int MenuLanguage, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_编辑框Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_编辑框Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------插入文本_编辑框Ex--------*/
		void InsertText(AutoInt weizhi, StringEx text);

		/*--------删除文本_编辑框Ex--------*/
		void DeleteText(AutoInt weizhi, AutoInt dellen);

		/*--------置光标位置_编辑框Ex--------*/
		void SetInsertCursor(AutoInt index);

		/*--------取光标位置_编辑框Ex--------*/
		AutoInt GetInsertCursor();

		/*--------置选择文本长度_编辑框Ex--------*/
		void SetSelectLeng(AutoInt SelectLeng);

		/*--------取选择文本长度_编辑框Ex--------*/
		AutoInt GetSelectLeng();

		/*--------保证显示字符_编辑框Ex--------*/
		void GuaranteeVisibleText(AutoInt index, int mode);

		/*----------组件编辑框EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取内容--------*/
		StringEx Content();
		/*--------置内容--------*/
		void Content(StringEx Content);
		/*--------取对齐方式--------*/
		int Align();
		/*--------置对齐方式--------*/
		void Align(int Align);
		/*--------取输入方式--------*/
		int InputMode();
		/*--------置输入方式--------*/
		void InputMode(int InputMode);
		/*--------取最大容许长度--------*/
		int MaxInput();
		/*--------置最大容许长度--------*/
		void MaxInput(int MaxInput);
		/*--------取密码替换符--------*/
		StringEx PasswordSubstitution();
		/*--------置密码替换符--------*/
		void PasswordSubstitution(StringEx PasswordSubstitution);
		/*--------取光标色--------*/
		int CursorColor();
		/*--------置光标色--------*/
		void CursorColor(int CursorColor);
		/*--------取字体--------*/
		BindataEx Font();
		/*--------置字体--------*/
		void Font(BindataEx Font);
		/*--------取字体色--------*/
		int FontClour();
		/*--------置字体色--------*/
		void FontClour(int FontClour);
		/*--------取选中字体色--------*/
		int SelectedFontColor();
		/*--------置选中字体色--------*/
		void SelectedFontColor(int SelectedFontColor);
		/*--------取选中背景色--------*/
		int SelectedColor();
		/*--------置选中背景色--------*/
		void SelectedColor(int SelectedColor);
		/*--------取左预留--------*/
		int LeftReservation();
		/*--------置左预留--------*/
		void LeftReservation(int LeftReservation);
		/*--------取右预留--------*/
		int RightReservation();
		/*--------置右预留--------*/
		void RightReservation(int RightReservation);
		/*--------取是否容许多行--------*/
		BOOL Multiline();
		/*--------置是否容许多行--------*/
		void Multiline(BOOL Multiline);
		/*--------取自动换行--------*/
		BOOL Wrapped();
		/*--------置自动换行--------*/
		void Wrapped(BOOL Wrapped);
		/*--------取滚动条方式--------*/
		int ScrollBarMode();
		/*--------置滚动条方式--------*/
		void ScrollBarMode(int ScrollBarMode);
		/*--------取菜单项目宽--------*/
		int MenuTableWidth();
		/*--------置菜单项目宽--------*/
		void MenuTableWidth(int MenuTableWidth);
		/*--------取菜单项目高--------*/
		int MenuTableHeight();
		/*--------置菜单项目高--------*/
		void MenuTableHeight(int MenuTableHeight);
		/*--------取菜单字体--------*/
		BindataEx MenuFont();
		/*--------置菜单字体--------*/
		void MenuFont(BindataEx MenuFont);
		/*--------取菜单字体色--------*/
		int MenuFontClour();
		/*--------置菜单字体色--------*/
		void MenuFontClour(int MenuFontClour);
		/*--------取菜单禁止字体色--------*/
		int MenuDisabledFontClour();
		/*--------置菜单禁止字体色--------*/
		void MenuDisabledFontClour(int MenuDisabledFontClour);
		/*--------取菜单透明度--------*/
		int MenuTransparency();
		/*--------置菜单透明度--------*/
		void MenuTransparency(int MenuTransparency);
		/*--------取菜单语言--------*/
		int MenuLanguage();
		/*--------置菜单语言--------*/
		void MenuLanguage(int MenuLanguage);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件编辑框EX属性读写函数声明结束--------*/


	};
}
//ComboboxEx
namespace exui
{
	class ComboboxEx : public ControlEx
	{
	public:
		ComboboxEx();
		~ComboboxEx();

	private:


	public:

		/*--------创建组件_组合框EX--------*/
		ComboboxEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, StringEx Content, int Align, int InputMode, int MaxInput, StringEx PasswordSubstitution, int CursorColor, BindataEx Font, int FontClour, int SelectedFontColor, int SelectedColor, int LeftReservation, int RightReservation, int MenuTableWidth, int MenuTableHeight, BindataEx MenuFont, int MenuFontClour, int MenuDisabledFontClour, int MenuTransparency, int MenuLanguage, int DownListWidth, int DownListMaxHeight, AutoInt DownListCurrentSelection, int DownListIconWidth, int DownListIconHeight, BindataEx DownListFont, BOOL DownListAlternate, int DownListScrollBarMode, int DownListAttributeTransparency, BindataEx DownListItemData, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_组合框Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_组合框Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------插入文本_组合框Ex--------*/
		void InsertText(AutoInt weizhi, StringEx text);

		/*--------删除文本_组合框Ex--------*/
		void DeleteText(AutoInt weizhi, AutoInt dellen);

		/*--------置光标位置_组合框Ex--------*/
		void SetInsertCursor(AutoInt index);

		/*--------取光标位置_组合框Ex--------*/
		int GetInsertCursor();

		/*--------置选择文本长度_组合框Ex--------*/
		void SetSelectLeng(AutoInt SelectLeng);

		/*--------取选择文本长度_组合框Ex--------*/
		int GetSelectLeng();

		/*--------保证显示字符_组合框Ex--------*/
		void GuaranteeVisibleText(AutoInt index, int mode);

		/*--------弹出_组合框Ex--------*/
		void PopUp_DownlistEx();

		/*--------关闭_组合框Ex--------*/
		void Close_DownlistEx();

		/*--------是否已弹出_组合框Ex--------*/
		BOOL IsPopUp_DownlistEx();

		/*--------插入_组合框Ex--------*/
		AutoInt InsertItem_DownlistEx(AutoInt index, AutoInt addnum, AutoInt Data, BitmapEx Ico, StringEx Title, int FontColor, int Size);

		/*--------删除_组合框Ex--------*/
		void DeleteItem_DownlistEx(AutoInt index, int deletenum);

		/*--------取数量_组合框Ex--------*/
		AutoInt GetItemCount_DownlistEx();

		/*--------置附加值_组合框Ex--------*/
		void SetItemData_DownlistEx(AutoInt index, AutoInt Data);

		/*--------取附加值_组合框Ex--------*/
		AutoInt GetItemData_DownlistEx(AutoInt index);

		/*--------置图标_组合框Ex--------*/
		void SetItemIco_DownlistEx(AutoInt index, BitmapEx Ico);

		/*--------取图标_组合框Ex--------*/
		BitmapEx GetItemIco_DownlistEx(AutoInt index);

		/*--------置标题_组合框Ex--------*/
		void SetItemTitle_DownlistEx(AutoInt index, StringEx Title);

		/*--------取标题_组合框Ex--------*/
		StringEx GetItemTitle_DownlistEx(AutoInt index);

		/*--------置字体色_组合框Ex--------*/
		void SetItemFontColor_DownlistEx(AutoInt index, int FontColor);

		/*--------取字体色_组合框Ex--------*/
		int GetItemFontColor_DownlistEx(AutoInt index);

		/*--------置项目高度_组合框Ex--------*/
		void SetItemSize_DownlistEx(AutoInt index, int Size);

		/*--------取项目高度_组合框Ex--------*/
		int GetItemSize_DownlistEx(AutoInt index);

		/*--------保证显示_组合框Ex--------*/
		void GuaranteeVisible_DownlistEx(AutoInt index, int mode);

		/*----------组件组合框EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取内容--------*/
		StringEx Content();
		/*--------置内容--------*/
		void Content(StringEx Content);
		/*--------取对齐方式--------*/
		int Align();
		/*--------置对齐方式--------*/
		void Align(int Align);
		/*--------取输入方式--------*/
		int InputMode();
		/*--------置输入方式--------*/
		void InputMode(int InputMode);
		/*--------取最大容许长度--------*/
		int MaxInput();
		/*--------置最大容许长度--------*/
		void MaxInput(int MaxInput);
		/*--------取密码替换符--------*/
		StringEx PasswordSubstitution();
		/*--------置密码替换符--------*/
		void PasswordSubstitution(StringEx PasswordSubstitution);
		/*--------取光标色--------*/
		int CursorColor();
		/*--------置光标色--------*/
		void CursorColor(int CursorColor);
		/*--------取字体--------*/
		BindataEx Font();
		/*--------置字体--------*/
		void Font(BindataEx Font);
		/*--------取字体色--------*/
		int FontClour();
		/*--------置字体色--------*/
		void FontClour(int FontClour);
		/*--------取选中字体色--------*/
		int SelectedFontColor();
		/*--------置选中字体色--------*/
		void SelectedFontColor(int SelectedFontColor);
		/*--------取选中背景色--------*/
		int SelectedColor();
		/*--------置选中背景色--------*/
		void SelectedColor(int SelectedColor);
		/*--------取左预留--------*/
		int LeftReservation();
		/*--------置左预留--------*/
		void LeftReservation(int LeftReservation);
		/*--------取右预留--------*/
		int RightReservation();
		/*--------置右预留--------*/
		void RightReservation(int RightReservation);
		/*--------取菜单项目宽--------*/
		int MenuTableWidth();
		/*--------置菜单项目宽--------*/
		void MenuTableWidth(int MenuTableWidth);
		/*--------取菜单项目高--------*/
		int MenuTableHeight();
		/*--------置菜单项目高--------*/
		void MenuTableHeight(int MenuTableHeight);
		/*--------取菜单字体--------*/
		BindataEx MenuFont();
		/*--------置菜单字体--------*/
		void MenuFont(BindataEx MenuFont);
		/*--------取菜单字体色--------*/
		int MenuFontClour();
		/*--------置菜单字体色--------*/
		void MenuFontClour(int MenuFontClour);
		/*--------取菜单禁止字体色--------*/
		int MenuDisabledFontClour();
		/*--------置菜单禁止字体色--------*/
		void MenuDisabledFontClour(int MenuDisabledFontClour);
		/*--------取菜单透明度--------*/
		int MenuTransparency();
		/*--------置菜单透明度--------*/
		void MenuTransparency(int MenuTransparency);
		/*--------取菜单语言--------*/
		int MenuLanguage();
		/*--------置菜单语言--------*/
		void MenuLanguage(int MenuLanguage);
		/*--------取列表宽度--------*/
		int DownListWidth();
		/*--------置列表宽度--------*/
		void DownListWidth(int DownListWidth);
		/*--------取列表最大高--------*/
		int DownListMaxHeight();
		/*--------置列表最大高--------*/
		void DownListMaxHeight(int DownListMaxHeight);
		/*--------取列表现行选中项--------*/
		int DownListCurrentItem();
		/*--------置列表现行选中项--------*/
		void DownListCurrentItem(int DownListCurrentItem);
		/*--------取列表图标宽--------*/
		int DownListIconWidth();
		/*--------置列表图标宽--------*/
		void DownListIconWidth(int DownListIconWidth);
		/*--------取列表图标高--------*/
		int DownListIconHeight();
		/*--------置列表图标高--------*/
		void DownListIconHeight(int DownListIconHeight);
		/*--------取列表字体--------*/
		BindataEx DownListFont();
		/*--------置列表字体--------*/
		void DownListFont(BindataEx DownListFont);
		/*--------取列表隔行换色--------*/
		BOOL DownListAlternate();
		/*--------置列表隔行换色--------*/
		void DownListAlternate(BOOL DownListAlternate);
		/*--------取列表滚动条方式--------*/
		int DownListScrollBarMode();
		/*--------置列表滚动条方式--------*/
		void DownListScrollBarMode(int DownListScrollBarMode);
		/*--------取列表透明度--------*/
		int DownListTransparency();
		/*--------置列表透明度--------*/
		void DownListTransparency(int DownListTransparency);
		/*--------取列表项目数据--------*/
		BindataEx DownListItemData();
		/*--------置列表项目数据--------*/
		void DownListItemData(BindataEx DownListItemData);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件组合框EX属性读写函数声明结束--------*/


	};
}
//MinutesboxEx
namespace exui
{
	class MinutesboxEx : public ControlEx
	{
	public:
		MinutesboxEx();
		~MinutesboxEx();

	private:


	public:

		/*--------创建组件_分组框EX--------*/
		MinutesboxEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, BindataEx Picture, int IconWidth, int IconHeight, StringEx Text, int TextMode, BindataEx Font, int FontClour, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_分组框Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_分组框Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------添加组件_分组框Ex--------*/
		BOOL InsertControl(Control_P ControlHand, int left, int top);

		/*--------移除组件_分组框Ex--------*/
		BOOL RemoveControl(Control_P ControlHand, Control_P Parentcontrol, int left, int top);

		/*----------组件分组框EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取图标--------*/
		BindataEx Picture();
		/*--------置图标--------*/
		void Picture(BindataEx Picture);
		/*--------取图标宽--------*/
		int IconWidth();
		/*--------置图标宽--------*/
		void IconWidth(int IconWidth);
		/*--------取图标高--------*/
		int IconHeight();
		/*--------置图标高--------*/
		void IconHeight(int IconHeight);
		/*--------取标题--------*/
		StringEx Title();
		/*--------置标题--------*/
		void Title(StringEx Title);
		/*--------取风格--------*/
		int Style();
		/*--------置风格--------*/
		void Style(int Style);
		/*--------取字体--------*/
		BindataEx Font();
		/*--------置字体--------*/
		void Font(BindataEx Font);
		/*--------取字体色--------*/
		int FontClour();
		/*--------置字体色--------*/
		void FontClour(int FontClour);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件分组框EX属性读写函数声明结束--------*/


	};
}
//MultifunctionButtonEx
namespace exui
{
	class MultifunctionButtonEx : public ControlEx
	{
	public:
		MultifunctionButtonEx();
		~MultifunctionButtonEx();

	private:


	public:

		/*--------创建组件_多功能按钮EX--------*/
		MultifunctionButtonEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, int ButtonStyles, int PartnerSize, int BackColor, BindataEx Icon, int IconWidth, int IconHeight, int Align, StringEx Text, BindataEx Font, int FontClour, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_多功能按钮EX--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_多功能按钮EX--------*/
		AutoInt GetAttribute(AutoInt index);

		/*----------组件多功能按钮EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取按钮样式--------*/
		int ButtonStyles();
		/*--------置按钮样式--------*/
		void ButtonStyles(int ButtonStyles);
		/*--------取伙伴钮尺寸--------*/
		int PartnerSize();
		/*--------置伙伴钮尺寸--------*/
		void PartnerSize(int PartnerSize);
		/*--------取背景颜色--------*/
		int BackColor();
		/*--------置背景颜色--------*/
		void BackColor(int BackColor);
		/*--------取图标--------*/
		BindataEx Icon();
		/*--------置图标--------*/
		void Icon(BindataEx Icon);
		/*--------取图标宽--------*/
		int IconWidth();
		/*--------置图标宽--------*/
		void IconWidth(int IconWidth);
		/*--------取图标高--------*/
		int IconHeight();
		/*--------置图标高--------*/
		void IconHeight(int IconHeight);
		/*--------取对齐方式--------*/
		int Align();
		/*--------置对齐方式--------*/
		void Align(int Align);
		/*--------取标题--------*/
		StringEx Title();
		/*--------置标题--------*/
		void Title(StringEx Title);
		/*--------取字体--------*/
		BindataEx Font();
		/*--------置字体--------*/
		void Font(BindataEx Font);
		/*--------取字体色--------*/
		int FontClour();
		/*--------置字体色--------*/
		void FontClour(int FontClour);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件多功能按钮EX属性读写函数声明结束--------*/


	};
}
//PictureBoxEx
namespace exui
{
	class PictureBoxEx : public ControlEx
	{
	public:
		PictureBoxEx();
		~PictureBoxEx();

	private:


	public:

		/*--------创建组件_图片框EX--------*/
		PictureBoxEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, BindataEx Picture, int MapMode, int Angle, BOOL PlayAnimation, int AllFrame, int CurrentFrame, int Rotate, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_图片框Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_图片框Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*----------组件图片框EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取图片--------*/
		BindataEx Picture();
		/*--------置图片--------*/
		void Picture(BindataEx Picture);
		/*--------取底图方式--------*/
		int MapMode();
		/*--------置底图方式--------*/
		void MapMode(int MapMode);
		/*--------取圆角度--------*/
		int Angle();
		/*--------置圆角度--------*/
		void Angle(int Angle);
		/*--------取播放动画--------*/
		BOOL PlayAnimation();
		/*--------置播放动画--------*/
		void PlayAnimation(BOOL PlayAnimation);
		/*--------取总帧数--------*/
		int AllFrame();
		/*--------置总帧数--------*/
		void AllFrame(int AllFrame);
		/*--------取当前帧--------*/
		int CurrentFrame();
		/*--------置当前帧--------*/
		void CurrentFrame(int CurrentFrame);
		/*--------取旋转角度--------*/
		int Rotate();
		/*--------置旋转角度--------*/
		void Rotate(int Rotate);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件图片框EX属性读写函数声明结束--------*/


	};
}
//ProgressbarEx
namespace exui
{
	class ProgressbarEx : public ControlEx
	{
	public:
		ProgressbarEx();
		~ProgressbarEx();

	private:


	public:

		/*--------创建组件_进度条EX--------*/
		ProgressbarEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, int Position, int MiniPosition, int MaxiPosition, int Style, int Direction, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_进度条Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_进度条Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*----------组件进度条EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取位置--------*/
		int Position();
		/*--------置位置--------*/
		void Position(int Position);
		/*--------取最小位置--------*/
		int MiniPosition();
		/*--------置最小位置--------*/
		void MiniPosition(int MiniPosition);
		/*--------取最大位置--------*/
		int MaxiPosition();
		/*--------置最大位置--------*/
		void MaxiPosition(int MaxiPosition);
		/*--------取进度样式--------*/
		int Style();
		/*--------置进度样式--------*/
		void Style(int Style);
		/*--------取进度方向--------*/
		int Direction();
		/*--------置进度方向--------*/
		void Direction(int Direction);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件进度条EX属性读写函数声明结束--------*/


	};
}
//ScrollbarEx
namespace exui
{
	class ScrollbarEx : public ControlEx
	{
	public:
		ScrollbarEx();
		~ScrollbarEx();

	private:


	public:

		/*--------创建组件_滚动条EX--------*/
		ScrollbarEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, int Position, int MiniPosition, int MaxiPosition, int PageLength, int RowChangeValue, int PageChangeValue, int ScrollChangeValue, BOOL DragTrace, BOOL schedule, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_滚动条Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_滚动条Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*----------组件滚动条EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取位置--------*/
		int Position();
		/*--------置位置--------*/
		void Position(int Position);
		/*--------取最小位置--------*/
		int MiniPosition();
		/*--------置最小位置--------*/
		void MiniPosition(int MiniPosition);
		/*--------取最大位置--------*/
		int MaxiPosition();
		/*--------置最大位置--------*/
		void MaxiPosition(int MaxiPosition);
		/*--------取页长--------*/
		int PageLength();
		/*--------置页长--------*/
		void PageLength(int PageLength);
		/*--------取行改变值--------*/
		int RowChangeValue();
		/*--------置行改变值--------*/
		void RowChangeValue(int RowChangeValue);
		/*--------取页改变值--------*/
		int PageChangeValue();
		/*--------置页改变值--------*/
		void PageChangeValue(int PageChangeValue);
		/*--------取滚动改变值--------*/
		int ScrollChangeValue();
		/*--------置滚动改变值--------*/
		void ScrollChangeValue(int ScrollChangeValue);
		/*--------取容许拖动跟踪--------*/
		BOOL DragTrace();
		/*--------置容许拖动跟踪--------*/
		void DragTrace(BOOL DragTrace);
		/*--------取纵向--------*/
		BOOL schedule();
		/*--------置纵向--------*/
		void schedule(BOOL schedule);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件滚动条EX属性读写函数声明结束--------*/


	};
}
//SliderbarEx
namespace exui
{
	class SliderbarEx : public ControlEx
	{
	public:
		SliderbarEx();
		~SliderbarEx();

	private:


	public:

		/*--------创建组件_滑块条EX--------*/
		SliderbarEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, int Position, int Progress, int MiniPosition, int MaxiPosition, int RowChangeValue, int PageChangeValue, BOOL DragTrace, int Style, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_滑块条Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_滑块条Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*----------组件滑块条EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取位置--------*/
		int Position();
		/*--------置位置--------*/
		void Position(int Position);
		/*--------取进度位置--------*/
		int Progress();
		/*--------置进度位置--------*/
		void Progress(int Progress);
		/*--------取最小位置--------*/
		int MiniPosition();
		/*--------置最小位置--------*/
		void MiniPosition(int MiniPosition);
		/*--------取最大位置--------*/
		int MaxiPosition();
		/*--------置最大位置--------*/
		void MaxiPosition(int MaxiPosition);
		/*--------取滚动改变值--------*/
		int ScrollChangeValue();
		/*--------置滚动改变值--------*/
		void ScrollChangeValue(int ScrollChangeValue);
		/*--------取页改变值--------*/
		int PageChangeValue();
		/*--------置页改变值--------*/
		void PageChangeValue(int PageChangeValue);
		/*--------取容许拖动跟踪--------*/
		BOOL DragTrace();
		/*--------置容许拖动跟踪--------*/
		void DragTrace(BOOL DragTrace);
		/*--------取样式--------*/
		int Style();
		/*--------置样式--------*/
		void Style(int Style);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件滑块条EX属性读写函数声明结束--------*/


	};
}
//SelectthefolderEx
namespace exui
{
	class SelectthefolderEx : public ControlEx
	{
	public:
		SelectthefolderEx();
		~SelectthefolderEx();

	private:


	public:

		/*--------创建组件_选择夹EX--------*/
		SelectthefolderEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, AutoInt CurrentTable, int TableSize, int Direction, int Spacing, int TableStyle, int Retain, int IconWidth, int IconHeight, BindataEx Font, BindataEx TableData, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_选择夹Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_选择夹Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------插入_选择夹Ex--------*/
		AutoInt InsertTab(AutoInt index, AutoInt Data, BitmapEx Ico, StringEx Title, int FontColor, int Size);

		/*--------删除_选择夹Ex--------*/
		void DeleteTab(AutoInt ctrlindex);

		/*--------取数量_选择夹Ex--------*/
		AutoInt GetTabCount();

		/*--------取关联数值_选择夹Ex--------*/
		AutoInt GetTabData(AutoInt index);

		/*--------置关联数值_选择夹Ex--------*/
		void SetTabData(AutoInt index, AutoInt Data);

		/*--------置图标_选择夹Ex--------*/
		void SetTabIco(AutoInt index, BitmapEx Ico);

		/*--------取图标_选择夹Ex--------*/
		BitmapEx GetTabIco(AutoInt index);

		/*--------置文本_选择夹Ex--------*/
		void SetTabTitle(AutoInt index, StringEx Title);

		/*--------取文本_选择夹Ex--------*/
		StringEx GetTabTitle(AutoInt index);

		/*--------置字体色_选择夹Ex--------*/
		void SetTabFontColor(AutoInt index, int FontColor);

		/*--------取字体色_选择夹Ex--------*/
		int GetTabFontColor(AutoInt index);

		/*--------置尺寸_选择夹Ex--------*/
		void SetTabSize(AutoInt index, int Size);

		/*--------取尺寸_选择夹Ex--------*/
		int GetTabSize(AutoInt index);

		/*--------保证显示_选择夹Ex--------*/
		void GuaranteeVisible(AutoInt index, int DisplayMode);

		/*--------添加组件_选择夹Ex--------*/
		BOOL InsertControl(AutoInt Table, Control_P ControlHand, int left, int top);

		/*--------移除组件_选择夹Ex--------*/
		BOOL RemoveControl(AutoInt Table, Control_P ControlHand, Control_P Parentcontrol, int left, int top);

		/*----------组件选择夹EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取现行子夹--------*/
		int CurrentTable();
		/*--------置现行子夹--------*/
		void CurrentTable(int CurrentTable);
		/*--------取子夹头尺寸--------*/
		int TableSize();
		/*--------置子夹头尺寸--------*/
		void TableSize(int TableSize);
		/*--------取子夹头方向--------*/
		int Direction();
		/*--------置子夹头方向--------*/
		void Direction(int Direction);
		/*--------取间距--------*/
		int Spacing();
		/*--------置间距--------*/
		void Spacing(int Spacing);
		/*--------取子夹头样式--------*/
		int TableStyle();
		/*--------置子夹头样式--------*/
		void TableStyle(int TableStyle);
		/*--------取保留属性--------*/
		int Reserve();
		/*--------置保留属性--------*/
		void Reserve(int Reserve);
		/*--------取图标宽--------*/
		int IconWidth();
		/*--------置图标宽--------*/
		void IconWidth(int IconWidth);
		/*--------取图标高--------*/
		int IconHeight();
		/*--------置图标高--------*/
		void IconHeight(int IconHeight);
		/*--------取字体--------*/
		BindataEx Font();
		/*--------置字体--------*/
		void Font(BindataEx Font);
		/*--------取子夹管理--------*/
		BindataEx TableData();
		/*--------置子夹管理--------*/
		void TableData(BindataEx TableData);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件选择夹EX属性读写函数声明结束--------*/


	};
}
//ToolbarEx
namespace exui
{
	class ToolbarEx : public ControlEx
	{
	public:
		ToolbarEx();
		~ToolbarEx();

	private:


	public:

		/*--------创建组件_工具条EX--------*/
		ToolbarEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, int BackMode, BOOL schedule, int Spacing, int IconWidth, int IconHeight, BindataEx Font, BindataEx ButtonData, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_工具条Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_工具条Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------插入_工具条Ex--------*/
		AutoInt InsertButton(AutoInt index, AutoInt Data, BitmapEx Ico, StringEx Title, int FontColor, int Size, int Type, int Align, BOOL Disabled, BOOL Selected);

		/*--------删除_工具条Ex--------*/
		void DeleteButton(AutoInt index);

		/*--------取数量_工具条Ex--------*/
		AutoInt GetButtonCount();

		/*--------置关联数值_工具条Ex--------*/
		void SetButtonData(AutoInt index, AutoInt Data);

		/*--------取关联数值_工具条Ex--------*/
		AutoInt GetButtonData(AutoInt index);

		/*--------置图标_工具条Ex--------*/
		void SetButtonIco(AutoInt index, BitmapEx Ico);

		/*--------取图标_工具条Ex--------*/
		BitmapEx GetButtonIco(AutoInt index);

		/*--------置文本_工具条Ex--------*/
		void SetButtonTitle(AutoInt index, StringEx Title);

		/*--------取文本_工具条Ex--------*/
		StringEx GetButtonTitle(AutoInt index);

		/*--------置字体色_工具条Ex--------*/
		void SetButtonFontColor(AutoInt index, int FontColor);

		/*--------取字体色_工具条Ex--------*/
		int GetButtonFontColor(AutoInt index);

		/*--------置尺寸_工具条Ex--------*/
		void SetButtonSize(AutoInt index, int Size);

		/*--------取尺寸_工具条Ex--------*/
		int GetButtonSize(AutoInt index);

		/*--------置类型_工具条Ex--------*/
		void SetButtonType(AutoInt index, int Type);

		/*--------取类型_工具条Ex--------*/
		int GetButtonType(AutoInt index);

		/*--------置对齐方式_工具条Ex--------*/
		void SetButtonAlign(AutoInt index, int Align);

		/*--------取对齐方式_工具条Ex--------*/
		int GetButtonAlign(AutoInt index);

		/*--------置选中_工具条Ex--------*/
		void SetButtonSelected(AutoInt index, BOOL Selected);

		/*--------取选中_工具条Ex--------*/
		BOOL GetButtonSelected(AutoInt index);

		/*--------置禁止_工具条Ex--------*/
		void SetButtonDisabled(AutoInt index, BOOL Disabled);

		/*--------取禁止_工具条Ex--------*/
		BOOL GetButtonDisabled(AutoInt index);

		/*--------置字体_工具条Ex--------*/
		void SetButtonFont(AutoInt index, BindataEx Font);

		/*--------取字体_工具条Ex--------*/
		BindataEx GetButtonFont(AutoInt index);

		/*--------置皮肤_工具条Ex--------*/
		void SetButtonSkin(AutoInt index, BindataEx Skin);

		/*--------取皮肤_工具条Ex--------*/
		BindataEx GetButtonSkin(AutoInt index);

		/*--------保证显示_工具条Ex--------*/
		void GuaranteeVisible(AutoInt index, int DisplayMode);

		/*----------组件工具条EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取背景样式--------*/
		int BackMode();
		/*--------置背景样式--------*/
		void BackMode(int BackMode);
		/*--------取纵向--------*/
		BOOL schedule();
		/*--------置纵向--------*/
		void schedule(BOOL schedule);
		/*--------取间距--------*/
		int Spacing();
		/*--------置间距--------*/
		void Spacing(int Spacing);
		/*--------取图标宽--------*/
		int IconWidth();
		/*--------置图标宽--------*/
		void IconWidth(int IconWidth);
		/*--------取图标高--------*/
		int IconHeight();
		/*--------置图标高--------*/
		void IconHeight(int IconHeight);
		/*--------取字体--------*/
		BindataEx Font();
		/*--------置字体--------*/
		void Font(BindataEx Font);
		/*--------取按钮管理--------*/
		BindataEx ButtonData();
		/*--------置按钮管理--------*/
		void ButtonData(BindataEx ButtonData);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件工具条EX属性读写函数声明结束--------*/


	};
}
//ListboxEx
namespace exui
{
	class ListboxEx : public ControlEx
	{
	public:
		ListboxEx();
		~ListboxEx();

	private:


	public:

		/*--------创建组件_列表框EX--------*/
		ListboxEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, AutoInt CurrentItem, int IconWidth, int IconHeight, BindataEx Font, BOOL AlternateColor, int SelectMode, int ScrollBarMode, BindataEx ItemData, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_列表框Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_列表框Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------插入_列表框Ex--------*/
		AutoInt InsertItem(AutoInt index, AutoInt addnum, AutoInt Data, BitmapEx Ico, StringEx Title, int FontColor, int Size);

		/*--------删除_列表框Ex--------*/
		void DeleteItem(AutoInt index, AutoInt deletenum);

		/*--------取数量_列表框Ex--------*/
		AutoInt GetItemCount();

		/*--------置附加值_列表框Ex--------*/
		void SetItemData(AutoInt index, AutoInt Data);

		/*--------取附加值_列表框Ex--------*/
		AutoInt GetItemData(AutoInt index);

		/*--------置图标_列表框Ex--------*/
		void SetItemIco(AutoInt index, BitmapEx Ico);

		/*--------取图标_列表框Ex--------*/
		BitmapEx GetItemIco(AutoInt index);

		/*--------置标题_列表框Ex--------*/
		void SetItemTitle(AutoInt index, StringEx Title);

		/*--------取标题_列表框Ex--------*/
		StringEx GetItemTitle(AutoInt index);

		/*--------取字体色_列表框Ex--------*/
		int GetItemFontColor(AutoInt index);

		/*--------置字体色_列表框Ex--------*/
		void SetItemFontColor(AutoInt index, int FontColor);

		/*--------置项目高度_列表框Ex--------*/
		void SetItemSize(AutoInt index, int Size);

		/*--------取项目高度_列表框Ex--------*/
		int GetItemSize(AutoInt index);

		/*--------置字体_列表框Ex--------*/
		void SetItemFont(AutoInt index, BindataEx Font);

		/*--------取字体_列表框Ex--------*/
		BindataEx GetItemFont(AutoInt index);

		/*--------置皮肤_列表框Ex--------*/
		void SetItemSkin(AutoInt index, BindataEx Skin);

		/*--------取皮肤_列表框Ex--------*/
		BindataEx GetItemSkin(AutoInt index);

		/*--------置选中_列表框Ex--------*/
		void SetItemSelected(AutoInt index, BOOL Selected);

		/*--------取选中_列表框Ex--------*/
		BOOL GetItemSelected(AutoInt index);

		/*--------置滚动回调_列表框Ex--------*/
		void SetScrollCallBack(ExuiCallback Callback);

		/*--------取滚动回调_列表框Ex--------*/
		ExuiCallback GetScrollCallBack();

		/*--------置虚表回调_列表框Ex--------*/
		void SetVirtualTableCallBack(ExuiCallback Callback);

		/*--------取虚表回调_列表框Ex--------*/
		ExuiCallback GetVirtualTableCallBack();

		/*--------保证显示_列表框Ex--------*/
		void GuaranteeVisible(AutoInt index, int mode);

		/*----------组件列表框EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取现行选中项--------*/
		int CurrentItem();
		/*--------置现行选中项--------*/
		void CurrentItem(int CurrentItem);
		/*--------取图标宽--------*/
		int IconWidth();
		/*--------置图标宽--------*/
		void IconWidth(int IconWidth);
		/*--------取图标高--------*/
		int IconHeight();
		/*--------置图标高--------*/
		void IconHeight(int IconHeight);
		/*--------取字体--------*/
		BindataEx Font();
		/*--------置字体--------*/
		void Font(BindataEx Font);
		/*--------取隔行换色--------*/
		BOOL AlternateColor();
		/*--------置隔行换色--------*/
		void AlternateColor(BOOL AlternateColor);
		/*--------取选中模式--------*/
		int SelectMode();
		/*--------置选中模式--------*/
		void SelectMode(int SelectMode);
		/*--------取滚动条方式--------*/
		int ScrollBarMode();
		/*--------置滚动条方式--------*/
		void ScrollBarMode(int ScrollBarMode);
		/*--------取项目数据--------*/
		BindataEx ItemData();
		/*--------置项目数据--------*/
		void ItemData(BindataEx ItemData);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件列表框EX属性读写函数声明结束--------*/


	};
}
//SuperListboxEx
namespace exui
{
	class SuperListboxEx : public ControlEx
	{
	public:
		SuperListboxEx();
		~SuperListboxEx();

	private:


	public:

		/*--------创建组件_超级列表框EX--------*/
		SuperListboxEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, AutoInt CurrentItem, AutoInt CurrentColumn, int HeadHeight, int HeadMode, BOOL AlternateColor, int SelectMode, BOOL EntireLine, int EventMode, int EditMode, int LineMode, int ScrollBarMode, BindataEx ItemData, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_超级列表框Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_超级列表框Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------插入列_超级列表框Ex--------*/
		AutoInt InsertColumn(AutoInt cloid, AutoInt addnum, BitmapEx Ico, StringEx Title, int FontColor, int wight, int min, int max, BindataEx Font, int IcoW, int IcoH, int Align, BindataEx ItemFont, int ItemIcoW, int ItemIcoH, int ItemAlign);

		/*--------删除列_超级列表框Ex--------*/
		void DeleteColumn(AutoInt delindex, AutoInt deletenum);

		/*--------取列数量_超级列表框Ex--------*/
		AutoInt GetColumnCount();

		/*--------置列属性_超级列表框Ex--------*/
		void SetColumnAttribute(AutoInt index, AutoInt AttributeId, BitmapEx Ico, StringEx Title, int FontColor, int wight, int min, int max, BindataEx Font, int IcoW, int IcoH, int Align, BindataEx ItemFont, int ItemIcoW, int ItemIcoH, int ItemAlign);

		/*--------取列属性_超级列表框Ex--------*/
		AutoInt GetColumnAttribute(AutoInt index, AutoInt AttributeId);

		/*--------插入项目_超级列表框Ex--------*/
		AutoInt InsertItem(AutoInt index, AutoInt addnum, AutoInt Data, int Size);

		/*--------删除项目_超级列表框Ex--------*/
		void DeleteItem(AutoInt delindex, AutoInt deletenum);

		/*--------取数量_超级列表框Ex--------*/
		AutoInt GetItemCount();

		/*--------置附加值_超级列表框Ex--------*/
		void SetItemData(AutoInt index, AutoInt Data);

		/*--------取附加值_超级列表框Ex--------*/
		AutoInt GetItemData(AutoInt index);

		/*--------置图标_超级列表框Ex--------*/
		void SetItemIco(AutoInt index, AutoInt cloid, BitmapEx Ico);

		/*--------取图标_超级列表框Ex--------*/
		BitmapEx GetItemIco(AutoInt index, AutoInt cloid);

		/*--------置文本_超级列表框Ex--------*/
		void SetItemTitle(AutoInt index, AutoInt cloid, StringEx Title);

		/*--------取文本_超级列表框Ex--------*/
		StringEx GetItemTitle(AutoInt index, AutoInt cloid);

		/*--------置字体色_超级列表框Ex--------*/
		void SetItemFontColor(AutoInt index, AutoInt cloid, int FontColor);

		/*--------取字体色_超级列表框Ex--------*/
		int GetItemFontColor(AutoInt index, AutoInt cloid);

		/*--------置项目高度_超级列表框Ex--------*/
		void SetItemSize(AutoInt index, int Size);

		/*--------取项目高度_超级列表框Ex--------*/
		int GetItemSize(AutoInt index);

		/*--------置字体_超级列表框Ex--------*/
		void SetItemFont(AutoInt index, AutoInt cloid, BindataEx Font);

		/*--------取字体_超级列表框Ex--------*/
		BindataEx GetItemFont(AutoInt index, AutoInt cloid);

		/*--------置皮肤_超级列表框Ex--------*/
		void SetItemSkin(AutoInt index, AutoInt cloid, BindataEx Skin);

		/*--------取皮肤_超级列表框Ex--------*/
		BindataEx GetItemSkin(AutoInt index, AutoInt cloid);

		/*--------置选中_超级列表框Ex--------*/
		void SetItemSelected(AutoInt index, AutoInt cloid, BOOL Selected);

		/*--------取选中_超级列表框Ex--------*/
		BOOL GetItemSelected(AutoInt index, AutoInt cloid);

		/*--------置项目编辑配置_超级列表框Ex--------*/
		void SetItemEditConfig(int AttributeId, int InputMode, int MaxInput, StringEx PasswordSubstitution, int Align, int Multiline, int Wrapped, BindataEx Skin, BindataEx Cursor, BindataEx Font, int FontClour, int SelectedFontColor, int SelectedColor, int CursorColor, int ScrollBarMode, int MenuTableWidth, int MenuTableHeight, BindataEx MenuFont, int MenuFontClour, int MenuDisabledFontClour, int MenuTransparency, int MenuLanguage);

		/*--------取项目编辑配置_超级列表框Ex--------*/
		AutoInt GetItemEditConfig(int AttributeId);

		/*--------进入编辑_超级列表框Ex--------*/
		void EnterEdit(AutoInt index, AutoInt cloid, AutoInt mode);

		/*--------退出编辑_超级列表框Ex--------*/
		void ExitEdit(AutoInt index, AutoInt cloid, AutoInt mode);

		/*--------置滚动回调_超级列表框Ex--------*/
		void SetScrollCallBack(ExuiCallback Callback);

		/*--------取滚动回调_超级列表框Ex--------*/
		ExuiCallback GetScrollCallBack();

		/*--------置虚表回调_超级列表框Ex--------*/
		void SetVirtualTableCallBack(ExuiCallback Callback);

		/*--------取虚表回调_超级列表框Ex--------*/
		ExuiCallback GetVirtualTableCallBack();

		/*--------保证显示_超级列表框Ex--------*/
		void GuaranteeVisible(AutoInt index, int cid, int mode);

		/*----------组件超级列表框EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取现行选中项--------*/
		int CurrentItem();
		/*--------置现行选中项--------*/
		void CurrentItem(int CurrentItem);
		/*--------取现行选中列--------*/
		int CurrentColumn();
		/*--------置现行选中列--------*/
		void CurrentColumn(int CurrentColumn);
		/*--------取表头高度--------*/
		int HeadHeight();
		/*--------置表头高度--------*/
		void HeadHeight(int HeadHeight);
		/*--------取表头模式--------*/
		int HeadMode();
		/*--------置表头模式--------*/
		void HeadMode(int HeadMode);
		/*--------取隔行换色--------*/
		BOOL AlternateColor();
		/*--------置隔行换色--------*/
		void AlternateColor(BOOL AlternateColor);
		/*--------取选中模式--------*/
		int SelectMode();
		/*--------置选中模式--------*/
		void SelectMode(int SelectMode);
		/*--------取整行选择--------*/
		BOOL EntireLine();
		/*--------置整行选择--------*/
		void EntireLine(BOOL EntireLine);
		/*--------取事件模式--------*/
		int EventMode();
		/*--------置事件模式--------*/
		void EventMode(int EventMode);
		/*--------取编辑模式--------*/
		int EditMode();
		/*--------置编辑模式--------*/
		void EditMode(int EditMode);
		/*--------取表格线模式--------*/
		int LineMode();
		/*--------置表格线模式--------*/
		void LineMode(int LineMode);
		/*--------取滚动条方式--------*/
		int ScrollBarMode();
		/*--------置滚动条方式--------*/
		void ScrollBarMode(int ScrollBarMode);
		/*--------取项目数据--------*/
		BindataEx ItemData();
		/*--------置项目数据--------*/
		void ItemData(BindataEx ItemData);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件超级列表框EX属性读写函数声明结束--------*/


	};
}
//IcoListboxEx
namespace exui
{
	class IcoListboxEx : public ControlEx
	{
	public:
		IcoListboxEx();
		~IcoListboxEx();

	private:


	public:

		/*--------创建组件_图标列表框EX--------*/
		IcoListboxEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, AutoInt CurrentItem, int TableWidth, int TableHeight, int HSpacing, int LSpacing, int IconWidth, int IconHeight, BindataEx Font, int PageLayout, int Align, int SelectMode, int ScrollBarMode, BindataEx ItemData, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_图标列表框Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_图标列表框Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------插入项目_图标列表框Ex--------*/
		AutoInt InsertItem(AutoInt index, AutoInt addnum, AutoInt Data, BitmapEx Ico, StringEx Title, int FontColor);

		/*--------删除项目_图标列表框Ex--------*/
		void DeleteItem(AutoInt delindex, AutoInt deletenum);

		/*--------取项目数量_图标列表框Ex--------*/
		AutoInt GetItemCount();

		/*--------置附加值_图标列表框Ex--------*/
		void SetItemData(AutoInt index, AutoInt Data);

		/*--------取附加值_图标列表框Ex--------*/
		AutoInt GetItemData(AutoInt index);

		/*--------置图标_图标列表框Ex--------*/
		void SetItemIco(AutoInt index, BitmapEx Ico);

		/*--------取图标_图标列表框Ex--------*/
		BitmapEx GetItemIco(AutoInt index);

		/*--------置文本_图标列表框Ex--------*/
		void SetItemTitle(AutoInt index, StringEx Title);

		/*--------取文本_图标列表框Ex--------*/
		StringEx GetItemTitle(AutoInt index);

		/*--------置字体色_图标列表框Ex--------*/
		void SetItemFontColor(AutoInt index, int FontColor);

		/*--------取字体色_图标列表框Ex--------*/
		int GetItemFontColor(AutoInt index);

		/*--------置字体_图标列表框Ex--------*/
		void SetItemFont(AutoInt index, BindataEx Font);

		/*--------取字体_图标列表框Ex--------*/
		BindataEx GetItemFont(AutoInt index);

		/*--------置皮肤_图标列表框Ex--------*/
		void SetItemSkin(AutoInt index, BindataEx Skin);

		/*--------取皮肤_图标列表框Ex--------*/
		BindataEx GetItemSkin(AutoInt index);

		/*--------置选中_图标列表框Ex--------*/
		void SetItemSelected(AutoInt index, BOOL Selected);

		/*--------取选中_图标列表框Ex--------*/
		BOOL GetItemSelected(AutoInt index);

		/*--------置布局配置_图标列表框Ex--------*/
		void SetItemLayoutConfig(AutoInt index, int ConfigId, int Reserve1, int Reserve2, int Reserve3, int Width, int Height, int Align, int IconWidth, int IconHeight);

		/*--------取布局配置_图标列表框Ex--------*/
		int GetItemLayoutConfig(AutoInt index, int ConfigId);

		/*--------置滚动回调_图标列表框Ex--------*/
		void SetScrollCallBack(ExuiCallback Callback);

		/*--------取滚动回调_图标列表框Ex--------*/
		ExuiCallback GetScrollCallBack();

		/*--------置虚表回调_图标列表框Ex--------*/
		void SetVirtualTableCallBack(ExuiCallback Callback);

		/*--------取虚表回调_图标列表框Ex--------*/
		ExuiCallback GetVirtualTableCallBack();

		/*--------保证显示_图标列表框Ex--------*/
		void GuaranteeVisible(AutoInt index, int mode);

		/*----------组件图标列表框EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取现行选中项--------*/
		int CurrentItem();
		/*--------置现行选中项--------*/
		void CurrentItem(int CurrentItem);
		/*--------取表项宽度--------*/
		int TableWidth();
		/*--------置表项宽度--------*/
		void TableWidth(int TableWidth);
		/*--------取表项高度--------*/
		int TableHeight();
		/*--------置表项高度--------*/
		void TableHeight(int TableHeight);
		/*--------取横间距--------*/
		int HSpacing();
		/*--------置横间距--------*/
		void HSpacing(int HSpacing);
		/*--------取纵间距--------*/
		int LSpacing();
		/*--------置纵间距--------*/
		void LSpacing(int LSpacing);
		/*--------取图标宽--------*/
		int IconWidth();
		/*--------置图标宽--------*/
		void IconWidth(int IconWidth);
		/*--------取图标高--------*/
		int IconHeight();
		/*--------置图标高--------*/
		void IconHeight(int IconHeight);
		/*--------取字体--------*/
		BindataEx Font();
		/*--------置字体--------*/
		void Font(BindataEx Font);
		/*--------取布局模式--------*/
		int PageLayout();
		/*--------置布局模式--------*/
		void PageLayout(int PageLayout);
		/*--------取对齐方式--------*/
		int Align();
		/*--------置对齐方式--------*/
		void Align(int Align);
		/*--------取选中模式--------*/
		int SelectMode();
		/*--------置选中模式--------*/
		void SelectMode(int SelectMode);
		/*--------取滚动条方式--------*/
		int ScrollBarMode();
		/*--------置滚动条方式--------*/
		void ScrollBarMode(int ScrollBarMode);
		/*--------取项目数据--------*/
		BindataEx ItemData();
		/*--------置项目数据--------*/
		void ItemData(BindataEx ItemData);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件图标列表框EX属性读写函数声明结束--------*/


	};
}
//TreeListEx
namespace exui
{
	class TreeListEx : public ControlEx
	{
	public:
		TreeListEx();
		~TreeListEx();

	private:


	public:

		/*--------创建组件_树形列表框EX--------*/
		TreeListEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, AutoInt CurrentItem, BOOL AutoSize, int FoldBtnWidth, int FoldBtnHeight, int IconWidth, int IconHeight, BindataEx Font, int ChildMigration, int BackMigration, int ContentMigration, int SelectMode, int LineMode, int ScrollBarMode, BindataEx ItemData, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_树形框Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_树形框Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------插入_树形框Ex--------*/
		AutoInt InsertItem(AutoInt index, AutoInt addnum, AutoInt Data, BitmapEx Ico, StringEx Title, int FontColor, int Width, int Size, int ItemType, BOOL SetFold, int InsMode);

		/*--------删除_树形框Ex--------*/
		void DeleteItem(AutoInt delindex, AutoInt deletenum);

		/*--------取数量_树形框Ex--------*/
		AutoInt GetItemCount();

		/*--------置附加值_树形框Ex--------*/
		void SetItemData(AutoInt index, AutoInt Data);

		/*--------取附加值_树形框Ex--------*/
		AutoInt GetItemData(AutoInt index);

		/*--------置图标_树形框Ex--------*/
		void SetItemIco(AutoInt index, BitmapEx Ico);

		/*--------取图标_树形框Ex--------*/
		BitmapEx GetItemIco(AutoInt index);

		/*--------置文本_树形框Ex--------*/
		void SetItemTitle(AutoInt index, StringEx Title);

		/*--------取文本_树形框Ex--------*/
		StringEx GetItemTitle(AutoInt index);

		/*--------置字体色_树形框Ex--------*/
		void SetItemFontColor(AutoInt index, int FontColor);

		/*--------取字体色_树形框Ex--------*/
		int GetItemFontColor(AutoInt index);

		/*--------置项目宽度_树形框Ex--------*/
		void SetItemWidth(AutoInt index, int Width);

		/*--------取项目宽度_树形框Ex--------*/
		int GetItemWidth(AutoInt index);

		/*--------置项目高度_树形框Ex--------*/
		void SetItemSize(AutoInt index, int Size);

		/*--------取项目高度_树形框Ex--------*/
		int GetItemSize(AutoInt index);

		/*--------置项目风格_树形框Ex--------*/
		void SetItemType(AutoInt index, int style);

		/*--------取项目风格_树形框Ex--------*/
		int GetItemType(AutoInt index);

		/*--------置字体_树形框Ex--------*/
		void SetItemFont(AutoInt index, BindataEx Font);

		/*--------取字体_树形框Ex--------*/
		BindataEx GetItemFont(AutoInt index);

		/*--------置皮肤_树形框Ex--------*/
		void SetItemSkin(AutoInt index, BindataEx Skin);

		/*--------取皮肤_树形框Ex--------*/
		BindataEx GetItemSkin(AutoInt index);

		/*--------置选中_树形框Ex--------*/
		void SetItemSelected(AutoInt index, BOOL Selected);

		/*--------取选中_树形框Ex--------*/
		BOOL GetItemSelected(AutoInt index);

		/*--------置折叠状态_树形框Ex--------*/
		void SetItemFold(AutoInt index, BOOL Fold);

		/*--------取折叠状态_树形框Ex--------*/
		BOOL GetItemFold(AutoInt index);

		/*--------取缩进层次_树形框Ex--------*/
		int GetItemLevel(AutoInt index);

		/*--------取子项目数_树形框Ex--------*/
		AutoInt GetSubItemCount(AutoInt index);

		/*--------取下一级子项目数_树形框Ex--------*/
		AutoInt GetNextSubItemCount(AutoInt index);

		/*--------取祖宗项目_树形框Ex--------*/
		AutoInt GetAncestorItem(AutoInt index);

		/*--------取父项目_树形框Ex--------*/
		AutoInt GetFatherItem(AutoInt index);

		/*--------取上一同层项目_树形框Ex--------*/
		AutoInt GetLastItem(AutoInt index);

		/*--------取下一同层项目_树形框Ex--------*/
		AutoInt GetNextItem(AutoInt index);

		/*--------置滚动回调_树形框Ex--------*/
		void SetScrollCallBack(ExuiCallback Callback);

		/*--------取滚动回调_树形框Ex--------*/
		ExuiCallback GetScrollCallBack();

		/*--------置虚表回调_树形框Ex--------*/
		void SetVirtualTableCallBack(ExuiCallback Callback);

		/*--------取虚表回调_树形框Ex--------*/
		ExuiCallback GetVirtualTableCallBack();

		/*--------保证显示_树形框Ex--------*/
		void GuaranteeVisible(AutoInt index, int mode);

		/*----------组件树形列表框EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取现行选中项--------*/
		int CurrentItem();
		/*--------置现行选中项--------*/
		void CurrentItem(int CurrentItem);
		/*--------取项目自动扩展--------*/
		BOOL AutoSize();
		/*--------置项目自动扩展--------*/
		void AutoSize(BOOL AutoSize);
		/*--------取折叠钮宽度--------*/
		int FoldBtnWidth();
		/*--------置折叠钮宽度--------*/
		void FoldBtnWidth(int FoldBtnWidth);
		/*--------取折叠钮高度--------*/
		int FoldBtnHeight();
		/*--------置折叠钮高度--------*/
		void FoldBtnHeight(int FoldBtnHeight);
		/*--------取图标宽--------*/
		int IconWidth();
		/*--------置图标宽--------*/
		void IconWidth(int IconWidth);
		/*--------取图标高--------*/
		int IconHeight();
		/*--------置图标高--------*/
		void IconHeight(int IconHeight);
		/*--------取字体--------*/
		BindataEx Font();
		/*--------置字体--------*/
		void Font(BindataEx Font);
		/*--------取子级缩进--------*/
		int ChildMigration();
		/*--------置子级缩进--------*/
		void ChildMigration(int ChildMigration);
		/*--------取背景缩进--------*/
		int BackMigration();
		/*--------置背景缩进--------*/
		void BackMigration(int BackMigration);
		/*--------取内容缩进--------*/
		int ContentMigration();
		/*--------置内容缩进--------*/
		void ContentMigration(int ContentMigration);
		/*--------取选中模式--------*/
		int SelectMode();
		/*--------置选中模式--------*/
		void SelectMode(int SelectMode);
		/*--------取表格线模式--------*/
		int LineMode();
		/*--------置表格线模式--------*/
		void LineMode(int LineMode);
		/*--------取滚动条方式--------*/
		int ScrollBarMode();
		/*--------置滚动条方式--------*/
		void ScrollBarMode(int ScrollBarMode);
		/*--------取项目数据--------*/
		BindataEx ItemData();
		/*--------置项目数据--------*/
		void ItemData(BindataEx ItemData);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件树形列表框EX属性读写函数声明结束--------*/


	};
}
//WebBrowserEx
namespace exui
{
	class WebBrowserEx : public ControlEx
	{
	public:
		WebBrowserEx();
		~WebBrowserEx();

	private:


	public:

		/*--------创建组件_浏览框EX--------*/
		WebBrowserEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BOOL BackgroundTransparent, int MediaVolume, int ZoomFactor, BOOL NavigationToNewWind, BOOL CookieEnabled, BOOL NpapiEnabled, BOOL MenuEnabled, int DragEnable, StringEx StartPage, StringEx UserAgent, int Version, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_浏览框Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_浏览框Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------取内核句柄_浏览框Ex--------*/
		AutoInt GetWebView();

		/*--------载入_浏览框Ex--------*/
		void Load(StringEx url, StringEx baseUrl, int Loadmode);

		/*--------后退_浏览框Ex--------*/
		BOOL GoBack();

		/*--------前进_浏览框Ex--------*/
		BOOL GoForward();

		/*--------可否后退_浏览框Ex--------*/
		BOOL GoCanBack();

		/*--------可否前进_浏览框Ex--------*/
		BOOL GoCanForward();

		/*--------重载_浏览框Ex--------*/
		void ReLoad();

		/*--------停止_浏览框Ex--------*/
		void StopLoading();

		/*--------取地址_浏览框Ex--------*/
		StringEx GetUrl(StringEx Param1);

		/*--------取标题_浏览框Ex--------*/
		StringEx GetTitle(StringEx Param1);

		/*--------取源码_浏览框Ex--------*/
		StringEx GetSource(StringEx Param1);

		/*--------执行命令_浏览框Ex--------*/
		AutoInt RunCmd(AutoInt Cmd, AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4);

		/*--------取Cookie_浏览框Ex--------*/
		StringEx GetCookie(StringEx Param1);

		/*--------置Cookie_浏览框Ex--------*/
		void SetCookie(StringEx url, StringEx cookie);

		/*--------执行Js命令_浏览框Ex--------*/
		StringEx RunJsCmd(StringEx Cmd, StringEx  Param1, StringEx  Param2, StringEx  Param3);

		/*--------绑定Js命令_浏览框Ex--------*/
		void BindJsCmd(StringEx Cmd, void* Param1, AutoInt Param2, AutoInt Param3);

		/*--------否正在加载_浏览框Ex--------*/
		BOOL IsLoading();

		/*--------是否载入失败_浏览框Ex--------*/
		BOOL IsLoadingSucceeded();

		/*--------是否载入完毕_浏览框Ex--------*/
		BOOL IsLoadingCompleted();

		/*--------是否加载完成_浏览框Ex--------*/
		BOOL IsDocumentReady();

		/*--------启用开发者工具_浏览框Ex--------*/
		void ShowDevtools(StringEx path);

		/*----------组件浏览框EX属性读写函数声明开始--------*/


/*--------取背景透明--------*/
		BOOL BackgroundTransparent();
		/*--------置背景透明--------*/
		void BackgroundTransparent(BOOL BackgroundTransparent);
		/*--------取媒体音量--------*/
		int MediaVolume();
		/*--------置媒体音量--------*/
		void MediaVolume(int MediaVolume);
		/*--------取缩放度--------*/
		int ZoomFactor();
		/*--------置缩放度--------*/
		void ZoomFactor(int ZoomFactor);
		/*--------取容许新窗口跳转--------*/
		BOOL NavigationToNewWind();
		/*--------置容许新窗口跳转--------*/
		void NavigationToNewWind(BOOL NavigationToNewWind);
		/*--------取启用Cookie--------*/
		BOOL CookieEnabled();
		/*--------置启用Cookie--------*/
		void CookieEnabled(BOOL CookieEnabled);
		/*--------取启用插件--------*/
		BOOL NpapiEnabled();
		/*--------置启用插件--------*/
		void NpapiEnabled(BOOL NpapiEnabled);
		/*--------取启用菜单--------*/
		BOOL MenuEnabled();
		/*--------置启用菜单--------*/
		void MenuEnabled(BOOL MenuEnabled);
		/*--------取拖拽模式--------*/
		int DragEnable();
		/*--------置拖拽模式--------*/
		void DragEnable(int DragEnable);
		/*--------取初始页面--------*/
		BindataEx StartPage();
		/*--------置初始页面--------*/
		void StartPage(BindataEx StartPage);
		/*--------取用户代理--------*/
		BindataEx UserAgent();
		/*--------置用户代理--------*/
		void UserAgent(BindataEx UserAgent);
		/*--------取版本--------*/
		int Version();
		/*--------置版本--------*/
		void Version(int Version);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件浏览框EX属性读写函数声明结束--------*/


	};
}
//CalendarBoxEx
namespace exui
{
	class CalendarBoxEx : public ControlEx
	{
	public:
		CalendarBoxEx();
		~CalendarBoxEx();

	private:


	public:

		/*--------创建组件_日历框EX--------*/
		CalendarBoxEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, StringEx MiniDate, StringEx MaxDate, StringEx SelectionDate, BindataEx TitleFont, BindataEx WeekFont, BindataEx DayFont, BindataEx TimeFont, int TitleClour, int WeekClour, int WeekendWeekClour, int DayClour, int WeekendClour, int OtherMonthClour, int TimeFontClour, BOOL OnlyThisMonth, int TimeMode, BOOL AutoSelect, int TitleHeight, int WeekHeight, int TimeRegulatorHeight, int PartnerHeight, int Language, BindataEx ItemData, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_日历框Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_日历框Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------置选中_日历框Ex--------*/
		void SetItemSelected(AutoInt index, BOOL Selected);

		/*--------取选中_日历框Ex--------*/

		BOOL GetItemSelected(AutoInt index);

		/*--------置皮肤_日历框Ex--------*/
		void SetItemSkin(AutoInt index, BindataEx Skin);

		/*--------取皮肤_日历框Ex--------*/
		BindataEx GetItemSkin(AutoInt index);

		/*----------组件日历框EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取最小日期--------*/
		StringEx MiniDate();
		/*--------置最小日期--------*/
		void MiniDate(StringEx MiniDate);
		/*--------取最大日期--------*/
		StringEx MaxDate();
		/*--------置最大日期--------*/
		void MaxDate(StringEx MaxDate);
		/*--------取选中日期--------*/
		StringEx SelectionDate();
		/*--------置选中日期--------*/
		void SelectionDate(StringEx SelectionDate);
		/*--------取标题字体--------*/
		BindataEx TitleFont();
		/*--------置标题字体--------*/
		void TitleFont(BindataEx TitleFont);
		/*--------取星期字体--------*/
		BindataEx WeekFont();
		/*--------置星期字体--------*/
		void WeekFont(BindataEx WeekFont);
		/*--------取日期字体--------*/
		BindataEx DayFont();
		/*--------置日期字体--------*/
		void DayFont(BindataEx DayFont);
		/*--------取时间字体--------*/
		BindataEx TimeFont();
		/*--------置时间字体--------*/
		void TimeFont(BindataEx TimeFont);
		/*--------取标题色--------*/
		int TitleClour();
		/*--------置标题色--------*/
		void TitleClour(int TitleClour);
		/*--------取星期色--------*/
		int WeekClour();
		/*--------置星期色--------*/
		void WeekClour(int WeekClour);
		/*--------取双休星期色--------*/
		int WeekendWeekClour();
		/*--------置双休星期色--------*/
		void WeekendWeekClour(int WeekendWeekClour);
		/*--------取日期色--------*/
		int DayClour();
		/*--------置日期色--------*/
		void DayClour(int DayClour);
		/*--------取双休日期色--------*/
		int WeekendClour();
		/*--------置双休日期色--------*/
		void WeekendClour(int WeekendClour);
		/*--------取非本月色--------*/
		int OtherMonthClour();
		/*--------置非本月色--------*/
		void OtherMonthClour(int OtherMonthClour);
		/*--------取时间字体色--------*/
		int TimeFontClour();
		/*--------置时间字体色--------*/
		void TimeFontClour(int TimeFontClour);
		/*--------取只显示本月--------*/
		BOOL OnlyThisMonth();
		/*--------置只显示本月--------*/
		void OnlyThisMonth(BOOL OnlyThisMonth);
		/*--------取时间调整模式--------*/
		int TimeMode();
		/*--------置时间调整模式--------*/
		void TimeMode(int TimeMode);
		/*--------取自动选中--------*/
		BOOL AutoSelect();
		/*--------置自动选中--------*/
		void AutoSelect(BOOL AutoSelect);
		/*--------取标题高度--------*/
		int TitleHeight();
		/*--------置标题高度--------*/
		void TitleHeight(int TitleHeight);
		/*--------取星期高度--------*/
		int WeekHeight();
		/*--------置星期高度--------*/
		void WeekHeight(int WeekHeight);
		/*--------取时间调节器高度--------*/
		int TimeRegulatorHeight();
		/*--------置时间调节器高度--------*/
		void TimeRegulatorHeight(int TimeRegulatorHeight);
		/*--------取伙伴高度--------*/
		int PartnerHeight();
		/*--------置伙伴高度--------*/
		void PartnerHeight(int PartnerHeight);
		/*--------取语言--------*/
		int Language();
		/*--------置语言--------*/
		void Language(int Language);
		/*--------取项目数据--------*/
		BindataEx ItemData();
		/*--------置项目数据--------*/
		void ItemData(BindataEx ItemData);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件日历框EX属性读写函数声明结束--------*/


	};
}
//ColorPickEx
namespace exui
{
	class ColorPickEx : public ControlEx
	{
	public:
		ColorPickEx();
		~ColorPickEx();

	private:


	public:

		/*--------创建组件_选色板EX--------*/
		ColorPickEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, BindataEx Font, int FontClour, int NowClour, BOOL DragTrace, int ClourMode, BindataEx QuickClours, int Style, int RegulatorHeight, int QuickClourSize, int PartnerHeight, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_选色板EX--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_选色板EX--------*/
		AutoInt GetAttribute(AutoInt index);

		/*----------组件选色板EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取字体--------*/
		BindataEx Font();
		/*--------置字体--------*/
		void Font(BindataEx Font);
		/*--------取字体色--------*/
		int FontClour();
		/*--------置字体色--------*/
		void FontClour(int FontClour);
		/*--------取当前色--------*/
		int NowClour();
		/*--------置当前色--------*/
		void NowClour(int NowClour);
		/*--------取调整跟踪--------*/
		BOOL DragTrace();
		/*--------置调整跟踪--------*/
		void DragTrace(BOOL DragTrace);
		/*--------取颜色模式--------*/
		int ClourMode();
		/*--------置颜色模式--------*/
		void ClourMode(int ClourMode);
		/*--------取备选色--------*/
		BindataEx QuickClours();
		/*--------置备选色--------*/
		void QuickClours(BindataEx QuickClours);
		/*--------取样式--------*/
		int Style();
		/*--------置样式--------*/
		void Style(int Style);
		/*--------取调节器高度--------*/
		int RegulatorHeight();
		/*--------置调节器高度--------*/
		void RegulatorHeight(int RegulatorHeight);
		/*--------取备选色直径--------*/
		int QuickClourSize();
		/*--------置备选色直径--------*/
		void QuickClourSize(int QuickClourSize);
		/*--------取伙伴高度--------*/
		int PartnerHeight();
		/*--------置伙伴高度--------*/
		void PartnerHeight(int PartnerHeight);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件选色板EX属性读写函数声明结束--------*/


	};
}
//RichEditEx
namespace exui
{
	class RichEditEx : public ControlEx
	{
	public:
		RichEditEx();
		~RichEditEx();

	private:


	public:

		/*--------创建组件_富文本框EX--------*/
		RichEditEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, StringEx Content, int Type, BOOL BackTransparent, int BackClour, int CursorColor, BindataEx DefaultCharFormat, BindataEx DefaultParaFormat, int InputMode, int MaxInput, StringEx PasswordSubstitution, BOOL Multiline, BOOL Wrapped, int ScrollBarMode, int MenuTableWidth, int MenuTableHeight, BindataEx MenuFont, int MenuFontClour, int MenuDisabledFontClour, int MenuTransparency, int MenuLanguage, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_富文本框EX--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_富文本框EX--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------插入文本_编辑框Ex--------*/
		void InsertText(AutoInt index, StringEx text, int UseRich, StringEx FontName, int Size, int style, int TextColor, int Alignment, int LineHeight);

		/*--------删除文本_编辑框Ex--------*/
		void DeleteText(AutoInt index, AutoInt dellen);

		/*--------置光标位置_编辑框Ex--------*/
		void SetInsertCursor(AutoInt index);

		/*--------取光标位置_编辑框Ex--------*/
		AutoInt GetInsertCursor();

		/*--------置选择文本长度_编辑框Ex--------*/
		void SetSelectLeng(AutoInt SelectLeng);

		/*--------取选择文本长度_编辑框Ex--------*/
		AutoInt GetSelectLeng();

		/*--------保证显示字符_编辑框Ex--------*/
		void GuaranteeVisibleText(AutoInt index, int mode);

		/*--------置选择区字符格式--------*/
		BOOL SetSelCharFormat(BindataEx CharFormat);

		/*--------取选择区字符格式--------*/
		BindataEx GetSelCharFormat();

		/*--------置选择区段落方式--------*/
		BOOL SetSelParaFormat(BindataEx CharFormat);

		/*--------取选择区段落方式--------*/
		BindataEx GetSelParaFormat();

		/*--------执行接口命令--------*/
		AutoInt RunServicesCmd(UINT msg, WPARAM wparam, LPARAM lparam, LRESULT* plresult);

		/*----------组件富文本框EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取内容--------*/
		StringEx Content();
		/*--------置内容--------*/
		void Content(StringEx Content);
		/*--------取类型--------*/
		int Type();
		/*--------置类型--------*/
		void Type(int Type);
		/*--------取背景透明--------*/
		BOOL BackTransparent();
		/*--------置背景透明--------*/
		void BackTransparent(BOOL BackTransparent);
		/*--------取背景色--------*/
		int BackClour();
		/*--------置背景色--------*/
		void BackClour(int BackClour);
		/*--------取光标色--------*/
		int CursorColor();
		/*--------置光标色--------*/
		void CursorColor(int CursorColor);
		/*--------取默认字符格式--------*/
		BindataEx DefaultCharFormat();
		/*--------置默认字符格式--------*/
		void DefaultCharFormat(BindataEx DefaultCharFormat);
		/*--------取默认段落格式--------*/
		BindataEx DefaultParaFormat();
		/*--------置默认段落格式--------*/
		void DefaultParaFormat(BindataEx DefaultParaFormat);
		/*--------取输入方式--------*/
		int InputMode();
		/*--------置输入方式--------*/
		void InputMode(int InputMode);
		/*--------取最大容许长度--------*/
		int MaxInput();
		/*--------置最大容许长度--------*/
		void MaxInput(int MaxInput);
		/*--------取密码替换符--------*/
		StringEx PasswordSubstitution();
		/*--------置密码替换符--------*/
		void PasswordSubstitution(StringEx PasswordSubstitution);
		/*--------取是否容许多行--------*/
		BOOL Multiline();
		/*--------置是否容许多行--------*/
		void Multiline(BOOL Multiline);
		/*--------取自动换行--------*/
		BOOL Wrapped();
		/*--------置自动换行--------*/
		void Wrapped(BOOL Wrapped);
		/*--------取滚动条方式--------*/
		int ScrollBarMode();
		/*--------置滚动条方式--------*/
		void ScrollBarMode(int ScrollBarMode);
		/*--------取菜单项目宽--------*/
		int MenuTableWidth();
		/*--------置菜单项目宽--------*/
		void MenuTableWidth(int MenuTableWidth);
		/*--------取菜单项目高--------*/
		int MenuTableHeight();
		/*--------置菜单项目高--------*/
		void MenuTableHeight(int MenuTableHeight);
		/*--------取菜单字体--------*/
		BindataEx MenuFont();
		/*--------置菜单字体--------*/
		void MenuFont(BindataEx MenuFont);
		/*--------取菜单字体色--------*/
		int MenuFontClour();
		/*--------置菜单字体色--------*/
		void MenuFontClour(int MenuFontClour);
		/*--------取菜单禁止字体色--------*/
		int MenuDisabledFontClour();
		/*--------置菜单禁止字体色--------*/
		void MenuDisabledFontClour(int MenuDisabledFontClour);
		/*--------取菜单透明度--------*/
		int MenuTransparency();
		/*--------置菜单透明度--------*/
		void MenuTransparency(int MenuTransparency);
		/*--------取菜单语言--------*/
		int MenuLanguage();
		/*--------置菜单语言--------*/
		void MenuLanguage(int MenuLanguage);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件富文本框EX属性读写函数声明结束--------*/


	};
}
//ExtendEx
namespace exui
{
	class ExtendEx : public ControlEx
	{
	public:
		ExtendEx();
		~ExtendEx();

	private:


	public:

		/*--------创建组件_扩展组件EX--------*/
		ExtendEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx AttrEx, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_扩展组件Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_扩展组件Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------启用开发者工具_扩展组件Ex--------*/
		int ShowDevtools(StringEx toolspath);

		/*--------执行扩展命令_扩展组件Ex--------*/
		StringEx RunExtendCmd(StringEx Cmd, StringEx  Param1, StringEx  Param2, StringEx  Param3);

		/*----------组件扩展组件EX属性读写函数声明开始--------*/


/*--------取扩展属性表--------*/
		BindataEx ExtendAttr();
		/*--------置扩展属性表--------*/
		void ExtendAttr(BindataEx ExtendAttr);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件扩展组件EX属性读写函数声明结束--------*/


	};
}
//AnimationbuttonEx
namespace exui
{
	class AnimationbuttonEx : public ControlEx
	{
	public:
		AnimationbuttonEx();
		~AnimationbuttonEx();

	private:


	public:

		/*--------创建组件_动画按钮EX--------*/
		AnimationbuttonEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, BOOL Selected, BindataEx Icon, int IconWidth, int IconHeight, int Align, StringEx Text, BindataEx Font, int FontClour, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_动画按钮EX--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_动画按钮EX--------*/
		AutoInt GetAttribute(AutoInt index);

		/*----------组件动画按钮EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取启用动画--------*/
		BOOL EnableAnimation();
		/*--------置启用动画--------*/
		void EnableAnimation(BOOL EnableAnimation);
		/*--------取图标--------*/
		BindataEx Icon();
		/*--------置图标--------*/
		void Icon(BindataEx Icon);
		/*--------取图标宽--------*/
		int IconWidth();
		/*--------置图标宽--------*/
		void IconWidth(int IconWidth);
		/*--------取图标高--------*/
		int IconHeight();
		/*--------置图标高--------*/
		void IconHeight(int IconHeight);
		/*--------取对齐方式--------*/
		int Align();
		/*--------置对齐方式--------*/
		void Align(int Align);
		/*--------取标题--------*/
		StringEx Title();
		/*--------置标题--------*/
		void Title(StringEx Title);
		/*--------取字体--------*/
		BindataEx Font();
		/*--------置字体--------*/
		void Font(BindataEx Font);
		/*--------取字体色--------*/
		int FontClour();
		/*--------置字体色--------*/
		void FontClour(int FontClour);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件动画按钮EX属性读写函数声明结束--------*/


	};
}
//AdvancedFormEx
namespace exui
{
	class AdvancedFormEx : public ControlEx
	{
	public:
		AdvancedFormEx();
		~AdvancedFormEx();

	private:


	public:

		/*--------创建组件_高级表格EX--------*/
		AdvancedFormEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, AutoInt CurrentItem, AutoInt CurrentColumn, AutoInt HeaderRowNum, AutoInt HeaderColumnNum, AutoInt BottomFixedRow, AutoInt RightFixedColumn, int AdjustmentMode, int SelectMode, BOOL EntireLine, int EventMode, int EditMode, BOOL TreeType, int LineMode, int ScrollBarMode, BindataEx ItemData, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_高级表格Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_高级表格Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------插入列_高级表格Ex--------*/
		AutoInt InsertColumn(AutoInt cloid, AutoInt addnum, int wight, int min, int max, BindataEx HeaderFont, int HeaderIcoW, int HeaderIcoH, int HeaderAlign, BindataEx ItemFont, int ItemIcoW, int ItemIcoH, int ItemAlign, BindataEx FixedItemFont, int FixedItemIcoW, int FixedItemIcoH, int FixedItemAlign, int HeaderTreeType, int TreeType, int BottomFixedTreeType, int ChildMigration, int ContentMigration, int LineMode, BindataEx Skin);

		/*--------删除列_高级表格Ex--------*/
		void DeleteColumn(AutoInt delindex, AutoInt deletenum);

		/*--------取列数量_高级表格Ex--------*/
		int GetColumnCount();

		/*--------置列属性_高级表格Ex--------*/
		void SetColumnAttribute(AutoInt index, AutoInt Endindex, AutoInt AttributeId, int wight, int min, int max, BindataEx HeaderFont, int HeaderIcoW, int HeaderIcoH, int HeaderAlign, BindataEx ItemFont, int ItemIcoW, int ItemIcoH, int ItemAlign, BindataEx FixedItemFont, int FixedItemIcoW, int FixedItemIcoH, int FixedItemAlign, int HeaderTreeType, int TreeType, int BottomFixedTreeType, int ChildMigration, int ContentMigration, int LineMode, BindataEx Skin);

		/*--------取列属性_高级表格Ex--------*/
		int GetColumnAttribute(AutoInt index, AutoInt AttributeId);

		/*--------插入项目_高级表格Ex--------*/
		AutoInt InsertItem(AutoInt index, AutoInt addnum, AutoInt Data, int Height, int MinHeight, int MaxHeight, int Style, BOOL SetFold, int InsMode);

		/*--------删除项目_高级表格Ex--------*/
		void DeleteItem(AutoInt delindex, AutoInt deletenum);

		/*--------取数量_高级表格Ex--------*/
		AutoInt GetItemCount();

		/*--------置附加值_高级表格Ex--------*/
		void SetItemData(AutoInt StarRow, AutoInt EndRow, AutoInt Data);

		/*--------取附加值_高级表格Ex--------*/
		AutoInt GetItemData(AutoInt index);

		/*--------置图标_高级表格Ex--------*/
		void SetItemIco(AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn, BitmapEx Ico);

		/*--------取图标_高级表格Ex--------*/
		BitmapEx GetItemIco(AutoInt index, AutoInt cloid);

		/*--------置文本_高级表格Ex--------*/
		void SetItemTitle(AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn, StringEx Title);

		/*--------取文本_高级表格Ex--------*/
		StringEx GetItemTitle(AutoInt index, AutoInt cloid);

		/*--------置字体色_高级表格Ex--------*/
		void SetItemFontColor(AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn, int FontColor);

		/*--------取字体色_高级表格Ex--------*/
		int GetItemFontColor(AutoInt index, AutoInt cloid);

		/*--------置项目高度_高级表格Ex--------*/
		void SetItemHeight(AutoInt StarRow, AutoInt EndRow, int Height);

		/*--------取项目高度_高级表格Ex--------*/
		int GetItemHeight(AutoInt index);

		/*--------置项目最大高度_高级表格Ex--------*/
		void SetItemMaxHeight(AutoInt StarRow, AutoInt EndRow, int Height);

		/*--------取项目最大高度_高级表格Ex--------*/
		int GetItemMaxHeight(AutoInt index);

		/*--------置项目最小高度_高级表格Ex--------*/
		void SetItemMinHeight(AutoInt StarRow, AutoInt EndRow, int Height);

		/*--------取项目最小高度_高级表格Ex--------*/
		int GetItemMinHeight(AutoInt index);

		/*--------合并单元格_高级表格Ex--------*/
		void MergeCell(AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn);

		/*--------分解单元格_高级表格Ex--------*/
		void BreakCell(AutoInt StarRow, AutoInt StarColumn);

		/*--------取单元格合并信息_高级表格Ex--------*/
		AutoInt GetMergeInfo(AutoInt StarRow, AutoInt StarColumn, int Type);

		/*--------置字体_高级表格Ex--------*/
		void SetItemFont(AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn, BindataEx Font);

		/*--------取字体_高级表格Ex--------*/
		BindataEx GetItemFont(AutoInt index, AutoInt cloid);

		/*--------置皮肤_高级表格Ex--------*/
		void SetItemSkin(AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn, BindataEx Skin);

		/*--------取皮肤_高级表格Ex--------*/
		BindataEx GetItemSkin(AutoInt index, AutoInt cloid);

		/*--------置对齐方式_高级表格Ex--------*/
		void SetItemAlign(AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn, int Align);

		/*--------取对齐方式_高级表格Ex--------*/

		int GetItemAlign(AutoInt index, AutoInt cloid);

		/*--------置选中状态_高级表格Ex--------*/
		void SetItemSelected(AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn, BOOL Select);

		/*--------取选中状态_高级表格Ex--------*/
		BOOL GetItemSelected(AutoInt index, AutoInt cloid);

		/*--------置项目风格_高级表格Ex--------*/
		void SetItemType(AutoInt StarRow, AutoInt EndRow, int style);

		/*--------取项目风格_高级表格Ex--------*/
		int GetItemType(AutoInt index);

		/*--------置折叠状态_高级表格Ex--------*/
		void SetItemFold(AutoInt StarRow, AutoInt EndRow, BOOL Fold);

		/*--------取折叠状态_高级表格Ex--------*/
		BOOL GetItemFold(AutoInt index);

		/*--------取缩进层次_高级表格Ex--------*/
		int GetItemLevel(AutoInt index);

		/*--------取子项目数_高级表格Ex--------*/
		int GetSubItemCount(AutoInt index);

		/*--------取下一级子项目数_高级表格Ex--------*/
		int GetNextSubItemCount(AutoInt index);

		/*--------取祖宗项目_高级表格Ex--------*/
		int GetAncestorItem(AutoInt index);

		/*--------取父项目_高级表格Ex--------*/
		int GetFatherItem(AutoInt index);

		/*--------取上一同层项目_高级表格Ex--------*/
		int GetLastItem(AutoInt index);

		/*--------取下一同层项目_高级表格Ex--------*/
		int GetNextItem(AutoInt index);

		/*--------置项目编辑配置_高级表格Ex--------*/
		void SetItemEditConfig(int AttributeId, int InputMode, int MaxInput, StringEx PasswordSubstitution, int Align, int Multiline, int Wrapped, BindataEx Skin, BindataEx Cursor, BindataEx Font, int FontClour, int SelectedFontColor, int SelectedColor, int CursorColor, int ScrollBarMode, int MenuTableWidth, int MenuTableHeight, BindataEx MenuFont, int MenuFontClour, int MenuDisabledFontClour, int MenuTransparency, int MenuLanguage);

		/*--------取项目编辑配置_高级表格Ex--------*/
		AutoInt GetItemEditConfig(int AttributeId);

		/*--------进入编辑_高级表格Ex--------*/
		void EnterEdit(AutoInt index, AutoInt cloid, AutoInt mode);

		/*--------退出编辑_高级表格Ex--------*/
		void ExitEdit(AutoInt index, AutoInt cloid, AutoInt mode);

		/*--------置滚动回调_高级表格Ex--------*/
		void SetScrollCallBack(ExuiCallback Callback);

		/*--------取滚动回调_高级表格Ex--------*/
		ExuiCallback GetScrollCallBack();

		/*--------置虚表回调_高级表格Ex--------*/
		void SetVirtualTableCallBack(ExuiCallback Callback);

		/*--------取虚表回调_高级表格Ex--------*/
		ExuiCallback GetVirtualTableCallBack();

		/*--------保证显示_高级表格Ex--------*/
		void GuaranteeVisible(AutoInt index, AutoInt cid, int mode);


		/*----------组件高级表格EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取现行选中项--------*/
		int CurrentItem();
		/*--------置现行选中项--------*/
		void CurrentItem(int CurrentItem);
		/*--------取现行选中列--------*/
		int CurrentColumn();
		/*--------置现行选中列--------*/
		void CurrentColumn(int CurrentColumn);
		/*--------取表头行数--------*/
		int HeaderRowNum();
		/*--------置表头行数--------*/
		void HeaderRowNum(int HeaderRowNum);
		/*--------取表头列数--------*/
		int HeaderColumnNum();
		/*--------置表头列数--------*/
		void HeaderColumnNum(int HeaderColumnNum);
		/*--------取底悬浮行数--------*/
		int BottomFixedRow();
		/*--------置底悬浮行数--------*/
		void BottomFixedRow(int BottomFixedRow);
		/*--------取右悬浮列数--------*/
		int RightFixedColumn();
		/*--------置右悬浮列数--------*/
		void RightFixedColumn(int RightFixedColumn);
		/*--------取调整模式--------*/
		int AdjustmentMode();
		/*--------置调整模式--------*/
		void AdjustmentMode(int AdjustmentMode);
		/*--------取选中模式--------*/
		int SelectMode();
		/*--------置选中模式--------*/
		void SelectMode(int SelectMode);
		/*--------取整行选择--------*/
		BOOL EntireLine();
		/*--------置整行选择--------*/
		void EntireLine(BOOL EntireLine);
		/*--------取事件模式--------*/
		int EventMode();
		/*--------置事件模式--------*/
		void EventMode(int EventMode);
		/*--------取编辑模式--------*/
		int EditMode();
		/*--------置编辑模式--------*/
		void EditMode(int EditMode);
		/*--------取树形表格--------*/
		BOOL TreeType();
		/*--------置树形表格--------*/
		void TreeType(BOOL TreeType);
		/*--------取表格线模式--------*/
		int LineMode();
		/*--------置表格线模式--------*/
		void LineMode(int LineMode);
		/*--------取滚动条方式--------*/
		int ScrollBarMode();
		/*--------置滚动条方式--------*/
		void ScrollBarMode(int ScrollBarMode);
		/*--------取项目数据--------*/
		BindataEx ItemData();
		/*--------置项目数据--------*/
		void ItemData(BindataEx ItemData);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件高级表格EX属性读写函数声明结束--------*/



	};
}
//BarChartEx
namespace exui
{
	class BarChartEx : public ControlEx
	{
	public:
		BarChartEx();
		~BarChartEx();

	private:


	public:

		/*--------创建组件_柱状图Ex--------*/
		BarChartEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, int ChartStyle, StringEx TemplateH, int ColorHA, int GridStyleH, int GridColorH, BindataEx FontH, int FontColorH, StringEx TemplateV, int MiniV, int MaxV, int ColorVA, int GridStyleV, int GridColorV, BindataEx FontV, int FontColorV, int Leftretain, int Topretain, int Rightretain, int bottomretain, AutoInt hotcolumn, int GraphWidth, int GraphSpace, BindataEx GraphData, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_柱状图Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_柱状图Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------置图例数量_柱状图Ex--------*/
		void SetGraphCount(AutoInt Count);

		/*--------取图例数量_柱状图Ex--------*/
		AutoInt GetGraphCount();

		/*--------置图例属性_柱状图Ex--------*/
		void SetGraphAttr(AutoInt index, AutoInt AttrId, int Colour, int Rgn);

		/*--------取图例属性_柱状图Ex--------*/
		AutoInt GetGraphAttr(AutoInt index, AutoInt AttrId);

		/*--------置图例值_柱状图Ex--------*/
		void SetGraphValue(AutoInt index, AutoInt Cid, int Value);

		/*--------取图例值_柱状图Ex--------*/
		int GetGraphValue(AutoInt index, AutoInt Cid);



		/*----------组件柱状图EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取图表风格--------*/
		int ChartStyle();
		/*--------置图表风格--------*/
		void ChartStyle(int ChartStyle);
		/*--------取横轴标题模版--------*/
		StringEx TemplateH();
		/*--------置横轴标题模版--------*/
		void TemplateH(StringEx TemplateH);
		/*--------取横轴颜色--------*/
		int ColorHA();
		/*--------置横轴颜色--------*/
		void ColorHA(int ColorHA);
		/*--------取横轴网格类型--------*/
		int GridStyleH();
		/*--------置横轴网格类型--------*/
		void GridStyleH(int GridStyleH);
		/*--------取横轴网格色--------*/
		int GridColorH();
		/*--------置横轴网格色--------*/
		void GridColorH(int GridColorH);
		/*--------取横轴字体--------*/
		BindataEx FontH();
		/*--------置横轴字体--------*/
		void FontH(BindataEx FontH);
		/*--------取横轴字体色--------*/
		int FontColorH();
		/*--------置横轴字体色--------*/
		void FontColorH(int FontColorH);
		/*--------取纵轴标题模版--------*/
		StringEx TemplateV();
		/*--------置纵轴标题模版--------*/
		void TemplateV(StringEx TemplateV);
		/*--------取纵轴最小值--------*/
		int MiniV();
		/*--------置纵轴最小值--------*/
		void MiniV(int MiniV);
		/*--------取纵轴最大值--------*/
		int MaxV();
		/*--------置纵轴最大值--------*/
		void MaxV(int MaxV);
		/*--------取纵轴颜色--------*/
		int ColorVA();
		/*--------置纵轴颜色--------*/
		void ColorVA(int ColorVA);
		/*--------取纵轴网格类型--------*/
		int GridStyleV();
		/*--------置纵轴网格类型--------*/
		void GridStyleV(int GridStyleV);
		/*--------取纵轴网格色--------*/
		int GridColorV();
		/*--------置纵轴网格色--------*/
		void GridColorV(int GridColorV);
		/*--------取纵轴字体--------*/
		BindataEx FontV();
		/*--------置纵轴字体--------*/
		void FontV(BindataEx FontV);
		/*--------取纵轴字体色--------*/
		int FontColorV();
		/*--------置纵轴字体色--------*/
		void FontColorV(int FontColorV);
		/*--------取左预留--------*/
		int LeftReserve();
		/*--------置左预留--------*/
		void LeftReserve(int LeftReserve);
		/*--------取顶预留--------*/
		int TopReserve();
		/*--------置顶预留--------*/
		void TopReserve(int TopReserve);
		/*--------取右预留--------*/
		int RightReserve();
		/*--------置右预留--------*/
		void RightReserve(int RightReserve);
		/*--------取底预留--------*/
		int BottomReserve();
		/*--------置底预留--------*/
		void BottomReserve(int BottomReserve);
		/*--------取热点图列--------*/
		int HotColumn();
		/*--------置热点图列--------*/
		void HotColumn(int HotColumn);
		/*--------取图例宽度--------*/
		int GraphWidth();
		/*--------置图例宽度--------*/
		void GraphWidth(int GraphWidth);
		/*--------取图例间距--------*/
		int GraphSpace();
		/*--------置图例间距--------*/
		void GraphSpace(int GraphSpace);
		/*--------取图例管理--------*/
		BindataEx GraphData();
		/*--------置图例管理--------*/
		void GraphData(BindataEx GraphData);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件柱状图EX属性读写函数声明结束--------*/



	};
}
//CurveChartEx
namespace exui
{
	class CurveChartEx : public ControlEx
	{
	public:
		CurveChartEx();
		~CurveChartEx();

	private:


	public:
		/*--------创建组件_曲线图Ex--------*/
		CurveChartEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, int ChartStyle, StringEx TemplateH, int ColorHA, int GridStyleH, int GridColorH, BindataEx FontH, int FontColorH, StringEx TemplateV, int MiniV, int MaxV, int ColorVA, int GridStyleV, int GridColorV, BindataEx FontV, int FontColorV, int Leftretain, int Topretain, int Rightretain, int bottomretain, AutoInt hotcolumn, BindataEx GraphData, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_曲线图Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_曲线图Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------置图例数量_曲线图Ex--------*/
		void SetGraphCount(AutoInt Count);

		/*--------取图例数量_曲线图Ex--------*/
		AutoInt GetGraphCount();

		/*--------置图例属性_曲线图Ex--------*/
		void SetGraphAttr(AutoInt index, AutoInt AttrId, int Colour, int Tension, int width, int style, int PointSize, int HotPointSize);

		/*--------取图例属性_曲线图Ex--------*/
		AutoInt GetGraphAttr(AutoInt index, AutoInt AttrId);

		/*--------置图例值_曲线图Ex--------*/
		void SetGraphValue(AutoInt index, AutoInt Cid, int Value);

		/*--------取图例值_曲线图Ex--------*/
		int GetGraphValue(AutoInt index, AutoInt Cid);



		/*----------组件曲线图Ex属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取图表风格--------*/
		int ChartStyle();
		/*--------置图表风格--------*/
		void ChartStyle(int ChartStyle);
		/*--------取横轴标题模版--------*/
		StringEx TemplateH();
		/*--------置横轴标题模版--------*/
		void TemplateH(StringEx TemplateH);
		/*--------取横轴颜色--------*/
		int ColorHA();
		/*--------置横轴颜色--------*/
		void ColorHA(int ColorHA);
		/*--------取横轴网格类型--------*/
		int GridStyleH();
		/*--------置横轴网格类型--------*/
		void GridStyleH(int GridStyleH);
		/*--------取横轴网格色--------*/
		int GridColorH();
		/*--------置横轴网格色--------*/
		void GridColorH(int GridColorH);
		/*--------取横轴字体--------*/
		BindataEx FontH();
		/*--------置横轴字体--------*/
		void FontH(BindataEx FontH);
		/*--------取横轴字体色--------*/
		int FontColorH();
		/*--------置横轴字体色--------*/
		void FontColorH(int FontColorH);
		/*--------取纵轴标题模版--------*/
		StringEx TemplateV();
		/*--------置纵轴标题模版--------*/
		void TemplateV(StringEx TemplateV);
		/*--------取纵轴最小值--------*/
		int MiniV();
		/*--------置纵轴最小值--------*/
		void MiniV(int MiniV);
		/*--------取纵轴最大值--------*/
		int MaxV();
		/*--------置纵轴最大值--------*/
		void MaxV(int MaxV);
		/*--------取纵轴颜色--------*/
		int ColorVA();
		/*--------置纵轴颜色--------*/
		void ColorVA(int ColorVA);
		/*--------取纵轴网格类型--------*/
		int GridStyleV();
		/*--------置纵轴网格类型--------*/
		void GridStyleV(int GridStyleV);
		/*--------取纵轴网格色--------*/
		int GridColorV();
		/*--------置纵轴网格色--------*/
		void GridColorV(int GridColorV);
		/*--------取纵轴字体--------*/
		BindataEx FontV();
		/*--------置纵轴字体--------*/
		void FontV(BindataEx FontV);
		/*--------取纵轴字体色--------*/
		int FontColorV();
		/*--------置纵轴字体色--------*/
		void FontColorV(int FontColorV);
		/*--------取左预留--------*/
		int LeftReserve();
		/*--------置左预留--------*/
		void LeftReserve(int LeftReserve);
		/*--------取顶预留--------*/
		int TopReserve();
		/*--------置顶预留--------*/
		void TopReserve(int TopReserve);
		/*--------取右预留--------*/
		int RightReserve();
		/*--------置右预留--------*/
		void RightReserve(int RightReserve);
		/*--------取底预留--------*/
		int BottomReserve();
		/*--------置底预留--------*/
		void BottomReserve(int BottomReserve);
		/*--------取热点图列--------*/
		int HotColumn();
		/*--------置热点图列--------*/
		void HotColumn(int HotColumn);
		/*--------取图例管理--------*/
		BindataEx GraphData();
		/*--------置图例管理--------*/
		void GraphData(BindataEx GraphData);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件曲线图Ex属性读写函数声明结束--------*/






	};
}
//PieChartEx
namespace exui
{
	class PieChartEx : public ControlEx
	{
	public:
		PieChartEx();
		~PieChartEx();

	private:


	public:

		/*--------创建组件_饼形图Ex--------*/
		PieChartEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, int Leftretain, int Topretain, int Rightretain, int bottomretain, AutoInt hotGraph, int GraphStar, int GraphWidth, BOOL Zoom, BindataEx ItemData, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_饼形图Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_饼形图Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------置图例数量_饼形图Ex--------*/
		void SetGraphCount(AutoInt Count);

		/*--------取图例数量_饼形图Ex--------*/
		AutoInt GetGraphCount();

		/*--------置图例属性_饼形图Ex--------*/
		void SetGraphAttr(AutoInt index, AutoInt AttrId, int color);

		/*--------取图例属性_饼形图Ex--------*/
		AutoInt GetGraphAttr(AutoInt index, AutoInt AttrId);

		/*--------置图例值_饼形图Ex--------*/
		void SetGraphValue(AutoInt index, int Value);

		/*--------取图例值_饼形图Ex--------*/
		int GetGraphValue(AutoInt index);



		/*----------组件饼形图EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取左预留--------*/
		int LeftReserve();
		/*--------置左预留--------*/
		void LeftReserve(int LeftReserve);
		/*--------取顶预留--------*/
		int TopReserve();
		/*--------置顶预留--------*/
		void TopReserve(int TopReserve);
		/*--------取右预留--------*/
		int RightReserve();
		/*--------置右预留--------*/
		void RightReserve(int RightReserve);
		/*--------取底预留--------*/
		int BottomReserve();
		/*--------置底预留--------*/
		void BottomReserve(int BottomReserve);
		/*--------取热点图例--------*/
		int HotGraph();
		/*--------置热点图例--------*/
		void HotGraph(int HotGraph);
		/*--------取图例起始度--------*/
		int GraphStar();
		/*--------置图例起始度--------*/
		void GraphStar(int GraphStar);
		/*--------取图例宽度--------*/
		int GraphWidth();
		/*--------置图例宽度--------*/
		void GraphWidth(int GraphWidth);
		/*--------取缩放--------*/
		BOOL Zoom();
		/*--------置缩放--------*/
		void Zoom(BOOL Zoom);
		/*--------取图例管理--------*/
		BindataEx GraphData();
		/*--------置图例管理--------*/
		void GraphData(BindataEx GraphData);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件饼形图EX属性读写函数声明结束--------*/




	};
}
//SlideButtonEx
namespace exui
{
	class SlideButtonEx : public ControlEx
	{
	public:
		SlideButtonEx();
		~SlideButtonEx();

	private:


	public:

		/*--------创建组件_滑动按钮EX--------*/
		SlideButtonEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, BOOL Selected, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_滑动按钮Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_滑动按钮Ex--------*/
		AutoInt GetAttribute(AutoInt index);





		/*----------组件滑动按钮EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取选中--------*/
		BOOL Selected();
		/*--------置选中--------*/
		void Selected(BOOL Selected);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件滑动按钮EX属性读写函数声明结束--------*/



	};
}
//CandleChartEx
namespace exui
{
	class CandleChartEx : public ControlEx
	{
	public:
		CandleChartEx();
		~CandleChartEx();

	private:


	public:



		/*--------创建组件_烛线图Ex--------*/
		CandleChartEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, int ChartStyle, StringEx TemplateH, int ColorHA, int GridStyleH, int GridColorH, BindataEx FontH, int FontColorH, StringEx TemplateV, int MiniV, int MaxV, int ColorVA, int GridStyleV, int GridColorV, BindataEx FontV, int FontColorV, int Leftretain, int Topretain, int Rightretain, int bottomretain, AutoInt hotcolumn, int GraphWidth, BOOL NegativeHollow, int NegativeColor, BOOL PositiveHollow, int PositiveColor, BindataEx ItemData, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_烛线图Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_烛线图Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------置图例值_烛线图Ex--------*/
		void SetGraphValue(AutoInt index, AutoInt DataId, int openVal, int closeVal, int lowVal, int heighVal);

		/*--------取图例值_烛线图Ex--------*/
		int GetGraphValue(AutoInt index, AutoInt DataId);




		/*----------组件烛线图Ex属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取图表风格--------*/
		int ChartStyle();
		/*--------置图表风格--------*/
		void ChartStyle(int ChartStyle);
		/*--------取横轴标题模版--------*/
		StringEx TemplateH();
		/*--------置横轴标题模版--------*/
		void TemplateH(StringEx TemplateH);
		/*--------取横轴颜色--------*/
		int ColorHA();
		/*--------置横轴颜色--------*/
		void ColorHA(int ColorHA);
		/*--------取横轴网格类型--------*/
		int GridStyleH();
		/*--------置横轴网格类型--------*/
		void GridStyleH(int GridStyleH);
		/*--------取横轴网格色--------*/
		int GridColorH();
		/*--------置横轴网格色--------*/
		void GridColorH(int GridColorH);
		/*--------取横轴字体--------*/
		BindataEx FontH();
		/*--------置横轴字体--------*/
		void FontH(BindataEx FontH);
		/*--------取横轴字体色--------*/
		int FontColorH();
		/*--------置横轴字体色--------*/
		void FontColorH(int FontColorH);
		/*--------取纵轴标题模版--------*/
		StringEx TemplateV();
		/*--------置纵轴标题模版--------*/
		void TemplateV(StringEx TemplateV);
		/*--------取纵轴最小值--------*/
		int MiniV();
		/*--------置纵轴最小值--------*/
		void MiniV(int MiniV);
		/*--------取纵轴最大值--------*/
		int MaxV();
		/*--------置纵轴最大值--------*/
		void MaxV(int MaxV);
		/*--------取纵轴颜色--------*/
		int ColorVA();
		/*--------置纵轴颜色--------*/
		void ColorVA(int ColorVA);
		/*--------取纵轴网格类型--------*/
		int GridStyleV();
		/*--------置纵轴网格类型--------*/
		void GridStyleV(int GridStyleV);
		/*--------取纵轴网格色--------*/
		int GridColorV();
		/*--------置纵轴网格色--------*/
		void GridColorV(int GridColorV);
		/*--------取纵轴字体--------*/
		BindataEx FontV();
		/*--------置纵轴字体--------*/
		void FontV(BindataEx FontV);
		/*--------取纵轴字体色--------*/
		int FontColorV();
		/*--------置纵轴字体色--------*/
		void FontColorV(int FontColorV);
		/*--------取左预留--------*/
		int LeftReserve();
		/*--------置左预留--------*/
		void LeftReserve(int LeftReserve);
		/*--------取顶预留--------*/
		int TopReserve();
		/*--------置顶预留--------*/
		void TopReserve(int TopReserve);
		/*--------取右预留--------*/
		int RightReserve();
		/*--------置右预留--------*/
		void RightReserve(int RightReserve);
		/*--------取底预留--------*/
		int BottomReserve();
		/*--------置底预留--------*/
		void BottomReserve(int BottomReserve);
		/*--------取热点图列--------*/
		int HotColumn();
		/*--------置热点图列--------*/
		void HotColumn(int HotColumn);
		/*--------取图例宽度--------*/
		int GraphWidth();
		/*--------置图例宽度--------*/
		void GraphWidth(int GraphWidth);
		/*--------取阴线镂空--------*/
		BOOL NegativeHollow();
		/*--------置阴线镂空--------*/
		void NegativeHollow(BOOL NegativeHollow);
		/*--------取阴线颜色--------*/
		int NegativeColor();
		/*--------置阴线颜色--------*/
		void NegativeColor(int NegativeColor);
		/*--------取阳线镂空--------*/
		BOOL PositiveHollow();
		/*--------置阳线镂空--------*/
		void PositiveHollow(BOOL PositiveHollow);
		/*--------取阳线颜色--------*/
		int PositiveColor();
		/*--------置阳线颜色--------*/
		void PositiveColor(int PositiveColor);
		/*--------取图例管理--------*/
		BindataEx GraphData();
		/*--------置图例管理--------*/
		void GraphData(BindataEx GraphData);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件烛线图Ex属性读写函数声明结束--------*/



	};
}
//DrawPanelEx
namespace exui
{
	class DrawPanelEx : public ControlEx
	{
	public:
		DrawPanelEx();
		~DrawPanelEx();

	private:


	public:
		/*--------创建组件_画板EX--------*/
		DrawPanelEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, int BackColor, int LineColor, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_画板EX--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_画板EX--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------画图片_画板EX--------*/
		void DrawImage(BitmapEx Image, float dstX, float dstY, float dstWidth, float dstHeight, float srcx, float srcy, float srcwidth, float srcheight, int Alpha);

		/*--------画文本_画板EX--------*/
		void DrawString(StringEx string, float dstX, float dstY, float dstWidth, float dstHeight, BindataEx font, AutoInt StringFormat, int DrawMode, int DrawParame, int color, int auxcolor);

		/*--------画多边形_画板EX--------*/
		void DrawPolygon(float* Points, int count, int DrawMode, int DrawParame, int color, int auxcolor);

		/*--------画矩形_画板EX--------*/
		void DrawRect(float X, float Y, float Width, float Height, int DrawMode, int DrawParame, int color, int auxcolor);

		/*--------画椭圆_画板EX--------*/
		void DrawEllipse(float X, float Y, float Width, float Height, float Rgn, int DrawMode, int DrawParame, int color, int auxcolor);

		/*--------画饼_画板EX--------*/
		void DrawPie(float X, float Y, float Width, float Height, float starAngle, float sweepAngle, int DrawMode, int DrawParame, int color, int auxcolor);

		/*--------画曲线_画板EX--------*/
		void DrawCurve(float* Points, int count, float tension, int Mode, int DrawMode, int DrawParame, int color, int auxcolor);

		/*--------画弧线_画板EX--------*/
		void DrawArc(float X, float Y, float Width, float Height, float starAngle, float sweepAngle, int DrawMode, int DrawParame, int color, int auxcolor);

		/*--------画直线_画板EX--------*/
		void DrawLine(float X, float Y, float Width, float Height, int DrawMode, int DrawParame, int color, int auxcolor);

		/*--------清除_画板EX--------*/
		void Clear(int colour, float X, float Y, float Width, float Height, BOOL mode);

		/*--------更新_画板EX--------*/
		void Update(float X, float Y, float Width, float Height, BOOL mode);

		/*--------置剪辑区_画板EX--------*/
		void SetClip(int CombineMd, AutoInt Clip, int cliptype, float X, float Y, float Width, float Height, float Rgn);

		/*--------置绘图质量_画板EX--------*/
		void SetGraphicsquality(int Smoothing, int Interpolation, int PixelOffset);

		/*--------取绘图数据_画板EX--------*/
		AutoInt GetGraphicsData(int Type, float X, float Y, float Width, float Height);




		/*----------组件画板EX属性读写函数声明开始--------*/


/*--------取背景颜色--------*/
		int BackColor();
		/*--------置背景颜色--------*/
		void BackColor(int BackColor);
		/*--------取边线色--------*/
		int LineColor();
		/*--------置边线色--------*/
		void LineColor(int LineColor);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件画板EX属性读写函数声明结束--------*/



	};

}
//ScrollLayoutBoxEx
namespace exui
{

	class ScrollLayoutBoxEx : public ControlEx
	{
	public:
		ScrollLayoutBoxEx();
		~ScrollLayoutBoxEx();

	private:

	public:
		/*--------创建组件_滚动布局框EX--------*/
		ScrollLayoutBoxEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, int Mode, int HorizontalPosition, int VerticalPosition, int MaxHorizontalPosition, int MaxVerticalPosition, int retain1, int retain2, int retain3, int retain4, int retain5, int retain6, int ScrollBarMode, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_滚动布局框EX--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_滚动布局框EX--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------添加组件_滚动布局框EX--------*/
		BOOL InsertControl(Control_P ControlHand, int left, int top);

		/*--------移除组件_滚动布局框EX--------*/
		BOOL RemoveControl(Control_P ControlHand, Control_P Parentcontrol, int left, int top);


		/*----------组件滚动布局框EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取模式--------*/
		int Mode();
		/*--------置模式--------*/
		void Mode(int Mode);
		/*--------取横向位置--------*/
		int HorizontalPosition();
		/*--------置横向位置--------*/
		void HorizontalPosition(int HorizontalPosition);
		/*--------取纵向位置--------*/
		int VerticalPosition();
		/*--------置纵向位置--------*/
		void VerticalPosition(int VerticalPosition);
		/*--------取横向最大位置--------*/
		int MaxHorizontalPosition();
		/*--------置横向最大位置--------*/
		void MaxHorizontalPosition(int MaxHorizontalPosition);
		/*--------取纵向最大位置--------*/
		int MaxVerticalPosition();
		/*--------置纵向最大位置--------*/
		void MaxVerticalPosition(int MaxVerticalPosition);
		/*--------取保留属性1--------*/
		int Reserve1();
		/*--------置保留属性1--------*/
		void Reserve1(int Reserve1);
		/*--------取保留属性2--------*/
		int Reserve2();
		/*--------置保留属性2--------*/
		void Reserve2(int Reserve2);
		/*--------取保留属性3--------*/
		int Reserve3();
		/*--------置保留属性3--------*/
		void Reserve3(int Reserve3);
		/*--------取保留属性4--------*/
		int Reserve4();
		/*--------置保留属性4--------*/
		void Reserve4(int Reserve4);
		/*--------取保留属性5--------*/
		int Reserve5();
		/*--------置保留属性5--------*/
		void Reserve5(int Reserve5);
		/*--------取保留属性6--------*/
		int Reserve6();
		/*--------置保留属性6--------*/
		void Reserve6(int Reserve6);
		/*--------取滚动条方式--------*/
		int ScrollBarMode();
		/*--------置滚动条方式--------*/
		void ScrollBarMode(int ScrollBarMode);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件滚动布局框EX属性读写函数声明结束--------*/


	};


}
//MediaboxEx
namespace exui
{

	class MediaboxEx : public ControlEx
	{
	public:
		MediaboxEx();
		~MediaboxEx();

	private:

	public:

		/*--------创建组件_影像框EX--------*/
		MediaboxEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, StringEx MediaFile, int Align, int PlayState, int PlayMode, int TotalProgress, int CurrentPosition, int volume, int PlaySpeed, int Reserve1, int Reserve2, int Reserve3, int Reserve4, int Reserve5, int Reserve6, int Reserve7, int Reserve8, int Reserve9, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_影像框EX--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_影像框EX--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------执行命令_影像框EX--------*/
		AutoInt RunCmd(AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4);


		/*----------组件影像框EX属性读写函数声明开始--------*/


		/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取影像文件--------*/
		StringEx MediaFile();
		/*--------置影像文件--------*/
		void MediaFile(StringEx MediaFile);
		/*--------取对齐方式--------*/
		int Align();
		/*--------置对齐方式--------*/
		void Align(int Align);
		/*--------取播放状态--------*/
		int PlayState();
		/*--------置播放状态--------*/
		void PlayState(int PlayState);
		/*--------取播放模式--------*/
		int PlayMode();
		/*--------置播放模式--------*/
		void PlayMode(int PlayMode);
		/*--------取总进度--------*/
		int TotalProgress();
		/*--------置总进度--------*/
		void TotalProgress(int TotalProgress);
		/*--------取当前进度--------*/
		int CurrentPosition();
		/*--------置当前进度--------*/
		void CurrentPosition(int CurrentPosition);
		/*--------取音量--------*/
		int volume();
		/*--------置音量--------*/
		void volume(int volume);
		/*--------取播放速度--------*/
		int PlaySpeed();
		/*--------置播放速度--------*/
		void PlaySpeed(int PlaySpeed);
		/*--------取保留1--------*/
		int Reserve1();
		/*--------置保留1--------*/
		void Reserve1(int Reserve1);
		/*--------取保留2--------*/
		int Reserve2();
		/*--------置保留2--------*/
		void Reserve2(int Reserve2);
		/*--------取保留3--------*/
		int Reserve3();
		/*--------置保留3--------*/
		void Reserve3(int Reserve3);
		/*--------取保留4--------*/
		int Reserve4();
		/*--------置保留4--------*/
		void Reserve4(int Reserve4);
		/*--------取保留5--------*/
		int Reserve5();
		/*--------置保留5--------*/
		void Reserve5(int Reserve5);
		/*--------取保留6--------*/
		int Reserve6();
		/*--------置保留6--------*/
		void Reserve6(int Reserve6);
		/*--------取保留7--------*/
		int Reserve7();
		/*--------置保留7--------*/
		void Reserve7(int Reserve7);
		/*--------取保留8--------*/
		int Reserve8();
		/*--------置保留8--------*/
		void Reserve8(int Reserve8);
		/*--------取保留9--------*/
		int Reserve9();
		/*--------置保留9--------*/
		void Reserve9(int Reserve9);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件影像框EX属性读写函数声明结束--------*/


	};


}
//CarouselBoxEx
namespace exui
{
	class CarouselBoxEx : public ControlEx
	{
	public:
		CarouselBoxEx();
		~CarouselBoxEx();

	private:


	public:

		/*--------创建组件_轮播框EX--------*/
		CarouselBoxEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, AutoInt CurrentPosition, int Schedule, int Spacing, int LayoutStyle, int LayoutParam, int CtrlModel, int CarouselMode, int ScrollDirection, int AutoPlay, int DurationTime, int FrameDelay, int Step, int AlgorithmMode, BindataEx ItemData, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_轮播框Ex--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_轮播框Ex--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------插入_轮播框Ex--------*/
		AutoInt InsertItem(AutoInt index, AutoInt Data, BitmapEx Skin, BitmapEx Ico, int IconWidth, int IconHeight, StringEx Title, BitmapEx Font, int FontColor, int HotFontColor, int Type, int Align, int reserve, int Size);

		/*--------删除_轮播框Ex--------*/
		void DeleteItem(AutoInt index);

		/*--------取数量_轮播框Ex--------*/
		AutoInt GetItemCount();

		/*--------置项目属性_轮播框Ex--------*/
		void SetItemAttribute(AutoInt index, int AttributeId, AutoInt Data, BitmapEx Skin, BitmapEx Ico, int IconWidth, int IconHeight, StringEx Title, BitmapEx Font, int FontColor, int HotFontColor, int Type, int Align, int reserve, int Size);

		/*--------取项目属性_轮播框Ex--------*/
		AutoInt GetItemAttribute(AutoInt index, int AttributeId);

		/*--------保证显示_轮播框Ex--------*/
		AutoInt RunCmd(AutoInt Cmd, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4);

		/*----------组件轮播框EX属性读写函数声明开始--------*/


/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------当前进度--------*/
		AutoInt CurrentPosition();
		/*--------当前进度--------*/
		void CurrentPosition(AutoInt CurrentPosition);
		/*--------纵向--------*/
		int Schedule();
		/*--------纵向--------*/
		void Schedule(int Schedule);

		/*--------间距--------*/
		int Spacing();
		/*--------间距--------*/
		void Spacing(int Spacing);

		/*--------布局样式--------*/
		int LayoutStyle();
		/*--------布局样式--------*/
		void LayoutStyle(int LayoutStyle);

		/*--------布局参数--------*/
		int LayoutParam();
		/*--------布局参数--------*/
		void LayoutParam(int LayoutParam);

		/*--------控制模式--------*/
		int CtrlModel();
		/*--------控制模式--------*/
		void CtrlModel(int CtrlModel);

		/*--------轮播模式--------*/
		int CarouselMode();
		/*--------轮播模式--------*/
		void CarouselMode(int CarouselMode);

		/*--------滚动方向--------*/
		int ScrollDirection();
		/*--------滚动方向--------*/
		void ScrollDirection(int ScrollDirection);

		/*--------自动播放--------*/
		int AutoPlay();
		/*--------自动播放--------*/
		void AutoPlay(int AutoPlay);

		/*--------停留时长--------*/
		int DurationTime();
		/*--------停留时长--------*/
		void DurationTime(int DurationTime);

		/*--------帧延迟--------*/
		int FrameDelay();
		/*--------帧延迟--------*/
		void FrameDelay(int FrameDelay);

		/*--------帧步进--------*/
		int Step();
		/*--------帧步进--------*/
		void Step(int Step);

		/*--------滚动算法--------*/
		int AlgorithmMode();
		/*--------滚动算法--------*/
		void AlgorithmMode(int AlgorithmMode);
		/*--------取项目数据--------*/
		BindataEx ItemData();
		/*--------置项目数据--------*/
		void ItemData(BindataEx ItemData);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件轮播框EX属性读写函数声明结束--------*/


	};
}
//SuperEditEx
namespace exui
{

	class SuperEditEx : public ControlEx
	{
	public:
		SuperEditEx();
		~SuperEditEx();

	private:

	public:

		/*--------创建组件_超级编辑框EX--------*/
		SuperEditEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, StringEx Kernel, StringEx Attribute, StringEx Data, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_超级编辑框EX--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_超级编辑框EX--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------执行命令_超级编辑框EX--------*/
		StringEx RunCmd(StringEx Cmd, StringEx Parameter1, StringEx Parameter2, StringEx Parameter3, StringEx Parameter4, StringEx Parameter5, StringEx Parameter6, StringEx Parameter7, StringEx Parameter8, StringEx Parameter9);


		/*----------组件超级编辑框EX属性读写函数声明开始--------*/


		/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取核心--------*/
		StringEx Kernel();
		/*--------置核心--------*/
		void Kernel(StringEx Kernel);
		/*--------取属性--------*/
		StringEx Attribute();
		/*--------置属性--------*/
		void Attribute(StringEx Attribute);
		/*--------取数据--------*/
		StringEx Data();
		/*--------置数据--------*/
		void Data(StringEx Data);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件超级编辑框EX属性读写函数声明结束--------*/


	};


}
//ChatBoxEx
namespace exui
{

	class ChatBoxEx : public ControlEx
	{
	public:
		ChatBoxEx();
		~ChatBoxEx();

	private:

	public:

		/*--------创建组件_聊天框EX--------*/
		ChatBoxEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, StringEx Kernel, StringEx Attribute, StringEx Data, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_聊天框EX--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_聊天框EX--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------执行命令_聊天框EX--------*/
		StringEx RunCmd(StringEx Cmd, StringEx Parameter1, StringEx Parameter2, StringEx Parameter3, StringEx Parameter4, StringEx Parameter5, StringEx Parameter6, StringEx Parameter7, StringEx Parameter8, StringEx Parameter9);


		/*----------组件聊天框EX属性读写函数声明开始--------*/


		/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取核心--------*/
		StringEx Kernel();
		/*--------置核心--------*/
		void Kernel(StringEx Kernel);
		/*--------取属性--------*/
		StringEx Attribute();
		/*--------置属性--------*/
		void Attribute(StringEx Attribute);
		/*--------取数据--------*/
		StringEx Data();
		/*--------置数据--------*/
		void Data(StringEx Data);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件聊天框EX属性读写函数声明结束--------*/


	};


}
//SuperChartEx
namespace exui
{

	class SuperChartEx : public ControlEx
	{
	public:
		SuperChartEx();
		~SuperChartEx();

	private:

	public:

		/*--------创建组件_超级图表框EX--------*/
		SuperChartEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, StringEx Kernel, StringEx Attribute, StringEx Data, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_超级图表框EX--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_超级图表框EX--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------执行命令_超级图表框EX--------*/
		StringEx RunCmd(StringEx Cmd, StringEx Parameter1, StringEx Parameter2, StringEx Parameter3, StringEx Parameter4, StringEx Parameter5, StringEx Parameter6, StringEx Parameter7, StringEx Parameter8, StringEx Parameter9);


		/*----------组件超级图表框EX属性读写函数声明开始--------*/


		/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取核心--------*/
		StringEx Kernel();
		/*--------置核心--------*/
		void Kernel(StringEx Kernel);
		/*--------取属性--------*/
		StringEx Attribute();
		/*--------置属性--------*/
		void Attribute(StringEx Attribute);
		/*--------取数据--------*/
		StringEx Data();
		/*--------置数据--------*/
		void Data(StringEx Data);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件超级图表框EX属性读写函数声明结束--------*/


	};


}
//DataReportBoxEx
namespace exui
{

	class DataReportBoxEx : public ControlEx
	{
	public:
		DataReportBoxEx();
		~DataReportBoxEx();

	private:

	public:

		/*--------创建组件_数据报表框EX--------*/
		DataReportBoxEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, StringEx Kernel, StringEx Attribute, StringEx Data, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_数据报表框EX--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_数据报表框EX--------*/
		AutoInt GetAttribute(AutoInt index);

		/*--------执行命令_数据报表框EX--------*/
		StringEx RunCmd(StringEx Cmd, StringEx Parameter1, StringEx Parameter2, StringEx Parameter3, StringEx Parameter4, StringEx Parameter5, StringEx Parameter6, StringEx Parameter7, StringEx Parameter8, StringEx Parameter9);


		/*----------组件数据报表框EX属性读写函数声明开始--------*/


		/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------取核心--------*/
		StringEx Kernel();
		/*--------置核心--------*/
		void Kernel(StringEx Kernel);
		/*--------取属性--------*/
		StringEx Attribute();
		/*--------置属性--------*/
		void Attribute(StringEx Attribute);
		/*--------取数据--------*/
		StringEx Data();
		/*--------置数据--------*/
		void Data(StringEx Data);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);



		/*----------组件数据报表框EX属性读写函数声明结束--------*/


	};


}


//PageNavigBarEx
namespace exui
{

	class PageNavigBarEx : public ControlEx
	{
	public:
		PageNavigBarEx();
		~PageNavigBarEx();

	private:

	public:

		/*--------创建组件_分页导航条EX--------*/
		PageNavigBarEx_P Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, int PageCount, int CurrentPage, int NavigBtnCount, int Align, BindataEx Font, int FontColor, int CurrentFontClour, int LeftReservation, int RightReservation, BindataEx ElemeData, BindataEx Layout);

		/*--------置属性_分页导航条EX--------*/
		void SetAttribute(AutoInt index, AutoInt attribute);

		/*--------取属性_分页导航条EX--------*/
		AutoInt GetAttribute(AutoInt index);

		/*----------组件分页导航条EX属性读写函数声明开始--------*/

		/*--------取皮肤--------*/
		BindataEx Skin();
		/*--------置皮肤--------*/
		void Skin(BindataEx Skin);
		/*--------模式--------*/
		int Mode();
		/*--------模式--------*/
		void Mode(int Mode);
		/*--------总页数--------*/
		int PageCount();
		/*--------总页数--------*/
		void PageCount(int PageCount);
		/*--------当前页--------*/
		int CurrentPage();
		/*--------当前页--------*/
		void CurrentPage(int CurrentPage);
		/*--------页码数量--------*/
		int NavigBtnCount();
		/*--------页码数量--------*/
		void NavigBtnCount(int NavigBtnCount);
		/*--------对齐方式--------*/
		int Align();
		/*--------对齐方式--------*/
		void Align(int Align);
		/*--------取字体--------*/
		BindataEx Font();
		/*--------置字体--------*/
		void Font(BindataEx Data);
		/*--------字体色--------*/
		int FontColor();
		/*--------字体色--------*/
		void FontColor(int FontColor);
		/*--------选中字体色--------*/
		int CurrentFontClour();
		/*--------选中字体色--------*/
		void CurrentFontClour(int CurrentFontClour);
		/*--------左预留--------*/
		int LeftReservation();
		/*--------左预留--------*/
		void LeftReservation(int LeftReservation);
		/*--------右预留--------*/
		int RightReservation();
		/*--------右预留--------*/
		void RightReservation(int RightReservation);
		/*--------取扩展元素--------*/
		BindataEx ElemeData();
		/*--------置扩展元素--------*/
		void ElemeData(BindataEx ElemeData);
		/*--------取布局器--------*/
		BindataEx LayoutData();
		/*--------置布局器--------*/
		void LayoutData(BindataEx LayoutData);

		/*----------组件分页导航条EX属性读写函数声明结束--------*/


	};


}



//ExuiProgramEx
namespace exui
{

	class ExuiProgramEx : public BaseEx
	{

	public:
		AutoInt Hwnd;
		ExuiProgramEx();
		~ExuiProgramEx();
	public:



		/*----------缓动任务Ex事件通知开始--------*/


/*
* @brief 缓动任务事件
*
*缓动任务事件
* @return 返回数据格式 1001
* @param EventControl 事件组件 参数提供事件发生任务句柄
* @param EventType 事件类型
* @param Parameter1 相关参数1 参数值视事件类型而定
* @param Parameter2 相关参数2 参数值视事件类型而定
* @param Parameter3 相关参数3 参数值视事件类型而定
* @param Parameter4 相关参数4 参数值视事件类型而定
*/
		virtual AutoInt OnSlowMotionTaskExEvent_Event(SlowMotionTaskEx_P EventControl, AutoInt EventType, AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4) { return NULL; }


		/*----------缓动任务Ex事件通知结束--------*/


		/*----------定时器Ex事件通知开始--------*/


/*
* @brief 定时器事件
*
*定时器事件
* @return 返回数据格式 1001
* @param EventControl 事件组件 参数提供事件发生定时器句柄
* @param EventType 事件类型
* @param Parameter1 相关参数1 参数值视事件类型而定
* @param Parameter2 相关参数2 参数值视事件类型而定
* @param Parameter3 相关参数3 参数值视事件类型而定
* @param Parameter4 相关参数4 参数值视事件类型而定
*/
		virtual AutoInt OnTimerExEvent_Event(TimeEx_P EventControl, AutoInt EventType, AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4) { return NULL; }


		/*----------定时器Ex事件通知结束--------*/


				/*----------菜单Ex事件通知开始--------*/

		/*
		* @brief 扩展处理弹出
		*
		*扩展处理弹出
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数3 相关参数4
		*/
		virtual AutoInt Event_MenuEx_ExtendPop(MenuEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 扩展处理关闭
		*
		*扩展处理关闭
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数3 相关参数4
		*/
		virtual AutoInt Event_MenuEx_ExtendClose(MenuEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 扩展处理测量
		*
		*扩展处理测量
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数3 相关参数4
		*/
		virtual AutoInt Event_MenuEx_ExtendMmeasure(MenuEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 扩展处理重画
		*
		*扩展处理重画
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数3 相关参数4
		*/
		virtual AutoInt Event_MenuEx_ExtendDraw(MenuEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 将弹出
		*
		*将弹出
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数3 相关参数4
		*/
		virtual AutoInt Event_MenuEx_WillPop(MenuEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 已弹出
		*
		*已弹出
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数3 相关参数4
		*/
		virtual AutoInt Event_MenuEx_Pop(MenuEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 将关闭
		*
		*将关闭
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数3 相关参数4
		*/
		virtual AutoInt Event_MenuEx_WillClose(MenuEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 已关闭
		*
		*已关闭
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数3 相关参数4
		*/
		virtual AutoInt Event_MenuEx_Close(MenuEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 项目左键单击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMenuExEvent_Item_L_Click(MenuEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 项目右键单击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMenuExEvent_Item_R_Click(MenuEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 项目鼠标左键放开
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMenuExEvent_Item_L_Up(MenuEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 项目鼠标右键放开
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMenuExEvent_Item_R_Up(MenuEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 项目鼠标左键按下
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMenuExEvent_Item_L_Down(MenuEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 项目鼠标右键按下
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMenuExEvent_Item_R_Down(MenuEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 项目左键双击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMenuExEvent_Item_L_DClick(MenuEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 项目右键双击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMenuExEvent_Item_R_DClick(MenuEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 项目鼠标进入
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMenuExEvent_Item_MouseIn(MenuEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 项目鼠标离开
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMenuExEvent_Item_MouseOut(MenuEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }



		/*----------菜单Ex事件通知结束--------*/








				/*----------下拉列表Ex事件通知开始--------*/

		/*
		* @brief 扩展处理弹出
		*
		*扩展处理弹出
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数3 相关参数4
		*/
		virtual AutoInt Event_DownlistEx_ExtendPop(DownlistEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 扩展处理关闭
		*
		*扩展处理关闭
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数3 相关参数4
		*/
		virtual AutoInt Event_DownlistEx_ExtendClose(DownlistEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 扩展处理测量
		*
		*扩展处理测量
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数3 相关参数4
		*/
		virtual AutoInt Event_DownlistEx_ExtendMmeasure(DownlistEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 扩展处理重画
		*
		*扩展处理重画
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数3 相关参数4
		*/
		virtual AutoInt Event_DownlistEx_ExtendDraw(DownlistEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 将弹出
		*
		*将弹出
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数3 相关参数4
		*/
		virtual AutoInt Event_DownlistEx_WillPop(DownlistEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 已弹出
		*
		*已弹出
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数3 相关参数4
		*/
		virtual AutoInt Event_DownlistEx_Pop(DownlistEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 将关闭
		*
		*将关闭
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数3 相关参数4
		*/
		virtual AutoInt Event_DownlistEx_WillClose(DownlistEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 已关闭
		*
		*已关闭
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数3 相关参数4
		*/
		virtual AutoInt Event_DownlistEx_Close(DownlistEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 项目左键单击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnDownlistExEvent_Item_L_Click(DownlistEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 项目右键单击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnDownlistExEvent_Item_R_Click(DownlistEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 项目鼠标左键放开
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnDownlistExEvent_Item_L_Up(DownlistEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 项目鼠标右键放开
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnDownlistExEvent_Item_R_Up(DownlistEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 项目鼠标左键按下
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnDownlistExEvent_Item_L_Down(DownlistEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 项目鼠标右键按下
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnDownlistExEvent_Item_R_Down(DownlistEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 项目左键双击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnDownlistExEvent_Item_L_DClick(DownlistEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 项目右键双击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnDownlistExEvent_Item_R_DClick(DownlistEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 项目鼠标进入
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnDownlistExEvent_Item_MouseIn(DownlistEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
		* @brief 项目鼠标离开
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnDownlistExEvent_Item_MouseOut(DownlistEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }



		/*----------下拉列表Ex事件通知结束--------*/




		/*----------系统窗口事件通知开始--------*/



		/*
		* @brief 创建完毕
		*
		*创建完毕
		* @return 返回数据格式 1001
		* @param EventControl 事件窗口 参数提供事件发生窗口句柄
		* @param EventType 事件类型
		* @param wParam 相关参数1 系统提供,自行查阅文档
		* @param lParam 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowBoxExEvent_Create(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam) { return NULL; }


		/*
		* @brief 可否被关闭
		*
		* 可否被关闭
		* @return 返回数据格式 1001
		* @param EventControl 事件窗口 参数提供事件发生窗口句柄
		* @param EventType 事件类型
		* @param wParam 相关参数1 系统提供,自行查阅文档
		* @param lParam 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowBoxExEvent_Close(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam) { return NULL; }

		/*
		* @brief 将被销毁
		*
		* 将被销毁
		* @return 返回数据格式 1001
		* @param EventControl 事件窗口 参数提供事件发生窗口句柄
		* @param EventType 事件类型
		* @param wParam 相关参数1 系统提供,自行查阅文档
		* @param lParam 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowBoxExEvent_Destroy(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam) { return NULL; }

		/*
		* @brief 位置被改变
		*
		* 位置被改变
		* @return 返回数据格式 1001
		* @param EventControl 事件窗口 参数提供事件发生窗口句柄
		* @param EventType 事件类型
		* @param wParam 相关参数1 系统提供,自行查阅文档
		* @param lParam 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowBoxExEvent_Move(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam) { return NULL; }


		/*
		* @brief 尺寸被改变
		*
		* 尺寸被改变
		* @return 返回数据格式 1001
		* @param EventControl 事件窗口 参数提供事件发生窗口句柄
		* @param EventType 事件类型
		* @param wParam 相关参数1 系统提供,自行查阅文档
		* @param lParam 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowBoxExEvent_Size(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam) { return NULL; }



		/*
		* @brief 激活状态改变
		*
		* 激活状态改变
		* @return 返回数据格式 1001
		* @param EventControl 事件窗口 参数提供事件发生窗口句柄
		* @param EventType 事件类型
		* @param wParam 相关参数1 系统提供,自行查阅文档
		* @param lParam 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowBoxExEvent_Activate(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam) { return NULL; }


		/*
		* @brief 可视状态改变
		*
		* 可视状态改变
		* @return 返回数据格式 1001
		* @param EventControl 事件窗口 参数提供事件发生窗口句柄
		* @param EventType 事件类型
		* @param wParam 相关参数1 系统提供,自行查阅文档
		* @param lParam 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowBoxExEvent_ShowWindow(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam) { return NULL; }


		/*
		* @brief 鼠标左键被按下
		*
		* 鼠标左键被按下
		* @return 返回数据格式 1001
		* @param EventControl 事件窗口 参数提供事件发生窗口句柄
		* @param EventType 事件类型
		* @param wParam 相关参数1 系统提供,自行查阅文档
		* @param lParam 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowBoxExEvent_L_Down(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam) { return NULL; }


		/*
		* @brief 鼠标左键被放开
		*
		* 鼠标左键被放开
		* @return 返回数据格式 1001
		* @param EventControl 事件窗口 参数提供事件发生窗口句柄
		* @param EventType 事件类型
		* @param wParam 相关参数1 系统提供,自行查阅文档
		* @param lParam 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowBoxExEvent_L_Up(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam) { return NULL; }


		/*
		* @brief 左键双击
		*
		* 左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件窗口 参数提供事件发生窗口句柄
		* @param EventType 事件类型
		* @param wParam 相关参数1 系统提供,自行查阅文档
		* @param lParam 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowBoxExEvent_L_DClick(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam) { return NULL; }



		/*
		* @brief 鼠标右键被按下
		*
		* 鼠标右键被按下
		* @return 返回数据格式 1001
		* @param EventControl 事件窗口 参数提供事件发生窗口句柄
		* @param EventType 事件类型
		* @param wParam 相关参数1 系统提供,自行查阅文档
		* @param lParam 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowBoxExEvent_R_Down(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam) { return NULL; }




		/*
		* @brief 鼠标右键被放开
		*
		* 鼠标右键被放开
		* @return 返回数据格式 1001
		* @param EventControl 事件窗口 参数提供事件发生窗口句柄
		* @param EventType 事件类型
		* @param wParam 相关参数1 系统提供,自行查阅文档
		* @param lParam 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowBoxExEvent_R_Up(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam) { return NULL; }




		/*
		* @brief 鼠标位置被移动
		*
		* 鼠标位置被移动
		* @return 返回数据格式 1001
		* @param EventControl 事件窗口 参数提供事件发生窗口句柄
		* @param EventType 事件类型
		* @param wParam 相关参数1 系统提供,自行查阅文档
		* @param lParam 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowBoxExEvent_MouseMove(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam) { return NULL; }



		/*
		* @brief 获得焦点
		*
		* 获得焦点
		* @return 返回数据格式 1001
		* @param EventControl 事件窗口 参数提供事件发生窗口句柄
		* @param EventType 事件类型
		* @param wParam 相关参数1 系统提供,自行查阅文档
		* @param lParam 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowBoxExEvent_SetFocus(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam) { return NULL; }


		/*
		* @brief 失去焦点
		*
		* 失去焦点
		* @return 返回数据格式 1001
		* @param EventControl 事件窗口 参数提供事件发生窗口句柄
		* @param EventType 事件类型
		* @param wParam 相关参数1 系统提供,自行查阅文档
		* @param lParam 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowBoxExEvent_KillFocus(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam) { return NULL; }


		/*
		* @brief 按下某键
		*
		* 按下某键
		* @return 返回数据格式 1001
		* @param EventControl 事件窗口 参数提供事件发生窗口句柄
		* @param EventType 事件类型
		* @param wParam 相关参数1 系统提供,自行查阅文档
		* @param lParam 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowBoxExEvent_KeyDown(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam) { return NULL; }


		/*
		* @brief 放开某键
		*
		* 放开某键
		* @return 返回数据格式 1001
		* @param EventControl 事件窗口 参数提供事件发生窗口句柄
		* @param EventType 事件类型
		* @param wParam 相关参数1 系统提供,自行查阅文档
		* @param lParam 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowBoxExEvent_KeyUp(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam) { return NULL; }



		/*
		* @brief 字符输入
		*
		* 字符输入
		* @return 返回数据格式 1001
		* @param EventControl 事件窗口 参数提供事件发生窗口句柄
		* @param EventType 事件类型
		* @param wParam 相关参数1 系统提供,自行查阅文档
		* @param lParam 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowBoxExEvent_Char(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam) { return NULL; }



		/*
		* @brief 滚轮滚动
		*
		* 滚轮滚动
		* @return 返回数据格式 1001
		* @param EventControl 事件窗口 参数提供事件发生窗口句柄
		* @param EventType 事件类型
		* @param wParam 相关参数1 系统提供,自行查阅文档
		* @param lParam 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowBoxExEvent_MouseWheel(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam) { return NULL; }



		/*
		* @brief 接收到文件拖放
		*
		* 接收到文件拖放
		* @return 返回数据格式 1001
		* @param EventControl 事件窗口 参数提供事件发生窗口句柄
		* @param EventType 事件类型
		* @param wParam 相关参数1 系统提供,自行查阅文档
		* @param lParam 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowBoxExEvent_DropFiles(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam) { return NULL; }


		/*
		* @brief 托盘图标事件
		*
		* 托盘图标事件
		* @return 返回数据格式 1001
		* @param EventControl 事件窗口 参数提供事件发生窗口句柄
		* @param EventType 事件类型
		* @param wParam 相关参数1 系统提供,自行查阅文档
		* @param lParam 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowBoxExEvent_NotifyIcon(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam) { return NULL; }

		/*
		* @brief 其他事件
		*
		* 其他事件
		* @return 返回数据格式 1001
		* @param EventControl 事件窗口 参数提供事件发生窗口句柄
		* @param EventType 事件类型
		* @param wParam 相关参数1 系统提供,自行查阅文档
		* @param lParam 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowBoxExEvent_Other(HWND EventControl, AutoInt EventType, AutoInt wParam, AutoInt lParam) { return NULL; }





		/*----------系统窗口事件通知结束--------*/






		/*----------组件事件通知开始--------*/


/*
* @brief 鼠标左键单击
*
*鼠标左键单击
* @return 返回数据格式 1001
* @param EventControl 事件组件 参数提供事件发生组件句柄
* @param EventMsg 事件消息值 参数提供事件消息值
* @param PointX 坐标x 坐标x
* @param PointY 坐标y 坐标y
* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
*/
		virtual AutoInt OnWindowExEvent_L_Click(WindowEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowExEvent_R_Click(WindowEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowExEvent_L_Down(WindowEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowExEvent_R_Down(WindowEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowExEvent_L_Up(WindowEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowExEvent_R_Up(WindowEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowExEvent_L_DClick(WindowEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowExEvent_R_DClick(WindowEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowExEvent_MouseMove(WindowEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnWindowExEvent_MouseEnterLeave(WindowEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowExEvent_MouseWheel(WindowEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowExEvent_keyEvent(WindowEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWindowExEvent_CharInput(WindowEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnWindowExEvent_Focus(WindowEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnWindowExEvent_Other(WindowEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnWindowExEvent_ElemEvent(WindowEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnWindowExEvent_TaskEvent(WindowEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnWindowExEvent_PublicEvent(WindowEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnWindowExEvent_PrivateEvent(WindowEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnWindowExEvent_ParentWinRefreshEvent(WindowEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 反馈事件
		*
		*由调用反馈事件命令触发本事件的最佳场景为多线程时更新ui界面.
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnWindowExEvent_FeedBackEvent(WindowEx_P EventControl, AutoInt EventMsg, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 按钮左键单击
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnWindowExEvent_Button_L_Click(WindowEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮右键单击
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnWindowExEvent_Button_R_Click(WindowEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮鼠标左键放开
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnWindowExEvent_Button_L_Up(WindowEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮鼠标右键放开
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnWindowExEvent_Button_R_Up(WindowEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮鼠标左键按下
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnWindowExEvent_Button_L_Down(WindowEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮鼠标右键按下
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnWindowExEvent_Button_R_Down(WindowEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮左键双击
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnWindowExEvent_Button_L_DClick(WindowEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮右键双击
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnWindowExEvent_Button_R_DClick(WindowEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮鼠标进入
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnWindowExEvent_Button_MouseIn(WindowEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮鼠标离开
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnWindowExEvent_Button_MouseOut(WindowEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnFilterExEvent_L_Click(FilterEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnFilterExEvent_R_Click(FilterEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnFilterExEvent_L_Down(FilterEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnFilterExEvent_R_Down(FilterEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnFilterExEvent_L_Up(FilterEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnFilterExEvent_R_Up(FilterEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnFilterExEvent_L_DClick(FilterEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnFilterExEvent_R_DClick(FilterEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnFilterExEvent_MouseMove(FilterEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnFilterExEvent_MouseEnterLeave(FilterEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnFilterExEvent_MouseWheel(FilterEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnFilterExEvent_keyEvent(FilterEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnFilterExEvent_CharInput(FilterEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnFilterExEvent_Focus(FilterEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnFilterExEvent_Other(FilterEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnFilterExEvent_ElemEvent(FilterEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnFilterExEvent_TaskEvent(FilterEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnFilterExEvent_PublicEvent(FilterEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnFilterExEvent_PrivateEvent(FilterEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnFilterExEvent_ParentWinRefreshEvent(FilterEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMinutesboxExEvent_L_Click(MinutesboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMinutesboxExEvent_R_Click(MinutesboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMinutesboxExEvent_L_Down(MinutesboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMinutesboxExEvent_R_Down(MinutesboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMinutesboxExEvent_L_Up(MinutesboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMinutesboxExEvent_R_Up(MinutesboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMinutesboxExEvent_L_DClick(MinutesboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMinutesboxExEvent_R_DClick(MinutesboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMinutesboxExEvent_MouseMove(MinutesboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMinutesboxExEvent_MouseEnterLeave(MinutesboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMinutesboxExEvent_MouseWheel(MinutesboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMinutesboxExEvent_keyEvent(MinutesboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMinutesboxExEvent_CharInput(MinutesboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnMinutesboxExEvent_Focus(MinutesboxEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMinutesboxExEvent_Other(MinutesboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMinutesboxExEvent_ElemEvent(MinutesboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnMinutesboxExEvent_TaskEvent(MinutesboxEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnMinutesboxExEvent_PublicEvent(MinutesboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnMinutesboxExEvent_PrivateEvent(MinutesboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnMinutesboxExEvent_ParentWinRefreshEvent(MinutesboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSelectthefolderExEvent_L_Click(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSelectthefolderExEvent_R_Click(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSelectthefolderExEvent_L_Down(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSelectthefolderExEvent_R_Down(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSelectthefolderExEvent_L_Up(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSelectthefolderExEvent_R_Up(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSelectthefolderExEvent_L_DClick(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSelectthefolderExEvent_R_DClick(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSelectthefolderExEvent_MouseMove(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSelectthefolderExEvent_MouseEnterLeave(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSelectthefolderExEvent_MouseWheel(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSelectthefolderExEvent_keyEvent(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSelectthefolderExEvent_CharInput(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnSelectthefolderExEvent_Focus(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSelectthefolderExEvent_Other(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSelectthefolderExEvent_ElemEvent(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnSelectthefolderExEvent_TaskEvent(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnSelectthefolderExEvent_PublicEvent(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnSelectthefolderExEvent_PrivateEvent(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnSelectthefolderExEvent_ParentWinRefreshEvent(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 子夹将变
		*
		*子夹即将改变触发本事件返回0容许改变,非0阻止
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param TabIndex 子夹索引 索引从0开始
		* @param RelatedTab 因果子夹 暂无参数说明
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		*/
		virtual AutoInt OnSelectthefolderExEvent_Tab_WillChange(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt TabIndex, AutoInt RelatedTab, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 子夹已变
		*
		*子夹已经改变后触发本事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param TabIndex 当前子夹 暂无参数说明
		* @param RelatedTab 因果子夹 暂无参数说明
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		*/
		virtual AutoInt OnSelectthefolderExEvent_Tab_Changed(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt TabIndex, AutoInt RelatedTab, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 子夹左键单击
		*
		*子夹事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param TabIndex 子夹索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSelectthefolderExEvent_Tab_L_Click(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt TabIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 子夹右键单击
		*
		*子夹事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param TabIndex 子夹索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSelectthefolderExEvent_Tab_R_Click(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt TabIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 子夹鼠标左键放开
		*
		*子夹事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param TabIndex 子夹索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSelectthefolderExEvent_Tab_L_Up(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt TabIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 子夹鼠标右键放开
		*
		*子夹事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param TabIndex 子夹索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSelectthefolderExEvent_Tab_R_Up(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt TabIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 子夹鼠标左键按下
		*
		*子夹事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param TabIndex 子夹索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSelectthefolderExEvent_Tab_L_Down(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt TabIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 子夹鼠标右键按下
		*
		*子夹事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param TabIndex 子夹索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSelectthefolderExEvent_Tab_R_Down(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt TabIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 子夹左键双击
		*
		*子夹事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param TabIndex 子夹索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSelectthefolderExEvent_Tab_L_DClick(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt TabIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 子夹右键双击
		*
		*子夹事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param TabIndex 子夹索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSelectthefolderExEvent_Tab_R_DClick(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt TabIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 子夹鼠标进入
		*
		*子夹事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param TabIndex 子夹索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSelectthefolderExEvent_Tab_MouseIn(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt TabIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 子夹鼠标离开
		*
		*子夹事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param TabIndex 子夹索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSelectthefolderExEvent_Tab_MouseOut(SelectthefolderEx_P EventControl, AutoInt EventMsg, AutoInt TabIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPictureBoxExEvent_L_Click(PictureBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPictureBoxExEvent_R_Click(PictureBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPictureBoxExEvent_L_Down(PictureBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPictureBoxExEvent_R_Down(PictureBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPictureBoxExEvent_L_Up(PictureBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPictureBoxExEvent_R_Up(PictureBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPictureBoxExEvent_L_DClick(PictureBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPictureBoxExEvent_R_DClick(PictureBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPictureBoxExEvent_MouseMove(PictureBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnPictureBoxExEvent_MouseEnterLeave(PictureBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPictureBoxExEvent_MouseWheel(PictureBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPictureBoxExEvent_keyEvent(PictureBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPictureBoxExEvent_CharInput(PictureBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnPictureBoxExEvent_Focus(PictureBoxEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnPictureBoxExEvent_Other(PictureBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnPictureBoxExEvent_ElemEvent(PictureBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnPictureBoxExEvent_TaskEvent(PictureBoxEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnPictureBoxExEvent_PublicEvent(PictureBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnPictureBoxExEvent_PrivateEvent(PictureBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnPictureBoxExEvent_ParentWinRefreshEvent(PictureBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnLabelExEvent_L_Click(LabelEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnLabelExEvent_R_Click(LabelEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnLabelExEvent_L_Down(LabelEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnLabelExEvent_R_Down(LabelEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnLabelExEvent_L_Up(LabelEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnLabelExEvent_R_Up(LabelEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnLabelExEvent_L_DClick(LabelEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnLabelExEvent_R_DClick(LabelEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnLabelExEvent_MouseMove(LabelEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnLabelExEvent_MouseEnterLeave(LabelEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnLabelExEvent_MouseWheel(LabelEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnLabelExEvent_keyEvent(LabelEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnLabelExEvent_CharInput(LabelEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnLabelExEvent_Focus(LabelEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnLabelExEvent_Other(LabelEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnLabelExEvent_ElemEvent(LabelEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnLabelExEvent_TaskEvent(LabelEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnLabelExEvent_PublicEvent(LabelEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnLabelExEvent_PrivateEvent(LabelEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnLabelExEvent_ParentWinRefreshEvent(LabelEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnButtonExEvent_L_Click(ButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnButtonExEvent_R_Click(ButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnButtonExEvent_L_Down(ButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnButtonExEvent_R_Down(ButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnButtonExEvent_L_Up(ButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnButtonExEvent_R_Up(ButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnButtonExEvent_L_DClick(ButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnButtonExEvent_R_DClick(ButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnButtonExEvent_MouseMove(ButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnButtonExEvent_MouseEnterLeave(ButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnButtonExEvent_MouseWheel(ButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnButtonExEvent_keyEvent(ButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnButtonExEvent_CharInput(ButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnButtonExEvent_Focus(ButtonEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnButtonExEvent_Other(ButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnButtonExEvent_ElemEvent(ButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnButtonExEvent_TaskEvent(ButtonEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnButtonExEvent_PublicEvent(ButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnButtonExEvent_PrivateEvent(ButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnButtonExEvent_ParentWinRefreshEvent(ButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnImagebuttonExEvent_L_Click(ImagebuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnImagebuttonExEvent_R_Click(ImagebuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnImagebuttonExEvent_L_Down(ImagebuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnImagebuttonExEvent_R_Down(ImagebuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnImagebuttonExEvent_L_Up(ImagebuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnImagebuttonExEvent_R_Up(ImagebuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnImagebuttonExEvent_L_DClick(ImagebuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnImagebuttonExEvent_R_DClick(ImagebuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnImagebuttonExEvent_MouseMove(ImagebuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnImagebuttonExEvent_MouseEnterLeave(ImagebuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnImagebuttonExEvent_MouseWheel(ImagebuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnImagebuttonExEvent_keyEvent(ImagebuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnImagebuttonExEvent_CharInput(ImagebuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnImagebuttonExEvent_Focus(ImagebuttonEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnImagebuttonExEvent_Other(ImagebuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnImagebuttonExEvent_ElemEvent(ImagebuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnImagebuttonExEvent_TaskEvent(ImagebuttonEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnImagebuttonExEvent_PublicEvent(ImagebuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnImagebuttonExEvent_PrivateEvent(ImagebuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnImagebuttonExEvent_ParentWinRefreshEvent(ImagebuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperbuttonExEvent_L_Click(SuperbuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperbuttonExEvent_R_Click(SuperbuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperbuttonExEvent_L_Down(SuperbuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperbuttonExEvent_R_Down(SuperbuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperbuttonExEvent_L_Up(SuperbuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperbuttonExEvent_R_Up(SuperbuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperbuttonExEvent_L_DClick(SuperbuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperbuttonExEvent_R_DClick(SuperbuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperbuttonExEvent_MouseMove(SuperbuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSuperbuttonExEvent_MouseEnterLeave(SuperbuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperbuttonExEvent_MouseWheel(SuperbuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperbuttonExEvent_keyEvent(SuperbuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperbuttonExEvent_CharInput(SuperbuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnSuperbuttonExEvent_Focus(SuperbuttonEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSuperbuttonExEvent_Other(SuperbuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSuperbuttonExEvent_ElemEvent(SuperbuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnSuperbuttonExEvent_TaskEvent(SuperbuttonEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnSuperbuttonExEvent_PublicEvent(SuperbuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnSuperbuttonExEvent_PrivateEvent(SuperbuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnSuperbuttonExEvent_ParentWinRefreshEvent(SuperbuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_L_Click(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_R_Click(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_L_Down(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_R_Down(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_L_Up(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_R_Up(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_L_DClick(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_R_DClick(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_MouseMove(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_MouseEnterLeave(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_MouseWheel(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_keyEvent(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_CharInput(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_Focus(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_Other(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_ElemEvent(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_TaskEvent(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_PublicEvent(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_PrivateEvent(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_ParentWinRefreshEvent(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 按钮左键单击
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_Button_L_Click(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮右键单击
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_Button_R_Click(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮鼠标左键放开
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_Button_L_Up(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮鼠标右键放开
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_Button_R_Up(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮鼠标左键按下
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_Button_L_Down(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮鼠标右键按下
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_Button_R_Down(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮左键双击
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_Button_L_DClick(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮右键双击
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_Button_R_DClick(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮鼠标进入
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_Button_MouseIn(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮鼠标离开
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMultifunctionButtonExEvent_Button_MouseOut(MultifunctionButtonEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRadiobuttonExEvent_L_Click(RadiobuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRadiobuttonExEvent_R_Click(RadiobuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRadiobuttonExEvent_L_Down(RadiobuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRadiobuttonExEvent_R_Down(RadiobuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRadiobuttonExEvent_L_Up(RadiobuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRadiobuttonExEvent_R_Up(RadiobuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRadiobuttonExEvent_L_DClick(RadiobuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRadiobuttonExEvent_R_DClick(RadiobuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRadiobuttonExEvent_MouseMove(RadiobuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnRadiobuttonExEvent_MouseEnterLeave(RadiobuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRadiobuttonExEvent_MouseWheel(RadiobuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRadiobuttonExEvent_keyEvent(RadiobuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRadiobuttonExEvent_CharInput(RadiobuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnRadiobuttonExEvent_Focus(RadiobuttonEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnRadiobuttonExEvent_Other(RadiobuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnRadiobuttonExEvent_ElemEvent(RadiobuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnRadiobuttonExEvent_TaskEvent(RadiobuttonEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnRadiobuttonExEvent_PublicEvent(RadiobuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnRadiobuttonExEvent_PrivateEvent(RadiobuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnRadiobuttonExEvent_ParentWinRefreshEvent(RadiobuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 选中状态改变
		*
		*选中状态改变
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param SelectType 新状态 新状态
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnRadiobuttonExEvent_SelectChanged(RadiobuttonEx_P EventControl, AutoInt EventMsg, AutoInt SelectType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChoiceboxExEvent_L_Click(ChoiceboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChoiceboxExEvent_R_Click(ChoiceboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChoiceboxExEvent_L_Down(ChoiceboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChoiceboxExEvent_R_Down(ChoiceboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChoiceboxExEvent_L_Up(ChoiceboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChoiceboxExEvent_R_Up(ChoiceboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChoiceboxExEvent_L_DClick(ChoiceboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChoiceboxExEvent_R_DClick(ChoiceboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChoiceboxExEvent_MouseMove(ChoiceboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnChoiceboxExEvent_MouseEnterLeave(ChoiceboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChoiceboxExEvent_MouseWheel(ChoiceboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChoiceboxExEvent_keyEvent(ChoiceboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChoiceboxExEvent_CharInput(ChoiceboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnChoiceboxExEvent_Focus(ChoiceboxEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnChoiceboxExEvent_Other(ChoiceboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnChoiceboxExEvent_ElemEvent(ChoiceboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnChoiceboxExEvent_TaskEvent(ChoiceboxEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnChoiceboxExEvent_PublicEvent(ChoiceboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnChoiceboxExEvent_PrivateEvent(ChoiceboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnChoiceboxExEvent_ParentWinRefreshEvent(ChoiceboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 选中状态改变
		*
		*选中状态改变
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param SelectType 新状态 新状态
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnChoiceboxExEvent_SelectChanged(ChoiceboxEx_P EventControl, AutoInt EventMsg, AutoInt SelectType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnProgressbarExEvent_L_Click(ProgressbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnProgressbarExEvent_R_Click(ProgressbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnProgressbarExEvent_L_Down(ProgressbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnProgressbarExEvent_R_Down(ProgressbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnProgressbarExEvent_L_Up(ProgressbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnProgressbarExEvent_R_Up(ProgressbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnProgressbarExEvent_L_DClick(ProgressbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnProgressbarExEvent_R_DClick(ProgressbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnProgressbarExEvent_MouseMove(ProgressbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnProgressbarExEvent_MouseEnterLeave(ProgressbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnProgressbarExEvent_MouseWheel(ProgressbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnProgressbarExEvent_keyEvent(ProgressbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnProgressbarExEvent_CharInput(ProgressbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnProgressbarExEvent_Focus(ProgressbarEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnProgressbarExEvent_Other(ProgressbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnProgressbarExEvent_ElemEvent(ProgressbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnProgressbarExEvent_TaskEvent(ProgressbarEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnProgressbarExEvent_PublicEvent(ProgressbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnProgressbarExEvent_PrivateEvent(ProgressbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnProgressbarExEvent_ParentWinRefreshEvent(ProgressbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSliderbarExEvent_L_Click(SliderbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSliderbarExEvent_R_Click(SliderbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSliderbarExEvent_L_Down(SliderbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSliderbarExEvent_R_Down(SliderbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSliderbarExEvent_L_Up(SliderbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSliderbarExEvent_R_Up(SliderbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSliderbarExEvent_L_DClick(SliderbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSliderbarExEvent_R_DClick(SliderbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSliderbarExEvent_MouseMove(SliderbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSliderbarExEvent_MouseEnterLeave(SliderbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSliderbarExEvent_MouseWheel(SliderbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSliderbarExEvent_keyEvent(SliderbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSliderbarExEvent_CharInput(SliderbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnSliderbarExEvent_Focus(SliderbarEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSliderbarExEvent_Other(SliderbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSliderbarExEvent_ElemEvent(SliderbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnSliderbarExEvent_TaskEvent(SliderbarEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnSliderbarExEvent_PublicEvent(SliderbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnSliderbarExEvent_PrivateEvent(SliderbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnSliderbarExEvent_ParentWinRefreshEvent(SliderbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 进度位置改变
		*
		*进度位置改变
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Position 进度 进度
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSliderbarExEvent_PositionChanged(SliderbarEx_P EventControl, AutoInt EventMsg, AutoInt Position, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollbarExEvent_L_Click(ScrollbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollbarExEvent_R_Click(ScrollbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollbarExEvent_L_Down(ScrollbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollbarExEvent_R_Down(ScrollbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollbarExEvent_L_Up(ScrollbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollbarExEvent_R_Up(ScrollbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollbarExEvent_L_DClick(ScrollbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollbarExEvent_R_DClick(ScrollbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollbarExEvent_MouseMove(ScrollbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnScrollbarExEvent_MouseEnterLeave(ScrollbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollbarExEvent_MouseWheel(ScrollbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollbarExEvent_keyEvent(ScrollbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollbarExEvent_CharInput(ScrollbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnScrollbarExEvent_Focus(ScrollbarEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnScrollbarExEvent_Other(ScrollbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnScrollbarExEvent_ElemEvent(ScrollbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnScrollbarExEvent_TaskEvent(ScrollbarEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnScrollbarExEvent_PublicEvent(ScrollbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnScrollbarExEvent_PrivateEvent(ScrollbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnScrollbarExEvent_ParentWinRefreshEvent(ScrollbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 进度位置改变
		*
		*进度位置改变
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Position 进度 进度
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnScrollbarExEvent_PositionChanged(ScrollbarEx_P EventControl, AutoInt EventMsg, AutoInt Position, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnEditboxExEvent_L_Click(EditboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnEditboxExEvent_R_Click(EditboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnEditboxExEvent_L_Down(EditboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnEditboxExEvent_R_Down(EditboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnEditboxExEvent_L_Up(EditboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnEditboxExEvent_R_Up(EditboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnEditboxExEvent_L_DClick(EditboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnEditboxExEvent_R_DClick(EditboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnEditboxExEvent_MouseMove(EditboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnEditboxExEvent_MouseEnterLeave(EditboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnEditboxExEvent_MouseWheel(EditboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnEditboxExEvent_keyEvent(EditboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnEditboxExEvent_CharInput(EditboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnEditboxExEvent_Focus(EditboxEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnEditboxExEvent_Other(EditboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnEditboxExEvent_ElemEvent(EditboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnEditboxExEvent_TaskEvent(EditboxEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnEditboxExEvent_PublicEvent(EditboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnEditboxExEvent_PrivateEvent(EditboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnEditboxExEvent_ParentWinRefreshEvent(EditboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 内容被改变
		*
		*当编辑框内改变时触发此事件,可通过参数1改变模式来判断改变因由做不同的处理.改变模式参考ContentChanged_前缀常量
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ChangedType 改变模式 暂无参数说明
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnEditboxExEvent_ContentChanged(EditboxEx_P EventControl, AutoInt EventMsg, AutoInt ChangedType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnComboboxExEvent_L_Click(ComboboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnComboboxExEvent_R_Click(ComboboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnComboboxExEvent_L_Down(ComboboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnComboboxExEvent_R_Down(ComboboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnComboboxExEvent_L_Up(ComboboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnComboboxExEvent_R_Up(ComboboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnComboboxExEvent_L_DClick(ComboboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnComboboxExEvent_R_DClick(ComboboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnComboboxExEvent_MouseMove(ComboboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnComboboxExEvent_MouseEnterLeave(ComboboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnComboboxExEvent_MouseWheel(ComboboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnComboboxExEvent_keyEvent(ComboboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnComboboxExEvent_CharInput(ComboboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnComboboxExEvent_Focus(ComboboxEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnComboboxExEvent_Other(ComboboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnComboboxExEvent_ElemEvent(ComboboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnComboboxExEvent_TaskEvent(ComboboxEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnComboboxExEvent_PublicEvent(ComboboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnComboboxExEvent_PrivateEvent(ComboboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnComboboxExEvent_ParentWinRefreshEvent(ComboboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 内容被改变
		*
		*当编辑框内改变时触发此事件,可通过参数1改变模式来判断改变因由做不同的处理.改变模式参考ContentChanged_前缀常量
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ChangedType 改变模式 暂无参数说明
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnComboboxExEvent_ContentChanged(ComboboxEx_P EventControl, AutoInt EventMsg, AutoInt ChangedType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 列表将被选择
		*
		*列表被选择,注意此事件触发时并不会直接改变组合框内容属性而是在本事件触发完毕返回0后改变内容返回非0时将阻止改变内容若要在此事件内获取组合框将变内容请用对应命令获取参数被选择项目的列表项标题
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 被选择项目 被选择项目索引,起始值从0开始
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnComboboxExEvent_CurrentItemWillChange(ComboboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 列表被选择
		*
		*列表被选择,注意此事件触发时组合框内容以改变
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 被选择项目 被选择项目索引,起始值从0开始
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnComboboxExEvent_CurrentItemChanged(ComboboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnToolbarExEvent_L_Click(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnToolbarExEvent_R_Click(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnToolbarExEvent_L_Down(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnToolbarExEvent_R_Down(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnToolbarExEvent_L_Up(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnToolbarExEvent_R_Up(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnToolbarExEvent_L_DClick(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnToolbarExEvent_R_DClick(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnToolbarExEvent_MouseMove(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnToolbarExEvent_MouseEnterLeave(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnToolbarExEvent_MouseWheel(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnToolbarExEvent_keyEvent(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnToolbarExEvent_CharInput(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnToolbarExEvent_Focus(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnToolbarExEvent_Other(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnToolbarExEvent_ElemEvent(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnToolbarExEvent_TaskEvent(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnToolbarExEvent_PublicEvent(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnToolbarExEvent_PrivateEvent(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnToolbarExEvent_ParentWinRefreshEvent(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 按钮左键单击
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnToolbarExEvent_Button_L_Click(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮右键单击
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnToolbarExEvent_Button_R_Click(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮鼠标左键放开
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnToolbarExEvent_Button_L_Up(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮鼠标右键放开
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnToolbarExEvent_Button_R_Up(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮鼠标左键按下
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnToolbarExEvent_Button_L_Down(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮鼠标右键按下
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnToolbarExEvent_Button_R_Down(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮左键双击
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnToolbarExEvent_Button_L_DClick(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮右键双击
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnToolbarExEvent_Button_R_DClick(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮鼠标进入
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnToolbarExEvent_Button_MouseIn(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 按钮鼠标离开
		*
		*按钮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ButtonIndex 按钮索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnToolbarExEvent_Button_MouseOut(ToolbarEx_P EventControl, AutoInt EventMsg, AutoInt ButtonIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnListboxExEvent_L_Click(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnListboxExEvent_R_Click(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnListboxExEvent_L_Down(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnListboxExEvent_R_Down(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }

		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnListboxExEvent_L_Up(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnListboxExEvent_R_Up(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnListboxExEvent_L_DClick(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnListboxExEvent_R_DClick(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnListboxExEvent_MouseMove(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnListboxExEvent_MouseEnterLeave(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnListboxExEvent_MouseWheel(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnListboxExEvent_keyEvent(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnListboxExEvent_CharInput(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnListboxExEvent_Focus(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnListboxExEvent_Other(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnListboxExEvent_ElemEvent(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnListboxExEvent_TaskEvent(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnListboxExEvent_PublicEvent(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnListboxExEvent_PrivateEvent(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnListboxExEvent_ParentWinRefreshEvent(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 项目左键单击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnListboxExEvent_Item_L_Click(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目右键单击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnListboxExEvent_Item_R_Click(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标左键放开
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnListboxExEvent_Item_L_Up(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标右键放开
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnListboxExEvent_Item_R_Up(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标左键按下
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnListboxExEvent_Item_L_Down(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标右键按下
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnListboxExEvent_Item_R_Down(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目左键双击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnListboxExEvent_Item_L_DClick(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目右键双击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnListboxExEvent_Item_R_DClick(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标进入
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnListboxExEvent_Item_MouseIn(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标离开
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnListboxExEvent_Item_MouseOut(ListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnIcoListboxExEvent_L_Click(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnIcoListboxExEvent_R_Click(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnIcoListboxExEvent_L_Down(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnIcoListboxExEvent_R_Down(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnIcoListboxExEvent_L_Up(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnIcoListboxExEvent_R_Up(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnIcoListboxExEvent_L_DClick(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnIcoListboxExEvent_R_DClick(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnIcoListboxExEvent_MouseMove(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnIcoListboxExEvent_MouseEnterLeave(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnIcoListboxExEvent_MouseWheel(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnIcoListboxExEvent_keyEvent(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnIcoListboxExEvent_CharInput(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnIcoListboxExEvent_Focus(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnIcoListboxExEvent_Other(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnIcoListboxExEvent_ElemEvent(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnIcoListboxExEvent_TaskEvent(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnIcoListboxExEvent_PublicEvent(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnIcoListboxExEvent_PrivateEvent(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnIcoListboxExEvent_ParentWinRefreshEvent(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 项目左键单击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnIcoListboxExEvent_Item_L_Click(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目右键单击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnIcoListboxExEvent_Item_R_Click(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标左键放开
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnIcoListboxExEvent_Item_L_Up(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标右键放开
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnIcoListboxExEvent_Item_R_Up(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标左键按下
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnIcoListboxExEvent_Item_L_Down(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标右键按下
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnIcoListboxExEvent_Item_R_Down(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目左键双击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnIcoListboxExEvent_Item_L_DClick(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目右键双击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnIcoListboxExEvent_Item_R_DClick(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标进入
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnIcoListboxExEvent_Item_MouseIn(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标离开
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnIcoListboxExEvent_Item_MouseOut(IcoListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperListboxExEvent_L_Click(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperListboxExEvent_R_Click(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperListboxExEvent_L_Down(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperListboxExEvent_R_Down(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperListboxExEvent_L_Up(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperListboxExEvent_R_Up(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperListboxExEvent_L_DClick(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperListboxExEvent_R_DClick(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperListboxExEvent_MouseMove(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSuperListboxExEvent_MouseEnterLeave(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperListboxExEvent_MouseWheel(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperListboxExEvent_keyEvent(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperListboxExEvent_CharInput(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnSuperListboxExEvent_Focus(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSuperListboxExEvent_Other(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSuperListboxExEvent_ElemEvent(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnSuperListboxExEvent_TaskEvent(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnSuperListboxExEvent_PublicEvent(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnSuperListboxExEvent_PrivateEvent(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnSuperListboxExEvent_ParentWinRefreshEvent(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 项目左键单击
		*
		*表项事件区分是否命中表列,处理需要区分事件来源参数
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ElemeIndex 元素索引 暂无
		* @param EventSource 事件来源 事件来源有2种情况表项事件(在命中表项时无论是否命中表列时均会触发)表列事件(仅在命中表列时触发)当命中表列时会同时触发表项事件
		*/
		virtual AutoInt OnSuperListboxExEvent_Item_L_Click(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ElemeIndex, AutoInt EventSource) { return NULL; }
		/*
		* @brief 项目右键单击
		*
		*表项事件区分是否命中表列,处理需要区分事件来源参数
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ElemeIndex 元素索引 暂无
		* @param EventSource 事件来源 事件来源有2种情况表项事件(在命中表项时无论是否命中表列时均会触发)表列事件(仅在命中表列时触发)当命中表列时会同时触发表项事件
		*/
		virtual AutoInt OnSuperListboxExEvent_Item_R_Click(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ElemeIndex, AutoInt EventSource) { return NULL; }
		/*
		* @brief 项目鼠标左键放开
		*
		*表项事件区分是否命中表列,处理需要区分事件来源参数
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ElemeIndex 元素索引 暂无
		* @param EventSource 事件来源 事件来源有2种情况表项事件(在命中表项时无论是否命中表列时均会触发)表列事件(仅在命中表列时触发)当命中表列时会同时触发表项事件
		*/
		virtual AutoInt OnSuperListboxExEvent_Item_L_Up(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ElemeIndex, AutoInt EventSource) { return NULL; }
		/*
		* @brief 项目鼠标右键放开
		*
		*表项事件区分是否命中表列,处理需要区分事件来源参数
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ElemeIndex 元素索引 暂无
		* @param EventSource 事件来源 事件来源有2种情况表项事件(在命中表项时无论是否命中表列时均会触发)表列事件(仅在命中表列时触发)当命中表列时会同时触发表项事件
		*/
		virtual AutoInt OnSuperListboxExEvent_Item_R_Up(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ElemeIndex, AutoInt EventSource) { return NULL; }
		/*
		* @brief 项目鼠标左键按下
		*
		*表项事件区分是否命中表列,处理需要区分事件来源参数
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ElemeIndex 元素索引 暂无
		* @param EventSource 事件来源 事件来源有2种情况表项事件(在命中表项时无论是否命中表列时均会触发)表列事件(仅在命中表列时触发)当命中表列时会同时触发表项事件
		*/
		virtual AutoInt OnSuperListboxExEvent_Item_L_Down(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ElemeIndex, AutoInt EventSource) { return NULL; }
		/*
		* @brief 项目鼠标右键按下
		*
		*表项事件区分是否命中表列,处理需要区分事件来源参数
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ElemeIndex 元素索引 暂无
		* @param EventSource 事件来源 事件来源有2种情况表项事件(在命中表项时无论是否命中表列时均会触发)表列事件(仅在命中表列时触发)当命中表列时会同时触发表项事件
		*/
		virtual AutoInt OnSuperListboxExEvent_Item_R_Down(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ElemeIndex, AutoInt EventSource) { return NULL; }
		/*
		* @brief 项目左键双击
		*
		*表项事件区分是否命中表列,处理需要区分事件来源参数
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ElemeIndex 元素索引 暂无
		* @param EventSource 事件来源 事件来源有2种情况表项事件(在命中表项时无论是否命中表列时均会触发)表列事件(仅在命中表列时触发)当命中表列时会同时触发表项事件
		*/
		virtual AutoInt OnSuperListboxExEvent_Item_L_DClick(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ElemeIndex, AutoInt EventSource) { return NULL; }
		/*
		* @brief 项目右键双击
		*
		*表项事件区分是否命中表列,处理需要区分事件来源参数
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ElemeIndex 元素索引 暂无
		* @param EventSource 事件来源 事件来源有2种情况表项事件(在命中表项时无论是否命中表列时均会触发)表列事件(仅在命中表列时触发)当命中表列时会同时触发表项事件
		*/
		virtual AutoInt OnSuperListboxExEvent_Item_R_DClick(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ElemeIndex, AutoInt EventSource) { return NULL; }
		/*
		* @brief 项目鼠标进入
		*
		*表项事件区分是否命中表列,处理需要区分事件来源参数
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ElemeIndex 元素索引 暂无
		* @param EventSource 事件来源 事件来源有2种情况表项事件(在命中表项时无论是否命中表列时均会触发)表列事件(仅在命中表列时触发)当命中表列时会同时触发表项事件
		*/
		virtual AutoInt OnSuperListboxExEvent_Item_MouseIn(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ElemeIndex, AutoInt EventSource) { return NULL; }
		/*
		* @brief 项目鼠标离开
		*
		*表项事件区分是否命中表列,处理需要区分事件来源参数
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ElemeIndex 元素索引 暂无
		* @param EventSource 事件来源 事件来源有2种情况表项事件(在命中表项时无论是否命中表列时均会触发)表列事件(仅在命中表列时触发)当命中表列时会同时触发表项事件
		*/
		virtual AutoInt OnSuperListboxExEvent_Item_MouseOut(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ElemeIndex, AutoInt EventSource) { return NULL; }
		/*
		* @brief 项目将进入编辑
		*
		*项目进入编辑 返回 1阻止编辑
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param EnterMode 进入模式 参数记录项目进入编辑的触发原因
		* @param EditContent 项目内容 内容指针可以使用pts命令获取内容做校验
		*/
		virtual AutoInt OnSuperListboxExEvent_Item_WillEnterEdit(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt EnterMode, AutoInt EditContent) { return NULL; }
		/*
		* @brief 项目将退出编辑
		*
		*项目退出编辑 返回 1阻止结果
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ExitMode 退出模式 参数记录项目退出编辑的触发原因
		* @param EditContent 项目内容 内容指针可以使用pts命令获取内容做校验
		*/
		virtual AutoInt OnSuperListboxExEvent_Item_WillExitEdit(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ExitMode, AutoInt EditContent) { return NULL; }
		/*
		* @brief 项目进入编辑
		*
		*项目进入编辑
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param EnterMode 进入模式 参数记录项目进入编辑的触发原因
		* @param EditContent 项目内容 内容指针可以使用pts命令获取内容做校验
		*/
		virtual AutoInt OnSuperListboxExEvent_Item_EnterEdit(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt EnterMode, AutoInt EditContent) { return NULL; }
		/*
		* @brief 项目退出编辑
		*
		*项目退出编辑
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ExitMode 退出模式 参数记录项目退出编辑的触发原因
		* @param EditContent 项目内容 内容指针可以使用pts命令获取内容做校验
		*/
		virtual AutoInt OnSuperListboxExEvent_Item_ExitEdit(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ExitMode, AutoInt EditContent) { return NULL; }
		/*
		* @brief 项目尺寸被改变
		*
		*项目尺寸被改变
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ChangedType 调整类型 调整类型
		* @param ChangValue 新尺寸 新尺寸
		*/
		virtual AutoInt OnSuperListboxExEvent_Item_SizeChanged(SuperListboxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ChangedType, AutoInt ChangValue) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnTreeListExEvent_L_Click(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnTreeListExEvent_R_Click(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnTreeListExEvent_L_Down(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnTreeListExEvent_R_Down(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnTreeListExEvent_L_Up(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnTreeListExEvent_R_Up(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnTreeListExEvent_L_DClick(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnTreeListExEvent_R_DClick(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnTreeListExEvent_MouseMove(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnTreeListExEvent_MouseEnterLeave(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnTreeListExEvent_MouseWheel(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnTreeListExEvent_keyEvent(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnTreeListExEvent_CharInput(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnTreeListExEvent_Focus(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnTreeListExEvent_Other(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnTreeListExEvent_ElemEvent(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnTreeListExEvent_TaskEvent(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnTreeListExEvent_PublicEvent(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnTreeListExEvent_PrivateEvent(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnTreeListExEvent_ParentWinRefreshEvent(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 项目左键单击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnTreeListExEvent_Item_L_Click(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目右键单击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnTreeListExEvent_Item_R_Click(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标左键放开
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnTreeListExEvent_Item_L_Up(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标右键放开
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnTreeListExEvent_Item_R_Up(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标左键按下
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnTreeListExEvent_Item_L_Down(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标右键按下
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnTreeListExEvent_Item_R_Down(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目左键双击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnTreeListExEvent_Item_L_DClick(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目右键双击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnTreeListExEvent_Item_R_DClick(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标进入
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnTreeListExEvent_Item_MouseIn(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标离开
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnTreeListExEvent_Item_MouseOut(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目折叠将改变
		*
		*项目折叠将改变
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnTreeListExEvent_Item_FoldChanged(TreeListEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWebBrowserExEvent_L_Click(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWebBrowserExEvent_R_Click(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWebBrowserExEvent_L_Down(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWebBrowserExEvent_R_Down(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWebBrowserExEvent_L_Up(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWebBrowserExEvent_R_Up(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWebBrowserExEvent_L_DClick(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWebBrowserExEvent_R_DClick(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWebBrowserExEvent_MouseMove(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnWebBrowserExEvent_MouseEnterLeave(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWebBrowserExEvent_MouseWheel(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWebBrowserExEvent_keyEvent(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnWebBrowserExEvent_CharInput(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnWebBrowserExEvent_Focus(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnWebBrowserExEvent_Other(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnWebBrowserExEvent_ElemEvent(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnWebBrowserExEvent_TaskEvent(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnWebBrowserExEvent_PublicEvent(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnWebBrowserExEvent_PrivateEvent(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnWebBrowserExEvent_ParentWinRefreshEvent(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 开始载入
		*
		*开始载入
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Url 地址 获取事件的文本参数可以用 ptu或PTA (浏览框EX1.执行命令 (999, Url, 编码, 0, 0))
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param EncodType 相关文本参数编码
		*/
		virtual AutoInt OnWebBrowserExEvent_Navigation(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt Url, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt EncodType) { return NULL; }
		/*
		* @brief 即将打开新窗口
		*
		*即将打开新窗口
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Url 地址 获取事件的文本参数可以用 ptu或PTA (浏览框EX1.执行命令 (999, Url, 编码, 0, 0))
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param EncodType 相关文本参数编码
		*/
		virtual AutoInt OnWebBrowserExEvent_CreateView(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt Url, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt EncodType) { return NULL; }
		/*
		* @brief Url改变
		*
		*Url改变
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Url 地址 获取事件的文本参数可以用 ptu或PTA (浏览框EX1.执行命令 (999, Url, 编码, 0, 0))
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param EncodType 相关文本参数编码
		*/
		virtual AutoInt OnWebBrowserExEvent_URLChanged(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt Url, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt EncodType) { return NULL; }
		/*
		* @brief 标题改变
		*
		*标题改变
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Title 标题 获取事件的文本参数可以用 ptu或PTA (浏览框EX1.执行命令 (999, Url, 编码, 0, 0))
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param EncodType 相关文本参数编码
		*/
		virtual AutoInt OnWebBrowserExEvent_TitleChanged(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt Title, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt EncodType) { return NULL; }
		/*
		* @brief 文档就绪
		*
		*文档就绪
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param EncodType 相关文本参数编码
		*/
		virtual AutoInt OnWebBrowserExEvent_DocumentReady(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt EncodType) { return NULL; }
		/*
		* @brief 载入完成
		*
		*载入完成
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Url 地址 获取事件的文本参数可以用 ptu或PTA (浏览框EX1.执行命令 (999, Url, 编码, 0, 0))
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param EncodType 相关文本参数编码
		*/
		virtual AutoInt OnWebBrowserExEvent_LoadingFinish(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt Url, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt EncodType) { return NULL; }
		/*
		* @brief 文件下载
		*
		*文件下载
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Url 地址 获取事件的文本参数可以用 ptu或PTA (浏览框EX1.执行命令 (999, Url, 编码, 0, 0))
		* @param ContentLength 长度 Reserve1
		* @param Downparam 任务参数 Reserve2
		* @param EncodType 相关文本参数编码
		*/
		virtual AutoInt OnWebBrowserExEvent_Download(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt Url, AutoInt ContentLength, AutoInt DownParam, AutoInt EncodType) { return NULL; }
		/*
		* @brief 调用提示框
		*
		*调用提示框
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Msg 提示文本 获取事件的文本参数可以用 ptu或PTA (浏览框EX1.执行命令 (999, Url, 编码, 0, 0))
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param EncodType 相关文本参数编码
		*/
		virtual AutoInt OnWebBrowserExEvent_AlertBox(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt Msg, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt EncodType) { return NULL; }
		/*
		* @brief 调用信息框
		*
		*调用信息框
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Msg 提示文本 获取事件的文本参数可以用 ptu或PTA (浏览框EX1.执行命令 (999, Url, 编码, 0, 0))
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param EncodType 相关文本参数编码
		*/
		virtual AutoInt OnWebBrowserExEvent_ConfirmBox(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt Msg, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt EncodType) { return NULL; }
		/*
		* @brief 调用输入框
		*
		*调用输入框
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Msg 提示文本 获取事件的文本参数可以用 ptu或PTA (浏览框EX1.执行命令 (999, Url, 编码, 0, 0))
		* @param DefaultResult 默认文本 获取事件的文本参数可以用 ptu或PTA (浏览框EX1.执行命令 (999, Url, 编码, 0, 0))
		* @param Result 返回文本 保留未用
		* @param EncodType 相关文本参数编码
		*/
		virtual AutoInt OnWebBrowserExEvent_PromptBox(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt Msg, AutoInt DefaultResult, AutoInt Result, AutoInt EncodType) { return NULL; }
		/*
		* @brief 资源载入开始
		*
		*url载入开始
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Url 地址 获取事件的文本参数可以用 ptu或PTA (浏览框EX1.执行命令 (999, Url, 编码, 0, 0))
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param EncodType 相关文本参数编码
		*/
		virtual AutoInt OnWebBrowserExEvent_LoadUrlBegin(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt Url, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt EncodType) { return NULL; }
		/*
		* @brief 资源载入出错
		*
		*url载入出错
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Url 获取事件的文本参数可以用 ptu或PTA (浏览框EX1.执行命令 (999, Url, 编码, 0, 0))
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param EncodType 编码 相关文本参数编码
		*/
		virtual AutoInt OnWebBrowserExEvent_LoadUrlFail(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt Url, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt EncodType) { return NULL; }
		/*
		* @brief 资源载入完毕
		*
		*url载入完毕
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Url 地址 获取事件的文本参数可以用 ptu或PTA (浏览框EX1.执行命令 (999, Url, 0, 0, 0))
		* @param Buf 数据 数据
		* @param BufLen 数据长度 数据长度
		* @param EncodType 编码 相关文本参数编码
		*/
		virtual AutoInt OnWebBrowserExEvent_LoadUrlEnd(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt Url, AutoInt Buf, AutoInt BufLen, AutoInt EncodType) { return NULL; }
		/*
		* @brief 网络响应事件
		*
		*网络响应事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Url 地址 获取事件的文本参数可以用 ptu或PTA (浏览框EX1.执行命令 (999, Url, 编码, 0, 0))
		* @param RelatedParam1 相关参数 相关参数
		* @param RelatedParam2 相关参数 相关参数
		* @param EncodType 编码 相关文本参数编码
		*/
		virtual AutoInt OnWebBrowserExEvent_NetResponse(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt Url, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt EncodType) { return NULL; }
		/*
		* @brief 响应Js命令
		*
		*响应Js命令
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Cmd 命令名
		* @param Param1 参数1
		* @param Param2 参数2
		* @param Param3 参数3
		*/
		virtual AutoInt OnWebBrowserExEvent_JsCallResponse(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt Cmd, AutoInt Param1, AutoInt Param2, AutoInt Param3) { return NULL; }
		/*
		* @brief 保留事件1
		*
		*保留事件1
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Param1 Param1
		* @param Param2 Param2
		* @param Param3 Param3
		* @param Param4 Param4
		*/
		virtual AutoInt OnWebBrowserExEvent_Reserve1(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }
		/*
			* @brief 保留事件2
			*
			*保留事件2
			* @return 返回数据格式 1001
			* @param EventControl 事件组件 参数提供事件发生组件句柄
			* @param EventMsg 事件消息值 参数提供事件消息值
			* @param Param1 Param1
			* @param Param2 Param2
			* @param Param3 Param3
			* @param Param4 Param4
			*/
		virtual AutoInt OnWebBrowserExEvent_Reserve2(WebBrowserEx_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4) { return NULL; }



		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCalendarBoxExEvent_L_Click(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCalendarBoxExEvent_R_Click(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCalendarBoxExEvent_L_Down(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCalendarBoxExEvent_R_Down(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCalendarBoxExEvent_L_Up(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCalendarBoxExEvent_R_Up(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCalendarBoxExEvent_L_DClick(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCalendarBoxExEvent_R_DClick(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCalendarBoxExEvent_MouseMove(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCalendarBoxExEvent_MouseEnterLeave(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCalendarBoxExEvent_MouseWheel(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCalendarBoxExEvent_keyEvent(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCalendarBoxExEvent_CharInput(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnCalendarBoxExEvent_Focus(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCalendarBoxExEvent_Other(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCalendarBoxExEvent_ElemEvent(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnCalendarBoxExEvent_TaskEvent(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnCalendarBoxExEvent_PublicEvent(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnCalendarBoxExEvent_PrivateEvent(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnCalendarBoxExEvent_ParentWinRefreshEvent(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 项目左键单击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCalendarBoxExEvent_Item_L_Click(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目右键单击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCalendarBoxExEvent_Item_R_Click(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标左键放开
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCalendarBoxExEvent_Item_L_Up(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标右键放开
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCalendarBoxExEvent_Item_R_Up(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标左键按下
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCalendarBoxExEvent_Item_L_Down(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标右键按下
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCalendarBoxExEvent_Item_R_Down(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目左键双击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCalendarBoxExEvent_Item_L_DClick(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目右键双击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCalendarBoxExEvent_Item_R_DClick(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标进入
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCalendarBoxExEvent_Item_MouseIn(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标离开
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCalendarBoxExEvent_Item_MouseOut(CalendarBoxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnColorPickExEvent_L_Click(ColorPickEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnColorPickExEvent_R_Click(ColorPickEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnColorPickExEvent_L_Down(ColorPickEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnColorPickExEvent_R_Down(ColorPickEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnColorPickExEvent_L_Up(ColorPickEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnColorPickExEvent_R_Up(ColorPickEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnColorPickExEvent_L_DClick(ColorPickEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnColorPickExEvent_R_DClick(ColorPickEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnColorPickExEvent_MouseMove(ColorPickEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnColorPickExEvent_MouseEnterLeave(ColorPickEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnColorPickExEvent_MouseWheel(ColorPickEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnColorPickExEvent_keyEvent(ColorPickEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnColorPickExEvent_CharInput(ColorPickEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnColorPickExEvent_Focus(ColorPickEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnColorPickExEvent_Other(ColorPickEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnColorPickExEvent_ElemEvent(ColorPickEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnColorPickExEvent_TaskEvent(ColorPickEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnColorPickExEvent_PublicEvent(ColorPickEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnColorPickExEvent_PrivateEvent(ColorPickEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnColorPickExEvent_ParentWinRefreshEvent(ColorPickEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 当前色被改变
		*
		*当前色被改变
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Color 颜色 颜色
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnColorPickExEvent_ColorChanged(ColorPickEx_P EventControl, AutoInt EventMsg, AutoInt Color, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRichEditExEvent_L_Click(RichEditEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRichEditExEvent_R_Click(RichEditEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRichEditExEvent_L_Down(RichEditEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRichEditExEvent_R_Down(RichEditEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRichEditExEvent_L_Up(RichEditEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRichEditExEvent_R_Up(RichEditEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRichEditExEvent_L_DClick(RichEditEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRichEditExEvent_R_DClick(RichEditEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRichEditExEvent_MouseMove(RichEditEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnRichEditExEvent_MouseEnterLeave(RichEditEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRichEditExEvent_MouseWheel(RichEditEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRichEditExEvent_keyEvent(RichEditEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnRichEditExEvent_CharInput(RichEditEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnRichEditExEvent_Focus(RichEditEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnRichEditExEvent_Other(RichEditEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnRichEditExEvent_ElemEvent(RichEditEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnRichEditExEvent_TaskEvent(RichEditEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnRichEditExEvent_PublicEvent(RichEditEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnRichEditExEvent_PrivateEvent(RichEditEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnRichEditExEvent_ParentWinRefreshEvent(RichEditEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 内容被改变
		*
		*当编辑框内改变时触发此事件,可通过参数1改变模式来判断改变因由做不同的处理.改变模式参考ContentChanged_前缀常量
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ChangedType 改变模式 暂无参数说明
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnRichEditExEvent_ContentChanged(RichEditEx_P EventControl, AutoInt EventMsg, AutoInt ChangedType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 选择区被改变
		*
		*选择区被改变
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param  附加参数1 附加参数
		* @param  附加参数2 附加参数
		* @param  附加参数3 附加参数
		* @param  附加参数4 附加参数
		*/
		virtual AutoInt OnRichEditExEvent_SelectionChanged(RichEditEx_P EventControl, AutoInt EventMsg, AutoInt, AutoInt, AutoInt, AutoInt) { return NULL; }
		/*
		* @brief 接口其它事件
		*
		*接口其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnRichEditExEvent_OtherExtendEvent(RichEditEx_P EventControl, AutoInt EventMsg, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnExtendExEvent_L_Click(ExtendEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnExtendExEvent_R_Click(ExtendEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnExtendExEvent_L_Down(ExtendEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnExtendExEvent_R_Down(ExtendEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnExtendExEvent_L_Up(ExtendEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnExtendExEvent_R_Up(ExtendEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnExtendExEvent_L_DClick(ExtendEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnExtendExEvent_R_DClick(ExtendEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnExtendExEvent_MouseMove(ExtendEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnExtendExEvent_MouseEnterLeave(ExtendEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnExtendExEvent_MouseWheel(ExtendEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnExtendExEvent_keyEvent(ExtendEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnExtendExEvent_CharInput(ExtendEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnExtendExEvent_Focus(ExtendEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnExtendExEvent_Other(ExtendEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnExtendExEvent_ElemEvent(ExtendEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnExtendExEvent_TaskEvent(ExtendEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnExtendExEvent_PublicEvent(ExtendEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnExtendExEvent_PrivateEvent(ExtendEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnExtendExEvent_ParentWinRefreshEvent(ExtendEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 扩展事件
		*
		*扩展事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnExtendExEvent_ExtendEvent(ExtendEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAnimationbuttonExEvent_L_Click(AnimationbuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAnimationbuttonExEvent_R_Click(AnimationbuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAnimationbuttonExEvent_L_Down(AnimationbuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAnimationbuttonExEvent_R_Down(AnimationbuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAnimationbuttonExEvent_L_Up(AnimationbuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAnimationbuttonExEvent_R_Up(AnimationbuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAnimationbuttonExEvent_L_DClick(AnimationbuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAnimationbuttonExEvent_R_DClick(AnimationbuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAnimationbuttonExEvent_MouseMove(AnimationbuttonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnAnimationbuttonExEvent_MouseEnterLeave(AnimationbuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAnimationbuttonExEvent_MouseWheel(AnimationbuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAnimationbuttonExEvent_keyEvent(AnimationbuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAnimationbuttonExEvent_CharInput(AnimationbuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnAnimationbuttonExEvent_Focus(AnimationbuttonEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnAnimationbuttonExEvent_Other(AnimationbuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnAnimationbuttonExEvent_ElemEvent(AnimationbuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnAnimationbuttonExEvent_TaskEvent(AnimationbuttonEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnAnimationbuttonExEvent_PublicEvent(AnimationbuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnAnimationbuttonExEvent_PrivateEvent(AnimationbuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnAnimationbuttonExEvent_ParentWinRefreshEvent(AnimationbuttonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAdvancedFormExEvent_L_Click(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAdvancedFormExEvent_R_Click(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAdvancedFormExEvent_L_Down(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAdvancedFormExEvent_R_Down(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAdvancedFormExEvent_L_Up(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAdvancedFormExEvent_R_Up(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAdvancedFormExEvent_L_DClick(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAdvancedFormExEvent_R_DClick(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAdvancedFormExEvent_MouseMove(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnAdvancedFormExEvent_MouseEnterLeave(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAdvancedFormExEvent_MouseWheel(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAdvancedFormExEvent_keyEvent(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnAdvancedFormExEvent_CharInput(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnAdvancedFormExEvent_Focus(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnAdvancedFormExEvent_Other(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnAdvancedFormExEvent_ElemEvent(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnAdvancedFormExEvent_TaskEvent(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnAdvancedFormExEvent_PublicEvent(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnAdvancedFormExEvent_PrivateEvent(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnAdvancedFormExEvent_ParentWinRefreshEvent(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 项目左键单击
		*
		*表项事件区分是否命中表列,处理需要区分事件来源参数
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ElemeIndex 元素索引 暂无
		* @param EventSource 事件来源 事件来源有2种情况表项事件(在命中表项时无论是否命中表列时均会触发)表列事件(仅在命中表列时触发)当命中表列时会同时触发表项事件
		*/
		virtual AutoInt OnAdvancedFormExEvent_Item_L_Click(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ElemeIndex, AutoInt EventSource) { return NULL; }
		/*
		* @brief 项目右键单击
		*
		*表项事件区分是否命中表列,处理需要区分事件来源参数
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ElemeIndex 元素索引 暂无
		* @param EventSource 事件来源 事件来源有2种情况表项事件(在命中表项时无论是否命中表列时均会触发)表列事件(仅在命中表列时触发)当命中表列时会同时触发表项事件
		*/
		virtual AutoInt OnAdvancedFormExEvent_Item_R_Click(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ElemeIndex, AutoInt EventSource) { return NULL; }
		/*
		* @brief 项目鼠标左键放开
		*
		*表项事件区分是否命中表列,处理需要区分事件来源参数
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ElemeIndex 元素索引 暂无
		* @param EventSource 事件来源 事件来源有2种情况表项事件(在命中表项时无论是否命中表列时均会触发)表列事件(仅在命中表列时触发)当命中表列时会同时触发表项事件
		*/
		virtual AutoInt OnAdvancedFormExEvent_Item_L_Up(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ElemeIndex, AutoInt EventSource) { return NULL; }
		/*
		* @brief 项目鼠标右键放开
		*
		*表项事件区分是否命中表列,处理需要区分事件来源参数
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ElemeIndex 元素索引 暂无
		* @param EventSource 事件来源 事件来源有2种情况表项事件(在命中表项时无论是否命中表列时均会触发)表列事件(仅在命中表列时触发)当命中表列时会同时触发表项事件
		*/
		virtual AutoInt OnAdvancedFormExEvent_Item_R_Up(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ElemeIndex, AutoInt EventSource) { return NULL; }
		/*
		* @brief 项目鼠标左键按下
		*
		*表项事件区分是否命中表列,处理需要区分事件来源参数
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ElemeIndex 元素索引 暂无
		* @param EventSource 事件来源 事件来源有2种情况表项事件(在命中表项时无论是否命中表列时均会触发)表列事件(仅在命中表列时触发)当命中表列时会同时触发表项事件
		*/
		virtual AutoInt OnAdvancedFormExEvent_Item_L_Down(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ElemeIndex, AutoInt EventSource) { return NULL; }
		/*
		* @brief 项目鼠标右键按下
		*
		*表项事件区分是否命中表列,处理需要区分事件来源参数
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ElemeIndex 元素索引 暂无
		* @param EventSource 事件来源 事件来源有2种情况表项事件(在命中表项时无论是否命中表列时均会触发)表列事件(仅在命中表列时触发)当命中表列时会同时触发表项事件
		*/
		virtual AutoInt OnAdvancedFormExEvent_Item_R_Down(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ElemeIndex, AutoInt EventSource) { return NULL; }
		/*
		* @brief 项目左键双击
		*
		*表项事件区分是否命中表列,处理需要区分事件来源参数
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ElemeIndex 元素索引 暂无
		* @param EventSource 事件来源 事件来源有2种情况表项事件(在命中表项时无论是否命中表列时均会触发)表列事件(仅在命中表列时触发)当命中表列时会同时触发表项事件
		*/
		virtual AutoInt OnAdvancedFormExEvent_Item_L_DClick(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ElemeIndex, AutoInt EventSource) { return NULL; }
		/*
		* @brief 项目右键双击
		*
		*表项事件区分是否命中表列,处理需要区分事件来源参数
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ElemeIndex 元素索引 暂无
		* @param EventSource 事件来源 事件来源有2种情况表项事件(在命中表项时无论是否命中表列时均会触发)表列事件(仅在命中表列时触发)当命中表列时会同时触发表项事件
		*/
		virtual AutoInt OnAdvancedFormExEvent_Item_R_DClick(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ElemeIndex, AutoInt EventSource) { return NULL; }
		/*
		* @brief 项目鼠标进入
		*
		*表项事件区分是否命中表列,处理需要区分事件来源参数
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ElemeIndex 元素索引 暂无
		* @param EventSource 事件来源 事件来源有2种情况表项事件(在命中表项时无论是否命中表列时均会触发)表列事件(仅在命中表列时触发)当命中表列时会同时触发表项事件
		*/
		virtual AutoInt OnAdvancedFormExEvent_Item_MouseIn(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ElemeIndex, AutoInt EventSource) { return NULL; }
		/*
		* @brief 项目鼠标离开
		*
		*表项事件区分是否命中表列,处理需要区分事件来源参数
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ElemeIndex 元素索引 暂无
		* @param EventSource 事件来源 事件来源有2种情况表项事件(在命中表项时无论是否命中表列时均会触发)表列事件(仅在命中表列时触发)当命中表列时会同时触发表项事件
		*/
		virtual AutoInt OnAdvancedFormExEvent_Item_MouseOut(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ElemeIndex, AutoInt EventSource) { return NULL; }
		/*
		* @brief 项目将进入编辑
		*
		*项目进入编辑 返回 1阻止编辑
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param EnterMode 进入模式 参数记录项目进入编辑的触发原因
		* @param EditContent 项目内容 内容指针可以使用pts命令获取内容做校验
		*/
		virtual AutoInt OnAdvancedFormExEvent_Item_WillEnterEdit(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt EnterMode, AutoInt EditContent) { return NULL; }
		/*
		* @brief 项目将退出编辑
		*
		*项目退出编辑 返回 1阻止结果
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ExitMode 退出模式 参数记录项目退出编辑的触发原因
		* @param EditContent 项目内容 内容指针可以使用pts命令获取内容做校验
		*/
		virtual AutoInt OnAdvancedFormExEvent_Item_WillExitEdit(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ExitMode, AutoInt EditContent) { return NULL; }
		/*
		* @brief 项目进入编辑
		*
		*项目进入编辑
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param EnterMode 进入模式 参数记录项目进入编辑的触发原因
		* @param EditContent 项目内容 内容指针可以使用pts命令获取内容做校验
		*/
		virtual AutoInt OnAdvancedFormExEvent_Item_EnterEdit(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt EnterMode, AutoInt EditContent) { return NULL; }
		/*
		* @brief 项目退出编辑
		*
		*项目退出编辑
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ExitMode 退出模式 参数记录项目退出编辑的触发原因
		* @param EditContent 项目内容 内容指针可以使用pts命令获取内容做校验
		*/
		virtual AutoInt OnAdvancedFormExEvent_Item_ExitEdit(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ExitMode, AutoInt EditContent) { return NULL; }
		/*
		* @brief 项目尺寸被改变
		*
		*项目尺寸被改变
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param ChangedType 调整类型 调整类型
		* @param ChangValue 新尺寸 新尺寸
		*/
		virtual AutoInt OnAdvancedFormExEvent_Item_SizeChanged(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt ChangedType, AutoInt ChangValue) { return NULL; }
		/*
		* @brief 项目折叠将改变
		*
		*项目折叠将改变
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param ColumnIndex 列索引 起始值从0开始
		* @param RelatedParam2 相关参数1 相关参数1
		* @param RelatedParam3 相关参数2 相关参数2
		*/
		virtual AutoInt OnAdvancedFormExEvent_Item_FoldChanged(AdvancedFormEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt ColumnIndex, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSlideButtonExEvent_L_Click(SlideButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSlideButtonExEvent_R_Click(SlideButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSlideButtonExEvent_L_Down(SlideButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSlideButtonExEvent_R_Down(SlideButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSlideButtonExEvent_L_Up(SlideButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSlideButtonExEvent_R_Up(SlideButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSlideButtonExEvent_L_DClick(SlideButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSlideButtonExEvent_R_DClick(SlideButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSlideButtonExEvent_MouseMove(SlideButtonEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSlideButtonExEvent_MouseEnterLeave(SlideButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSlideButtonExEvent_MouseWheel(SlideButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSlideButtonExEvent_keyEvent(SlideButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSlideButtonExEvent_CharInput(SlideButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnSlideButtonExEvent_Focus(SlideButtonEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSlideButtonExEvent_Other(SlideButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSlideButtonExEvent_ElemEvent(SlideButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnSlideButtonExEvent_TaskEvent(SlideButtonEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnSlideButtonExEvent_PublicEvent(SlideButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnSlideButtonExEvent_PrivateEvent(SlideButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnSlideButtonExEvent_ParentWinRefreshEvent(SlideButtonEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 选中状态改变
		*
		*选中状态改变
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param SelectType 新状态 新状态
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSlideButtonExEvent_SelectChanged(SlideButtonEx_P EventControl, AutoInt EventMsg, AutoInt SelectType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPieChartExEvent_L_Click(PieChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPieChartExEvent_R_Click(PieChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPieChartExEvent_L_Down(PieChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPieChartExEvent_R_Down(PieChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPieChartExEvent_L_Up(PieChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPieChartExEvent_R_Up(PieChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPieChartExEvent_L_DClick(PieChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPieChartExEvent_R_DClick(PieChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPieChartExEvent_MouseMove(PieChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnPieChartExEvent_MouseEnterLeave(PieChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPieChartExEvent_MouseWheel(PieChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPieChartExEvent_keyEvent(PieChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPieChartExEvent_CharInput(PieChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnPieChartExEvent_Focus(PieChartEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnPieChartExEvent_Other(PieChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnPieChartExEvent_ElemEvent(PieChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnPieChartExEvent_TaskEvent(PieChartEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnPieChartExEvent_PublicEvent(PieChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnPieChartExEvent_PrivateEvent(PieChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnPieChartExEvent_ParentWinRefreshEvent(PieChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 图例事件
		*
		*图例事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param GraphIndex 图例索引 图例索引
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		*/
		virtual AutoInt OnPieChartExEvent_ChartEvent(PieChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt GraphIndex, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnBarChartExEvent_L_Click(BarChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnBarChartExEvent_R_Click(BarChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnBarChartExEvent_L_Down(BarChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnBarChartExEvent_R_Down(BarChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnBarChartExEvent_L_Up(BarChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnBarChartExEvent_R_Up(BarChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnBarChartExEvent_L_DClick(BarChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnBarChartExEvent_R_DClick(BarChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnBarChartExEvent_MouseMove(BarChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnBarChartExEvent_MouseEnterLeave(BarChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnBarChartExEvent_MouseWheel(BarChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnBarChartExEvent_keyEvent(BarChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnBarChartExEvent_CharInput(BarChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnBarChartExEvent_Focus(BarChartEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnBarChartExEvent_Other(BarChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnBarChartExEvent_ElemEvent(BarChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnBarChartExEvent_TaskEvent(BarChartEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnBarChartExEvent_PublicEvent(BarChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnBarChartExEvent_PrivateEvent(BarChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnBarChartExEvent_ParentWinRefreshEvent(BarChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 图例事件
		*
		*图例事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param GraphIndex 图例索引 图例索引
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		*/
		virtual AutoInt OnBarChartExEvent_ChartEvent(BarChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt GraphIndex, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCurveChartExEvent_L_Click(CurveChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCurveChartExEvent_R_Click(CurveChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCurveChartExEvent_L_Down(CurveChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCurveChartExEvent_R_Down(CurveChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCurveChartExEvent_L_Up(CurveChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCurveChartExEvent_R_Up(CurveChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCurveChartExEvent_L_DClick(CurveChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCurveChartExEvent_R_DClick(CurveChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCurveChartExEvent_MouseMove(CurveChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCurveChartExEvent_MouseEnterLeave(CurveChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCurveChartExEvent_MouseWheel(CurveChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCurveChartExEvent_keyEvent(CurveChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCurveChartExEvent_CharInput(CurveChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnCurveChartExEvent_Focus(CurveChartEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCurveChartExEvent_Other(CurveChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCurveChartExEvent_ElemEvent(CurveChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnCurveChartExEvent_TaskEvent(CurveChartEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnCurveChartExEvent_PublicEvent(CurveChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnCurveChartExEvent_PrivateEvent(CurveChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnCurveChartExEvent_ParentWinRefreshEvent(CurveChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 图例事件
		*
		*图例事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param GraphIndex 图例索引 图例索引
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		*/
		virtual AutoInt OnCurveChartExEvent_ChartEvent(CurveChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt GraphIndex, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCandleChartExEvent_L_Click(CandleChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCandleChartExEvent_R_Click(CandleChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCandleChartExEvent_L_Down(CandleChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCandleChartExEvent_R_Down(CandleChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCandleChartExEvent_L_Up(CandleChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCandleChartExEvent_R_Up(CandleChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCandleChartExEvent_L_DClick(CandleChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCandleChartExEvent_R_DClick(CandleChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCandleChartExEvent_MouseMove(CandleChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCandleChartExEvent_MouseEnterLeave(CandleChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCandleChartExEvent_MouseWheel(CandleChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCandleChartExEvent_keyEvent(CandleChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCandleChartExEvent_CharInput(CandleChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnCandleChartExEvent_Focus(CandleChartEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCandleChartExEvent_Other(CandleChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCandleChartExEvent_ElemEvent(CandleChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnCandleChartExEvent_TaskEvent(CandleChartEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnCandleChartExEvent_PublicEvent(CandleChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnCandleChartExEvent_PrivateEvent(CandleChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnCandleChartExEvent_ParentWinRefreshEvent(CandleChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 图例事件
		*
		*图例事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param GraphIndex 图例索引 图例索引
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		*/
		virtual AutoInt OnCandleChartExEvent_ChartEvent(CandleChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt GraphIndex, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDrawPanelExEvent_L_Click(DrawPanelEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDrawPanelExEvent_R_Click(DrawPanelEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDrawPanelExEvent_L_Down(DrawPanelEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDrawPanelExEvent_R_Down(DrawPanelEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDrawPanelExEvent_L_Up(DrawPanelEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDrawPanelExEvent_R_Up(DrawPanelEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDrawPanelExEvent_L_DClick(DrawPanelEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDrawPanelExEvent_R_DClick(DrawPanelEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDrawPanelExEvent_MouseMove(DrawPanelEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnDrawPanelExEvent_MouseEnterLeave(DrawPanelEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDrawPanelExEvent_MouseWheel(DrawPanelEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDrawPanelExEvent_keyEvent(DrawPanelEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDrawPanelExEvent_CharInput(DrawPanelEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnDrawPanelExEvent_Focus(DrawPanelEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnDrawPanelExEvent_Other(DrawPanelEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnDrawPanelExEvent_ElemEvent(DrawPanelEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnDrawPanelExEvent_TaskEvent(DrawPanelEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnDrawPanelExEvent_PublicEvent(DrawPanelEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnDrawPanelExEvent_PrivateEvent(DrawPanelEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnDrawPanelExEvent_ParentWinRefreshEvent(DrawPanelEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 绘画事件
		*
		*绘画事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnDrawPanelExEvent_DrawEvent(DrawPanelEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollLayoutBoxExEvent_L_Click(ScrollLayoutBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollLayoutBoxExEvent_R_Click(ScrollLayoutBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollLayoutBoxExEvent_L_Down(ScrollLayoutBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollLayoutBoxExEvent_R_Down(ScrollLayoutBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollLayoutBoxExEvent_L_Up(ScrollLayoutBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollLayoutBoxExEvent_R_Up(ScrollLayoutBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollLayoutBoxExEvent_L_DClick(ScrollLayoutBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollLayoutBoxExEvent_R_DClick(ScrollLayoutBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollLayoutBoxExEvent_MouseMove(ScrollLayoutBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnScrollLayoutBoxExEvent_MouseEnterLeave(ScrollLayoutBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollLayoutBoxExEvent_MouseWheel(ScrollLayoutBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollLayoutBoxExEvent_keyEvent(ScrollLayoutBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnScrollLayoutBoxExEvent_CharInput(ScrollLayoutBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnScrollLayoutBoxExEvent_Focus(ScrollLayoutBoxEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnScrollLayoutBoxExEvent_Other(ScrollLayoutBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnScrollLayoutBoxExEvent_ElemEvent(ScrollLayoutBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnScrollLayoutBoxExEvent_TaskEvent(ScrollLayoutBoxEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnScrollLayoutBoxExEvent_PublicEvent(ScrollLayoutBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnScrollLayoutBoxExEvent_PrivateEvent(ScrollLayoutBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnScrollLayoutBoxExEvent_ParentWinRefreshEvent(ScrollLayoutBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }







		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMediaboxExEvent_L_Click(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMediaboxExEvent_R_Click(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMediaboxExEvent_L_Down(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMediaboxExEvent_R_Down(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMediaboxExEvent_L_Up(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMediaboxExEvent_R_Up(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMediaboxExEvent_L_DClick(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMediaboxExEvent_R_DClick(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMediaboxExEvent_MouseMove(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMediaboxExEvent_MouseEnterLeave(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMediaboxExEvent_MouseWheel(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMediaboxExEvent_keyEvent(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnMediaboxExEvent_CharInput(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnMediaboxExEvent_Focus(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMediaboxExEvent_Other(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMediaboxExEvent_ElemEvent(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnMediaboxExEvent_TaskEvent(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnMediaboxExEvent_PublicEvent(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnMediaboxExEvent_PrivateEvent(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnMediaboxExEvent_ParentWinRefreshEvent(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 播放进度改变
		*
		*播放进度改变
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Position 进度 进度
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnMediaboxExEvent_PlayPositionChanged(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt Position, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 保留事件1
		*
		*保留事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnMediaboxExEvent_RelatedEvent1(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 保留事件2
		*
		*保留事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnMediaboxExEvent_RelatedEvent2(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 保留事件2
		*
		*保留事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnMediaboxExEvent_RelatedEvent3(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 保留事件4
		*
		*保留事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnMediaboxExEvent_RelatedEvent4(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 保留事件5
		*
		*保留事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnMediaboxExEvent_RelatedEvent5(MediaboxEx_P EventControl, AutoInt EventMsg, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }






		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCarouselBoxExEvent_L_Click(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCarouselBoxExEvent_R_Click(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCarouselBoxExEvent_L_Down(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCarouselBoxExEvent_R_Down(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }

		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCarouselBoxExEvent_L_Up(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCarouselBoxExEvent_R_Up(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCarouselBoxExEvent_L_DClick(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCarouselBoxExEvent_R_DClick(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCarouselBoxExEvent_MouseMove(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCarouselBoxExEvent_MouseEnterLeave(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCarouselBoxExEvent_MouseWheel(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCarouselBoxExEvent_keyEvent(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnCarouselBoxExEvent_CharInput(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnCarouselBoxExEvent_Focus(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCarouselBoxExEvent_Other(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCarouselBoxExEvent_ElemEvent(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnCarouselBoxExEvent_TaskEvent(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnCarouselBoxExEvent_PublicEvent(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnCarouselBoxExEvent_PrivateEvent(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnCarouselBoxExEvent_ParentWinRefreshEvent(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 项目左键单击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCarouselBoxExEvent_Item_L_Click(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目右键单击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCarouselBoxExEvent_Item_R_Click(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标左键放开
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCarouselBoxExEvent_Item_L_Up(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标右键放开
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCarouselBoxExEvent_Item_R_Up(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标左键按下
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCarouselBoxExEvent_Item_L_Down(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标右键按下
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCarouselBoxExEvent_Item_R_Down(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目左键双击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCarouselBoxExEvent_Item_L_DClick(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目右键双击
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCarouselBoxExEvent_Item_R_DClick(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标进入
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCarouselBoxExEvent_Item_MouseIn(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 项目鼠标离开
		*
		*项目事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param ItemIndex 项目索引 起始值从0开始.
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnCarouselBoxExEvent_Item_MouseOut(CarouselBoxEx_P EventControl, AutoInt EventMsg, AutoInt ItemIndex, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }












		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperEditExEvent_L_Click(SuperEditEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperEditExEvent_R_Click(SuperEditEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperEditExEvent_L_Down(SuperEditEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperEditExEvent_R_Down(SuperEditEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperEditExEvent_L_Up(SuperEditEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperEditExEvent_R_Up(SuperEditEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperEditExEvent_L_DClick(SuperEditEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperEditExEvent_R_DClick(SuperEditEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperEditExEvent_MouseMove(SuperEditEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSuperEditExEvent_MouseEnterLeave(SuperEditEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperEditExEvent_MouseWheel(SuperEditEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperEditExEvent_keyEvent(SuperEditEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperEditExEvent_CharInput(SuperEditEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnSuperEditExEvent_Focus(SuperEditEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSuperEditExEvent_Other(SuperEditEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSuperEditExEvent_ElemEvent(SuperEditEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnSuperEditExEvent_TaskEvent(SuperEditEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnSuperEditExEvent_PublicEvent(SuperEditEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnSuperEditExEvent_PrivateEvent(SuperEditEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnSuperEditExEvent_ParentWinRefreshEvent(SuperEditEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 事件通知
		*
		*事件通知
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Position 进度 进度
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSuperEditExEvent_EventNotification(SuperEditEx_P EventControl, AutoInt EventMsg, AutoInt Position, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }









		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChatBoxExEvent_L_Click(ChatBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChatBoxExEvent_R_Click(ChatBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChatBoxExEvent_L_Down(ChatBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChatBoxExEvent_R_Down(ChatBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChatBoxExEvent_L_Up(ChatBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChatBoxExEvent_R_Up(ChatBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChatBoxExEvent_L_DClick(ChatBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChatBoxExEvent_R_DClick(ChatBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChatBoxExEvent_MouseMove(ChatBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnChatBoxExEvent_MouseEnterLeave(ChatBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChatBoxExEvent_MouseWheel(ChatBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChatBoxExEvent_keyEvent(ChatBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnChatBoxExEvent_CharInput(ChatBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnChatBoxExEvent_Focus(ChatBoxEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnChatBoxExEvent_Other(ChatBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnChatBoxExEvent_ElemEvent(ChatBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnChatBoxExEvent_TaskEvent(ChatBoxEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnChatBoxExEvent_PublicEvent(ChatBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnChatBoxExEvent_PrivateEvent(ChatBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnChatBoxExEvent_ParentWinRefreshEvent(ChatBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 事件通知
		*
		*事件通知
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Position 进度 进度
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnChatBoxExEvent_EventNotification(ChatBoxEx_P EventControl, AutoInt EventMsg, AutoInt Position, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }










		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperChartExEvent_L_Click(SuperChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperChartExEvent_R_Click(SuperChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperChartExEvent_L_Down(SuperChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperChartExEvent_R_Down(SuperChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperChartExEvent_L_Up(SuperChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperChartExEvent_R_Up(SuperChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperChartExEvent_L_DClick(SuperChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperChartExEvent_R_DClick(SuperChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperChartExEvent_MouseMove(SuperChartEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSuperChartExEvent_MouseEnterLeave(SuperChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperChartExEvent_MouseWheel(SuperChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperChartExEvent_keyEvent(SuperChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnSuperChartExEvent_CharInput(SuperChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnSuperChartExEvent_Focus(SuperChartEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSuperChartExEvent_Other(SuperChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSuperChartExEvent_ElemEvent(SuperChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnSuperChartExEvent_TaskEvent(SuperChartEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnSuperChartExEvent_PublicEvent(SuperChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnSuperChartExEvent_PrivateEvent(SuperChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnSuperChartExEvent_ParentWinRefreshEvent(SuperChartEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 事件通知
		*
		*事件通知
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Position 进度 进度
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnSuperChartExEvent_EventNotification(SuperChartEx_P EventControl, AutoInt EventMsg, AutoInt Position, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }











		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDataReportBoxExEvent_L_Click(DataReportBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDataReportBoxExEvent_R_Click(DataReportBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDataReportBoxExEvent_L_Down(DataReportBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDataReportBoxExEvent_R_Down(DataReportBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDataReportBoxExEvent_L_Up(DataReportBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDataReportBoxExEvent_R_Up(DataReportBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDataReportBoxExEvent_L_DClick(DataReportBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDataReportBoxExEvent_R_DClick(DataReportBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDataReportBoxExEvent_MouseMove(DataReportBoxEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnDataReportBoxExEvent_MouseEnterLeave(DataReportBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDataReportBoxExEvent_MouseWheel(DataReportBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDataReportBoxExEvent_keyEvent(DataReportBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnDataReportBoxExEvent_CharInput(DataReportBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnDataReportBoxExEvent_Focus(DataReportBoxEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnDataReportBoxExEvent_Other(DataReportBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnDataReportBoxExEvent_ElemEvent(DataReportBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnDataReportBoxExEvent_TaskEvent(DataReportBoxEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnDataReportBoxExEvent_PublicEvent(DataReportBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnDataReportBoxExEvent_PrivateEvent(DataReportBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnDataReportBoxExEvent_ParentWinRefreshEvent(DataReportBoxEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 事件通知
		*
		*事件通知
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Position 进度 进度
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnDataReportBoxExEvent_EventNotification(DataReportBoxEx_P EventControl, AutoInt EventMsg, AutoInt Position, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
















		/*
		* @brief 鼠标左键单击
		*
		*鼠标左键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPageNavigBarExEvent_L_Click(PageNavigBarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键单击
		*
		*鼠标右键单击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPageNavigBarExEvent_R_Click(PageNavigBarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键按下
		*
		*鼠标左键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPageNavigBarExEvent_L_Down(PageNavigBarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键按下
		*
		*鼠标右键按下
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPageNavigBarExEvent_R_Down(PageNavigBarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键放开
		*
		*鼠标左键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPageNavigBarExEvent_L_Up(PageNavigBarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键放开
		*
		*鼠标右键放开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPageNavigBarExEvent_R_Up(PageNavigBarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标左键双击
		*
		*鼠标左键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPageNavigBarExEvent_L_DClick(PageNavigBarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标右键双击
		*
		*鼠标右键双击
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPageNavigBarExEvent_R_DClick(PageNavigBarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标位置移动
		*
		*鼠标位置移动
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param PointX 坐标x 坐标x
		* @param PointY 坐标y 坐标y
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPageNavigBarExEvent_MouseMove(PageNavigBarEx_P EventControl, AutoInt EventMsg, AutoInt PointX, AutoInt PointY, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 鼠标进入离开
		*
		*鼠标进入离开
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnPageNavigBarExEvent_MouseEnterLeave(PageNavigBarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 滚轮事件
		*
		*滚轮事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPageNavigBarExEvent_MouseWheel(PageNavigBarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 键盘事件
		*
		*键盘事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPageNavigBarExEvent_keyEvent(PageNavigBarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 字符输入事件
		*
		*字符输入事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParamHwnd 事件窗口 事件窗口
		* @param RelatedParam1 相关参数1 系统提供,自行查阅文档
		* @param RelatedParam2 相关参数2 系统提供,自行查阅文档
		*/
		virtual AutoInt OnPageNavigBarExEvent_CharInput(PageNavigBarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParamHwnd, AutoInt RelatedParam1, AutoInt RelatedParam2) { return NULL; }
		/*
		* @brief 焦点事件
		*
		*焦点事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param FocusMode 焦点状态 0失去焦点,非0获得焦点
		* @param RelatedControl 因果组件 因果组件
		* @param RelatedEvent 因果事件 因果事件
		* @param RelatedParam 相关参数 相关参数
		*/
		virtual AutoInt OnPageNavigBarExEvent_Focus(PageNavigBarEx_P EventControl, AutoInt EventMsg, AutoInt FocusMode, AutoInt RelatedControl, AutoInt RelatedEvent, AutoInt RelatedParam) { return NULL; }
		/*
		* @brief 其它事件
		*
		*其它事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnPageNavigBarExEvent_Other(PageNavigBarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 元素事件
		*
		*元素事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 相关参数1
		* @param ElemType 元素归属 相关参数1
		* @param Index 项目索引 相关参数2
		* @param ColumnId 列索引 相关参数3
		* @param CtrId 元素索引 相关参数4
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnPageNavigBarExEvent_ElemEvent(PageNavigBarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }
		/*
		* @brief 任务事件
		*
		*任务事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param CallMode 调用方式 调用方式
		* @param EventMark 任务标记 任务标记
		* @param EventParam 任务参数 任务参数
		*/
		virtual AutoInt OnPageNavigBarExEvent_TaskEvent(PageNavigBarEx_P EventControl, AutoInt EventMsg, AutoInt CallMode, AutoInt EventMark, AutoInt EventParam) { return NULL; }
		/*
		* @brief 公有事件
		*
		*公有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnPageNavigBarExEvent_PublicEvent(PageNavigBarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 私有事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnPageNavigBarExEvent_PrivateEvent(PageNavigBarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 父窗口重画事件
		*
		*私有事件
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param EventType 事件类型 事件类型
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		* @param RelatedParam4 相关参数4 相关参数4
		*/
		virtual AutoInt OnPageNavigBarExEvent_ParentWinRefreshEvent(PageNavigBarEx_P EventControl, AutoInt EventMsg, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4) { return NULL; }
		/*
		* @brief 事件通知
		*
		*事件通知
		* @return 返回数据格式 1001
		* @param EventControl 事件组件 参数提供事件发生组件句柄
		* @param EventMsg 事件消息值 参数提供事件消息值
		* @param Position 进度 进度
		* @param RelatedParam1 相关参数1 相关参数1
		* @param RelatedParam2 相关参数2 相关参数2
		* @param RelatedParam3 相关参数3 相关参数3
		*/
		virtual AutoInt OnPageNavigBarExEvent_EventNotification(PageNavigBarEx_P EventControl, AutoInt EventMsg, AutoInt Position, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3) { return NULL; }












		/*----------组件事件通知结束--------*/


	private:

		AutoInt Load(Control_P Parent, HWND Window, AutoInt mode, AutoInt Parameter1, AutoInt Parameter2);


	public:

		//创建主窗口
		AutoInt LoadMainWindow();
		//创建窗口
		AutoInt LoadWindow(BaseEx& Parent);
		//创建子窗口
		AutoInt LoadChildWindow(BaseEx& Parent);
		//创建弹出子窗口
		AutoInt LoadPopupChildWindow(BaseEx& Parent);
		//创建对话框窗口
		AutoInt LoadDialogWindow(BaseEx& Parent);
		//创建到组件
		AutoInt LoadToControl(BaseEx& Parent);
		//创建到窗口
		AutoInt LoadToWindow(BaseEx& Parent);





	public:

		/*--------信息框Ex--------*/
		int MsgBoxEx(BindataEx Ico, StringEx Title, BindataEx TipIco, StringEx TipTitle, StringEx Button, int DefaultBtn, HWND hWndParent, int Style, BindataEx Skin, ExuiCallback callback, AutoInt Parameter);

		/*--------输入框Ex--------*/
		StringEx InputBoxEx(BindataEx Ico, StringEx Title, BindataEx TipIco, StringEx TipTitle, StringEx Button, int DefaultBtn, StringEx Conten, int InputMode, StringEx PasswordSubstitution, int MaxInput, BOOL Multiline, AutoInt* SelectResult, HWND hWndParent, int Style, BindataEx Skin, ExuiCallback callback, AutoInt Parameter_);

		/*--------弹出提示框Ex--------*/
		int PopUpTipBoxEx(BindataEx Ico, StringEx TipTitle, int PopUpMode, int x, int y, int width, int height, int AutoCloseTime, int Angle, int BackColor, int BorderColor, int LineWidth, BindataEx Font, int FontColor, int Align, int Transparency, int ReferType, Control_P ReferControl);

		/*--------关闭提示框Ex--------*/
		void CloseTipBoxEx();

		/*--------精简输入框Ex--------*/
		StringEx PopUpLiteInputEx(int X, int Y, int nWidth, int nHeight, ExuiCallback callback, StringEx Content, int InputMode, int MaxInput, StringEx PasswordSubstitution, int Align, BOOL Multiline, BOOL Wrapped, BindataEx Skin, BindataEx Cursor, BindataEx Font, int FontClour, int SelectedFontColor, int SelectedColor, int CursorColor, int ScrollBarMode, int MenuTableWidth, int MenuTableHeight, BindataEx MenuFont, int MenuFontClour, int MenuDisabledFontClour, int MenuTransparency, int MenuLanguage, int ReferType, Control_P ReferControl);

		/*--------时间选择框--------*/
		StringEx TimePickBoxEx(BindataEx Ico, StringEx TipTitle, StringEx Button, int DefaultBtn, int Width, int Height, StringEx SelectionDate, StringEx MiniDate, StringEx MaxDate, BOOL OtherMonthClour, int TimeMode, int Language, AutoInt* SelectResult, HWND hWndParent, int Style, BindataEx Skin, ExuiCallback Callback, AutoInt Parameter_);

		/*--------颜色选择框--------*/
		int ColorPickExBoxEx(BindataEx Ico, StringEx TipTitle, StringEx Button, int DefaultBtn, int Width, int Height, int NowClour, BindataEx QuickClours, int ClourMode, int ColorPickStyle, AutoInt* SelectResult, HWND hWndParent, int Style, BindataEx Skin, ExuiCallback Callback, AutoInt Parameter_);

		/*--------弹出时间选择器--------*/
		StringEx PopUpTimePickEx(int LEFT, int Top, int Width, int Height, StringEx Button, int DefaultBtn, StringEx SelectionDate, StringEx MiniDate, StringEx MaxDate, BOOL OnlyThisMonth, int TimeMode, int Language, AutoInt* SelectResult, HWND hWndParent, int Style, BindataEx Skin, ExuiCallback Callback, AutoInt Parameter_, int ReferType, Control_P ReferControl);

		/*--------弹出颜色选择器--------*/
		int PopUpColorPickEx(int LEFT, int Top, int Width, int Height, StringEx Button, int DefaultBtn, int NowClour, BindataEx QuickClours, int ClourMode, int ColorPickStyle, AutoInt* SelectResult, HWND hWndParent, int Style, BindataEx Skin, ExuiCallback Callback, AutoInt Parameter_, int ReferType, Control_P ReferControl);

		/*--------弹出通知框Ex--------*/
		AutoInt PopUpNotificationBoxEx(BindataEx Ico, StringEx Title, BindataEx TipIco, StringEx TipTitle, StringEx Button, int Style, int PopUpMode, Control_P Parent, int AutoCloseTime, int Reserve, BindataEx Skin, ExuiCallback callback, AutoInt Parameter);

		/*--------关闭通知框Ex--------*/
		void CloseNotificationBoxEx(AutoInt NotificationBoxHandle);

		/*--------弹出项目标题编辑器Ex--------*/
		StringEx PopUpItemInputEx(AutoInt index, AutoInt cloid, AutoInt retain, int mode, StringEx Content, int InputMode, int MaxInput, StringEx PasswordSubstitution, int Align, BOOL Multiline, BOOL Wrapped, BindataEx Skin, BindataEx Cursor, BindataEx Font, int FontClour, int SelectedFontColor, int SelectedColor, int CursorColor, int ScrollBarMode, int MenuTableWidth, int MenuTableHeight, BindataEx MenuFont, int MenuFontClour, int MenuDisabledFontClour, int MenuTransparency, int MenuLanguage);
		/*--------测量文本--------*/
		BindataEx RexMeasureStringEx(StringEx string, BindataEx Fontex, AutoInt StringFormat, float x, float y, float width, float height, int mode);

		/*--------读事件参数数据Ex--------*/
		BindataEx   ReadEventParamDataEx(AutoInt DataPtr, AutoInt DataType);
		/*--------写事件参数数据Ex--------*/
		void   WriteEventParamDataEx(AutoInt DataPtr, BindataEx Data, AutoInt DataType);
		/*--------打包事件返回数据Ex--------*/
		AutoInt   PackEventReturnDataEx(BindataEx Data, AutoInt DataType);
		/*--------解析事件返回数据Ex--------*/
		BindataEx   UnpackEventReturnDataEx(AutoInt DataPtr, AutoInt DataType);

		/*--------读事件参数文本Ex--------*/
		StringEx   ReadEventParamTextEx(AutoInt TextPtr, AutoInt DataType);
		/*--------写事件参数文本Ex--------*/
		void   WriteEventParamTextEx(AutoInt TextPtr, StringEx Text, AutoInt DataType);
		/*--------打包事件返回文本Ex--------*/
		AutoInt   PackEventReturnTextEx(StringEx Text, AutoInt DataType);
		/*--------解析事件返回文本Ex--------*/
		StringEx   UnpackEventReturnTextEx(AutoInt TextPtr, AutoInt DataType);

	};


}
