#pragma once
#include <windows.h>

/*------------------------------------------EXUI数据类型定义开始------------------------------------------*/

#define ExuiKrnlnVersion 20250220


#ifdef _WIN64

typedef long long AutoInt;

#define ExuiLibNameTagText TEXT("ExuiKrnln_Win64_%d.lib")
#define ExuiLibNameText1 TEXT("ExuiKrnln_Win64_20250220.lib")
#define ExuiLibNameText2 TEXT("ExuiKrnln_Win64.lib")
#define ExuiLibNameText3 TEXT("lib\\ExuiKrnln\\ExuiKrnln_Win64.lib")
#define ExuiLibNameConfigText TEXT("SoftWare\\ExuiLib64")


#else

typedef int AutoInt;

#define ExuiLibNameTagText TEXT("ExuiKrnln_Win32_%d.lib")
#define ExuiLibNameText1 TEXT("ExuiKrnln_Win32_20250220.lib")
#define ExuiLibNameText2 TEXT("ExuiKrnln_Win32.lib")
#define ExuiLibNameText3 TEXT("lib\\ExuiKrnln\\ExuiKrnln_Win32.lib")
#define ExuiLibNameConfigText TEXT("SoftWare\\ExuiLib32")

#endif

#define ExuiTypeId 2
#define ExuiThisId 3

#define GetControlExType(ControlHandle) (((AutoInt*)ControlHandle)[ExuiTypeId])
#define GetControlExClass(ControlHandle,ThisClass) (((ThisClass*)ControlHandle)[ExuiThisId])
#define GetControlExClassThis(ControlHandle,ThisClass) (((ThisClass**)ControlHandle)[ExuiThisId])


typedef void* ImageEx_P;
struct ImageEX;
typedef void* BinEx_P;
struct BinEx;
typedef void* Control_P;
struct Control;
typedef void* StrEx_P;
struct StrEx;
typedef void* MenuEx_P;
struct MenuEx;
typedef void* MenuInfoEx_P;
struct MenuInfoEx;
typedef void* DownlistEx_P;
struct DownlistEx;
typedef void* WindowEx_P;
struct WindowEx;
typedef void* LabelEx_P;
struct LabelEx;
typedef void* FilterEx_P;
struct FilterEx;
typedef void* ButtonEx_P;
struct ButtonEx;
typedef void* ImagebuttonEx_P;
struct ImagebuttonEx;
typedef void* SuperbuttonEx_P;
struct SuperbuttonEx;
typedef void* ChoiceboxEx_P;
struct ChoiceboxEx;
typedef void* RadiobuttonEx_P;
struct RadiobuttonEx;
typedef void* EditboxEx_P;
struct EditboxEx;
typedef void* ComboboxEx_P;
struct ComboboxEx;
typedef void* MinutesboxEx_P;
struct MinutesboxEx;
typedef void* MultifunctionButtonEx_P;
struct MultifunctionButtonEx;
typedef void* PictureBoxEx_P;
struct PictureBoxEx;
typedef void* ProgressbarEx_P;
struct ProgressbarEx;
typedef void* ScrollbarEx_P;
struct ScrollbarEx;
typedef void* SliderbarEx_P;
struct SliderbarEx;
typedef void* SelectthefolderEx_P;
struct SelectthefolderEx;
typedef void* ToolbarEx_P;
struct ToolbarEx;
typedef void* ListboxEx_P;
struct ListboxEx;
typedef void* SuperListboxEx_P;
struct SuperListboxEx;
typedef void* IcoListboxEx_P;
struct IcoListboxEx;
typedef void* TreeListEx_P;
struct TreeListEx;
typedef void* WebBrowserEx_P;
struct WebBrowserEx;
typedef void* CalendarBoxEx_P;
struct CalendarBoxEx;
typedef void* ColorPickEx_P;
struct ColorPickEx;
typedef void* RichEditEx_P;
struct RichEditEx;
typedef void* ExtendEx_P;
struct ExtendEx;
typedef void* AnimationbuttonEx_P;
struct AnimationbuttonEx;
typedef void* AdvancedFormEx_P;
struct AdvancedFormEx;
typedef void* SlideButtonEx_P;
struct SlideButtonEx;
typedef void* PieChartEx_P;
struct PieChartEx;
typedef void* BarChartEx_P;
struct BarChartEx;
typedef void* CurveChartEx_P;
struct CurveChartEx;
typedef void* CandleChartEx_P;
struct CandleChartEx;
typedef void* DrawPanelEx_P;
struct DrawPanelEx;
typedef void* ScrollLayoutBoxEx_P;
struct ScrollLayoutBoxEx;

typedef void* MediaboxEx_P;
struct MediaboxEx;

typedef void* CarouselBoxEx_P;
struct CarouselBoxEx;


typedef void* SuperEditEx_P;
struct SuperEditEx;

typedef void* ChatBoxEx_P;
struct ChatBoxEx;


typedef void* SuperChartEx_P;
struct SuperChartEx;

typedef void* DataReportBoxEx_P;
struct DataReportBoxEx;

typedef void* PageNavigBarEx_P;
struct PageNavigBarEx;

typedef void* StructEx_P;
struct StructEx;
typedef void* RectEx_P;
struct Rect_Ex;
typedef void* RectfEx_P;
struct RectF_Ex;
typedef void* SlowMotionTaskEx_P;
struct SlowMotionTaskEx;
typedef void* TimeEx_P;
struct TimeEx;

typedef void* GpGraphics_P;

typedef void* GpStringFormat_P;



/*------------------------------------------EXUI数据类型定义结束------------------------------------------*/



/*------------------------------------------EXUI常量定义开始------------------------------------------*/
enum Constant_DataType
{
	DataType_Struct = 0,
	DataType_Skin = 1,
	DataType_Cur = 2,
	DataType_Font = 4,
	DataType_CharFormat = 8,
	DataType_ParaFormat = 16,
	DataType_Layout = 32,

};


enum Constant_ControlType {
	ControlType_ExuiProgramEx = 10,
	ControlType_WindowBoxEx = 100,
	ControlType_MenuEx = 800,
	ControlType_DownlistEx = 900,
	ControlType_Control = 1000,
	ControlType_WindowEx = 1000,
	ControlType_FilterEx = 1100,
	ControlType_MinutesboxEx = 1200,
	ControlType_SelectthefolderEx = 1300,
	ControlType_PictureBoxEx = 1400,
	ControlType_LabelEx = 1500,
	ControlType_ButtonEx = 1600,
	ControlType_ImagebuttonEx = 1700,
	ControlType_SuperbuttonEx = 1800,
	ControlType_MultifunctionButtonEx = 1900,
	ControlType_RadiobuttonEx = 2000,
	ControlType_ChoiceboxEx = 2100,
	ControlType_ProgressbarEx = 2200,
	ControlType_SliderbarEx = 2300,
	ControlType_ScrollbarEx = 2400,
	ControlType_EditboxEx = 2500,
	ControlType_ComboboxEx = 2600,
	ControlType_ToolbarEx = 2700,
	ControlType_ListboxEx = 2800,
	ControlType_IcoListboxEx = 2900,
	ControlType_SuperListboxEx = 3000,
	ControlType_TreeListEx = 3100,
	ControlType_WebBrowserEx = 3200,
	ControlType_CalendarBoxEx = 3300,
	ControlType_ColorPickEx = 3400,
	ControlType_RichEditEx = 3500,
	ControlType_ExtendEx = 3600,
	ControlType_AnimationbuttonEx = 3700,
	ControlType_AdvancedFormEx = 3800,
	ControlType_SlideButtonEx = 3900,
	ControlType_PieChartEx = 4000,
	ControlType_BarChartEx = 4100,
	ControlType_CurveChartEx = 4200,
	ControlType_CandleChartEx = 4300,
	ControlType_DrawPanelEx = 4400,
	ControlType_ScrollLayoutBoxEx = 4500,
	ControlType_MediaboxEx = 4600,
	ControlType_CarouselBoxEx = 4700,
	ControlType_SuperEditEx = 4800,
	ControlType_ChatBoxEx = 4900,
	ControlType_SuperChartEx = 5000,
	ControlType_DataReportBoxEx = 5100,
	ControlType_PageNavigBarEx = 5200,

	
};

enum Constant_Rc_Skin
{
	Rc_Skin_Msgbox = 700,
	Rc_Skin_MsgboxCtrlBtn = 701,
	Rc_Skin_MenuEx = 800,
	Rc_Skin_DownlistEx = 900,
	Rc_Skin_WindowEx = 1000,
	Rc_Skin_WindowExBtn = 1001,
	Rc_Skin_LogoEx = 1002,
	Rc_Cur_WindowEx = 1099,
	Rc_Skin_FilterEx = 1100,
	Rc_Skin_MinutesboxEx = 1200,
	Rc_Skin_SelectthefolderEx = 1300,
	Rc_Skin_PictureBoxEx = 1400,
	Rc_Skin_LabelEx = 1500,
	Rc_Skin_ButtonEx = 1600,
	Rc_Skin_ImagebuttonEx = 1700,
	Rc_Skin_SuperbuttonEx = 1800,
	Rc_Skin_MultifunctionButtonEx = 1900,
	Rc_Skin_RadiobuttonEx = 2000,
	Rc_Skin_ChoiceboxEx = 2100,
	Rc_Skin_ProgressbarEx = 2200,
	Rc_Skin_SliderbarEx = 2300,
	Rc_Skin_ScrollbarEx = 2400,
	Rc_Skin_EditboxEx = 2500,
	Rc_Cur_EditboxEx = 2599,
	Rc_Skin_ComboboxEx = 2600,
	Rc_Cur_ComboboxEx = 2699,
	Rc_Skin_ToolbarEx = 2700,
	Rc_Skin_ListboxEx = 2800,
	Rc_Skin_IcoListboxEx = 2900,
	Rc_Skin_SuperListboxEx = 3000,
	Rc_Cur_SuperListboxEx = 3001,
	Rc_Skin_TreeListEx = 3100,
	Rc_Skin_WebBrowserEx = 3200,
	Rc_Cur_WebBrowserEx = 3299,
	Rc_Skin_CalendarBoxEx = 3300,
	Rc_Skin_ColorPickEx = 3400,
	Rc_Skin_RichEditEx = 3500,
	Rc_Cur_RichEditEx = 3599,
	Rc_Skin_ExtendEx = 3600,
	Rc_Cur_ExtendEx = 3601,
	Rc_Skin_AnimationbuttonEx = 3700,
	Rc_Skin_AdvancedFormEx = 3800,
	Rc_Cur_AdvancedFormEx = 3801,
	Rc_Skin_SlideButtonEx = 3900,
	Rc_Skin_PieChartEx = 4000,
	Rc_Skin_BarChartEx = 4100,
	Rc_Skin_CurveChartEx = 4200,
	Rc_Skin_CandleChartEx = 4300,
	Rc_Skin_DrawPanelEx = 4400,
	Rc_Skin_ScrollLayoutBoxEx = 4500,
	Rc_Skin_MediaboxEx = 4600,
	Rc_Skin_CarouselBoxEx = 4700,
	Rc_Skin_SuperEditEx = 4800,
	Rc_Skin_ChatBoxEx = 4900,
	Rc_Skin_SuperChartEx = 5000,
	Rc_Skin_DataReportBoxEx = 5100,
	Rc_Skin_PageNavigBarEx = 5200,
		
	

};

enum Constant_Control
{
	Attr_Control_Visual = 0,//可视
	Attr_Control_Disabled,//禁止
	Attr_Control_Transparency,//透明度
	Attr_Control_Pierced,//鼠标穿透
	Attr_Control_FocusWeight,//焦点权重
	Attr_Control_Cursor,//鼠标指针
	Attr_Control_Sign,//标记值


	Event_Control_L_Click = 660,
	Event_Control_R_Click,
	Event_Control_L_Down,
	Event_Control_R_Down,
	Event_Control_L_Up,
	Event_Control_R_Up,
	Event_Control_L_DClick,
	Event_Control_R_DClick,
	Event_Control_MouseMove,
	Event_Control_MouseEnterLeave,
	Event_Control_MouseWheel,
	Event_Control_keyEvent,
	Event_Control_CharInput,
	Event_Control_Focus,
	Event_Control_Other,
	Event_Control_Other_WindowEx_Minmaxinfo,
	Event_Control_GetElemEvent = 800,
	Event_Control_PublicEvent,
	Event_Control_PrivateEvent,
	Event_Control_ElemEvent,
	Event_Control_TaskEvent,
	Event_Control_ParentWinRefreshEvent,



	Event_Control_MBUTTONUP = 1001,
	Event_Control_MBUTTONDBLCLK,
	Event_Control_MBUTTONDown,
	Event_Control_SETCURSOR,
	Event_Control_GETMINMAXINFO,
	Event_Control_PositionChange,
	Event_Control_SizeChenge,
	Event_Control_Refresh,
	Event_Control_RefreshToWindow,
	Event_Control_Disabled,
	Event_Control_Visual,
	Event_Control_CreateOver,
	Event_Control_Destroy,
	Event_Control_SettleContainer,

	FocusMode_LoseFocus = 0,
	FocusMode_GetFocus = 2,
	Focusrc_L_Down = 0,
	Focusrc_api_set = 0,
	Focusrc_change = 0,

	Event_Elem_layout = 65535,
	Event_Elem_LC,
	Event_Elem_RC,
	Event_Elem_LUp,
	Event_Elem_RUp,
	Event_Elem_LDwon,
	Event_Elem_RDwon,
	Event_Elem_LDC,
	Event_Elem_RDC,
	Event_Elem_Hit,
	Event_Elem_Leave,
	Event_Elem_SetCur,


	Event_Elem_Edit_WillIn = 65557,
	Event_Elem_Edit_WillOut,
	Event_Elem_Edit_In,
	Event_Elem_Edit_Out,
	Event_Elem_Slid_change = 65559,


};

enum Constant_AdvancedFormEx {
	/*-------------------------------AdvancedFormEx私有属性常量-------------------------------*/
	AdvancedFormEx_Attr_Star = 6,
	AdvancedFormEx_Attr_Skin, //皮肤
	AdvancedFormEx_Attr_CurrentItem, //现行选中项
	AdvancedFormEx_Attr_CurrentColumn, //现行选中列
	AdvancedFormEx_Attr_HeaderRowNum, //表头行数
	AdvancedFormEx_Attr_HeaderColumnNum, //表头列数
	AdvancedFormEx_Attr_BottomFixedRow, //底悬浮行数
	AdvancedFormEx_Attr_RightFixedColumn, //右悬浮列数
	AdvancedFormEx_Attr_AdjustmentMode, //调整模式
	AdvancedFormEx_Attr_SelectMode, //选中模式
	AdvancedFormEx_Attr_EntireLine, //整行选择
	AdvancedFormEx_Attr_EventMode,//事件模式
	AdvancedFormEx_Attr_EditMode,//编辑模式
	AdvancedFormEx_Attr_TreeType, //树形表格
	AdvancedFormEx_Attr_LineMode, //表格线模式
	AdvancedFormEx_Attr_ScrollBarMode, //滚动条方式
	AdvancedFormEx_Attr_ItemData,
	AdvancedFormEx_Attr_ElemeData,
	AdvancedFormEx_Attr_LayoutData,
	AdvancedFormEx_Attr_End,
	/*-------------------------------AdvancedFormEx私有属性常量结束-------------------------------*/
		/*-------------------------------AdvancedFormEx私有事件常量-------------------------------*/
	Event_AdvancedFormEx_Item_L_Click = 644,
	Event_AdvancedFormEx_Item_R_Click,
	Event_AdvancedFormEx_Item_L_Up,
	Event_AdvancedFormEx_Item_R_Up,
	Event_AdvancedFormEx_Item_L_Down,
	Event_AdvancedFormEx_Item_R_Down,
	Event_AdvancedFormEx_Item_L_DClick,
	Event_AdvancedFormEx_Item_R_DClick,
	Event_AdvancedFormEx_Item_MouseIn,
	Event_AdvancedFormEx_Item_MouseOut,
	Event_AdvancedFormEx_Item_WillEnterEdit,
	Event_AdvancedFormEx_Item_WillExitEdit,
	Event_AdvancedFormEx_Item_EnterEdit,
	Event_AdvancedFormEx_Item_ExitEdit,
	Event_AdvancedFormEx_Item_SizeChanged,
	Event_AdvancedFormEx_Item_FoldChanged,
	/*-------------------------------AdvancedFormEx私有事件常量结束-------------------------------*/

	/*-------------------------------AdvancedFormEx其他常量-------------------------------*/

	/*-------------------------------AdvancedFormEx其他常量结束-------------------------------*/
};

enum Constant_AnimationbuttonEx {
	/*-------------------------------AnimationbuttonEx私有属性常量-------------------------------*/
	AnimationbuttonEx_Attr_Star = 6,
	AnimationbuttonEx_Attr_Skin, //皮肤
	AnimationbuttonEx_Attr_EnableAnimation, //启用动画
	AnimationbuttonEx_Attr_Icon, //图标
	AnimationbuttonEx_Attr_IconWidth, //图标宽
	AnimationbuttonEx_Attr_IconHeight, //图标高
	AnimationbuttonEx_Attr_Align, //图标居上
	AnimationbuttonEx_Attr_Title, //标题
	AnimationbuttonEx_Attr_Font, //字体
	AnimationbuttonEx_Attr_FontClour, //字体色
	AnimationbuttonEx_Attr_Title_A, //标题
	AnimationbuttonEx_Attr_ElemeData,
	AnimationbuttonEx_Attr_LayoutData,
	AnimationbuttonEx_Attr_End,
	/*-------------------------------AnimationbuttonEx私有属性常量结束-------------------------------*/
		/*-------------------------------AnimationbuttonEx私有事件常量-------------------------------*/

		/*-------------------------------AnimationbuttonEx私有事件常量结束-------------------------------*/

		/*-------------------------------AnimationbuttonEx其他常量-------------------------------*/

		/*-------------------------------AnimationbuttonEx其他常量结束-------------------------------*/
};

enum Constant_BarChartEx {
	/*-------------------------------BarChartEx私有属性常量-------------------------------*/
	BarChartEx_Attr_Star = 6,
	BarChartEx_Attr_Skin, //皮肤
	BarChartEx_Attr_ChartStyle,
	BarChartEx_Attr_TemplateH,
	BarChartEx_Attr_ColorHA,
	BarChartEx_Attr_GridStyleH,
	BarChartEx_Attr_GridColorH,
	BarChartEx_Attr_FontH,
	BarChartEx_Attr_FontColorH,
	BarChartEx_Attr_TemplateV,
	BarChartEx_Attr_MiniV,
	BarChartEx_Attr_MaxV,
	BarChartEx_Attr_ColorVA,
	BarChartEx_Attr_GridStyleV,
	BarChartEx_Attr_GridColorV,
	BarChartEx_Attr_FontV,
	BarChartEx_Attr_FontColorV,
	BarChartEx_Attr_LeftReserve,
	BarChartEx_Attr_TopReserve,
	BarChartEx_Attr_RightReserve,
	BarChartEx_Attr_BottomReserve,
	BarChartEx_Attr_HotColumn,
	BarChartEx_Attr_GraphWidth,
	BarChartEx_Attr_GraphSpace,
	BarChartEx_Attr_TemplateH_A,
	BarChartEx_Attr_TemplateV_A,
	BarChartEx_Attr_GraphData,//图例管理
	BarChartEx_Attr_ElemeData,//扩展元素
	BarChartEx_Attr_LayoutData,
	BarChartEx_Attr_End,
	/*-------------------------------BarChartEx私有属性常量结束-------------------------------*/
		/*-------------------------------BarChartEx私有事件常量-------------------------------*/
	Event_BarChartEx_ChartEvent = 659,
	/*-------------------------------BarChartEx私有事件常量结束-------------------------------*/

	/*-------------------------------BarChartEx其他常量-------------------------------*/

	/*-------------------------------BarChartEx其他常量结束-------------------------------*/
};
enum Constant_ButtonEx {
	/*-------------------------------ButtonEx私有属性常量-------------------------------*/
	ButtonEx_Attr_Star = 6,
	ButtonEx_Attr_Skin, //皮肤
	ButtonEx_Attr_Title, //标题
	ButtonEx_Attr_Font, //字体
	ButtonEx_Attr_FontClour, //字体色
	ButtonEx_Attr_AnimationParam,
	ButtonEx_Attr_Title_A, //标题
	ButtonEx_Attr_ElemeData,
	ButtonEx_Attr_LayoutData,
	ButtonEx_Attr_End,
	/*-------------------------------ButtonEx私有属性常量结束-------------------------------*/
		/*-------------------------------ButtonEx私有事件常量-------------------------------*/

		/*-------------------------------ButtonEx私有事件常量结束-------------------------------*/

		/*-------------------------------ButtonEx其他常量-------------------------------*/

		/*-------------------------------ButtonEx其他常量结束-------------------------------*/
};
enum Constant_CalendarBoxEx {
	/*-------------------------------CalendarBoxEx私有属性常量-------------------------------*/
	CalendarBoxEx_Attr_Star = 6,
	CalendarBoxEx_Attr_Skin, //皮肤
	CalendarBoxEx_Attr_MiniDate, //最小日期
	CalendarBoxEx_Attr_MaxDate, //最大日期
	CalendarBoxEx_Attr_SelectionDate, //选中日期
	CalendarBoxEx_Attr_TitleFont, //标题字体
	CalendarBoxEx_Attr_WeekFont, //星期字体
	CalendarBoxEx_Attr_DayFont, //日期字体
	CalendarBoxEx_Attr_TimeFont, //时间字体
	CalendarBoxEx_Attr_TitleClour, //标题色
	CalendarBoxEx_Attr_WeekClour, //星期色
	CalendarBoxEx_Attr_WeekendWeekClour, //双休星期色
	CalendarBoxEx_Attr_DayClour, //日期色
	CalendarBoxEx_Attr_WeekendClour, //双休日期色
	CalendarBoxEx_Attr_OtherMonthClour, //非本月色
	CalendarBoxEx_Attr_TimeFontClour,//时间字体色
	CalendarBoxEx_Attr_OnlyThisMonth, //只显示本月
	CalendarBoxEx_Attr_TimeMode, //时间调整模式
	CalendarBoxEx_Attr_AutoSelect,//自动选中
	CalendarBoxEx_Attr_TitleHeight,//标题高度
	CalendarBoxEx_Attr_WeekHeight,//星期高度
	CalendarBoxEx_Attr_TimeRegulatorHeight,//时间调节器高度
	CalendarBoxEx_Attr_PartnerHeight,//伙伴高度
	CalendarBoxEx_Attr_Language,//语言
	CalendarBoxEx_Attr_MiniDate_A, //最小日期
	CalendarBoxEx_Attr_MaxDate_A, //最大日期
	CalendarBoxEx_Attr_SelectionDate_A, //选中日期
	CalendarBoxEx_Attr_ItemData,
	CalendarBoxEx_Attr_ElemeData,
	CalendarBoxEx_Attr_LayoutData,
	CalendarBoxEx_Attr_End,
	/*-------------------------------CalendarBoxEx私有属性常量结束-------------------------------*/
		/*-------------------------------CalendarBoxEx私有事件常量-------------------------------*/
	Event_CalendarBoxEx_Item_L_Click = 650,
	Event_CalendarBoxEx_Item_R_Click,
	Event_CalendarBoxEx_Item_L_Up,
	Event_CalendarBoxEx_Item_R_Up,
	Event_CalendarBoxEx_Item_L_Down,
	Event_CalendarBoxEx_Item_R_Down,
	Event_CalendarBoxEx_Item_L_DClick,
	Event_CalendarBoxEx_Item_R_DClick,
	Event_CalendarBoxEx_Item_MouseIn,
	Event_CalendarBoxEx_Item_MouseOut,
	/*-------------------------------CalendarBoxEx私有事件常量结束-------------------------------*/

	/*-------------------------------CalendarBoxEx其他常量-------------------------------*/

	/*-------------------------------CalendarBoxEx其他常量结束-------------------------------*/
};
enum Constant_CandleChartEx {
	/*-------------------------------CandleChartEx私有属性常量-------------------------------*/
	CandleChartEx_Attr_Star = 6,
	CandleChartEx_Attr_Skin, //皮肤
	CandleChartEx_Attr_ChartStyle,
	CandleChartEx_Attr_TemplateH,
	CandleChartEx_Attr_ColorHA,
	CandleChartEx_Attr_GridStyleH,
	CandleChartEx_Attr_GridColorH,
	CandleChartEx_Attr_FontH,
	CandleChartEx_Attr_FontColorH,
	CandleChartEx_Attr_TemplateV,
	CandleChartEx_Attr_MiniV,
	CandleChartEx_Attr_MaxV,
	CandleChartEx_Attr_ColorVA,
	CandleChartEx_Attr_GridStyleV,
	CandleChartEx_Attr_GridColorV,
	CandleChartEx_Attr_FontV,
	CandleChartEx_Attr_FontColorV,
	CandleChartEx_Attr_LeftReserve,
	CandleChartEx_Attr_TopReserve,
	CandleChartEx_Attr_RightReserve,
	CandleChartEx_Attr_BottomReserve,
	CandleChartEx_Attr_HotColumn,
	CandleChartEx_Attr_GraphWidth,
	CandleChartEx_Attr_NegativeHollow,
	CandleChartEx_Attr_NegativeColor,
	CandleChartEx_Attr_PositiveHollow,
	CandleChartEx_Attr_PositiveColor,
	CandleChartEx_Attr_TemplateH_A,
	CandleChartEx_Attr_TemplateV_A,
	CandleChartEx_Attr_GraphData,//图例管理
	CandleChartEx_Attr_ElemeData,//扩展元素
	CandleChartEx_Attr_LayoutData,
	CandleChartEx_Attr_End,
	/*-------------------------------CandleChartEx私有属性常量结束-------------------------------*/
		/*-------------------------------CandleChartEx私有事件常量-------------------------------*/
	Event_CandleChartEx_ChartEvent = 659,
	/*-------------------------------CandleChartEx私有事件常量结束-------------------------------*/

	/*-------------------------------CandleChartEx其他常量-------------------------------*/

	/*-------------------------------CandleChartEx其他常量结束-------------------------------*/
};
enum Constant_ChoiceboxEx {
	/*-------------------------------ChoiceboxEx私有属性常量-------------------------------*/
	ChoiceboxEx_Attr_Star = 6,
	ChoiceboxEx_Attr_Skin, //皮肤
	ChoiceboxEx_Attr_Selected, //选中
	ChoiceboxEx_Attr_Title, //标题
	ChoiceboxEx_Attr_Font, //字体
	ChoiceboxEx_Attr_FontClour, //字体色
	ChoiceboxEx_Attr_AnimationParam,
	ChoiceboxEx_Attr_Title_A, //标题
	ChoiceboxEx_Attr_SelectMode,
	ChoiceboxEx_Attr_SelectState,
	ChoiceboxEx_Attr_ElemeData,
	ChoiceboxEx_Attr_LayoutData,
	ChoiceboxEx_Attr_End,
	/*-------------------------------ChoiceboxEx私有属性常量结束-------------------------------*/
		/*-------------------------------ChoiceboxEx私有事件常量-------------------------------*/
	Event_ChoiceboxEx_SelectChanged = 659,
		/*-------------------------------ChoiceboxEx私有事件常量结束-------------------------------*/

		/*-------------------------------ChoiceboxEx其他常量-------------------------------*/

		/*-------------------------------ChoiceboxEx其他常量结束-------------------------------*/
};
enum Constant_ColorPickEx {
	/*-------------------------------ColorPickEx私有属性常量-------------------------------*/
	ColorPickEx_Attr_Star = 6,
	ColorPickEx_Attr_Skin, //皮肤
	ColorPickEx_Attr_Font, //字体
	ColorPickEx_Attr_FontClour, //字体色
	ColorPickEx_Attr_NowClour, //当前色
	ColorPickEx_Attr_DragTrace, //调整跟踪
	ColorPickEx_Attr_ClourMode, //颜色模式
	ColorPickEx_Attr_QuickClours, //备选色
	ColorPickEx_Attr_Style, //样式
	ColorPickEx_Attr_RegulatorHeight,//调节器高度
	ColorPickEx_Attr_QuickClourSize,//备选色直径
	ColorPickEx_Attr_PartnerHeight,//伙伴高度
	ColorPickEx_Attr_ElemeData,
	ColorPickEx_Attr_LayoutData,
	ColorPickEx_Attr_End,
	/*-------------------------------ColorPickEx私有属性常量结束-------------------------------*/
		/*-------------------------------ColorPickEx私有事件常量-------------------------------*/
	Event_ColorPickEx_ColorChanged = 659,
	/*-------------------------------ColorPickEx私有事件常量结束-------------------------------*/

	/*-------------------------------ColorPickEx其他常量-------------------------------*/

	/*-------------------------------ColorPickEx其他常量结束-------------------------------*/
};
enum Constant_ComboboxEx {
	/*-------------------------------ComboboxEx私有属性常量-------------------------------*/
	ComboboxEx_Attr_Star = 6,
	ComboboxEx_Attr_Skin, //皮肤
	ComboboxEx_Attr_Content, //内容
	ComboboxEx_Attr_Align, //对齐方式
	ComboboxEx_Attr_InputMode, //输入方式
	ComboboxEx_Attr_MaxInput, //最大容许长度
	ComboboxEx_Attr_PasswordSubstitution, //密码替换符
	ComboboxEx_Attr_CursorColor, //光标色
	ComboboxEx_Attr_Font, //字体
	ComboboxEx_Attr_FontClour, //字体色
	ComboboxEx_Attr_SelectedFontColor, //选中字体色
	ComboboxEx_Attr_SelectedColor, //选中背景色
	ComboboxEx_Attr_LeftReservation, //左预留
	ComboboxEx_Attr_RightReservation, //右预留
	ComboboxEx_Attr_MenuTableWidth, //菜单项目宽
	ComboboxEx_Attr_MenuTableHeight, //菜单项目高
	ComboboxEx_Attr_MenuFont, //菜单字体
	ComboboxEx_Attr_MenuFontClour, //菜单字体色
	ComboboxEx_Attr_MenuDisabledFontClour, //菜单禁止字体色
	ComboboxEx_Attr_MenuTransparency, //菜单透明度
	ComboboxEx_Attr_MenuLanguage, //菜单语言
	ComboboxEx_Attr_DownListWidth, //列表宽度
	ComboboxEx_Attr_DownListMaxHeight, //列表最大高
	ComboboxEx_Attr_DownListCurrentItem, //列表现行选中项
	ComboboxEx_Attr_DownListIconWidth, //列表图标宽
	ComboboxEx_Attr_DownListIconHeight, //列表图标高
	ComboboxEx_Attr_DownListFont, //列表字体
	ComboboxEx_Attr_DownListAlternate, //列表隔行换色
	ComboboxEx_Attr_DownListScrollBarMode, //列表滚动条方式
	ComboboxEx_Attr_DownListTransparency, //列表透明度
	ComboboxEx_Attr_DownListItemData, //列表列表项目数据
	ComboboxEx_Attr_Content_A, //内容
	ComboboxEx_Attr_PasswordSubstitution_A, //密码替换符
	ComboboxEx_Attr_ElemeData,
	ComboboxEx_Attr_LayoutData,
	ComboboxEx_Attr_End,
	/*-------------------------------ComboboxEx私有属性常量结束-------------------------------*/
		/*-------------------------------ComboboxEx私有事件常量-------------------------------*/
	Event_ComboboxEx_ContentChanged = 657,
	Event_ComboboxEx_CurrentItemWillChange,
	Event_ComboboxEx_CurrentItemChanged,

	/*-------------------------------ComboboxEx私有事件常量结束-------------------------------*/

	/*-------------------------------ComboboxEx其他常量-------------------------------*/

	/*-------------------------------ComboboxEx其他常量结束-------------------------------*/
};
enum Constant_CurveChartEx {
	/*-------------------------------CurveChartEx私有属性常量-------------------------------*/
	CurveChartEx_Attr_Star = 6,
	CurveChartEx_Attr_Skin, //皮肤
	CurveChartEx_Attr_ChartStyle,
	CurveChartEx_Attr_TemplateH,
	CurveChartEx_Attr_ColorHA,
	CurveChartEx_Attr_GridStyleH,
	CurveChartEx_Attr_GridColorH,
	CurveChartEx_Attr_FontH,
	CurveChartEx_Attr_FontColorH,
	CurveChartEx_Attr_TemplateV,
	CurveChartEx_Attr_MiniV,
	CurveChartEx_Attr_MaxV,
	CurveChartEx_Attr_ColorVA,
	CurveChartEx_Attr_GridStyleV,
	CurveChartEx_Attr_GridColorV,
	CurveChartEx_Attr_FontV,
	CurveChartEx_Attr_FontColorV,
	CurveChartEx_Attr_LeftReserve,
	CurveChartEx_Attr_TopReserve,
	CurveChartEx_Attr_RightReserve,
	CurveChartEx_Attr_BottomReserve,
	CurveChartEx_Attr_HotColumn,
	CurveChartEx_Attr_TemplateH_A,
	CurveChartEx_Attr_TemplateV_A,
	CurveChartEx_Attr_GraphData,//图例管理
	CurveChartEx_Attr_ElemeData,//扩展元素
	CurveChartEx_Attr_LayoutData,
	CurveChartEx_Attr_End,
	/*-------------------------------CurveChartEx私有属性常量结束-------------------------------*/
		/*-------------------------------CurveChartEx私有事件常量-------------------------------*/
	Event_CurveChartEx_ChartEvent = 659,
	/*-------------------------------CurveChartEx私有事件常量结束-------------------------------*/

	/*-------------------------------CurveChartEx其他常量-------------------------------*/

	/*-------------------------------CurveChartEx其他常量结束-------------------------------*/
};

enum Constant_DrawPanelEx {
	/*-------------------------------DrawPanelEx私有属性常量-------------------------------*/
	DrawPanelEx_Attr_Star = 6,
	DrawPanelEx_Attr_BackColor,
	DrawPanelEx_Attr_LineColor,
	DrawPanelEx_Attr_ElemeData,
	DrawPanelEx_Attr_LayoutData,
	DrawPanelEx_Attr_DrawPanelEx_Attr_End,
	/*-------------------------------DrawPanelEx私有属性常量结束-------------------------------*/
		/*-------------------------------DrawPanelEx私有事件常量-------------------------------*/
	Event_DrawPanelEx_DrawEvent = 659,
	/*-------------------------------DrawPanelEx私有事件常量结束-------------------------------*/

	/*-------------------------------DrawPanelEx其他常量-------------------------------*/

	/*-------------------------------DrawPanelEx其他常量结束-------------------------------*/
};
enum Constant_EditboxEx {
	/*-------------------------------EditboxEx私有属性常量-------------------------------*/
	EditboxEx_Attr_Star = 6,
	EditboxEx_Attr_Skin, //皮肤
	EditboxEx_Attr_Content, //内容
	EditboxEx_Attr_Align, //对齐方式
	EditboxEx_Attr_InputMode, //输入方式
	EditboxEx_Attr_MaxInput, //最大容许长度
	EditboxEx_Attr_PasswordSubstitution, //密码替换符
	EditboxEx_Attr_CursorColor, //光标色
	EditboxEx_Attr_Font, //字体
	EditboxEx_Attr_FontClour, //字体色
	EditboxEx_Attr_SelectedFontColor, //选中字体色
	EditboxEx_Attr_SelectedColor, //选中背景色
	EditboxEx_Attr_LeftReservation, //左预留
	EditboxEx_Attr_RightReservation, //右预留
	EditboxEx_Attr_Multiline, //是否容许多行
	EditboxEx_Attr_Wrapped, //自动换行
	EditboxEx_Attr_ScrollBarMode, //滚动条方式
	EditboxEx_Attr_MenuTableWidth, //菜单项目宽
	EditboxEx_Attr_MenuTableHeight, //菜单项目高
	EditboxEx_Attr_MenuFont, //菜单字体
	EditboxEx_Attr_MenuFontClour, //菜单字体色
	EditboxEx_Attr_MenuDisabledFontClour, //菜单禁止字体色
	EditboxEx_Attr_MenuTransparency, //菜单透明度
	EditboxEx_Attr_MenuLanguage, //菜单语言
	EditboxEx_Attr_Content_A, //内容
	EditboxEx_Attr_PasswordSubstitution_A, //密码替换符
	EditboxEx_Attr_ElemeData,
	EditboxEx_Attr_LayoutData,
	EditboxEx_Attr_End,
	/*-------------------------------EditboxEx私有属性常量结束-------------------------------*/
		/*-------------------------------EditboxEx私有事件常量-------------------------------*/
	Event_EditboxEx_ContentChanged = 659,
	/*-------------------------------EditboxEx私有事件常量结束-------------------------------*/

	/*-------------------------------EditboxEx其他常量-------------------------------*/

	/*-------------------------------EditboxEx其他常量结束-------------------------------*/
};
enum Constant_ExtendEx {
	/*-------------------------------ExtendEx私有属性常量-------------------------------*/
	ExtendEx_Attr_Star = 6,
	ExtendEx_Attr_ExtendAttr, //扩展属性表
	ExtendEx_Attr_ElemeData,
	ExtendEx_Attr_LayoutData,
	ExtendEx_Attr_End,
	/*-------------------------------ExtendEx私有属性常量结束-------------------------------*/
		/*-------------------------------ExtendEx私有事件常量-------------------------------*/
	Event_ExtendEx_ExtendEvent = 659,
	/*-------------------------------ExtendEx私有事件常量结束-------------------------------*/

	/*-------------------------------ExtendEx其他常量-------------------------------*/

	/*-------------------------------ExtendEx其他常量结束-------------------------------*/
};
enum Constant_FilterEx {
	/*-------------------------------FilterEx私有属性常量-------------------------------*/
	FilterEx_Attr_Star = 6,
	FilterEx_Attr_FilterExMode,// 滤镜参数
	FilterEx_Attr_Parameter1,// 滤镜参数1
	FilterEx_Attr_Parameter2,// 滤镜参数2
	FilterEx_Attr_Parameter3,// 滤镜参数3
	FilterEx_Attr_Parameter4,// 滤镜参数4
	FilterEx_Attr_ElemeData,
	FilterEx_Attr_LayoutData,
	FilterEx_Attr_End,
	/*-------------------------------FilterEx私有属性常量结束-------------------------------*/
		/*-------------------------------FilterEx私有事件常量-------------------------------*/

		/*-------------------------------FilterEx私有事件常量结束-------------------------------*/

		/*-------------------------------FilterEx其他常量-------------------------------*/

		/*-------------------------------FilterEx其他常量结束-------------------------------*/
};
enum Constant_IcoListboxEx {
	/*-------------------------------IcoListboxEx私有属性常量-------------------------------*/
	IcoListboxEx_Attr_Star = 6,
	IcoListboxEx_Attr_Skin, //皮肤
	IcoListboxEx_Attr_CurrentItem, //现行选中项
	IcoListboxEx_Attr_TableWidth, //表项宽度
	IcoListboxEx_Attr_TableHeight, //表项高度
	IcoListboxEx_Attr_HSpacing, //横间距
	IcoListboxEx_Attr_LSpacing, //纵间距
	IcoListboxEx_Attr_IconWidth, //图标宽
	IcoListboxEx_Attr_IconHeight, //图标高
	IcoListboxEx_Attr_Font, //字体
	IcoListboxEx_Attr_PageLayout, //布局模式
	IcoListboxEx_Attr_Align, //对齐方式
	IcoListboxEx_Attr_SelectMode, //选中模式
	IcoListboxEx_Attr_ScrollBarMode, //滚动条方式
	IcoListboxEx_Attr_ItemData,
	IcoListboxEx_Attr_ElemeData,
	IcoListboxEx_Attr_LayoutData,
	IcoListboxEx_Attr_End,
	/*-------------------------------IcoListboxEx私有属性常量结束-------------------------------*/
		/*-------------------------------IcoListboxEx私有事件常量-------------------------------*/
	Event_IcoListboxEx_Item_L_Click = 650,
	Event_IcoListboxEx_Item_R_Click,
	Event_IcoListboxEx_Item_L_Up,
	Event_IcoListboxEx_Item_R_Up,
	Event_IcoListboxEx_Item_L_Down,
	Event_IcoListboxEx_Item_R_Down,
	Event_IcoListboxEx_Item_L_DClick,
	Event_IcoListboxEx_Item_R_DClick,
	Event_IcoListboxEx_Item_MouseIn,
	Event_IcoListboxEx_Item_MouseOut,
	/*-------------------------------IcoListboxEx私有事件常量结束-------------------------------*/

	/*-------------------------------IcoListboxEx其他常量-------------------------------*/

	/*-------------------------------IcoListboxEx其他常量结束-------------------------------*/
};
enum Constant_ImagebuttonEx {
	/*-------------------------------ImagebuttonEx私有属性常量-------------------------------*/
	ImagebuttonEx_Attr_Star = 6,
	ImagebuttonEx_Attr_Skin, //皮肤
	ImagebuttonEx_Attr_Selected, //选中
	ImagebuttonEx_Attr_AnimationParam,
	ImagebuttonEx_Attr_ElemeData,
	ImagebuttonEx_Attr_LayoutData,
	ImagebuttonEx_Attr_End,
	/*-------------------------------ImagebuttonEx私有属性常量结束-------------------------------*/
		/*-------------------------------ImagebuttonEx私有事件常量-------------------------------*/

		/*-------------------------------ImagebuttonEx私有事件常量结束-------------------------------*/

		/*-------------------------------ImagebuttonEx其他常量-------------------------------*/

		/*-------------------------------ImagebuttonEx其他常量结束-------------------------------*/
};
enum Constant_LabelEx {
	/*-------------------------------LabelEx私有属性常量-------------------------------*/
	LabelEx_Attr_Star = 6,
	LabelEx_Attr_Title, //标题
	LabelEx_Attr_Align, //对齐方式
	LabelEx_Attr_BackColor, //背景色
	LabelEx_Attr_Font, //字体
	LabelEx_Attr_FontClour, //字体色
	LabelEx_Attr_Title_A, //标题
	LabelEx_Attr_Rotate,
	LabelEx_Attr_ElemeData,
	LabelEx_Attr_LayoutData,
	LabelEx_Attr_End,
	/*-------------------------------LabelEx私有属性常量结束-------------------------------*/
		/*-------------------------------LabelEx私有事件常量-------------------------------*/

		/*-------------------------------LabelEx私有事件常量结束-------------------------------*/

		/*-------------------------------LabelEx其他常量-------------------------------*/

		/*-------------------------------LabelEx其他常量结束-------------------------------*/
};
enum Constant_ListboxEx {
	/*-------------------------------ListboxEx私有属性常量-------------------------------*/
	ListboxEx_Attr_Star = 6,
	ListboxEx_Attr_Skin, //皮肤
	ListboxEx_Attr_CurrentItem, //现行选中项
	ListboxEx_Attr_IconWidth, //图标宽
	ListboxEx_Attr_IconHeight, //图标高
	ListboxEx_Attr_Font, //字体
	ListboxEx_Attr_AlternateColor, //隔行换色
	ListboxEx_Attr_SelectMode, //选中模式
	ListboxEx_Attr_ScrollBarMode, //滚动条方式
	ListboxEx_Attr_ItemData, //列表项目数据
	ListboxEx_Attr_ElemeData,
	ListboxEx_Attr_LayoutData,
	ListboxEx_Attr_End,
	/*-------------------------------ListboxEx私有属性常量结束-------------------------------*/
		/*-------------------------------ListboxEx私有事件常量-------------------------------*/
	Event_ListboxEx_Item_L_Click = 650,
	Event_ListboxEx_Item_R_Click,
	Event_ListboxEx_Item_L_Up,
	Event_ListboxEx_Item_R_Up,
	Event_ListboxEx_Item_L_Down,
	Event_ListboxEx_Item_R_Down,
	Event_ListboxEx_Item_L_DClick,
	Event_ListboxEx_Item_R_DClick,
	Event_ListboxEx_Item_MouseIn,
	Event_ListboxEx_Item_MouseOut,
	/*-------------------------------ListboxEx私有事件常量结束-------------------------------*/

	/*-------------------------------ListboxEx其他常量-------------------------------*/

	/*-------------------------------ListboxEx其他常量结束-------------------------------*/
};


enum Constant_MenuEx {

	/*-------------------------------MenuEx私有属性常量-------------------------------*/
	MenuEx_Attr_IconWidth = 1, //图标宽
	MenuEx_Attr_IconHeight = 2, //图标高
	MenuEx_Attr_Transparency = 4, //透明度
	MenuEx_Attr_Skin = 8, //皮肤
	MenuEx_Attr_Font = 16, // 字体
	MenuEx_Attr_Cursor = 32, //鼠标指针
	MenuEx_Attr_MenuTag = 64, // 菜单标识值
	MenuEx_Attr_callback = 128, //回调
	MenuEx_Attr_MenuTableWidth = 256,
	MenuEx_Attr_MenuTableHeight = 512,
	MenuEx_Attr_FontClour = 1024,
	MenuEx_Attr_DisabledFontClour = 2048,
	/*-------------------------------MenuEx私有属性常量结束-------------------------------*/
/*-------------------------------MenuEx私有事件常量-------------------------------*/
Event_MenuEx_ExtendPop = 642,
Event_MenuEx_ExtendClose,
Event_MenuEx_ExtendMmeasure,
Event_MenuEx_ExtendDraw,
Event_MenuEx_WillPop,
Event_MenuEx_Pop,
Event_MenuEx_WillClose,
Event_MenuEx_Close,

Event_MenuEx_Item_L_Click,
Event_MenuEx_Item_R_Click,
Event_MenuEx_Item_L_Up,
Event_MenuEx_Item_R_Up,
Event_MenuEx_Item_L_Down,
Event_MenuEx_Item_R_Down,
Event_MenuEx_Item_L_DClick,
Event_MenuEx_Item_R_DClick,
Event_MenuEx_Item_MouseIn,
Event_MenuEx_Item_MouseOut,
/*-------------------------------MenuEx私有事件常量结束-------------------------------*/

/*-------------------------------MenuEx其他常量-------------------------------*/

/*-------------------------------MenuEx其他常量结束-------------------------------*/
};
enum Constant_DownlistEx {
	/*-------------------------------DownlistEx私有属性常量-------------------------------*/
	DownlistEx_Attr_IconWidth = 1, //列表图标宽
	DownlistEx_Attr_IconHeight = 2, //列表图标高
	DownlistEx_Attr_Skin = 4, //列表皮肤
	DownlistEx_Attr_Font = 8, //列表字体
	DownlistEx_Attr_Cursor = 16, //列表鼠标光标
	DownlistEx_Attr_AlternateColor = 32, //列表隔行换色
	DownlistEx_Attr_ScrollBarMode = 64, //列表滚动条方式
	DownlistEx_Attr_CurrentItem = 128, //列表现行选中项
	DownlistEx_Attr_Transparency = 256, //列表透明度
	DownlistEx_Attr_callback = 512, //列表事件回调
	DownlistEx_Attr_Tag = 1024, //列表标识值
	DownlistEx_Attr_ItemData = 2048, //项目数据 保留用作内部与组合框交互数据
	/*-------------------------------DownlistEx私有属性常量结束-------------------------------*/
/*-------------------------------DownlistEx私有事件常量-------------------------------*/
Event_DownlistEx_ExtendPop = 642,
Event_DownlistEx_ExtendClose,
Event_DownlistEx_ExtendMmeasure,
Event_DownlistEx_ExtendDraw,
Event_DownlistEx_WillPop,
Event_DownlistEx_Pop,
Event_DownlistEx_WillClose,
Event_DownlistEx_Close,
Event_DownlistEx_Item_L_Click,
Event_DownlistEx_Item_R_Click,
Event_DownlistEx_Item_L_Up,
Event_DownlistEx_Item_R_Up,
Event_DownlistEx_Item_L_Down,
Event_DownlistEx_Item_R_Down,
Event_DownlistEx_Item_L_DClick,
Event_DownlistEx_Item_R_DClick,
Event_DownlistEx_Item_MouseIn,
Event_DownlistEx_Item_MouseOut,
/*-------------------------------DownlistEx私有事件常量结束-------------------------------*/
		/*-------------------------------DownlistEx其他常量-------------------------------*/

		/*-------------------------------DownlistEx其他常量结束-------------------------------*/
};

enum Constant_MinutesboxEx {
	/*-------------------------------MinutesboxEx私有属性常量-------------------------------*/
	MinutesboxEx_Attr_Star = 6,
	MinutesboxEx_Attr_Skin, //皮肤
	MinutesboxEx_Attr_Picture, //图标
	MinutesboxEx_Attr_IconWidth, //图标宽
	MinutesboxEx_Attr_IconHeight, //图标高
	MinutesboxEx_Attr_Title, //标题
	MinutesboxEx_Attr_Style, //风格
	MinutesboxEx_Attr_Font, //字体
	MinutesboxEx_Attr_FontClour, //字体色
	MinutesboxEx_Attr_Title_A, //标题
	MinutesboxEx_Attr_ElemeData,
	MinutesboxEx_Attr_LayoutData,
	MinutesboxEx_Attr_End,
	/*-------------------------------MinutesboxEx私有属性常量结束-------------------------------*/
		/*-------------------------------MinutesboxEx私有事件常量-------------------------------*/

		/*-------------------------------MinutesboxEx私有事件常量结束-------------------------------*/

		/*-------------------------------MinutesboxEx其他常量-------------------------------*/

		/*-------------------------------MinutesboxEx其他常量结束-------------------------------*/
};
enum Constant_MultifunctionButtonEx {
	/*-------------------------------MultifunctionButtonEx私有属性常量-------------------------------*/
	MultifunctionButtonEx_Attr_Star = 6,
	MultifunctionButtonEx_Attr_Skin, //皮肤
	MultifunctionButtonEx_Attr_ButtonStyles, //按钮样式
	MultifunctionButtonEx_Attr_PartnerSize, //伙伴钮尺寸
	MultifunctionButtonEx_Attr_BackColor, //背景颜色
	MultifunctionButtonEx_Attr_Icon, //图标
	MultifunctionButtonEx_Attr_IconWidth, //图标宽
	MultifunctionButtonEx_Attr_IconHeight, //图标高
	MultifunctionButtonEx_Attr_Align, //对齐方式
	MultifunctionButtonEx_Attr_Title, //标题
	MultifunctionButtonEx_Attr_Font, //字体
	MultifunctionButtonEx_Attr_FontClour, //字体色
	MultifunctionButtonEx_Attr_Title_A, //标题
	MultifunctionButtonEx_Attr_ElemeData,
	MultifunctionButtonEx_Attr_LayoutData,
	MultifunctionButtonEx_Attr_End,
	/*-------------------------------MultifunctionButtonEx私有属性常量结束-------------------------------*/
		/*-------------------------------MultifunctionButtonEx私有事件常量-------------------------------*/
	Event_MultifunctionButtonEx_Button_L_Click = 650,
	Event_MultifunctionButtonEx_Button_R_Click,
	Event_MultifunctionButtonEx_Button_L_Up,
	Event_MultifunctionButtonEx_Button_R_Up,
	Event_MultifunctionButtonEx_Button_L_Down,
	Event_MultifunctionButtonEx_Button_R_Down,
	Event_MultifunctionButtonEx_Button_L_DClick,
	Event_MultifunctionButtonEx_Button_R_DClick,
	Event_MultifunctionButtonEx_Button_MouseIn,
	Event_MultifunctionButtonEx_Button_MouseOut,
	/*-------------------------------MultifunctionButtonEx私有事件常量结束-------------------------------*/

	/*-------------------------------MultifunctionButtonEx其他常量-------------------------------*/

	/*-------------------------------MultifunctionButtonEx其他常量结束-------------------------------*/
};
enum Constant_PictureBoxEx {
	/*-------------------------------PictureBoxEx私有属性常量-------------------------------*/
	PictureBoxEx_Attr_Star = 6,
	PictureBoxEx_Attr_Skin, //皮肤
	PictureBoxEx_Attr_Picture, //图片
	PictureBoxEx_Attr_MapMode, //底图方式
	PictureBoxEx_Attr_Angle, //圆角度
	PictureBoxEx_Attr_PlayAnimation, //播放动画
	PictureBoxEx_Attr_AllFrame, //总帧数
	PictureBoxEx_Attr_CurrentFrame, //当前帧
	PictureBoxEx_Attr_Rotate,//旋转度
	PictureBoxEx_Attr_ElemeData,
	PictureBoxEx_Attr_LayoutData,
	PictureBoxEx_Attr_End,
	/*-------------------------------PictureBoxEx私有属性常量结束-------------------------------*/
		/*-------------------------------PictureBoxEx私有事件常量-------------------------------*/
	Event_PieChartEx_ChartEvent = 659,
	/*-------------------------------PictureBoxEx私有事件常量结束-------------------------------*/

	/*-------------------------------PictureBoxEx其他常量-------------------------------*/

	/*-------------------------------PictureBoxEx其他常量结束-------------------------------*/
};
enum Constant_PieChartEx {
	/*-------------------------------PieChartEx私有属性常量-------------------------------*/
	PieChartEx_Attr_Star = 6,
	PieChartEx_Attr_Skin, //皮肤
	PieChartEx_Attr_LeftReserve,
	PieChartEx_Attr_TopReserve,
	PieChartEx_Attr_RightReserve,
	PieChartEx_Attr_BottomReserve,
	PieChartEx_Attr_HotGraph,
	PieChartEx_Attr_GraphStar, //图例起始度
	PieChartEx_Attr_GraphWidth, //图例宽度
	PieChartEx_Attr_Zoom,
	PieChartEx_Attr_GraphData,//图例管理
	PieChartEx_Attr_ElemeData,//扩展元素
	PieChartEx_Attr_LayoutData,
	PieChartEx_Attr_End,
	/*-------------------------------PieChartEx私有属性常量结束-------------------------------*/
		/*-------------------------------PieChartEx私有事件常量-------------------------------*/

		/*-------------------------------PieChartEx私有事件常量结束-------------------------------*/

		/*-------------------------------PieChartEx其他常量-------------------------------*/

		/*-------------------------------PieChartEx其他常量结束-------------------------------*/
};
enum Constant_ProgressbarEx {
	/*-------------------------------ProgressbarEx私有属性常量-------------------------------*/
	ProgressbarEx_Attr_Star = 6,
	ProgressbarEx_Attr_Skin, //皮肤
	ProgressbarEx_Attr_Position, //位置
	ProgressbarEx_Attr_MiniPosition, //最小位置
	ProgressbarEx_Attr_MaxiPosition, //最大位置
	ProgressbarEx_Attr_Style, //进度样式
	ProgressbarEx_Attr_Direction, //进度方向
	ProgressbarEx_Attr_ElemeData,
	ProgressbarEx_Attr_LayoutData,
	ProgressbarEx_Attr_End,
	/*-------------------------------ProgressbarEx私有属性常量结束-------------------------------*/
		/*-------------------------------ProgressbarEx私有事件常量-------------------------------*/

		/*-------------------------------ProgressbarEx私有事件常量结束-------------------------------*/

		/*-------------------------------ProgressbarEx其他常量-------------------------------*/

		/*-------------------------------ProgressbarEx其他常量结束-------------------------------*/
};
enum Constant_RadiobuttonEx {
	/*-------------------------------RadiobuttonEx私有属性常量-------------------------------*/
	RadiobuttonEx_Attr_Star = 6,
	RadiobuttonEx_Attr_Skin, //皮肤
	RadiobuttonEx_Attr_Selected, //选中
	RadiobuttonEx_Attr_Title, //标题
	RadiobuttonEx_Attr_Font, //字体
	RadiobuttonEx_Attr_FontClour, //字体色
	RadiobuttonEx_Attr_AnimationParam,
	RadiobuttonEx_Attr_Title_A, //标题
	RadiobuttonEx_Attr_ElemeData,
	RadiobuttonEx_Attr_LayoutData,
	RadiobuttonEx_Attr_End,
	/*-------------------------------RadiobuttonEx私有属性常量结束-------------------------------*/
		/*-------------------------------RadiobuttonEx私有事件常量-------------------------------*/
	Event_RadiobuttonEx_SelectChanged = 659,
		/*-------------------------------RadiobuttonEx私有事件常量结束-------------------------------*/

		/*-------------------------------RadiobuttonEx其他常量-------------------------------*/

		/*-------------------------------RadiobuttonEx其他常量结束-------------------------------*/
};
enum Constant_RichEditEx {
	/*-------------------------------RichEditEx私有属性常量-------------------------------*/
	RichEditEx_Attr_Star = 6,
	RichEditEx_Attr_Skin, //皮肤
	RichEditEx_Attr_Content, //内容
	RichEditEx_Attr_Type,// 类型
	RichEditEx_Attr_BackTransparent, // 背景透明
	RichEditEx_Attr_BackClour, //背景色
	RichEditEx_Attr_CursorColor,// 光标色
	RichEditEx_Attr_DefaultCharFormat,//默认字符格式
	RichEditEx_Attr_DefaultParaFormat,//默认段落格式
	RichEditEx_Attr_InputMode, //输入方式
	RichEditEx_Attr_MaxInput, //最大容许长度
	RichEditEx_Attr_PasswordSubstitution, //密码替换符
	RichEditEx_Attr_Multiline, //是否容许多行
	RichEditEx_Attr_Wrapped, //自动换行
	RichEditEx_Attr_ScrollBarMode, //滚动条方式
	RichEditEx_Attr_MenuTableWidth, //菜单项目宽
	RichEditEx_Attr_MenuTableHeight, //菜单项目高
	RichEditEx_Attr_MenuFont, //菜单字体
	RichEditEx_Attr_MenuFontClour, //菜单字体色
	RichEditEx_Attr_MenuDisabledFontClour, //菜单禁止字体色
	RichEditEx_Attr_MenuTransparency, //菜单透明度
	RichEditEx_Attr_MenuLanguage, //菜单语言
	RichEditEx_Attr_Content_A,
	RichEditEx_Attr_PasswordSubstitution_A, //密码替换符
	RichEditEx_Attr_ElemeData,
	RichEditEx_Attr_LayoutData,
	RichEditEx_Attr_End,
	/*-------------------------------RichEditEx私有属性常量结束-------------------------------*/
		/*-------------------------------RichEditEx私有事件常量-------------------------------*/
	Event_RichEditEx_ContentChanged = 657,
	Event_RichEditEx_SelectionChanged,
	Event_RichEditEx_OtherExtendEvent,
	/*-------------------------------RichEditEx私有事件常量结束-------------------------------*/

	/*-------------------------------RichEditEx其他常量-------------------------------*/

	/*-------------------------------RichEditEx其他常量结束-------------------------------*/
};
enum Constant_ScrollbarEx {
	/*-------------------------------ScrollbarEx私有属性常量-------------------------------*/
	ScrollbarEx_Attr_Star = 6,
	ScrollbarEx_Attr_Skin, //皮肤
	ScrollbarEx_Attr_Position, //位置
	ScrollbarEx_Attr_MiniPosition, //最小位置
	ScrollbarEx_Attr_MaxiPosition, //最大位置
	ScrollbarEx_Attr_PageLength, //页长
	ScrollbarEx_Attr_RowChangeValue, //行改变值
	ScrollbarEx_Attr_PageChangeValue, //页改变值
	ScrollbarEx_Attr_ScrollChangeValue, //滚动改变值
	ScrollbarEx_Attr_DragTrace, //容许拖动跟踪
	ScrollbarEx_Attr_schedule, //纵向
	ScrollbarEx_Attr_ElemeData,
	ScrollbarEx_Attr_LayoutData,
	ScrollbarEx_Attr_End,
	/*-------------------------------ScrollbarEx私有属性常量结束-------------------------------*/
		/*-------------------------------ScrollbarEx私有事件常量-------------------------------*/
	Event_ScrollbarEx_PositionChanged = 659,
	/*-------------------------------ScrollbarEx私有事件常量结束-------------------------------*/

	/*-------------------------------ScrollbarEx其他常量-------------------------------*/

	/*-------------------------------ScrollbarEx其他常量结束-------------------------------*/
};
enum Constant_SelectthefolderEx {
	/*-------------------------------SelectthefolderEx私有属性常量-------------------------------*/
	SelectthefolderEx_Attr_Star = 6,
	SelectthefolderEx_Attr_Skin, //皮肤
	SelectthefolderEx_Attr_CurrentTable, //现行子夹
	SelectthefolderEx_Attr_TableSize, //子夹头尺寸
	SelectthefolderEx_Attr_Direction, //子夹头方向
	SelectthefolderEx_Attr_Spacing, //间距
	SelectthefolderEx_Attr_TableStyle, //子夹头样式
	SelectthefolderEx_Attr_Reserve, //保留属性
	SelectthefolderEx_Attr_IconWidth, //图标宽
	SelectthefolderEx_Attr_IconHeight, //图标高
	SelectthefolderEx_Attr_Font, //字体
	SelectthefolderEx_Attr_TableData, //子夹管理
	SelectthefolderEx_Attr_ElemeData,
	SelectthefolderEx_Attr_LayoutData,
	SelectthefolderEx_Attr_End,
	/*-------------------------------SelectthefolderEx私有属性常量结束-------------------------------*/
		/*-------------------------------SelectthefolderEx私有事件常量-------------------------------*/
	Event_SelectthefolderEx_Tab_WillChange = 648,
	Event_SelectthefolderEx_Tab_Changed,
	Event_SelectthefolderEx_Tab_L_Click,
	Event_SelectthefolderEx_Tab_R_Click,
	Event_SelectthefolderEx_Tab_L_Up,
	Event_SelectthefolderEx_Tab_R_Up,
	Event_SelectthefolderEx_Tab_L_Down,
	Event_SelectthefolderEx_Tab_R_Down,
	Event_SelectthefolderEx_Tab_L_DClick,
	Event_SelectthefolderEx_Tab_R_DClick,
	Event_SelectthefolderEx_Tab_MouseIn,
	Event_SelectthefolderEx_Tab_MouseOut,
	/*-------------------------------SelectthefolderEx私有事件常量结束-------------------------------*/

	/*-------------------------------SelectthefolderEx其他常量-------------------------------*/

	/*-------------------------------SelectthefolderEx其他常量结束-------------------------------*/
};
enum Constant_SlideButtonEx
{
	/*-------------------------------SlideButtonEx私有属性常量-------------------------------*/
	SlideButtonEx_Attr_Star = 6,
	SlideButtonEx_Attr_Skin, //皮肤
	SlideButtonEx_Attr_Selected, //选中
	SlideButtonEx_Attr_ElemeData,
	SlideButtonEx_Attr_LayoutData,
	SlideButtonEx_Attr_End,
	/*-------------------------------SlideButtonEx私有属性常量结束-------------------------------*/
		/*-------------------------------SlideButtonEx私有事件常量-------------------------------*/
	Event_SlideButtonEx_SelectChanged = 659,
	/*-------------------------------SlideButtonEx私有事件常量结束-------------------------------*/

	/*-------------------------------SlideButtonEx其他常量-------------------------------*/

	/*-------------------------------SlideButtonEx其他常量结束-------------------------------*/
};
enum Constant_SliderbarEx {
	/*-------------------------------SliderbarEx私有属性常量-------------------------------*/
	SliderbarEx_Attr_Star = 6,
	SliderbarEx_Attr_Skin, //皮肤
	SliderbarEx_Attr_Position, //位置
	SliderbarEx_Attr_Progress,//进度位置
	SliderbarEx_Attr_MiniPosition, //最小位置
	SliderbarEx_Attr_MaxiPosition, //最大位置
	SliderbarEx_Attr_ScrollChangeValue, //行改变值
	SliderbarEx_Attr_PageChangeValue, //页改变值
	SliderbarEx_Attr_DragTrace, //容许拖动跟踪
	SliderbarEx_Attr_Style, //样式
	SliderbarEx_Attr_ElemeData,
	SliderbarEx_Attr_LayoutData,
	SliderbarEx_Attr_End,
	/*-------------------------------SliderbarEx私有属性常量结束-------------------------------*/
		/*-------------------------------SliderbarEx私有事件常量-------------------------------*/
	Event_SliderbarEx_PositionChanged = 659,
	/*-------------------------------SliderbarEx私有事件常量结束-------------------------------*/

	/*-------------------------------SliderbarEx其他常量-------------------------------*/

	/*-------------------------------SliderbarEx其他常量结束-------------------------------*/
};
enum Constant_SuperbuttonEx {
	/*-------------------------------SuperbuttonEx私有属性常量-------------------------------*/
	SuperbuttonEx_Attr_Star = 6,
	SuperbuttonEx_Attr_Skin, //皮肤
	SuperbuttonEx_Attr_Selected, //选中
	SuperbuttonEx_Attr_Icon, //图标
	SuperbuttonEx_Attr_IconWidth, //图标宽
	SuperbuttonEx_Attr_IconHeight, //图标高
	SuperbuttonEx_Attr_Align, //对齐方式
	SuperbuttonEx_Attr_Title, //标题
	SuperbuttonEx_Attr_Font, //字体
	SuperbuttonEx_Attr_FontClour, //字体色
	SuperbuttonEx_Attr_AnimationParam,
	SuperbuttonEx_Attr_Title_A, //标题
	SuperbuttonEx_Attr_ElemeData,
	SuperbuttonEx_Attr_LayoutData,
	SuperbuttonEx_Attr_End,
	/*-------------------------------SuperbuttonEx私有属性常量结束-------------------------------*/
		/*-------------------------------SuperbuttonEx私有事件常量-------------------------------*/

		/*-------------------------------SuperbuttonEx私有事件常量结束-------------------------------*/

		/*-------------------------------SuperbuttonEx其他常量-------------------------------*/

		/*-------------------------------SuperbuttonEx其他常量结束-------------------------------*/
};
enum Constant_SuperListboxEx {
	/*-------------------------------SuperListboxEx私有属性常量-------------------------------*/
	SuperListboxEx_Attr_Star = 6,
	SuperListboxEx_Attr_Skin, //皮肤
	SuperListboxEx_Attr_CurrentItem, //现行选中项
	SuperListboxEx_Attr_CurrentColumn,//现行选中列
	SuperListboxEx_Attr_HeadHeight, //表头高度
	SuperListboxEx_Attr_HeadMode, //表头模式
	SuperListboxEx_Attr_AlternateColor, //隔行换色
	SuperListboxEx_Attr_SelectMode, //选中模式
	SuperListboxEx_Attr_EntireLine, //整行选择
	SuperListboxEx_Attr_EventMode,//事件模式 
	SuperListboxEx_Attr_EditMode,//事件模式 
	SuperListboxEx_Attr_LineMode, //表格线模式
	SuperListboxEx_Attr_ScrollBarMode, //滚动条方式
	SuperListboxEx_Attr_ItemData,
	SuperListboxEx_Attr_ElemeData,
	SuperListboxEx_Attr_LayoutData,
	SuperListboxEx_Attr_End,
	/*-------------------------------SuperListboxEx私有属性常量结束-------------------------------*/
		/*-------------------------------SuperListboxEx私有事件常量-------------------------------*/
	Event_SuperListboxEx_Item_L_Click = 645,
	Event_SuperListboxEx_Item_R_Click,
	Event_SuperListboxEx_Item_L_Up,
	Event_SuperListboxEx_Item_R_Up,
	Event_SuperListboxEx_Item_L_Down,
	Event_SuperListboxEx_Item_R_Down,
	Event_SuperListboxEx_Item_L_DClick,
	Event_SuperListboxEx_Item_R_DClick,
	Event_SuperListboxEx_Item_MouseIn,
	Event_SuperListboxEx_Item_MouseOut,
	Event_SuperListboxEx_Item_WillEnterEdit,
	Event_SuperListboxEx_Item_WillExitEdit,
	Event_SuperListboxEx_Item_EnterEdit,
	Event_SuperListboxEx_Item_ExitEdit,
	Event_SuperListboxEx_Item_SizeChanged,

	/*-------------------------------SuperListboxEx私有事件常量结束-------------------------------*/

	/*-------------------------------SuperListboxEx其他常量-------------------------------*/

	/*-------------------------------SuperListboxEx其他常量结束-------------------------------*/
};
enum Constant_ToolbarEx {
	/*-------------------------------ToolbarEx私有属性常量-------------------------------*/
	ToolbarEx_Attr_Star = 6,
	ToolbarEx_Attr_Skin, //皮肤
	ToolbarEx_Attr_BackMode, //背景样式
	ToolbarEx_Attr_schedule, //纵向
	ToolbarEx_Attr_Spacing, //间距
	ToolbarEx_Attr_IconWidth, //图标宽
	ToolbarEx_Attr_IconHeight, //图标高
	ToolbarEx_Attr_Font, //字体
	ToolbarEx_Attr_ButtonData, //按钮管理
	ToolbarEx_Attr_ElemeData,
	ToolbarEx_Attr_LayoutData,
	ToolbarEx_Attr_End,
	/*-------------------------------ToolbarEx私有属性常量结束-------------------------------*/
		/*-------------------------------ToolbarEx私有事件常量-------------------------------*/
	Event_ToolbarEx_Button_L_Click = 650,
	Event_ToolbarEx_Button_R_Click,
	Event_ToolbarEx_Button_L_Up,
	Event_ToolbarEx_Button_R_Up,
	Event_ToolbarEx_Button_L_Down,
	Event_ToolbarEx_Button_R_Down,
	Event_ToolbarEx_Button_L_DClick,
	Event_ToolbarEx_Button_R_DClick,
	Event_ToolbarEx_Button_MouseIn,
	Event_ToolbarEx_Button_MouseOut,
	/*-------------------------------ToolbarEx私有事件常量结束-------------------------------*/

	/*-------------------------------ToolbarEx其他常量-------------------------------*/

	/*-------------------------------ToolbarEx其他常量结束-------------------------------*/
};
enum Constant_TreeListEx {
	/*-------------------------------TreeListEx私有属性常量-------------------------------*/
	TreeListEx_Attr_Star = 6,
	TreeListEx_Attr_Skin, //皮肤
	TreeListEx_Attr_CurrentItem, //现行选中项
	TreeListEx_Attr_AutoSize, //项目自动扩展
	TreeListEx_Attr_FoldBtnWidth, //折叠钮宽度
	TreeListEx_Attr_FoldBtnHeight, //折叠钮高度
	TreeListEx_Attr_IconWidth, //图标宽
	TreeListEx_Attr_IconHeight, //图标高
	TreeListEx_Attr_Font, //字体
	TreeListEx_Attr_ChildMigration, //子级缩进
	TreeListEx_Attr_BackMigration, //背景缩进
	TreeListEx_Attr_ContentMigration, //内容缩进
	TreeListEx_Attr_SelectMode, //选中模式
	TreeListEx_Attr_LineMode, //表格线模式
	TreeListEx_Attr_ScrollBarMode, //滚动条方式
	TreeListEx_Attr_ItemData,
	TreeListEx_Attr_ElemeData,
	TreeListEx_Attr_LayoutData,
	TreeListEx_Attr_End,
	/*-------------------------------TreeListEx私有属性常量结束-------------------------------*/
		/*-------------------------------TreeListEx私有事件常量-------------------------------*/
	Event_TreeListEx_Item_L_Click = 649,
	Event_TreeListEx_Item_R_Click,
	Event_TreeListEx_Item_L_Up,
	Event_TreeListEx_Item_R_Up,
	Event_TreeListEx_Item_L_Down,
	Event_TreeListEx_Item_R_Down,
	Event_TreeListEx_Item_L_DClick,
	Event_TreeListEx_Item_R_DClick,
	Event_TreeListEx_Item_MouseIn,
	Event_TreeListEx_Item_MouseOut,
	Event_TreeListEx_Item_FoldChanged,
	/*-------------------------------TreeListEx私有事件常量结束-------------------------------*/

	/*-------------------------------TreeListEx其他常量-------------------------------*/

	/*-------------------------------TreeListEx其他常量结束-------------------------------*/
};
enum Constant_WebBrowserEx {
	/*-------------------------------WebBrowserEx私有属性常量-------------------------------*/
	WebBrowserEx_Attr_Star = 6,
	WebBrowserEx_Attr_BackgroundTransparent, //背景透明
	WebBrowserEx_Attr_MediaVolume, //媒体音量
	WebBrowserEx_Attr_ZoomFactor, //缩放度
	WebBrowserEx_Attr_NavigationToNewWind, //容许新窗口跳转
	WebBrowserEx_Attr_CookieEnabled, //启用Cookie
	WebBrowserEx_Attr_NpapiEnabled, //启用插件
	WebBrowserEx_Attr_MenuEnabled, //启用菜单
	WebBrowserEx_Attr_DragEnable, //拖拽模式
	WebBrowserEx_Attr_StartPage,//初始页面
	WebBrowserEx_Attr_UserAgent, //其它配置
	WebBrowserEx_Attr_Version, //版本
	WebBrowserEx_Attr_ElemeData,
	WebBrowserEx_Attr_LayoutData,
	WebBrowserEx_Attr_End,
	/*-------------------------------WebBrowserEx私有属性常量结束-------------------------------*/
	
	/*-------------------------------WebBrowserEx私有事件常量-------------------------------*/
	Event_WebBrowserEx_Navigation = 643,
	Event_WebBrowserEx_CreateView,
	Event_WebBrowserEx_URLChanged,
	Event_WebBrowserEx_TitleChanged,
	Event_WebBrowserEx_DocumentReady,
	Event_WebBrowserEx_LoadingFinish,
	Event_WebBrowserEx_Download,
	Event_WebBrowserEx_AlertBox,
	Event_WebBrowserEx_ConfirmBox,
	Event_WebBrowserEx_PromptBox,
	Event_WebBrowserEx_LoadUrlBegin,
	Event_WebBrowserEx_LoadUrlFail,
	Event_WebBrowserEx_LoadUrlEnd,
	Event_WebBrowserEx_NetResponse,
	Event_WebBrowserEx_JsCallResponse,
	Event_WebBrowserEx_Reserve1,
	Event_WebBrowserEx_Reserve2,
	/*-------------------------------WebBrowserEx私有事件常量结束-------------------------------*/

	/*-------------------------------WebBrowserEx其他常量-------------------------------*/

	/*-------------------------------WebBrowserEx其他常量结束-------------------------------*/
};
enum Constant_WindowEx {
	/*-------------------------------WindowEx私有属性常量-------------------------------*/
	WindowEx_Attr_Star = 6,
	WindowEx_Attr_Skin, //皮肤
	WindowEx_Attr_Icon, //图标
	WindowEx_Attr_Title, //标题
	WindowEx_Attr_Font, //字体
	WindowEx_Attr_FontClour, //字体色
	WindowEx_Attr_LayeredTransparency, //分层透明
	WindowEx_Attr_DragPositionMode, //拖动模式
	WindowEx_Attr_DragSizeMode, //尺寸调整模式
	WindowEx_Attr_MaxMode, //最大化模式
	WindowEx_Attr_ButtonData, //控制钮
	WindowEx_Attr_AnimationParam,
	WindowEx_Attr_Title_A,
	WindowEx_Attr_ElemeData,
	WindowEx_Attr_LayoutData,
	WindowEx_Attr_End,
	/*-------------------------------WindowEx私有属性常量结束-------------------------------*/

		/*-------------------------------WindowEx私有事件常量-------------------------------*/
	Event_WindowEx_FeedBackEvent = 649,
	Event_WindowEx_Button_L_Click,
	Event_WindowEx_Button_R_Click,
	Event_WindowEx_Button_L_Up,
	Event_WindowEx_Button_R_Up,
	Event_WindowEx_Button_L_Down,
	Event_WindowEx_Button_R_Down,
	Event_WindowEx_Button_L_DClick,
	Event_WindowEx_Button_R_DClick,
	Event_WindowEx_Button_MouseIn,
	Event_WindowEx_Button_MouseOut,
	/*-------------------------------WindowEx私有事件常量结束-------------------------------*/

	/*-------------------------------WindowEx其他常量-------------------------------*/

	/*-------------------------------WindowEx其他常量结束-------------------------------*/
};


enum Constant_ScrollLayoutBoxEx {
	/*-------------------------------ScrollLayoutBoxEx私有属性常量-------------------------------*/
	ScrollLayoutBoxEx_Attr_Star = 6,
	ScrollLayoutBoxEx_Attr_Skin, //皮肤
	ScrollLayoutBoxEx_Attr_Mode,
	ScrollLayoutBoxEx_Attr_HorizontalPosition,
	ScrollLayoutBoxEx_Attr_VerticalPosition,
	ScrollLayoutBoxEx_Attr_MaxHorizontalPosition,
	ScrollLayoutBoxEx_Attr_MaxVerticalPosition,
	ScrollLayoutBoxEx_Attr_Reserve1,
	ScrollLayoutBoxEx_Attr_Reserve2,
	ScrollLayoutBoxEx_Attr_Reserve3,
	ScrollLayoutBoxEx_Attr_Reserve4,
	ScrollLayoutBoxEx_Attr_Reserve5,
	ScrollLayoutBoxEx_Attr_Reserve6,
	ScrollLayoutBoxEx_Attr_ScrollBarMode,
	ScrollLayoutBoxEx_Attr_ElemeData,//扩展元素
	ScrollLayoutBoxEx_Attr_LayoutData,
	ScrollLayoutBoxEx_Attr_End,
	/*-------------------------------ScrollLayoutBoxEx私有属性常量结束-------------------------------*/
		/*-------------------------------ScrollLayoutBoxEx私有事件常量-------------------------------*/
	Event_ScrollLayoutBoxEx_Reserve1 = 654,
	Event_ScrollLayoutBoxEx_Reserve2,
	Event_ScrollLayoutBoxEx_Reserve3,
	Event_ScrollLayoutBoxEx_Reserve4,
	Event_ScrollLayoutBoxEx_Reserve5,
	Event_ScrollLayoutBoxEx_Reserve6,

	/*-------------------------------ScrollLayoutBoxEx私有事件常量结束-------------------------------*/

	/*-------------------------------ScrollLayoutBoxEx其他常量-------------------------------*/

	/*-------------------------------ScrollLayoutBoxEx其他常量结束-------------------------------*/
};


enum Constant_MediaboxEx {
	/*-------------------------------MediaboxEx私有属性常量-------------------------------*/
	MediaboxEx_Attr_Star = 6,
	MediaboxEx_Attr_Skin, //皮肤
	MediaboxEx_Attr_MediaFile,//影像文件
	MediaboxEx_Attr_Align,//对齐方式
	MediaboxEx_Attr_PlayState,//播放状态
	MediaboxEx_Attr_PlayMode,//播放模式
	MediaboxEx_Attr_TotalProgress,//总进度
	MediaboxEx_Attr_CurrentPosition,//当前进度
	MediaboxEx_Attr_volume, //音量
	MediaboxEx_Attr_PlaySpeed,//播放速度
	MediaboxEx_Attr_Reserve1,//保留1
	MediaboxEx_Attr_Reserve2,//保留2
	MediaboxEx_Attr_Reserve3,//保留3
	MediaboxEx_Attr_Reserve4,//保留4
	MediaboxEx_Attr_Reserve5,//保留5
	MediaboxEx_Attr_Reserve6,//保留6
	MediaboxEx_Attr_Reserve7,//保留7
	MediaboxEx_Attr_Reserve8,//保留8
	MediaboxEx_Attr_Reserve9,//保留9
	MediaboxEx_Attr_File_A,//影像文件A
	MediaboxEx_Attr_ElemeData,//扩展元素
	MediaboxEx_Attr_LayoutData,//布局器
	MediaboxEx_Attr_End,
	/*-------------------------------MediaboxEx私有属性常量结束-------------------------------*/
		/*-------------------------------MediaboxEx私有事件常量-------------------------------*/
	Event_MediaboxEx_PlayPositionChanged = 654,
	Event_MediaboxEx_RelatedEvent1,
	Event_MediaboxEx_RelatedEvent2,
	Event_MediaboxEx_RelatedEvent3,
	Event_MediaboxEx_RelatedEvent4,
	Event_MediaboxEx_RelatedEvent5,


	/*-------------------------------MediaboxEx私有事件常量结束-------------------------------*/

	/*-------------------------------MediaboxEx其他常量-------------------------------*/

	/*-------------------------------MediaboxEx其他常量结束-------------------------------*/
};


enum Constant_CarouselBoxEx {
	/*-------------------------------CarouselBoxEx私有属性常量-------------------------------*/
	CarouselBoxEx_Attr_Star = 6,
	CarouselBoxEx_Attr_Skin, //皮肤
	CarouselBoxEx_Attr_CurrentPosition, //当前进度
	CarouselBoxEx_Attr_Schedule, //纵向
	CarouselBoxEx_Attr_Spacing, //间距
	CarouselBoxEx_Attr_LayoutStyle,//布局样式
	CarouselBoxEx_Attr_LayoutParam, //布局参数
	CarouselBoxEx_Attr_CtrlModel,//控制模式
	CarouselBoxEx_Attr_CarouselMode,//轮播模式 页模式或连续滚动
	CarouselBoxEx_Attr_ScrollDirection,//滚动方向 正反
	CarouselBoxEx_Attr_AutoPlay, //自动播放 
	CarouselBoxEx_Attr_DurationTime,//停留时长
	CarouselBoxEx_Attr_FrameDelay,//帧延迟
	CarouselBoxEx_Attr_Step,//帧步进
	CarouselBoxEx_Attr_AlgorithmMode,//滚动算法
	CarouselBoxEx_Attr_ItemData, //列表项目数据
	CarouselBoxEx_Attr_ElemeData,
	CarouselBoxEx_Attr_LayoutData,
	CarouselBoxEx_Attr_End,
	/*-------------------------------CarouselBoxEx私有属性常量结束-------------------------------*/
		/*-------------------------------CarouselBoxEx私有事件常量-------------------------------*/
	Event_CarouselBoxEx_Item_L_Click = 649,
	Event_CarouselBoxEx_Item_R_Click,
	Event_CarouselBoxEx_Item_L_Up,
	Event_CarouselBoxEx_Item_R_Up,
	Event_CarouselBoxEx_Item_L_Down,
	Event_CarouselBoxEx_Item_R_Down,
	Event_CarouselBoxEx_Item_L_DClick,
	Event_CarouselBoxEx_Item_R_DClick,
	Event_CarouselBoxEx_Item_MouseIn,
	Event_CarouselBoxEx_Item_MouseOut,
	Event_CarouselBoxEx_CarouselEvent,
	/*-------------------------------CarouselBoxEx私有事件常量结束-------------------------------*/

	/*-------------------------------CarouselBoxEx其他常量-------------------------------*/

	/*-------------------------------CarouselBoxEx其他常量结束-------------------------------*/
};



enum Constant_SuperEditEx {
	/*-------------------------------SuperEditEx私有属性常量-------------------------------*/
	SuperEditEx_Attr_Star = 6,
	SuperEditEx_Attr_Skin, //皮肤
	SuperEditEx_Attr_MediaFile,//影像文件
	SuperEditEx_Attr_Kernel, //核心
	SuperEditEx_Attr_Attribute, //属性
	SuperEditEx_Attr_Data, //配置
	SuperEditEx_Attr_Kernel_A, //核心
	SuperEditEx_Attr_Attribute_A, //属性
	SuperEditEx_Attr_Data_A, //配置
	SuperEditEx_Attr_ElemeData,//扩展元素
	SuperEditEx_Attr_LayoutData,//布局器
	SuperEditEx_Attr_End,
	/*-------------------------------SuperEditEx私有属性常量结束-------------------------------*/
		/*-------------------------------SuperEditEx私有事件常量-------------------------------*/
	Event_SuperEditEx_EventNotification = 659,


	/*-------------------------------SuperEditEx私有事件常量结束-------------------------------*/

	/*-------------------------------SuperEditEx其他常量-------------------------------*/

	/*-------------------------------SuperEditEx其他常量结束-------------------------------*/
};

enum Constant_ChatBoxEx {
	/*-------------------------------ChatBoxEx私有属性常量-------------------------------*/
	ChatBoxEx_Attr_Star = 6,
	ChatBoxEx_Attr_Skin, //皮肤
	ChatBoxEx_Attr_MediaFile,//影像文件
	ChatBoxEx_Attr_Kernel, //核心
	ChatBoxEx_Attr_Attribute, //属性
	ChatBoxEx_Attr_Data, //配置
	ChatBoxEx_Attr_Kernel_A, //核心
	ChatBoxEx_Attr_Attribute_A, //属性
	ChatBoxEx_Attr_Data_A, //配置
	ChatBoxEx_Attr_ElemeData,//扩展元素
	ChatBoxEx_Attr_LayoutData,//布局器
	ChatBoxEx_Attr_End,
	/*-------------------------------ChatBoxEx私有属性常量结束-------------------------------*/
		/*-------------------------------ChatBoxEx私有事件常量-------------------------------*/
	Event_ChatBoxEx_EventNotification = 659,


	/*-------------------------------ChatBoxEx私有事件常量结束-------------------------------*/

	/*-------------------------------ChatBoxEx其他常量-------------------------------*/

	/*-------------------------------ChatBoxEx其他常量结束-------------------------------*/
};

enum Constant_SuperChartEx {
	/*-------------------------------SuperChartEx私有属性常量-------------------------------*/
	SuperChartEx_Attr_Star = 6,
	SuperChartEx_Attr_Skin, //皮肤
	SuperChartEx_Attr_MediaFile,//影像文件
	SuperChartEx_Attr_Kernel, //核心
	SuperChartEx_Attr_Attribute, //属性
	SuperChartEx_Attr_Data, //配置
	SuperChartEx_Attr_Kernel_A, //核心
	SuperChartEx_Attr_Attribute_A, //属性
	SuperChartEx_Attr_Data_A, //配置
	SuperChartEx_Attr_ElemeData,//扩展元素
	SuperChartEx_Attr_LayoutData,//布局器
	SuperChartEx_Attr_End,
	/*-------------------------------SuperChartEx私有属性常量结束-------------------------------*/
		/*-------------------------------SuperChartEx私有事件常量-------------------------------*/
	Event_SuperChartEx_EventNotification = 659,


	/*-------------------------------SuperChartEx私有事件常量结束-------------------------------*/

	/*-------------------------------SuperChartEx其他常量-------------------------------*/

	/*-------------------------------SuperChartEx其他常量结束-------------------------------*/
};


enum Constant_DataReportBoxEx {
	/*-------------------------------DataReportBoxEx私有属性常量-------------------------------*/
	DataReportBoxEx_Attr_Star = 6,
	DataReportBoxEx_Attr_Skin, //皮肤
	DataReportBoxEx_Attr_MediaFile,//影像文件
	DataReportBoxEx_Attr_Kernel, //核心
	DataReportBoxEx_Attr_Attribute, //属性
	DataReportBoxEx_Attr_Data, //配置
	DataReportBoxEx_Attr_Kernel_A, //核心
	DataReportBoxEx_Attr_Attribute_A, //属性
	DataReportBoxEx_Attr_Data_A, //配置
	DataReportBoxEx_Attr_ElemeData,//扩展元素
	DataReportBoxEx_Attr_LayoutData,//布局器
	DataReportBoxEx_Attr_End,
	/*-------------------------------DataReportBoxEx私有属性常量结束-------------------------------*/
		/*-------------------------------DataReportBoxEx私有事件常量-------------------------------*/
	Event_DataReportBoxEx_EventNotification = 659,


	/*-------------------------------DataReportBoxEx私有事件常量结束-------------------------------*/

	/*-------------------------------DataReportBoxEx其他常量-------------------------------*/

	/*-------------------------------DataReportBoxEx其他常量结束-------------------------------*/
};


enum Constant_PageNavigBarEx {
	/*-------------------------------PageNavigBarEx私有属性常量-------------------------------*/
	PageNavigBarEx_Attr_Star = 6,
	PageNavigBarEx_Attr_Skin, //皮肤
	PageNavigBarEx_Attr_Mode, //模式
	PageNavigBarEx_Attr_PageCount, //总页数
	PageNavigBarEx_Attr_CurrentPage, //当前页
	PageNavigBarEx_Attr_NavigBtnCount, //页码数量
	PageNavigBarEx_Attr_Align,//对齐方式
	PageNavigBarEx_Attr_Font, //字体
	PageNavigBarEx_Attr_FontColor, //字体色
	PageNavigBarEx_Attr_CurrentFontClour,//选中字体色
	PageNavigBarEx_Attr_LeftReservation, //左预留
	PageNavigBarEx_Attr_RightReservation,//右预留
	PageNavigBarEx_Attr_ElemeData,//扩展元素
	PageNavigBarEx_Attr_LayoutData,//布局器
	PageNavigBarEx_Attr_End,
	/*-------------------------------PageNavigBarEx私有属性常量结束-------------------------------*/
		/*-------------------------------PageNavigBarEx私有事件常量-------------------------------*/
	Event_PageNavigBarEx_EventNotification = 659,


	/*-------------------------------PageNavigBarEx私有事件常量结束-------------------------------*/

	/*-------------------------------PageNavigBarEx其他常量-------------------------------*/

	/*-------------------------------PageNavigBarEx其他常量结束-------------------------------*/
};


/*------------------------------------------EXUI常量定义结束------------------------------------------*/



/*------------------------------------------EXUI事件定义开始------------------------------------------*/
//组件句柄 事件类型 参数1 参数2 参数3 参数4
typedef  AutoInt(CALLBACK* ExuiCallback)(Control_P, AutoInt msg, AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4);
//1 缓动任务 2事件类型 3任务id 4 位置 5进度 6附加参数
typedef int (CALLBACK* SlowMotionCallBack)(SlowMotionTaskEx_P Task, AutoInt Msg, AutoInt index, AutoInt result, AutoInt progress, AutoInt parameter);
/* 时钟id 掉用次数 理论次数 重入次数 其它信息 附加参数 */
typedef void (CALLBACK* TimeExCallBack)(SlowMotionTaskEx_P Task, AutoInt num, int ShouldNum, AutoInt ReentryNum, AutoInt TimerOrWaitFired, AutoInt Parameter);
/*------------------------------------------EXUI事件定义结束------------------------------------------*/



/*------------------------------------------EXUIAPI定义开始------------------------------------------*/

#define EX_UI_APIINFO(CMDINFO) \
/*--------调用exui内部功能--------*/\
CMDINFO(AutoInt,CallInternalFunction_Ex,( AutoInt msg, AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4));\
\
/*--------装载位图--------*/\
CMDINFO(ImageEx_P,LoadImageEx,( BinEx_P data, int Mode, int Width, int Height, int Angle, int LineClour, int LineWidth, int Alignment));\
\
/*--------销毁位图--------*/\
CMDINFO(void,DestroyImageEx,( ImageEx_P image));\
\
/*--------取位图属性--------*/\
CMDINFO(AutoInt,GetImageAttributeEx,( ImageEx_P image, AutoInt index));\
\
/*--------取位图数据--------*/\
CMDINFO(BinEx_P,GetImageData,( ImageEx_P image));\
\
/*--------创建内存Ex--------*/\
CMDINFO(BinEx_P,CreateBinEx,( const void * data, int datalength));\
\
/*--------销毁内存Ex--------*/\
CMDINFO(void,DeleteBinEx,( BinEx_P binex));\
\
/*--------取内存Ex数据地址--------*/\
CMDINFO(void *,GetBinExDataPointer,( BinEx_P binex));\
\
/*--------取内存Ex数据长度--------*/\
CMDINFO(int,GetBinExDatalength,( BinEx_P binex));\
\
/*--------执行BinEx命令--------*/\
CMDINFO(AutoInt,ExecuteBinExCmd,(BinEx_P binex, AutoInt Cmd, AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4));\
\
/*--------读事件参数数据Ex--------*/\
CMDINFO(BinEx_P,ReadEventParamDataEx,(AutoInt DataPtr, AutoInt DataType));\
\
/*--------写事件参数数据Ex--------*/\
CMDINFO(void,WriteEventParamDataEx,(AutoInt DataPtr, BinEx_P Data, AutoInt DataType));\
\
/*--------打包事件返回数据Ex--------*/\
CMDINFO(AutoInt,PackEventReturnDataEx,(BinEx_P Data, AutoInt DataType));\
\
/*--------解析事件返回数据--------*/\
CMDINFO(BinEx_P,UnpackEventReturnDataEx,(AutoInt Data, AutoInt DataType));\
\
/*--------读事件参数文本Ex--------*/\
CMDINFO(StrEx_P,ReadEventParamTextEx,(AutoInt TextPtr, AutoInt DataType));\
\
/*--------写事件参数文本Ex--------*/\
CMDINFO(void,WriteEventParamTextEx,(AutoInt TextPtr, StrEx_P Text, AutoInt DataType));\
\
/*--------打包事件返回文本Ex--------*/\
CMDINFO(AutoInt,PackEventReturnTextEx,(StrEx_P Text, AutoInt DataType));\
\
/*--------解析事件返回文本--------*/\
CMDINFO(StrEx_P,UnpackEventReturnTextEx,(AutoInt TextPtr, AutoInt DataType));\
\
/*--------组件创建--------*/\
CMDINFO(BOOL,ControlCreate,( Control_P control, Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, void * Callback, void * PrivateEvent, int controlType, int penetrate, BinEx_P Cursor, int focusweight, AutoInt sign, BinEx_P Layout));\
\
/*--------组件销毁--------*/\
CMDINFO(void,ControlDestroy,( Control_P control));\
\
/*--------组件是否已创建--------*/\
CMDINFO(BOOL,ControlIsCreate,( Control_P control));\
\
/*--------组件发送消息--------*/\
CMDINFO(AutoInt,ControlSendMessage,( Control_P control, AutoInt msg, AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4));\
\
/*--------组件发送子级消息--------*/\
CMDINFO(void,ControlSendChildMessage,( Control_P control, AutoInt msg, AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4));\
\
/*--------组件更新缓存--------*/\
CMDINFO(BOOL,ControlUpdateCache,(int Mode));\
\
/*--------组件取距窗口顶边--------*/\
CMDINFO(int,ControlGetWindowTop,( Control_P control));\
\
/*--------组件取距窗口左边--------*/\
CMDINFO(int,ControlGetWindowLeft,( Control_P control));\
\
/*--------组件置左边--------*/\
CMDINFO(void,ControlSetLeft,( Control_P control, int Left));\
\
/*--------组件取左边--------*/\
CMDINFO(int,ControlGetLeft,( Control_P control));\
\
/*--------组件置顶边--------*/\
CMDINFO(void,ControlSetTop,( Control_P control, int Top));\
\
/*--------组件取顶边--------*/\
CMDINFO(int,ControlGetTop,( Control_P control));\
\
/*--------组件置宽度--------*/\
CMDINFO(void,ControlSetWidth,( Control_P control, int Width));\
\
/*--------组件取宽度--------*/\
CMDINFO(int,ControlGetWidth,( Control_P control));\
\
/*--------组件置高度--------*/\
CMDINFO(void,ControlSetHeight,( Control_P control, int Height));\
\
/*--------组件取高度--------*/\
CMDINFO(int,ControlGetHeight,( Control_P control));\
\
/*--------组件取矩形--------*/\
CMDINFO(void,ControlGetRect,( Control_P control, int * Left, int * Top, int * Width, int * Height));\
\
/*--------组件置矩形--------*/\
CMDINFO(void,ControlSetRect,( Control_P control, int Left, int Top, int Width, int Height));\
\
/*--------组件取真实可视--------*/\
CMDINFO(BOOL,ControlGetTrueVisual,( Control_P control));\
\
/*--------组件取可视--------*/\
CMDINFO(BOOL,ControlGetVisual,( Control_P control));\
\
/*--------组件置可视--------*/\
CMDINFO(void,ControlSetVisual,( Control_P control, BOOL Visual));\
\
/*--------组件取真实禁止--------*/\
CMDINFO(BOOL,ControlGetTrueDisabled,( Control_P control));\
\
/*--------组件取禁止--------*/\
CMDINFO(BOOL,ControlGetDisabled,( Control_P control));\
\
/*--------组件置禁止--------*/\
CMDINFO(void,ControlSetDisabled,( Control_P control, BOOL UpdateDisabled));\
\
/*--------组件置光标--------*/\
CMDINFO(void,ControlSetCursor,( Control_P control, BinEx_P Cursor));\
\
/*--------组件取光标--------*/\
CMDINFO(BinEx_P,ControlGetCursor,( Control_P control));\
\
/*--------组件置光标id--------*/\
CMDINFO(void,ControlSetCursorId,( Control_P control, int CursorId));\
\
/*--------组件取光标id--------*/\
CMDINFO(int,ControlGetCursorId,( Control_P control));\
\
/*--------组件取鼠标穿透--------*/\
CMDINFO(int,ControlGetPenetrate,( Control_P control));\
\
/*--------组件置鼠标穿透--------*/\
CMDINFO(void,ControlSetPenetrate,( Control_P control, int Penetrate));\
\
/*--------组件取透明度--------*/\
CMDINFO(int,ControlGetTransparency,( Control_P control));\
\
/*--------组件置透明度--------*/\
CMDINFO(void,ControlSetTransparency,( Control_P control, int Transparency));\
\
/*--------组件置焦点权重--------*/\
CMDINFO(void,ControlSetFocusWeight,( Control_P control, int FocusWeight));\
\
/*--------组件取焦点权重--------*/\
CMDINFO(int,ControlGetFocusWeight,( Control_P control));\
\
/*--------组件置焦点组件--------*/\
CMDINFO(void,ControlSetFocusControl,( Control_P control));\
\
/*--------组件取焦点组件--------*/\
CMDINFO(Control_P,ControlGetFocusControl,( ));\
\
/*--------组件取上一焦点组件--------*/\
CMDINFO(Control_P,ControlGetMaxFocusWeightControl,( Control_P control));\
\
/*--------组件取下一焦点组件--------*/\
CMDINFO(Control_P,ControlGetNextFocusControl,( Control_P control));\
\
/*--------组件取鼠标捕获组件--------*/\
CMDINFO(Control_P,ControlGetCaptureControl,( ));\
\
/*--------组件置鼠标捕获组件--------*/\
CMDINFO(void,ControlSetCaptureControl,( Control_P control));\
\
/*--------组件取热点组件--------*/\
CMDINFO(Control_P,ControlGetHotControl,( ));\
\
/*--------组件取左键按下组件--------*/\
CMDINFO(Control_P,ControlGetLeftPressControl,( ));\
\
/*--------组件取右键按下组件--------*/\
CMDINFO(Control_P,ControlGetRightPressControl,( ));\
\
/*--------组件置类型--------*/\
CMDINFO(void,ControlSetcontrolType,( Control_P control, int controlType));\
\
/*--------组件取类型--------*/\
CMDINFO(int,ControlGetcontrolType,( Control_P control));\
\
/*--------组件置标识--------*/\
CMDINFO(void,ControlSetsign,( Control_P control, AutoInt sign));\
\
/*--------组件取标识--------*/\
CMDINFO(AutoInt,ControlGetsign,( Control_P control));\
\
/*--------组件置回调--------*/\
CMDINFO(ExuiCallback,ControlSetCallback,( Control_P control, int mode, ExuiCallback Callback));\
\
/*--------组件取回调--------*/\
CMDINFO(ExuiCallback,ControlGetCallback,( Control_P control, int Mode));\
\
/*--------组件调用回调--------*/\
CMDINFO(AutoInt,ControInvokeCallback,( Control_P control, AutoInt msg, AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4, ExuiCallback ExuiCallback));\
\
/*--------组件取所在窗口句柄--------*/\
CMDINFO(HWND,ControlGetWindow,( Control_P control));\
\
/*--------组件取组件绑定窗口--------*/\
CMDINFO(HWND,ControlGetBindWin,( Control_P control));\
\
/*--------组件取窗口绑定组件--------*/\
CMDINFO(Control_P,ControlGetBindControl,( HWND WindowHandle));\
\
/*--------组件取设备上下文--------*/\
CMDINFO(HDC,ControlGetDc,( Control_P control));\
\
/*--------组件取图形--------*/\
CMDINFO(AutoInt,ControlGetGraphics,( Control_P control));\
\
/*--------组件取位图--------*/\
CMDINFO(AutoInt,ControlGetHBITMAP,( Control_P control));\
\
/*--------组件添加重绘区--------*/\
CMDINFO(void,ControlAddRedrawRect,( Control_P control, int Left, int Top, int Width, int Height));\
\
/*--------组件请求重绘--------*/\
CMDINFO(void,ControlRedraw,( Control_P control));\
\
/*--------组件暂停重画--------*/\
CMDINFO(int,ControlLockUpdate,( Control_P control));\
\
/*--------组件容许重画--------*/\
CMDINFO(int,ControlUnlockUpdate,( Control_P control));\
\
/*--------组件取重画锁定计数--------*/\
CMDINFO(int,ControlGetLockUpdateCount,( Control_P control));\
\
/*--------组件取父组件--------*/\
CMDINFO(Control_P,ControlGetParentControl,( Control_P control));\
\
/*--------组件置父组件--------*/\
CMDINFO(BOOL,ControlSetParentControl,( Control_P control, Control_P Parentcontrol, BOOL mode, int NewLeft, int NewTop));\
\
/*--------组件可有子级--------*/\
CMDINFO(BOOL,ControlHaveChild,( Control_P control));\
\
/*--------组件取子级组件数--------*/\
CMDINFO(int,ControlGetChildCount,( Control_P control, BOOL Mode));\
\
/*--------组件枚举子级组件--------*/\
CMDINFO(BinEx_P,ControlEnumerateChild,( Control_P control, BOOL Mode));\
\
/*--------组件取上一组件--------*/\
CMDINFO(Control_P,ControlGetLastcontrol,( Control_P control));\
\
/*--------组件取下一组件--------*/\
CMDINFO(Control_P,ControlGetNextcontrol,( Control_P control));\
\
/*--------组件取嵌套层次--------*/\
CMDINFO(int,ControlGetNestingLevel,( Control_P control));\
\
/*--------组件取层次组件--------*/\
CMDINFO(Control_P,ControlGetlevelcontrol,( AutoInt level));\
\
/*--------组件置层次--------*/\
CMDINFO(void,ControlSetlevel,( Control_P control,int mode, Control_P RelativeControl));\
\
/*--------组件取层次--------*/\
CMDINFO(int,ControlGetlevel,( Control_P control));\
\
/*--------组件置所在窗口分层透明--------*/\
CMDINFO(void,ControlSetWinLayered,( Control_P control, int Layered));\
\
/*--------组件取所在窗口分层透明--------*/\
CMDINFO(AutoInt,ControlGetWinLayered,( Control_P control));\
\
/*--------组件取所在窗口图形--------*/\
CMDINFO(AutoInt,ControlGetWinGraphics,( Control_P control));\
\
/*--------组件取所在窗口位图--------*/\
CMDINFO(HBITMAP,ControlGetWinHBITMAP,( Control_P control));\
\
/*--------组件取所在窗口设备上下文--------*/\
CMDINFO(HDC,ControlGetWinDc,( Control_P control));\
\
/*--------组件置所在窗口刷新回调--------*/\
CMDINFO(AutoInt,ControlSetWinRefreshCallBack,( Control_P control, AutoInt CallBack));\
\
/*--------组件取所在窗口刷新回调--------*/\
CMDINFO(AutoInt,ControlGetWinRefreshCallBack,( Control_P control));\
\
/*--------组件刷新所在窗口--------*/\
CMDINFO(void,ControlRefreshWin,( Control_P control));\
\
/*--------组件暂停重画所在窗口--------*/\
CMDINFO(int,ControlLockWinUpdate,( Control_P control));\
\
/*--------组件容许重画所在窗口--------*/\
CMDINFO(int,ControlUnlockWinUpdate,( Control_P control));\
\
/*--------组件取所在窗口重画锁定状态--------*/\
CMDINFO(int,ControlGetWinLockUpdateCount,( Control_P control));\
\
/*--------组件取所在页面--------*/\
CMDINFO(int,ControlGetPage,( Control_P control));\
\
/*--------组件置所在页面--------*/\
CMDINFO(void,ControlSetPage,( Control_P control, int Page));\
\
/*--------组件取当前页面--------*/\
CMDINFO(int,ControlGetCurrentPage,( Control_P control));\
\
/*--------组件置当前页面--------*/\
CMDINFO(void,ControlSetCurrentPage,( Control_P control, int Page));\
\
/*--------组件取布局器--------*/\
CMDINFO(BinEx_P,ControlGetLayoutConfig,( Control_P control));\
\
/*--------组件置布局器--------*/\
CMDINFO(void,ControlSetLayoutConfig,( Control_P control, BinEx_P LayoutConfig));\
\
/*--------组件提交任务--------*/\
CMDINFO(AutoInt,ControlSubmitTask,( Control_P control, AutoInt type, AutoInt cmd, AutoInt sign, AutoInt Parame));\
\
/*--------组件执行元素命令--------*/\
CMDINFO(AutoInt,ControlRunElemCmd,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt Cmd, AutoInt parameter1, AutoInt parameter2, AutoInt parameter3, AutoInt parameter4, AutoInt parameter5, AutoInt parameter6, AutoInt parameter7, AutoInt parameter8, AutoInt parameter9, AutoInt parameter10));\
\
/*--------置底层元素数量--------*/\
CMDINFO(void,ControlSetUnderlyElemCount,( Control_P control, AutoInt Type, AutoInt cloid, AutoInt Num));\
\
/*--------取底层元素数量--------*/\
CMDINFO(AutoInt,ControlGetUnderlyElemCount,( Control_P control, AutoInt Type, AutoInt cloid));\
\
/*--------组件插入元素--------*/\
CMDINFO(AutoInt,ControlInsertElem,( Control_P control, AutoInt Type, AutoInt cloid, AutoInt ctrlindex, AutoInt addnum));\
\
/*--------组件删除元素--------*/\
CMDINFO(void,ControlDeleteElem,( Control_P control, AutoInt Type, AutoInt cloid, AutoInt delindex, AutoInt deletenum));\
\
/*--------组件取元素数量--------*/\
CMDINFO(AutoInt,ControlGetElemCount,( Control_P control, AutoInt Type, AutoInt cloid));\
\
/*--------组件重置元素--------*/\
CMDINFO(void,ControlResetElem,( Control_P control, AutoInt Type, AutoInt cloid, AutoInt delindex, AutoInt deletenum));\
\
/*--------组件置元素属性_图片--------*/\
CMDINFO(void,ControlSetElemAttribute_Imsge,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, void * callback, int Layout, int left, int top, int width, int Height, int Ctrstyle));\
\
/*--------组件取元素属性_图片--------*/\
CMDINFO(AutoInt,ControlGetElemAttribute_Imsge,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId));\
\
/*--------组件置元素内容_图片--------*/\
CMDINFO(void,ControlSetElemData_Imsge,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, ImageEx_P IcoData, int Align, int PlayAnimation));\
\
/*--------组件取元素内容_图片--------*/\
CMDINFO(AutoInt,ControlGetElemData_Imsge,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId));\
\
/*--------组件置元素属性_文本--------*/\
CMDINFO(void,ControlSetElemAttribute_Text,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, void * callback, int Layout, int left, int top, int width, int Height, int Ctrstyle, BinEx_P Font, int Align));\
\
/*--------组件取元素属性_文本--------*/\
CMDINFO(AutoInt,ControlGetElemAttribute_Text,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId));\
\
/*--------组件置元素内容_文本--------*/\
CMDINFO(void,ControlSetElemData_Text,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, StrEx_P Title, int FontColor));\
\
/*--------组件取元素内容_文本--------*/\
CMDINFO(AutoInt,ControlGetElemData_Text,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId));\
\
/*--------组件置元素属性_按钮--------*/\
CMDINFO(void,ControlSetElemAttribute_Button,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, void * callback, int Layout, int left, int top, int width, int Height, int Ctrstyle, BinEx_P Skin, BinEx_P Font));\
\
/*--------组件取元素属性_按钮--------*/\
CMDINFO(AutoInt,ControlGetElemAttribute_Button,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId));\
\
/*--------组件置元素内容_按钮--------*/\
CMDINFO(void,ControlSetElemData_Button,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, StrEx_P Title, int FontColor, BOOL Select));\
\
/*--------组件取元素内容_按钮--------*/\
CMDINFO(AutoInt,ControlGetElemData_Button,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId));\
\
/*--------组件置元素属性_选择框--------*/\
CMDINFO(void,ControlSetElemAttribute_Select,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, void * callback, int Layout, int left, int top, int width, int Height, int Ctrstyle, BinEx_P Skin, BinEx_P Font, int IconWidth, int IconHeight,int SelectMode));\
\
/*--------组件取元素属性_选择框--------*/\
CMDINFO(AutoInt,ControlGetElemAttribute_Select,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId));\
\
/*--------组件置元素内容_选择框--------*/\
CMDINFO(void,ControlSetElemData_Select,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, StrEx_P Title, int FontColor, int Select));\
\
/*--------组件取元素内容_选择框--------*/\
CMDINFO(AutoInt,ControlGetElemData_Select,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId));\
\
/*--------组件置元素属性_编辑文本--------*/\
CMDINFO(void,ControlSetElemAttribute_EditText,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, void * callback, int Layout, int left, int top, int width, int Height, int Ctrstyle, int ShowMode, BinEx_P Skin, BinEx_P Font, int Align, StrEx_P PasswordSubstitution, BinEx_P Cursor, BOOL Multiline, int InputMode, int MaxInput, BinEx_P EditFont, int EditFontColor, int CursorColor, int SelectedFontColor, int SelectedColor, int MenuTableWidth, int MenuTableHeight, BinEx_P MenuFont, int MenuFontClour, int MenuDisabledFontClour, int MenuLanguage));\
\
/*--------组件取元素属性_编辑文本--------*/\
CMDINFO(AutoInt,ControlGetElemAttribute_EditText,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId));\
\
/*--------组件置元素内容_编辑文本--------*/\
CMDINFO(void,ControlSetElemData_EditText,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, StrEx_P Content, int FontClour));\
\
/*--------组件取元素内容_编辑文本--------*/\
CMDINFO(AutoInt,ControlGetElemData_EditText,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId));\
\
/*--------组件置元素属性_组合按钮--------*/\
CMDINFO(void,ControlSetElemAttribute_ComboButton,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, void * callback, int Layout, int left, int top, int width, int Height, int Ctrstyle, BinEx_P Skin, BinEx_P Font, int IconWidth, int IconHeight, int Align));\
\
/*--------组件取元素属性_组合按钮--------*/\
CMDINFO(AutoInt,ControlGetElemAttribute_ComboButton,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId));\
\
/*--------组件置元素内容_组合按钮--------*/\
CMDINFO(void,ControlSetElemData_ComboButton,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, int BackColor, ImageEx_P image, StrEx_P Title, int FontColor, int PartnerStay));\
\
/*--------组件取元素内容_组合按钮--------*/\
CMDINFO(AutoInt,ControlGetElemData_ComboButton,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId));\
\
/*--------组件置元素属性_进度条--------*/\
CMDINFO(void,ControlSetElemAttribute_Progressbar,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, void * callback, int Layout, int left, int top, int width, int Height, int Ctrstyle, BinEx_P Skin, int Style));\
\
/*--------组件取元素属性_进度条--------*/\
CMDINFO(AutoInt,ControlGetElemAttribute_Progressbar,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId));\
\
/*--------组件置元素内容_进度条--------*/\
CMDINFO(void,ControlSetElemData_Progressbar,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, int Position));\
\
/*--------组件取元素内容_进度条--------*/\
CMDINFO(AutoInt,ControlGetElemData_Progressbar,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId));\
\
/*--------组件置元素属性_滑块条--------*/\
CMDINFO(void,ControlSetElemAttribute_Sliderbar,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, void * callback, int Layout, int left, int top, int width, int Height, int Ctrstyle, BinEx_P Skin, int Style));\
\
/*--------组件取元素属性_滑块条--------*/\
CMDINFO(AutoInt,ControlGetElemAttribute_Sliderbar,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId));\
\
/*--------组件置元素内容_滑块条--------*/\
CMDINFO(void,ControlSetElemData_Sliderbar,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, int Position));\
\
/*--------组件取元素内容_滑块条--------*/\
CMDINFO(AutoInt,ControlGetElemData_Sliderbar,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId));\
\
/*--------组件置元素属性_颜色--------*/\
CMDINFO(void,ControlSetElemAttribute_Colour,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, void * callback, int Layout, int left, int top, int width, int Height, int Ctrstyle));\
\
/*--------组件取元素属性_颜色--------*/\
CMDINFO(AutoInt,ControlGetElemAttribute_Colour,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId));\
\
/*--------组件置元素内容_颜色--------*/\
CMDINFO(void,ControlSetElemData_Colour,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, int style, int Colour));\
\
/*--------组件取元素内容_颜色--------*/\
CMDINFO(AutoInt,ControlGetElemData_Colour,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId));\
\
/*--------组件置元素属性_直线--------*/\
CMDINFO(void,ControlSetElemAttribute_Line,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, void * callback, int Layout, int left, int top, int width, int Height, int Ctrstyle, int LineWidth));\
\
/*--------组件取元素属性_直线--------*/\
CMDINFO(AutoInt,ControlGetElemAttribute_Line,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId));\
\
/*--------组件置元素内容_直线--------*/\
CMDINFO(void,ControlSetElemData_Line,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, int style, int Colour));\
\
/*--------组件取元素内容_直线--------*/\
CMDINFO(AutoInt,ControlGetElemData_Line,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId));\
\
/*--------组件置元素属性_自定义--------*/\
CMDINFO(void,ControlSetElemAttribute_Custom,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, void * callback, int Layout, int left, int top, int width, int Height, int Ctrstyle,  BinEx_P CustomTag, BinEx_P CustomElemAttr9, BinEx_P CustomElemAttr10, BinEx_P CustomElemAttr11, BinEx_P CustomElemAttr12, BinEx_P CustomElemAttr13, BinEx_P CustomElemAttr14, BinEx_P CustomElemAttr15, BinEx_P CustomElemAttr16, BinEx_P CustomElemAttr17, BinEx_P CustomElemAttr18, BinEx_P CustomElemAttr19, BinEx_P CustomElemAttr20, BinEx_P CustomElemAttr21, BinEx_P CustomElemAttr22, BinEx_P CustomElemAttr23, BinEx_P CustomElemAttr24, BinEx_P CustomElemAttr25, BinEx_P CustomElemAttr26, BinEx_P CustomElemAttr27, BinEx_P CustomElemAttr28, BinEx_P CustomElemAttr29, BinEx_P CustomElemAttr30, BinEx_P CustomElemAttr31, BinEx_P CustomElemAttr32));\
\
/*--------组件取元素属性_自定义--------*/\
CMDINFO(AutoInt,ControlGetElemAttribute_Custom,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId));\
\
/*--------组件置元素内容_自定义--------*/\
CMDINFO(void,ControlSetElemData_Custom,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId,  BOOL Show, BOOL Ban, BinEx_P CustomElemData3, BinEx_P CustomElemData4, BinEx_P CustomElemData5, BinEx_P CustomElemData6, BinEx_P CustomElemData7, BinEx_P CustomElemData8, BinEx_P CustomElemData9, BinEx_P CustomElemData10, BinEx_P CustomElemData11, BinEx_P CustomElemData12, BinEx_P CustomElemData13, BinEx_P CustomElemData14, BinEx_P CustomElemData15, BinEx_P CustomElemData16, BinEx_P CustomElemData17, BinEx_P CustomElemData18, BinEx_P CustomElemData19, BinEx_P CustomElemData20, BinEx_P CustomElemData21, BinEx_P CustomElemData22, BinEx_P CustomElemData23, BinEx_P CustomElemData24, BinEx_P CustomElemData25, BinEx_P CustomElemData26, BinEx_P CustomElemData27, BinEx_P CustomElemData28, BinEx_P CustomElemData29, BinEx_P CustomElemData30, BinEx_P CustomElemData31, BinEx_P CustomElemData32));\
\
/*--------组件取元素内容_自定义--------*/\
CMDINFO(AutoInt,ControlGetElemData_Custom,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId));\
\
/*--------组件置元素属性_组件--------*/\
CMDINFO(void,ControlSetElemAttribute_Control,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, void * callback, int Layout, int left, int top, int width, int Height, int Ctrstyle));\
\
/*--------组件取元素属性_组件--------*/\
CMDINFO(AutoInt,ControlGetElemAttribute_Control,( Control_P control, AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId));\
\
/*--------组件置元素内容_组件--------*/\
CMDINFO(void,ControlSetElemData_Control,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, BinEx_P UserData, int Mode, Control_P Handle, BinEx_P  Data));\
\
/*--------组件取元素内容_组件--------*/\
CMDINFO(AutoInt,ControlGetElemData_Control,( Control_P control, AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId));\
\
/*--------信息框Ex--------*/\
CMDINFO(int,MsgBox_Ex,( BinEx_P Ico, StrEx_P Title, BinEx_P TipIco, StrEx_P TipTitle, StrEx_P Button, int DefaultBtn, HWND hWndParent, int Style, BinEx_P Skin, ExuiCallback callback, AutoInt Parameter));\
\
/*--------输入框Ex--------*/\
CMDINFO(StrEx_P,InputBox_Ex,( BinEx_P Ico, StrEx_P Title, BinEx_P TipIco, StrEx_P TipTitle, StrEx_P Button, int DefaultBtn, StrEx_P Conten, int InputMode, StrEx_P PasswordSubstitution, int MaxInput, BOOL Multiline, AutoInt * SelectResult, HWND hWndParent, int Style, BinEx_P Skin, ExuiCallback callback, AutoInt Parameter_));\
\
/*--------弹出提示框Ex--------*/\
CMDINFO(int,PopUpTipBoxEx,( BinEx_P Ico, StrEx_P TipTitle, int PopUpMode, int x, int y, int width, int height, int AutoCloseTime, int Angle, int BackColor, int BorderColor, int LineWidth, BinEx_P Font, int FontColor, int Align, int Transparency, int ReferType, Control_P ReferControl));\
\
/*--------关闭提示框Ex--------*/\
CMDINFO(void,CloseTipBoxEx,( ));\
\
/*--------弹出提示框Ex--------*/\
CMDINFO(AutoInt,PopUpNotificationBoxEx,( BinEx_P Ico, StrEx_P Title, BinEx_P TipIco, StrEx_P TipTitle, StrEx_P Button, int Style, int PopUpMode, Control_P Parent, int AutoCloseTime, int Reserve,  BinEx_P Skin, ExuiCallback callback, AutoInt Parameter));\
\
/*--------关闭提示框Ex--------*/\
CMDINFO(void,CloseNotificationBoxEx,(AutoInt NotificationBoxHandle));\
\
/*--------精简输入框Ex--------*/\
CMDINFO(StrEx_P,PopUpLiteInputEx,( int X, int Y, int nWidth, int nHeight, ExuiCallback callback, StrEx_P Content, int InputMode, int MaxInput, StrEx_P PasswordSubstitution, int Align, BOOL Multiline, BOOL Wrapped, BinEx_P Skin, BinEx_P Cursor, BinEx_P Font, int FontClour, int SelectedFontColor, int SelectedColor, int CursorColor, int ScrollBarMode, int MenuTableWidth, int MenuTableHeight, BinEx_P MenuFont, int MenuFontClour, int MenuDisabledFontClour, int MenuTransparency, int MenuLanguage, int ReferType, Control_P ReferControl));\
\
/*--------时间选择框--------*/\
CMDINFO(StrEx_P,TimePickBox_Ex,( BinEx_P Ico, StrEx_P TipTitle, StrEx_P Button, int DefaultBtn, int Width, int Height, StrEx_P SelectionDate, StrEx_P MiniDate, StrEx_P MaxDate, BOOL OtherMonthClour, int TimeMode, int Language, AutoInt * SelectResult, HWND hWndParent, int Style, BinEx_P Skin, ExuiCallback Callback, AutoInt Parameter_));\
\
/*--------颜色选择框--------*/\
CMDINFO(int,ColorPickExBox_Ex,( BinEx_P Ico, StrEx_P TipTitle, StrEx_P Button, int DefaultBtn, int Width, int Height, int NowClour, BinEx_P QuickClours, int ClourMode, int ColorPickStyle, AutoInt * SelectResult, HWND hWndParent, int Style, BinEx_P Skin, ExuiCallback Callback, AutoInt Parameter_));\
\
/*--------弹出时间选择器--------*/\
CMDINFO(StrEx_P,PopUpTimePick_Ex,( int LEFT, int Top, int Width, int Height, StrEx_P Button, int DefaultBtn, StrEx_P SelectionDate, StrEx_P MiniDate, StrEx_P MaxDate, BOOL OnlyThisMonth, int TimeMode, int Language, AutoInt * SelectResult, HWND hWndParent, int Style, BinEx_P Skin, ExuiCallback Callback, AutoInt Parameter_, int ReferType, Control_P ReferControl));\
\
/*--------弹出颜色选择器--------*/\
CMDINFO(int,PopUpColorPick_Ex,( int LEFT, int Top, int Width, int Height, StrEx_P Button, int DefaultBtn, int NowClour, BinEx_P QuickClours, int ClourMode, int ColorPickStyle, AutoInt * SelectResult, HWND hWndParent, int Style, BinEx_P Skin, ExuiCallback Callback, AutoInt Parameter_, int ReferType, Control_P ReferControl));\
\
/*--------文件选择框--------*/\
CMDINFO(int,FileSelectionbox_Ex,( ));\
\
/*--------弹出项目标题编辑器Ex--------*/\
CMDINFO(StrEx_P,PopUpItemInputEx,( Control_P control, AutoInt index, AutoInt cloid, AutoInt Reserve, int mode, StrEx_P Content, int InputMode, int MaxInput, StrEx_P PasswordSubstitution, int Align, BOOL Multiline, BOOL Wrapped, BinEx_P Skin, BinEx_P Cursor, BinEx_P Font, int FontClour, int SelectedFontColor, int SelectedColor, int CursorColor, int ScrollBarMode, int MenuTableWidth, int MenuTableHeight, BinEx_P MenuFont, int MenuFontClour, int MenuDisabledFontClour, int MenuTransparency, int MenuLanguage));\
\
/*--------创建_菜单Ex--------*/\
CMDINFO(MenuEx_P,Create_MenuEx,( int IconWidth, int IconHeight, BinEx_P Skin, BinEx_P Font, BinEx_P Cursor, int Transparency, void * Callback, AutoInt MenuTag));\
\
/*--------置属性_菜单Ex--------*/\
CMDINFO(void,SetAttribute_MenuEx,( MenuEx_P control, AutoInt AttributeId, int IconWidth, int IconHeight, BinEx_P Skin, BinEx_P Font, BinEx_P Cursor, int Transparency, void * callback, AutoInt MenuTag));\
\
/*--------取属性_菜单Ex--------*/\
CMDINFO(AutoInt,GetAttribute_MenuEx,( MenuEx_P control, AutoInt index));\
\
/*--------销毁_菜单Ex--------*/\
CMDINFO(void,Destroy_MenuEx,( MenuEx_P control));\
\
/*--------是否已弹出_菜单Ex--------*/\
CMDINFO(BOOL,IsPopUp_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId));\
\
/*--------弹出_菜单Ex--------*/\
CMDINFO(void,PopUp_MenuEx,( MenuEx_P menuEx, MenuInfoEx_P MenuId, int left, int top, HWND hWndParent, BOOL Modal, Control_P ExceptionControl, int ReferType, Control_P ReferControl));\
\
/*--------关闭_菜单Ex--------*/\
CMDINFO(void,Close_MenuEx,( MenuEx_P contro, MenuInfoEx_P MenuIdl, BOOL Mode));\
\
/*--------插入_菜单Ex--------*/\
CMDINFO(AutoInt,InsertItem_MenuEx,( MenuEx_P menuEx, MenuInfoEx_P ParentMenu, AutoInt index, AutoInt Data, ImageEx_P Ico, StrEx_P Title, int FontColor, int DisabledFontColor, int Width, int Height, int Type, BOOL Disabled));\
\
/*--------删除_菜单Ex--------*/\
CMDINFO(void,DeleteItem_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId));\
\
/*--------取数量_菜单Ex--------*/\
CMDINFO(AutoInt,GetItemCount_MenuEx,( MenuEx_P control));\
\
/*--------置图标_菜单Ex--------*/\
CMDINFO(void,SetItemIco_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId, ImageEx_P Ico));\
\
/*--------取图标_菜单Ex--------*/\
CMDINFO(ImageEx_P,GetItemIco_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId));\
\
/*--------置标题_菜单Ex--------*/\
CMDINFO(void,SetItemTitle_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId, StrEx_P Title));\
\
/*--------取标题_菜单Ex--------*/\
CMDINFO(StrEx_P,GetItemTitle_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId));\
\
/*--------取附加值_菜单Ex--------*/\
CMDINFO(AutoInt,GetItemData_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId));\
\
/*--------置附加值_菜单Ex--------*/\
CMDINFO(void,SetItemData_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId, AutoInt Data));\
\
/*--------取字体色_菜单Ex--------*/\
CMDINFO(int,GetItemFontColor_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId));\
\
/*--------置字体色_菜单Ex--------*/\
CMDINFO(void,SetItemFontColor_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId, int FontColor));\
\
/*--------取禁止字体色_菜单Ex--------*/\
CMDINFO(int,GetItemDisabledFontColor_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId));\
\
/*--------置禁止字体色_菜单Ex--------*/\
CMDINFO(void,SetItemDisabledFontColor_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId, int DisabledFontColor));\
\
/*--------置项目信息_菜单Ex--------*/\
CMDINFO(void,SetItemInfo_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId, int Type, AutoInt Data));\
\
/*--------取项目信息_菜单Ex--------*/\
CMDINFO(AutoInt,GetItemInfo_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId, int Type));\
\
/*--------置类型_菜单Ex--------*/\
CMDINFO(void,SetItemType_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId, int Type));\
\
/*--------取类型_菜单Ex--------*/\
CMDINFO(int,GetItemType_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId));\
\
/*--------取禁止_菜单Ex--------*/\
CMDINFO(BOOL,GetItemDisabled_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId));\
\
/*--------置禁止_菜单Ex--------*/\
CMDINFO(void,SetItemDisabled_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId, BOOL Disabled));\
\
/*--------置项目宽度_菜单Ex--------*/\
CMDINFO(void,SetItemWidth_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId, int Width));\
\
/*--------取项目宽度_菜单Ex--------*/\
CMDINFO(int,GetItemWidth_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId));\
\
/*--------置项目高度_菜单Ex--------*/\
CMDINFO(void,SetItemHeight_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId, int Height));\
\
/*--------取项目高度_菜单Ex--------*/\
CMDINFO(int,GetItemHeight_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId));\
\
/*--------取子菜单数量_菜单Ex--------*/\
CMDINFO(int,GetSubItemCount_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId));\
\
/*--------取子菜单_菜单Ex--------*/\
CMDINFO(MenuInfoEx_P,GetSubItem_MenuEx,( MenuEx_P control, MenuInfoEx_P MenuId, AutoInt index));\
\
/*--------创建_下拉列表Ex--------*/\
CMDINFO(DownlistEx_P,Create_DownlistEx,( int IconWidth, int IconHeight, BinEx_P Skin, BinEx_P Font, BinEx_P Cursor, BOOL AlternateColor, int ScrollBarMode, AutoInt CurrentItem, int Transparency, void * Callback, AutoInt ListTag));\
\
/*--------销毁_下拉列表Ex--------*/\
CMDINFO(void,Destroy_DownlistEx,( DownlistEx_P control));\
\
/*--------置属性_下拉列表Ex--------*/\
CMDINFO(void,SetAttribute_DownlistEx,( DownlistEx_P control, AutoInt AttributeId, int IconWidth, int IconHeight, BinEx_P Skin, BinEx_P Font, BinEx_P Cursor, BOOL AlternateColor, int ScrollBarMode, int CurrentItem, int Transparency, void * callback, int ListTag));\
\
/*--------取属性_下拉列表Ex--------*/\
CMDINFO(AutoInt,GetAttribute_DownlistEx,( DownlistEx_P control, AutoInt index));\
\
/*--------弹出_下拉列表Ex--------*/\
CMDINFO(void,PopUp_DownlistEx,( DownlistEx_P control, int X, int Y, int nWidth, int nHeight, HWND hWndParent, BOOL Modal, Control_P ExceptionControl, int ReferType, Control_P ReferControl));\
\
/*--------关闭_下拉列表Ex--------*/\
CMDINFO(void,Close_DownlistEx,( DownlistEx_P control));\
\
/*--------是否已弹出_下拉列表Ex--------*/\
CMDINFO(BOOL,IsPopUp_DownlistEx,( DownlistEx_P control));\
\
/*--------插入_下拉列表Ex--------*/\
CMDINFO(AutoInt,InsertItem_DownlistEx,( DownlistEx_P control, AutoInt index, AutoInt addnum, AutoInt Data, ImageEx_P Ico, StrEx_P Title, int FontColor, int Size));\
\
/*--------删除_下拉列表Ex--------*/\
CMDINFO(void,DeleteItem_DownlistEx,( DownlistEx_P control, AutoInt index, AutoInt deletenum));\
\
/*--------取数量_下拉列表Ex--------*/\
CMDINFO(AutoInt,GetItemCount_DownlistEx,( DownlistEx_P control));\
\
/*--------置附加值_下拉列表Ex--------*/\
CMDINFO(void,SetItemData_DownlistEx,( DownlistEx_P control, AutoInt index, AutoInt Data));\
\
/*--------取附加值_下拉列表Ex--------*/\
CMDINFO(AutoInt,GetItemData_DownlistEx,( DownlistEx_P control, AutoInt index));\
\
/*--------置图标_下拉列表Ex--------*/\
CMDINFO(void,SetItemIco_DownlistEx,( DownlistEx_P control, AutoInt index, ImageEx_P Ico));\
\
/*--------取图标_下拉列表Ex--------*/\
CMDINFO(ImageEx_P,GetItemIco_DownlistEx,( DownlistEx_P control, AutoInt index));\
\
/*--------置标题_下拉列表Ex--------*/\
CMDINFO(void,SetItemTitle_DownlistEx,( DownlistEx_P control, AutoInt index, StrEx_P Title));\
\
/*--------取标题_下拉列表Ex--------*/\
CMDINFO(StrEx_P,GetItemTitle_DownlistEx,( DownlistEx_P control, AutoInt index));\
\
/*--------置字体色_下拉列表Ex--------*/\
CMDINFO(void,SetItemFontColor_DownlistEx,( DownlistEx_P control, AutoInt index, int FontColor));\
\
/*--------取字体色_下拉列表Ex--------*/\
CMDINFO(int,GetItemFontColor_DownlistEx,( DownlistEx_P control, AutoInt index));\
\
/*--------置项目高度_下拉列表Ex--------*/\
CMDINFO(void,SetItemSize_DownlistEx,( DownlistEx_P control, AutoInt index, int Size));\
\
/*--------取项目高度_下拉列表Ex--------*/\
CMDINFO(int,GetItemSize_DownlistEx,( DownlistEx_P control, AutoInt index));\
\
/*--------保证显示_下拉列表Ex--------*/\
CMDINFO(void,GuaranteeVisible_DownlistEx,( DownlistEx_P control, AutoInt index, int mode));\
\
/*--------创建缓动任务--------*/\
CMDINFO(SlowMotionTaskEx_P,CreateSlowMotionTaskEx,( AutoInt Num, AutoInt Mode, AutoInt Star, AutoInt End, AutoInt FrameDelay, AutoInt Step, SlowMotionCallBack Callback, AutoInt parameter, BOOL Wait, BOOL ManualDestroy));\
\
/*--------置缓动任务配置--------*/\
CMDINFO(void,SetSlowMotionTaskConfigEx,( SlowMotionTaskEx_P Task_Queue, AutoInt index, AutoInt Mode, AutoInt Star, AutoInt End));\
\
/*--------执行缓动任务命令--------*/\
CMDINFO(AutoInt,RunSlowMotionTaskCmd,( SlowMotionTaskEx_P Task,AutoInt Cmd, AutoInt index, AutoInt param1, AutoInt param2, AutoInt param3, AutoInt param4));\
\
/*--------销毁缓动队列任务--------*/\
CMDINFO(void,DestroySlowMotionTaskEx,( SlowMotionTaskEx_P Task_Queue));\
\
/*--------播放窗口动画--------*/\
CMDINFO(BOOL,PlayWindowAnimationEx,( Control_P control, AutoInt scale, AutoInt AnimationType, AutoInt AlgorithmMode, AutoInt FrameDelay, AutoInt Step, BOOL FadeInAndOut, BOOL Wait, BOOL DestroyControl));\
\
/*--------载入组件--------*/\
CMDINFO(BOOL,LoadControlEx,( Control_P Parent, HWND Window, AutoInt mode, AutoInt Parameter1, AutoInt Parameter2, BinEx_P bin, AutoInt Callback, AutoInt CallbackParameter));\
\
/*--------保存组件--------*/\
CMDINFO(BinEx_P,packControlEx,( Control_P control, HWND Window, AutoInt mode, AutoInt Parameter1, AutoInt Parameter2, AutoInt Reserve, AutoInt Callback, AutoInt CallbackParameter));\
\
/*--------测量文本--------*/\
CMDINFO(BinEx_P,RexMeasureStringEx,( StrEx_P string, BinEx_P Fontex, AutoInt StringFormat, float x, float y, float width, float height, int mode));\
\
/*--------创建定时器--------*/\
CMDINFO(TimeEx_P,CreateTimerEx,(  AutoInt Period, TimeExCallBack TimeCallBack, AutoInt Parameter, BOOL Reentry,  AutoInt Flags));\
\
/*--------销毁定时器--------*/\
CMDINFO(void,DeleteTimerEx,( TimeEx_P time));\
\
/*--------置定时器配置--------*/\
CMDINFO(void,SetConfigTimerEx,( TimeEx_P time, AutoInt type, AutoInt value));\
\
/*--------取定时器配置--------*/\
CMDINFO(AutoInt,GetConfigTimerEx,( TimeEx_P time, AutoInt type));\
\
/*--------异常捕获--------*/\
CMDINFO(void,ExceptionCaptureEx,( void * UserExceptionCall, void * UserExceptionCall2));\
\
/*--------载入主题--------*/\
CMDINFO(BOOL,LoadThemeEx,( BinEx_P Theme, HWND hwnd, ExuiCallback Callback,int Param));\
\
/*--------创建窗口--------*/\
CMDINFO(HWND,Create_WindowBoxEx,( int mode, int Location, HWND hWndParent, int X, int Y, int nWidth, int nHeight, WNDPROC WndProc, AutoInt* InitAttr, BOOL Visual, BOOL Disabled, BinEx_P Icon, StrEx_P Title, BOOL AlwaysTop, BOOL Taskbar, BOOL EscClose, StrEx_P ClassName, int classstyle, int dwExStyle, int dwStyle, AutoInt Sign));\
\
/*--------是否已创建--------*/\
CMDINFO(BOOL,IsWindow_WindowBoxEx,( HWND hwnd));\
\
/*--------销毁窗口--------*/\
CMDINFO(void,Destroy_WindowBoxEx,( HWND hwnd));\
\
/*--------窗口置左边--------*/\
CMDINFO(void,SetLeft_WindowBoxEx,( HWND hwnd, int Left));\
\
/*--------窗口取左边--------*/\
CMDINFO(int,GetLeft_WindowBoxEx,( HWND hwnd));\
\
/*--------窗口置顶边--------*/\
CMDINFO(void,SetTop_WindowBoxEx,( HWND hwnd, int Top));\
\
/*--------窗口取顶边--------*/\
CMDINFO(int,GetTop_WindowBoxEx,( HWND hwnd));\
\
/*--------窗口置宽度--------*/\
CMDINFO(void,SetWidth_WindowBoxEx,( HWND hwnd, int Width));\
\
/*--------窗口取宽度--------*/\
CMDINFO(int,GetWidth_WindowBoxEx,( HWND hwnd));\
\
/*--------窗口置高度--------*/\
CMDINFO(void,SetHeight_WindowBoxEx,( HWND hwnd, int Height));\
\
/*--------窗口取高度--------*/\
CMDINFO(int,GetHeight_WindowBoxEx,( HWND hwnd));\
\
/*--------置矩形--------*/\
CMDINFO(void,SetRect_WindowBoxEx,( HWND hwnd, int left, int top, int Width, int Height));\
\
/*--------取矩形--------*/\
CMDINFO(void,GetRect_WindowBoxEx,( HWND hwnd, int * Left, int * Top, int * Width, int * Height));\
\
/*--------置位置--------*/\
CMDINFO(void,SetLocation_WindowBoxEx,( HWND hwnd, int Location));\
\
/*--------取位置--------*/\
CMDINFO(int,GetLocation_WindowBoxEx,( HWND hwnd));\
\
/*--------取可视--------*/\
CMDINFO(BOOL,GetVisual_WindowBoxEx,( HWND hwnd));\
\
/*--------置可视--------*/\
CMDINFO(void,SetVisual_WindowBoxEx,( HWND hwnd, BOOL Visual));\
\
/*--------取禁止--------*/\
CMDINFO(BOOL,GetDisabled_WindowBoxEx,( HWND hwnd));\
\
/*--------置禁止--------*/\
CMDINFO(void,SetDisabled_WindowBoxEx,( HWND hwnd, BOOL Disabled));\
\
/*--------窗口置标识--------*/\
CMDINFO(void,SetSign_WindowBoxEx,( HWND hwnd, AutoInt Sign));\
\
/*--------窗口取标识--------*/\
CMDINFO(AutoInt,GetSign_WindowBoxEx,( HWND hwnd));\
\
/*--------置父窗口--------*/\
CMDINFO(void,SetParent_WindowBoxEx,( HWND hwnd, HWND Parent));\
\
/*--------取父窗口--------*/\
CMDINFO(HWND,GetParent_WindowBoxEx,( HWND hwnd));\
\
/*--------取图标--------*/\
CMDINFO(BinEx_P,GetIco_WindowBoxEx,( HWND hwnd));\
\
/*--------置图标--------*/\
CMDINFO(void,SetIco_WindowBoxEx,( HWND hwnd, BinEx_P icon));\
\
/*--------取标题--------*/\
CMDINFO(StrEx_P,GetTitle_WindowBoxEx,( HWND hwnd));\
\
/*--------置标题--------*/\
CMDINFO(void,SetTitle_WindowBoxEx,( HWND hwnd, StrEx_P Title));\
\
/*--------取风格--------*/\
CMDINFO(int,GetStyle_WindowBoxEx,( HWND hwnd));\
\
/*--------置风格--------*/\
CMDINFO(void,Setstyle_WindowBoxEx,( HWND hwnd, int style));\
\
/*--------取扩展风格--------*/\
CMDINFO(int,GetExStyle_WindowBoxEx,( HWND hwnd));\
\
/*--------置扩展风格--------*/\
CMDINFO(void,SetExStyle_WindowBoxEx,( HWND hwnd, int ExStyle));\
\
/*--------置任务栏显示--------*/\
CMDINFO(void,SetTaskbar_WindowBoxEx,( HWND hwnd, BOOL show));\
\
/*--------取任务栏显示--------*/\
CMDINFO(BOOL,GetTaskbar_WindowBoxEx,( HWND hwnd));\
\
/*--------置总在最前--------*/\
CMDINFO(void,SetAlwaysTop_WindowBoxEx,( HWND hwnd, BOOL AlwaysTop));\
\
/*--------取总在最前--------*/\
CMDINFO(BOOL,GetAlwaysTop_WindowBoxEx,( HWND hwnd));\
\
/*--------置退出键关闭--------*/\
CMDINFO(void,SetEscClose_WindowBoxEx,( HWND hwnd, BOOL EscClose));\
\
/*--------取退出键关闭--------*/\
CMDINFO(BOOL,GetEscClose_WindowBoxEx,( HWND hwnd));\
\
/*--------启用文件拖放--------*/\
CMDINFO(void,EnableDragDrop_WindowBoxEx,( HWND hwnd));\
\
/*--------关闭文件拖放--------*/\
CMDINFO(void,CloseDragDrop_WindowBoxEx,( HWND hwnd));\
\
/*--------置焦点--------*/\
CMDINFO(void,SetFocus_WindowBoxEx,( HWND hwnd));\
\
/*--------可有焦点--------*/\
CMDINFO(BOOL,GetFocus_WindowBoxEx,( HWND hwnd));\
\
/*--------调整层次--------*/\
CMDINFO(void,SetLevel_WindowBoxEx,( HWND hwnd, int Level));\
\
/*--------置托盘图标--------*/\
CMDINFO(void,SetNotifyIcon_WindowBoxEx,( HWND hwnd, BinEx_P Icon, StrEx_P Tip));\
\
/*--------发送消息--------*/\
CMDINFO(AutoInt,SendMessage_WindowBoxEx,( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam));\
\
/*--------投递消息--------*/\
CMDINFO(AutoInt,PostMessage_WindowBoxEx,( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam));\
\
/*--------消息循环--------*/\
CMDINFO(void,MessageLoop_WindowBoxEx,( HWND hwnd));\
\
/*--------创建--------*/\
CMDINFO(StructEx_P,Create_StructEx,( ));\
\
/*--------销毁--------*/\
CMDINFO(void,Destroy_StructEx,( StructEx_P Struct));\
\
/*--------载入--------*/\
CMDINFO(BOOL,Load_StructEx,( StructEx_P Struct, BinEx_P Data, AutoInt DataType, AutoInt Parameter1, AutoInt Parameter2, AutoInt Callback, AutoInt CallbackParameter));\
\
/*--------打包--------*/\
CMDINFO(BinEx_P,Pack_StructEx,( StructEx_P Struct, AutoInt DataType, AutoInt Parameter1, AutoInt Parameter2, AutoInt Callback, AutoInt CallbackParameter));\
\
/*--------插入--------*/\
CMDINFO(AutoInt,InsertMember_StructEx,( StructEx_P Struct, AutoInt Member, AutoInt addnum));\
\
/*--------删除--------*/\
CMDINFO(void,DeleteMember_StructEx,( StructEx_P Struct, AutoInt delMember, AutoInt deletenum));\
\
/*--------取数量--------*/\
CMDINFO(AutoInt,GetMemberCount_StructEx,( StructEx_P Struct));\
\
/*--------取类型--------*/\
CMDINFO(int,GetMemberType_StructEx,( StructEx_P Struct, AutoInt Member));\
\
/*--------置名称--------*/\
CMDINFO(void,SetMemberName_StructEx,( StructEx_P Struct, AutoInt Member, StrEx_P Name));\
\
/*--------取名称--------*/\
CMDINFO(StrEx_P,GetMemberName_StructEx,( StructEx_P Struct, AutoInt Member));\
\
/*--------置逻辑--------*/\
CMDINFO(void,SetMemberBool_StructEx,( StructEx_P Struct, AutoInt Member, BOOL logic));\
\
/*--------取逻辑--------*/\
CMDINFO(BOOL,GetMemberBool_StructEx,( StructEx_P Struct, AutoInt Member));\
\
/*--------置整数--------*/\
CMDINFO(void,SetMemberInt_StructEx,( StructEx_P Struct, AutoInt Member, int integer));\
\
/*--------取整数--------*/\
CMDINFO(int,GetMemberInt_StructEx,( StructEx_P Struct, AutoInt Member));\
\
/*--------置长整数--------*/\
CMDINFO(void,SetMemberLongInt_StructEx,( StructEx_P Struct, AutoInt Member, INT64 longinteger));\
\
/*--------取长整数--------*/\
CMDINFO(INT64,GetMemberLongInt_StructEx,( StructEx_P Struct, AutoInt Member));\
\
/*--------置小数--------*/\
CMDINFO(void,SetMemberFloat_StructEx,( StructEx_P Struct, AutoInt Member, float decimal));\
\
/*--------取小数--------*/\
CMDINFO(float,GetMemberFloat_StructEx,( StructEx_P Struct, AutoInt Member));\
\
/*--------置双精度小数--------*/\
CMDINFO(void,SetMemberDouble_StructEx,( StructEx_P Struct, AutoInt Member, double DoubleDecimal));\
\
/*--------取双精度小数--------*/\
CMDINFO(double,GetMemberDouble_StructEx,( StructEx_P Struct, AutoInt Member));\
\
/*--------置文本--------*/\
CMDINFO(void,SetMemberText_StructEx,( StructEx_P Struct, AutoInt Member, StrEx_P Text));\
\
/*--------取文本--------*/\
CMDINFO(StrEx_P,GetMemberText_StructEx,( StructEx_P Struct, AutoInt Member));\
\
/*--------置数据--------*/\
CMDINFO(void,SetMemberBin_StructEx,( StructEx_P Struct, AutoInt Member, BinEx_P Bin));\
\
/*--------取数据--------*/\
CMDINFO(BinEx_P,GetMemberBin_StructEx,( StructEx_P Struct, AutoInt Member));\
\
/*--------置结构体--------*/\
CMDINFO(void,SetMemberStruct_StructEx,( StructEx_P Struct, AutoInt Member, StructEx_P StructInfo));\
\
/*--------取结构体--------*/\
CMDINFO(StructEx_P,GetMemberStruct_StructEx,( StructEx_P Struct, AutoInt Member));\
\
/*--------插入--------*/\
CMDINFO(AutoInt,InsertAttr_StructEx,( StructEx_P Struct, AutoInt Attr, AutoInt addnum));\
\
/*--------删除--------*/\
CMDINFO(void,DeleteAttr_StructEx,( StructEx_P Struct, AutoInt delAttr, AutoInt deletenum));\
\
/*--------取数量--------*/\
CMDINFO(AutoInt,GetAttrCount_StructEx,( StructEx_P Struct));\
\
/*--------取类型--------*/\
CMDINFO(int,GetAttrType_StructEx,( StructEx_P Struct, AutoInt Attr));\
\
/*--------置名称--------*/\
CMDINFO(void,SetAttrName_StructEx,( StructEx_P Struct, AutoInt Attr, StrEx_P Name));\
\
/*--------取名称--------*/\
CMDINFO(StrEx_P,GetAttrName_StructEx,( StructEx_P Struct, AutoInt Attr));\
\
/*--------置逻辑--------*/\
CMDINFO(void,SetAttrBool_StructEx,( StructEx_P Struct, AutoInt Attr, BOOL logic));\
\
/*--------取逻辑--------*/\
CMDINFO(BOOL,GetAttrBool_StructEx,( StructEx_P Struct, AutoInt Attr));\
\
/*--------置整数--------*/\
CMDINFO(void,SetAttrInt_StructEx,( StructEx_P Struct, AutoInt Attr, int integer));\
\
/*--------取整数--------*/\
CMDINFO(int,GetAttrInt_StructEx,( StructEx_P Struct, AutoInt Attr));\
\
/*--------置长整数--------*/\
CMDINFO(void,SetAttrLongInt_StructEx,( StructEx_P Struct, AutoInt Attr, INT64 longinteger));\
\
/*--------取长整数--------*/\
CMDINFO(INT64,GetAttrLongInt_StructEx,( StructEx_P Struct, AutoInt Attr));\
\
/*--------置小数--------*/\
CMDINFO(void,SetAttrFloat_StructEx,( StructEx_P Struct, AutoInt Attr, float decimal));\
\
/*--------取小数--------*/\
CMDINFO(float,GetAttrFloat_StructEx,( StructEx_P Struct, AutoInt Attr));\
\
/*--------置双精度小数--------*/\
CMDINFO(void,SetAttrDouble_StructEx,( StructEx_P Struct, AutoInt Attr, double DoubleDecimal));\
\
/*--------取双精度小数--------*/\
CMDINFO(double,GetAttrDouble_StructEx,( StructEx_P Struct, AutoInt Attr));\
\
/*--------置文本--------*/\
CMDINFO(void,SetAttrText_StructEx,( StructEx_P Struct, AutoInt Attr, StrEx_P Text));\
\
/*--------取文本--------*/\
CMDINFO(StrEx_P,GetAttrText_StructEx,( StructEx_P Struct, AutoInt Attr));\
\
/*--------置数据--------*/\
CMDINFO(void,SetAttrBin_StructEx,( StructEx_P Struct, AutoInt Attr, BinEx_P Bin));\
\
/*--------取数据--------*/\
CMDINFO(BinEx_P,GetAttrBin_StructEx,( StructEx_P Struct, AutoInt Attr));\
\
/*--------置结构体--------*/\
CMDINFO(void,SetAttrStruct_StructEx,( StructEx_P Struct, AutoInt Attr, StructEx_P StructInfo));\
\
/*--------取结构体--------*/\
CMDINFO(StructEx_P,GetAttrStruct_StructEx,( StructEx_P Struct, AutoInt Attr));\
\
/*--------创建组件_窗口EX--------*/\
CMDINFO(WindowEx_P,CreateControl_WindowEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, BinEx_P Icon, StrEx_P Text, BinEx_P Font, int FontClour, int LayeredTransparency, int DragPositionMode, int DragSizeMode, int MaxMode, BinEx_P ButtonData, int AnimationParam, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_窗口EX--------*/\
CMDINFO(void,SetAttribute_WindowEx,( WindowEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_窗口EX--------*/\
CMDINFO(AutoInt,GetAttribute_WindowEx,( WindowEx_P control, AutoInt index));\
\
/*--------插入控制纽_窗口EX--------*/\
CMDINFO(AutoInt,InsertButton_WindowEx,( WindowEx_P control, AutoInt index, AutoInt Data, int Type, BinEx_P skin, BOOL Visual, BOOL Disabled, BOOL Selected));\
\
/*--------删除_窗口EX--------*/\
CMDINFO(void,DeleteButton_WindowEx,( WindowEx_P control, AutoInt index));\
\
/*--------置类型_窗口EX--------*/\
CMDINFO(void,SetButtonType_WindowEx,( WindowEx_P control, AutoInt index, int Type));\
\
/*--------取类型_窗口EX--------*/\
CMDINFO(int,GetButtonType_WindowEx,( WindowEx_P control, AutoInt index));\
\
/*--------置按钮皮肤_窗口EX--------*/\
CMDINFO(void,SetButtonskin_WindowEx,( WindowEx_P control, AutoInt index, BinEx_P skin));\
\
/*--------取按钮皮肤_窗口EX--------*/\
CMDINFO(BinEx_P,GetButtonskin_WindowEx,( WindowEx_P control, AutoInt index));\
\
/*--------置可视_窗口EX--------*/\
CMDINFO(void,SetButtonVisual_WindowEx,( WindowEx_P control, AutoInt index, BOOL Show));\
\
/*--------取可视_窗口EX--------*/\
CMDINFO(BOOL,GetButtonVisual_WindowEx,( WindowEx_P control, AutoInt index));\
\
/*--------置禁止_窗口EX--------*/\
CMDINFO(void,SetButtonDisabled_WindowEx,( WindowEx_P control, AutoInt index, BOOL Disabled));\
\
/*--------取禁止_窗口EX--------*/\
CMDINFO(BOOL,GetButtonDisabled_WindowEx,( WindowEx_P control, AutoInt index));\
\
/*--------置选中_窗口EX--------*/\
CMDINFO(void,SetButtonSelected_WindowEx,( WindowEx_P control, AutoInt index, BOOL Selected));\
\
/*--------取选中_窗口EX--------*/\
CMDINFO(BOOL,GetButtonSelected_WindowEx,( WindowEx_P control, AutoInt index));\
\
/*--------取数量_窗口EX--------*/\
CMDINFO(AutoInt,GetButtonCount_WindowEx,( WindowEx_P control));\
\
/*--------取数值_窗口EX--------*/\
CMDINFO(AutoInt,GetButtonData_WindowEx,( WindowEx_P control, AutoInt index));\
\
/*--------置数值_窗口EX--------*/\
CMDINFO(void,SetButtonData_WindowEx,( WindowEx_P control, AutoInt index, AutoInt Data));\
\
/*--------添加组件_窗口EX--------*/\
CMDINFO(BOOL,InsertControl_WindowEx,( WindowEx_P control, Control_P ControlHand, int left, int top));\
\
/*--------移除组件_窗口EX--------*/\
CMDINFO(BOOL,RemoveControl_WindowEx,( WindowEx_P control, Control_P ControlHand, Control_P Parentcontrol, int left, int top));\
\
/*--------调用反馈事件_窗口EX--------*/\
CMDINFO(AutoInt,CallFeedBackEvent_WindowEx,( WindowEx_P control, AutoInt Relevant1, AutoInt Relevant2, BOOL Mode));\
\
/*--------创建组件_标签EX--------*/\
CMDINFO(LabelEx_P,CreateControl_LabelEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, StrEx_P Text, int Align, int BackColor, BinEx_P Font, int FontClour, int Rotate, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_标签Ex--------*/\
CMDINFO(void,SetAttribute_LabelEx,( LabelEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_标签Ex--------*/\
CMDINFO(AutoInt,GetAttribute_LabelEx,( LabelEx_P control, AutoInt index));\
\
/*--------创建组件_滤镜EX--------*/\
CMDINFO(FilterEx_P,CreateControl_FilterEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, int FilterExMode, int Parameter1, int Parameter2, int Parameter3, int Parameter4, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_滤镜Ex--------*/\
CMDINFO(void,SetAttribute_FilterEx,( FilterEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_滤镜Ex--------*/\
CMDINFO(AutoInt,GetAttribute_FilterEx,( FilterEx_P control, AutoInt index));\
\
/*--------创建组件_按钮EX--------*/\
CMDINFO(ButtonEx_P,CreateControl_ButtonEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, StrEx_P Text, BinEx_P Font, int FontClour, int AnimationParam, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_按钮Ex--------*/\
CMDINFO(void,SetAttribute_ButtonEx,( ButtonEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_按钮Ex--------*/\
CMDINFO(AutoInt,GetAttribute_ButtonEx,( ButtonEx_P control, AutoInt index));\
\
/*--------创建组件_图片按钮EX--------*/\
CMDINFO(ImagebuttonEx_P,CreateControl_ImagebuttonEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, BOOL Selected, int AnimationParam, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_图片按钮Ex--------*/\
CMDINFO(void,SetAttribute_ImagebuttonEx,( ImagebuttonEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_图片按钮Ex--------*/\
CMDINFO(AutoInt,GetAttribute_ImagebuttonEx,( ImagebuttonEx_P control, AutoInt index));\
\
/*--------创建组件_超级按钮EX--------*/\
CMDINFO(SuperbuttonEx_P,CreateControl_SuperbuttonEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, BOOL Selected, BinEx_P Icon, int IconWidth, int IconHeight, int  Align, StrEx_P Text, BinEx_P Font, int FontClour, int AnimationParam, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_超级按钮Ex--------*/\
CMDINFO(void,SetAttribute_SuperbuttonEx,( SuperbuttonEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_超级按钮Ex--------*/\
CMDINFO(AutoInt,GetAttribute_SuperbuttonEx,( SuperbuttonEx_P control, AutoInt index));\
\
/*--------创建组件_选择框EX--------*/\
CMDINFO(ChoiceboxEx_P,CreateControl_ChoiceboxEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, BOOL Selected, StrEx_P Text, BinEx_P Font, int FontClour, int AnimationParam, int SelectedMode, int SelectedState, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_选择框Ex--------*/\
CMDINFO(void,SetAttribute_ChoiceboxEx,( ChoiceboxEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_选择框Ex--------*/\
CMDINFO(AutoInt,GetAttribute_ChoiceboxEx,( ChoiceboxEx_P control, AutoInt index));\
\
/*--------创建组件_单选框EX--------*/\
CMDINFO(RadiobuttonEx_P,CreateControl_RadiobuttonEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, BOOL Selected, StrEx_P Text, BinEx_P Font, int FontClour, int AnimationParam, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_单选框Ex--------*/\
CMDINFO(void,SetAttribute_RadiobuttonEx,( RadiobuttonEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_单选框Ex--------*/\
CMDINFO(AutoInt,GetAttribute_RadiobuttonEx,( RadiobuttonEx_P control, AutoInt index));\
\
/*--------创建组件_编辑框EX--------*/\
CMDINFO(EditboxEx_P,CreateControl_EditboxEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, StrEx_P Content, int Align, int InputMode, int MaxInput, StrEx_P PasswordSubstitution, int CursorColor, BinEx_P Font, int FontClour, int SelectedFontColor, int SelectedColor, int LeftReservation, int RightReservation, BOOL Multiline, BOOL Wrapped, int ScrollBarMode, int MenuTableWidth, int MenuTableHeight, BinEx_P MenuFont, int MenuFontClour, int MenuDisabledFontClour, int MenuTransparency, int MenuLanguage, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_编辑框Ex--------*/\
CMDINFO(void,SetAttribute_EditboxEx,( EditboxEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_编辑框Ex--------*/\
CMDINFO(AutoInt,GetAttribute_EditboxEx,( EditboxEx_P control, AutoInt index));\
\
/*--------插入文本_编辑框Ex--------*/\
CMDINFO(void,InsertText_EditboxEx,( EditboxEx_P control, AutoInt weizhi, StrEx_P text));\
\
/*--------删除文本_编辑框Ex--------*/\
CMDINFO(void,DeleteText_EditboxEx,( EditboxEx_P control, AutoInt weizhi, AutoInt dellen));\
\
/*--------置光标位置_编辑框Ex--------*/\
CMDINFO(void,SetInsertCursor_EditboxEx,( EditboxEx_P control, AutoInt index));\
\
/*--------取光标位置_编辑框Ex--------*/\
CMDINFO(AutoInt,GetInsertCursor_EditboxEx,( EditboxEx_P control));\
\
/*--------置选择文本长度_编辑框Ex--------*/\
CMDINFO(void,SetSelectLeng_EditboxEx,( EditboxEx_P control, AutoInt SelectLeng));\
\
/*--------取选择文本长度_编辑框Ex--------*/\
CMDINFO(AutoInt,GetSelectLeng_EditboxEx,( EditboxEx_P control));\
\
/*--------保证显示字符_编辑框Ex--------*/\
CMDINFO(void,GuaranteeVisibleText_EditboxEx,( EditboxEx_P control, AutoInt index, int mode));\
\
/*--------创建组件_组合框EX--------*/\
CMDINFO(ComboboxEx_P,CreateControl_ComboboxEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, StrEx_P Content, int Align, int InputMode, int MaxInput, StrEx_P PasswordSubstitution, int CursorColor, BinEx_P Font, int FontClour, int SelectedFontColor, int SelectedColor, int LeftReservation, int RightReservation, int MenuTableWidth, int MenuTableHeight, BinEx_P MenuFont, int MenuFontClour, int MenuDisabledFontClour, int MenuTransparency, int MenuLanguage, int DownListWidth, int DownListMaxHeight, AutoInt DownListCurrentItem, int DownListIconWidth, int DownListIconHeight, BinEx_P DownListFont, BOOL DownListAlternate, int DownListScrollBarMode, int DownListAttributeTransparency, BinEx_P DownListItemData, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_组合框Ex--------*/\
CMDINFO(void,SetAttribute_ComboboxEx,( ComboboxEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_组合框Ex--------*/\
CMDINFO(AutoInt,GetAttribute_ComboboxEx,( ComboboxEx_P control, AutoInt index));\
\
/*--------插入文本_组合框Ex--------*/\
CMDINFO(void,InsertText_ComboboxEx,( ComboboxEx_P control, AutoInt weizhi, StrEx_P text));\
\
/*--------删除文本_组合框Ex--------*/\
CMDINFO(void,DeleteText_ComboboxEx,( ComboboxEx_P control, AutoInt weizhi, AutoInt dellen));\
\
/*--------置光标位置_组合框Ex--------*/\
CMDINFO(void,SetInsertCursor_ComboboxEx,( ComboboxEx_P control, AutoInt index));\
\
/*--------取光标位置_组合框Ex--------*/\
CMDINFO(int,GetInsertCursor_ComboboxEx,( ComboboxEx_P control));\
\
/*--------置选择文本长度_组合框Ex--------*/\
CMDINFO(void,SetSelectLeng_ComboboxEx,( ComboboxEx_P control, AutoInt SelectLeng));\
\
/*--------取选择文本长度_组合框Ex--------*/\
CMDINFO(int,GetSelectLeng_ComboboxEx,( ComboboxEx_P control));\
\
/*--------保证显示字符_组合框Ex--------*/\
CMDINFO(void,GuaranteeVisibleText_ComboboxEx,( ComboboxEx_P control, AutoInt index, int mode));\
\
/*--------弹出_组合框Ex--------*/\
CMDINFO(void,PopUp_DownlistEx_ComboboxEx,( ComboboxEx_P control));\
\
/*--------关闭_组合框Ex--------*/\
CMDINFO(void,Close_DownlistEx_ComboboxEx,( ComboboxEx_P control));\
\
/*--------是否已弹出_组合框Ex--------*/\
CMDINFO(BOOL,IsPopUp_DownlistEx_ComboboxEx,( ComboboxEx_P control));\
\
/*--------插入_组合框Ex--------*/\
CMDINFO(AutoInt,InsertItem_DownlistEx_ComboboxEx,( ComboboxEx_P control, AutoInt index, AutoInt addnum, AutoInt Data, ImageEx_P Ico, StrEx_P Title, int FontColor, int Size));\
\
/*--------删除_组合框Ex--------*/\
CMDINFO(void,DeleteItem_DownlistEx_ComboboxEx,( ComboboxEx_P control, AutoInt index, int deletenum));\
\
/*--------取数量_组合框Ex--------*/\
CMDINFO(AutoInt,GetItemCount_DownlistEx_ComboboxEx,( ComboboxEx_P control));\
\
/*--------置附加值_组合框Ex--------*/\
CMDINFO(void,SetItemData_DownlistEx_ComboboxEx,( ComboboxEx_P control, AutoInt index, AutoInt Data));\
\
/*--------取附加值_组合框Ex--------*/\
CMDINFO(AutoInt,GetItemData_DownlistEx_ComboboxEx,( ComboboxEx_P control, AutoInt index));\
\
/*--------置图标_组合框Ex--------*/\
CMDINFO(void,SetItemIco_DownlistEx_ComboboxEx,( ComboboxEx_P control, AutoInt index, ImageEx_P Ico));\
\
/*--------取图标_组合框Ex--------*/\
CMDINFO(ImageEx_P,GetItemIco_DownlistEx_ComboboxEx,( ComboboxEx_P control, AutoInt index));\
\
/*--------置标题_组合框Ex--------*/\
CMDINFO(void,SetItemTitle_DownlistEx_ComboboxEx,( ComboboxEx_P control, AutoInt index, StrEx_P Title));\
\
/*--------取标题_组合框Ex--------*/\
CMDINFO(StrEx_P,GetItemTitle_DownlistEx_ComboboxEx,( ComboboxEx_P control, AutoInt index));\
\
/*--------置字体色_组合框Ex--------*/\
CMDINFO(void,SetItemFontColor_DownlistEx_ComboboxEx,( ComboboxEx_P control, AutoInt index, int FontColor));\
\
/*--------取字体色_组合框Ex--------*/\
CMDINFO(int,GetItemFontColor_DownlistEx_ComboboxEx,( ComboboxEx_P control, AutoInt index));\
\
/*--------置项目高度_组合框Ex--------*/\
CMDINFO(void,SetItemSize_DownlistEx_ComboboxEx,( ComboboxEx_P control, AutoInt index, int Size));\
\
/*--------取项目高度_组合框Ex--------*/\
CMDINFO(int,GetItemSize_DownlistEx_ComboboxEx,( ComboboxEx_P control, AutoInt index));\
\
/*--------保证显示_组合框Ex--------*/\
CMDINFO(void,GuaranteeVisible_DownlistEx_ComboboxEx,( ComboboxEx_P control, AutoInt index, int mode));\
\
/*--------创建组件_分组框EX--------*/\
CMDINFO(MinutesboxEx_P,CreateControl_MinutesboxEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, BinEx_P Picture, int IconWidth, int IconHeight, StrEx_P Text, int TextMode, BinEx_P Font, int FontClour, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_分组框Ex--------*/\
CMDINFO(void,SetAttribute_MinutesboxEx,( MinutesboxEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_分组框Ex--------*/\
CMDINFO(AutoInt,GetAttribute_MinutesboxEx,( MinutesboxEx_P control, AutoInt index));\
\
/*--------添加组件_分组框Ex--------*/\
CMDINFO(BOOL,InsertControl_MinutesboxEx,( MinutesboxEx_P control, Control_P ControlHand, int left, int top));\
\
/*--------移除组件_分组框Ex--------*/\
CMDINFO(BOOL,RemoveControl_MinutesboxEx,( MinutesboxEx_P control, Control_P ControlHand, Control_P Parentcontrol, int left, int top));\
\
/*--------创建组件_多功能按钮EX--------*/\
CMDINFO(MultifunctionButtonEx_P,CreateControl_MultifunctionButtonEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, int ButtonStyles, int PartnerSize, int BackColor, BinEx_P Icon, int IconWidth, int IconHeight, int Align, StrEx_P Text, BinEx_P Font, int FontClour, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_多功能按钮EX--------*/\
CMDINFO(void,SetAttribute_MultifunctionButtonEx,( MultifunctionButtonEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_多功能按钮EX--------*/\
CMDINFO(AutoInt,GetAttribute_MultifunctionButtonEx,( MultifunctionButtonEx_P control, AutoInt index));\
\
/*--------创建组件_图片框EX--------*/\
CMDINFO(PictureBoxEx_P,CreateControl_PictureBoxEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, BinEx_P Picture, int MapMode, int Angle, BOOL PlayAnimation, int AllFrame, int CurrentFrame, int Rotate, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_图片框Ex--------*/\
CMDINFO(void,SetAttribute_PictureBoxEx,( PictureBoxEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_图片框Ex--------*/\
CMDINFO(AutoInt,GetAttribute_PictureBoxEx,( PictureBoxEx_P control, AutoInt index));\
\
/*--------创建组件_进度条EX--------*/\
CMDINFO(ProgressbarEx_P,CreateControl_ProgressbarEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, int Position, int MiniPosition, int MaxiPosition, int Style, int Direction, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_进度条Ex--------*/\
CMDINFO(void,SetAttribute_ProgressbarEx,( ProgressbarEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_进度条Ex--------*/\
CMDINFO(AutoInt,GetAttribute_ProgressbarEx,( ProgressbarEx_P control, AutoInt index));\
\
/*--------创建组件_滚动条EX--------*/\
CMDINFO(ScrollbarEx_P,CreateControl_ScrollbarEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, int Position, int MiniPosition, int MaxiPosition, int PageLength, int RowChangeValue, int PageChangeValue, int ScrollChangeValue, BOOL DragTrace, BOOL schedule, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_滚动条Ex--------*/\
CMDINFO(void,SetAttribute_ScrollbarEx,( ScrollbarEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_滚动条Ex--------*/\
CMDINFO(AutoInt,GetAttribute_ScrollbarEx,( ScrollbarEx_P control, AutoInt index));\
\
/*--------创建组件_滑块条EX--------*/\
CMDINFO(SliderbarEx_P,CreateControl_SliderbarEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, int Position, int Progress, int MiniPosition, int MaxiPosition, int RowChangeValue, int PageChangeValue, BOOL DragTrace, int Style, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_滑块条Ex--------*/\
CMDINFO(void,SetAttribute_SliderbarEx,( SliderbarEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_滑块条Ex--------*/\
CMDINFO(AutoInt,GetAttribute_SliderbarEx,( SliderbarEx_P control, AutoInt index));\
\
/*--------创建组件_选择夹EX--------*/\
CMDINFO(SelectthefolderEx_P,CreateControl_SelectthefolderEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, AutoInt CurrentTable, int TableSize, int Direction, int Spacing, int TableStyle, int Reserve, int IconWidth, int IconHeight, BinEx_P Font, BinEx_P TableData, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_选择夹Ex--------*/\
CMDINFO(void,SetAttribute_SelectthefolderEx,( SelectthefolderEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_选择夹Ex--------*/\
CMDINFO(AutoInt,GetAttribute_SelectthefolderEx,( SelectthefolderEx_P control, AutoInt index));\
\
/*--------插入_选择夹Ex--------*/\
CMDINFO(AutoInt,InsertTab_SelectthefolderEx,( SelectthefolderEx_P control, AutoInt index, AutoInt Data, ImageEx_P Ico, StrEx_P Title, int FontColor, int Size));\
\
/*--------删除_选择夹Ex--------*/\
CMDINFO(void,DeleteTab_SelectthefolderEx,( SelectthefolderEx_P control, AutoInt ctrlindex));\
\
/*--------取数量_选择夹Ex--------*/\
CMDINFO(AutoInt,GetTabCount_SelectthefolderEx,( SelectthefolderEx_P control));\
\
/*--------取关联数值_选择夹Ex--------*/\
CMDINFO(AutoInt,GetTabData_SelectthefolderEx,( SelectthefolderEx_P control, AutoInt index));\
\
/*--------置关联数值_选择夹Ex--------*/\
CMDINFO(void,SetTabData_SelectthefolderEx,( SelectthefolderEx_P control, AutoInt index, AutoInt Data));\
\
/*--------置图标_选择夹Ex--------*/\
CMDINFO(void,SetTabIco_SelectthefolderEx,( SelectthefolderEx_P control, AutoInt index, ImageEx_P Ico));\
\
/*--------取图标_选择夹Ex--------*/\
CMDINFO(ImageEx_P,GetTabIco_SelectthefolderEx,( SelectthefolderEx_P control, AutoInt index));\
\
/*--------置文本_选择夹Ex--------*/\
CMDINFO(void,SetTabTitle_SelectthefolderEx,( SelectthefolderEx_P control, AutoInt index, StrEx_P Title));\
\
/*--------取文本_选择夹Ex--------*/\
CMDINFO(StrEx_P,GetTabTitle_SelectthefolderEx,( SelectthefolderEx_P control, AutoInt index));\
\
/*--------置字体色_选择夹Ex--------*/\
CMDINFO(void,SetTabFontColor_SelectthefolderEx,( SelectthefolderEx_P control, AutoInt index, int FontColor));\
\
/*--------取字体色_选择夹Ex--------*/\
CMDINFO(int,GetTabFontColor_SelectthefolderEx,( SelectthefolderEx_P control, AutoInt index));\
\
/*--------置尺寸_选择夹Ex--------*/\
CMDINFO(void,SetTabSize_SelectthefolderEx,( SelectthefolderEx_P control, AutoInt index, int Size));\
\
/*--------取尺寸_选择夹Ex--------*/\
CMDINFO(int,GetTabSize_SelectthefolderEx,( SelectthefolderEx_P control, AutoInt index));\
\
/*--------保证显示_选择夹Ex--------*/\
CMDINFO(void,GuaranteeVisible_SelectthefolderEx,( SelectthefolderEx_P control, AutoInt index, int DisplayMode));\
\
/*--------添加组件_选择夹Ex--------*/\
CMDINFO(BOOL,InsertControl_SelectthefolderEx,( SelectthefolderEx_P control, AutoInt Table, Control_P ControlHand, int left, int top));\
\
/*--------移除组件_选择夹Ex--------*/\
CMDINFO(BOOL,RemoveControl_SelectthefolderEx,( SelectthefolderEx_P control, AutoInt Table, Control_P ControlHand, Control_P Parentcontrol, int left, int top));\
\
/*--------创建组件_工具条EX--------*/\
CMDINFO(ToolbarEx_P,CreateControl_ToolbarEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, int BackMode, BOOL schedule, int Spacing, int IconWidth, int IconHeight, BinEx_P Font, BinEx_P ButtonData, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_工具条Ex--------*/\
CMDINFO(void,SetAttribute_ToolbarEx,( ToolbarEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_工具条Ex--------*/\
CMDINFO(AutoInt,GetAttribute_ToolbarEx,( ToolbarEx_P control, AutoInt index));\
\
/*--------插入_工具条Ex--------*/\
CMDINFO(AutoInt,InsertButton_ToolbarEx,( ToolbarEx_P control, AutoInt index, AutoInt Data, ImageEx_P Ico, StrEx_P Title, int FontColor, int Size, int Type, int Align, BOOL Disabled, BOOL Selected));\
\
/*--------删除_工具条Ex--------*/\
CMDINFO(void,DeleteButton_ToolbarEx,( ToolbarEx_P control, AutoInt index));\
\
/*--------取数量_工具条Ex--------*/\
CMDINFO(AutoInt,GetButtonCount_ToolbarEx,( ToolbarEx_P control));\
\
/*--------置关联数值_工具条Ex--------*/\
CMDINFO(void,SetButtonData_ToolbarEx,( ToolbarEx_P control, AutoInt index, AutoInt Data));\
\
/*--------取关联数值_工具条Ex--------*/\
CMDINFO(AutoInt,GetButtonData_ToolbarEx,( ToolbarEx_P control, AutoInt index));\
\
/*--------置图标_工具条Ex--------*/\
CMDINFO(void,SetButtonIco_ToolbarEx,( ToolbarEx_P control, AutoInt index, ImageEx_P Ico));\
\
/*--------取图标_工具条Ex--------*/\
CMDINFO(ImageEx_P,GetButtonIco_ToolbarEx,( ToolbarEx_P control, AutoInt index));\
\
/*--------置文本_工具条Ex--------*/\
CMDINFO(void,SetButtonTitle_ToolbarEx,( ToolbarEx_P control, AutoInt index, StrEx_P Title));\
\
/*--------取文本_工具条Ex--------*/\
CMDINFO(StrEx_P,GetButtonTitle_ToolbarEx,( ToolbarEx_P control, AutoInt index));\
\
/*--------置字体色_工具条Ex--------*/\
CMDINFO(void,SetButtonFontColor_ToolbarEx,( ToolbarEx_P control, AutoInt index, int FontColor));\
\
/*--------取字体色_工具条Ex--------*/\
CMDINFO(int,GetButtonFontColor_ToolbarEx,( ToolbarEx_P control, AutoInt index));\
\
/*--------置尺寸_工具条Ex--------*/\
CMDINFO(void,SetButtonSize_ToolbarEx,( ToolbarEx_P control, AutoInt index, int Size));\
\
/*--------取尺寸_工具条Ex--------*/\
CMDINFO(int,GetButtonSize_ToolbarEx,( ToolbarEx_P control, AutoInt index));\
\
/*--------置类型_工具条Ex--------*/\
CMDINFO(void,SetButtonType_ToolbarEx,( ToolbarEx_P control, AutoInt index, int Type));\
\
/*--------取类型_工具条Ex--------*/\
CMDINFO(int,GetButtonType_ToolbarEx,( ToolbarEx_P control, AutoInt index));\
\
/*--------置对齐方式_工具条Ex--------*/\
CMDINFO(void,SetButtonAlign_ToolbarEx,( ToolbarEx_P control, AutoInt index, int Align));\
\
/*--------取对齐方式_工具条Ex--------*/\
CMDINFO(int,GetButtonAlign_ToolbarEx,( ToolbarEx_P control, AutoInt index));\
\
/*--------置选中_工具条Ex--------*/\
CMDINFO(void,SetButtonSelected_ToolbarEx,( ToolbarEx_P control, AutoInt index, BOOL Selected));\
\
/*--------取选中_工具条Ex--------*/\
CMDINFO(BOOL,GetButtonSelected_ToolbarEx,( ToolbarEx_P control, AutoInt index));\
\
/*--------置禁止_工具条Ex--------*/\
CMDINFO(void,SetButtonDisabled_ToolbarEx,( ToolbarEx_P control, AutoInt index, BOOL Disabled));\
\
/*--------取禁止_工具条Ex--------*/\
CMDINFO(BOOL,GetButtonDisabled_ToolbarEx,( ToolbarEx_P control, AutoInt index));\
\
/*--------置字体_工具条Ex--------*/\
CMDINFO(void,SetButtonFont_ToolbarEx,( ToolbarEx_P control, AutoInt index, BinEx_P Font));\
\
/*--------取字体_工具条Ex--------*/\
CMDINFO(BinEx_P,GetButtonFont_ToolbarEx,( ToolbarEx_P control, AutoInt index));\
\
/*--------置皮肤_工具条Ex--------*/\
CMDINFO(void,SetButtonSkin_ToolbarEx,( ToolbarEx_P control, AutoInt index, BinEx_P Skin));\
\
/*--------取皮肤_工具条Ex--------*/\
CMDINFO(BinEx_P,GetButtonSkin_ToolbarEx,( ToolbarEx_P control, AutoInt index));\
\
/*--------保证显示_工具条Ex--------*/\
CMDINFO(void,GuaranteeVisible_ToolbarEx,( ToolbarEx_P control, AutoInt index, int DisplayMode));\
\
/*--------创建组件_列表框EX--------*/\
CMDINFO(ListboxEx_P,CreateControl_ListboxEx,(Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt* InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, AutoInt CurrentItem, int IconWidth, int IconHeight, BinEx_P Font, BOOL AlternateColor, int SelectMode, int ScrollBarMode, BinEx_P ItemData, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_列表框Ex--------*/\
CMDINFO(void,SetAttribute_ListboxEx,( ListboxEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_列表框Ex--------*/\
CMDINFO(AutoInt,GetAttribute_ListboxEx,( ListboxEx_P control, AutoInt index));\
\
/*--------插入_列表框Ex--------*/\
CMDINFO(AutoInt,InsertItem_ListboxEx,( ListboxEx_P control, AutoInt index, AutoInt addnum, AutoInt Data, ImageEx_P Ico, StrEx_P Title, int FontColor, int Size));\
\
/*--------删除_列表框Ex--------*/\
CMDINFO(void,DeleteItem_ListboxEx,( ListboxEx_P control, AutoInt index, AutoInt deletenum));\
\
/*--------取数量_列表框Ex--------*/\
CMDINFO(AutoInt,GetItemCount_ListboxEx,( ListboxEx_P control));\
\
/*--------置附加值_列表框Ex--------*/\
CMDINFO(void,SetItemData_ListboxEx,( ListboxEx_P control, AutoInt index, AutoInt Data));\
\
/*--------取附加值_列表框Ex--------*/\
CMDINFO(AutoInt,GetItemData_ListboxEx,( ListboxEx_P control, AutoInt index));\
\
/*--------置图标_列表框Ex--------*/\
CMDINFO(void,SetItemIco_ListboxEx,( ListboxEx_P control, AutoInt index, ImageEx_P Ico));\
\
/*--------取图标_列表框Ex--------*/\
CMDINFO(ImageEx_P,GetItemIco_ListboxEx,( ListboxEx_P control, AutoInt index));\
\
/*--------置标题_列表框Ex--------*/\
CMDINFO(void,SetItemTitle_ListboxEx,( ListboxEx_P control, AutoInt index, StrEx_P Title));\
\
/*--------取标题_列表框Ex--------*/\
CMDINFO(StrEx_P,GetItemTitle_ListboxEx,( ListboxEx_P control, AutoInt index));\
\
/*--------取字体色_列表框Ex--------*/\
CMDINFO(int,GetItemFontColor_ListboxEx,( ListboxEx_P control, AutoInt index));\
\
/*--------置字体色_列表框Ex--------*/\
CMDINFO(void,SetItemFontColor_ListboxEx,( ListboxEx_P control, AutoInt index, int FontColor));\
\
/*--------置项目高度_列表框Ex--------*/\
CMDINFO(void,SetItemSize_ListboxEx,( ListboxEx_P control, AutoInt index, int Size));\
\
/*--------取项目高度_列表框Ex--------*/\
CMDINFO(int,GetItemSize_ListboxEx,( ListboxEx_P control, AutoInt index));\
\
/*--------置字体_列表框Ex--------*/\
CMDINFO(void,SetItemFont_ListboxEx,( ListboxEx_P control, AutoInt index, BinEx_P Font));\
\
/*--------取字体_列表框Ex--------*/\
CMDINFO(BinEx_P,GetItemFont_ListboxEx,( ListboxEx_P control, AutoInt index));\
\
/*--------置皮肤_列表框Ex--------*/\
CMDINFO(void,SetItemSkin_ListboxEx,( ListboxEx_P control, AutoInt index, BinEx_P Skin));\
\
/*--------取皮肤_列表框Ex--------*/\
CMDINFO(BinEx_P,GetItemSkin_ListboxEx,( ListboxEx_P control, AutoInt index));\
\
/*--------置选中_列表框Ex--------*/\
CMDINFO(void,SetItemSelected_ListboxEx,( ListboxEx_P control, AutoInt index, BOOL Selected));\
\
/*--------取选中_列表框Ex--------*/\
CMDINFO(BOOL,GetItemSelected_ListboxEx,( ListboxEx_P control, AutoInt index));\
\
/*--------置滚动回调_列表框Ex--------*/\
CMDINFO(void,SetScrollCallBack_ListboxEx,( ListboxEx_P control, ExuiCallback Callback));\
\
/*--------取滚动回调_列表框Ex--------*/\
CMDINFO(ExuiCallback,GetScrollCallBack_ListboxEx,( ListboxEx_P control));\
\
/*--------置虚表回调_列表框Ex--------*/\
CMDINFO(void,SetVirtualTableCallBack_ListboxEx,( ListboxEx_P control, ExuiCallback Callback));\
\
/*--------取虚表回调_列表框Ex--------*/\
CMDINFO(ExuiCallback,GetVirtualTableCallBack_ListboxEx,( ListboxEx_P control));\
\
/*--------保证显示_列表框Ex--------*/\
CMDINFO(void,GuaranteeVisible_ListboxEx,( ListboxEx_P control, AutoInt index, int mode));\
\
/*--------创建组件_超级列表框EX--------*/\
CMDINFO(SuperListboxEx_P,CreateControl_SuperListboxEx,( Control_P  Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt* InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, AutoInt CurrentItem, AutoInt CurrentColumn, int HeadHeight, int HeadMode, BOOL AlternateColor, int SelectMode, BOOL EntireLine, int EventMode, int EditMode,int LineMode, int ScrollBarMode, BinEx_P ItemData, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_超级列表框Ex--------*/\
CMDINFO(void,SetAttribute_SuperListboxEx,( SuperListboxEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_超级列表框Ex--------*/\
CMDINFO(AutoInt,GetAttribute_SuperListboxEx,( SuperListboxEx_P control, AutoInt index));\
\
/*--------插入列_超级列表框Ex--------*/\
CMDINFO(AutoInt,InsertColumn_SuperListboxEx,( SuperListboxEx_P control, AutoInt cloid, AutoInt addnum, ImageEx_P Ico, StrEx_P Title, int FontColor, int wight, int min, int max, BinEx_P Font, int IcoW, int IcoH, int Align, BinEx_P ItemFont, int ItemIcoW, int ItemIcoH, int ItemAlign));\
\
/*--------删除列_超级列表框Ex--------*/\
CMDINFO(void,DeleteColumn_SuperListboxEx,( SuperListboxEx_P control, AutoInt delindex, AutoInt deletenum));\
\
/*--------取列数量_超级列表框Ex--------*/\
CMDINFO(AutoInt,GetColumnCount_SuperListboxEx,( SuperListboxEx_P control));\
\
/*--------置列属性_超级列表框Ex--------*/\
CMDINFO(void,SetColumnAttribute_SuperListboxEx,( SuperListboxEx_P control, AutoInt index, AutoInt AttributeId, ImageEx_P Ico, StrEx_P Title, int FontColor, int wight, int min, int max, BinEx_P Font, int IcoW, int IcoH, int Align, BinEx_P ItemFont, int ItemIcoW, int ItemIcoH, int ItemAlign));\
\
/*--------取列属性_超级列表框Ex--------*/\
CMDINFO(AutoInt,GetColumnAttribute_SuperListboxEx,( SuperListboxEx_P control, AutoInt index, AutoInt AttributeId));\
\
/*--------插入项目_超级列表框Ex--------*/\
CMDINFO(AutoInt,InsertItem_SuperListboxEx,( SuperListboxEx_P control, AutoInt index, AutoInt addnum, AutoInt Data, int Size));\
\
/*--------删除项目_超级列表框Ex--------*/\
CMDINFO(void,DeleteItem_SuperListboxEx,( SuperListboxEx_P control, AutoInt delindex, AutoInt deletenum));\
\
/*--------取数量_超级列表框Ex--------*/\
CMDINFO(AutoInt,GetItemCount_SuperListboxEx,( SuperListboxEx_P control));\
\
/*--------置附加值_超级列表框Ex--------*/\
CMDINFO(void,SetItemData_SuperListboxEx,( SuperListboxEx_P control, AutoInt index, AutoInt Data));\
\
/*--------取附加值_超级列表框Ex--------*/\
CMDINFO(AutoInt,GetItemData_SuperListboxEx,( SuperListboxEx_P control, AutoInt index));\
\
/*--------置图标_超级列表框Ex--------*/\
CMDINFO(void,SetItemIco_SuperListboxEx,( SuperListboxEx_P control, AutoInt index, AutoInt cloid, ImageEx_P Ico));\
\
/*--------取图标_超级列表框Ex--------*/\
CMDINFO(ImageEx_P,GetItemIco_SuperListboxEx,( SuperListboxEx_P control, AutoInt index, AutoInt cloid));\
\
/*--------置文本_超级列表框Ex--------*/\
CMDINFO(void,SetItemTitle_SuperListboxEx,( SuperListboxEx_P control, AutoInt index, AutoInt cloid, StrEx_P Title));\
\
/*--------取文本_超级列表框Ex--------*/\
CMDINFO(StrEx_P,GetItemTitle_SuperListboxEx,( SuperListboxEx_P control, AutoInt index, AutoInt cloid));\
\
/*--------置字体色_超级列表框Ex--------*/\
CMDINFO(void,SetItemFontColor_SuperListboxEx,( SuperListboxEx_P control, AutoInt index, AutoInt cloid, int FontColor));\
\
/*--------取字体色_超级列表框Ex--------*/\
CMDINFO(int,GetItemFontColor_SuperListboxEx,( SuperListboxEx_P control, AutoInt index, AutoInt cloid));\
\
/*--------置项目高度_超级列表框Ex--------*/\
CMDINFO(void,SetItemSize_SuperListboxEx,( SuperListboxEx_P control, AutoInt index, int Size));\
\
/*--------取项目高度_超级列表框Ex--------*/\
CMDINFO(int,GetItemSize_SuperListboxEx,( SuperListboxEx_P control, AutoInt index));\
\
/*--------置字体_超级列表框Ex--------*/\
CMDINFO(void,SetItemFont_SuperListboxEx,( SuperListboxEx_P control, AutoInt index, AutoInt cloid, BinEx_P Font));\
\
/*--------取字体_超级列表框Ex--------*/\
CMDINFO(BinEx_P,GetItemFont_SuperListboxEx,( SuperListboxEx_P control, AutoInt index, AutoInt cloid));\
\
/*--------置皮肤_超级列表框Ex--------*/\
CMDINFO(void,SetItemSkin_SuperListboxEx,( SuperListboxEx_P control, AutoInt index, AutoInt cloid, BinEx_P Skin));\
\
/*--------取皮肤_超级列表框Ex--------*/\
CMDINFO(BinEx_P,GetItemSkin_SuperListboxEx,( SuperListboxEx_P control, AutoInt index, AutoInt cloid));\
\
/*--------置选中_超级列表框Ex--------*/\
CMDINFO(void,SetItemSelected_SuperListboxEx,( SuperListboxEx_P control, AutoInt index, AutoInt cloid, BOOL Selected));\
\
/*--------取选中_超级列表框Ex--------*/\
CMDINFO(BOOL,GetItemSelected_SuperListboxEx,( SuperListboxEx_P control, AutoInt index, AutoInt cloid));\
\
/*--------置项目编辑配置_超级列表框Ex--------*/\
CMDINFO(void,SetItemEditConfig_SuperListboxEx,(SuperListboxEx_P control, int AttributeId,  int InputMode, int MaxInput, StrEx_P PasswordSubstitution, int Align, int Multiline, int Wrapped, BinEx_P Skin, BinEx_P Cursor, BinEx_P Font, int FontClour, int SelectedFontColor, int SelectedColor, int CursorColor, int ScrollBarMode, int MenuTableWidth, int MenuTableHeight, BinEx_P MenuFont, int MenuFontClour, int MenuDisabledFontClour, int MenuTransparency, int MenuLanguage));\
\
/*--------取项目编辑配置_超级列表框Ex--------*/\
CMDINFO(AutoInt,GetItemEditConfig_SuperListboxEx,(SuperListboxEx_P control, int AttributeId));\
\
/*--------进入编辑_超级列表框Ex--------*/\
CMDINFO(void,EnterEdit_SuperListboxEx,( SuperListboxEx_P control, AutoInt index, AutoInt cloid, AutoInt mode));\
\
/*--------退出编辑_超级列表框Ex--------*/\
CMDINFO(void,ExitEdit_SuperListboxEx,( SuperListboxEx_P control, AutoInt index, AutoInt cloid, AutoInt mode));\
\
/*--------置滚动回调_超级列表框Ex--------*/\
CMDINFO(void,SetScrollCallBack_SuperListboxEx,( SuperListboxEx_P control, ExuiCallback Callback));\
\
/*--------取滚动回调_超级列表框Ex--------*/\
CMDINFO(ExuiCallback,GetScrollCallBack_SuperListboxEx,( SuperListboxEx_P control));\
\
/*--------置虚表回调_超级列表框Ex--------*/\
CMDINFO(void,SetVirtualTableCallBack_SuperListboxEx,( SuperListboxEx_P control, ExuiCallback Callback));\
\
/*--------取虚表回调_超级列表框Ex--------*/\
CMDINFO(ExuiCallback,GetVirtualTableCallBack_SuperListboxEx,( SuperListboxEx_P control));\
\
/*--------保证显示_超级列表框Ex--------*/\
CMDINFO(void,GuaranteeVisible_SuperListboxEx,( SuperListboxEx_P control, AutoInt index, int cid, int mode));\
\
/*--------创建组件_图标列表框EX--------*/\
CMDINFO(IcoListboxEx_P,CreateControl_IcoListboxEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt* InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, AutoInt CurrentItem, int TableWidth, int TableHeight, int HSpacing, int LSpacing, int IconWidth, int IconHeight, BinEx_P Font, int PageLayout, int Align, int SelectMode,int ScrollBarMode, BinEx_P ItemData, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_图标列表框Ex--------*/\
CMDINFO(void,SetAttribute_IcoListboxEx,( IcoListboxEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_图标列表框Ex--------*/\
CMDINFO(AutoInt,GetAttribute_IcoListboxEx,( IcoListboxEx_P control, AutoInt index));\
\
/*--------插入项目_图标列表框Ex--------*/\
CMDINFO(AutoInt,InsertItem_IcoListboxEx,( IcoListboxEx_P control, AutoInt index, AutoInt addnum, AutoInt Data, ImageEx_P Ico, StrEx_P Title, int FontColor));\
\
/*--------删除项目_图标列表框Ex--------*/\
CMDINFO(void,DeleteItem_IcoListboxEx,( IcoListboxEx_P control, AutoInt delindex, AutoInt deletenum));\
\
/*--------取项目数量_图标列表框Ex--------*/\
CMDINFO(AutoInt,GetItemCount_IcoListboxEx,( IcoListboxEx_P control));\
\
/*--------置附加值_图标列表框Ex--------*/\
CMDINFO(void,SetItemData_IcoListboxEx,( IcoListboxEx_P control, AutoInt index, AutoInt Data));\
\
/*--------取附加值_图标列表框Ex--------*/\
CMDINFO(AutoInt,GetItemData_IcoListboxEx,( IcoListboxEx_P control, AutoInt index));\
\
/*--------置图标_图标列表框Ex--------*/\
CMDINFO(void,SetItemIco_IcoListboxEx,( IcoListboxEx_P control, AutoInt index, ImageEx_P Ico));\
\
/*--------取图标_图标列表框Ex--------*/\
CMDINFO(ImageEx_P,GetItemIco_IcoListboxEx,( IcoListboxEx_P control, AutoInt index));\
\
/*--------置文本_图标列表框Ex--------*/\
CMDINFO(void,SetItemTitle_IcoListboxEx,( IcoListboxEx_P control, AutoInt index, StrEx_P Title));\
\
/*--------取文本_图标列表框Ex--------*/\
CMDINFO(StrEx_P,GetItemTitle_IcoListboxEx,( IcoListboxEx_P control, AutoInt index));\
\
/*--------置字体色_图标列表框Ex--------*/\
CMDINFO(void,SetItemFontColor_IcoListboxEx,( IcoListboxEx_P control, AutoInt index, int FontColor));\
\
/*--------取字体色_图标列表框Ex--------*/\
CMDINFO(int,GetItemFontColor_IcoListboxEx,( IcoListboxEx_P control, AutoInt index));\
\
/*--------置字体_图标列表框Ex--------*/\
CMDINFO(void,SetItemFont_IcoListboxEx,( IcoListboxEx_P control, AutoInt index, BinEx_P Font));\
\
/*--------取字体_图标列表框Ex--------*/\
CMDINFO(BinEx_P,GetItemFont_IcoListboxEx,( IcoListboxEx_P control, AutoInt index));\
\
/*--------置皮肤_图标列表框Ex--------*/\
CMDINFO(void,SetItemSkin_IcoListboxEx,( IcoListboxEx_P control, AutoInt index, BinEx_P Skin));\
\
/*--------取皮肤_图标列表框Ex--------*/\
CMDINFO(BinEx_P,GetItemSkin_IcoListboxEx,( IcoListboxEx_P control, AutoInt index));\
\
/*--------置选中_图标列表框Ex--------*/\
CMDINFO(void,SetItemSelected_IcoListboxEx,( IcoListboxEx_P control, AutoInt index, BOOL Selected));\
\
/*--------取选中_图标列表框Ex--------*/\
CMDINFO(BOOL,GetItemSelected_IcoListboxEx,( IcoListboxEx_P control, AutoInt index));\
\
/*--------置布局配置_图标列表框Ex--------*/\
CMDINFO(void,SetItemLayoutConfig_IcoListboxEx,( IcoListboxEx_P control, AutoInt index, int ConfigId,  int Reserve1, int Reserve2, int Reserve3,  int Width, int Height, int Align, int IconWidth, int IconHeight));\
\
/*--------取布局配置_图标列表框Ex--------*/\
CMDINFO(int,GetItemLayoutConfig_IcoListboxEx,( IcoListboxEx_P control, AutoInt index, int ConfigId));\
\
/*--------置滚动回调_图标列表框Ex--------*/\
CMDINFO(void,SetScrollCallBack_IcoListboxEx,( IcoListboxEx_P control, ExuiCallback Callback));\
\
/*--------取滚动回调_图标列表框Ex--------*/\
CMDINFO(ExuiCallback,GetScrollCallBack_IcoListboxEx,( IcoListboxEx_P control));\
\
/*--------置虚表回调_图标列表框Ex--------*/\
CMDINFO(void,SetVirtualTableCallBack_IcoListboxEx,( IcoListboxEx_P control, ExuiCallback Callback));\
\
/*--------取虚表回调_图标列表框Ex--------*/\
CMDINFO(ExuiCallback,GetVirtualTableCallBack_IcoListboxEx,( IcoListboxEx_P control));\
\
/*--------保证显示_图标列表框Ex--------*/\
CMDINFO(void,GuaranteeVisible_IcoListboxEx,( IcoListboxEx_P control, AutoInt index, int mode));\
\
/*--------创建组件_树形列表框EX--------*/\
CMDINFO(TreeListEx_P,CreateControl_TreeListEx,(Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt* InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, AutoInt CurrentItem, BOOL AutoSize, int FoldBtnWidth, int FoldBtnHeight, int IconWidth, int IconHeight, BinEx_P Font, int ChildMigration, int BackMigration, int ContentMigration,int SelectMode,  int LineMode, int ScrollBarMode, BinEx_P ItemData, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_树形框Ex--------*/\
CMDINFO(void,SetAttribute_TreeListEx,( TreeListEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_树形框Ex--------*/\
CMDINFO(AutoInt,GetAttribute_TreeListEx,( TreeListEx_P control, AutoInt index));\
\
/*--------插入_树形框Ex--------*/\
CMDINFO(AutoInt,InsertItem_TreeListEx,( TreeListEx_P control, AutoInt index, AutoInt addnum, AutoInt Data, ImageEx_P Ico, StrEx_P Title, int FontColor, int Width, int Size, int ItemType, BOOL SetFold, int InsMode));\
\
/*--------删除_树形框Ex--------*/\
CMDINFO(void,DeleteItem_TreeListEx,( TreeListEx_P control, AutoInt delindex, AutoInt deletenum));\
\
/*--------取数量_树形框Ex--------*/\
CMDINFO(AutoInt,GetItemCount_TreeListEx,( TreeListEx_P control));\
\
/*--------置附加值_树形框Ex--------*/\
CMDINFO(void,SetItemData_TreeListEx,( TreeListEx_P control, AutoInt index, AutoInt Data));\
\
/*--------取附加值_树形框Ex--------*/\
CMDINFO(AutoInt,GetItemData_TreeListEx,( TreeListEx_P control, AutoInt index));\
\
/*--------置图标_树形框Ex--------*/\
CMDINFO(void,SetItemIco_TreeListEx,( TreeListEx_P control, AutoInt index, ImageEx_P Ico));\
\
/*--------取图标_树形框Ex--------*/\
CMDINFO(ImageEx_P,GetItemIco_TreeListEx,( TreeListEx_P control, AutoInt index));\
\
/*--------置文本_树形框Ex--------*/\
CMDINFO(void,SetItemTitle_TreeListEx,( TreeListEx_P control, AutoInt index, StrEx_P Title));\
\
/*--------取文本_树形框Ex--------*/\
CMDINFO(StrEx_P,GetItemTitle_TreeListEx,( TreeListEx_P control, AutoInt index));\
\
/*--------置字体色_树形框Ex--------*/\
CMDINFO(void,SetItemFontColor_TreeListEx,( TreeListEx_P control, AutoInt index, int FontColor));\
\
/*--------取字体色_树形框Ex--------*/\
CMDINFO(int,GetItemFontColor_TreeListEx,( TreeListEx_P control, AutoInt index));\
\
/*--------置项目宽度_树形框Ex--------*/\
CMDINFO(void,SetItemWidth_TreeListEx,( TreeListEx_P control, AutoInt index, int Width));\
\
/*--------取项目宽度_树形框Ex--------*/\
CMDINFO(int,GetItemWidth_TreeListEx,( TreeListEx_P control, AutoInt index));\
\
/*--------置项目高度_树形框Ex--------*/\
CMDINFO(void,SetItemSize_TreeListEx,( TreeListEx_P control, AutoInt index, int Size));\
\
/*--------取项目高度_树形框Ex--------*/\
CMDINFO(int,GetItemSize_TreeListEx,( TreeListEx_P control, AutoInt index));\
\
/*--------置项目风格_树形框Ex--------*/\
CMDINFO(void,SetItemType_TreeListEx,( TreeListEx_P control, AutoInt index, int style));\
\
/*--------取项目风格_树形框Ex--------*/\
CMDINFO(int,GetItemType_TreeListEx,( TreeListEx_P control, AutoInt index));\
\
/*--------置字体_树形框Ex--------*/\
CMDINFO(void,SetItemFont_TreeListEx,( TreeListEx_P control, AutoInt index, BinEx_P Font));\
\
/*--------取字体_树形框Ex--------*/\
CMDINFO(BinEx_P,GetItemFont_TreeListEx,( TreeListEx_P control, AutoInt index));\
\
/*--------置皮肤_树形框Ex--------*/\
CMDINFO(void,SetItemSkin_TreeListEx,( TreeListEx_P control, AutoInt index, BinEx_P Skin));\
\
/*--------取皮肤_树形框Ex--------*/\
CMDINFO(BinEx_P,GetItemSkin_TreeListEx,( TreeListEx_P control, AutoInt index));\
\
/*--------置选中_树形框Ex--------*/\
CMDINFO(void,SetItemSelected_TreeListEx,( TreeListEx_P control, AutoInt index, BOOL Selected));\
\
/*--------取选中_树形框Ex--------*/\
CMDINFO(BOOL,GetItemSelected_TreeListEx,( TreeListEx_P control, AutoInt index));\
\
/*--------置折叠状态_树形框Ex--------*/\
CMDINFO(void,SetItemFold_TreeListEx,( TreeListEx_P control, AutoInt index, BOOL Fold));\
\
/*--------取折叠状态_树形框Ex--------*/\
CMDINFO(BOOL,GetItemFold_TreeListEx,( TreeListEx_P control, AutoInt index));\
\
/*--------取缩进层次_树形框Ex--------*/\
CMDINFO(int,GetItemLevel_TreeListEx,( TreeListEx_P control, AutoInt index));\
\
/*--------取子项目数_树形框Ex--------*/\
CMDINFO(AutoInt,GetSubItemCount_TreeListEx,( TreeListEx_P control, AutoInt index));\
\
/*--------取下一级子项目数_树形框Ex--------*/\
CMDINFO(AutoInt,GetNextSubItemCount_TreeListEx,( TreeListEx_P control, AutoInt index));\
\
/*--------取祖宗项目_树形框Ex--------*/\
CMDINFO(AutoInt,GetAncestorItem_TreeListEx,( TreeListEx_P control, AutoInt index));\
\
/*--------取父项目_树形框Ex--------*/\
CMDINFO(AutoInt,GetFatherItem_TreeListEx,( TreeListEx_P control, AutoInt index));\
\
/*--------取上一同层项目_树形框Ex--------*/\
CMDINFO(AutoInt,GetLastItem_TreeListEx,( TreeListEx_P control, AutoInt index));\
\
/*--------取下一同层项目_树形框Ex--------*/\
CMDINFO(AutoInt,GetNextItem_TreeListEx,( TreeListEx_P control, AutoInt index));\
\
/*--------置滚动回调_树形框Ex--------*/\
CMDINFO(void,SetScrollCallBack_TreeListEx,( TreeListEx_P control, ExuiCallback Callback));\
\
/*--------取滚动回调_树形框Ex--------*/\
CMDINFO(ExuiCallback,GetScrollCallBack_TreeListEx,( TreeListEx_P control));\
\
/*--------置虚表回调_树形框Ex--------*/\
CMDINFO(void,SetVirtualTableCallBack_TreeListEx,( TreeListEx_P control, ExuiCallback Callback));\
\
/*--------取虚表回调_树形框Ex--------*/\
CMDINFO(ExuiCallback,GetVirtualTableCallBack_TreeListEx,( TreeListEx_P control));\
\
/*--------保证显示_树形框Ex--------*/\
CMDINFO(void,GuaranteeVisible_TreeListEx,( TreeListEx_P control, AutoInt index, int mode));\
\
/*--------创建组件_浏览框EX--------*/\
CMDINFO(WebBrowserEx_P,CreateControl_WebBrowserEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BOOL BackgroundTransparent, int MediaVolume, int ZoomFactor, BOOL NavigationToNewWind, BOOL CookieEnabled, BOOL NpapiEnabled, BOOL MenuEnabled, int DragEnable, StrEx_P StartPage, StrEx_P UserAgent, int Version, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_浏览框Ex--------*/\
CMDINFO(void,SetAttribute_WebBrowserEx,( WebBrowserEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_浏览框Ex--------*/\
CMDINFO(AutoInt,GetAttribute_WebBrowserEx,( WebBrowserEx_P control, AutoInt index));\
\
/*--------取内核句柄_浏览框Ex--------*/\
CMDINFO(AutoInt,GetWebView_WebBrowserEx,( WebBrowserEx_P control));\
\
/*--------载入_浏览框Ex--------*/\
CMDINFO(void,Load_WebBrowserEx,( WebBrowserEx_P control, StrEx_P url, StrEx_P baseUrl, int Loadmode));\
\
/*--------后退_浏览框Ex--------*/\
CMDINFO(BOOL,GoBack_WebBrowserEx,( WebBrowserEx_P control));\
\
/*--------前进_浏览框Ex--------*/\
CMDINFO(BOOL,GoForward_WebBrowserEx,( WebBrowserEx_P control));\
\
/*--------可否后退_浏览框Ex--------*/\
CMDINFO(BOOL,GoCanBack_WebBrowserEx,( WebBrowserEx_P control));\
\
/*--------可否前进_浏览框Ex--------*/\
CMDINFO(BOOL,GoCanForward_WebBrowserEx,( WebBrowserEx_P control));\
\
/*--------重载_浏览框Ex--------*/\
CMDINFO(void,ReLoad_WebBrowserEx,( WebBrowserEx_P control));\
\
/*--------停止_浏览框Ex--------*/\
CMDINFO(void,StopLoading_WebBrowserEx,( WebBrowserEx_P control));\
\
/*--------取地址_浏览框Ex--------*/\
CMDINFO(StrEx_P,GetUrl_WebBrowserEx,( WebBrowserEx_P control,  StrEx_P Param1));\
\
/*--------取标题_浏览框Ex--------*/\
CMDINFO(StrEx_P,GetTitle_WebBrowserEx,( WebBrowserEx_P control, StrEx_P Param1));\
\
/*--------取源码_浏览框Ex--------*/\
CMDINFO(StrEx_P,GetSource_WebBrowserEx,( WebBrowserEx_P control, StrEx_P Param1));\
\
/*--------取文本_浏览框Ex--------*/\
CMDINFO(StrEx_P,GetText_WebBrowserEx,(WebBrowserEx_P control, StrEx_P DataType, void* DataPtr, AutoInt DataLong, AutoInt DataCode, AutoInt Param1));\
\
/*--------取数据_浏览框Ex--------*/\
CMDINFO(StrEx_P,GetData_WebBrowserEx,(WebBrowserEx_P control, StrEx_P DataType, void* DataPtr, AutoInt DataLong, AutoInt DataCode, AutoInt Param1));\
\
/*--------执行命令_浏览框Ex--------*/\
CMDINFO(AutoInt,RunCmd_WebBrowserEx,( WebBrowserEx_P control, AutoInt Cmd, AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4));\
\
/*--------取cookie_浏览框Ex--------*/\
CMDINFO(StrEx_P,GetCookie_WebBrowserEx,( WebBrowserEx_P control, StrEx_P Param1));\
\
/*--------置cookie_浏览框Ex--------*/\
CMDINFO(void,SetCookie_WebBrowserEx,( WebBrowserEx_P control, StrEx_P url, StrEx_P Cookie));\
\
/*--------执行js_浏览框Ex--------*/\
CMDINFO(StrEx_P,RunJsCmd_WebBrowserEx,( WebBrowserEx_P control, StrEx_P Cmd, StrEx_P  Param1, StrEx_P  Param2, StrEx_P  Param3));\
\
/*--------绑定js_浏览框Ex--------*/\
CMDINFO(void,BindJsCmd_WebBrowserEx,( WebBrowserEx_P control, StrEx_P Cmd,void* Param1, AutoInt Param2, AutoInt Param3));\
\
/*--------否正在加载_浏览框Ex--------*/\
CMDINFO(BOOL,IsLoading_WebBrowserEx,( WebBrowserEx_P control));\
\
/*--------是否载入失败_浏览框Ex--------*/\
CMDINFO(BOOL,IsLoadingSucceeded_WebBrowserEx,( WebBrowserEx_P control));\
\
/*--------是否载入完毕_浏览框Ex--------*/\
CMDINFO(BOOL,IsLoadingCompleted_WebBrowserEx,( WebBrowserEx_P control));\
\
/*--------是否加载完成_浏览框Ex--------*/\
CMDINFO(BOOL,IsDocumentReady_WebBrowserEx,( WebBrowserEx_P control));\
\
/*--------启用开发者工具_浏览框Ex--------*/\
CMDINFO(void,ShowDevtools_WebBrowserEx,( WebBrowserEx_P control, StrEx_P path));\
\
/*--------创建组件_日历框EX--------*/\
CMDINFO(CalendarBoxEx_P,CreateControl_CalendarBoxEx,(Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt* InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, StrEx_P MiniDate, StrEx_P MaxDate, StrEx_P SelectionDate, BinEx_P TitleFont, BinEx_P WeekFont, BinEx_P DayFont, BinEx_P TimeFont, int TitleClour, int WeekClour, int WeekendWeekClour, int DayClour, int WeekendClour, int OtherMonthClour, int TimeFontClour, BOOL OnlyThisMonth, int TimeMode, BOOL AutoSelect, int TitleHeight, int WeekHeight, int TimeRegulatorHeight, int PartnerHeight, int Language, BinEx_P ItemData, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_日历框Ex--------*/\
CMDINFO(void,SetAttribute_CalendarBoxEx,( CalendarBoxEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_日历框Ex--------*/\
CMDINFO(AutoInt,GetAttribute_CalendarBoxEx,( CalendarBoxEx_P control, AutoInt index));\
\
/*--------置选中_日历框Ex--------*/\
CMDINFO(void,SetItemSelected_CalendarBoxEx,( CalendarBoxEx_P control, AutoInt index, BOOL Selected));\
\
/*--------取选中_日历框Ex--------*/\
\
CMDINFO(BOOL,GetItemSelected_CalendarBoxEx,( CalendarBoxEx_P control, AutoInt index));\
\
/*--------置皮肤_日历框Ex--------*/\
CMDINFO(void,SetItemSkin_CalendarBoxEx,( CalendarBoxEx_P control, AutoInt index, BinEx_P Skin));\
\
/*--------取皮肤_日历框Ex--------*/\
CMDINFO(BinEx_P,GetItemSkin_CalendarBoxEx,( CalendarBoxEx_P control, AutoInt index));\
\
/*--------创建组件_选色板EX--------*/\
CMDINFO(ColorPickEx_P,CreateControl_ColorPickEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, BinEx_P Font, int FontClour, int NowClour, BOOL DragTrace, int ClourMode, BinEx_P QuickClours, int Style, int RegulatorHeight, int QuickClourSize, int PartnerHeight, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_选色板EX--------*/\
CMDINFO(void,SetAttribute_ColorPickEx,( ColorPickEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_选色板EX--------*/\
CMDINFO(AutoInt,GetAttribute_ColorPickEx,( ColorPickEx_P control, AutoInt index));\
\
/*--------创建组件_富文本框EX--------*/\
CMDINFO(RichEditEx_P,CreateControl_RichEditEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, StrEx_P Content, int Type, BOOL BackTransparent, int BackClour, int CursorColor, BinEx_P DefaultCharFormat, BinEx_P DefaultParaFormat, int InputMode, int MaxInput, StrEx_P PasswordSubstitution, BOOL Multiline, BOOL Wrapped, int ScrollBarMode, int MenuTableWidth, int MenuTableHeight, BinEx_P MenuFont, int MenuFontClour, int MenuDisabledFontClour, int MenuTransparency, int MenuLanguage, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_富文本框EX--------*/\
CMDINFO(void,SetAttribute_RichEditEx,( RichEditEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_富文本框EX--------*/\
CMDINFO(AutoInt,GetAttribute_RichEditEx,( RichEditEx_P control, AutoInt index));\
\
/*--------插入文本_编辑框Ex--------*/\
CMDINFO(void,InsertText_RichEditEx,( RichEditEx_P control, AutoInt index, StrEx_P text,int UseRich, StrEx_P FontName, int Size, int style, int TextColor, int Alignment, int LineHeight));\
\
/*--------删除文本_编辑框Ex--------*/\
CMDINFO(void,DeleteText_RichEditEx,( RichEditEx_P control, AutoInt index, AutoInt dellen));\
\
/*--------置光标位置_编辑框Ex--------*/\
CMDINFO(void,SetInsertCursor_RichEditEx,( RichEditEx_P control, AutoInt index));\
\
/*--------取光标位置_编辑框Ex--------*/\
CMDINFO(AutoInt,GetInsertCursor_RichEditEx,( RichEditEx_P control));\
\
/*--------置选择文本长度_编辑框Ex--------*/\
CMDINFO(void,SetSelectLeng_RichEditEx,( RichEditEx_P control, AutoInt SelectLeng));\
\
/*--------取选择文本长度_编辑框Ex--------*/\
CMDINFO(AutoInt,GetSelectLeng_RichEditEx,( RichEditEx_P control));\
\
/*--------保证显示字符_编辑框Ex--------*/\
CMDINFO(void,GuaranteeVisibleText_RichEditEx,( RichEditEx_P control, AutoInt index, int mode));\
\
/*--------置选择区字符格式--------*/\
CMDINFO(BOOL,SetSelCharFormat_RichEditEx,( RichEditEx_P control, BinEx_P CharFormat));\
\
/*--------取选择区字符格式--------*/\
CMDINFO(BinEx_P,GetSelCharFormat_RichEditEx,( RichEditEx_P control));\
\
/*--------置选择区段落方式--------*/\
CMDINFO(BOOL,SetSelParaFormat_RichEditEx,( RichEditEx_P control, BinEx_P CharFormat));\
\
/*--------取选择区段落方式--------*/\
CMDINFO(BinEx_P,GetSelParaFormat_RichEditEx,( RichEditEx_P control));\
\
/*--------执行接口命令--------*/\
CMDINFO(AutoInt,RunServicesCmd_RichEditEx,( RichEditEx_P control, UINT msg, WPARAM wparam, LPARAM lparam, LRESULT * plresult));\
\
/*--------创建组件_扩展组件EX--------*/\
CMDINFO(ExtendEx_P,CreateControl_ExtendEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P AttrEx, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_扩展组件Ex--------*/\
CMDINFO(void,SetAttribute_ExtendEx,( ExtendEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_扩展组件Ex--------*/\
CMDINFO(AutoInt,GetAttribute_ExtendEx,( ExtendEx_P control, AutoInt index));\
\
/*--------启用开发者工具_扩展组件Ex--------*/\
CMDINFO(int,ShowDevtools_ExtendEx,( ExtendEx_P control, StrEx_P toolspath));\
\
/*--------执行扩展命令_扩展组件Ex--------*/\
CMDINFO(StrEx_P,RunExtendCmd_ExtendEx,( ExtendEx_P control, StrEx_P Cmd, StrEx_P  Param1, StrEx_P  Param2, StrEx_P  Param3));\
\
/*--------创建组件_动画按钮EX--------*/\
CMDINFO(AnimationbuttonEx_P,CreateControl_AnimationbuttonEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, BOOL Selected, BinEx_P Icon, int IconWidth, int IconHeight, int Align, StrEx_P Text, BinEx_P Font, int FontClour, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_动画按钮EX--------*/\
CMDINFO(void,SetAttribute_AnimationbuttonEx,( AnimationbuttonEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_动画按钮EX--------*/\
CMDINFO(AutoInt,GetAttribute_AnimationbuttonEx,( AnimationbuttonEx_P control, AutoInt index));\
\
/*--------创建组件_高级表格EX--------*/\
CMDINFO(AdvancedFormEx_P,CreateControl_AdvancedFormEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt* InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, AutoInt CurrentItem, AutoInt CurrentColumn, AutoInt HeaderRowNum, AutoInt HeaderColumnNum, AutoInt BottomFixedRow, AutoInt RightFixedColumn, int AdjustmentMode, int SelectMode, BOOL EntireLine, int EventMode, int EditMode,BOOL TreeType, int LineMode, int ScrollBarMode, BinEx_P ItemData, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_高级表格Ex--------*/\
CMDINFO(void,SetAttribute_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_高级表格Ex--------*/\
CMDINFO(AutoInt,GetAttribute_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index));\
\
/*--------插入列_高级表格Ex--------*/\
CMDINFO(AutoInt,InsertColumn_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt cloid, AutoInt addnum, int wight, int min, int max,	BinEx_P HeaderFont, int HeaderIcoW, int HeaderIcoH, int HeaderAlign,	BinEx_P ItemFont, int ItemIcoW, int ItemIcoH, int ItemAlign,	BinEx_P FixedItemFont, int FixedItemIcoW, int FixedItemIcoH, int FixedItemAlign,	int HeaderTreeType, int TreeType,  int BottomFixedTreeType, 	int ChildMigration,int ContentMigration , int	LineMode , BinEx_P Skin));\
\
/*--------删除列_高级表格Ex--------*/\
CMDINFO(void,DeleteColumn_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt delindex, AutoInt deletenum));\
\
/*--------取列数量_高级表格Ex--------*/\
CMDINFO(int,GetColumnCount_AdvancedFormEx,( AdvancedFormEx_P control));\
\
/*--------置列属性_高级表格Ex--------*/\
CMDINFO(void,SetColumnAttribute_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index, AutoInt Endindex, AutoInt AttributeId, int wight, int min, int max,	BinEx_P HeaderFont, int HeaderIcoW, int HeaderIcoH, int HeaderAlign,	BinEx_P ItemFont, int ItemIcoW, int ItemIcoH, int ItemAlign,	BinEx_P FixedItemFont, int FixedItemIcoW, int FixedItemIcoH, int FixedItemAlign,	int HeaderTreeType, int TreeType,  int BottomFixedTreeType, 	int ChildMigration, int ContentMigration , int	LineMode ,BinEx_P Skin));\
\
/*--------取列属性_高级表格Ex--------*/\
CMDINFO(int,GetColumnAttribute_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index, AutoInt AttributeId));\
\
/*--------插入项目_高级表格Ex--------*/\
CMDINFO(AutoInt,InsertItem_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index, AutoInt addnum, AutoInt Data, int Height, int MinHeight, int MaxHeight, int Style, BOOL SetFold, int InsMode));\
\
/*--------删除项目_高级表格Ex--------*/\
CMDINFO(void,DeleteItem_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt delindex, AutoInt deletenum));\
\
/*--------取数量_高级表格Ex--------*/\
CMDINFO(AutoInt,GetItemCount_AdvancedFormEx,( AdvancedFormEx_P control));\
\
/*--------置附加值_高级表格Ex--------*/\
CMDINFO(void,SetItemData_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt StarRow, AutoInt EndRow, AutoInt Data));\
\
/*--------取附加值_高级表格Ex--------*/\
CMDINFO(AutoInt,GetItemData_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index));\
\
/*--------置图标_高级表格Ex--------*/\
CMDINFO(void,SetItemIco_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn, ImageEx_P Ico));\
\
/*--------取图标_高级表格Ex--------*/\
CMDINFO(ImageEx_P,GetItemIco_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index, AutoInt cloid));\
\
/*--------置文本_高级表格Ex--------*/\
CMDINFO(void,SetItemTitle_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn, StrEx_P Title));\
\
/*--------取文本_高级表格Ex--------*/\
CMDINFO(StrEx_P,GetItemTitle_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index, AutoInt cloid));\
\
/*--------置字体色_高级表格Ex--------*/\
CMDINFO(void,SetItemFontColor_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn, int FontColor));\
\
/*--------取字体色_高级表格Ex--------*/\
CMDINFO(int,GetItemFontColor_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index, AutoInt cloid));\
\
/*--------置项目高度_高级表格Ex--------*/\
CMDINFO(void,SetItemHeight_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt StarRow, AutoInt EndRow, int Height));\
\
/*--------取项目高度_高级表格Ex--------*/\
CMDINFO(int,GetItemHeight_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index));\
\
/*--------置项目最大高度_高级表格Ex--------*/\
CMDINFO(void,SetItemMaxHeight_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt StarRow, AutoInt EndRow, int Height));\
\
/*--------取项目最大高度_高级表格Ex--------*/\
CMDINFO(int,GetItemMaxHeight_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index));\
\
/*--------置项目最小高度_高级表格Ex--------*/\
CMDINFO(void,SetItemMinHeight_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt StarRow, AutoInt EndRow, int Height));\
\
/*--------取项目最小高度_高级表格Ex--------*/\
CMDINFO(int,GetItemMinHeight_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index));\
\
/*--------合并单元格_高级表格Ex--------*/\
CMDINFO(void,MergeCell_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn));\
\
/*--------分解单元格_高级表格Ex--------*/\
CMDINFO(void,BreakCell_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt StarRow, AutoInt StarColumn));\
\
/*--------取单元格合并信息_高级表格Ex--------*/\
CMDINFO(AutoInt,GetMergeInfo_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt StarRow, AutoInt StarColumn, int Type));\
\
/*--------置字体_高级表格Ex--------*/\
CMDINFO(void,SetItemFont_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn, BinEx_P Font));\
\
/*--------取字体_高级表格Ex--------*/\
CMDINFO(BinEx_P,GetItemFont_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index, AutoInt cloid));\
\
/*--------置皮肤_高级表格Ex--------*/\
CMDINFO(void,SetItemSkin_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn, BinEx_P Skin));\
\
/*--------取皮肤_高级表格Ex--------*/\
CMDINFO(BinEx_P,GetItemSkin_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index, AutoInt cloid));\
\
/*--------置对齐方式_高级表格Ex--------*/\
CMDINFO(void, SetItemAlign_AdvancedFormEx, (AdvancedFormEx_P control, AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn, int  Align));\
\
/*--------取对齐方式_高级表格Ex--------*/\
\
CMDINFO(int, GetItemAlign_AdvancedFormEx, (AdvancedFormEx_P control, AutoInt index, AutoInt cloid));\
\
/*--------置选中状态_高级表格Ex--------*/\
CMDINFO(void,SetItemSelected_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn, BOOL Select));\
\
/*--------取选中状态_高级表格Ex--------*/\
CMDINFO(BOOL,GetItemSelected_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index, AutoInt cloid));\
\
/*--------置项目风格_高级表格Ex--------*/\
CMDINFO(void,SetItemType_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt StarRow, AutoInt EndRow, int style));\
\
/*--------取项目风格_高级表格Ex--------*/\
CMDINFO(int,GetItemType_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index));\
\
/*--------置折叠状态_高级表格Ex--------*/\
CMDINFO(void,SetItemFold_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt StarRow, AutoInt EndRow, BOOL Fold));\
\
/*--------取折叠状态_高级表格Ex--------*/\
CMDINFO(BOOL,GetItemFold_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index));\
\
/*--------取缩进层次_高级表格Ex--------*/\
CMDINFO(int,GetItemLevel_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index));\
\
/*--------取子项目数_高级表格Ex--------*/\
CMDINFO(int,GetSubItemCount_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index));\
\
/*--------取下一级子项目数_高级表格Ex--------*/\
CMDINFO(int,GetNextSubItemCount_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index));\
\
/*--------取祖宗项目_高级表格Ex--------*/\
CMDINFO(int,GetAncestorItem_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index));\
\
/*--------取父项目_高级表格Ex--------*/\
CMDINFO(int,GetFatherItem_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index));\
\
/*--------取上一同层项目_高级表格Ex--------*/\
CMDINFO(int,GetLastItem_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index));\
\
/*--------取下一同层项目_高级表格Ex--------*/\
CMDINFO(int,GetNextItem_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index));\
\
/*--------置项目编辑配置_高级表格Ex--------*/\
CMDINFO(void,SetItemEditConfig_AdvancedFormEx,(AdvancedFormEx_P control, int AttributeId,  int InputMode, int MaxInput, StrEx_P PasswordSubstitution, int Align, int Multiline, int Wrapped, BinEx_P Skin, BinEx_P Cursor, BinEx_P Font, int FontClour, int SelectedFontColor, int SelectedColor, int CursorColor, int ScrollBarMode, int MenuTableWidth, int MenuTableHeight, BinEx_P MenuFont, int MenuFontClour, int MenuDisabledFontClour, int MenuTransparency, int MenuLanguage));\
\
/*--------取项目编辑配置_高级表格Ex--------*/\
CMDINFO(AutoInt,GetItemEditConfig_AdvancedFormEx,(AdvancedFormEx_P control, int AttributeId));\
\
/*--------进入编辑_高级表格Ex--------*/\
CMDINFO(void,EnterEdit_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index, AutoInt cloid, AutoInt mode));\
\
/*--------退出编辑_高级表格Ex--------*/\
CMDINFO(void,ExitEdit_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index, AutoInt cloid, AutoInt mode));\
\
/*--------置滚动回调_高级表格Ex--------*/\
CMDINFO(void,SetScrollCallBack_AdvancedFormEx,( AdvancedFormEx_P control, ExuiCallback Callback));\
\
/*--------取滚动回调_高级表格Ex--------*/\
CMDINFO(ExuiCallback,GetScrollCallBack_AdvancedFormEx,( AdvancedFormEx_P control));\
\
/*--------置虚表回调_高级表格Ex--------*/\
CMDINFO(void,SetVirtualTableCallBack_AdvancedFormEx,( AdvancedFormEx_P control, ExuiCallback Callback));\
\
/*--------取虚表回调_高级表格Ex--------*/\
CMDINFO(ExuiCallback,GetVirtualTableCallBack_AdvancedFormEx,( AdvancedFormEx_P control));\
\
/*--------保证显示_高级表格Ex--------*/\
CMDINFO(void,GuaranteeVisible_AdvancedFormEx,( AdvancedFormEx_P control, AutoInt index, AutoInt cid, int mode));\
\
/*--------创建组件_滑动按钮EX--------*/\
CMDINFO(SlideButtonEx_P,CreateControl_SlideButtonEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, BOOL Selected, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_滑动按钮Ex--------*/\
CMDINFO(void,SetAttribute_SlideButtonEx,( SlideButtonEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_滑动按钮Ex--------*/\
CMDINFO(AutoInt,GetAttribute_SlideButtonEx,( SlideButtonEx_P control, AutoInt index));\
\
/*--------创建组件_饼形图Ex--------*/\
CMDINFO(PieChartEx_P,CreateControl_PieChartEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, int LeftReserve, int TopReserve, int RightReserve, int BottomReserve, AutoInt hotGraph, int GraphStar, int GraphWidth, BOOL Zoom, BinEx_P ItemData, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_饼形图Ex--------*/\
CMDINFO(void,SetAttribute_PieChartEx,( PieChartEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_饼形图Ex--------*/\
CMDINFO(AutoInt,GetAttribute_PieChartEx,( PieChartEx_P control, AutoInt index));\
\
/*--------置图例数量_饼形图Ex--------*/\
CMDINFO(void,SetGraphCount_PieChartEx,( PieChartEx_P control, AutoInt Count));\
\
/*--------取图例数量_饼形图Ex--------*/\
CMDINFO(AutoInt,GetGraphCount_PieChartEx,( PieChartEx_P control));\
\
/*--------置图例属性_饼形图Ex--------*/\
CMDINFO(void,SetGraphAttr_PieChartEx,( PieChartEx_P control, AutoInt index, AutoInt AttrId, int color));\
\
/*--------取图例属性_饼形图Ex--------*/\
CMDINFO(AutoInt,GetGraphAttr_PieChartEx,( PieChartEx_P control, AutoInt index, AutoInt AttrId));\
\
/*--------置图例值_饼形图Ex--------*/\
CMDINFO(void,SetGraphValue_PieChartEx,( PieChartEx_P control, AutoInt index, int Value));\
\
/*--------取图例值_饼形图Ex--------*/\
CMDINFO(int,GetGraphValue_PieChartEx,( PieChartEx_P control, AutoInt index));\
\
/*--------创建组件_柱状图Ex--------*/\
CMDINFO(BarChartEx_P,CreateControl_BarChartEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, int ChartStyle, StrEx_P TemplateH, int ColorHA, int GridStyleH, int GridColorH, BinEx_P FontH, int FontColorH, StrEx_P TemplateV, int MiniV, int MaxV, int ColorVA, int GridStyleV, int GridColorV, BinEx_P FontV, int FontColorV, int LeftReserve, int TopReserve, int RightReserve, int BottomReserve, AutoInt HotColumn, int GraphWidth, int GraphSpace, BinEx_P GraphData, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_柱状图Ex--------*/\
CMDINFO(void,SetAttribute_BarChartEx,( BarChartEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_柱状图Ex--------*/\
CMDINFO(AutoInt,GetAttribute_BarChartEx,( BarChartEx_P control, AutoInt index));\
\
/*--------置图例数量_柱状图Ex--------*/\
CMDINFO(void,SetGraphCount_BarChartEx,( BarChartEx_P control, AutoInt Count));\
\
/*--------取图例数量_柱状图Ex--------*/\
CMDINFO(AutoInt,GetGraphCount_BarChartEx,( BarChartEx_P control));\
\
/*--------置图例属性_柱状图Ex--------*/\
CMDINFO(void,SetGraphAttr_BarChartEx,( BarChartEx_P control, AutoInt index, AutoInt AttrId, int Colour, int Rgn));\
\
/*--------取图例属性_柱状图Ex--------*/\
CMDINFO(AutoInt,GetGraphAttr_BarChartEx,( BarChartEx_P control, AutoInt index, AutoInt AttrId));\
\
/*--------置图例值_柱状图Ex--------*/\
CMDINFO(void,SetGraphValue_BarChartEx,( BarChartEx_P control, AutoInt index, AutoInt Cid, int Value));\
\
/*--------取图例值_柱状图Ex--------*/\
CMDINFO(int,GetGraphValue_BarChartEx,( BarChartEx_P control, AutoInt index, AutoInt Cid));\
\
/*--------创建组件_曲线图Ex--------*/\
CMDINFO(CurveChartEx_P,CreateControl_CurveChartEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, int ChartStyle, StrEx_P TemplateH, int ColorHA, int GridStyleH, int GridColorH, BinEx_P FontH, int FontColorH, StrEx_P TemplateV, int MiniV, int MaxV, int ColorVA, int GridStyleV, int GridColorV, BinEx_P FontV, int FontColorV, int LeftReserve, int TopReserve, int RightReserve, int BottomReserve, AutoInt HotColumn, BinEx_P GraphData, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_曲线图Ex--------*/\
CMDINFO(void,SetAttribute_CurveChartEx,( CurveChartEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_曲线图Ex--------*/\
CMDINFO(AutoInt,GetAttribute_CurveChartEx,( CurveChartEx_P control, AutoInt index));\
\
/*--------置图例数量_曲线图Ex--------*/\
CMDINFO(void,SetGraphCount_CurveChartEx,( CurveChartEx_P control, AutoInt Count));\
\
/*--------取图例数量_曲线图Ex--------*/\
CMDINFO(AutoInt,GetGraphCount_CurveChartEx,( CurveChartEx_P control));\
\
/*--------置图例属性_曲线图Ex--------*/\
CMDINFO(void,SetGraphAttr_CurveChartEx,( CurveChartEx_P control, AutoInt index, AutoInt AttrId, int Colour, int Tension, int width, int style, int PointSize, int HotPointSize));\
\
/*--------取图例属性_曲线图Ex--------*/\
CMDINFO(AutoInt,GetGraphAttr_CurveChartEx,( CurveChartEx_P control, AutoInt index, AutoInt AttrId));\
\
/*--------置图例值_曲线图Ex--------*/\
CMDINFO(void,SetGraphValue_CurveChartEx,( CurveChartEx_P control, AutoInt index, AutoInt Cid, int Value));\
\
/*--------取图例值_曲线图Ex--------*/\
CMDINFO(int,GetGraphValue_CurveChartEx,( CurveChartEx_P control, AutoInt index, AutoInt Cid));\
\
/*--------创建组件_烛线图Ex--------*/\
CMDINFO(CandleChartEx_P,CreateControl_CandleChartEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, int ChartStyle, StrEx_P TemplateH, int ColorHA, int GridStyleH, int GridColorH, BinEx_P FontH, int FontColorH, StrEx_P TemplateV, int MiniV, int MaxV, int ColorVA, int GridStyleV, int GridColorV, BinEx_P FontV, int FontColorV, int LeftReserve, int TopReserve, int RightReserve, int BottomReserve, AutoInt HotColumn, int GraphWidth, BOOL NegativeHollow, int NegativeColor, BOOL PositiveHollow, int PositiveColor, BinEx_P ItemData, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_烛线图Ex--------*/\
CMDINFO(void,SetAttribute_CandleChartEx,( CandleChartEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_烛线图Ex--------*/\
CMDINFO(AutoInt,GetAttribute_CandleChartEx,( CandleChartEx_P control, AutoInt index));\
\
/*--------置图例值_烛线图Ex--------*/\
CMDINFO(void,SetGraphValue_CandleChartEx,( CandleChartEx_P control, AutoInt index, AutoInt DataId, int openVal, int closeVal, int lowVal, int heighVal));\
\
/*--------取图例值_烛线图Ex--------*/\
CMDINFO(int,GetGraphValue_CandleChartEx,( CandleChartEx_P control, AutoInt index, AutoInt DataId));\
\
/*--------创建组件_画板EX--------*/\
CMDINFO(DrawPanelEx_P,CreateControl_DrawPanelEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, int BackColor, int LineColor, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_画板EX--------*/\
CMDINFO(void,SetAttribute_DrawPanelEx,( DrawPanelEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_画板EX--------*/\
CMDINFO(AutoInt,GetAttribute_DrawPanelEx,( DrawPanelEx_P control, AutoInt index));\
\
/*--------画图片_画板EX--------*/\
CMDINFO(void,DrawImage_DrawPanelEx,( DrawPanelEx_P control, ImageEx_P Image, float dstX, float dstY, float dstWidth, float dstHeight, float srcx, float srcy, float srcwidth, float srcheight, int Alpha));\
\
/*--------画文本_画板EX--------*/\
CMDINFO(void,DrawString_DrawPanelEx,( DrawPanelEx_P control, StrEx_P string, float dstX, float dstY, float dstWidth, float dstHeight, BinEx_P font, AutoInt StringFormat, int DrawMode, int DrawParame, int color, int auxcolor));\
\
/*--------画多边形_画板EX--------*/\
CMDINFO(void,DrawPolygon_DrawPanelEx,( DrawPanelEx_P control, float * Points, int count, int DrawMode, int DrawParame, int color, int auxcolor));\
\
/*--------画矩形_画板EX--------*/\
CMDINFO(void,DrawRect_DrawPanelEx,( DrawPanelEx_P control, float X, float Y, float Width, float Height, int DrawMode, int DrawParame, int color, int auxcolor));\
\
/*--------画椭圆_画板EX--------*/\
CMDINFO(void,DrawEllipse_DrawPanelEx,( DrawPanelEx_P control, float X, float Y, float Width, float Height, float Rgn, int DrawMode, int DrawParame, int color, int auxcolor));\
\
/*--------画饼_画板EX--------*/\
CMDINFO(void,DrawPie_DrawPanelEx,( DrawPanelEx_P control, float X, float Y, float Width, float Height, float starAngle, float sweepAngle, int DrawMode, int DrawParame, int color, int auxcolor));\
\
/*--------画曲线_画板EX--------*/\
CMDINFO(void,DrawCurve_DrawPanelEx,( DrawPanelEx_P control, float * Points, int count, float tension, int Mode, int DrawMode, int DrawParame, int color, int auxcolor));\
\
/*--------画弧线_画板EX--------*/\
CMDINFO(void,DrawArc_DrawPanelEx,( DrawPanelEx_P control, float X, float Y, float Width, float Height, float starAngle, float sweepAngle, int DrawMode, int DrawParame, int color, int auxcolor));\
\
/*--------画直线_画板EX--------*/\
CMDINFO(void,DrawLine_DrawPanelEx,( DrawPanelEx_P control, float X, float Y, float Width, float Height, int DrawMode, int DrawParame, int color, int auxcolor));\
\
/*--------清除_画板EX--------*/\
CMDINFO(void,Clear_DrawPanelEx,( DrawPanelEx_P control, int colour, float X, float Y, float Width, float Height, BOOL mode));\
\
/*--------更新_画板EX--------*/\
CMDINFO(void,Update_DrawPanelEx,( DrawPanelEx_P control, float X, float Y, float Width, float Height, BOOL mode));\
\
/*--------置剪辑区_画板EX--------*/\
CMDINFO(void,SetClip_DrawPanelEx,( DrawPanelEx_P control, int CombineMd, AutoInt Clip, int cliptype, float X, float Y, float Width, float Height, float Rgn));\
\
/*--------置绘图质量_画板EX--------*/\
CMDINFO(void,SetGraphicsquality_DrawPanelEx,( DrawPanelEx_P control, int Smoothing, int Interpolation, int PixelOffset));\
\
/*--------取绘图数据_画板EX--------*/\
CMDINFO(AutoInt,GetGraphicsData_DrawPanelEx,( DrawPanelEx_P control, int Type, float X, float Y, float Width, float Height));\
\
/*--------创建组件_滚动布局框EX--------*/\
CMDINFO(ScrollLayoutBoxEx_P,CreateControl_ScrollLayoutBoxEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt* InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, int Mode ,int HorizontalPosition, int VerticalPosition, int MaxHorizontalPosition, int MaxVerticalPosition,int Reserve1, int Reserve2, int Reserve3, int Reserve4, int Reserve5, int Reserve6, int ScrollBarMode, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_滚动布局框EX--------*/\
CMDINFO(void,SetAttribute_ScrollLayoutBoxEx,( ScrollLayoutBoxEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_滚动布局框EX--------*/\
CMDINFO(AutoInt,GetAttribute_ScrollLayoutBoxEx,( ScrollLayoutBoxEx_P control, AutoInt index));\
\
/*--------添加组件_滚动布局框EX--------*/\
CMDINFO(BOOL,InsertControl_ScrollLayoutBoxEx,( ScrollLayoutBoxEx_P control, Control_P ControlHand, int left, int top));\
\
/*--------移除组件_滚动布局框EX--------*/\
CMDINFO(BOOL,RemoveControl_ScrollLayoutBoxEx,( ScrollLayoutBoxEx_P control, Control_P ControlHand, Control_P Parentcontrol, int left, int top));\
\
/*--------创建组件_影像框Ex--------*/\
CMDINFO(MediaboxEx_P,CreateControl_MediaboxEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag,BinEx_P Skin,StrEx_P MediaFile, int Align,   int PlayState, int PlayMode, int TotalProgress, int CurrentPosition, int volume, int PlaySpeed,int Reserve1, int Reserve2, int Reserve3, int Reserve4, int Reserve5, int Reserve6, int Reserve7, int Reserve8, int Reserve9, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_影像框Ex--------*/\
CMDINFO(void,SetAttribute_MediaboxEx,( MediaboxEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_影像框Ex--------*/\
CMDINFO(AutoInt,GetAttribute_MediaboxEx,( MediaboxEx_P control, AutoInt index));\
\
/*--------执行命令_影像框Ex--------*/\
CMDINFO(AutoInt,RunCmd_MediaboxEx,(MediaboxEx_P control, AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4));\
\
/*--------创建组件_轮播框EX--------*/\
CMDINFO(CarouselBoxEx_P,CreateControl_CarouselBoxEx,(Control_P  Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt* InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin, AutoInt CurrentPosition, int Schedule, int Spacing, int LayoutStyle, int LayoutParam, int CtrlModel, int CarouselMode, int ScrollDirection, int AutoPlay, int DurationTime, int FrameDelay, int Step, int AlgorithmMode, BinEx_P ItemData, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_轮播框Ex--------*/\
CMDINFO(void,SetAttribute_CarouselBoxEx,( CarouselBoxEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_轮播框Ex--------*/\
CMDINFO(AutoInt,GetAttribute_CarouselBoxEx,( CarouselBoxEx_P control, AutoInt index));\
\
/*--------插入项目_轮播框Ex--------*/\
CMDINFO(AutoInt,InsertItem_CarouselBoxEx,( CarouselBoxEx_P control, AutoInt index, AutoInt Data, BinEx_P  Skin, ImageEx_P  Ico, int IconWidth, int IconHeight, StrEx_P Title, BinEx_P  Font, int FontColor, int HotFontColor, int Type, int Align, int reserve, int Size));\
\
/*--------删除项目_轮播框Ex--------*/\
CMDINFO(void,DeleteItem_CarouselBoxEx,( CarouselBoxEx_P control, AutoInt index));\
\
/*--------取项目数量_轮播框Ex--------*/\
CMDINFO(AutoInt,GetItemCount_CarouselBoxEx,( CarouselBoxEx_P control));\
\
/*--------置项目属性_轮播框Ex--------*/\
CMDINFO(void,SetItemAttribute_CarouselBoxEx,( CarouselBoxEx_P control, AutoInt index, int AttributeId, AutoInt Data, BinEx_P  Skin, ImageEx_P  Ico, int IconWidth, int IconHeight, StrEx_P Title, BinEx_P  Font, int FontColor, int HotFontColor, int Type, int Align, int reserve, int Size));\
\
/*--------取项目属性_轮播框Ex--------*/\
CMDINFO(AutoInt,GetItemAttribute_CarouselBoxEx,( CarouselBoxEx_P control, AutoInt index, int AttributeId));\
\
/*--------执行命令_轮播框Ex--------*/\
CMDINFO(AutoInt,RunCmd_CarouselBoxEx,(CarouselBoxEx_P control, AutoInt Cmd, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4));\
\
/*--------创建组件_超级编辑框Ex--------*/\
CMDINFO(SuperEditEx_P,CreateControl_SuperEditEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag,BinEx_P Skin,StrEx_P Kernel,StrEx_P Attribute,StrEx_P Data, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_超级编辑框Ex--------*/\
CMDINFO(void,SetAttribute_SuperEditEx,( SuperEditEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_超级编辑框Ex--------*/\
CMDINFO(AutoInt,GetAttribute_SuperEditEx,( SuperEditEx_P control, AutoInt index));\
\
/*--------执行命令_超级编辑框Ex--------*/\
CMDINFO(StrEx_P,RunCmd_SuperEditEx,(SuperEditEx_P control, StrEx_P Cmd,StrEx_P Parameter1, StrEx_P Parameter2, StrEx_P Parameter3, StrEx_P Parameter4, StrEx_P Parameter5, StrEx_P Parameter6, StrEx_P Parameter7, StrEx_P Parameter8, StrEx_P Parameter9));\
\
/*--------创建组件_聊天框Ex--------*/\
CMDINFO(ChatBoxEx_P,CreateControl_ChatBoxEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag,BinEx_P Skin,StrEx_P Kernel,StrEx_P Attribute,StrEx_P Data, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_聊天框Ex--------*/\
CMDINFO(void,SetAttribute_ChatBoxEx,( ChatBoxEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_聊天框Ex--------*/\
CMDINFO(AutoInt,GetAttribute_ChatBoxEx,( ChatBoxEx_P control, AutoInt index));\
\
/*--------执行命令_聊天框Ex--------*/\
CMDINFO(StrEx_P,RunCmd_ChatBoxEx,(ChatBoxEx_P control, StrEx_P Cmd,StrEx_P Parameter1, StrEx_P Parameter2, StrEx_P Parameter3, StrEx_P Parameter4, StrEx_P Parameter5, StrEx_P Parameter6, StrEx_P Parameter7, StrEx_P Parameter8, StrEx_P Parameter9));\
\
/*--------创建组件_超级图表框Ex--------*/\
CMDINFO(SuperChartEx_P,CreateControl_SuperChartEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag,BinEx_P Skin,StrEx_P Kernel,StrEx_P Attribute,StrEx_P Data, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_超级图表框Ex--------*/\
CMDINFO(void,SetAttribute_SuperChartEx,( SuperChartEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_超级图表框Ex--------*/\
CMDINFO(AutoInt,GetAttribute_SuperChartEx,( SuperChartEx_P control, AutoInt index));\
\
/*--------执行命令_超级图表框Ex--------*/\
CMDINFO(StrEx_P,RunCmd_SuperChartEx,(SuperChartEx_P control, StrEx_P Cmd,StrEx_P Parameter1, StrEx_P Parameter2, StrEx_P Parameter3, StrEx_P Parameter4, StrEx_P Parameter5, StrEx_P Parameter6, StrEx_P Parameter7, StrEx_P Parameter8, StrEx_P Parameter9));\
\
/*--------创建组件_数据报表框Ex--------*/\
CMDINFO(DataReportBoxEx_P,CreateControl_DataReportBoxEx,( Control_P Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt * InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag,BinEx_P Skin,StrEx_P Kernel,StrEx_P Attribute,StrEx_P Data, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_数据报表框Ex--------*/\
CMDINFO(void,SetAttribute_DataReportBoxEx,( DataReportBoxEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_数据报表框Ex--------*/\
CMDINFO(AutoInt,GetAttribute_DataReportBoxEx,( DataReportBoxEx_P control, AutoInt index));\
\
/*--------执行命令_数据报表框Ex--------*/\
CMDINFO(StrEx_P,RunCmd_DataReportBoxEx,(DataReportBoxEx_P control, StrEx_P Cmd,StrEx_P Parameter1, StrEx_P Parameter2, StrEx_P Parameter3, StrEx_P Parameter4, StrEx_P Parameter5, StrEx_P Parameter6, StrEx_P Parameter7, StrEx_P Parameter8, StrEx_P Parameter9));\
\
/*--------创建组件_分页导航条Ex--------*/\
CMDINFO(PageNavigBarEx_P,CreateControl_PageNavigBarEx,(Control_P  Parent, HWND Window, int LEFT, int Top, int Width, int Height, ExuiCallback Callback, AutoInt* InitAttr, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BinEx_P Cursor, AutoInt Tag, BinEx_P Skin,  int PageCount,  int CurrentPage, int NavigBtnCount, int Align, BinEx_P Font, int FontColor, int CurrentFontClour,int LeftReservation, int RightReservation, BinEx_P ElemeData, BinEx_P Layout));\
\
/*--------置属性_分页导航条Ex--------*/\
CMDINFO(void,SetAttribute_PageNavigBarEx,( PageNavigBarEx_P control, AutoInt index, AutoInt attribute));\
\
/*--------取属性_分页导航条Ex--------*/\
CMDINFO(AutoInt,GetAttribute_PageNavigBarEx,( PageNavigBarEx_P control, AutoInt index));\
\

/*------------------------------------------EXUIAPI定义结束------------------------------------------*/




/*------------------------------------------以下代码无需关注-----------------------------------------*/






#define Definition2(returnVal , name , parameter) \
 __declspec(selectany) PFN_##name name = NULL;\

#define Definition(returnVal , name , parameter) \
typedef returnVal (WINAPI * PFN_##name) parameter ;\
 extern  PFN_##name name;\

#define Initial(returnVal , name , parameter) name## = (PFN_##name)MyGetProcAddress(exuihDllInst, #name);\

EX_UI_APIINFO(Definition)

extern  HINSTANCE exuihDllInst;
HINSTANCE __stdcall InitExui(const TCHAR* data, int datalong);
BinEx_P ReadRcDataEx(const TCHAR* strFileType, int wResID);



#ifdef UNICODE
#define apiwcscat wcscat
#define apifopen _wfopen_s
#else
#define apiwcscat strcat
#define apifopen fopen_s
#endif 


#ifdef EFNEDEBUG
#define UseExuiDll 1
#endif





#ifdef UseExuiDll

#define LoadMsage(msg)  

#define InitExuiLibDefault() \
if (!exuihDllInst) {\
LoadTtpe = 1;MyLoadLibrary = (MyLoadLibraryFunc)LoadLibrary;MyGetProcAddress = (MyGetProcAddressFunc)GetProcAddress;MyFreeLibrary = (MyFreeLibraryFunc)FreeLibrary;\
if(!exuihDllInst){exuihDllInst = LoadLibrary(ExuiLibNameText1);}\
if(!exuihDllInst){exuihDllInst = LoadLibrary(ExuiLibNameText2);}\
if (!exuihDllInst) { exuihDllInst = LoadLibrary(ExuiLibNameText3); }\
if (!exuihDllInst)\
{\
TCHAR ExuiFile[MAX_PATH];long DataLong = MAX_PATH * sizeof(TCHAR);memset(ExuiFile, 0, DataLong); RegQueryValue(HKEY_LOCAL_MACHINE, ExuiLibNameConfigText, ExuiFile, &DataLong);\
if (ExuiFile[0]) {exuihDllInst = LoadLibrary(ExuiFile); LoadMsage(ExuiFile); }\
}}\


#else



#define LoadMsage(msg) \

//#define LoadMsage(msg) if(exuihDllInst){ MessageBox(((HWND)0), msg, TEXT("Tip_OK"), 0);} else{ MessageBox(((HWND)0), msg, TEXT("Tip_Warning"), 0);} 

#define InitExuiLibDefault() \
if (!exuihDllInst) { \
const unsigned char* GETEXUILIB(); int GETEXUILIBLEN(); \
LoadTtpe = 2;	MyLoadLibrary = (MyLoadLibraryFunc)MemoryLoadLibrary_Exui;MyGetProcAddress = (MyGetProcAddressFunc)MemoryGetProcAddress_Exui;	MyFreeLibrary = (MyFreeLibraryFunc)MemoryFreeLibrary_Exui;exuihDllInst = MemoryLoadLibrary_Exui((LPCVOID)GETEXUILIB(), GETEXUILIBLEN()); \
if (!exuihDllInst) { MessageBoxA(((HWND)0), "当前系统版本过低,界面库加载失败,若为xp系统请使用xp版界面库", "警告", 0);}\
}





#endif






#define EX_UI_CMDINFO \
EX_UI_APIINFO(Definition2)\
typedef HMODULE(__stdcall* MyLoadLibraryFunc)(LPVOID, SIZE_T); \
typedef FARPROC(__stdcall* MyGetProcAddressFunc)(HMODULE, LPCSTR); \
typedef BOOL(__stdcall* MyFreeLibraryFunc)(HMODULE); \
HINSTANCE exuihDllInst = NULL; HANDLE hmap = NULL; void** loaddata = NULL;  MyLoadLibraryFunc MyLoadLibrary = NULL;        MyGetProcAddressFunc  MyGetProcAddress = NULL;    MyFreeLibraryFunc  MyFreeLibrary = NULL;   AutoInt LoadTtpe = NULL;\
HINSTANCE __stdcall InitExui(const TCHAR *  data, int datalong) \
{\
if (datalong == -9) { return exuihDllInst; }	if (exuihDllInst) { return exuihDllInst; }\
TCHAR ExuiLibName[MAX_PATH];memset(ExuiLibName, 0, sizeof(ExuiLibName)); wsprintf(ExuiLibName, ExuiLibNameTagText, (int)GetCurrentProcessId()); \
hmap = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, ExuiLibName); \
if (hmap) {loaddata = (void**)MapViewOfFile(hmap, FILE_MAP_ALL_ACCESS, 0, 0, 0); }else { hmap = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE | SEC_COMMIT, 0, 1024, ExuiLibName); if (hmap) { loaddata = (void**)MapViewOfFile(hmap, FILE_MAP_READ | FILE_MAP_WRITE, 0, 0, 0); } }\
if (!loaddata) {  return NULL; }\
if ((loaddata[0])){LoadTtpe = (AutoInt)loaddata[0]; 	MyLoadLibrary = (MyLoadLibraryFunc)loaddata[1]; 	MyGetProcAddress = (MyGetProcAddressFunc)loaddata[2]; 		MyFreeLibrary = (MyFreeLibraryFunc)loaddata[3]; 		exuihDllInst = (HINSTANCE)loaddata[4]; }\
else\
{\
if (!data) { InitExuiLibDefault(); }\
else {\
		if (datalong > 0) { LoadTtpe = 2; MyLoadLibrary = (MyLoadLibraryFunc)MemoryLoadLibrary_Exui;		MyGetProcAddress = (MyGetProcAddressFunc)MemoryGetProcAddress_Exui; 		MyFreeLibrary = (MyFreeLibraryFunc)MemoryFreeLibrary_Exui;		exuihDllInst = MemoryLoadLibrary_Exui(data, datalong); }\
		else {              LoadTtpe = 1; MyLoadLibrary = (MyLoadLibraryFunc)LoadLibrary;			MyGetProcAddress = (MyGetProcAddressFunc)GetProcAddress; 			MyFreeLibrary = (MyFreeLibraryFunc)FreeLibrary;				exuihDllInst = LoadLibrary(data); }\
}\
loaddata[0] = (void*)LoadTtpe; loaddata[1] = (void*)MyLoadLibrary; loaddata[2] = (void*)MyGetProcAddress; loaddata[3] = (void*)MyFreeLibrary; loaddata[4] = (void*)exuihDllInst; \
}\
if (exuihDllInst) { EX_UI_APIINFO(Initial) if (CallInternalFunction_Ex) { CallInternalFunction_Ex(2008, (AutoInt)exuihDllInst, 0, 1, ExuiKrnlnVersion); } }else { memset(loaddata, 0, MAX_PATH); }\
UnmapViewOfFile(loaddata); \
return exuihDllInst; \
}\
\
BinEx_P ReadRcDataEx(const TCHAR * strFileType, int wResID)\
{\
	MEMORY_BASIC_INFORMATION info;\
	::VirtualQuery(&ReadRcDataEx, &info, sizeof(info));\
	HMODULE hModule = (HMODULE)info.AllocationBase;\
	if (!hModule) { hModule= GetModuleHandle(NULL); }\
	HRSRC hrsc = FindResource(hModule, MAKEINTRESOURCE(wResID), strFileType);\
	HGLOBAL hG = LoadResource(hModule, hrsc);\
	FreeResource(hrsc);\
	return CreateBinEx((void*)LockResource(hG), SizeofResource(hModule, hrsc));\
}\







// CloseHandle(hmap); 
//if(!exuihDllInst){MessageBoxA(((HWND)0), lpdata, "装载失败", 0);}else{MessageBoxA(((HWND)0), lpdata, "装载成功", 0); }
/*if (exuihDllInst == NULL) { return NULL; / * MessageBoxA(((HWND)0), "程序缺少界面运行库必须依赖的 ExuiKrnln_Win32.lib 文件", "警告", 0); return NULL;* / }*/
//if(exuihDllInst ){MessageBoxA(((HWND)0), "内存版本加载成功", "警告", 0);}else{MessageBoxA(((HWND)0), "内存版本加载失败", "警告", 0);}

