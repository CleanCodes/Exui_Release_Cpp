#include "Exui.h"



#define GetControlProgramEx(ControlHandle) GetControlExClass(ControlHandle,ExuiProgramEx)

#define GetControlProgramExThis(ControlHandle)  GetControlExClassThis(ControlHandle,ExuiProgramEx)



#define InitCreateAttrStar(Exui_EventPro)  AutoInt InitAttr[5]{ 0,(AutoInt)&control,(AutoInt)&Exui_EventPro,((Parent.GetType() == ControlType_ExuiProgramEx) ? (AutoInt)&Parent : ((Parent.GetType() == ControlType_WindowBoxEx) ? (AutoInt)GetProp((HWND)Parent.control, TEXT("ExuiWindow_BindThis")) : ((AutoInt*)Parent.control)[3])),Tag};

#define GetCreateParentControl(Parent) (Control_P)(Parent.GetType() == ControlType_WindowBoxEx ? ControlGetBindControl((HWND)Parent.control) : (Parent.GetType() == ControlType_ExuiProgramEx ? Parent.control : Parent.control))

#define GetCreateParentHwnd(Parent) (HWND)(Parent.GetType() == ControlType_WindowBoxEx ? Parent.control : (Parent.GetType() == ControlType_ExuiProgramEx ? (Control_P)((ExuiProgramEx*)&Parent)->Hwnd :  ControlGetWindow(Parent.control)))

#define InitCreateAttrEnd(IsWindowBoxEx)  if(Parent.GetType() == ControlType_ExuiProgramEx){ if(IsWindowBoxEx){ if(!(((ExuiProgramEx*)&Parent)->Hwnd)){((ExuiProgramEx*)&Parent)->Hwnd=(AutoInt)control;} }else if(!Parent.control){ Parent.control=control;}  }




//CallBack
namespace exui
{

	/*----------其他事件分发开始--------*/




	/*----系统窗口组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_WindowBoxEx(HWND hwnd, AutoInt EventType, AutoInt wParam, AutoInt lParam)
	{
		ExuiProgramEx* WinExthis = ((ExuiProgramEx*)GetProp((HWND)hwnd, TEXT("ExuiWindow_BindThis")));
		switch (EventType)
		{
		case WM_CREATE: return WinExthis->OnWindowBoxExEvent_Create(hwnd, EventType, wParam, lParam); break;
		case WM_CLOSE: return WinExthis->OnWindowBoxExEvent_Close(hwnd, EventType, wParam, lParam); break;
		case WM_DESTROY: return WinExthis->OnWindowBoxExEvent_Destroy(hwnd, EventType, wParam, lParam); break;
		case WM_MOVE: return WinExthis->OnWindowBoxExEvent_Move(hwnd, EventType, wParam, lParam); break;
		case WM_SIZE: return WinExthis->OnWindowBoxExEvent_Size(hwnd, EventType, wParam, lParam); break;
		case WM_ACTIVATE: return WinExthis->OnWindowBoxExEvent_Activate(hwnd, EventType, wParam, lParam); break;
		case WM_SHOWWINDOW: return WinExthis->OnWindowBoxExEvent_ShowWindow(hwnd, EventType, wParam, lParam); break;
		case WM_LBUTTONDOWN: return WinExthis->OnWindowBoxExEvent_L_Down(hwnd, EventType, wParam, lParam); break;
		case WM_LBUTTONUP: return WinExthis->OnWindowBoxExEvent_L_Up(hwnd, EventType, wParam, lParam); break;
		case WM_LBUTTONDBLCLK: return WinExthis->OnWindowBoxExEvent_L_DClick(hwnd, EventType, wParam, lParam); break;
		case WM_RBUTTONDOWN: return WinExthis->OnWindowBoxExEvent_R_Down(hwnd, EventType, wParam, lParam); break;
		case WM_RBUTTONUP: return WinExthis->OnWindowBoxExEvent_R_Up(hwnd, EventType, wParam, lParam); break;
		case WM_MOUSEMOVE: return WinExthis->OnWindowBoxExEvent_MouseMove(hwnd, EventType, wParam, lParam); break;
		case WM_SETFOCUS: return WinExthis->OnWindowBoxExEvent_SetFocus(hwnd, EventType, wParam, lParam); break;
		case WM_KILLFOCUS: return WinExthis->OnWindowBoxExEvent_KillFocus(hwnd, EventType, wParam, lParam); break;
		case WM_KEYDOWN: return WinExthis->OnWindowBoxExEvent_KeyDown(hwnd, EventType, wParam, lParam); break;
		case WM_KEYUP: return WinExthis->OnWindowBoxExEvent_KeyUp(hwnd, EventType, wParam, lParam); break;
		case WM_CHAR: return WinExthis->OnWindowBoxExEvent_Char(hwnd, EventType, wParam, lParam); break;
		case WM_MOUSEWHEEL: return WinExthis->OnWindowBoxExEvent_MouseWheel(hwnd, EventType, wParam, lParam); break;
		case WM_DROPFILES: return WinExthis->OnWindowBoxExEvent_DropFiles(hwnd, EventType, wParam, lParam); break;
		case 12313: return WinExthis->OnWindowBoxExEvent_NotifyIcon(hwnd, EventType, wParam, lParam); break;
		default: break;
		}
		return WinExthis->OnWindowBoxExEvent_Other(hwnd, EventType, wParam, lParam);
	}

	/*----缓动任务EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_SlowMotionTaskEx(SlowMotionTaskEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		
		return GetControlProgramExThis(EventControl)->OnSlowMotionTaskExEvent_Event((SlowMotionTaskEx_P)EventControl, EventType, Param1, Param2, Param3, Param4);

	}

	/*----定时器EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_TimerEx(TimeEx_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		return GetControlProgramExThis(EventControl)->OnTimerExEvent_Event((TimeEx_P)EventControl, EventType, Param1, Param2, Param3, Param4);

	}

	/*----菜单EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_MenuEx(Control_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventType)
		{
		case Event_MenuEx_ExtendPop: return GetControlProgramExThis(EventControl)->Event_MenuEx_ExtendPop((MenuEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_MenuEx_ExtendClose: return GetControlProgramExThis(EventControl)->Event_MenuEx_ExtendClose((MenuEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_MenuEx_ExtendMmeasure: return GetControlProgramExThis(EventControl)->Event_MenuEx_ExtendMmeasure((MenuEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_MenuEx_ExtendDraw: return GetControlProgramExThis(EventControl)->Event_MenuEx_ExtendDraw((MenuEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_MenuEx_WillPop: return GetControlProgramExThis(EventControl)->Event_MenuEx_WillPop((MenuEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_MenuEx_Pop: return GetControlProgramExThis(EventControl)->Event_MenuEx_Pop((MenuEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_MenuEx_WillClose: return GetControlProgramExThis(EventControl)->Event_MenuEx_WillClose((MenuEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_MenuEx_Close: return GetControlProgramExThis(EventControl)->Event_MenuEx_Close((MenuEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_MenuEx_Item_L_Click: return GetControlProgramExThis(EventControl)->OnMenuExEvent_Item_L_Click((MenuEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_MenuEx_Item_R_Click: return GetControlProgramExThis(EventControl)->OnMenuExEvent_Item_R_Click((MenuEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_MenuEx_Item_L_Up: return GetControlProgramExThis(EventControl)->OnMenuExEvent_Item_L_Up((MenuEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_MenuEx_Item_R_Up: return GetControlProgramExThis(EventControl)->OnMenuExEvent_Item_R_Up((MenuEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_MenuEx_Item_L_Down: return GetControlProgramExThis(EventControl)->OnMenuExEvent_Item_L_Down((MenuEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_MenuEx_Item_R_Down: return GetControlProgramExThis(EventControl)->OnMenuExEvent_Item_R_Down((MenuEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_MenuEx_Item_L_DClick: return GetControlProgramExThis(EventControl)->OnMenuExEvent_Item_L_DClick((MenuEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_MenuEx_Item_R_DClick: return GetControlProgramExThis(EventControl)->OnMenuExEvent_Item_R_DClick((MenuEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_MenuEx_Item_MouseIn: return GetControlProgramExThis(EventControl)->OnMenuExEvent_Item_MouseIn((MenuEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_MenuEx_Item_MouseOut: return GetControlProgramExThis(EventControl)->OnMenuExEvent_Item_MouseOut((MenuEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		default: break;
		}
		return NULL;
	}

	/*----下拉列表EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_DownlistEx(Control_P EventControl, AutoInt EventType, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventType)
		{
		case Event_DownlistEx_ExtendPop: return GetControlProgramExThis(EventControl)->Event_DownlistEx_ExtendPop((DownlistEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_DownlistEx_ExtendClose: return GetControlProgramExThis(EventControl)->Event_DownlistEx_ExtendClose((DownlistEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_DownlistEx_ExtendMmeasure: return GetControlProgramExThis(EventControl)->Event_DownlistEx_ExtendMmeasure((DownlistEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_DownlistEx_ExtendDraw: return GetControlProgramExThis(EventControl)->Event_DownlistEx_ExtendDraw((DownlistEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_DownlistEx_WillPop: return GetControlProgramExThis(EventControl)->Event_DownlistEx_WillPop((DownlistEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_DownlistEx_Pop: return GetControlProgramExThis(EventControl)->Event_DownlistEx_Pop((DownlistEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_DownlistEx_WillClose: return GetControlProgramExThis(EventControl)->Event_DownlistEx_WillClose((DownlistEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_DownlistEx_Close: return GetControlProgramExThis(EventControl)->Event_DownlistEx_Close((DownlistEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_DownlistEx_Item_L_Click: return GetControlProgramExThis(EventControl)->OnDownlistExEvent_Item_L_Click((DownlistEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_DownlistEx_Item_R_Click: return GetControlProgramExThis(EventControl)->OnDownlistExEvent_Item_R_Click((DownlistEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_DownlistEx_Item_L_Up: return GetControlProgramExThis(EventControl)->OnDownlistExEvent_Item_L_Up((DownlistEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_DownlistEx_Item_R_Up: return GetControlProgramExThis(EventControl)->OnDownlistExEvent_Item_R_Up((DownlistEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_DownlistEx_Item_L_Down: return GetControlProgramExThis(EventControl)->OnDownlistExEvent_Item_L_Down((DownlistEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_DownlistEx_Item_R_Down: return GetControlProgramExThis(EventControl)->OnDownlistExEvent_Item_R_Down((DownlistEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_DownlistEx_Item_L_DClick: return GetControlProgramExThis(EventControl)->OnDownlistExEvent_Item_L_DClick((DownlistEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_DownlistEx_Item_R_DClick: return GetControlProgramExThis(EventControl)->OnDownlistExEvent_Item_R_DClick((DownlistEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_DownlistEx_Item_MouseIn: return GetControlProgramExThis(EventControl)->OnDownlistExEvent_Item_MouseIn((DownlistEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		case Event_DownlistEx_Item_MouseOut: return GetControlProgramExThis(EventControl)->OnDownlistExEvent_Item_MouseOut((DownlistEx_P)EventControl, EventType, Param1, Param2, Param3, Param4); break;
		default: break;
		}
		return NULL;
	}


	/*----窗口重画事件组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_ParentWinRefreshEvent_ControlEx(WindowEx_P EventControl, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4)
	{
					
		switch (GetControlExType(EventControl))
		{
		case ControlType_WindowEx:return GetControlProgramExThis(EventControl)->OnWindowExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4);		    break;
		case ControlType_FilterEx: return GetControlProgramExThis(EventControl)->OnFilterExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_LabelEx: return GetControlProgramExThis(EventControl)->OnLabelExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ExtendEx: return GetControlProgramExThis(EventControl)->OnExtendExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_MinutesboxEx: return GetControlProgramExThis(EventControl)->OnMinutesboxExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ListboxEx: return GetControlProgramExThis(EventControl)->OnListboxExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_RadiobuttonEx: return GetControlProgramExThis(EventControl)->OnRadiobuttonExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_IcoListboxEx: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ImagebuttonEx: return GetControlProgramExThis(EventControl)->OnImagebuttonExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_PictureBoxEx: return GetControlProgramExThis(EventControl)->OnPictureBoxExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_MultifunctionButtonEx:return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ToolbarEx: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ButtonEx: return GetControlProgramExThis(EventControl)->OnButtonExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_TreeListEx:return GetControlProgramExThis(EventControl)->OnTreeListExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_WebBrowserEx:return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_SliderbarEx:return GetControlProgramExThis(EventControl)->OnSliderbarExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ScrollbarEx:return GetControlProgramExThis(EventControl)->OnScrollbarExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ComboboxEx: return GetControlProgramExThis(EventControl)->OnComboboxExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_EditboxEx: return GetControlProgramExThis(EventControl)->OnEditboxExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_SuperListboxEx: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_SuperbuttonEx: return GetControlProgramExThis(EventControl)->OnSuperbuttonExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ProgressbarEx:return GetControlProgramExThis(EventControl)->OnProgressbarExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_SelectthefolderEx:return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ChoiceboxEx:return GetControlProgramExThis(EventControl)->OnChoiceboxExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_CalendarBoxEx: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ColorPickEx: return GetControlProgramExThis(EventControl)->OnColorPickExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_RichEditEx:return GetControlProgramExThis(EventControl)->OnRichEditExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_AnimationbuttonEx:return GetControlProgramExThis(EventControl)->OnAnimationbuttonExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_AdvancedFormEx: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_SlideButtonEx:return GetControlProgramExThis(EventControl)->OnSlideButtonExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_PieChartEx: return GetControlProgramExThis(EventControl)->OnPieChartExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_BarChartEx: return GetControlProgramExThis(EventControl)->OnBarChartExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_CurveChartEx: return GetControlProgramExThis(EventControl)->OnCurveChartExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_CandleChartEx:return GetControlProgramExThis(EventControl)->OnCandleChartExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_DrawPanelEx: return GetControlProgramExThis(EventControl)->OnDrawPanelExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ScrollLayoutBoxEx: return GetControlProgramExThis(EventControl)->OnScrollLayoutBoxExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_MediaboxEx: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_ParentWinRefreshEvent(EventControl, Event_Control_ParentWinRefreshEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		default: break;
		}

		return NULL;
	}

	/*----公有事件组件分发----*/
	static AutoInt Exui_EventPro_PublicEvent_ControlEx(WindowEx_P EventControl, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4)
	{

		switch (GetControlExType(EventControl))
		{
		case ControlType_WindowEx:return GetControlProgramExThis(EventControl)->OnWindowExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4);		    break;
		case ControlType_FilterEx: return GetControlProgramExThis(EventControl)->OnFilterExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_LabelEx: return GetControlProgramExThis(EventControl)->OnLabelExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ExtendEx: return GetControlProgramExThis(EventControl)->OnExtendExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_MinutesboxEx: return GetControlProgramExThis(EventControl)->OnMinutesboxExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ListboxEx: return GetControlProgramExThis(EventControl)->OnListboxExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_RadiobuttonEx: return GetControlProgramExThis(EventControl)->OnRadiobuttonExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_IcoListboxEx: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ImagebuttonEx: return GetControlProgramExThis(EventControl)->OnImagebuttonExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_PictureBoxEx: return GetControlProgramExThis(EventControl)->OnPictureBoxExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_MultifunctionButtonEx:return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ToolbarEx: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ButtonEx: return GetControlProgramExThis(EventControl)->OnButtonExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_TreeListEx:return GetControlProgramExThis(EventControl)->OnTreeListExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_WebBrowserEx:return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_SliderbarEx:return GetControlProgramExThis(EventControl)->OnSliderbarExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ScrollbarEx:return GetControlProgramExThis(EventControl)->OnScrollbarExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ComboboxEx: return GetControlProgramExThis(EventControl)->OnComboboxExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_EditboxEx: return GetControlProgramExThis(EventControl)->OnEditboxExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_SuperListboxEx: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_SuperbuttonEx: return GetControlProgramExThis(EventControl)->OnSuperbuttonExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ProgressbarEx:return GetControlProgramExThis(EventControl)->OnProgressbarExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_SelectthefolderEx:return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ChoiceboxEx:return GetControlProgramExThis(EventControl)->OnChoiceboxExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_CalendarBoxEx: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ColorPickEx: return GetControlProgramExThis(EventControl)->OnColorPickExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_RichEditEx:return GetControlProgramExThis(EventControl)->OnRichEditExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_AnimationbuttonEx:return GetControlProgramExThis(EventControl)->OnAnimationbuttonExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_AdvancedFormEx: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_SlideButtonEx:return GetControlProgramExThis(EventControl)->OnSlideButtonExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_PieChartEx: return GetControlProgramExThis(EventControl)->OnPieChartExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_BarChartEx: return GetControlProgramExThis(EventControl)->OnBarChartExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_CurveChartEx: return GetControlProgramExThis(EventControl)->OnCurveChartExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_CandleChartEx:return GetControlProgramExThis(EventControl)->OnCandleChartExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_DrawPanelEx: return GetControlProgramExThis(EventControl)->OnDrawPanelExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ScrollLayoutBoxEx: return GetControlProgramExThis(EventControl)->OnScrollLayoutBoxExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_MediaboxEx: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_PublicEvent(EventControl, Event_Control_PublicEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		default: break;
		}

		return NULL;
	}

	/*----私有事件组件分发----*/
	static AutoInt Exui_EventPro_PrivateEvent_ControlEx(WindowEx_P EventControl, AutoInt EventType, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3, AutoInt RelatedParam4)
	{

		switch (GetControlExType(EventControl))
		{
		case ControlType_WindowEx:return GetControlProgramExThis(EventControl)->OnWindowExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4);		    break;
		case ControlType_FilterEx: return GetControlProgramExThis(EventControl)->OnFilterExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_LabelEx: return GetControlProgramExThis(EventControl)->OnLabelExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ExtendEx: return GetControlProgramExThis(EventControl)->OnExtendExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_MinutesboxEx: return GetControlProgramExThis(EventControl)->OnMinutesboxExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ListboxEx: return GetControlProgramExThis(EventControl)->OnListboxExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_RadiobuttonEx: return GetControlProgramExThis(EventControl)->OnRadiobuttonExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_IcoListboxEx: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ImagebuttonEx: return GetControlProgramExThis(EventControl)->OnImagebuttonExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_PictureBoxEx: return GetControlProgramExThis(EventControl)->OnPictureBoxExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_MultifunctionButtonEx:return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ToolbarEx: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ButtonEx: return GetControlProgramExThis(EventControl)->OnButtonExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_TreeListEx:return GetControlProgramExThis(EventControl)->OnTreeListExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_WebBrowserEx:return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_SliderbarEx:return GetControlProgramExThis(EventControl)->OnSliderbarExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ScrollbarEx:return GetControlProgramExThis(EventControl)->OnScrollbarExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ComboboxEx: return GetControlProgramExThis(EventControl)->OnComboboxExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_EditboxEx: return GetControlProgramExThis(EventControl)->OnEditboxExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_SuperListboxEx: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_SuperbuttonEx: return GetControlProgramExThis(EventControl)->OnSuperbuttonExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ProgressbarEx:return GetControlProgramExThis(EventControl)->OnProgressbarExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_SelectthefolderEx:return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ChoiceboxEx:return GetControlProgramExThis(EventControl)->OnChoiceboxExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_CalendarBoxEx: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ColorPickEx: return GetControlProgramExThis(EventControl)->OnColorPickExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_RichEditEx:return GetControlProgramExThis(EventControl)->OnRichEditExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_AnimationbuttonEx:return GetControlProgramExThis(EventControl)->OnAnimationbuttonExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_AdvancedFormEx: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_SlideButtonEx:return GetControlProgramExThis(EventControl)->OnSlideButtonExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_PieChartEx: return GetControlProgramExThis(EventControl)->OnPieChartExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_BarChartEx: return GetControlProgramExThis(EventControl)->OnBarChartExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_CurveChartEx: return GetControlProgramExThis(EventControl)->OnCurveChartExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_CandleChartEx:return GetControlProgramExThis(EventControl)->OnCandleChartExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_DrawPanelEx: return GetControlProgramExThis(EventControl)->OnDrawPanelExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_ScrollLayoutBoxEx: return GetControlProgramExThis(EventControl)->OnScrollLayoutBoxExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		case ControlType_MediaboxEx: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_PrivateEvent(EventControl, Event_Control_PrivateEvent, EventType, RelatedParam1, RelatedParam2, RelatedParam3, RelatedParam4); break;
		default: break;
		}

		return NULL;
	}

	/*----任务事件组件分发----*/
	static AutoInt Exui_EventPro_TaskEvent_ControlEx(WindowEx_P EventControl, AutoInt CallMode, AutoInt EventType, AutoInt EventMark, AutoInt RelatedParam)
	{


		switch (GetControlExType(EventControl))
		{
		case ControlType_WindowEx:return GetControlProgramExThis(EventControl)->OnWindowExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam);		    break;
		case ControlType_FilterEx: return GetControlProgramExThis(EventControl)->OnFilterExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_LabelEx: return GetControlProgramExThis(EventControl)->OnLabelExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_ExtendEx: return GetControlProgramExThis(EventControl)->OnExtendExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_MinutesboxEx: return GetControlProgramExThis(EventControl)->OnMinutesboxExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_ListboxEx: return GetControlProgramExThis(EventControl)->OnListboxExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_RadiobuttonEx: return GetControlProgramExThis(EventControl)->OnRadiobuttonExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_IcoListboxEx: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_ImagebuttonEx: return GetControlProgramExThis(EventControl)->OnImagebuttonExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_PictureBoxEx: return GetControlProgramExThis(EventControl)->OnPictureBoxExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_MultifunctionButtonEx:return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_ToolbarEx: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_ButtonEx: return GetControlProgramExThis(EventControl)->OnButtonExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_TreeListEx:return GetControlProgramExThis(EventControl)->OnTreeListExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_WebBrowserEx:return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_SliderbarEx:return GetControlProgramExThis(EventControl)->OnSliderbarExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_ScrollbarEx:return GetControlProgramExThis(EventControl)->OnScrollbarExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_ComboboxEx: return GetControlProgramExThis(EventControl)->OnComboboxExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_EditboxEx: return GetControlProgramExThis(EventControl)->OnEditboxExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_SuperListboxEx: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_SuperbuttonEx: return GetControlProgramExThis(EventControl)->OnSuperbuttonExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_ProgressbarEx:return GetControlProgramExThis(EventControl)->OnProgressbarExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_SelectthefolderEx:return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_ChoiceboxEx:return GetControlProgramExThis(EventControl)->OnChoiceboxExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_CalendarBoxEx: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_ColorPickEx: return GetControlProgramExThis(EventControl)->OnColorPickExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_RichEditEx:return GetControlProgramExThis(EventControl)->OnRichEditExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_AnimationbuttonEx:return GetControlProgramExThis(EventControl)->OnAnimationbuttonExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_AdvancedFormEx: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_SlideButtonEx:return GetControlProgramExThis(EventControl)->OnSlideButtonExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_PieChartEx: return GetControlProgramExThis(EventControl)->OnPieChartExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_BarChartEx: return GetControlProgramExThis(EventControl)->OnBarChartExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_CurveChartEx: return GetControlProgramExThis(EventControl)->OnCurveChartExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_CandleChartEx:return GetControlProgramExThis(EventControl)->OnCandleChartExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_DrawPanelEx: return GetControlProgramExThis(EventControl)->OnDrawPanelExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_ScrollLayoutBoxEx: return GetControlProgramExThis(EventControl)->OnScrollLayoutBoxExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		case ControlType_MediaboxEx: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_TaskEvent(EventControl, Event_Control_TaskEvent, CallMode, EventMark, RelatedParam); break;
		default: break;
		}

		return NULL;
	}

	/*----元素事件组件分发----*/
	static AutoInt Exui_EventPro_ElemEvent_ControlEx(WindowEx_P EventControl, AutoInt EventType, AutoInt ElemType, AutoInt Index, AutoInt ColumnId, AutoInt CtrId, AutoInt RelatedParam1, AutoInt RelatedParam2, AutoInt RelatedParam3)
	{

		switch (GetControlExType(EventControl))
		{
		case ControlType_WindowEx:return GetControlProgramExThis(EventControl)->OnWindowExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3);		    break;
		case ControlType_FilterEx: return GetControlProgramExThis(EventControl)->OnFilterExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_LabelEx: return GetControlProgramExThis(EventControl)->OnLabelExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_ExtendEx: return GetControlProgramExThis(EventControl)->OnExtendExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_MinutesboxEx: return GetControlProgramExThis(EventControl)->OnMinutesboxExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_ListboxEx: return GetControlProgramExThis(EventControl)->OnListboxExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_RadiobuttonEx: return GetControlProgramExThis(EventControl)->OnRadiobuttonExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_IcoListboxEx: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_ImagebuttonEx: return GetControlProgramExThis(EventControl)->OnImagebuttonExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_PictureBoxEx: return GetControlProgramExThis(EventControl)->OnPictureBoxExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_MultifunctionButtonEx:return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_ToolbarEx: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_ButtonEx: return GetControlProgramExThis(EventControl)->OnButtonExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_TreeListEx:return GetControlProgramExThis(EventControl)->OnTreeListExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_WebBrowserEx:return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_SliderbarEx:return GetControlProgramExThis(EventControl)->OnSliderbarExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_ScrollbarEx:return GetControlProgramExThis(EventControl)->OnScrollbarExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_ComboboxEx: return GetControlProgramExThis(EventControl)->OnComboboxExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_EditboxEx: return GetControlProgramExThis(EventControl)->OnEditboxExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_SuperListboxEx: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_SuperbuttonEx: return GetControlProgramExThis(EventControl)->OnSuperbuttonExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_ProgressbarEx:return GetControlProgramExThis(EventControl)->OnProgressbarExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_SelectthefolderEx:return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_ChoiceboxEx:return GetControlProgramExThis(EventControl)->OnChoiceboxExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_CalendarBoxEx: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_ColorPickEx: return GetControlProgramExThis(EventControl)->OnColorPickExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_RichEditEx:return GetControlProgramExThis(EventControl)->OnRichEditExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_AnimationbuttonEx:return GetControlProgramExThis(EventControl)->OnAnimationbuttonExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_AdvancedFormEx: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_SlideButtonEx:return GetControlProgramExThis(EventControl)->OnSlideButtonExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_PieChartEx: return GetControlProgramExThis(EventControl)->OnPieChartExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_BarChartEx: return GetControlProgramExThis(EventControl)->OnBarChartExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_CurveChartEx: return GetControlProgramExThis(EventControl)->OnCurveChartExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_CandleChartEx:return GetControlProgramExThis(EventControl)->OnCandleChartExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_DrawPanelEx: return GetControlProgramExThis(EventControl)->OnDrawPanelExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_ScrollLayoutBoxEx: return GetControlProgramExThis(EventControl)->OnScrollLayoutBoxExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		case ControlType_MediaboxEx: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_ElemEvent(EventControl, Event_Control_ElemEvent, EventType, ElemType, Index, ColumnId, CtrId, RelatedParam1, RelatedParam2, RelatedParam3); break;
		default: break;
		}

		return NULL;
	}




	/*----------其他事件分发结束--------*/


	/*----------组件事件分发开始--------*/


/*----窗口EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_WindowEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnWindowExEvent_L_Click((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnWindowExEvent_R_Click((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnWindowExEvent_L_Down((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnWindowExEvent_R_Down((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnWindowExEvent_L_Up((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnWindowExEvent_R_Up((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnWindowExEvent_L_DClick((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnWindowExEvent_R_DClick((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnWindowExEvent_MouseMove((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnWindowExEvent_MouseEnterLeave((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnWindowExEvent_MouseWheel((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnWindowExEvent_keyEvent((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnWindowExEvent_CharInput((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnWindowExEvent_Focus((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnWindowExEvent_Other((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WindowEx_FeedBackEvent: return GetControlProgramExThis(EventControl)->OnWindowExEvent_FeedBackEvent((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WindowEx_Button_L_Click: return GetControlProgramExThis(EventControl)->OnWindowExEvent_Button_L_Click((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WindowEx_Button_R_Click: return GetControlProgramExThis(EventControl)->OnWindowExEvent_Button_R_Click((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WindowEx_Button_L_Up: return GetControlProgramExThis(EventControl)->OnWindowExEvent_Button_L_Up((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WindowEx_Button_R_Up: return GetControlProgramExThis(EventControl)->OnWindowExEvent_Button_R_Up((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WindowEx_Button_L_Down: return GetControlProgramExThis(EventControl)->OnWindowExEvent_Button_L_Down((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WindowEx_Button_R_Down: return GetControlProgramExThis(EventControl)->OnWindowExEvent_Button_R_Down((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WindowEx_Button_L_DClick: return GetControlProgramExThis(EventControl)->OnWindowExEvent_Button_L_DClick((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WindowEx_Button_R_DClick: return GetControlProgramExThis(EventControl)->OnWindowExEvent_Button_R_DClick((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WindowEx_Button_MouseIn: return GetControlProgramExThis(EventControl)->OnWindowExEvent_Button_MouseIn((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WindowEx_Button_MouseOut: return GetControlProgramExThis(EventControl)->OnWindowExEvent_Button_MouseOut((WindowEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----滤镜EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_FilterEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnFilterExEvent_L_Click((FilterEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnFilterExEvent_R_Click((FilterEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnFilterExEvent_L_Down((FilterEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnFilterExEvent_R_Down((FilterEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnFilterExEvent_L_Up((FilterEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnFilterExEvent_R_Up((FilterEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnFilterExEvent_L_DClick((FilterEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnFilterExEvent_R_DClick((FilterEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnFilterExEvent_MouseMove((FilterEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnFilterExEvent_MouseEnterLeave((FilterEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnFilterExEvent_MouseWheel((FilterEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnFilterExEvent_keyEvent((FilterEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnFilterExEvent_CharInput((FilterEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnFilterExEvent_Focus((FilterEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnFilterExEvent_Other((FilterEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----分组框EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_MinutesboxEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnMinutesboxExEvent_L_Click((MinutesboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnMinutesboxExEvent_R_Click((MinutesboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnMinutesboxExEvent_L_Down((MinutesboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnMinutesboxExEvent_R_Down((MinutesboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnMinutesboxExEvent_L_Up((MinutesboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnMinutesboxExEvent_R_Up((MinutesboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnMinutesboxExEvent_L_DClick((MinutesboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnMinutesboxExEvent_R_DClick((MinutesboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnMinutesboxExEvent_MouseMove((MinutesboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnMinutesboxExEvent_MouseEnterLeave((MinutesboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnMinutesboxExEvent_MouseWheel((MinutesboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnMinutesboxExEvent_keyEvent((MinutesboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnMinutesboxExEvent_CharInput((MinutesboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnMinutesboxExEvent_Focus((MinutesboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnMinutesboxExEvent_Other((MinutesboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----选择夹EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_SelectthefolderEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_L_Click((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_R_Click((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_L_Down((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_R_Down((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_L_Up((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_R_Up((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_L_DClick((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_R_DClick((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_MouseMove((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_MouseEnterLeave((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_MouseWheel((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_keyEvent((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_CharInput((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_Focus((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_Other((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SelectthefolderEx_Tab_WillChange: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_Tab_WillChange((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SelectthefolderEx_Tab_Changed: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_Tab_Changed((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SelectthefolderEx_Tab_L_Click: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_Tab_L_Click((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SelectthefolderEx_Tab_R_Click: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_Tab_R_Click((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SelectthefolderEx_Tab_L_Up: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_Tab_L_Up((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SelectthefolderEx_Tab_R_Up: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_Tab_R_Up((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SelectthefolderEx_Tab_L_Down: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_Tab_L_Down((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SelectthefolderEx_Tab_R_Down: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_Tab_R_Down((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SelectthefolderEx_Tab_L_DClick: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_Tab_L_DClick((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SelectthefolderEx_Tab_R_DClick: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_Tab_R_DClick((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SelectthefolderEx_Tab_MouseIn: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_Tab_MouseIn((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SelectthefolderEx_Tab_MouseOut: return GetControlProgramExThis(EventControl)->OnSelectthefolderExEvent_Tab_MouseOut((SelectthefolderEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----图片框EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_PictureBoxEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnPictureBoxExEvent_L_Click((PictureBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnPictureBoxExEvent_R_Click((PictureBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnPictureBoxExEvent_L_Down((PictureBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnPictureBoxExEvent_R_Down((PictureBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnPictureBoxExEvent_L_Up((PictureBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnPictureBoxExEvent_R_Up((PictureBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnPictureBoxExEvent_L_DClick((PictureBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnPictureBoxExEvent_R_DClick((PictureBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnPictureBoxExEvent_MouseMove((PictureBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnPictureBoxExEvent_MouseEnterLeave((PictureBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnPictureBoxExEvent_MouseWheel((PictureBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnPictureBoxExEvent_keyEvent((PictureBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnPictureBoxExEvent_CharInput((PictureBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnPictureBoxExEvent_Focus((PictureBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnPictureBoxExEvent_Other((PictureBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----标签EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_LabelEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnLabelExEvent_L_Click((LabelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnLabelExEvent_R_Click((LabelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnLabelExEvent_L_Down((LabelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnLabelExEvent_R_Down((LabelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnLabelExEvent_L_Up((LabelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnLabelExEvent_R_Up((LabelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnLabelExEvent_L_DClick((LabelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnLabelExEvent_R_DClick((LabelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnLabelExEvent_MouseMove((LabelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnLabelExEvent_MouseEnterLeave((LabelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnLabelExEvent_MouseWheel((LabelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnLabelExEvent_keyEvent((LabelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnLabelExEvent_CharInput((LabelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnLabelExEvent_Focus((LabelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnLabelExEvent_Other((LabelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----按钮EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_ButtonEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnButtonExEvent_L_Click((ButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnButtonExEvent_R_Click((ButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnButtonExEvent_L_Down((ButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnButtonExEvent_R_Down((ButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnButtonExEvent_L_Up((ButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnButtonExEvent_R_Up((ButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnButtonExEvent_L_DClick((ButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnButtonExEvent_R_DClick((ButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnButtonExEvent_MouseMove((ButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnButtonExEvent_MouseEnterLeave((ButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnButtonExEvent_MouseWheel((ButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnButtonExEvent_keyEvent((ButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnButtonExEvent_CharInput((ButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnButtonExEvent_Focus((ButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnButtonExEvent_Other((ButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----图片按钮EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_ImagebuttonEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnImagebuttonExEvent_L_Click((ImagebuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnImagebuttonExEvent_R_Click((ImagebuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnImagebuttonExEvent_L_Down((ImagebuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnImagebuttonExEvent_R_Down((ImagebuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnImagebuttonExEvent_L_Up((ImagebuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnImagebuttonExEvent_R_Up((ImagebuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnImagebuttonExEvent_L_DClick((ImagebuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnImagebuttonExEvent_R_DClick((ImagebuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnImagebuttonExEvent_MouseMove((ImagebuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnImagebuttonExEvent_MouseEnterLeave((ImagebuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnImagebuttonExEvent_MouseWheel((ImagebuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnImagebuttonExEvent_keyEvent((ImagebuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnImagebuttonExEvent_CharInput((ImagebuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnImagebuttonExEvent_Focus((ImagebuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnImagebuttonExEvent_Other((ImagebuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----超级按钮EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_SuperbuttonEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnSuperbuttonExEvent_L_Click((SuperbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnSuperbuttonExEvent_R_Click((SuperbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnSuperbuttonExEvent_L_Down((SuperbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnSuperbuttonExEvent_R_Down((SuperbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnSuperbuttonExEvent_L_Up((SuperbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnSuperbuttonExEvent_R_Up((SuperbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnSuperbuttonExEvent_L_DClick((SuperbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnSuperbuttonExEvent_R_DClick((SuperbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnSuperbuttonExEvent_MouseMove((SuperbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnSuperbuttonExEvent_MouseEnterLeave((SuperbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnSuperbuttonExEvent_MouseWheel((SuperbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnSuperbuttonExEvent_keyEvent((SuperbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnSuperbuttonExEvent_CharInput((SuperbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnSuperbuttonExEvent_Focus((SuperbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnSuperbuttonExEvent_Other((SuperbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----多功能按钮EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_MultifunctionButtonEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_L_Click((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_R_Click((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_L_Down((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_R_Down((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_L_Up((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_R_Up((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_L_DClick((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_R_DClick((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_MouseMove((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_MouseEnterLeave((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_MouseWheel((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_keyEvent((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_CharInput((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_Focus((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_Other((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_MultifunctionButtonEx_Button_L_Click: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_Button_L_Click((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_MultifunctionButtonEx_Button_R_Click: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_Button_R_Click((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_MultifunctionButtonEx_Button_L_Up: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_Button_L_Up((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_MultifunctionButtonEx_Button_R_Up: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_Button_R_Up((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_MultifunctionButtonEx_Button_L_Down: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_Button_L_Down((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_MultifunctionButtonEx_Button_R_Down: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_Button_R_Down((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_MultifunctionButtonEx_Button_L_DClick: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_Button_L_DClick((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_MultifunctionButtonEx_Button_R_DClick: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_Button_R_DClick((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_MultifunctionButtonEx_Button_MouseIn: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_Button_MouseIn((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_MultifunctionButtonEx_Button_MouseOut: return GetControlProgramExThis(EventControl)->OnMultifunctionButtonExEvent_Button_MouseOut((MultifunctionButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----单选框EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_RadiobuttonEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnRadiobuttonExEvent_L_Click((RadiobuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnRadiobuttonExEvent_R_Click((RadiobuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnRadiobuttonExEvent_L_Down((RadiobuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnRadiobuttonExEvent_R_Down((RadiobuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnRadiobuttonExEvent_L_Up((RadiobuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnRadiobuttonExEvent_R_Up((RadiobuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnRadiobuttonExEvent_L_DClick((RadiobuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnRadiobuttonExEvent_R_DClick((RadiobuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnRadiobuttonExEvent_MouseMove((RadiobuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnRadiobuttonExEvent_MouseEnterLeave((RadiobuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnRadiobuttonExEvent_MouseWheel((RadiobuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnRadiobuttonExEvent_keyEvent((RadiobuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnRadiobuttonExEvent_CharInput((RadiobuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnRadiobuttonExEvent_Focus((RadiobuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnRadiobuttonExEvent_Other((RadiobuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_RadiobuttonEx_SelectChanged:    return GetControlProgramExThis(EventControl)->OnRadiobuttonExEvent_SelectChanged((RadiobuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----选择框EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_ChoiceboxEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnChoiceboxExEvent_L_Click((ChoiceboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnChoiceboxExEvent_R_Click((ChoiceboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnChoiceboxExEvent_L_Down((ChoiceboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnChoiceboxExEvent_R_Down((ChoiceboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnChoiceboxExEvent_L_Up((ChoiceboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnChoiceboxExEvent_R_Up((ChoiceboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnChoiceboxExEvent_L_DClick((ChoiceboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnChoiceboxExEvent_R_DClick((ChoiceboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnChoiceboxExEvent_MouseMove((ChoiceboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnChoiceboxExEvent_MouseEnterLeave((ChoiceboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnChoiceboxExEvent_MouseWheel((ChoiceboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnChoiceboxExEvent_keyEvent((ChoiceboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnChoiceboxExEvent_CharInput((ChoiceboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnChoiceboxExEvent_Focus((ChoiceboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnChoiceboxExEvent_Other((ChoiceboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ChoiceboxEx_SelectChanged:    return GetControlProgramExThis(EventControl)->OnChoiceboxExEvent_SelectChanged((ChoiceboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----进度条EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_ProgressbarEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnProgressbarExEvent_L_Click((ProgressbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnProgressbarExEvent_R_Click((ProgressbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnProgressbarExEvent_L_Down((ProgressbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnProgressbarExEvent_R_Down((ProgressbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnProgressbarExEvent_L_Up((ProgressbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnProgressbarExEvent_R_Up((ProgressbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnProgressbarExEvent_L_DClick((ProgressbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnProgressbarExEvent_R_DClick((ProgressbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnProgressbarExEvent_MouseMove((ProgressbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnProgressbarExEvent_MouseEnterLeave((ProgressbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnProgressbarExEvent_MouseWheel((ProgressbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnProgressbarExEvent_keyEvent((ProgressbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnProgressbarExEvent_CharInput((ProgressbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnProgressbarExEvent_Focus((ProgressbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnProgressbarExEvent_Other((ProgressbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----滑块条EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_SliderbarEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnSliderbarExEvent_L_Click((SliderbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnSliderbarExEvent_R_Click((SliderbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnSliderbarExEvent_L_Down((SliderbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnSliderbarExEvent_R_Down((SliderbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnSliderbarExEvent_L_Up((SliderbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnSliderbarExEvent_R_Up((SliderbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnSliderbarExEvent_L_DClick((SliderbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnSliderbarExEvent_R_DClick((SliderbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnSliderbarExEvent_MouseMove((SliderbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnSliderbarExEvent_MouseEnterLeave((SliderbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnSliderbarExEvent_MouseWheel((SliderbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnSliderbarExEvent_keyEvent((SliderbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnSliderbarExEvent_CharInput((SliderbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnSliderbarExEvent_Focus((SliderbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnSliderbarExEvent_Other((SliderbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SliderbarEx_PositionChanged: return GetControlProgramExThis(EventControl)->OnSliderbarExEvent_PositionChanged((SliderbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----滚动条EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_ScrollbarEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnScrollbarExEvent_L_Click((ScrollbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnScrollbarExEvent_R_Click((ScrollbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnScrollbarExEvent_L_Down((ScrollbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnScrollbarExEvent_R_Down((ScrollbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnScrollbarExEvent_L_Up((ScrollbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnScrollbarExEvent_R_Up((ScrollbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnScrollbarExEvent_L_DClick((ScrollbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnScrollbarExEvent_R_DClick((ScrollbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnScrollbarExEvent_MouseMove((ScrollbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnScrollbarExEvent_MouseEnterLeave((ScrollbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnScrollbarExEvent_MouseWheel((ScrollbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnScrollbarExEvent_keyEvent((ScrollbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnScrollbarExEvent_CharInput((ScrollbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnScrollbarExEvent_Focus((ScrollbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnScrollbarExEvent_Other((ScrollbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ScrollbarEx_PositionChanged: return GetControlProgramExThis(EventControl)->OnScrollbarExEvent_PositionChanged((ScrollbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----编辑框EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_EditboxEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnEditboxExEvent_L_Click((EditboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnEditboxExEvent_R_Click((EditboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnEditboxExEvent_L_Down((EditboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnEditboxExEvent_R_Down((EditboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnEditboxExEvent_L_Up((EditboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnEditboxExEvent_R_Up((EditboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnEditboxExEvent_L_DClick((EditboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnEditboxExEvent_R_DClick((EditboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnEditboxExEvent_MouseMove((EditboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnEditboxExEvent_MouseEnterLeave((EditboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnEditboxExEvent_MouseWheel((EditboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnEditboxExEvent_keyEvent((EditboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnEditboxExEvent_CharInput((EditboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnEditboxExEvent_Focus((EditboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnEditboxExEvent_Other((EditboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_EditboxEx_ContentChanged: return GetControlProgramExThis(EventControl)->OnEditboxExEvent_ContentChanged((EditboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----组合框EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_ComboboxEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnComboboxExEvent_L_Click((ComboboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnComboboxExEvent_R_Click((ComboboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnComboboxExEvent_L_Down((ComboboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnComboboxExEvent_R_Down((ComboboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnComboboxExEvent_L_Up((ComboboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnComboboxExEvent_R_Up((ComboboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnComboboxExEvent_L_DClick((ComboboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnComboboxExEvent_R_DClick((ComboboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnComboboxExEvent_MouseMove((ComboboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnComboboxExEvent_MouseEnterLeave((ComboboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnComboboxExEvent_MouseWheel((ComboboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnComboboxExEvent_keyEvent((ComboboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnComboboxExEvent_CharInput((ComboboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnComboboxExEvent_Focus((ComboboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnComboboxExEvent_Other((ComboboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ComboboxEx_ContentChanged: return GetControlProgramExThis(EventControl)->OnComboboxExEvent_ContentChanged((ComboboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ComboboxEx_CurrentItemWillChange: return GetControlProgramExThis(EventControl)->OnComboboxExEvent_CurrentItemWillChange((ComboboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ComboboxEx_CurrentItemChanged: return GetControlProgramExThis(EventControl)->OnComboboxExEvent_CurrentItemChanged((ComboboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----工具条EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_ToolbarEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_L_Click((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_R_Click((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_L_Down((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_R_Down((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_L_Up((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_R_Up((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_L_DClick((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_R_DClick((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_MouseMove((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_MouseEnterLeave((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_MouseWheel((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_keyEvent((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_CharInput((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_Focus((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_Other((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ToolbarEx_Button_L_Click: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_Button_L_Click((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ToolbarEx_Button_R_Click: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_Button_R_Click((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ToolbarEx_Button_L_Up: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_Button_L_Up((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ToolbarEx_Button_R_Up: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_Button_R_Up((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ToolbarEx_Button_L_Down: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_Button_L_Down((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ToolbarEx_Button_R_Down: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_Button_R_Down((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ToolbarEx_Button_L_DClick: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_Button_L_DClick((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ToolbarEx_Button_R_DClick: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_Button_R_DClick((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ToolbarEx_Button_MouseIn: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_Button_MouseIn((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ToolbarEx_Button_MouseOut: return GetControlProgramExThis(EventControl)->OnToolbarExEvent_Button_MouseOut((ToolbarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----列表框EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_ListboxEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnListboxExEvent_L_Click((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnListboxExEvent_R_Click((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnListboxExEvent_L_Down((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnListboxExEvent_R_Down((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnListboxExEvent_L_Up((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnListboxExEvent_R_Up((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnListboxExEvent_L_DClick((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnListboxExEvent_R_DClick((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnListboxExEvent_MouseMove((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnListboxExEvent_MouseEnterLeave((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnListboxExEvent_MouseWheel((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnListboxExEvent_keyEvent((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnListboxExEvent_CharInput((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnListboxExEvent_Focus((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnListboxExEvent_Other((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ListboxEx_Item_L_Click: return GetControlProgramExThis(EventControl)->OnListboxExEvent_Item_L_Click((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ListboxEx_Item_R_Click: return GetControlProgramExThis(EventControl)->OnListboxExEvent_Item_R_Click((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ListboxEx_Item_L_Up: return GetControlProgramExThis(EventControl)->OnListboxExEvent_Item_L_Up((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ListboxEx_Item_R_Up: return GetControlProgramExThis(EventControl)->OnListboxExEvent_Item_R_Up((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ListboxEx_Item_L_Down: return GetControlProgramExThis(EventControl)->OnListboxExEvent_Item_L_Down((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ListboxEx_Item_R_Down: return GetControlProgramExThis(EventControl)->OnListboxExEvent_Item_R_Down((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ListboxEx_Item_L_DClick: return GetControlProgramExThis(EventControl)->OnListboxExEvent_Item_L_DClick((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ListboxEx_Item_R_DClick: return GetControlProgramExThis(EventControl)->OnListboxExEvent_Item_R_DClick((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ListboxEx_Item_MouseIn: return GetControlProgramExThis(EventControl)->OnListboxExEvent_Item_MouseIn((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ListboxEx_Item_MouseOut: return GetControlProgramExThis(EventControl)->OnListboxExEvent_Item_MouseOut((ListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----图标列表框EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_IcoListboxEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_L_Click((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_R_Click((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_L_Down((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_R_Down((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_L_Up((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_R_Up((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_L_DClick((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_R_DClick((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_MouseMove((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_MouseEnterLeave((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_MouseWheel((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_keyEvent((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_CharInput((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_Focus((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_Other((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_IcoListboxEx_Item_L_Click: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_Item_L_Click((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_IcoListboxEx_Item_R_Click: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_Item_R_Click((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_IcoListboxEx_Item_L_Up: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_Item_L_Up((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_IcoListboxEx_Item_R_Up: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_Item_R_Up((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_IcoListboxEx_Item_L_Down: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_Item_L_Down((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_IcoListboxEx_Item_R_Down: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_Item_R_Down((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_IcoListboxEx_Item_L_DClick: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_Item_L_DClick((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_IcoListboxEx_Item_R_DClick: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_Item_R_DClick((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_IcoListboxEx_Item_MouseIn: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_Item_MouseIn((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_IcoListboxEx_Item_MouseOut: return GetControlProgramExThis(EventControl)->OnIcoListboxExEvent_Item_MouseOut((IcoListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----超级列表框EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_SuperListboxEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_L_Click((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_R_Click((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_L_Down((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_R_Down((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_L_Up((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_R_Up((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_L_DClick((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_R_DClick((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_MouseMove((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_MouseEnterLeave((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_MouseWheel((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_keyEvent((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_CharInput((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_Focus((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_Other((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SuperListboxEx_Item_L_Click: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_Item_L_Click((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SuperListboxEx_Item_R_Click: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_Item_R_Click((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SuperListboxEx_Item_L_Up: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_Item_L_Up((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SuperListboxEx_Item_R_Up: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_Item_R_Up((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SuperListboxEx_Item_L_Down: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_Item_L_Down((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SuperListboxEx_Item_R_Down: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_Item_R_Down((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SuperListboxEx_Item_L_DClick: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_Item_L_DClick((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SuperListboxEx_Item_R_DClick: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_Item_R_DClick((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SuperListboxEx_Item_MouseIn: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_Item_MouseIn((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SuperListboxEx_Item_MouseOut: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_Item_MouseOut((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SuperListboxEx_Item_WillEnterEdit: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_Item_WillEnterEdit((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SuperListboxEx_Item_WillExitEdit: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_Item_WillExitEdit((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SuperListboxEx_Item_EnterEdit: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_Item_EnterEdit((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SuperListboxEx_Item_ExitEdit: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_Item_ExitEdit((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SuperListboxEx_Item_SizeChanged: return GetControlProgramExThis(EventControl)->OnSuperListboxExEvent_Item_SizeChanged((SuperListboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----树形列表框EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_TreeListEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_L_Click((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_R_Click((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_L_Down((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_R_Down((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_L_Up((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_R_Up((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_L_DClick((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_R_DClick((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_MouseMove((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_MouseEnterLeave((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_MouseWheel((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_keyEvent((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_CharInput((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_Focus((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_Other((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_TreeListEx_Item_L_Click: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_Item_L_Click((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_TreeListEx_Item_R_Click: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_Item_R_Click((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_TreeListEx_Item_L_Up: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_Item_L_Up((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_TreeListEx_Item_R_Up: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_Item_R_Up((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_TreeListEx_Item_L_Down: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_Item_L_Down((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_TreeListEx_Item_R_Down: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_Item_R_Down((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_TreeListEx_Item_L_DClick: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_Item_L_DClick((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_TreeListEx_Item_R_DClick: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_Item_R_DClick((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_TreeListEx_Item_MouseIn: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_Item_MouseIn((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_TreeListEx_Item_MouseOut: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_Item_MouseOut((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_TreeListEx_Item_FoldChanged: return GetControlProgramExThis(EventControl)->OnTreeListExEvent_Item_FoldChanged((TreeListEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----浏览框EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_WebBrowserEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_L_Click((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_R_Click((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_L_Down((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_R_Down((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_L_Up((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_R_Up((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_L_DClick((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_R_DClick((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_MouseMove((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_MouseEnterLeave((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_MouseWheel((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_keyEvent((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_CharInput((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_Focus((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_Other((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WebBrowserEx_Navigation: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_Navigation((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WebBrowserEx_CreateView: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_CreateView((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WebBrowserEx_URLChanged: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_URLChanged((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WebBrowserEx_TitleChanged: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_TitleChanged((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WebBrowserEx_DocumentReady: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_DocumentReady((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WebBrowserEx_LoadingFinish: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_LoadingFinish((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WebBrowserEx_Download: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_Download((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WebBrowserEx_AlertBox: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_AlertBox((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WebBrowserEx_ConfirmBox: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_ConfirmBox((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WebBrowserEx_PromptBox: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_PromptBox((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WebBrowserEx_LoadUrlBegin: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_LoadUrlBegin((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WebBrowserEx_LoadUrlFail: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_LoadUrlFail((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WebBrowserEx_LoadUrlEnd: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_LoadUrlEnd((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WebBrowserEx_NetResponse: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_NetResponse((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WebBrowserEx_JsCallResponse: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_JsCallResponse((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WebBrowserEx_Reserve1: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_Reserve1((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_WebBrowserEx_Reserve2: return GetControlProgramExThis(EventControl)->OnWebBrowserExEvent_Reserve2((WebBrowserEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;

		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}

	/*----日历框EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_CalendarBoxEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_L_Click((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_R_Click((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_L_Down((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_R_Down((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_L_Up((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_R_Up((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_L_DClick((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_R_DClick((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_MouseMove((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_MouseEnterLeave((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_MouseWheel((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_keyEvent((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_CharInput((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_Focus((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_Other((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_CalendarBoxEx_Item_L_Click: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_Item_L_Click((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_CalendarBoxEx_Item_R_Click: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_Item_R_Click((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_CalendarBoxEx_Item_L_Up: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_Item_L_Up((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_CalendarBoxEx_Item_R_Up: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_Item_R_Up((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_CalendarBoxEx_Item_L_Down: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_Item_L_Down((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_CalendarBoxEx_Item_R_Down: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_Item_R_Down((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_CalendarBoxEx_Item_L_DClick: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_Item_L_DClick((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_CalendarBoxEx_Item_R_DClick: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_Item_R_DClick((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_CalendarBoxEx_Item_MouseIn: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_Item_MouseIn((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_CalendarBoxEx_Item_MouseOut: return GetControlProgramExThis(EventControl)->OnCalendarBoxExEvent_Item_MouseOut((CalendarBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----选色板EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_ColorPickEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnColorPickExEvent_L_Click((ColorPickEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnColorPickExEvent_R_Click((ColorPickEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnColorPickExEvent_L_Down((ColorPickEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnColorPickExEvent_R_Down((ColorPickEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnColorPickExEvent_L_Up((ColorPickEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnColorPickExEvent_R_Up((ColorPickEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnColorPickExEvent_L_DClick((ColorPickEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnColorPickExEvent_R_DClick((ColorPickEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnColorPickExEvent_MouseMove((ColorPickEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnColorPickExEvent_MouseEnterLeave((ColorPickEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnColorPickExEvent_MouseWheel((ColorPickEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnColorPickExEvent_keyEvent((ColorPickEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnColorPickExEvent_CharInput((ColorPickEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnColorPickExEvent_Focus((ColorPickEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnColorPickExEvent_Other((ColorPickEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ColorPickEx_ColorChanged: return GetControlProgramExThis(EventControl)->OnColorPickExEvent_ColorChanged((ColorPickEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----富文本框EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_RichEditEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnRichEditExEvent_L_Click((RichEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnRichEditExEvent_R_Click((RichEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnRichEditExEvent_L_Down((RichEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnRichEditExEvent_R_Down((RichEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnRichEditExEvent_L_Up((RichEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnRichEditExEvent_R_Up((RichEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnRichEditExEvent_L_DClick((RichEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnRichEditExEvent_R_DClick((RichEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnRichEditExEvent_MouseMove((RichEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnRichEditExEvent_MouseEnterLeave((RichEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnRichEditExEvent_MouseWheel((RichEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnRichEditExEvent_keyEvent((RichEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnRichEditExEvent_CharInput((RichEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnRichEditExEvent_Focus((RichEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnRichEditExEvent_Other((RichEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_RichEditEx_ContentChanged: return GetControlProgramExThis(EventControl)->OnRichEditExEvent_ContentChanged((RichEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_RichEditEx_SelectionChanged: return GetControlProgramExThis(EventControl)->OnRichEditExEvent_SelectionChanged((RichEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_RichEditEx_OtherExtendEvent: return GetControlProgramExThis(EventControl)->OnRichEditExEvent_OtherExtendEvent((RichEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----扩展组件EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_ExtendEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnExtendExEvent_L_Click((ExtendEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnExtendExEvent_R_Click((ExtendEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnExtendExEvent_L_Down((ExtendEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnExtendExEvent_R_Down((ExtendEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnExtendExEvent_L_Up((ExtendEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnExtendExEvent_R_Up((ExtendEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnExtendExEvent_L_DClick((ExtendEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnExtendExEvent_R_DClick((ExtendEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnExtendExEvent_MouseMove((ExtendEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnExtendExEvent_MouseEnterLeave((ExtendEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnExtendExEvent_MouseWheel((ExtendEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnExtendExEvent_keyEvent((ExtendEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnExtendExEvent_CharInput((ExtendEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnExtendExEvent_Focus((ExtendEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnExtendExEvent_Other((ExtendEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ExtendEx_ExtendEvent: return GetControlProgramExThis(EventControl)->OnExtendExEvent_ExtendEvent((ExtendEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----动画按钮EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_AnimationbuttonEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnAnimationbuttonExEvent_L_Click((AnimationbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnAnimationbuttonExEvent_R_Click((AnimationbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnAnimationbuttonExEvent_L_Down((AnimationbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnAnimationbuttonExEvent_R_Down((AnimationbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnAnimationbuttonExEvent_L_Up((AnimationbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnAnimationbuttonExEvent_R_Up((AnimationbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnAnimationbuttonExEvent_L_DClick((AnimationbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnAnimationbuttonExEvent_R_DClick((AnimationbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnAnimationbuttonExEvent_MouseMove((AnimationbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnAnimationbuttonExEvent_MouseEnterLeave((AnimationbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnAnimationbuttonExEvent_MouseWheel((AnimationbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnAnimationbuttonExEvent_keyEvent((AnimationbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnAnimationbuttonExEvent_CharInput((AnimationbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnAnimationbuttonExEvent_Focus((AnimationbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnAnimationbuttonExEvent_Other((AnimationbuttonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----高级表格EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_AdvancedFormEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_L_Click((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_R_Click((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_L_Down((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_R_Down((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_L_Up((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_R_Up((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_L_DClick((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_R_DClick((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_MouseMove((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_MouseEnterLeave((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_MouseWheel((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_keyEvent((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_CharInput((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_Focus((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_Other((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_AdvancedFormEx_Item_L_Click: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_Item_L_Click((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_AdvancedFormEx_Item_R_Click: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_Item_R_Click((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_AdvancedFormEx_Item_L_Up: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_Item_L_Up((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_AdvancedFormEx_Item_R_Up: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_Item_R_Up((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_AdvancedFormEx_Item_L_Down: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_Item_L_Down((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_AdvancedFormEx_Item_R_Down: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_Item_R_Down((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_AdvancedFormEx_Item_L_DClick: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_Item_L_DClick((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_AdvancedFormEx_Item_R_DClick: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_Item_R_DClick((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_AdvancedFormEx_Item_MouseIn: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_Item_MouseIn((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_AdvancedFormEx_Item_MouseOut: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_Item_MouseOut((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_AdvancedFormEx_Item_WillEnterEdit: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_Item_WillEnterEdit((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_AdvancedFormEx_Item_WillExitEdit: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_Item_WillExitEdit((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_AdvancedFormEx_Item_EnterEdit: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_Item_EnterEdit((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_AdvancedFormEx_Item_ExitEdit: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_Item_ExitEdit((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_AdvancedFormEx_Item_SizeChanged: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_Item_SizeChanged((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_AdvancedFormEx_Item_FoldChanged: return GetControlProgramExThis(EventControl)->OnAdvancedFormExEvent_Item_FoldChanged((AdvancedFormEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----滑动按钮EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_SlideButtonEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnSlideButtonExEvent_L_Click((SlideButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnSlideButtonExEvent_R_Click((SlideButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnSlideButtonExEvent_L_Down((SlideButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnSlideButtonExEvent_R_Down((SlideButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnSlideButtonExEvent_L_Up((SlideButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnSlideButtonExEvent_R_Up((SlideButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnSlideButtonExEvent_L_DClick((SlideButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnSlideButtonExEvent_R_DClick((SlideButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnSlideButtonExEvent_MouseMove((SlideButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnSlideButtonExEvent_MouseEnterLeave((SlideButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnSlideButtonExEvent_MouseWheel((SlideButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnSlideButtonExEvent_keyEvent((SlideButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnSlideButtonExEvent_CharInput((SlideButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnSlideButtonExEvent_Focus((SlideButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnSlideButtonExEvent_Other((SlideButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SlideButtonEx_SelectChanged: return GetControlProgramExThis(EventControl)->OnSlideButtonExEvent_SelectChanged((SlideButtonEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----饼形图EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_PieChartEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnPieChartExEvent_L_Click((PieChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnPieChartExEvent_R_Click((PieChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnPieChartExEvent_L_Down((PieChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnPieChartExEvent_R_Down((PieChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnPieChartExEvent_L_Up((PieChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnPieChartExEvent_R_Up((PieChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnPieChartExEvent_L_DClick((PieChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnPieChartExEvent_R_DClick((PieChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnPieChartExEvent_MouseMove((PieChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnPieChartExEvent_MouseEnterLeave((PieChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnPieChartExEvent_MouseWheel((PieChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnPieChartExEvent_keyEvent((PieChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnPieChartExEvent_CharInput((PieChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnPieChartExEvent_Focus((PieChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnPieChartExEvent_Other((PieChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_PieChartEx_ChartEvent: return GetControlProgramExThis(EventControl)->OnPieChartExEvent_ChartEvent((PieChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----柱状图EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_BarChartEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnBarChartExEvent_L_Click((BarChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnBarChartExEvent_R_Click((BarChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnBarChartExEvent_L_Down((BarChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnBarChartExEvent_R_Down((BarChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnBarChartExEvent_L_Up((BarChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnBarChartExEvent_R_Up((BarChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnBarChartExEvent_L_DClick((BarChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnBarChartExEvent_R_DClick((BarChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnBarChartExEvent_MouseMove((BarChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnBarChartExEvent_MouseEnterLeave((BarChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnBarChartExEvent_MouseWheel((BarChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnBarChartExEvent_keyEvent((BarChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnBarChartExEvent_CharInput((BarChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnBarChartExEvent_Focus((BarChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnBarChartExEvent_Other((BarChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_BarChartEx_ChartEvent: return GetControlProgramExThis(EventControl)->OnBarChartExEvent_ChartEvent((BarChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----曲线图Ex组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_CurveChartEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnCurveChartExEvent_L_Click((CurveChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnCurveChartExEvent_R_Click((CurveChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnCurveChartExEvent_L_Down((CurveChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnCurveChartExEvent_R_Down((CurveChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnCurveChartExEvent_L_Up((CurveChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnCurveChartExEvent_R_Up((CurveChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnCurveChartExEvent_L_DClick((CurveChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnCurveChartExEvent_R_DClick((CurveChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnCurveChartExEvent_MouseMove((CurveChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnCurveChartExEvent_MouseEnterLeave((CurveChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnCurveChartExEvent_MouseWheel((CurveChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnCurveChartExEvent_keyEvent((CurveChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnCurveChartExEvent_CharInput((CurveChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnCurveChartExEvent_Focus((CurveChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnCurveChartExEvent_Other((CurveChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_CurveChartEx_ChartEvent: return GetControlProgramExThis(EventControl)->OnCurveChartExEvent_ChartEvent((CurveChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----烛线图Ex组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_CandleChartEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnCandleChartExEvent_L_Click((CandleChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnCandleChartExEvent_R_Click((CandleChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnCandleChartExEvent_L_Down((CandleChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnCandleChartExEvent_R_Down((CandleChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnCandleChartExEvent_L_Up((CandleChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnCandleChartExEvent_R_Up((CandleChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnCandleChartExEvent_L_DClick((CandleChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnCandleChartExEvent_R_DClick((CandleChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnCandleChartExEvent_MouseMove((CandleChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnCandleChartExEvent_MouseEnterLeave((CandleChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnCandleChartExEvent_MouseWheel((CandleChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnCandleChartExEvent_keyEvent((CandleChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnCandleChartExEvent_CharInput((CandleChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnCandleChartExEvent_Focus((CandleChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnCandleChartExEvent_Other((CandleChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_CandleChartEx_ChartEvent: return GetControlProgramExThis(EventControl)->OnCandleChartExEvent_ChartEvent((CandleChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----画板EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_DrawPanelEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnDrawPanelExEvent_L_Click((DrawPanelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnDrawPanelExEvent_R_Click((DrawPanelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnDrawPanelExEvent_L_Down((DrawPanelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnDrawPanelExEvent_R_Down((DrawPanelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnDrawPanelExEvent_L_Up((DrawPanelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnDrawPanelExEvent_R_Up((DrawPanelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnDrawPanelExEvent_L_DClick((DrawPanelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnDrawPanelExEvent_R_DClick((DrawPanelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnDrawPanelExEvent_MouseMove((DrawPanelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnDrawPanelExEvent_MouseEnterLeave((DrawPanelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnDrawPanelExEvent_MouseWheel((DrawPanelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnDrawPanelExEvent_keyEvent((DrawPanelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnDrawPanelExEvent_CharInput((DrawPanelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnDrawPanelExEvent_Focus((DrawPanelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnDrawPanelExEvent_Other((DrawPanelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_DrawPanelEx_DrawEvent: return GetControlProgramExThis(EventControl)->OnDrawPanelExEvent_DrawEvent((DrawPanelEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----滚动布局框EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_ScrollLayoutBoxEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnScrollLayoutBoxExEvent_L_Click((ScrollLayoutBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnScrollLayoutBoxExEvent_R_Click((ScrollLayoutBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnScrollLayoutBoxExEvent_L_Down((ScrollLayoutBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnScrollLayoutBoxExEvent_R_Down((ScrollLayoutBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnScrollLayoutBoxExEvent_L_Up((ScrollLayoutBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnScrollLayoutBoxExEvent_R_Up((ScrollLayoutBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnScrollLayoutBoxExEvent_L_DClick((ScrollLayoutBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnScrollLayoutBoxExEvent_R_DClick((ScrollLayoutBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnScrollLayoutBoxExEvent_MouseMove((ScrollLayoutBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnScrollLayoutBoxExEvent_MouseEnterLeave((ScrollLayoutBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnScrollLayoutBoxExEvent_MouseWheel((ScrollLayoutBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnScrollLayoutBoxExEvent_keyEvent((ScrollLayoutBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnScrollLayoutBoxExEvent_CharInput((ScrollLayoutBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnScrollLayoutBoxExEvent_Focus((ScrollLayoutBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnScrollLayoutBoxExEvent_Other((ScrollLayoutBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----影像框EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_MediaboxEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_L_Click((MediaboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_R_Click((MediaboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_L_Down((MediaboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_R_Down((MediaboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_L_Up((MediaboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_R_Up((MediaboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_L_DClick((MediaboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_R_DClick((MediaboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_MouseMove((MediaboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_MouseEnterLeave((MediaboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_MouseWheel((MediaboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_keyEvent((MediaboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_CharInput((MediaboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_Focus((MediaboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_Other((MediaboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_MediaboxEx_PlayPositionChanged: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_PlayPositionChanged((MediaboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_MediaboxEx_RelatedEvent1: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_RelatedEvent1((MediaboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_MediaboxEx_RelatedEvent2: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_RelatedEvent2((MediaboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_MediaboxEx_RelatedEvent3: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_RelatedEvent3((MediaboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_MediaboxEx_RelatedEvent4: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_RelatedEvent4((MediaboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_MediaboxEx_RelatedEvent5: return GetControlProgramExThis(EventControl)->OnMediaboxExEvent_RelatedEvent5((MediaboxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----轮播框EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_CarouselBoxEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_L_Click((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_R_Click((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_L_Down((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_R_Down((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_L_Up((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_R_Up((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_L_DClick((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_R_DClick((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_MouseMove((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_MouseEnterLeave((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_MouseWheel((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_keyEvent((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_CharInput((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_Focus((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_Other((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_CarouselBoxEx_Item_L_Click: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_Item_L_Click((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_CarouselBoxEx_Item_R_Click: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_Item_R_Click((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_CarouselBoxEx_Item_L_Up: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_Item_L_Up((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_CarouselBoxEx_Item_R_Up: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_Item_R_Up((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_CarouselBoxEx_Item_L_Down: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_Item_L_Down((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_CarouselBoxEx_Item_R_Down: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_Item_R_Down((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_CarouselBoxEx_Item_L_DClick: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_Item_L_DClick((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_CarouselBoxEx_Item_R_DClick: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_Item_R_DClick((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_CarouselBoxEx_Item_MouseIn: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_Item_MouseIn((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_CarouselBoxEx_Item_MouseOut: return GetControlProgramExThis(EventControl)->OnCarouselBoxExEvent_Item_MouseOut((CarouselBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----超级编辑框EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_SuperEditEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnSuperEditExEvent_L_Click((SuperEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnSuperEditExEvent_R_Click((SuperEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnSuperEditExEvent_L_Down((SuperEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnSuperEditExEvent_R_Down((SuperEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnSuperEditExEvent_L_Up((SuperEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnSuperEditExEvent_R_Up((SuperEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnSuperEditExEvent_L_DClick((SuperEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnSuperEditExEvent_R_DClick((SuperEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnSuperEditExEvent_MouseMove((SuperEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnSuperEditExEvent_MouseEnterLeave((SuperEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnSuperEditExEvent_MouseWheel((SuperEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnSuperEditExEvent_keyEvent((SuperEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnSuperEditExEvent_CharInput((SuperEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnSuperEditExEvent_Focus((SuperEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnSuperEditExEvent_Other((SuperEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SuperEditEx_EventNotification: return GetControlProgramExThis(EventControl)->OnSuperEditExEvent_EventNotification((SuperEditEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----聊天框EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_ChatBoxEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnChatBoxExEvent_L_Click((ChatBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnChatBoxExEvent_R_Click((ChatBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnChatBoxExEvent_L_Down((ChatBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnChatBoxExEvent_R_Down((ChatBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnChatBoxExEvent_L_Up((ChatBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnChatBoxExEvent_R_Up((ChatBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnChatBoxExEvent_L_DClick((ChatBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnChatBoxExEvent_R_DClick((ChatBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnChatBoxExEvent_MouseMove((ChatBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnChatBoxExEvent_MouseEnterLeave((ChatBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnChatBoxExEvent_MouseWheel((ChatBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnChatBoxExEvent_keyEvent((ChatBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnChatBoxExEvent_CharInput((ChatBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnChatBoxExEvent_Focus((ChatBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnChatBoxExEvent_Other((ChatBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_ChatBoxEx_EventNotification: return GetControlProgramExThis(EventControl)->OnChatBoxExEvent_EventNotification((ChatBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----超级图表框EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_SuperChartEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnSuperChartExEvent_L_Click((SuperChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnSuperChartExEvent_R_Click((SuperChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnSuperChartExEvent_L_Down((SuperChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnSuperChartExEvent_R_Down((SuperChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnSuperChartExEvent_L_Up((SuperChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnSuperChartExEvent_R_Up((SuperChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnSuperChartExEvent_L_DClick((SuperChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnSuperChartExEvent_R_DClick((SuperChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnSuperChartExEvent_MouseMove((SuperChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnSuperChartExEvent_MouseEnterLeave((SuperChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnSuperChartExEvent_MouseWheel((SuperChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnSuperChartExEvent_keyEvent((SuperChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnSuperChartExEvent_CharInput((SuperChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnSuperChartExEvent_Focus((SuperChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnSuperChartExEvent_Other((SuperChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_SuperChartEx_EventNotification: return GetControlProgramExThis(EventControl)->OnSuperChartExEvent_EventNotification((SuperChartEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----数据报表框EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_DataReportBoxEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnDataReportBoxExEvent_L_Click((DataReportBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnDataReportBoxExEvent_R_Click((DataReportBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnDataReportBoxExEvent_L_Down((DataReportBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnDataReportBoxExEvent_R_Down((DataReportBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnDataReportBoxExEvent_L_Up((DataReportBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnDataReportBoxExEvent_R_Up((DataReportBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnDataReportBoxExEvent_L_DClick((DataReportBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnDataReportBoxExEvent_R_DClick((DataReportBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnDataReportBoxExEvent_MouseMove((DataReportBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnDataReportBoxExEvent_MouseEnterLeave((DataReportBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnDataReportBoxExEvent_MouseWheel((DataReportBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnDataReportBoxExEvent_keyEvent((DataReportBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnDataReportBoxExEvent_CharInput((DataReportBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnDataReportBoxExEvent_Focus((DataReportBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnDataReportBoxExEvent_Other((DataReportBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_DataReportBoxEx_EventNotification: return GetControlProgramExThis(EventControl)->OnDataReportBoxExEvent_EventNotification((DataReportBoxEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}
	/*----分页导航条EX组件分发----*/
	static AutoInt CALLBACK Exui_EventPro_PageNavigBarEx(Control_P EventControl, AutoInt EventMsg, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		switch (EventMsg)
		{
		case Event_Control_L_Click: return GetControlProgramExThis(EventControl)->OnPageNavigBarExEvent_L_Click((PageNavigBarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Click: return GetControlProgramExThis(EventControl)->OnPageNavigBarExEvent_R_Click((PageNavigBarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Down: return GetControlProgramExThis(EventControl)->OnPageNavigBarExEvent_L_Down((PageNavigBarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Down: return GetControlProgramExThis(EventControl)->OnPageNavigBarExEvent_R_Down((PageNavigBarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_Up: return GetControlProgramExThis(EventControl)->OnPageNavigBarExEvent_L_Up((PageNavigBarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_Up: return GetControlProgramExThis(EventControl)->OnPageNavigBarExEvent_R_Up((PageNavigBarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_L_DClick: return GetControlProgramExThis(EventControl)->OnPageNavigBarExEvent_L_DClick((PageNavigBarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_R_DClick: return GetControlProgramExThis(EventControl)->OnPageNavigBarExEvent_R_DClick((PageNavigBarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseMove: return GetControlProgramExThis(EventControl)->OnPageNavigBarExEvent_MouseMove((PageNavigBarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseEnterLeave: return GetControlProgramExThis(EventControl)->OnPageNavigBarExEvent_MouseEnterLeave((PageNavigBarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_MouseWheel: return GetControlProgramExThis(EventControl)->OnPageNavigBarExEvent_MouseWheel((PageNavigBarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_keyEvent: return GetControlProgramExThis(EventControl)->OnPageNavigBarExEvent_keyEvent((PageNavigBarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_CharInput: return GetControlProgramExThis(EventControl)->OnPageNavigBarExEvent_CharInput((PageNavigBarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Focus: return GetControlProgramExThis(EventControl)->OnPageNavigBarExEvent_Focus((PageNavigBarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_Other: return GetControlProgramExThis(EventControl)->OnPageNavigBarExEvent_Other((PageNavigBarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_PageNavigBarEx_EventNotification: return GetControlProgramExThis(EventControl)->OnPageNavigBarExEvent_EventNotification((PageNavigBarEx_P)EventControl, EventMsg, Param1, Param2, Param3, Param4); break;
		case Event_Control_GetElemEvent: return (AutoInt)Exui_EventPro_ElemEvent_ControlEx;
		default: break;
		}
		return NULL;
	}

	/*----------组件事件分发结束--------*/

	static AutoInt CALLBACK  Exui_EventPro_LoadControlEx(Control_P CtrHandle, AutoInt EventType, AutoInt Parameter1, AutoInt Parameter2, AutoInt* Parameter3, AutoInt* Parameter4)
	{

		switch (EventType)
		{
		case 1: return -1; break;
		case 2:
		{
			Parameter3[1] = (AutoInt) & (((BaseEx*)(Parameter4[1]))->control);	Parameter3[3] = Parameter4[0];
			switch (Parameter1)
			{
			case ControlType_WindowBoxEx:Parameter4[1] += sizeof(WindowBoxEx); Parameter3[2] = (AutoInt)Exui_EventPro_WindowBoxEx;	break;
			case ControlType_WindowEx:Parameter4[1] += sizeof(WindowEx); Parameter3[2] = (AutoInt)Exui_EventPro_WindowEx;		    break;
			case ControlType_FilterEx: Parameter4[1] += sizeof(FilterEx); Parameter3[2] = (AutoInt)Exui_EventPro_FilterEx; break;
			case ControlType_LabelEx: Parameter4[1] += sizeof(LabelEx); Parameter3[2] = (AutoInt)Exui_EventPro_LabelEx; break;
			case ControlType_ExtendEx: Parameter4[1] += sizeof(ExtendEx); Parameter3[2] = (AutoInt)Exui_EventPro_ExtendEx; break;
			case ControlType_MinutesboxEx: Parameter4[1] += sizeof(MinutesboxEx); Parameter3[2] = (AutoInt)Exui_EventPro_MinutesboxEx; break;
			case ControlType_ListboxEx: Parameter4[1] += sizeof(ListboxEx); Parameter3[2] = (AutoInt)Exui_EventPro_ListboxEx; break;
			case ControlType_RadiobuttonEx: Parameter4[1] += sizeof(RadiobuttonEx); Parameter3[2] = (AutoInt)Exui_EventPro_RadiobuttonEx; break;
			case ControlType_IcoListboxEx: Parameter4[1] += sizeof(IcoListboxEx); Parameter3[2] = (AutoInt)Exui_EventPro_IcoListboxEx; break;
			case ControlType_ImagebuttonEx: Parameter4[1] += sizeof(ImagebuttonEx); Parameter3[2] = (AutoInt)Exui_EventPro_ImagebuttonEx; break;
			case ControlType_PictureBoxEx: Parameter4[1] += sizeof(PictureBoxEx); Parameter3[2] = (AutoInt)Exui_EventPro_PictureBoxEx; break;
			case ControlType_MultifunctionButtonEx: Parameter4[1] += sizeof(MultifunctionButtonEx); Parameter3[2] = (AutoInt)Exui_EventPro_MultifunctionButtonEx; break;
			case ControlType_ToolbarEx: Parameter4[1] += sizeof(ToolbarEx); Parameter3[2] = (AutoInt)Exui_EventPro_ToolbarEx; break;
			case ControlType_ButtonEx: Parameter4[1] += sizeof(ButtonEx); Parameter3[2] = (AutoInt)Exui_EventPro_ButtonEx; break;
			case ControlType_TreeListEx: Parameter4[1] += sizeof(TreeListEx); Parameter3[2] = (AutoInt)Exui_EventPro_TreeListEx; break;
			case ControlType_WebBrowserEx: Parameter4[1] += sizeof(WebBrowserEx); Parameter3[2] = (AutoInt)Exui_EventPro_WebBrowserEx; break;
			case ControlType_SliderbarEx: Parameter4[1] += sizeof(SliderbarEx); Parameter3[2] = (AutoInt)Exui_EventPro_SliderbarEx; break;
			case ControlType_ScrollbarEx: Parameter4[1] += sizeof(ScrollbarEx); Parameter3[2] = (AutoInt)Exui_EventPro_ScrollbarEx; break;
			case ControlType_ComboboxEx: Parameter4[1] += sizeof(ComboboxEx); Parameter3[2] = (AutoInt)Exui_EventPro_ComboboxEx; break;
			case ControlType_EditboxEx: Parameter4[1] += sizeof(EditboxEx); Parameter3[2] = (AutoInt)Exui_EventPro_EditboxEx; break;
			case ControlType_SuperListboxEx: Parameter4[1] += sizeof(SuperListboxEx); Parameter3[2] = (AutoInt)Exui_EventPro_SuperListboxEx; break;
			case ControlType_SuperbuttonEx: Parameter4[1] += sizeof(SuperbuttonEx); Parameter3[2] = (AutoInt)Exui_EventPro_SuperbuttonEx; break;
			case ControlType_ProgressbarEx: Parameter4[1] += sizeof(ProgressbarEx); Parameter3[2] = (AutoInt)Exui_EventPro_ProgressbarEx; break;
			case ControlType_SelectthefolderEx: Parameter4[1] += sizeof(SelectthefolderEx); Parameter3[2] = (AutoInt)Exui_EventPro_SelectthefolderEx; break;
			case ControlType_ChoiceboxEx: Parameter4[1] += sizeof(ChoiceboxEx); Parameter3[2] = (AutoInt)Exui_EventPro_ChoiceboxEx; break;
			case ControlType_CalendarBoxEx: Parameter4[1] += sizeof(CalendarBoxEx); Parameter3[2] = (AutoInt)Exui_EventPro_CalendarBoxEx; break;
			case ControlType_ColorPickEx: Parameter4[1] += sizeof(ColorPickEx); Parameter3[2] = (AutoInt)Exui_EventPro_ColorPickEx; break;
			case ControlType_RichEditEx: Parameter4[1] += sizeof(RichEditEx); Parameter3[2] = (AutoInt)Exui_EventPro_RichEditEx; break;
			case ControlType_AnimationbuttonEx: Parameter4[1] += sizeof(AnimationbuttonEx); Parameter3[2] = (AutoInt)Exui_EventPro_AnimationbuttonEx; break;
			case ControlType_AdvancedFormEx: Parameter4[1] += sizeof(AdvancedFormEx); Parameter3[2] = (AutoInt)Exui_EventPro_AdvancedFormEx; break;
			case ControlType_SlideButtonEx: Parameter4[1] += sizeof(SlideButtonEx); Parameter3[2] = (AutoInt)Exui_EventPro_SlideButtonEx; break;
			case ControlType_PieChartEx: Parameter4[1] += sizeof(PieChartEx); Parameter3[2] = (AutoInt)Exui_EventPro_PieChartEx; break;
			case ControlType_BarChartEx: Parameter4[1] += sizeof(BarChartEx); Parameter3[2] = (AutoInt)Exui_EventPro_BarChartEx; break;
			case ControlType_CurveChartEx: Parameter4[1] += sizeof(CurveChartEx); Parameter3[2] = (AutoInt)Exui_EventPro_CurveChartEx; break;
			case ControlType_CandleChartEx: Parameter4[1] += sizeof(CandleChartEx); Parameter3[2] = (AutoInt)Exui_EventPro_CandleChartEx; break;
			case ControlType_DrawPanelEx: Parameter4[1] += sizeof(DrawPanelEx); Parameter3[2] = (AutoInt)Exui_EventPro_DrawPanelEx; break;
			case ControlType_ScrollLayoutBoxEx: Parameter4[1] += sizeof(ScrollLayoutBoxEx); Parameter3[2] = (AutoInt)Exui_EventPro_ScrollLayoutBoxEx; break;
			case ControlType_MediaboxEx: Parameter4[1] += sizeof(MediaboxEx); Parameter3[2] = (AutoInt)Exui_EventPro_MediaboxEx; break;
			case ControlType_CarouselBoxEx: Parameter4[1] += sizeof(CarouselBoxEx); Parameter3[2] = (AutoInt)Exui_EventPro_CarouselBoxEx; break;
			case ControlType_SuperEditEx: Parameter4[1] += sizeof(SuperEditEx); Parameter3[2] = (AutoInt)Exui_EventPro_SuperEditEx; break;
			case ControlType_ChatBoxEx: Parameter4[1] += sizeof(ChatBoxEx); Parameter3[2] = (AutoInt)Exui_EventPro_ChatBoxEx; break;
			case ControlType_SuperChartEx: Parameter4[1] += sizeof(SuperChartEx); Parameter3[2] = (AutoInt)Exui_EventPro_SuperChartEx; break;
			case ControlType_DataReportBoxEx: Parameter4[1] += sizeof(DataReportBoxEx); Parameter3[2] = (AutoInt)Exui_EventPro_DataReportBoxEx; break;

			default: break;
			}
			break;
		}
		case 4:
			if (Parameter1 != ControlType_WindowBoxEx && !((ExuiProgramEx*)(Parameter4[0]))->control)
			{
				((ExuiProgramEx*)(Parameter4[0]))->control = (Control_P)CtrHandle;
				break;
			}
			if (Parameter1 == ControlType_WindowBoxEx && !((ExuiProgramEx*)(Parameter4[0]))->Hwnd)
			{
				((ExuiProgramEx*)(Parameter4[0]))->Hwnd = (AutoInt)CtrHandle;
				break;
			}
		default: break;
		}

		return 0;
	}


}
//BitmapEx
namespace exui
{
	BitmapEx::BitmapEx()
	{
		if (ImageHandle) { ::DestroyImageEx(ImageHandle); ImageHandle = NULL; }
	}

	BitmapEx::BitmapEx(const BindataEx& data, int Mode, int Width, int Height, int Angle, int LineClour, int LineWidth, int Alignment)
	{
		::DestroyImageEx(ImageHandle);
		ImageHandle = ::LoadImageEx((BinEx_P)data, Mode, Width, Height, Angle, LineClour, LineWidth, Alignment);
	}
	BitmapEx::BitmapEx(const StringEx& data, int Mode, int Width, int Height, int Angle, int LineClour, int LineWidth, int Alignment)
	{
		::DestroyImageEx(ImageHandle);
		ImageHandle = ::LoadImageEx((BinEx_P)data, Mode, Width, Height, Angle, LineClour, LineWidth, Alignment);
	}
	BitmapEx::BitmapEx(const byte* data, int datalong, int Mode, int Width, int Height, int Angle, int LineClour, int LineWidth, int Alignment)
	{
		::DestroyImageEx(ImageHandle);
		BindataEx bindata(data, datalong);
		ImageHandle = ::LoadImageEx((BinEx_P)bindata, Mode, Width, Height, Angle, LineClour, LineWidth, Alignment);
	}

	BitmapEx::BitmapEx(const ImageEx_P data)
	{
		::DestroyImageEx(ImageHandle); ImageHandle = (ImageEx_P)GetImageAttributeEx((ImageEx_P)data, 3);
	}
	BitmapEx::BitmapEx(const BitmapEx& data)
	{
		::DestroyImageEx(ImageHandle); ImageHandle = (ImageEx_P)GetImageAttributeEx((ImageEx_P)data.ImageHandle, 3);
	}

	BitmapEx:: ~BitmapEx()
	{
		if (ImageHandle) { ::DestroyImageEx(ImageHandle); ImageHandle = NULL; }
	}

	BitmapEx& BitmapEx::operator=(const BitmapEx& data)
	{
		::DestroyImageEx(ImageHandle); ImageHandle = (ImageEx_P)::GetImageAttributeEx((ImageEx_P)data.ImageHandle, 3);
		return *this;
	}

	BitmapEx& BitmapEx::operator=(const ImageEx_P& data)
	{
		::DestroyImageEx(ImageHandle); ImageHandle = (ImageEx_P)::GetImageAttributeEx((ImageEx_P)data, 3);
		return *this;
	}

	bool BitmapEx::operator==(const BitmapEx& data)
	{
		return  ImageHandle == data.ImageHandle;
	}

	bool BitmapEx::operator==(const ImageEx_P& data)
	{
		return  ImageHandle == data;
	}

	/*--------装载位图--------*/
	ImageEx_P BitmapEx::Load(BindataEx data, int Mode, int Width, int Height, int Angle, int LineClour, int LineWidth, int Alignment)
	{
		::DestroyImageEx(ImageHandle);
		ImageHandle = (ImageEx_P) 	::LoadImageEx((BinEx_P)data, Mode, Width, Height, Angle, LineClour, LineWidth, Alignment);
		return ImageHandle;
	}
	/*--------装载位图--------*/
	ImageEx_P BitmapEx::Load(StringEx data, int Mode, int Width, int Height, int Angle, int LineClour, int LineWidth, int Alignment)
	{
		::DestroyImageEx(ImageHandle);
		ImageHandle = (ImageEx_P) 	::LoadImageEx((BinEx_P)data, Mode, Width, Height, Angle, LineClour, LineWidth, Alignment);
		return ImageHandle;
	}
	/*--------装载位图--------*/
	ImageEx_P BitmapEx::Load(const byte* data, int datalong, int Mode, int Width, int Height, int Angle, int LineClour, int LineWidth, int Alignment)
	{
		::DestroyImageEx(ImageHandle);
		ImageHandle = (ImageEx_P) 	::LoadImageEx((BinEx_P)data, Mode, Width, Height, Angle, LineClour, LineWidth, Alignment);
		return ImageHandle;
	}


	/*--------销毁位图--------*/
	void BitmapEx::Destroy()
	{
		::DestroyImageEx(ImageHandle); ImageHandle = NULL;
	}

	/*--------取位图属性--------*/
	AutoInt BitmapEx::GetAttribute(AutoInt index)
	{
		return	::GetImageAttributeEx(ImageHandle, index);
	}

	/*--------取位图数据--------*/
	BindataEx BitmapEx::GetData()
	{
		return	::GetImageData(ImageHandle);
	}

}
//BindataEx
namespace exui
{
	BindataEx::BindataEx()
	{
		if (BinEx) { DeleteBinEx(BinEx); BinEx = NULL; }
	}
	BindataEx::BindataEx(const byte* data, int datalong)
	{
		ExecuteBinExCmd(BinEx, 20062, (AutoInt)&BinEx, (AutoInt)data, datalong, 0);
	}

	BindataEx::BindataEx(const BinEx_P data)
	{
		ExecuteBinExCmd(BinEx, 20060, (AutoInt)&BinEx, (AutoInt)data, 0, 0);
	}

	BindataEx::BindataEx(const BindataEx& data)
	{
		ExecuteBinExCmd(BinEx, 20060, (AutoInt)&BinEx, (AutoInt)data.BinEx, 0, 0);
	}
	BindataEx:: ~BindataEx()
	{
		if (BinEx) { DeleteBinEx(BinEx); BinEx = NULL; }
	}

	BindataEx& BindataEx::operator=(const BinEx_P data)
	{
		ExecuteBinExCmd(BinEx, 20060, (AutoInt)&BinEx, (AutoInt)data, 0, 0);
		return *this;
	}
	BindataEx& BindataEx::operator=(const BindataEx& data)
	{
		ExecuteBinExCmd(BinEx, 20060, (AutoInt)&BinEx, (AutoInt)data.BinEx, 0, 0);
		return *this;
	}

	BindataEx BindataEx::operator+(const BinEx_P data)
	{
		BindataEx Tmp;
		ExecuteBinExCmd(BinEx, 20070, (AutoInt)&Tmp.BinEx, (AutoInt)BinEx, (AutoInt)data, 0);
		return  Tmp;
	}
	BindataEx BindataEx::operator+(const BindataEx& data)
	{
		BindataEx Tmp;
		ExecuteBinExCmd(BinEx, 20070, (AutoInt)&Tmp.BinEx, (AutoInt)BinEx, (AutoInt)data.BinEx, 0);
		return  Tmp;
	}

	BindataEx& BindataEx::operator+=(const BinEx_P data)
	{
		ExecuteBinExCmd(BinEx, 20080, (AutoInt)&BinEx, (AutoInt)BinEx, (AutoInt)data, 0);
		return *this;
	}
	BindataEx& BindataEx::operator+=(const BindataEx& data)
	{
		ExecuteBinExCmd(BinEx, 20080, (AutoInt)&BinEx, (AutoInt)data.BinEx, 0, 0);
		return *this;
	}

	bool BindataEx::operator!=(const BinEx_P data)
	{
		return  ExecuteBinExCmd(BinEx, 20050, (AutoInt)&BinEx, (AutoInt)data, 0, 0);
	}
	bool BindataEx::operator!=(const BindataEx& data)
	{
		return  ExecuteBinExCmd(BinEx, 20050, (AutoInt)&BinEx, (AutoInt)data.BinEx, 0, 0);
	}
	bool BindataEx::operator==(const BinEx_P data)
	{
		return  ExecuteBinExCmd(BinEx, 20050, (AutoInt)&BinEx, (AutoInt)data, 0, 0);
	}
	bool BindataEx::operator==(const BindataEx& data)
	{
		return  ExecuteBinExCmd(BinEx, 20050, (AutoInt)&BinEx, (AutoInt)data.BinEx, 0, 0);
	}

	int BindataEx::GetDataLong()
	{
		return		GetBinExDatalength(BinEx);
	}


}
//StringEx
namespace exui
{
	StrEx_P	UnicodeToStrEx(const WCHAR* str)
	{
		return (StrEx_P)CreateBinEx(str, -6);
	}
	WCHAR* StrExToUnicode(StrEx_P str)
	{
		return (((WCHAR*)str) + 4);
	}


	StringEx::StringEx()
	{
		if (StrEx) { DeleteBinEx(StrEx); StrEx = NULL; }
	}
	StringEx::StringEx(const char* str)
	{
		ExecuteBinExCmd(StrEx, 10062, (AutoInt)&StrEx, (AutoInt)str, 0, 0);
	}
	StringEx::StringEx(const wchar_t* str)
	{
		ExecuteBinExCmd(StrEx, 10061, (AutoInt)&StrEx, (AutoInt)str, 0, 0);
	}
	StringEx::StringEx(const StrEx_P str)
	{
		ExecuteBinExCmd(StrEx, 10060, (AutoInt)&StrEx, (AutoInt)str, 0, 0);
	}
	StringEx::StringEx(const StringEx& str)
	{
		ExecuteBinExCmd(StrEx, 10060, (AutoInt)&StrEx, (AutoInt)str.StrEx, 0, 0);

	}
	StringEx:: ~StringEx()
	{
		if (StrEx) { DeleteBinEx(StrEx); StrEx = NULL; }
	}




	StringEx& StringEx::operator=(const char* str)
	{
		ExecuteBinExCmd(StrEx, 10062, (AutoInt)&StrEx, (AutoInt)str, 0, 0);
		return *this;
	}
	StringEx& StringEx::operator=(const wchar_t* str)
	{
		ExecuteBinExCmd(StrEx, 10061, (AutoInt)&StrEx, (AutoInt)str, 0, 0);
		return *this;
	}
	StringEx& StringEx::operator=(const StrEx_P str)
	{
		ExecuteBinExCmd(StrEx, 10060, (AutoInt)&StrEx, (AutoInt)str, 0, 0);
		return *this;

	}
	StringEx& StringEx::operator=(const StringEx& str)
	{
		ExecuteBinExCmd(StrEx, 10060, (AutoInt)&StrEx, (AutoInt)str.StrEx, 0, 0);
		return *this;
	}

	StringEx StringEx::operator+(const char* str)
	{
		StringEx Tmp;
		ExecuteBinExCmd(StrEx, 10072, (AutoInt)&Tmp.StrEx, (AutoInt)StrEx, (AutoInt)str, 0);
		return  Tmp;
	}
	StringEx StringEx::operator+(const wchar_t* str)
	{
		StringEx Tmp;
		ExecuteBinExCmd(StrEx, 10071, (AutoInt)&Tmp.StrEx, (AutoInt)StrEx, (AutoInt)str, 0);
		return  Tmp;
	}
	StringEx StringEx::operator+(const StrEx_P str)
	{
		StringEx Tmp;
		ExecuteBinExCmd(StrEx, 10070, (AutoInt)&Tmp.StrEx, (AutoInt)StrEx, (AutoInt)str, 0);
		return  Tmp;
	}
	StringEx StringEx::operator+(const StringEx& str)
	{
		StringEx Tmp;
		ExecuteBinExCmd(StrEx, 10070, (AutoInt)&Tmp.StrEx, (AutoInt)StrEx, (AutoInt)str.StrEx, 0);
		return  Tmp;
	}

	StringEx& StringEx::operator+=(const char* str)
	{
		ExecuteBinExCmd(StrEx, 10082, (AutoInt)&StrEx, (AutoInt)str, 0, 0);
		return *this;
	}
	StringEx& StringEx::operator+=(const wchar_t* str)
	{
		ExecuteBinExCmd(StrEx, 10081, (AutoInt)&StrEx, (AutoInt)str, 0, 0);
		return *this;
	}
	StringEx& StringEx::operator+=(const StrEx_P str)
	{
		ExecuteBinExCmd(StrEx, 10080, (AutoInt)&StrEx, (AutoInt)str, 0, 0);
		return *this;
	}
	StringEx& StringEx::operator+=(const StringEx& str)
	{
		ExecuteBinExCmd(StrEx, 10080, (AutoInt)&StrEx, (AutoInt)str.StrEx, (AutoInt)str.StrEx, 0);
		return *this;
	}


	bool StringEx::operator>(const char* str)
	{
		return Cmp(str) > 0;
	}
	bool StringEx::operator>(const  wchar_t* str)
	{
		return Cmp(str) > 0;
	}
	bool StringEx::operator>(const  StrEx_P str)
	{
		return Cmp(str) > 0;
	}
	bool StringEx::operator>(const StringEx& str)
	{
		return Cmp(str) > 0;
	}


	bool StringEx::operator<(const char* str)
	{
		return Cmp(str) < 0;
	}
	bool StringEx::operator<(const  wchar_t* str)
	{
		return Cmp(str) < 0;
	}
	bool StringEx::operator<(const  StrEx_P str)
	{
		return Cmp(str) < 0;
	}
	bool StringEx::operator<(const StringEx& str)
	{
		return Cmp(str) < 0;
	}


	bool StringEx::operator!=(const char* str)
	{
		return Cmp(str) != 0;
	}
	bool StringEx::operator!=(const  wchar_t* str)
	{
		return Cmp(str) != 0;
	}
	bool StringEx::operator!=(const  StrEx_P str)
	{
		return Cmp(str) != 0;
	}
	bool StringEx::operator!=(const StringEx& str)
	{
		return Cmp(str) != 0;
	}

	bool StringEx::operator==(const char* str)
	{
		return Cmp(str) == 0;
	}
	bool StringEx::operator==(const  wchar_t* str)
	{
		return Cmp(str) == 0;
	}
	bool StringEx::operator==(const  StrEx_P str)
	{
		return Cmp(str) == 0;
	}
	bool StringEx::operator==(const StringEx& str)
	{
		return Cmp(str) == 0;
	}

	bool StringEx::operator>=(const char* str)
	{
		return Cmp(str) >= 0;
	}
	bool StringEx::operator>=(const  wchar_t* str)
	{
		return Cmp(str) >= 0;
	}
	bool StringEx::operator>=(const  StrEx_P str)
	{
		return Cmp(str) >= 0;
	}
	bool StringEx::operator>=(const StringEx& str)
	{
		return Cmp(str) >= 0;
	}

	bool StringEx::operator<=(const char* str)
	{
		return Cmp(str) <= 0;
	}
	bool StringEx::operator<=(const  wchar_t* str)
	{
		return Cmp(str) <= 0;
	}
	bool StringEx::operator<=(const  StrEx_P str)
	{
		return Cmp(str) <= 0;
	}
	bool StringEx::operator<=(const StringEx& str)
	{
		return Cmp(str) <= 0;
	}


	int StringEx::Cmp(const char* str)
	{
		return  ExecuteBinExCmd(StrEx, 10052, (AutoInt)&StrEx, (AutoInt)str, 0, 0);
	}
	int StringEx::Cmp(const  wchar_t* str)
	{
		return  ExecuteBinExCmd(StrEx, 10051, (AutoInt)&StrEx, (AutoInt)str, 0, 0);
	}
	int StringEx::Cmp(const  StrEx_P str)
	{
		return  ExecuteBinExCmd(StrEx, 10050, (AutoInt)&StrEx, (AutoInt)str, 0, 0);
	}
	int StringEx::Cmp(const StringEx& str)
	{
		return  ExecuteBinExCmd(StrEx, 10050, (AutoInt)&StrEx, (AutoInt)str.StrEx, 0, 0);
	}

	wchar_t* StringEx::GetStr()
	{
		return (((WCHAR*)StrEx) + 4);
	}

	StrEx_P StringEx::GetStrEx()
	{
		return StrEx;
	}


}
//BaseEx
namespace exui
{

	BaseEx::BaseEx()
	{

	}


	BaseEx::BaseEx(const Control_P controlhandle)
	{
		control = (Control_P)controlhandle;
	}
	BaseEx::BaseEx(const BaseEx& baseex)
	{
		control = baseex.control;
		controltype = baseex.controltype;
	}

	BaseEx::~BaseEx()
	{

	}

	bool BaseEx::operator==(const  Control_P controlhandle)
	{
		return control == controlhandle;
	}
	bool BaseEx::operator==(const BaseEx& baseex)
	{
		return control == baseex.control;
	}



	bool BaseEx::operator!=(const  Control_P controlhandle)
	{
		return control != controlhandle;
	}

	bool BaseEx::operator!=(const BaseEx& baseex)
	{
		return control != baseex.control;
	}



	BaseEx& BaseEx::operator=(const Control_P controlhandle)
	{
		control = (Control_P)controlhandle;
		return *this;
	}
	BaseEx& BaseEx::operator=(const BaseEx& baseex)
	{
		control = baseex.control;
		controltype = baseex.controltype;
		return *this;
	}

	void BaseEx::SetHandle(Control_P ControlHamdle)
	{
		control = ControlHamdle;
	}
	Control_P BaseEx::GetHandle()
	{
		return control;
	}
	AutoInt BaseEx::GetType()
	{

		return controltype;
	}
	void BaseEx::SetType(AutoInt type)
	{
		controltype = type;
		return;
	}

}
//StructEx
namespace exui
{
	StructEx::StructEx()
	{

	}
	StructEx::~StructEx()
	{

	}

	StructEx::StructEx(const StructEx_P StructHandle)
	{
		Struct = (StructEx_P)StructHandle;
	}
	StructEx::StructEx(const StructEx& StructHandle)
	{
		Struct = StructHandle.Struct;
	}

	bool StructEx::operator!=(const  StructEx_P StructHandle)
	{
		return Struct != StructHandle;
	}
	bool StructEx::operator!=(const StructEx& structex)
	{
		return Struct != structex.Struct;
	}

	bool StructEx::operator==(const  StructEx_P StructHandle)
	{
		return Struct == StructHandle;
	}
	bool StructEx::operator==(const StructEx& structex)
	{
		return Struct == structex.Struct;
	}

	StructEx& StructEx::operator=(const StructEx_P StructHandle)
	{
		Struct = (StructEx_P)StructHandle;
		return *this;
	}
	StructEx& StructEx::operator=(const StructEx& structex)
	{
		Struct = structex.Struct;
		return *this;
	}


	/*--------创建--------*/
	StructEx_P StructEx::Create()
	{
		Struct = Create_StructEx();
		return  (StructEx_P)Struct;
	}
	/*--------销毁--------*/
	void StructEx::Destroy()
	{
		Destroy_StructEx((StructEx_P)Struct);
		Struct = NULL;
	}
	/*--------载入--------*/
	BOOL StructEx::Load(BindataEx Data, AutoInt DataType, AutoInt Parameter1, AutoInt Parameter2, AutoInt Callback, AutoInt CallbackParameter)
	{
		return Load_StructEx((StructEx_P)Struct, Data, DataType, Parameter1, Parameter2, Callback, CallbackParameter);
	}
	/*--------打包--------*/
	BindataEx StructEx::Pack(AutoInt DataType, AutoInt Parameter1, AutoInt Parameter2, AutoInt Callback, AutoInt CallbackParameter)
	{
		return Pack_StructEx((StructEx_P)Struct, DataType, Parameter1, Parameter2, Callback, CallbackParameter);
	}
	/*--------插入--------*/
	AutoInt StructEx::InsertMember(AutoInt Member, AutoInt addnum)
	{
		return InsertMember_StructEx((StructEx_P)Struct, Member, addnum);
	}
	/*--------删除--------*/
	void StructEx::DeleteMember(AutoInt delMember, AutoInt deletenum)
	{
		DeleteMember_StructEx((StructEx_P)Struct, delMember, deletenum);
	}
	/*--------取数量--------*/
	AutoInt StructEx::GetMemberCount()
	{
		return GetMemberCount_StructEx((StructEx_P)Struct);
	}
	/*--------取类型--------*/
	int StructEx::GetMemberType(AutoInt Member)
	{
		return GetMemberType_StructEx((StructEx_P)Struct, Member);
	}
	/*--------置名称--------*/
	void StructEx::SetMemberName(AutoInt Member, StringEx Name)
	{
		SetMemberName_StructEx((StructEx_P)Struct, Member, Name);
	}
	/*--------取名称--------*/
	StringEx StructEx::GetMemberName(AutoInt Member)
	{
		return GetMemberName_StructEx((StructEx_P)Struct, Member);
	}
	/*--------置逻辑--------*/
	void StructEx::SetMemberBool(AutoInt Member, BOOL logic)
	{
		SetMemberBool_StructEx((StructEx_P)Struct, Member, logic);
	}
	/*--------取逻辑--------*/
	BOOL StructEx::GetMemberBool(AutoInt Member)
	{
		return GetMemberBool_StructEx((StructEx_P)Struct, Member);
	}
	/*--------置整数--------*/
	void StructEx::SetMemberInt(AutoInt Member, int integer)
	{
		SetMemberInt_StructEx((StructEx_P)Struct, Member, integer);
	}
	/*--------取整数--------*/
	int StructEx::GetMemberInt(AutoInt Member)
	{
		return GetMemberInt_StructEx((StructEx_P)Struct, Member);
	}
	/*--------置长整数--------*/
	void StructEx::SetMemberLongInt(AutoInt Member, INT64 longinteger)
	{
		SetMemberLongInt_StructEx((StructEx_P)Struct, Member, longinteger);
	}
	/*--------取长整数--------*/
	INT64 StructEx::GetMemberLongInt(AutoInt Member)
	{
		return GetMemberLongInt_StructEx((StructEx_P)Struct, Member);
	}
	/*--------置小数--------*/
	void StructEx::SetMemberFloat(AutoInt Member, float decimal)
	{
		SetMemberFloat_StructEx((StructEx_P)Struct, Member, decimal);
	}
	/*--------取小数--------*/
	float StructEx::GetMemberFloat(AutoInt Member)
	{
		return GetMemberFloat_StructEx((StructEx_P)Struct, Member);
	}
	/*--------置双精度小数--------*/
	void StructEx::SetMemberDouble(AutoInt Member, double DoubleDecimal)
	{
		SetMemberDouble_StructEx((StructEx_P)Struct, Member, DoubleDecimal);
	}
	/*--------取双精度小数--------*/
	double StructEx::GetMemberDouble(AutoInt Member)
	{
		return GetMemberDouble_StructEx((StructEx_P)Struct, Member);
	}
	/*--------置文本--------*/
	void StructEx::SetMemberText(AutoInt Member, StringEx Text)
	{
		SetMemberText_StructEx((StructEx_P)Struct, Member, Text);
	}
	/*--------取文本--------*/
	StringEx StructEx::GetMemberText(AutoInt Member)
	{
		return GetMemberText_StructEx((StructEx_P)Struct, Member);
	}
	/*--------置数据--------*/
	void StructEx::SetMemberBin(AutoInt Member, BindataEx Bin)
	{
		SetMemberBin_StructEx((StructEx_P)Struct, Member, Bin);
	}
	/*--------取数据--------*/
	BindataEx StructEx::GetMemberBin(AutoInt Member)
	{
		return GetMemberBin_StructEx((StructEx_P)Struct, Member);
	}
	/*--------置结构体--------*/
	void StructEx::SetMemberStruct(AutoInt Member, StructEx StructInfo)
	{
		SetMemberStruct_StructEx((StructEx_P)Struct, Member, (StructEx_P)StructInfo);
	}
	/*--------取结构体--------*/
	StructEx StructEx::GetMemberStruct(AutoInt Member)
	{
		return (StructEx_P)GetMemberStruct_StructEx((StructEx_P)Struct, Member);
	}
	/*--------插入--------*/
	AutoInt StructEx::InsertAttr(AutoInt Attr, AutoInt addnum)
	{
		return InsertAttr_StructEx((StructEx_P)Struct, Attr, addnum);
	}
	/*--------删除--------*/
	void StructEx::DeleteAttr(AutoInt delAttr, AutoInt deletenum)
	{
		DeleteAttr_StructEx((StructEx_P)Struct, delAttr, deletenum);
	}
	/*--------取数量--------*/
	AutoInt StructEx::GetAttrCount()
	{
		return GetAttrCount_StructEx((StructEx_P)Struct);
	}
	/*--------取类型--------*/
	int StructEx::GetAttrType(AutoInt Attr)
	{
		return GetAttrType_StructEx((StructEx_P)Struct, Attr);
	}
	/*--------置名称--------*/
	void StructEx::SetAttrName(AutoInt Attr, StringEx Name)
	{
		SetAttrName_StructEx((StructEx_P)Struct, Attr, Name);
	}
	/*--------取名称--------*/
	StringEx StructEx::GetAttrName(AutoInt Attr)
	{
		return GetAttrName_StructEx((StructEx_P)Struct, Attr);
	}
	/*--------置逻辑--------*/
	void StructEx::SetAttrBool(AutoInt Attr, BOOL logic)
	{
		SetAttrBool_StructEx((StructEx_P)Struct, Attr, logic);
	}
	/*--------取逻辑--------*/
	BOOL StructEx::GetAttrBool(AutoInt Attr)
	{
		return GetAttrBool_StructEx((StructEx_P)Struct, Attr);
	}
	/*--------置整数--------*/
	void StructEx::SetAttrInt(AutoInt Attr, int integer)
	{
		SetAttrInt_StructEx((StructEx_P)Struct, Attr, integer);
	}
	/*--------取整数--------*/
	int StructEx::GetAttrInt(AutoInt Attr)
	{
		return GetAttrInt_StructEx((StructEx_P)Struct, Attr);
	}
	/*--------置长整数--------*/
	void StructEx::SetAttrLongInt(AutoInt Attr, INT64 longinteger)
	{
		SetAttrLongInt_StructEx((StructEx_P)Struct, Attr, longinteger);
	}
	/*--------取长整数--------*/
	INT64 StructEx::GetAttrLongInt(AutoInt Attr)
	{
		return GetAttrLongInt_StructEx((StructEx_P)Struct, Attr);
	}
	/*--------置小数--------*/
	void StructEx::SetAttrFloat(AutoInt Attr, float decimal)
	{
		SetAttrFloat_StructEx((StructEx_P)Struct, Attr, decimal);
	}
	/*--------取小数--------*/
	float StructEx::GetAttrFloat(AutoInt Attr)
	{
		return GetAttrFloat_StructEx((StructEx_P)Struct, Attr);
	}
	/*--------置双精度小数--------*/
	void StructEx::SetAttrDouble(AutoInt Attr, double DoubleDecimal)
	{
		SetAttrDouble_StructEx((StructEx_P)Struct, Attr, DoubleDecimal);
	}
	/*--------取双精度小数--------*/
	double StructEx::GetAttrDouble(AutoInt Attr)
	{
		return GetAttrDouble_StructEx((StructEx_P)Struct, Attr);
	}
	/*--------置文本--------*/
	void StructEx::SetAttrText(AutoInt Attr, StringEx Text)
	{
		SetAttrText_StructEx((StructEx_P)Struct, Attr, Text);
	}
	/*--------取文本--------*/
	StringEx StructEx::GetAttrText(AutoInt Attr)
	{
		return GetAttrText_StructEx((StructEx_P)Struct, Attr);
	}
	/*--------置数据--------*/
	void StructEx::SetAttrBin(AutoInt Attr, BindataEx Bin)
	{
		SetAttrBin_StructEx((StructEx_P)Struct, Attr, Bin);
	}
	/*--------取数据--------*/
	BindataEx StructEx::GetAttrBin(AutoInt Attr)
	{
		return GetAttrBin_StructEx((StructEx_P)Struct, Attr);
	}
	/*--------置结构体--------*/
	void StructEx::SetAttrStruct(AutoInt Attr, StructEx StructInfo)
	{
		SetAttrStruct_StructEx((StructEx_P)Struct, Attr, (StructEx_P)StructInfo);
	}
	/*--------取结构体--------*/
	StructEx StructEx::GetAttrStruct(AutoInt Attr)
	{
		return (StructEx_P)GetAttrStruct_StructEx((StructEx_P)Struct, Attr);
	}

}
//ControlEx
namespace exui
{
	ControlEx::ControlEx()
	{
		controltype = ControlType_Control;
	}
	ControlEx::~ControlEx()
	{

	}
	/*--------组件销毁--------*/
	void ControlEx::Destroy()
	{
		ControlDestroy(control);
	}
	/*--------组件是否已创建--------*/
	BOOL ControlEx::IsCreate()
	{
		return ControlIsCreate(control);
	}
	/*--------组件发送消息--------*/
	AutoInt ControlEx::SendMessage(AutoInt msg, AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4)
	{
		return ControlSendMessage(control, msg, Parameter1, Parameter2, Parameter3, Parameter4);
	}
	/*--------组件发送子级消息--------*/
	void ControlEx::SendChildMessage(AutoInt msg, AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4)
	{
		ControlSendChildMessage(control, msg, Parameter1, Parameter2, Parameter3, Parameter4);
	}
	/*--------组件更新缓存--------*/
	BOOL ControlEx::UpdateCache(int Mode)
	{
		return ControlUpdateCache(Mode);
	}
	/*--------组件取距窗口顶边--------*/
	int ControlEx::GetWindowTop()
	{
		return ControlGetWindowTop(control);
	}
	/*--------组件取距窗口左边--------*/
	int ControlEx::GetWindowLeft()
	{
		return ControlGetWindowLeft(control);
	}
	/*--------组件置左边--------*/
	void ControlEx::Left(int Left)
	{
		ControlSetLeft(control, Left);
	}
	/*--------组件取左边--------*/
	int ControlEx::Left()
	{
		return ControlGetLeft(control);
	}
	/*--------组件置顶边--------*/
	void ControlEx::Top(int Top)
	{
		ControlSetTop(control, Top);
	}
	/*--------组件取顶边--------*/
	int ControlEx::Top()
	{
		return ControlGetTop(control);
	}
	/*--------组件置宽度--------*/
	void ControlEx::Width(int Width)
	{
		ControlSetWidth(control, Width);
	}
	/*--------组件取宽度--------*/
	int ControlEx::Width()
	{
		return ControlGetWidth(control);
	}
	/*--------组件置高度--------*/
	void ControlEx::Height(int Height)
	{
		ControlSetHeight(control, Height);
	}
	/*--------组件取高度--------*/
	int ControlEx::Height()
	{
		return ControlGetHeight(control);
	}
	/*--------组件取矩形--------*/
	void ControlEx::GetRect(int* Left, int* Top, int* Width, int* Height)
	{
		ControlGetRect(control, Left, Top, Width, Height);
	}
	/*--------组件置矩形--------*/
	void ControlEx::SetRect(int Left, int Top, int Width, int Height)
	{
		ControlSetRect(control, Left, Top, Width, Height);
	}
	/*--------组件取真实可视--------*/
	BOOL ControlEx::GetTrueVisual()
	{
		return ControlGetTrueVisual(control);
	}
	/*--------组件取可视--------*/
	BOOL ControlEx::Visual()
	{
		return ControlGetVisual(control);
	}
	/*--------组件置可视--------*/
	void ControlEx::Visual(BOOL Visual)
	{
		ControlSetVisual(control, Visual);
	}
	/*--------组件取真实禁止--------*/
	BOOL ControlEx::GetTrueDisabled()
	{
		return ControlGetTrueDisabled(control);
	}
	/*--------组件取禁止--------*/
	BOOL ControlEx::Disabled()
	{
		return ControlGetDisabled(control);
	}
	/*--------组件置禁止--------*/
	void ControlEx::Disabled(BOOL UpdateDisabled)
	{
		ControlSetDisabled(control, UpdateDisabled);
	}
	/*--------组件置光标--------*/
	void ControlEx::Cursor(BindataEx Cursor)
	{
		ControlSetCursor(control, Cursor);
	}
	/*--------组件取光标--------*/
	BindataEx ControlEx::Cursor()
	{
		return ControlGetCursor(control);
	}
	/*--------组件置光标id--------*/
	void ControlEx::CursorId(int CursorId)
	{
		ControlSetCursorId(control, CursorId);
	}
	/*--------组件取光标id--------*/
	int ControlEx::CursorId()
	{
		return ControlGetCursorId(control);
	}
	/*--------组件取鼠标穿透--------*/
	int ControlEx::Penetrate()
	{
		return ControlGetPenetrate(control);
	}
	/*--------组件置鼠标穿透--------*/
	void ControlEx::Penetrate(int Penetrate)
	{
		ControlSetPenetrate(control, Penetrate);
	}
	/*--------组件取透明度--------*/
	int ControlEx::Transparency()
	{
		return ControlGetTransparency(control);
	}
	/*--------组件置透明度--------*/
	void ControlEx::Transparency(int Transparency)
	{
		ControlSetTransparency(control, Transparency);
	}
	/*--------组件置焦点权重--------*/
	void ControlEx::FocusWeight(int FocusWeight)
	{
		ControlSetFocusWeight(control, FocusWeight);
	}
	/*--------组件取焦点权重--------*/
	int ControlEx::FocusWeight()
	{
		return ControlGetFocusWeight(control);
	}
	/*--------组件置焦点组件--------*/
	void ControlEx::SetFocusControl()
	{
		ControlSetFocusControl(control);
	}
	/*--------组件取焦点组件--------*/
	Control_P ControlEx::GetFocusControl()
	{
		return ControlGetFocusControl();
	}
	/*--------组件取上一焦点组件--------*/
	Control_P ControlEx::GetMaxFocusWeightControl()
	{
		return ControlGetMaxFocusWeightControl(control);
	}
	/*--------组件取下一焦点组件--------*/
	Control_P ControlEx::GetNextFocusControl()
	{
		return ControlGetNextFocusControl(control);
	}
	/*--------组件取鼠标捕获组件--------*/
	Control_P ControlEx::GetCaptureControl()
	{
		return ControlGetCaptureControl();
	}
	/*--------组件置鼠标捕获组件--------*/
	void ControlEx::SetCaptureControl()
	{
		ControlSetCaptureControl(control);
	}
	/*--------组件取热点组件--------*/
	Control_P ControlEx::GetHotControl()
	{
		return ControlGetHotControl();
	}
	/*--------组件取左键按下组件--------*/
	Control_P ControlEx::GetLeftPressControl()
	{
		return ControlGetLeftPressControl();
	}
	/*--------组件取右键按下组件--------*/
	Control_P ControlEx::GetRightPressControl()
	{
		return ControlGetRightPressControl();
	}
	/*--------组件置类型--------*/
	void ControlEx::SetcontrolType(int controlType)
	{
		ControlSetcontrolType(control, controlType);
	}
	/*--------组件取类型--------*/
	int ControlEx::GetcontrolType()
	{
		return ControlGetcontrolType(control);
	}
	/*--------组件置标识--------*/
	void ControlEx::Sign(AutoInt sign)
	{
		ControlSetsign(control, sign);
	}
	/*--------组件取标识--------*/
	AutoInt ControlEx::Sign()
	{
		return ControlGetsign(control);
	}
	/*--------组件置回调--------*/
	ExuiCallback ControlEx::SetCallback(int mode, ExuiCallback Callback)
	{
		switch (mode)
		{
		case 101:
			return (ExuiCallback)ControlSetCallback(control, mode, (Callback == (ExuiCallback)1) ? (ExuiCallback)Exui_EventPro_PublicEvent_ControlEx : (ExuiCallback)Callback);
			break;
		case 102:
			return (ExuiCallback)ControlSetCallback(control, mode, (Callback == (ExuiCallback)1) ? (ExuiCallback)Exui_EventPro_PrivateEvent_ControlEx : (ExuiCallback)Callback);
			break;
		default:
			break;
		}
		return NULL;
	}
	/*--------组件取回调--------*/
	ExuiCallback ControlEx::GetCallback(int Mode)
	{
		return ControlGetCallback(control, Mode);
	}
	/*--------组件调用回调--------*/
	AutoInt ControlEx::InvokeCallback(AutoInt msg, AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4, ExuiCallback ExuiCallback)
	{
		return ControInvokeCallback(control, msg, Parameter1, Parameter2, Parameter3, Parameter4, ExuiCallback);
	}
	/*--------组件取所在窗口句柄--------*/
	HWND ControlEx::GetWindow()
	{
		return ControlGetWindow(control);
	}
	/*--------组件取组件绑定窗口--------*/
	HWND ControlEx::GetBindWin()
	{
		return ControlGetBindWin(control);
	}
	/*--------组件取窗口绑定组件--------*/
	Control_P ControlEx::GetBindControl(HWND WindowHandle)
	{
		return ControlGetBindControl(WindowHandle);
	}
	/*--------组件取设备上下文--------*/
	HDC ControlEx::GetDc()
	{
		return ControlGetDc(control);
	}
	/*--------组件取图形--------*/
	AutoInt ControlEx::GetGraphics()
	{
		return ControlGetGraphics(control);
	}
	/*--------组件取位图--------*/
	AutoInt ControlEx::GetHBITMAP()
	{
		return ControlGetHBITMAP(control);
	}
	/*--------组件添加重绘区--------*/
	void ControlEx::AddRedrawRect(int Left, int Top, int Width, int Height)
	{
		ControlAddRedrawRect(control, Left, Top, Width, Height);
	}
	/*--------组件请求重绘--------*/
	void ControlEx::Redraw()
	{
		ControlRedraw(control);
	}
	/*--------组件暂停重画--------*/
	int ControlEx::LockUpdate()
	{
		return ControlLockUpdate(control);
	}
	/*--------组件容许重画--------*/
	int ControlEx::UnlockUpdate()
	{
		return ControlUnlockUpdate(control);
	}
	/*--------组件取重画锁定计数--------*/
	int ControlEx::GetLockUpdateCount()
	{
		return ControlGetLockUpdateCount(control);
	}
	/*--------组件取父组件--------*/
	Control_P ControlEx::GetParentControl()
	{
		return ControlGetParentControl(control);
	}
	/*--------组件置父组件--------*/
	BOOL ControlEx::SetParentControl(Control_P Parentcontrol, BOOL mode, int NewLeft, int NewTop)
	{
		return ControlSetParentControl(control, Parentcontrol, mode, NewLeft, NewTop);
	}
	/*--------组件可有子级--------*/
	BOOL ControlEx::HaveChild()
	{
		return ControlHaveChild(control);
	}
	/*--------组件取子级组件数--------*/
	int ControlEx::GetChildCount(BOOL Mode)
	{
		return ControlGetChildCount(control, Mode);
	}
	/*--------组件枚举子级组件--------*/
	BindataEx ControlEx::EnumerateChild(BOOL Mode)
	{
		return ControlEnumerateChild(control, Mode);
	}
	/*--------组件取上一组件--------*/
	Control_P ControlEx::GetLastcontrol()
	{
		return ControlGetLastcontrol(control);
	}
	/*--------组件取下一组件--------*/
	Control_P ControlEx::GetNextcontrol()
	{
		return ControlGetNextcontrol(control);
	}
	/*--------组件取嵌套层次--------*/
	int ControlEx::GetNestingLevel()
	{
		return ControlGetNestingLevel(control);
	}
	/*--------组件取层次组件--------*/
	Control_P ControlEx::Getlevelcontrol(AutoInt level)
	{
		return ControlGetlevelcontrol(level);
	}
	/*--------组件置层次--------*/
	void ControlEx::Setlevel(int mode, Control_P RelativeControl)
	{
		ControlSetlevel(control, mode, RelativeControl);
	}
	/*--------组件取层次--------*/
	int ControlEx::Getlevel()
	{
		return ControlGetlevel(control);
	}
	/*--------组件置所在窗口分层透明--------*/
	void ControlEx::SetWinLayered(int Layered)
	{
		ControlSetWinLayered(control, Layered);
	}
	/*--------组件取所在窗口分层透明--------*/
	AutoInt ControlEx::GetWinLayered()
	{
		return ControlGetWinLayered(control);
	}
	/*--------组件取所在窗口图形--------*/
	AutoInt ControlEx::GetWinGraphics()
	{
		return ControlGetWinGraphics(control);
	}
	/*--------组件取所在窗口位图--------*/
	HBITMAP ControlEx::GetWinHBITMAP()
	{
		return ControlGetWinHBITMAP(control);
	}
	/*--------组件取所在窗口设备上下文--------*/
	HDC ControlEx::GetWinDc()
	{
		return ControlGetWinDc(control);
	}
	/*--------组件置所在窗口刷新回调--------*/
	AutoInt ControlEx::SetWinRefreshCallBack(AutoInt CallBack)
	{
		return (AutoInt)ControlSetWinRefreshCallBack(control, (CallBack == (AutoInt)1) ? (AutoInt)Exui_EventPro_ParentWinRefreshEvent_ControlEx : (AutoInt)CallBack);

	}
	/*--------组件取所在窗口刷新回调--------*/
	AutoInt ControlEx::GetWinRefreshCallBack()
	{
		return ControlGetWinRefreshCallBack(control);
	}
	/*--------组件刷新所在窗口--------*/
	void ControlEx::RefreshWin()
	{
		ControlRefreshWin(control);
	}
	/*--------组件暂停重画所在窗口--------*/
	int ControlEx::LockWinUpdate()
	{
		return ControlLockWinUpdate(control);
	}
	/*--------组件容许重画所在窗口--------*/
	int ControlEx::UnlockWinUpdate()
	{
		return ControlUnlockWinUpdate(control);
	}
	/*--------组件取所在窗口重画锁定状态--------*/
	int ControlEx::GetWinLockUpdateCount()
	{
		return ControlGetWinLockUpdateCount(control);
	}
	/*--------组件取所在页面--------*/
	int ControlEx::GetPage()
	{
		return ControlGetPage(control);
	}
	/*--------组件置所在页面--------*/
	void ControlEx::SetPage(int Page)
	{
		ControlSetPage(control, Page);
	}
	/*--------组件取当前页面--------*/
	int ControlEx::GetCurrentPage()
	{
		return ControlGetCurrentPage(control);
	}
	/*--------组件置当前页面--------*/
	void ControlEx::SetCurrentPage(int Page)
	{
		ControlSetCurrentPage(control, Page);
	}
	/*--------组件取布局器--------*/
	BindataEx ControlEx::GetLayoutConfig()
	{
		return ControlGetLayoutConfig(control);
	}
	/*--------组件置布局器--------*/
	void ControlEx::SetLayoutConfig(BindataEx LayoutConfig)
	{
		ControlSetLayoutConfig(control, LayoutConfig);
	}
	/*--------组件提交任务--------*/
	AutoInt ControlEx::SubmitTask(AutoInt type, AutoInt cmd, AutoInt sign, AutoInt Parame)
	{
		return (AutoInt)ControlSubmitTask(control, type, (cmd == (AutoInt)1) ? (AutoInt)Exui_EventPro_TaskEvent_ControlEx : (AutoInt)cmd, sign, Parame);

	}
	/*--------组件执行元素命令--------*/
	AutoInt ControlEx::RunElemCmd(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt Cmd, AutoInt parameter1, AutoInt parameter2, AutoInt parameter3, AutoInt parameter4, AutoInt parameter5, AutoInt parameter6, AutoInt parameter7, AutoInt parameter8, AutoInt parameter9, AutoInt parameter10)
	{
		return ControlRunElemCmd(control, Type, index, ColumnId, CtrId, Cmd, parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10);
	}
	/*--------置底层元素数量--------*/
	void ControlEx::SetUnderlyElemCount(AutoInt Type, AutoInt cloid, AutoInt Num)
	{
		ControlSetUnderlyElemCount(control, Type, cloid, Num);
	}
	/*--------取底层元素数量--------*/
	AutoInt ControlEx::GetUnderlyElemCount(AutoInt Type, AutoInt cloid)
	{
		return ControlGetUnderlyElemCount(control, Type, cloid);
	}
	/*--------组件插入元素--------*/
	AutoInt ControlEx::InsertElem(AutoInt Type, AutoInt cloid, AutoInt ctrlindex, AutoInt addnum)
	{
		return ControlInsertElem(control, Type, cloid, ctrlindex, addnum);
	}
	/*--------组件删除元素--------*/
	void ControlEx::DeleteElem(AutoInt Type, AutoInt cloid, AutoInt delindex, AutoInt deletenum)
	{
		ControlDeleteElem(control, Type, cloid, delindex, deletenum);
	}
	/*--------组件取元素数量--------*/
	AutoInt ControlEx::GetElemCount(AutoInt Type, AutoInt cloid)
	{
		return ControlGetElemCount(control, Type, cloid);
	}
	/*--------组件重置元素--------*/
	void ControlEx::ResetElem(AutoInt Type, AutoInt cloid, AutoInt delindex, AutoInt deletenum)
	{
		ControlResetElem(control, Type, cloid, delindex, deletenum);
	}
	/*--------组件置元素属性_图片--------*/
	void ControlEx::SetElemAttribute_Imsge(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle)
	{
		ControlSetElemAttribute_Imsge(control, Type, ColumnId, CtrId, AttributeId, (void*)Exui_EventPro_ElemEvent_ControlEx, Layout, left, top, width, Height, Ctrstyle);
	}
	/*--------组件取元素属性_图片--------*/
	AutoInt ControlEx::GetElemAttribute_Imsge(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId)
	{
		return ControlGetElemAttribute_Imsge(control, Type, ColumnId, CtrId, AttributeId);
	}
	/*--------组件置元素内容_图片--------*/
	void ControlEx::SetElemData_Imsge(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, BitmapEx IcoData, int Align, int PlayAnimation)
	{
		ControlSetElemData_Imsge(control, Type, index, ColumnId, CtrId, AttributeId, Show, Ban, IcoData, Align, PlayAnimation);
	}
	/*--------组件取元素内容_图片--------*/
	AutoInt ControlEx::GetElemData_Imsge(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId)
	{
		return ControlGetElemData_Imsge(control, Type, index, ColumnId, CtrId, DataId);
	}
	/*--------组件置元素属性_文本--------*/
	void ControlEx::SetElemAttribute_Text(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle, BindataEx Font, int Align)
	{
		ControlSetElemAttribute_Text(control, Type, ColumnId, CtrId, AttributeId, (void*)Exui_EventPro_ElemEvent_ControlEx, Layout, left, top, width, Height, Ctrstyle, Font, Align);
	}
	/*--------组件取元素属性_文本--------*/
	AutoInt ControlEx::GetElemAttribute_Text(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId)
	{
		return ControlGetElemAttribute_Text(control, Type, ColumnId, CtrId, AttributeId);
	}
	/*--------组件置元素内容_文本--------*/
	void ControlEx::SetElemData_Text(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, StringEx Title, int FontColor)
	{
		ControlSetElemData_Text(control, Type, index, ColumnId, CtrId, AttributeId, Show, Ban, Title, FontColor);
	}
	/*--------组件取元素内容_文本--------*/
	AutoInt ControlEx::GetElemData_Text(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId)
	{
		return ControlGetElemData_Text(control, Type, index, ColumnId, CtrId, DataId);
	}
	/*--------组件置元素属性_按钮--------*/
	void ControlEx::SetElemAttribute_Button(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle, BindataEx Skin, BindataEx Font)
	{
		ControlSetElemAttribute_Button(control, Type, ColumnId, CtrId, AttributeId, (void*)Exui_EventPro_ElemEvent_ControlEx, Layout, left, top, width, Height, Ctrstyle, Skin, Font);
	}
	/*--------组件取元素属性_按钮--------*/
	AutoInt ControlEx::GetElemAttribute_Button(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId)
	{
		return ControlGetElemAttribute_Button(control, Type, ColumnId, CtrId, AttributeId);
	}
	/*--------组件置元素内容_按钮--------*/
	void ControlEx::SetElemData_Button(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, StringEx Title, int FontColor, BOOL Select)
	{
		ControlSetElemData_Button(control, Type, index, ColumnId, CtrId, AttributeId, Show, Ban, Title, FontColor, Select);
	}
	/*--------组件取元素内容_按钮--------*/
	AutoInt ControlEx::GetElemData_Button(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId)
	{
		return ControlGetElemData_Button(control, Type, index, ColumnId, CtrId, DataId);
	}
	/*--------组件置元素属性_选择框--------*/
	void ControlEx::SetElemAttribute_Select(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle, BindataEx Skin, BindataEx Font, int IconWidth, int IconHeight, int SelectMode)
	{
		ControlSetElemAttribute_Select(control, Type, ColumnId, CtrId, AttributeId, (void*)Exui_EventPro_ElemEvent_ControlEx, Layout, left, top, width, Height, Ctrstyle, Skin, Font, IconWidth, IconHeight, SelectMode);
	}
	/*--------组件取元素属性_选择框--------*/
	AutoInt ControlEx::GetElemAttribute_Select(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId)
	{
		return ControlGetElemAttribute_Select(control, Type, ColumnId, CtrId, AttributeId);
	}
	/*--------组件置元素内容_选择框--------*/
	void ControlEx::SetElemData_Select(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, StringEx Title, int FontColor, int Select)
	{
		ControlSetElemData_Select(control, Type, index, ColumnId, CtrId, AttributeId, Show, Ban, Title, FontColor, Select);
	}
	/*--------组件取元素内容_选择框--------*/
	AutoInt ControlEx::GetElemData_Select(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId)
	{
		return ControlGetElemData_Select(control, Type, index, ColumnId, CtrId, DataId);
	}
	/*--------组件置元素属性_编辑文本--------*/
	void ControlEx::SetElemAttribute_EditText(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle, int ShowMode, BindataEx Skin, BindataEx Font, int Align, StringEx PasswordSubstitution, BindataEx Cursor, BOOL Multiline, int InputMode, int MaxInput, BindataEx EditFont, int EditFontColor, int CursorColor, int SelectedFontColor, int SelectedColor, int MenuTableWidth, int MenuTableHeight, BindataEx MenuFont, int MenuFontClour, int MenuDisabledFontClour, int MenuLanguage)
	{
		ControlSetElemAttribute_EditText(control, Type, ColumnId, CtrId, AttributeId, (void*)Exui_EventPro_ElemEvent_ControlEx, Layout, left, top, width, Height, Ctrstyle, ShowMode, Skin, Font, Align, PasswordSubstitution, Cursor, Multiline, InputMode, MaxInput, EditFont, EditFontColor, CursorColor, SelectedFontColor, SelectedColor, MenuTableWidth, MenuTableHeight, MenuFont, MenuFontClour, MenuDisabledFontClour, MenuLanguage);
	}
	/*--------组件取元素属性_编辑文本--------*/
	AutoInt ControlEx::GetElemAttribute_EditText(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId)
	{
		return ControlGetElemAttribute_EditText(control, Type, ColumnId, CtrId, AttributeId);
	}
	/*--------组件置元素内容_编辑文本--------*/
	void ControlEx::SetElemData_EditText(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, StringEx Content, int FontClour)
	{
		ControlSetElemData_EditText(control, Type, index, ColumnId, CtrId, AttributeId, Show, Ban, Content, FontClour);
	}
	/*--------组件取元素内容_编辑文本--------*/
	AutoInt ControlEx::GetElemData_EditText(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId)
	{
		return ControlGetElemData_EditText(control, Type, index, ColumnId, CtrId, DataId);
	}
	/*--------组件置元素属性_组合按钮--------*/
	void ControlEx::SetElemAttribute_ComboButton(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle, BindataEx Skin, BindataEx Font, int IconWidth, int IconHeight, int Align)
	{
		ControlSetElemAttribute_ComboButton(control, Type, ColumnId, CtrId, AttributeId, (void*)Exui_EventPro_ElemEvent_ControlEx, Layout, left, top, width, Height, Ctrstyle, Skin, Font, IconWidth, IconHeight, Align);
	}
	/*--------组件取元素属性_组合按钮--------*/
	AutoInt ControlEx::GetElemAttribute_ComboButton(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId)
	{
		return ControlGetElemAttribute_ComboButton(control, Type, ColumnId, CtrId, AttributeId);
	}
	/*--------组件置元素内容_组合按钮--------*/
	void ControlEx::SetElemData_ComboButton(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, int BackColor, BitmapEx image, StringEx Title, int FontColor, int PartnerStay)
	{
		ControlSetElemData_ComboButton(control, Type, index, ColumnId, CtrId, AttributeId, Show, Ban, BackColor, image, Title, FontColor, PartnerStay);
	}
	/*--------组件取元素内容_组合按钮--------*/
	AutoInt ControlEx::GetElemData_ComboButton(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId)
	{
		return ControlGetElemData_ComboButton(control, Type, index, ColumnId, CtrId, DataId);
	}
	/*--------组件置元素属性_进度条--------*/
	void ControlEx::SetElemAttribute_Progressbar(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle, BindataEx Skin, int Style)
	{
		ControlSetElemAttribute_Progressbar(control, Type, ColumnId, CtrId, AttributeId, (void*)Exui_EventPro_ElemEvent_ControlEx, Layout, left, top, width, Height, Ctrstyle, Skin, Style);
	}
	/*--------组件取元素属性_进度条--------*/
	AutoInt ControlEx::GetElemAttribute_Progressbar(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId)
	{
		return ControlGetElemAttribute_Progressbar(control, Type, ColumnId, CtrId, AttributeId);
	}
	/*--------组件置元素内容_进度条--------*/
	void ControlEx::SetElemData_Progressbar(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, int Position)
	{
		ControlSetElemData_Progressbar(control, Type, index, ColumnId, CtrId, AttributeId, Show, Ban, Position);
	}
	/*--------组件取元素内容_进度条--------*/
	AutoInt ControlEx::GetElemData_Progressbar(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId)
	{
		return ControlGetElemData_Progressbar(control, Type, index, ColumnId, CtrId, DataId);
	}
	/*--------组件置元素属性_滑块条--------*/
	void ControlEx::SetElemAttribute_Sliderbar(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle, BindataEx Skin, int Style)
	{
		ControlSetElemAttribute_Sliderbar(control, Type, ColumnId, CtrId, AttributeId, (void*)Exui_EventPro_ElemEvent_ControlEx, Layout, left, top, width, Height, Ctrstyle, Skin, Style);
	}
	/*--------组件取元素属性_滑块条--------*/
	AutoInt ControlEx::GetElemAttribute_Sliderbar(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId)
	{
		return ControlGetElemAttribute_Sliderbar(control, Type, ColumnId, CtrId, AttributeId);
	}
	/*--------组件置元素内容_滑块条--------*/
	void ControlEx::SetElemData_Sliderbar(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, int Position)
	{
		ControlSetElemData_Sliderbar(control, Type, index, ColumnId, CtrId, AttributeId, Show, Ban, Position);
	}
	/*--------组件取元素内容_滑块条--------*/
	AutoInt ControlEx::GetElemData_Sliderbar(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId)
	{
		return ControlGetElemData_Sliderbar(control, Type, index, ColumnId, CtrId, DataId);
	}
	/*--------组件置元素属性_颜色--------*/
	void ControlEx::SetElemAttribute_Colour(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle)
	{
		ControlSetElemAttribute_Colour(control, Type, ColumnId, CtrId, AttributeId, (void*)Exui_EventPro_ElemEvent_ControlEx, Layout, left, top, width, Height, Ctrstyle);
	}
	/*--------组件取元素属性_颜色--------*/
	AutoInt ControlEx::GetElemAttribute_Colour(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId)
	{
		return ControlGetElemAttribute_Colour(control, Type, ColumnId, CtrId, AttributeId);
	}
	/*--------组件置元素内容_颜色--------*/
	void ControlEx::SetElemData_Colour(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, int style, int Colour)
	{
		ControlSetElemData_Colour(control, Type, index, ColumnId, CtrId, AttributeId, Show, Ban, style, Colour);
	}
	/*--------组件取元素内容_颜色--------*/
	AutoInt ControlEx::GetElemData_Colour(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId)
	{
		return ControlGetElemData_Colour(control, Type, index, ColumnId, CtrId, DataId);
	}
	/*--------组件置元素属性_直线--------*/
	void ControlEx::SetElemAttribute_Line(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle, int LineWidth)
	{
		ControlSetElemAttribute_Line(control, Type, ColumnId, CtrId, AttributeId, (void*)Exui_EventPro_ElemEvent_ControlEx, Layout, left, top, width, Height, Ctrstyle, LineWidth);
	}
	/*--------组件取元素属性_直线--------*/
	AutoInt ControlEx::GetElemAttribute_Line(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId)
	{
		return ControlGetElemAttribute_Line(control, Type, ColumnId, CtrId, AttributeId);
	}
	/*--------组件置元素内容_直线--------*/
	void ControlEx::SetElemData_Line(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, int style, int Colour)
	{
		ControlSetElemData_Line(control, Type, index, ColumnId, CtrId, AttributeId, Show, Ban, style, Colour);
	}
	/*--------组件取元素内容_直线--------*/
	AutoInt ControlEx::GetElemData_Line(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId)
	{
		return ControlGetElemData_Line(control, Type, index, ColumnId, CtrId, DataId);
	}
	/*--------组件置元素属性_自定义--------*/
	void ControlEx::SetElemAttribute_Custom(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle, BindataEx CustomTag, BindataEx CustomElemAttr9, BindataEx CustomElemAttr10, BindataEx CustomElemAttr11, BindataEx CustomElemAttr12, BindataEx CustomElemAttr13, BindataEx CustomElemAttr14, BindataEx CustomElemAttr15, BindataEx CustomElemAttr16, BindataEx CustomElemAttr17, BindataEx CustomElemAttr18, BindataEx CustomElemAttr19, BindataEx CustomElemAttr20, BindataEx CustomElemAttr21, BindataEx CustomElemAttr22, BindataEx CustomElemAttr23, BindataEx CustomElemAttr24, BindataEx CustomElemAttr25, BindataEx CustomElemAttr26, BindataEx CustomElemAttr27, BindataEx CustomElemAttr28, BindataEx CustomElemAttr29, BindataEx CustomElemAttr30, BindataEx CustomElemAttr31, BindataEx CustomElemAttr32)
	{
		ControlSetElemAttribute_Custom(control, Type, ColumnId, CtrId, AttributeId, (void*)Exui_EventPro_ElemEvent_ControlEx, Layout, left, top, width, Height, Ctrstyle, CustomTag, CustomElemAttr9, CustomElemAttr10, CustomElemAttr11, CustomElemAttr12, CustomElemAttr13, CustomElemAttr14, CustomElemAttr15, CustomElemAttr16, CustomElemAttr17, CustomElemAttr18, CustomElemAttr19, CustomElemAttr20, CustomElemAttr21, CustomElemAttr22, CustomElemAttr23, CustomElemAttr24, CustomElemAttr25, CustomElemAttr26, CustomElemAttr27, CustomElemAttr28, CustomElemAttr29, CustomElemAttr30, CustomElemAttr31, CustomElemAttr32);
	}
	/*--------组件取元素属性_自定义--------*/
	AutoInt ControlEx::GetElemAttribute_Custom(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId)
	{
		return ControlGetElemAttribute_Custom(control, Type, ColumnId, CtrId, AttributeId);
	}
	/*--------组件置元素内容_自定义--------*/
	void ControlEx::SetElemData_Custom(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, BindataEx CustomElemData3, BindataEx CustomElemData4, BindataEx CustomElemData5, BindataEx CustomElemData6, BindataEx CustomElemData7, BindataEx CustomElemData8, BindataEx CustomElemData9, BindataEx CustomElemData10, BindataEx CustomElemData11, BindataEx CustomElemData12, BindataEx CustomElemData13, BindataEx CustomElemData14, BindataEx CustomElemData15, BindataEx CustomElemData16, BindataEx CustomElemData17, BindataEx CustomElemData18, BindataEx CustomElemData19, BindataEx CustomElemData20, BindataEx CustomElemData21, BindataEx CustomElemData22, BindataEx CustomElemData23, BindataEx CustomElemData24, BindataEx CustomElemData25, BindataEx CustomElemData26, BindataEx CustomElemData27, BindataEx CustomElemData28, BindataEx CustomElemData29, BindataEx CustomElemData30, BindataEx CustomElemData31, BindataEx CustomElemData32)
	{
		ControlSetElemData_Custom(control, Type, index, ColumnId, CtrId, AttributeId, Show, Ban, CustomElemData3, CustomElemData4, CustomElemData5, CustomElemData6, CustomElemData7, CustomElemData8, CustomElemData9, CustomElemData10, CustomElemData11, CustomElemData12, CustomElemData13, CustomElemData14, CustomElemData15, CustomElemData16, CustomElemData17, CustomElemData18, CustomElemData19, CustomElemData20, CustomElemData21, CustomElemData22, CustomElemData23, CustomElemData24, CustomElemData25, CustomElemData26, CustomElemData27, CustomElemData28, CustomElemData29, CustomElemData30, CustomElemData31, CustomElemData32);
	}
	/*--------组件取元素内容_自定义--------*/
	AutoInt ControlEx::GetElemData_Custom(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId)
	{
		return ControlGetElemData_Custom(control, Type, index, ColumnId, CtrId, DataId);
	}
	/*--------组件置元素属性_组件--------*/
	void ControlEx::SetElemAttribute_Control(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, int Layout, int left, int top, int width, int Height, int Ctrstyle)
	{
		ControlSetElemAttribute_Control(control, Type, ColumnId, CtrId, AttributeId, (void*)Exui_EventPro_ElemEvent_ControlEx, Layout, left, top, width, Height, Ctrstyle);
	}
	/*--------组件取元素属性_组件--------*/
	AutoInt ControlEx::GetElemAttribute_Control(AutoInt Type, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId)
	{
		return ControlGetElemAttribute_Control(control, Type, ColumnId, CtrId, AttributeId);
	}
	/*--------组件置元素内容_组件--------*/
	void ControlEx::SetElemData_Control(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt AttributeId, BOOL Show, BOOL Ban, BindataEx UserData, int Mode, Control_P Handle, BindataEx  Data)
	{
		ControlSetElemData_Control(control, Type, index, ColumnId, CtrId, AttributeId, Show, Ban, UserData, Mode, Handle, Data);
	}
	/*--------组件取元素内容_组件--------*/
	AutoInt ControlEx::GetElemData_Control(AutoInt Type, AutoInt index, AutoInt ColumnId, AutoInt CtrId, AutoInt DataId)
	{
		return ControlGetElemData_Control(control, Type, index, ColumnId, CtrId, DataId);
	}
}
//TimerEx
namespace exui
{
	TimerEx::TimerEx()
	{

	}
	TimerEx::~TimerEx()
	{

	}
	/*--------创建定时器--------*/
	TimeEx_P TimerEx::Create(BaseEx& Parent, AutoInt Period, AutoInt Tag, BOOL Reentry, AutoInt Flags)
	{
		InitCreateAttrStar(Exui_EventPro_TimerEx);
		control = (Control_P)CreateTimerEx(Period, (TimeExCallBack)-1, (AutoInt)InitAttr, Reentry, Flags);
		InitCreateAttrEnd(false);
		return (TimeEx_P)control;
	}
	/*--------销毁定时器--------*/
	void TimerEx::Delete()
	{
		DeleteTimerEx((TimeEx_P)control);
		control = NULL;
	}
	/*--------置定时器配置--------*/
	void TimerEx::SetConfig(AutoInt type, AutoInt value)
	{
		SetConfigTimerEx((TimeEx_P)control, type, value);
	}
	/*--------取定时器配置--------*/
	AutoInt TimerEx::GetConfig(AutoInt type)
	{
		return GetConfigTimerEx((TimeEx_P)control, type);
	}
}
//SlowMotionTaskEx
namespace exui
{
	SlowMotionTaskEx::SlowMotionTaskEx()
	{

	}
	SlowMotionTaskEx::~SlowMotionTaskEx()
	{

	}
	/*--------创建缓动任务--------*/
	SlowMotionTaskEx_P SlowMotionTaskEx::Create(BaseEx& Parent, AutoInt Num, AutoInt Mode, AutoInt Star, AutoInt End, AutoInt FrameDelay, AutoInt Step, AutoInt Tag, BOOL Wait, BOOL ManualDestroy)
	{
		InitCreateAttrStar(Exui_EventPro_SlowMotionTaskEx);
		CreateSlowMotionTaskEx(Num, Mode, Star, End, FrameDelay, Step, (SlowMotionCallBack)-1, (AutoInt)InitAttr, Wait, ManualDestroy);
		InitCreateAttrEnd(false);
		return (HWND)control;
	}
	/*--------置缓动任务配置--------*/
	void SlowMotionTaskEx::SetTaskConfigEx(AutoInt index, AutoInt Mode, AutoInt Star, AutoInt End)
	{
		SetSlowMotionTaskConfigEx((SlowMotionTaskEx_P)control, index, Mode, Star, End);
	}
	/*--------执行缓动任务命令--------*/
	AutoInt SlowMotionTaskEx::RunCmd(AutoInt Cmd, AutoInt index, AutoInt param1, AutoInt param2, AutoInt param3, AutoInt param4)
	{
		return RunSlowMotionTaskCmd((SlowMotionTaskEx_P)control, Cmd, index, param1, param2, param3, param4);
	}
	/*--------销毁缓动队列任务--------*/
	void SlowMotionTaskEx::Destroy()
	{
		DestroySlowMotionTaskEx((SlowMotionTaskEx_P)control);
	}
}
//MenuEx
namespace exui
{
	MenuEx::MenuEx()
	{

	}
	MenuEx::~MenuEx()
	{

	}
	/*--------创建_菜单Ex--------*/
	MenuEx_P MenuEx::Create(BaseEx& Parent, int IconWidth, int IconHeight, BindataEx Skin, BindataEx Font, BindataEx Cursor, int Transparency, AutoInt Tag)
	{
		InitCreateAttrStar(Exui_EventPro_MenuEx);
		Create_MenuEx(IconWidth, IconHeight, Skin, Font, Cursor, Transparency, (void*)-1, (AutoInt)InitAttr);
		InitCreateAttrEnd(false);
		return (HWND)control;

	}
	/*--------置属性_菜单Ex--------*/
	void MenuEx::SetAttribute(AutoInt AttributeId, int IconWidth, int IconHeight, BindataEx Skin, BindataEx Font, BindataEx Cursor, int Transparency, AutoInt MenuTag)
	{
		SetAttribute_MenuEx((HWND)control, AttributeId, IconWidth, IconHeight, Skin, Font, Cursor, Transparency, (void*)-1, MenuTag);
	}
	/*--------取属性_菜单Ex--------*/
	AutoInt MenuEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_MenuEx((HWND)control, index);
	}
	/*--------销毁_菜单Ex--------*/
	void MenuEx::Destroy()
	{
		Destroy_MenuEx((HWND)control);
	}
	/*--------是否已弹出_菜单Ex--------*/
	BOOL MenuEx::IsPopUp(MenuInfoEx_P MenuId)
	{
		return IsPopUp_MenuEx((HWND)control, MenuId);
	}
	/*--------弹出_菜单Ex--------*/
	void MenuEx::PopUp(MenuInfoEx_P MenuId, int left, int top, HWND hWndParent, BOOL Modal, Control_P ExceptionControl, int ReferType, Control_P ReferControl)
	{
		PopUp_MenuEx((HWND)control, MenuId, left, top, hWndParent, Modal, ExceptionControl, ReferType, ReferControl);
	}
	/*--------关闭_菜单Ex--------*/
	void MenuEx::Close(MenuInfoEx_P MenuIdl, BOOL Mode)
	{
		Close_MenuEx((HWND)control, MenuIdl, Mode);
	}
	/*--------插入_菜单Ex--------*/
	AutoInt MenuEx::InsertItem(MenuInfoEx_P ParentMenu, AutoInt index, AutoInt Data, BitmapEx Ico, StringEx Title, int FontColor, int DisabledFontColor, int Width, int Height, int Type, BOOL Disabled)
	{
		return InsertItem_MenuEx((HWND)control, ParentMenu, index, Data, Ico, Title, FontColor, DisabledFontColor, Width, Height, Type, Disabled);
	}
	/*--------删除_菜单Ex--------*/
	void MenuEx::DeleteItem(MenuInfoEx_P MenuId)
	{
		DeleteItem_MenuEx((HWND)control, MenuId);
	}
	/*--------取数量_菜单Ex--------*/
	AutoInt MenuEx::GetItemCount()
	{
		return GetItemCount_MenuEx((HWND)control);
	}
	/*--------置图标_菜单Ex--------*/
	void MenuEx::SetItemIco(MenuInfoEx_P MenuId, BitmapEx Ico)
	{
		SetItemIco_MenuEx((HWND)control, MenuId, Ico);
	}
	/*--------取图标_菜单Ex--------*/
	BitmapEx MenuEx::GetItemIco(MenuInfoEx_P MenuId)
	{
		return GetItemIco_MenuEx((HWND)control, MenuId);
	}
	/*--------置标题_菜单Ex--------*/
	void MenuEx::SetItemTitle(MenuInfoEx_P MenuId, StringEx Title)
	{
		SetItemTitle_MenuEx((HWND)control, MenuId, Title);
	}
	/*--------取标题_菜单Ex--------*/
	StringEx MenuEx::GetItemTitle(MenuInfoEx_P MenuId)
	{
		return GetItemTitle_MenuEx((HWND)control, MenuId);
	}
	/*--------取附加值_菜单Ex--------*/
	AutoInt MenuEx::GetItemData(MenuInfoEx_P MenuId)
	{
		return GetItemData_MenuEx((HWND)control, MenuId);
	}
	/*--------置附加值_菜单Ex--------*/
	void MenuEx::SetItemData(MenuInfoEx_P MenuId, AutoInt Data)
	{
		SetItemData_MenuEx((HWND)control, MenuId, Data);
	}
	/*--------取字体色_菜单Ex--------*/
	int MenuEx::GetItemFontColor(MenuInfoEx_P MenuId)
	{
		return GetItemFontColor_MenuEx((HWND)control, MenuId);
	}
	/*--------置字体色_菜单Ex--------*/
	void MenuEx::SetItemFontColor(MenuInfoEx_P MenuId, int FontColor)
	{
		SetItemFontColor_MenuEx((HWND)control, MenuId, FontColor);
	}
	/*--------取禁止字体色_菜单Ex--------*/
	int MenuEx::GetItemDisabledFontColor(MenuInfoEx_P MenuId)
	{
		return GetItemDisabledFontColor_MenuEx((HWND)control, MenuId);
	}
	/*--------置禁止字体色_菜单Ex--------*/
	void MenuEx::SetItemDisabledFontColor(MenuInfoEx_P MenuId, int DisabledFontColor)
	{
		SetItemDisabledFontColor_MenuEx((HWND)control, MenuId, DisabledFontColor);
	}
	/*--------置项目信息_菜单Ex--------*/
	void MenuEx::SetItemInfo(MenuInfoEx_P MenuId, int Type, AutoInt Data)
	{
		SetItemInfo_MenuEx((HWND)control, MenuId, Type, Data);
	}
	/*--------取项目信息_菜单Ex--------*/
	AutoInt MenuEx::GetItemInfo(MenuInfoEx_P MenuId, int Type)
	{
		return GetItemInfo_MenuEx((HWND)control, MenuId, Type);
	}
	/*--------置类型_菜单Ex--------*/
	void MenuEx::SetItemType(MenuInfoEx_P MenuId, int Type)
	{
		SetItemType_MenuEx((HWND)control, MenuId, Type);
	}
	/*--------取类型_菜单Ex--------*/
	int MenuEx::GetItemType(MenuInfoEx_P MenuId)
	{
		return GetItemType_MenuEx((HWND)control, MenuId);
	}
	/*--------取禁止_菜单Ex--------*/
	BOOL MenuEx::GetItemDisabled(MenuInfoEx_P MenuId)
	{
		return GetItemDisabled_MenuEx((HWND)control, MenuId);
	}
	/*--------置禁止_菜单Ex--------*/
	void MenuEx::SetItemDisabled(MenuInfoEx_P MenuId, BOOL Disabled)
	{
		SetItemDisabled_MenuEx((HWND)control, MenuId, Disabled);
	}
	/*--------置项目宽度_菜单Ex--------*/
	void MenuEx::SetItemWidth(MenuInfoEx_P MenuId, int Width)
	{
		SetItemWidth_MenuEx((HWND)control, MenuId, Width);
	}
	/*--------取项目宽度_菜单Ex--------*/
	int MenuEx::GetItemWidth(MenuInfoEx_P MenuId)
	{
		return GetItemWidth_MenuEx((HWND)control, MenuId);
	}
	/*--------置项目高度_菜单Ex--------*/
	void MenuEx::SetItemHeight(MenuInfoEx_P MenuId, int Height)
	{
		SetItemHeight_MenuEx((HWND)control, MenuId, Height);
	}
	/*--------取项目高度_菜单Ex--------*/
	int MenuEx::GetItemHeight(MenuInfoEx_P MenuId)
	{
		return GetItemHeight_MenuEx((HWND)control, MenuId);
	}
	/*--------取子菜单数量_菜单Ex--------*/
	int MenuEx::GetSubItemCount(MenuInfoEx_P MenuId)
	{
		return GetSubItemCount_MenuEx((HWND)control, MenuId);
	}
	/*--------取子菜单_菜单Ex--------*/
	MenuInfoEx_P MenuEx::GetSubItem(MenuInfoEx_P MenuId, AutoInt index)
	{
		return GetSubItem_MenuEx((HWND)control, MenuId, index);
	}
}
//DownlistEx
namespace exui
{
	DownlistEx::DownlistEx()
	{

	}
	DownlistEx::~DownlistEx()
	{

	}
	/*--------创建_下拉列表Ex--------*/
	DownlistEx_P DownlistEx::Create(BaseEx& Parent, int IconWidth, int IconHeight, BindataEx Skin, BindataEx Font, BindataEx Cursor, BOOL AlternateColor, int ScrollBarMode, AutoInt CurrentSelection, int Transparency, AutoInt Tag)
	{
		InitCreateAttrStar(Exui_EventPro_DownlistEx);
		Create_DownlistEx(IconWidth, IconHeight, Skin, Font, Cursor, AlternateColor, ScrollBarMode, CurrentSelection, Transparency, (void*)-1, (AutoInt)InitAttr);
		InitCreateAttrEnd(false);
		return (HWND)control;
	}
	/*--------销毁_下拉列表Ex--------*/
	void DownlistEx::Destroy()
	{
		Destroy_DownlistEx((HWND)control);
	}
	/*--------置属性_下拉列表Ex--------*/
	void DownlistEx::SetAttribute(AutoInt AttributeId, int IconWidth, int IconHeight, BindataEx Skin, BindataEx Font, BindataEx Cursor, BOOL AlternateColor, int ScrollBarMode, int CurrentSelection, int Transparency, int ListTag)
	{
		SetAttribute_DownlistEx((HWND)control, AttributeId, IconWidth, IconHeight, Skin, Font, Cursor, AlternateColor, ScrollBarMode, CurrentSelection, Transparency, (void*)-1, ListTag);
	}
	/*--------取属性_下拉列表Ex--------*/
	AutoInt DownlistEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_DownlistEx((HWND)control, index);
	}
	/*--------弹出_下拉列表Ex--------*/
	void DownlistEx::PopUp(int X, int Y, int nWidth, int nHeight, HWND hWndParent, BOOL Modal, Control_P ExceptionControl, int ReferType, Control_P ReferControl)
	{
		PopUp_DownlistEx((HWND)control, X, Y, nWidth, nHeight, hWndParent, Modal, ExceptionControl, ReferType, ReferControl);
	}
	/*--------关闭_下拉列表Ex--------*/
	void DownlistEx::Close()
	{
		Close_DownlistEx((HWND)control);
	}
	/*--------是否已弹出_下拉列表Ex--------*/
	BOOL DownlistEx::IsPopUp()
	{
		return IsPopUp_DownlistEx((HWND)control);
	}
	/*--------插入_下拉列表Ex--------*/
	AutoInt DownlistEx::InsertItem(AutoInt index, AutoInt addnum, AutoInt Data, BitmapEx Ico, StringEx Title, int FontColor, int Size)
	{
		return InsertItem_DownlistEx((HWND)control, index, addnum, Data, Ico, Title, FontColor, Size);
	}
	/*--------删除_下拉列表Ex--------*/
	void DownlistEx::DeleteItem(AutoInt index, AutoInt deletenum)
	{
		DeleteItem_DownlistEx((HWND)control, index, deletenum);
	}
	/*--------取数量_下拉列表Ex--------*/
	AutoInt DownlistEx::GetItemCount()
	{
		return GetItemCount_DownlistEx((HWND)control);
	}
	/*--------置附加值_下拉列表Ex--------*/
	void DownlistEx::SetItemData(AutoInt index, AutoInt Data)
	{
		SetItemData_DownlistEx((HWND)control, index, Data);
	}
	/*--------取附加值_下拉列表Ex--------*/
	AutoInt DownlistEx::GetItemData(AutoInt index)
	{
		return GetItemData_DownlistEx((HWND)control, index);
	}
	/*--------置图标_下拉列表Ex--------*/
	void DownlistEx::SetItemIco(AutoInt index, BitmapEx Ico)
	{
		SetItemIco_DownlistEx((HWND)control, index, Ico);
	}
	/*--------取图标_下拉列表Ex--------*/
	BitmapEx DownlistEx::GetItemIco(AutoInt index)
	{
		return GetItemIco_DownlistEx((HWND)control, index);
	}
	/*--------置标题_下拉列表Ex--------*/
	void DownlistEx::SetItemTitle(AutoInt index, StringEx Title)
	{
		SetItemTitle_DownlistEx((HWND)control, index, Title);
	}
	/*--------取标题_下拉列表Ex--------*/
	StringEx DownlistEx::GetItemTitle(AutoInt index)
	{
		return GetItemTitle_DownlistEx((HWND)control, index);
	}
	/*--------置字体色_下拉列表Ex--------*/
	void DownlistEx::SetItemFontColor(AutoInt index, int FontColor)
	{
		SetItemFontColor_DownlistEx((HWND)control, index, FontColor);
	}
	/*--------取字体色_下拉列表Ex--------*/
	int DownlistEx::GetItemFontColor(AutoInt index)
	{
		return GetItemFontColor_DownlistEx((HWND)control, index);
	}
	/*--------置项目高度_下拉列表Ex--------*/
	void DownlistEx::SetItemSize(AutoInt index, int Size)
	{
		SetItemSize_DownlistEx((HWND)control, index, Size);
	}
	/*--------取项目高度_下拉列表Ex--------*/
	int DownlistEx::GetItemSize(AutoInt index)
	{
		return GetItemSize_DownlistEx((HWND)control, index);
	}
	/*--------保证显示_下拉列表Ex--------*/
	void DownlistEx::GuaranteeVisible(AutoInt index, int mode)
	{
		GuaranteeVisible_DownlistEx((HWND)control, index, mode);
	}
}
//WindowBoxEx
namespace exui
{
	WindowBoxEx::WindowBoxEx()
	{
		controltype = ControlType_WindowBoxEx;
	}
	WindowBoxEx::~WindowBoxEx()
	{

	}
	/*--------创建窗口--------*/
	HWND WindowBoxEx::Create(BaseEx& Parent, int mode, int Location, int X, int Y, int nWidth, int nHeight, BOOL Visual, BOOL Disabled, BindataEx Icon, StringEx Title, BOOL AlwaysTop, BOOL Taskbar, BOOL EscClose, StringEx ClassName, int classstyle, int dwExStyle, int dwStyle, AutoInt Tag)
	{
		InitCreateAttrStar(Exui_EventPro_WindowBoxEx);
		Create_WindowBoxEx(mode, Location, GetCreateParentHwnd(Parent), X, Y, nWidth, nHeight, (WNDPROC)-1, InitAttr, Visual, Disabled, Icon, Title, AlwaysTop, Taskbar, EscClose, ClassName, classstyle, dwExStyle, dwStyle, Tag);
		InitCreateAttrEnd(true);
		return (HWND)control;
	}
	/*--------是否已创建--------*/
	BOOL WindowBoxEx::IsWindow()
	{
		return IsWindow_WindowBoxEx((HWND)control);
	}
	/*--------销毁窗口--------*/
	void WindowBoxEx::Destroy()
	{
		Destroy_WindowBoxEx((HWND)control);
	}
	/*--------窗口置左边--------*/
	void WindowBoxEx::Left(int Left)
	{
		SetLeft_WindowBoxEx((HWND)control, Left);
	}
	/*--------窗口取左边--------*/
	int WindowBoxEx::Left()
	{
		return GetLeft_WindowBoxEx((HWND)control);
	}
	/*--------窗口置顶边--------*/
	void WindowBoxEx::Top(int Top)
	{
		SetTop_WindowBoxEx((HWND)control, Top);
	}
	/*--------窗口取顶边--------*/
	int WindowBoxEx::Top()
	{
		return GetTop_WindowBoxEx((HWND)control);
	}
	/*--------窗口置宽度--------*/
	void WindowBoxEx::Width(int Width)
	{
		SetWidth_WindowBoxEx((HWND)control, Width);
	}
	/*--------窗口取宽度--------*/
	int WindowBoxEx::Width()
	{
		return GetWidth_WindowBoxEx((HWND)control);
	}
	/*--------窗口置高度--------*/
	void WindowBoxEx::Height(int Height)
	{
		SetHeight_WindowBoxEx((HWND)control, Height);
	}
	/*--------窗口取高度--------*/
	int WindowBoxEx::Height()
	{
		return GetHeight_WindowBoxEx((HWND)control);
	}
	/*--------置矩形--------*/
	void WindowBoxEx::SetRect(int left, int top, int Width, int Height)
	{
		SetRect_WindowBoxEx((HWND)control, left, top, Width, Height);
	}
	/*--------取矩形--------*/
	void WindowBoxEx::GetRect(int* Left, int* Top, int* Width, int* Height)
	{
		GetRect_WindowBoxEx((HWND)control, Left, Top, Width, Height);
	}
	/*--------置位置--------*/
	void WindowBoxEx::Location(int Location)
	{
		SetLocation_WindowBoxEx((HWND)control, Location);
	}
	/*--------取位置--------*/
	int WindowBoxEx::Location()
	{
		return GetLocation_WindowBoxEx((HWND)control);
	}
	/*--------取可视--------*/
	BOOL WindowBoxEx::Visual()
	{
		return GetVisual_WindowBoxEx((HWND)control);
	}
	/*--------置可视--------*/
	void WindowBoxEx::Visual(BOOL Visual)
	{
		SetVisual_WindowBoxEx((HWND)control, Visual);
	}
	/*--------取禁止--------*/
	BOOL WindowBoxEx::Disabled()
	{
		return GetDisabled_WindowBoxEx((HWND)control);
	}
	/*--------置禁止--------*/
	void WindowBoxEx::Disabled(BOOL Disabled)
	{
		SetDisabled_WindowBoxEx((HWND)control, Disabled);
	}
	/*--------窗口置标识--------*/
	void WindowBoxEx::Sign(AutoInt Sign)
	{
		SetSign_WindowBoxEx((HWND)control, Sign);
	}
	/*--------窗口取标识--------*/
	AutoInt WindowBoxEx::Sign()
	{
		return GetSign_WindowBoxEx((HWND)control);
	}
	/*--------置父窗口--------*/
	void WindowBoxEx::SetParent(HWND Parent)
	{
		SetParent_WindowBoxEx((HWND)control, Parent);
	}
	/*--------取父窗口--------*/
	HWND WindowBoxEx::GetParent()
	{
		return GetParent_WindowBoxEx((HWND)control);
	}
	/*--------取图标--------*/
	BindataEx WindowBoxEx::Ico()
	{
		return GetIco_WindowBoxEx((HWND)control);
	}
	/*--------置图标--------*/
	void WindowBoxEx::Ico(BindataEx icon)
	{
		SetIco_WindowBoxEx((HWND)control, icon);
	}
	/*--------取标题--------*/
	StringEx WindowBoxEx::Title()
	{
		return GetTitle_WindowBoxEx((HWND)control);
	}
	/*--------置标题--------*/
	void WindowBoxEx::Title(StringEx Title)
	{
		SetTitle_WindowBoxEx((HWND)control, Title);
	}
	/*--------取风格--------*/
	int WindowBoxEx::GetStyle()
	{
		return GetStyle_WindowBoxEx((HWND)control);
	}
	/*--------置风格--------*/
	void WindowBoxEx::Setstyle(int style)
	{
		Setstyle_WindowBoxEx((HWND)control, style);
	}
	/*--------取扩展风格--------*/
	int WindowBoxEx::GetExStyle()
	{
		return GetExStyle_WindowBoxEx((HWND)control);
	}
	/*--------置扩展风格--------*/
	void WindowBoxEx::SetExStyle(int ExStyle)
	{
		SetExStyle_WindowBoxEx((HWND)control, ExStyle);
	}
	/*--------置任务栏显示--------*/
	void WindowBoxEx::Taskbar(BOOL show)
	{
		SetTaskbar_WindowBoxEx((HWND)control, show);
	}
	/*--------取任务栏显示--------*/
	BOOL WindowBoxEx::Taskbar()
	{
		return GetTaskbar_WindowBoxEx((HWND)control);
	}
	/*--------置总在最前--------*/
	void WindowBoxEx::AlwaysTop(BOOL AlwaysTop)
	{
		SetAlwaysTop_WindowBoxEx((HWND)control, AlwaysTop);
	}
	/*--------取总在最前--------*/
	BOOL WindowBoxEx::AlwaysTop()
	{
		return GetAlwaysTop_WindowBoxEx((HWND)control);
	}
	/*--------置退出键关闭--------*/
	void WindowBoxEx::EscClose(BOOL EscClose)
	{
		SetEscClose_WindowBoxEx((HWND)control, EscClose);
	}
	/*--------取退出键关闭--------*/
	BOOL WindowBoxEx::EscClose()
	{
		return GetEscClose_WindowBoxEx((HWND)control);
	}
	/*--------启用文件拖放--------*/
	void WindowBoxEx::EnableDragDrop()
	{
		EnableDragDrop_WindowBoxEx((HWND)control);
	}
	/*--------关闭文件拖放--------*/
	void WindowBoxEx::CloseDragDrop()
	{
		CloseDragDrop_WindowBoxEx((HWND)control);
	}
	/*--------置焦点--------*/
	void WindowBoxEx::SetFocus()
	{
		SetFocus_WindowBoxEx((HWND)control);
	}
	/*--------可有焦点--------*/
	BOOL WindowBoxEx::GetFocus()
	{
		return GetFocus_WindowBoxEx((HWND)control);
	}
	/*--------调整层次--------*/
	void WindowBoxEx::SetLevel(int Level)
	{
		SetLevel_WindowBoxEx((HWND)control, Level);
	}
	/*--------置托盘图标--------*/
	void WindowBoxEx::SetNotifyIcon(BindataEx Icon, StringEx Tip)
	{
		SetNotifyIcon_WindowBoxEx((HWND)control, Icon, Tip);
	}
	/*--------发送消息--------*/
	AutoInt WindowBoxEx::SendMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		return SendMessage_WindowBoxEx((HWND)control, uMsg, wParam, lParam);
	}
	/*--------投递消息--------*/
	AutoInt WindowBoxEx::PostMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		return PostMessage_WindowBoxEx((HWND)control, uMsg, wParam, lParam);
	}
	/*--------消息循环--------*/
	void WindowBoxEx::MessageLoop()
	{
		MessageLoop_WindowBoxEx((HWND)control);
	}
}
//WindowEx
namespace exui
{
	WindowEx::WindowEx()
	{

	}
	WindowEx::~WindowEx()
	{

	}


	/*--------创建组件_窗口EX--------*/
	WindowEx_P WindowEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, BindataEx Icon, StringEx Text, BindataEx Font, int FontClour, int LayeredTransparency, int DragPositionMode, int DragSizeMode, int MaxMode, BindataEx ButtonData, int AnimationParam, BindataEx ElemeData, BindataEx Layout)
	{

		InitCreateAttrStar(Exui_EventPro_WindowEx);
		CreateControl_WindowEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, Icon, Text, Font, FontClour, LayeredTransparency, DragPositionMode, DragSizeMode, MaxMode, ButtonData, AnimationParam, ElemeData, Layout);
		InitCreateAttrEnd(false);
		return control;

	}
	/*--------置属性_窗口EX--------*/
	void WindowEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_WindowEx(control, index, attribute);
	}
	/*--------取属性_窗口EX--------*/
	AutoInt WindowEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_WindowEx(control, index);
	}
	/*--------插入控制纽_窗口EX--------*/
	AutoInt WindowEx::InsertButton(AutoInt index, AutoInt Data, int Type, BindataEx skin, BOOL Visual, BOOL Disabled, BOOL Selected)
	{
		return InsertButton_WindowEx(control, index, Data, Type, skin, Visual, Disabled, Selected);
	}
	/*--------删除_窗口EX--------*/
	void WindowEx::DeleteButton(AutoInt index)
	{
		DeleteButton_WindowEx(control, index);
	}
	/*--------置类型_窗口EX--------*/
	void WindowEx::SetButtonType(AutoInt index, int Type)
	{
		SetButtonType_WindowEx(control, index, Type);
	}
	/*--------取类型_窗口EX--------*/
	int WindowEx::GetButtonType(AutoInt index)
	{
		return GetButtonType_WindowEx(control, index);
	}
	/*--------置按钮皮肤_窗口EX--------*/
	void WindowEx::SetButtonskin(AutoInt index, BindataEx skin)
	{
		SetButtonskin_WindowEx(control, index, skin);
	}
	/*--------取按钮皮肤_窗口EX--------*/
	BindataEx WindowEx::GetButtonskin(AutoInt index)
	{
		return GetButtonskin_WindowEx(control, index);
	}
	/*--------置可视_窗口EX--------*/
	void WindowEx::SetButtonVisual(AutoInt index, BOOL Show)
	{
		SetButtonVisual_WindowEx(control, index, Show);
	}
	/*--------取可视_窗口EX--------*/
	BOOL WindowEx::GetButtonVisual(AutoInt index)
	{
		return GetButtonVisual_WindowEx(control, index);
	}
	/*--------置禁止_窗口EX--------*/
	void WindowEx::SetButtonDisabled(AutoInt index, BOOL Disabled)
	{
		SetButtonDisabled_WindowEx(control, index, Disabled);
	}
	/*--------取禁止_窗口EX--------*/
	BOOL WindowEx::GetButtonDisabled(AutoInt index)
	{
		return GetButtonDisabled_WindowEx(control, index);
	}
	/*--------置选中_窗口EX--------*/
	void WindowEx::SetButtonSelected(AutoInt index, BOOL Selected)
	{
		SetButtonSelected_WindowEx(control, index, Selected);
	}
	/*--------取选中_窗口EX--------*/
	BOOL WindowEx::GetButtonSelected(AutoInt index)
	{
		return GetButtonSelected_WindowEx(control, index);
	}
	/*--------取数量_窗口EX--------*/
	AutoInt WindowEx::GetButtonCount()
	{
		return GetButtonCount_WindowEx(control);
	}
	/*--------取数值_窗口EX--------*/
	AutoInt WindowEx::GetButtonData(AutoInt index)
	{
		return GetButtonData_WindowEx(control, index);
	}
	/*--------置数值_窗口EX--------*/
	void WindowEx::SetButtonData(AutoInt index, AutoInt Data)
	{
		SetButtonData_WindowEx(control, index, Data);
	}
	/*--------添加组件_窗口EX--------*/
	BOOL WindowEx::InsertControl(Control_P ControlHand, int left, int top)
	{
		return InsertControl_WindowEx(control, ControlHand, left, top);
	}
	/*--------移除组件_窗口EX--------*/
	BOOL WindowEx::RemoveControl(Control_P ControlHand, Control_P Parentcontrol, int left, int top)
	{
		return RemoveControl_WindowEx(control, ControlHand, Parentcontrol, left, top);
	}
	/*--------调用反馈事件_窗口EX--------*/
	AutoInt WindowEx::CallFeedBackEvent(AutoInt Relevant1, AutoInt Relevant2, BOOL Mode)
	{
		return CallFeedBackEvent_WindowEx(control, Relevant1, Relevant2, Mode);
	}

	/*--------播放窗口动画--------*/
	BOOL WindowEx::PlayWindowAnimation(AutoInt scale, AutoInt AnimationType, AutoInt AlgorithmMode, AutoInt FrameDelay, AutoInt Step, BOOL FadeInAndOut, BOOL Wait, BOOL DestroyControl)
	{
		return	PlayWindowAnimationEx(control, scale, AnimationType, AlgorithmMode, FrameDelay, Step, FadeInAndOut, Wait, DestroyControl);

	}


	/*----------组件窗口EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx WindowEx::Skin()
	{
		return (BinEx_P)GetAttribute_WindowEx(control, WindowEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void WindowEx::Skin(BindataEx Skin)
	{
		SetAttribute_WindowEx(control, WindowEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取图标--------*/
	BindataEx WindowEx::Icon()
	{
		return (BinEx_P)GetAttribute_WindowEx(control, WindowEx_Attr_Icon);
	}
	/*--------置图标--------*/
	void WindowEx::Icon(BindataEx Icon)
	{
		SetAttribute_WindowEx(control, WindowEx_Attr_Icon, (AutoInt)(BinEx_P)Icon);
	}
	/*--------取标题--------*/
	StringEx WindowEx::Title()
	{
		return (StrEx_P)GetAttribute_WindowEx(control, WindowEx_Attr_Title);
	}
	/*--------置标题--------*/
	void WindowEx::Title(StringEx Title)
	{
		SetAttribute_WindowEx(control, WindowEx_Attr_Title, (AutoInt)(StrEx_P)Title);
	}
	/*--------取字体--------*/
	BindataEx WindowEx::Font()
	{
		return (BinEx_P)GetAttribute_WindowEx(control, WindowEx_Attr_Font);
	}
	/*--------置字体--------*/
	void WindowEx::Font(BindataEx Font)
	{
		SetAttribute_WindowEx(control, WindowEx_Attr_Font, (AutoInt)(BinEx_P)Font);
	}
	/*--------取字体色--------*/
	int WindowEx::FontClour()
	{
		return (int)GetAttribute_WindowEx(control, WindowEx_Attr_FontClour);
	}
	/*--------置字体色--------*/
	void WindowEx::FontClour(int FontClour)
	{
		SetAttribute_WindowEx(control, WindowEx_Attr_FontClour, (AutoInt)FontClour);
	}
	/*--------取分层透明--------*/
	int WindowEx::LayeredTransparency()
	{
		return (int)GetAttribute_WindowEx(control, WindowEx_Attr_LayeredTransparency);
	}
	/*--------置分层透明--------*/
	void WindowEx::LayeredTransparency(int LayeredTransparency)
	{
		SetAttribute_WindowEx(control, WindowEx_Attr_LayeredTransparency, (AutoInt)LayeredTransparency);
	}
	/*--------取拖动模式--------*/
	int WindowEx::DragPositionMode()
	{
		return (int)GetAttribute_WindowEx(control, WindowEx_Attr_DragPositionMode);
	}
	/*--------置拖动模式--------*/
	void WindowEx::DragPositionMode(int DragPositionMode)
	{
		SetAttribute_WindowEx(control, WindowEx_Attr_DragPositionMode, (AutoInt)DragPositionMode);
	}
	/*--------取尺寸调整模式--------*/
	int WindowEx::DragSizeMode()
	{
		return (int)GetAttribute_WindowEx(control, WindowEx_Attr_DragSizeMode);
	}
	/*--------置尺寸调整模式--------*/
	void WindowEx::DragSizeMode(int DragSizeMode)
	{
		SetAttribute_WindowEx(control, WindowEx_Attr_DragSizeMode, (AutoInt)DragSizeMode);
	}
	/*--------取最大化模式--------*/
	int WindowEx::MaxMode()
	{
		return (int)GetAttribute_WindowEx(control, WindowEx_Attr_MaxMode);
	}
	/*--------置最大化模式--------*/
	void WindowEx::MaxMode(int MaxMode)
	{
		SetAttribute_WindowEx(control, WindowEx_Attr_MaxMode, (AutoInt)MaxMode);
	}
	/*--------取控制钮--------*/
	BindataEx WindowEx::ButtonData()
	{
		return (BinEx_P)GetAttribute_WindowEx(control, WindowEx_Attr_ButtonData);
	}
	/*--------置控制钮--------*/
	void WindowEx::ButtonData(BindataEx ButtonData)
	{
		SetAttribute_WindowEx(control, WindowEx_Attr_ButtonData, (AutoInt)(BinEx_P)ButtonData);
	}
	/*--------取控制钮动画参数--------*/
	int WindowEx::AnimationParam()
	{
		return (int)GetAttribute_WindowEx(control, WindowEx_Attr_AnimationParam);
	}
	/*--------置控制钮动画参数--------*/
	void WindowEx::AnimationParam(int AnimationParam)
	{
		SetAttribute_WindowEx(control, WindowEx_Attr_AnimationParam, (AutoInt)AnimationParam);
	}
	/*--------取扩展元素--------*/
	BindataEx WindowEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_WindowEx(control, WindowEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void WindowEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_WindowEx(control, WindowEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx WindowEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_WindowEx(control, WindowEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void WindowEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_WindowEx(control, WindowEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件窗口EX属性读写函数结束--------*/


}
//LabelEx
namespace exui
{
	LabelEx::LabelEx()
	{

	}
	LabelEx::~LabelEx()
	{

	}
	/*--------创建组件_标签EX--------*/
	LabelEx_P LabelEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, StringEx Text, int Align, int BackColor, BindataEx Font, int FontClour, int Rotate, BindataEx ElemeData, BindataEx Layout)
	{

		InitCreateAttrStar(Exui_EventPro_LabelEx);
		CreateControl_LabelEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Text, Align, BackColor, Font, FontClour, Rotate, ElemeData, Layout);
		InitCreateAttrEnd(false);
		return control;

	}
	/*--------置属性_标签Ex--------*/
	void LabelEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_LabelEx(control, index, attribute);
	}
	/*--------取属性_标签Ex--------*/
	AutoInt LabelEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_LabelEx(control, index);
	}


	/*----------组件标签EX属性读写函数开始--------*/


/*--------取标题--------*/
	StringEx LabelEx::Title()
	{
		return (StrEx_P)GetAttribute_LabelEx(control, LabelEx_Attr_Title);
	}
	/*--------置标题--------*/
	void LabelEx::Title(StringEx Title)
	{
		SetAttribute_LabelEx(control, LabelEx_Attr_Title, (AutoInt)(StrEx_P)Title);
	}
	/*--------取对齐方式--------*/
	int LabelEx::Align()
	{
		return (int)GetAttribute_LabelEx(control, LabelEx_Attr_Align);
	}
	/*--------置对齐方式--------*/
	void LabelEx::Align(int Align)
	{
		SetAttribute_LabelEx(control, LabelEx_Attr_Align, (AutoInt)Align);
	}
	/*--------取背景色--------*/
	int LabelEx::BackColor()
	{
		return (int)GetAttribute_LabelEx(control, LabelEx_Attr_BackColor);
	}
	/*--------置背景色--------*/
	void LabelEx::BackColor(int BackColor)
	{
		SetAttribute_LabelEx(control, LabelEx_Attr_BackColor, (AutoInt)BackColor);
	}
	/*--------取字体--------*/
	BindataEx LabelEx::Font()
	{
		return (BinEx_P)GetAttribute_LabelEx(control, LabelEx_Attr_Font);
	}
	/*--------置字体--------*/
	void LabelEx::Font(BindataEx Font)
	{
		SetAttribute_LabelEx(control, LabelEx_Attr_Font, (AutoInt)(BinEx_P)Font);
	}
	/*--------取字体色--------*/
	int LabelEx::FontClour()
	{
		return (int)GetAttribute_LabelEx(control, LabelEx_Attr_FontClour);
	}
	/*--------置字体色--------*/
	void LabelEx::FontClour(int FontClour)
	{
		SetAttribute_LabelEx(control, LabelEx_Attr_FontClour, (AutoInt)FontClour);
	}
	/*--------取旋转角度--------*/
	int LabelEx::Rotate()
	{
		return (int)GetAttribute_LabelEx(control, LabelEx_Attr_Rotate);
	}
	/*--------置旋转角度--------*/
	void LabelEx::Rotate(int Rotate)
	{
		SetAttribute_LabelEx(control, LabelEx_Attr_Rotate, (AutoInt)Rotate);
	}
	/*--------取扩展元素--------*/
	BindataEx LabelEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_LabelEx(control, LabelEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void LabelEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_LabelEx(control, LabelEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx LabelEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_LabelEx(control, LabelEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void LabelEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_LabelEx(control, LabelEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件标签EX属性读写函数结束--------*/


}
//FilterEx
namespace exui
{
	FilterEx::FilterEx()
	{

	}
	FilterEx::~FilterEx()
	{

	}
	/*--------创建组件_滤镜EX--------*/
	FilterEx_P FilterEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, int FilterExMode, int Parameter1, int Parameter2, int Parameter3, int Parameter4, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_FilterEx);
		CreateControl_FilterEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, FilterExMode, Parameter1, Parameter2, Parameter3, Parameter4, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;

	}
	/*--------置属性_滤镜Ex--------*/
	void FilterEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_FilterEx(control, index, attribute);
	}
	/*--------取属性_滤镜Ex--------*/
	AutoInt FilterEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_FilterEx(control, index);
	}




	/*----------组件滤镜EX属性读写函数开始--------*/


/*--------取滤镜类型--------*/
	int FilterEx::FilterExMode()
	{
		return (int)GetAttribute_FilterEx(control, FilterEx_Attr_FilterExMode);
	}
	/*--------置滤镜类型--------*/
	void FilterEx::FilterExMode(int FilterExMode)
	{
		SetAttribute_FilterEx(control, FilterEx_Attr_FilterExMode, (AutoInt)FilterExMode);
	}
	/*--------取滤镜参数1--------*/
	int FilterEx::Parameter1()
	{
		return (int)GetAttribute_FilterEx(control, FilterEx_Attr_Parameter1);
	}
	/*--------置滤镜参数1--------*/
	void FilterEx::Parameter1(int Parameter1)
	{
		SetAttribute_FilterEx(control, FilterEx_Attr_Parameter1, (AutoInt)Parameter1);
	}
	/*--------取滤镜参数2--------*/
	int FilterEx::Parameter2()
	{
		return (int)GetAttribute_FilterEx(control, FilterEx_Attr_Parameter2);
	}
	/*--------置滤镜参数2--------*/
	void FilterEx::Parameter2(int Parameter2)
	{
		SetAttribute_FilterEx(control, FilterEx_Attr_Parameter2, (AutoInt)Parameter2);
	}
	/*--------取滤镜参数3--------*/
	int FilterEx::Parameter3()
	{
		return (int)GetAttribute_FilterEx(control, FilterEx_Attr_Parameter3);
	}
	/*--------置滤镜参数3--------*/
	void FilterEx::Parameter3(int Parameter3)
	{
		SetAttribute_FilterEx(control, FilterEx_Attr_Parameter3, (AutoInt)Parameter3);
	}
	/*--------取滤镜参数4--------*/
	int FilterEx::Parameter4()
	{
		return (int)GetAttribute_FilterEx(control, FilterEx_Attr_Parameter4);
	}
	/*--------置滤镜参数4--------*/
	void FilterEx::Parameter4(int Parameter4)
	{
		SetAttribute_FilterEx(control, FilterEx_Attr_Parameter4, (AutoInt)Parameter4);
	}
	/*--------取扩展元素--------*/
	BindataEx FilterEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_FilterEx(control, FilterEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void FilterEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_FilterEx(control, FilterEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx FilterEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_FilterEx(control, FilterEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void FilterEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_FilterEx(control, FilterEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件滤镜EX属性读写函数结束--------*/




}
//ButtonEx
namespace exui
{
	ButtonEx::ButtonEx()
	{

	}
	ButtonEx::~ButtonEx()
	{

	}
	/*--------创建组件_按钮EX--------*/
	ButtonEx_P ButtonEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, StringEx Text, BindataEx Font, int FontClour, int AnimationParam, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_ButtonEx);
		CreateControl_ButtonEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, Text, Font, FontClour, AnimationParam, ElemeData, Layout);
		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_按钮Ex--------*/
	void ButtonEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_ButtonEx(control, index, attribute);
	}
	/*--------取属性_按钮Ex--------*/
	AutoInt ButtonEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_ButtonEx(control, index);
	}


	/*----------组件按钮EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx ButtonEx::Skin()
	{
		return (BinEx_P)GetAttribute_ButtonEx(control, ButtonEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void ButtonEx::Skin(BindataEx Skin)
	{
		SetAttribute_ButtonEx(control, ButtonEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取标题--------*/
	StringEx ButtonEx::Title()
	{
		return (StrEx_P)GetAttribute_ButtonEx(control, ButtonEx_Attr_Title);
	}
	/*--------置标题--------*/
	void ButtonEx::Title(StringEx Title)
	{
		SetAttribute_ButtonEx(control, ButtonEx_Attr_Title, (AutoInt)(StrEx_P)Title);
	}
	/*--------取字体--------*/
	BindataEx ButtonEx::Font()
	{
		return (BinEx_P)GetAttribute_ButtonEx(control, ButtonEx_Attr_Font);
	}
	/*--------置字体--------*/
	void ButtonEx::Font(BindataEx Font)
	{
		SetAttribute_ButtonEx(control, ButtonEx_Attr_Font, (AutoInt)(BinEx_P)Font);
	}
	/*--------取字体色--------*/
	int ButtonEx::FontClour()
	{
		return (int)GetAttribute_ButtonEx(control, ButtonEx_Attr_FontClour);
	}
	/*--------置字体色--------*/
	void ButtonEx::FontClour(int FontClour)
	{
		SetAttribute_ButtonEx(control, ButtonEx_Attr_FontClour, (AutoInt)FontClour);
	}
	/*--------取动画参数--------*/
	int ButtonEx::AnimationParam()
	{
		return (int)GetAttribute_ButtonEx(control, ButtonEx_Attr_AnimationParam);
	}
	/*--------置动画参数--------*/
	void ButtonEx::AnimationParam(int AnimationParam)
	{
		SetAttribute_ButtonEx(control, ButtonEx_Attr_AnimationParam, (AutoInt)AnimationParam);
	}
	/*--------取扩展元素--------*/
	BindataEx ButtonEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_ButtonEx(control, ButtonEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void ButtonEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_ButtonEx(control, ButtonEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx ButtonEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_ButtonEx(control, ButtonEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void ButtonEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_ButtonEx(control, ButtonEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件按钮EX属性读写函数结束--------*/


}
//ImagebuttonEx
namespace exui
{
	ImagebuttonEx::ImagebuttonEx()
	{

	}
	ImagebuttonEx::~ImagebuttonEx()
	{

	}
	/*--------创建组件_图片按钮EX--------*/
	ImagebuttonEx_P ImagebuttonEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, BOOL Selected, int AnimationParam, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_ImagebuttonEx);
		CreateControl_ImagebuttonEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, Selected, AnimationParam, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_图片按钮Ex--------*/
	void ImagebuttonEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_ImagebuttonEx(control, index, attribute);
	}
	/*--------取属性_图片按钮Ex--------*/
	AutoInt ImagebuttonEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_ImagebuttonEx(control, index);
	}

	/*----------组件图片按钮EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx ImagebuttonEx::Skin()
	{
		return (BinEx_P)GetAttribute_ImagebuttonEx(control, ImagebuttonEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void ImagebuttonEx::Skin(BindataEx Skin)
	{
		SetAttribute_ImagebuttonEx(control, ImagebuttonEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取选中--------*/
	BOOL ImagebuttonEx::Selected()
	{
		return (BOOL)GetAttribute_ImagebuttonEx(control, ImagebuttonEx_Attr_Selected);
	}
	/*--------置选中--------*/
	void ImagebuttonEx::Selected(BOOL Selected)
	{
		SetAttribute_ImagebuttonEx(control, ImagebuttonEx_Attr_Selected, (AutoInt)Selected);
	}
	/*--------取动画参数--------*/
	int ImagebuttonEx::AnimationParam()
	{
		return (int)GetAttribute_ImagebuttonEx(control, ImagebuttonEx_Attr_AnimationParam);
	}
	/*--------置动画参数--------*/
	void ImagebuttonEx::AnimationParam(int AnimationParam)
	{
		SetAttribute_ImagebuttonEx(control, ImagebuttonEx_Attr_AnimationParam, (AutoInt)AnimationParam);
	}
	/*--------取扩展元素--------*/
	BindataEx ImagebuttonEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_ImagebuttonEx(control, ImagebuttonEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void ImagebuttonEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_ImagebuttonEx(control, ImagebuttonEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx ImagebuttonEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_ImagebuttonEx(control, ImagebuttonEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void ImagebuttonEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_ImagebuttonEx(control, ImagebuttonEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件图片按钮EX属性读写函数结束--------*/


}
//SuperbuttonEx
namespace exui
{
	SuperbuttonEx::SuperbuttonEx()
	{

	}
	SuperbuttonEx::~SuperbuttonEx()
	{

	}

	/*--------创建组件_超级按钮EX--------*/
	SuperbuttonEx_P SuperbuttonEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, BOOL Selected, BindataEx Icon, int IconWidth, int IconHeight, int  Align, StringEx Text, BindataEx Font, int FontClour, int AnimationParam, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_SuperbuttonEx);
		return	CreateControl_SuperbuttonEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, Selected, Icon, IconWidth, IconHeight, Align, Text, Font, FontClour, AnimationParam, ElemeData, Layout);

	}

	/*--------置属性_超级按钮Ex--------*/
	void SuperbuttonEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_SuperbuttonEx(control, index, attribute);
	}

	/*--------取属性_超级按钮Ex--------*/
	AutoInt SuperbuttonEx::GetAttribute(AutoInt index)
	{

		return GetAttribute_SuperbuttonEx(control, index);
	}


	/*----------组件超级按钮EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx SuperbuttonEx::Skin()
	{
		return (BinEx_P)GetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void SuperbuttonEx::Skin(BindataEx Skin)
	{
		SetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取选中--------*/
	BOOL SuperbuttonEx::Selected()
	{
		return (BOOL)GetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_Selected);
	}
	/*--------置选中--------*/
	void SuperbuttonEx::Selected(BOOL Selected)
	{
		SetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_Selected, (AutoInt)Selected);
	}
	/*--------取图标--------*/
	BindataEx SuperbuttonEx::Icon()
	{
		return (BinEx_P)GetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_Icon);
	}
	/*--------置图标--------*/
	void SuperbuttonEx::Icon(BindataEx Icon)
	{
		SetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_Icon, (AutoInt)(BinEx_P)Icon);
	}
	/*--------取图标宽--------*/
	int SuperbuttonEx::IconWidth()
	{
		return (int)GetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_IconWidth);
	}
	/*--------置图标宽--------*/
	void SuperbuttonEx::IconWidth(int IconWidth)
	{
		SetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_IconWidth, (AutoInt)IconWidth);
	}
	/*--------取图标高--------*/
	int SuperbuttonEx::IconHeight()
	{
		return (int)GetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_IconHeight);
	}
	/*--------置图标高--------*/
	void SuperbuttonEx::IconHeight(int IconHeight)
	{
		SetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_IconHeight, (AutoInt)IconHeight);
	}
	/*--------取对齐方式--------*/
	int SuperbuttonEx::Align()
	{
		return (int)GetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_Align);
	}
	/*--------置对齐方式--------*/
	void SuperbuttonEx::Align(int Align)
	{
		SetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_Align, (AutoInt)Align);
	}
	/*--------取标题--------*/
	StringEx SuperbuttonEx::Title()
	{
		return (StrEx_P)GetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_Title);
	}
	/*--------置标题--------*/
	void SuperbuttonEx::Title(StringEx Title)
	{
		SetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_Title, (AutoInt)(StrEx_P)Title);
	}
	/*--------取字体--------*/
	BindataEx SuperbuttonEx::Font()
	{
		return (BinEx_P)GetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_Font);
	}
	/*--------置字体--------*/
	void SuperbuttonEx::Font(BindataEx Font)
	{
		SetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_Font, (AutoInt)(BinEx_P)Font);
	}
	/*--------取字体色--------*/
	int SuperbuttonEx::FontClour()
	{
		return (int)GetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_FontClour);
	}
	/*--------置字体色--------*/
	void SuperbuttonEx::FontClour(int FontClour)
	{
		SetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_FontClour, (AutoInt)FontClour);
	}
	/*--------取动画参数--------*/
	int SuperbuttonEx::AnimationParam()
	{
		return (int)GetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_AnimationParam);
	}
	/*--------置动画参数--------*/
	void SuperbuttonEx::AnimationParam(int AnimationParam)
	{
		SetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_AnimationParam, (AutoInt)AnimationParam);
	}
	/*--------取扩展元素--------*/
	BindataEx SuperbuttonEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void SuperbuttonEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx SuperbuttonEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void SuperbuttonEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_SuperbuttonEx(control, SuperbuttonEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件超级按钮EX属性读写函数结束--------*/


}
//ChoiceboxEx
namespace exui
{
	ChoiceboxEx::ChoiceboxEx()
	{

	}
	ChoiceboxEx::~ChoiceboxEx()
	{

	}
	/*--------创建组件_选择框EX--------*/
	ChoiceboxEx_P ChoiceboxEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, BOOL Selected, StringEx Text, BindataEx Font, int FontClour, int AnimationParam, int SelectedMode, int SelectedState, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_ChoiceboxEx);
		CreateControl_ChoiceboxEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, Selected, Text, Font, FontClour, AnimationParam, SelectedMode, SelectedState, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_选择框Ex--------*/
	void ChoiceboxEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_ChoiceboxEx(control, index, attribute);
	}
	/*--------取属性_选择框Ex--------*/
	AutoInt ChoiceboxEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_ChoiceboxEx(control, index);
	}


	/*----------组件选择框EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx ChoiceboxEx::Skin()
	{
		return (BinEx_P)GetAttribute_ChoiceboxEx(control, ChoiceboxEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void ChoiceboxEx::Skin(BindataEx Skin)
	{
		SetAttribute_ChoiceboxEx(control, ChoiceboxEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取选中--------*/
	BOOL ChoiceboxEx::Selected()
	{
		return (BOOL)GetAttribute_ChoiceboxEx(control, ChoiceboxEx_Attr_Selected);
	}
	/*--------置选中--------*/
	void ChoiceboxEx::Selected(BOOL Selected)
	{
		SetAttribute_ChoiceboxEx(control, ChoiceboxEx_Attr_Selected, (AutoInt)Selected);
	}
	/*--------取标题--------*/
	StringEx ChoiceboxEx::Title()
	{
		return (StrEx_P)GetAttribute_ChoiceboxEx(control, ChoiceboxEx_Attr_Title);
	}
	/*--------置标题--------*/
	void ChoiceboxEx::Title(StringEx Title)
	{
		SetAttribute_ChoiceboxEx(control, ChoiceboxEx_Attr_Title, (AutoInt)(StrEx_P)Title);
	}
	/*--------取字体--------*/
	BindataEx ChoiceboxEx::Font()
	{
		return (BinEx_P)GetAttribute_ChoiceboxEx(control, ChoiceboxEx_Attr_Font);
	}
	/*--------置字体--------*/
	void ChoiceboxEx::Font(BindataEx Font)
	{
		SetAttribute_ChoiceboxEx(control, ChoiceboxEx_Attr_Font, (AutoInt)(BinEx_P)Font);
	}
	/*--------取字体色--------*/
	int ChoiceboxEx::FontClour()
	{
		return (int)GetAttribute_ChoiceboxEx(control, ChoiceboxEx_Attr_FontClour);
	}
	/*--------置字体色--------*/
	void ChoiceboxEx::FontClour(int FontClour)
	{
		SetAttribute_ChoiceboxEx(control, ChoiceboxEx_Attr_FontClour, (AutoInt)FontClour);
	}
	/*--------取动画参数--------*/
	int ChoiceboxEx::AnimationParam()
	{
		return (int)GetAttribute_ChoiceboxEx(control, ChoiceboxEx_Attr_AnimationParam);
	}
	/*--------置动画参数--------*/
	void ChoiceboxEx::AnimationParam(int AnimationParam)
	{
		SetAttribute_ChoiceboxEx(control, ChoiceboxEx_Attr_AnimationParam, (AutoInt)AnimationParam);
	}
	/*--------取选中模式--------*/
	int ChoiceboxEx::SelectMode()
	{
		return (int)GetAttribute_ChoiceboxEx(control, ChoiceboxEx_Attr_SelectMode);
	}
	/*--------置选中模式--------*/
	void ChoiceboxEx::SelectMode(int SelectMode)
	{
		SetAttribute_ChoiceboxEx(control, ChoiceboxEx_Attr_SelectMode, (AutoInt)SelectMode);
	}
	/*--------取选中状态--------*/
	int ChoiceboxEx::SelectState()
	{
		return (int)GetAttribute_ChoiceboxEx(control, ChoiceboxEx_Attr_SelectState);
	}
	/*--------置选中状态--------*/
	void ChoiceboxEx::SelectState(int SelectState)
	{
		SetAttribute_ChoiceboxEx(control, ChoiceboxEx_Attr_SelectState, (AutoInt)SelectState);
	}
	/*--------取扩展元素--------*/
	BindataEx ChoiceboxEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_ChoiceboxEx(control, ChoiceboxEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void ChoiceboxEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_ChoiceboxEx(control, ChoiceboxEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx ChoiceboxEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_ChoiceboxEx(control, ChoiceboxEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void ChoiceboxEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_ChoiceboxEx(control, ChoiceboxEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件选择框EX属性读写函数结束--------*/


}
//RadiobuttonEx
namespace exui
{
	RadiobuttonEx::RadiobuttonEx()
	{

	}
	RadiobuttonEx::~RadiobuttonEx()
	{

	}
	/*--------创建组件_单选框EX--------*/
	RadiobuttonEx_P RadiobuttonEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, BOOL Selected, StringEx Text, BindataEx Font, int FontClour, int AnimationParam, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_RadiobuttonEx);
		CreateControl_RadiobuttonEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, Selected, Text, Font, FontClour, AnimationParam, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_单选框Ex--------*/
	void RadiobuttonEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_RadiobuttonEx(control, index, attribute);
	}
	/*--------取属性_单选框Ex--------*/
	AutoInt RadiobuttonEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_RadiobuttonEx(control, index);
	}

	/*----------组件单选框EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx RadiobuttonEx::Skin()
	{
		return (BinEx_P)GetAttribute_RadiobuttonEx(control, RadiobuttonEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void RadiobuttonEx::Skin(BindataEx Skin)
	{
		SetAttribute_RadiobuttonEx(control, RadiobuttonEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取选中--------*/
	BOOL RadiobuttonEx::Selected()
	{
		return (BOOL)GetAttribute_RadiobuttonEx(control, RadiobuttonEx_Attr_Selected);
	}
	/*--------置选中--------*/
	void RadiobuttonEx::Selected(BOOL Selected)
	{
		SetAttribute_RadiobuttonEx(control, RadiobuttonEx_Attr_Selected, (AutoInt)Selected);
	}
	/*--------取标题--------*/
	StringEx RadiobuttonEx::Title()
	{
		return (StrEx_P)GetAttribute_RadiobuttonEx(control, RadiobuttonEx_Attr_Title);
	}
	/*--------置标题--------*/
	void RadiobuttonEx::Title(StringEx Title)
	{
		SetAttribute_RadiobuttonEx(control, RadiobuttonEx_Attr_Title, (AutoInt)(StrEx_P)Title);
	}
	/*--------取字体--------*/
	BindataEx RadiobuttonEx::Font()
	{
		return (BinEx_P)GetAttribute_RadiobuttonEx(control, RadiobuttonEx_Attr_Font);
	}
	/*--------置字体--------*/
	void RadiobuttonEx::Font(BindataEx Font)
	{
		SetAttribute_RadiobuttonEx(control, RadiobuttonEx_Attr_Font, (AutoInt)(BinEx_P)Font);
	}
	/*--------取字体色--------*/
	int RadiobuttonEx::FontClour()
	{
		return (int)GetAttribute_RadiobuttonEx(control, RadiobuttonEx_Attr_FontClour);
	}
	/*--------置字体色--------*/
	void RadiobuttonEx::FontClour(int FontClour)
	{
		SetAttribute_RadiobuttonEx(control, RadiobuttonEx_Attr_FontClour, (AutoInt)FontClour);
	}
	/*--------取动画参数--------*/
	int RadiobuttonEx::AnimationParam()
	{
		return (int)GetAttribute_RadiobuttonEx(control, RadiobuttonEx_Attr_AnimationParam);
	}
	/*--------置动画参数--------*/
	void RadiobuttonEx::AnimationParam(int AnimationParam)
	{
		SetAttribute_RadiobuttonEx(control, RadiobuttonEx_Attr_AnimationParam, (AutoInt)AnimationParam);
	}
	/*--------取扩展元素--------*/
	BindataEx RadiobuttonEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_RadiobuttonEx(control, RadiobuttonEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void RadiobuttonEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_RadiobuttonEx(control, RadiobuttonEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx RadiobuttonEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_RadiobuttonEx(control, RadiobuttonEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void RadiobuttonEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_RadiobuttonEx(control, RadiobuttonEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件单选框EX属性读写函数结束--------*/


}
//EditboxEx
namespace exui
{
	EditboxEx::EditboxEx()
	{

	}
	EditboxEx::~EditboxEx()
	{

	}
	/*--------创建组件_编辑框EX--------*/
	EditboxEx_P EditboxEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, StringEx Content, int Align, int InputMode, int MaxInput, StringEx PasswordSubstitution, int CursorColor, BindataEx Font, int FontClour, int SelectedFontColor, int SelectedColor, int LeftReservation, int RightReservation, BOOL Multiline, BOOL Wrapped, int ScrollBarMode, int MenuTableWidth, int MenuTableHeight, BindataEx MenuFont, int MenuFontClour, int MenuDisabledFontClour, int MenuTransparency, int MenuLanguage, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_EditboxEx);
		CreateControl_EditboxEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, Content, Align, InputMode, MaxInput, PasswordSubstitution, CursorColor, Font, FontClour, SelectedFontColor, SelectedColor, LeftReservation, RightReservation, Multiline, Wrapped, ScrollBarMode, MenuTableWidth, MenuTableHeight, MenuFont, MenuFontClour, MenuDisabledFontClour, MenuTransparency, MenuLanguage, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_编辑框Ex--------*/
	void EditboxEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_EditboxEx(control, index, attribute);
	}
	/*--------取属性_编辑框Ex--------*/
	AutoInt EditboxEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_EditboxEx(control, index);
	}
	/*--------插入文本_编辑框Ex--------*/
	void EditboxEx::InsertText(AutoInt weizhi, StringEx text)
	{
		InsertText_EditboxEx(control, weizhi, text);
	}
	/*--------删除文本_编辑框Ex--------*/
	void EditboxEx::DeleteText(AutoInt weizhi, AutoInt dellen)
	{
		DeleteText_EditboxEx(control, weizhi, dellen);
	}
	/*--------置光标位置_编辑框Ex--------*/
	void EditboxEx::SetInsertCursor(AutoInt index)
	{
		SetInsertCursor_EditboxEx(control, index);
	}
	/*--------取光标位置_编辑框Ex--------*/
	AutoInt EditboxEx::GetInsertCursor()
	{
		return GetInsertCursor_EditboxEx(control);
	}
	/*--------置选择文本长度_编辑框Ex--------*/
	void EditboxEx::SetSelectLeng(AutoInt SelectLeng)
	{
		SetSelectLeng_EditboxEx(control, SelectLeng);
	}
	/*--------取选择文本长度_编辑框Ex--------*/
	AutoInt EditboxEx::GetSelectLeng()
	{
		return GetSelectLeng_EditboxEx(control);
	}
	/*--------保证显示字符_编辑框Ex--------*/
	void EditboxEx::GuaranteeVisibleText(AutoInt index, int mode)
	{
		GuaranteeVisibleText_EditboxEx(control, index, mode);
	}


	/*----------组件编辑框EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx EditboxEx::Skin()
	{
		return (BinEx_P)GetAttribute_EditboxEx(control, EditboxEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void EditboxEx::Skin(BindataEx Skin)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取内容--------*/
	StringEx EditboxEx::Content()
	{
		return (StrEx_P)GetAttribute_EditboxEx(control, EditboxEx_Attr_Content);
	}
	/*--------置内容--------*/
	void EditboxEx::Content(StringEx Content)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_Content, (AutoInt)(StrEx_P)Content);
	}
	/*--------取对齐方式--------*/
	int EditboxEx::Align()
	{
		return (int)GetAttribute_EditboxEx(control, EditboxEx_Attr_Align);
	}
	/*--------置对齐方式--------*/
	void EditboxEx::Align(int Align)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_Align, (AutoInt)Align);
	}
	/*--------取输入方式--------*/
	int EditboxEx::InputMode()
	{
		return (int)GetAttribute_EditboxEx(control, EditboxEx_Attr_InputMode);
	}
	/*--------置输入方式--------*/
	void EditboxEx::InputMode(int InputMode)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_InputMode, (AutoInt)InputMode);
	}
	/*--------取最大容许长度--------*/
	int EditboxEx::MaxInput()
	{
		return (int)GetAttribute_EditboxEx(control, EditboxEx_Attr_MaxInput);
	}
	/*--------置最大容许长度--------*/
	void EditboxEx::MaxInput(int MaxInput)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_MaxInput, (AutoInt)MaxInput);
	}
	/*--------取密码替换符--------*/
	StringEx EditboxEx::PasswordSubstitution()
	{
		return (StrEx_P)GetAttribute_EditboxEx(control, EditboxEx_Attr_PasswordSubstitution);
	}
	/*--------置密码替换符--------*/
	void EditboxEx::PasswordSubstitution(StringEx PasswordSubstitution)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_PasswordSubstitution, (AutoInt)(StrEx_P)PasswordSubstitution);
	}
	/*--------取光标色--------*/
	int EditboxEx::CursorColor()
	{
		return (int)GetAttribute_EditboxEx(control, EditboxEx_Attr_CursorColor);
	}
	/*--------置光标色--------*/
	void EditboxEx::CursorColor(int CursorColor)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_CursorColor, (AutoInt)CursorColor);
	}
	/*--------取字体--------*/
	BindataEx EditboxEx::Font()
	{
		return (BinEx_P)GetAttribute_EditboxEx(control, EditboxEx_Attr_Font);
	}
	/*--------置字体--------*/
	void EditboxEx::Font(BindataEx Font)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_Font, (AutoInt)(BinEx_P)Font);
	}
	/*--------取字体色--------*/
	int EditboxEx::FontClour()
	{
		return (int)GetAttribute_EditboxEx(control, EditboxEx_Attr_FontClour);
	}
	/*--------置字体色--------*/
	void EditboxEx::FontClour(int FontClour)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_FontClour, (AutoInt)FontClour);
	}
	/*--------取选中字体色--------*/
	int EditboxEx::SelectedFontColor()
	{
		return (int)GetAttribute_EditboxEx(control, EditboxEx_Attr_SelectedFontColor);
	}
	/*--------置选中字体色--------*/
	void EditboxEx::SelectedFontColor(int SelectedFontColor)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_SelectedFontColor, (AutoInt)SelectedFontColor);
	}
	/*--------取选中背景色--------*/
	int EditboxEx::SelectedColor()
	{
		return (int)GetAttribute_EditboxEx(control, EditboxEx_Attr_SelectedColor);
	}
	/*--------置选中背景色--------*/
	void EditboxEx::SelectedColor(int SelectedColor)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_SelectedColor, (AutoInt)SelectedColor);
	}
	/*--------取左预留--------*/
	int EditboxEx::LeftReservation()
	{
		return (int)GetAttribute_EditboxEx(control, EditboxEx_Attr_LeftReservation);
	}
	/*--------置左预留--------*/
	void EditboxEx::LeftReservation(int LeftReservation)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_LeftReservation, (AutoInt)LeftReservation);
	}
	/*--------取右预留--------*/
	int EditboxEx::RightReservation()
	{
		return (int)GetAttribute_EditboxEx(control, EditboxEx_Attr_RightReservation);
	}
	/*--------置右预留--------*/
	void EditboxEx::RightReservation(int RightReservation)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_RightReservation, (AutoInt)RightReservation);
	}
	/*--------取是否容许多行--------*/
	BOOL EditboxEx::Multiline()
	{
		return (BOOL)GetAttribute_EditboxEx(control, EditboxEx_Attr_Multiline);
	}
	/*--------置是否容许多行--------*/
	void EditboxEx::Multiline(BOOL Multiline)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_Multiline, (AutoInt)Multiline);
	}
	/*--------取自动换行--------*/
	BOOL EditboxEx::Wrapped()
	{
		return (BOOL)GetAttribute_EditboxEx(control, EditboxEx_Attr_Wrapped);
	}
	/*--------置自动换行--------*/
	void EditboxEx::Wrapped(BOOL Wrapped)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_Wrapped, (AutoInt)Wrapped);
	}
	/*--------取滚动条方式--------*/
	int EditboxEx::ScrollBarMode()
	{
		return (int)GetAttribute_EditboxEx(control, EditboxEx_Attr_ScrollBarMode);
	}
	/*--------置滚动条方式--------*/
	void EditboxEx::ScrollBarMode(int ScrollBarMode)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_ScrollBarMode, (AutoInt)ScrollBarMode);
	}
	/*--------取菜单项目宽--------*/
	int EditboxEx::MenuTableWidth()
	{
		return (int)GetAttribute_EditboxEx(control, EditboxEx_Attr_MenuTableWidth);
	}
	/*--------置菜单项目宽--------*/
	void EditboxEx::MenuTableWidth(int MenuTableWidth)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_MenuTableWidth, (AutoInt)MenuTableWidth);
	}
	/*--------取菜单项目高--------*/
	int EditboxEx::MenuTableHeight()
	{
		return (int)GetAttribute_EditboxEx(control, EditboxEx_Attr_MenuTableHeight);
	}
	/*--------置菜单项目高--------*/
	void EditboxEx::MenuTableHeight(int MenuTableHeight)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_MenuTableHeight, (AutoInt)MenuTableHeight);
	}
	/*--------取菜单字体--------*/
	BindataEx EditboxEx::MenuFont()
	{
		return (BinEx_P)GetAttribute_EditboxEx(control, EditboxEx_Attr_MenuFont);
	}
	/*--------置菜单字体--------*/
	void EditboxEx::MenuFont(BindataEx MenuFont)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_MenuFont, (AutoInt)(BinEx_P)(BinEx_P)MenuFont);
	}
	/*--------取菜单字体色--------*/
	int EditboxEx::MenuFontClour()
	{
		return (int)GetAttribute_EditboxEx(control, EditboxEx_Attr_MenuFontClour);
	}
	/*--------置菜单字体色--------*/
	void EditboxEx::MenuFontClour(int MenuFontClour)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_MenuFontClour, (AutoInt)MenuFontClour);
	}
	/*--------取菜单禁止字体色--------*/
	int EditboxEx::MenuDisabledFontClour()
	{
		return (int)GetAttribute_EditboxEx(control, EditboxEx_Attr_MenuDisabledFontClour);
	}
	/*--------置菜单禁止字体色--------*/
	void EditboxEx::MenuDisabledFontClour(int MenuDisabledFontClour)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_MenuDisabledFontClour, (AutoInt)MenuDisabledFontClour);
	}
	/*--------取菜单透明度--------*/
	int EditboxEx::MenuTransparency()
	{
		return (int)GetAttribute_EditboxEx(control, EditboxEx_Attr_MenuTransparency);
	}
	/*--------置菜单透明度--------*/
	void EditboxEx::MenuTransparency(int MenuTransparency)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_MenuTransparency, (AutoInt)MenuTransparency);
	}
	/*--------取菜单语言--------*/
	int EditboxEx::MenuLanguage()
	{
		return (int)GetAttribute_EditboxEx(control, EditboxEx_Attr_MenuLanguage);
	}
	/*--------置菜单语言--------*/
	void EditboxEx::MenuLanguage(int MenuLanguage)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_MenuLanguage, (AutoInt)MenuLanguage);
	}
	/*--------取扩展元素--------*/
	BindataEx EditboxEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_EditboxEx(control, EditboxEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void EditboxEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx EditboxEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_EditboxEx(control, EditboxEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void EditboxEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_EditboxEx(control, EditboxEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件编辑框EX属性读写函数结束--------*/


}
//ComboboxEx
namespace exui
{
	ComboboxEx::ComboboxEx()
	{

	}
	ComboboxEx::~ComboboxEx()
	{

	}
	/*--------创建组件_组合框EX--------*/
	ComboboxEx_P ComboboxEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, StringEx Content, int Align, int InputMode, int MaxInput, StringEx PasswordSubstitution, int CursorColor, BindataEx Font, int FontClour, int SelectedFontColor, int SelectedColor, int LeftReservation, int RightReservation, int MenuTableWidth, int MenuTableHeight, BindataEx MenuFont, int MenuFontClour, int MenuDisabledFontClour, int MenuTransparency, int MenuLanguage, int DownListWidth, int DownListMaxHeight, AutoInt DownListCurrentSelection, int DownListIconWidth, int DownListIconHeight, BindataEx DownListFont, BOOL DownListAlternate, int DownListScrollBarMode, int DownListAttributeTransparency, BindataEx DownListItemData, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_ComboboxEx);
		CreateControl_ComboboxEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, Content, Align, InputMode, MaxInput, PasswordSubstitution, CursorColor, Font, FontClour, SelectedFontColor, SelectedColor, LeftReservation, RightReservation, MenuTableWidth, MenuTableHeight, MenuFont, MenuFontClour, MenuDisabledFontClour, MenuTransparency, MenuLanguage, DownListWidth, DownListMaxHeight, DownListCurrentSelection, DownListIconWidth, DownListIconHeight, DownListFont, DownListAlternate, DownListScrollBarMode, DownListAttributeTransparency, DownListItemData, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_组合框Ex--------*/
	void ComboboxEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_ComboboxEx(control, index, attribute);
	}
	/*--------取属性_组合框Ex--------*/
	AutoInt ComboboxEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_ComboboxEx(control, index);
	}
	/*--------插入文本_组合框Ex--------*/
	void ComboboxEx::InsertText(AutoInt weizhi, StringEx text)
	{
		InsertText_ComboboxEx(control, weizhi, text);
	}
	/*--------删除文本_组合框Ex--------*/
	void ComboboxEx::DeleteText(AutoInt weizhi, AutoInt dellen)
	{
		DeleteText_ComboboxEx(control, weizhi, dellen);
	}
	/*--------置光标位置_组合框Ex--------*/
	void ComboboxEx::SetInsertCursor(AutoInt index)
	{
		SetInsertCursor_ComboboxEx(control, index);
	}
	/*--------取光标位置_组合框Ex--------*/
	int ComboboxEx::GetInsertCursor()
	{
		return GetInsertCursor_ComboboxEx(control);
	}
	/*--------置选择文本长度_组合框Ex--------*/
	void ComboboxEx::SetSelectLeng(AutoInt SelectLeng)
	{
		SetSelectLeng_ComboboxEx(control, SelectLeng);
	}
	/*--------取选择文本长度_组合框Ex--------*/
	int ComboboxEx::GetSelectLeng()
	{
		return GetSelectLeng_ComboboxEx(control);
	}
	/*--------保证显示字符_组合框Ex--------*/
	void ComboboxEx::GuaranteeVisibleText(AutoInt index, int mode)
	{
		GuaranteeVisibleText_ComboboxEx(control, index, mode);
	}
	/*--------弹出_组合框Ex--------*/
	void ComboboxEx::PopUp_DownlistEx()
	{
		PopUp_DownlistEx_ComboboxEx(control);
	}
	/*--------关闭_组合框Ex--------*/
	void ComboboxEx::Close_DownlistEx()
	{
		Close_DownlistEx_ComboboxEx(control);
	}
	/*--------是否已弹出_组合框Ex--------*/
	BOOL ComboboxEx::IsPopUp_DownlistEx()
	{
		return IsPopUp_DownlistEx_ComboboxEx(control);
	}
	/*--------插入_组合框Ex--------*/
	AutoInt ComboboxEx::InsertItem_DownlistEx(AutoInt index, AutoInt addnum, AutoInt Data, BitmapEx Ico, StringEx Title, int FontColor, int Size)
	{
		return InsertItem_DownlistEx_ComboboxEx(control, index, addnum, Data, Ico, Title, FontColor, Size);
	}
	/*--------删除_组合框Ex--------*/
	void ComboboxEx::DeleteItem_DownlistEx(AutoInt index, int deletenum)
	{
		DeleteItem_DownlistEx_ComboboxEx(control, index, deletenum);
	}
	/*--------取数量_组合框Ex--------*/
	AutoInt ComboboxEx::GetItemCount_DownlistEx()
	{
		return GetItemCount_DownlistEx_ComboboxEx(control);
	}
	/*--------置附加值_组合框Ex--------*/
	void ComboboxEx::SetItemData_DownlistEx(AutoInt index, AutoInt Data)
	{
		SetItemData_DownlistEx_ComboboxEx(control, index, Data);
	}
	/*--------取附加值_组合框Ex--------*/
	AutoInt ComboboxEx::GetItemData_DownlistEx(AutoInt index)
	{
		return GetItemData_DownlistEx_ComboboxEx(control, index);
	}
	/*--------置图标_组合框Ex--------*/
	void ComboboxEx::SetItemIco_DownlistEx(AutoInt index, BitmapEx Ico)
	{
		SetItemIco_DownlistEx_ComboboxEx(control, index, Ico);
	}
	/*--------取图标_组合框Ex--------*/
	BitmapEx ComboboxEx::GetItemIco_DownlistEx(AutoInt index)
	{
		return GetItemIco_DownlistEx_ComboboxEx(control, index);
	}
	/*--------置标题_组合框Ex--------*/
	void ComboboxEx::SetItemTitle_DownlistEx(AutoInt index, StringEx Title)
	{
		SetItemTitle_DownlistEx_ComboboxEx(control, index, Title);
	}
	/*--------取标题_组合框Ex--------*/
	StringEx ComboboxEx::GetItemTitle_DownlistEx(AutoInt index)
	{
		return GetItemTitle_DownlistEx_ComboboxEx(control, index);
	}
	/*--------置字体色_组合框Ex--------*/
	void ComboboxEx::SetItemFontColor_DownlistEx(AutoInt index, int FontColor)
	{
		SetItemFontColor_DownlistEx_ComboboxEx(control, index, FontColor);
	}
	/*--------取字体色_组合框Ex--------*/
	int ComboboxEx::GetItemFontColor_DownlistEx(AutoInt index)
	{
		return GetItemFontColor_DownlistEx_ComboboxEx(control, index);
	}
	/*--------置项目高度_组合框Ex--------*/
	void ComboboxEx::SetItemSize_DownlistEx(AutoInt index, int Size)
	{
		SetItemSize_DownlistEx_ComboboxEx(control, index, Size);
	}
	/*--------取项目高度_组合框Ex--------*/
	int ComboboxEx::GetItemSize_DownlistEx(AutoInt index)
	{
		return GetItemSize_DownlistEx_ComboboxEx(control, index);
	}
	/*--------保证显示_组合框Ex--------*/
	void ComboboxEx::GuaranteeVisible_DownlistEx(AutoInt index, int mode)
	{
		GuaranteeVisible_DownlistEx_ComboboxEx(control, index, mode);
	}

	/*----------组件组合框EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx ComboboxEx::Skin()
	{
		return (BinEx_P)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void ComboboxEx::Skin(BindataEx Skin)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_Skin, (AutoInt)(BinEx_P)(BinEx_P)Skin);
	}
	/*--------取内容--------*/
	StringEx ComboboxEx::Content()
	{
		return (StrEx_P)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_Content);
	}
	/*--------置内容--------*/
	void ComboboxEx::Content(StringEx Content)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_Content, (AutoInt)(StrEx_P)Content);
	}
	/*--------取对齐方式--------*/
	int ComboboxEx::Align()
	{
		return (int)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_Align);
	}
	/*--------置对齐方式--------*/
	void ComboboxEx::Align(int Align)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_Align, (AutoInt)Align);
	}
	/*--------取输入方式--------*/
	int ComboboxEx::InputMode()
	{
		return (int)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_InputMode);
	}
	/*--------置输入方式--------*/
	void ComboboxEx::InputMode(int InputMode)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_InputMode, (AutoInt)InputMode);
	}
	/*--------取最大容许长度--------*/
	int ComboboxEx::MaxInput()
	{
		return (int)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_MaxInput);
	}
	/*--------置最大容许长度--------*/
	void ComboboxEx::MaxInput(int MaxInput)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_MaxInput, (AutoInt)MaxInput);
	}
	/*--------取密码替换符--------*/
	StringEx ComboboxEx::PasswordSubstitution()
	{
		return (StrEx_P)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_PasswordSubstitution);
	}
	/*--------置密码替换符--------*/
	void ComboboxEx::PasswordSubstitution(StringEx PasswordSubstitution)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_PasswordSubstitution, (AutoInt)(StrEx_P)PasswordSubstitution);
	}
	/*--------取光标色--------*/
	int ComboboxEx::CursorColor()
	{
		return (int)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_CursorColor);
	}
	/*--------置光标色--------*/
	void ComboboxEx::CursorColor(int CursorColor)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_CursorColor, (AutoInt)CursorColor);
	}
	/*--------取字体--------*/
	BindataEx ComboboxEx::Font()
	{
		return (BinEx_P)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_Font);
	}
	/*--------置字体--------*/
	void ComboboxEx::Font(BindataEx Font)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_Font, (AutoInt)(BinEx_P)(BinEx_P)Font);
	}
	/*--------取字体色--------*/
	int ComboboxEx::FontClour()
	{
		return (int)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_FontClour);
	}
	/*--------置字体色--------*/
	void ComboboxEx::FontClour(int FontClour)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_FontClour, (AutoInt)(BinEx_P)FontClour);
	}
	/*--------取选中字体色--------*/
	int ComboboxEx::SelectedFontColor()
	{
		return (int)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_SelectedFontColor);
	}
	/*--------置选中字体色--------*/
	void ComboboxEx::SelectedFontColor(int SelectedFontColor)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_SelectedFontColor, (AutoInt)SelectedFontColor);
	}
	/*--------取选中背景色--------*/
	int ComboboxEx::SelectedColor()
	{
		return (int)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_SelectedColor);
	}
	/*--------置选中背景色--------*/
	void ComboboxEx::SelectedColor(int SelectedColor)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_SelectedColor, (AutoInt)SelectedColor);
	}
	/*--------取左预留--------*/
	int ComboboxEx::LeftReservation()
	{
		return (int)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_LeftReservation);
	}
	/*--------置左预留--------*/
	void ComboboxEx::LeftReservation(int LeftReservation)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_LeftReservation, (AutoInt)LeftReservation);
	}
	/*--------取右预留--------*/
	int ComboboxEx::RightReservation()
	{
		return (int)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_RightReservation);
	}
	/*--------置右预留--------*/
	void ComboboxEx::RightReservation(int RightReservation)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_RightReservation, (AutoInt)RightReservation);
	}
	/*--------取菜单项目宽--------*/
	int ComboboxEx::MenuTableWidth()
	{
		return (int)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_MenuTableWidth);
	}
	/*--------置菜单项目宽--------*/
	void ComboboxEx::MenuTableWidth(int MenuTableWidth)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_MenuTableWidth, (AutoInt)MenuTableWidth);
	}
	/*--------取菜单项目高--------*/
	int ComboboxEx::MenuTableHeight()
	{
		return (int)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_MenuTableHeight);
	}
	/*--------置菜单项目高--------*/
	void ComboboxEx::MenuTableHeight(int MenuTableHeight)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_MenuTableHeight, (AutoInt)MenuTableHeight);
	}
	/*--------取菜单字体--------*/
	BindataEx ComboboxEx::MenuFont()
	{
		return (BinEx_P)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_MenuFont);
	}
	/*--------置菜单字体--------*/
	void ComboboxEx::MenuFont(BindataEx MenuFont)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_MenuFont, (AutoInt)(BinEx_P)MenuFont);
	}
	/*--------取菜单字体色--------*/
	int ComboboxEx::MenuFontClour()
	{
		return (int)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_MenuFontClour);
	}
	/*--------置菜单字体色--------*/
	void ComboboxEx::MenuFontClour(int MenuFontClour)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_MenuFontClour, (AutoInt)MenuFontClour);
	}
	/*--------取菜单禁止字体色--------*/
	int ComboboxEx::MenuDisabledFontClour()
	{
		return (int)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_MenuDisabledFontClour);
	}
	/*--------置菜单禁止字体色--------*/
	void ComboboxEx::MenuDisabledFontClour(int MenuDisabledFontClour)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_MenuDisabledFontClour, (AutoInt)MenuDisabledFontClour);
	}
	/*--------取菜单透明度--------*/
	int ComboboxEx::MenuTransparency()
	{
		return (int)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_MenuTransparency);
	}
	/*--------置菜单透明度--------*/
	void ComboboxEx::MenuTransparency(int MenuTransparency)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_MenuTransparency, (AutoInt)MenuTransparency);
	}
	/*--------取菜单语言--------*/
	int ComboboxEx::MenuLanguage()
	{
		return (int)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_MenuLanguage);
	}
	/*--------置菜单语言--------*/
	void ComboboxEx::MenuLanguage(int MenuLanguage)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_MenuLanguage, (AutoInt)MenuLanguage);
	}
	/*--------取列表宽度--------*/
	int ComboboxEx::DownListWidth()
	{
		return (int)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_DownListWidth);
	}
	/*--------置列表宽度--------*/
	void ComboboxEx::DownListWidth(int DownListWidth)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_DownListWidth, (AutoInt)DownListWidth);
	}
	/*--------取列表最大高--------*/
	int ComboboxEx::DownListMaxHeight()
	{
		return (int)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_DownListMaxHeight);
	}
	/*--------置列表最大高--------*/
	void ComboboxEx::DownListMaxHeight(int DownListMaxHeight)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_DownListMaxHeight, (AutoInt)DownListMaxHeight);
	}
	/*--------取列表现行选中项--------*/
	int ComboboxEx::DownListCurrentItem()
	{
		return (int)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_DownListCurrentItem);
	}
	/*--------置列表现行选中项--------*/
	void ComboboxEx::DownListCurrentItem(int DownListCurrentItem)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_DownListCurrentItem, (AutoInt)DownListCurrentItem);
	}
	/*--------取列表图标宽--------*/
	int ComboboxEx::DownListIconWidth()
	{
		return (int)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_DownListIconWidth);
	}
	/*--------置列表图标宽--------*/
	void ComboboxEx::DownListIconWidth(int DownListIconWidth)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_DownListIconWidth, (AutoInt)DownListIconWidth);
	}
	/*--------取列表图标高--------*/
	int ComboboxEx::DownListIconHeight()
	{
		return (int)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_DownListIconHeight);
	}
	/*--------置列表图标高--------*/
	void ComboboxEx::DownListIconHeight(int DownListIconHeight)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_DownListIconHeight, (AutoInt)DownListIconHeight);
	}
	/*--------取列表字体--------*/
	BindataEx ComboboxEx::DownListFont()
	{
		return (BinEx_P)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_DownListFont);
	}
	/*--------置列表字体--------*/
	void ComboboxEx::DownListFont(BindataEx DownListFont)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_DownListFont, (AutoInt)(BinEx_P)DownListFont);
	}
	/*--------取列表隔行换色--------*/
	BOOL ComboboxEx::DownListAlternate()
	{
		return (BOOL)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_DownListAlternate);
	}
	/*--------置列表隔行换色--------*/
	void ComboboxEx::DownListAlternate(BOOL DownListAlternate)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_DownListAlternate, (AutoInt)DownListAlternate);
	}
	/*--------取列表滚动条方式--------*/
	int ComboboxEx::DownListScrollBarMode()
	{
		return (int)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_DownListScrollBarMode);
	}
	/*--------置列表滚动条方式--------*/
	void ComboboxEx::DownListScrollBarMode(int DownListScrollBarMode)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_DownListScrollBarMode, (AutoInt)DownListScrollBarMode);
	}
	/*--------取列表透明度--------*/
	int ComboboxEx::DownListTransparency()
	{
		return (int)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_DownListTransparency);
	}
	/*--------置列表透明度--------*/
	void ComboboxEx::DownListTransparency(int DownListTransparency)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_DownListTransparency, (AutoInt)DownListTransparency);
	}
	/*--------取列表项目数据--------*/
	BindataEx ComboboxEx::DownListItemData()
	{
		return (BinEx_P)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_DownListItemData);
	}
	/*--------置列表项目数据--------*/
	void ComboboxEx::DownListItemData(BindataEx DownListItemData)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_DownListItemData, (AutoInt)(BinEx_P)DownListItemData);
	}
	/*--------取扩展元素--------*/
	BindataEx ComboboxEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void ComboboxEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_ElemeData, (AutoInt)(BinEx_P)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx ComboboxEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_ComboboxEx(control, ComboboxEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void ComboboxEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_ComboboxEx(control, ComboboxEx_Attr_LayoutData, (AutoInt)(BinEx_P)(BinEx_P)LayoutData);
	}



	/*----------组件组合框EX属性读写函数结束--------*/


}
//MinutesboxEx
namespace exui
{
	MinutesboxEx::MinutesboxEx()
	{

	}
	MinutesboxEx::~MinutesboxEx()
	{

	}
	/*--------创建组件_分组框EX--------*/
	MinutesboxEx_P MinutesboxEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, BindataEx Picture, int IconWidth, int IconHeight, StringEx Text, int TextMode, BindataEx Font, int FontClour, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_MinutesboxEx);
		CreateControl_MinutesboxEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, Picture, IconWidth, IconHeight, Text, TextMode, Font, FontClour, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_分组框Ex--------*/
	void MinutesboxEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_MinutesboxEx(control, index, attribute);
	}
	/*--------取属性_分组框Ex--------*/
	AutoInt MinutesboxEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_MinutesboxEx(control, index);
	}
	/*--------添加组件_分组框Ex--------*/
	BOOL MinutesboxEx::InsertControl(Control_P ControlHand, int left, int top)
	{
		return InsertControl_MinutesboxEx(control, ControlHand, left, top);
	}
	/*--------移除组件_分组框Ex--------*/
	BOOL MinutesboxEx::RemoveControl(Control_P ControlHand, Control_P Parentcontrol, int left, int top)
	{
		return RemoveControl_MinutesboxEx(control, ControlHand, Parentcontrol, left, top);
	}

	/*----------组件分组框EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx MinutesboxEx::Skin()
	{
		return (BinEx_P)GetAttribute_MinutesboxEx(control, MinutesboxEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void MinutesboxEx::Skin(BindataEx Skin)
	{
		SetAttribute_MinutesboxEx(control, MinutesboxEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取图标--------*/
	BindataEx MinutesboxEx::Picture()
	{
		return (BinEx_P)GetAttribute_MinutesboxEx(control, MinutesboxEx_Attr_Picture);
	}
	/*--------置图标--------*/
	void MinutesboxEx::Picture(BindataEx Picture)
	{
		SetAttribute_MinutesboxEx(control, MinutesboxEx_Attr_Picture, (AutoInt)(BinEx_P)Picture);
	}
	/*--------取图标宽--------*/
	int MinutesboxEx::IconWidth()
	{
		return (int)GetAttribute_MinutesboxEx(control, MinutesboxEx_Attr_IconWidth);
	}
	/*--------置图标宽--------*/
	void MinutesboxEx::IconWidth(int IconWidth)
	{
		SetAttribute_MinutesboxEx(control, MinutesboxEx_Attr_IconWidth, (AutoInt)IconWidth);
	}
	/*--------取图标高--------*/
	int MinutesboxEx::IconHeight()
	{
		return (int)GetAttribute_MinutesboxEx(control, MinutesboxEx_Attr_IconHeight);
	}
	/*--------置图标高--------*/
	void MinutesboxEx::IconHeight(int IconHeight)
	{
		SetAttribute_MinutesboxEx(control, MinutesboxEx_Attr_IconHeight, (AutoInt)IconHeight);
	}
	/*--------取标题--------*/
	StringEx MinutesboxEx::Title()
	{
		return (StrEx_P)GetAttribute_MinutesboxEx(control, MinutesboxEx_Attr_Title);
	}
	/*--------置标题--------*/
	void MinutesboxEx::Title(StringEx Title)
	{
		SetAttribute_MinutesboxEx(control, MinutesboxEx_Attr_Title, (AutoInt)(StrEx_P)Title);
	}
	/*--------取风格--------*/
	int MinutesboxEx::Style()
	{
		return (int)GetAttribute_MinutesboxEx(control, MinutesboxEx_Attr_Style);
	}
	/*--------置风格--------*/
	void MinutesboxEx::Style(int Style)
	{
		SetAttribute_MinutesboxEx(control, MinutesboxEx_Attr_Style, (AutoInt)Style);
	}
	/*--------取字体--------*/
	BindataEx MinutesboxEx::Font()
	{
		return (BinEx_P)GetAttribute_MinutesboxEx(control, MinutesboxEx_Attr_Font);
	}
	/*--------置字体--------*/
	void MinutesboxEx::Font(BindataEx Font)
	{
		SetAttribute_MinutesboxEx(control, MinutesboxEx_Attr_Font, (AutoInt)(BinEx_P)Font);
	}
	/*--------取字体色--------*/
	int MinutesboxEx::FontClour()
	{
		return (int)GetAttribute_MinutesboxEx(control, MinutesboxEx_Attr_FontClour);
	}
	/*--------置字体色--------*/
	void MinutesboxEx::FontClour(int FontClour)
	{
		SetAttribute_MinutesboxEx(control, MinutesboxEx_Attr_FontClour, (AutoInt)FontClour);
	}
	/*--------取扩展元素--------*/
	BindataEx MinutesboxEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_MinutesboxEx(control, MinutesboxEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void MinutesboxEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_MinutesboxEx(control, MinutesboxEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx MinutesboxEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_MinutesboxEx(control, MinutesboxEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void MinutesboxEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_MinutesboxEx(control, MinutesboxEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件分组框EX属性读写函数结束--------*/


}
//MultifunctionButtonEx
namespace exui
{
	MultifunctionButtonEx::MultifunctionButtonEx()
	{

	}
	MultifunctionButtonEx::~MultifunctionButtonEx()
	{

	}
	/*--------创建组件_多功能按钮EX--------*/
	MultifunctionButtonEx_P MultifunctionButtonEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, int ButtonStyles, int PartnerSize, int BackColor, BindataEx Icon, int IconWidth, int IconHeight, int Align, StringEx Text, BindataEx Font, int FontClour, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_MultifunctionButtonEx);
		CreateControl_MultifunctionButtonEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, ButtonStyles, PartnerSize, BackColor, Icon, IconWidth, IconHeight, Align, Text, Font, FontClour, ElemeData, Layout);
		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_多功能按钮EX--------*/
	void MultifunctionButtonEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_MultifunctionButtonEx(control, index, attribute);
	}
	/*--------取属性_多功能按钮EX--------*/
	AutoInt MultifunctionButtonEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_MultifunctionButtonEx(control, index);
	}

	/*----------组件多功能按钮EX属性读写函数开始--------*/


	/*--------取皮肤--------*/
	BindataEx MultifunctionButtonEx::Skin()
	{
		return (BinEx_P)GetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void MultifunctionButtonEx::Skin(BindataEx Skin)
	{
		SetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取按钮样式--------*/
	int MultifunctionButtonEx::ButtonStyles()
	{
		return (int)GetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_ButtonStyles);
	}
	/*--------置按钮样式--------*/
	void MultifunctionButtonEx::ButtonStyles(int ButtonStyles)
	{
		SetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_ButtonStyles, (AutoInt)ButtonStyles);
	}
	/*--------取伙伴钮尺寸--------*/
	int MultifunctionButtonEx::PartnerSize()
	{
		return (int)GetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_PartnerSize);
	}
	/*--------置伙伴钮尺寸--------*/
	void MultifunctionButtonEx::PartnerSize(int PartnerSize)
	{
		SetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_PartnerSize, (AutoInt)PartnerSize);
	}
	/*--------取背景颜色--------*/
	int MultifunctionButtonEx::BackColor()
	{
		return (int)GetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_BackColor);
	}
	/*--------置背景颜色--------*/
	void MultifunctionButtonEx::BackColor(int BackColor)
	{
		SetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_BackColor, (AutoInt)BackColor);
	}
	/*--------取图标--------*/
	BindataEx MultifunctionButtonEx::Icon()
	{
		return (BinEx_P)GetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_Icon);
	}
	/*--------置图标--------*/
	void MultifunctionButtonEx::Icon(BindataEx Icon)
	{
		SetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_Icon, (AutoInt)(BinEx_P)Icon);
	}
	/*--------取图标宽--------*/
	int MultifunctionButtonEx::IconWidth()
	{
		return (int)GetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_IconWidth);
	}
	/*--------置图标宽--------*/
	void MultifunctionButtonEx::IconWidth(int IconWidth)
	{
		SetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_IconWidth, (AutoInt)IconWidth);
	}
	/*--------取图标高--------*/
	int MultifunctionButtonEx::IconHeight()
	{
		return (int)GetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_IconHeight);
	}
	/*--------置图标高--------*/
	void MultifunctionButtonEx::IconHeight(int IconHeight)
	{
		SetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_IconHeight, (AutoInt)IconHeight);
	}
	/*--------取对齐方式--------*/
	int MultifunctionButtonEx::Align()
	{
		return (int)GetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_Align);
	}
	/*--------置对齐方式--------*/
	void MultifunctionButtonEx::Align(int Align)
	{
		SetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_Align, (AutoInt)Align);
	}
	/*--------取标题--------*/
	StringEx MultifunctionButtonEx::Title()
	{
		return (StrEx_P)GetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_Title);
	}
	/*--------置标题--------*/
	void MultifunctionButtonEx::Title(StringEx Title)
	{
		SetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_Title, (AutoInt)(StrEx_P)Title);
	}
	/*--------取字体--------*/
	BindataEx MultifunctionButtonEx::Font()
	{
		return (BinEx_P)GetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_Font);
	}
	/*--------置字体--------*/
	void MultifunctionButtonEx::Font(BindataEx Font)
	{
		SetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_Font, (AutoInt)(BinEx_P)Font);
	}
	/*--------取字体色--------*/
	int MultifunctionButtonEx::FontClour()
	{
		return (int)GetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_FontClour);
	}
	/*--------置字体色--------*/
	void MultifunctionButtonEx::FontClour(int FontClour)
	{
		SetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_FontClour, (AutoInt)FontClour);
	}
	/*--------取扩展元素--------*/
	BindataEx MultifunctionButtonEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void MultifunctionButtonEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx MultifunctionButtonEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void MultifunctionButtonEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_MultifunctionButtonEx(control, MultifunctionButtonEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件多功能按钮EX属性读写函数结束--------*/


}
//PictureBoxEx
namespace exui
{
	PictureBoxEx::PictureBoxEx()
	{

	}
	PictureBoxEx::~PictureBoxEx()
	{

	}
	/*--------创建组件_图片框EX--------*/
	PictureBoxEx_P PictureBoxEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, BindataEx Picture, int MapMode, int Angle, BOOL PlayAnimation, int AllFrame, int CurrentFrame, int Rotate, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_PictureBoxEx);
		CreateControl_PictureBoxEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, Picture, MapMode, Angle, PlayAnimation, AllFrame, CurrentFrame, Rotate, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_图片框Ex--------*/
	void PictureBoxEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_PictureBoxEx(control, index, attribute);
	}
	/*--------取属性_图片框Ex--------*/
	AutoInt PictureBoxEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_PictureBoxEx(control, index);
	}


	/*----------组件图片框EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx PictureBoxEx::Skin()
	{
		return (BinEx_P)GetAttribute_PictureBoxEx(control, PictureBoxEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void PictureBoxEx::Skin(BindataEx Skin)
	{
		SetAttribute_PictureBoxEx(control, PictureBoxEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取图片--------*/
	BindataEx PictureBoxEx::Picture()
	{
		return (BinEx_P)GetAttribute_PictureBoxEx(control, PictureBoxEx_Attr_Picture);
	}
	/*--------置图片--------*/
	void PictureBoxEx::Picture(BindataEx Picture)
	{
		SetAttribute_PictureBoxEx(control, PictureBoxEx_Attr_Picture, (AutoInt)(BinEx_P)Picture);
	}
	/*--------取底图方式--------*/
	int PictureBoxEx::MapMode()
	{
		return (int)GetAttribute_PictureBoxEx(control, PictureBoxEx_Attr_MapMode);
	}
	/*--------置底图方式--------*/
	void PictureBoxEx::MapMode(int MapMode)
	{
		SetAttribute_PictureBoxEx(control, PictureBoxEx_Attr_MapMode, (AutoInt)MapMode);
	}
	/*--------取圆角度--------*/
	int PictureBoxEx::Angle()
	{
		return (int)GetAttribute_PictureBoxEx(control, PictureBoxEx_Attr_Angle);
	}
	/*--------置圆角度--------*/
	void PictureBoxEx::Angle(int Angle)
	{
		SetAttribute_PictureBoxEx(control, PictureBoxEx_Attr_Angle, (AutoInt)Angle);
	}
	/*--------取播放动画--------*/
	BOOL PictureBoxEx::PlayAnimation()
	{
		return (BOOL)GetAttribute_PictureBoxEx(control, PictureBoxEx_Attr_PlayAnimation);
	}
	/*--------置播放动画--------*/
	void PictureBoxEx::PlayAnimation(BOOL PlayAnimation)
	{
		SetAttribute_PictureBoxEx(control, PictureBoxEx_Attr_PlayAnimation, (AutoInt)PlayAnimation);
	}
	/*--------取总帧数--------*/
	int PictureBoxEx::AllFrame()
	{
		return (int)GetAttribute_PictureBoxEx(control, PictureBoxEx_Attr_AllFrame);
	}
	/*--------置总帧数--------*/
	void PictureBoxEx::AllFrame(int AllFrame)
	{
		SetAttribute_PictureBoxEx(control, PictureBoxEx_Attr_AllFrame, (AutoInt)AllFrame);
	}
	/*--------取当前帧--------*/
	int PictureBoxEx::CurrentFrame()
	{
		return (int)GetAttribute_PictureBoxEx(control, PictureBoxEx_Attr_CurrentFrame);
	}
	/*--------置当前帧--------*/
	void PictureBoxEx::CurrentFrame(int CurrentFrame)
	{
		SetAttribute_PictureBoxEx(control, PictureBoxEx_Attr_CurrentFrame, (AutoInt)CurrentFrame);
	}
	/*--------取旋转角度--------*/
	int PictureBoxEx::Rotate()
	{
		return (int)GetAttribute_PictureBoxEx(control, PictureBoxEx_Attr_Rotate);
	}
	/*--------置旋转角度--------*/
	void PictureBoxEx::Rotate(int Rotate)
	{
		SetAttribute_PictureBoxEx(control, PictureBoxEx_Attr_Rotate, (AutoInt)Rotate);
	}
	/*--------取扩展元素--------*/
	BindataEx PictureBoxEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_PictureBoxEx(control, PictureBoxEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void PictureBoxEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_PictureBoxEx(control, PictureBoxEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx PictureBoxEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_PictureBoxEx(control, PictureBoxEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void PictureBoxEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_PictureBoxEx(control, PictureBoxEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件图片框EX属性读写函数结束--------*/


}
//ProgressbarEx
namespace exui
{
	ProgressbarEx::ProgressbarEx()
	{

	}
	ProgressbarEx::~ProgressbarEx()
	{

	}
	/*--------创建组件_进度条EX--------*/
	ProgressbarEx_P ProgressbarEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, int Position, int MiniPosition, int MaxiPosition, int Style, int Direction, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_WindowEx);
		CreateControl_ProgressbarEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, Position, MiniPosition, MaxiPosition, Style, Direction, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_进度条Ex--------*/
	void ProgressbarEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_ProgressbarEx(control, index, attribute);
	}
	/*--------取属性_进度条Ex--------*/
	AutoInt ProgressbarEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_ProgressbarEx(control, index);
	}

	/*----------组件进度条EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx ProgressbarEx::Skin()
	{
		return (BinEx_P)GetAttribute_ProgressbarEx(control, ProgressbarEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void ProgressbarEx::Skin(BindataEx Skin)
	{
		SetAttribute_ProgressbarEx(control, ProgressbarEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取位置--------*/
	int ProgressbarEx::Position()
	{
		return (int)GetAttribute_ProgressbarEx(control, ProgressbarEx_Attr_Position);
	}
	/*--------置位置--------*/
	void ProgressbarEx::Position(int Position)
	{
		SetAttribute_ProgressbarEx(control, ProgressbarEx_Attr_Position, (AutoInt)Position);
	}
	/*--------取最小位置--------*/
	int ProgressbarEx::MiniPosition()
	{
		return (int)GetAttribute_ProgressbarEx(control, ProgressbarEx_Attr_MiniPosition);
	}
	/*--------置最小位置--------*/
	void ProgressbarEx::MiniPosition(int MiniPosition)
	{
		SetAttribute_ProgressbarEx(control, ProgressbarEx_Attr_MiniPosition, (AutoInt)MiniPosition);
	}
	/*--------取最大位置--------*/
	int ProgressbarEx::MaxiPosition()
	{
		return (int)GetAttribute_ProgressbarEx(control, ProgressbarEx_Attr_MaxiPosition);
	}
	/*--------置最大位置--------*/
	void ProgressbarEx::MaxiPosition(int MaxiPosition)
	{
		SetAttribute_ProgressbarEx(control, ProgressbarEx_Attr_MaxiPosition, (AutoInt)MaxiPosition);
	}
	/*--------取进度样式--------*/
	int ProgressbarEx::Style()
	{
		return (int)GetAttribute_ProgressbarEx(control, ProgressbarEx_Attr_Style);
	}
	/*--------置进度样式--------*/
	void ProgressbarEx::Style(int Style)
	{
		SetAttribute_ProgressbarEx(control, ProgressbarEx_Attr_Style, (AutoInt)Style);
	}
	/*--------取进度方向--------*/
	int ProgressbarEx::Direction()
	{
		return (int)GetAttribute_ProgressbarEx(control, ProgressbarEx_Attr_Direction);
	}
	/*--------置进度方向--------*/
	void ProgressbarEx::Direction(int Direction)
	{
		SetAttribute_ProgressbarEx(control, ProgressbarEx_Attr_Direction, (AutoInt)Direction);
	}
	/*--------取扩展元素--------*/
	BindataEx ProgressbarEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_ProgressbarEx(control, ProgressbarEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void ProgressbarEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_ProgressbarEx(control, ProgressbarEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx ProgressbarEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_ProgressbarEx(control, ProgressbarEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void ProgressbarEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_ProgressbarEx(control, ProgressbarEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件进度条EX属性读写函数结束--------*/


}
//ScrollbarEx
namespace exui
{
	ScrollbarEx::ScrollbarEx()
	{

	}
	ScrollbarEx::~ScrollbarEx()
	{

	}
	/*--------创建组件_滚动条EX--------*/
	ScrollbarEx_P ScrollbarEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, int Position, int MiniPosition, int MaxiPosition, int PageLength, int RowChangeValue, int PageChangeValue, int ScrollChangeValue, BOOL DragTrace, BOOL schedule, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_ScrollbarEx);
		CreateControl_ScrollbarEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, Position, MiniPosition, MaxiPosition, PageLength, RowChangeValue, PageChangeValue, ScrollChangeValue, DragTrace, schedule, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_滚动条Ex--------*/
	void ScrollbarEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_ScrollbarEx(control, index, attribute);
	}
	/*--------取属性_滚动条Ex--------*/
	AutoInt ScrollbarEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_ScrollbarEx(control, index);
	}

	/*----------组件滚动条EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx ScrollbarEx::Skin()
	{
		return (BinEx_P)GetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void ScrollbarEx::Skin(BindataEx Skin)
	{
		SetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取位置--------*/
	int ScrollbarEx::Position()
	{
		return (int)GetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_Position);
	}
	/*--------置位置--------*/
	void ScrollbarEx::Position(int Position)
	{
		SetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_Position, (AutoInt)Position);
	}
	/*--------取最小位置--------*/
	int ScrollbarEx::MiniPosition()
	{
		return (int)GetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_MiniPosition);
	}
	/*--------置最小位置--------*/
	void ScrollbarEx::MiniPosition(int MiniPosition)
	{
		SetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_MiniPosition, (AutoInt)MiniPosition);
	}
	/*--------取最大位置--------*/
	int ScrollbarEx::MaxiPosition()
	{
		return (int)GetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_MaxiPosition);
	}
	/*--------置最大位置--------*/
	void ScrollbarEx::MaxiPosition(int MaxiPosition)
	{
		SetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_MaxiPosition, (AutoInt)MaxiPosition);
	}
	/*--------取页长--------*/
	int ScrollbarEx::PageLength()
	{
		return (int)GetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_PageLength);
	}
	/*--------置页长--------*/
	void ScrollbarEx::PageLength(int PageLength)
	{
		SetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_PageLength, (AutoInt)PageLength);
	}
	/*--------取行改变值--------*/
	int ScrollbarEx::RowChangeValue()
	{
		return (int)GetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_RowChangeValue);
	}
	/*--------置行改变值--------*/
	void ScrollbarEx::RowChangeValue(int RowChangeValue)
	{
		SetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_RowChangeValue, (AutoInt)RowChangeValue);
	}
	/*--------取页改变值--------*/
	int ScrollbarEx::PageChangeValue()
	{
		return (int)GetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_PageChangeValue);
	}
	/*--------置页改变值--------*/
	void ScrollbarEx::PageChangeValue(int PageChangeValue)
	{
		SetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_PageChangeValue, (AutoInt)PageChangeValue);
	}
	/*--------取滚动改变值--------*/
	int ScrollbarEx::ScrollChangeValue()
	{
		return (int)GetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_ScrollChangeValue);
	}
	/*--------置滚动改变值--------*/
	void ScrollbarEx::ScrollChangeValue(int ScrollChangeValue)
	{
		SetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_ScrollChangeValue, (AutoInt)ScrollChangeValue);
	}
	/*--------取容许拖动跟踪--------*/
	BOOL ScrollbarEx::DragTrace()
	{
		return (BOOL)GetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_DragTrace);
	}
	/*--------置容许拖动跟踪--------*/
	void ScrollbarEx::DragTrace(BOOL DragTrace)
	{
		SetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_DragTrace, (AutoInt)DragTrace);
	}
	/*--------取纵向--------*/
	BOOL ScrollbarEx::schedule()
	{
		return (BOOL)GetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_schedule);
	}
	/*--------置纵向--------*/
	void ScrollbarEx::schedule(BOOL schedule)
	{
		SetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_schedule, (AutoInt)schedule);
	}
	/*--------取扩展元素--------*/
	BindataEx ScrollbarEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void ScrollbarEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx ScrollbarEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void ScrollbarEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_ScrollbarEx(control, ScrollbarEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件滚动条EX属性读写函数结束--------*/


}
//SliderbarEx
namespace exui
{
	SliderbarEx::SliderbarEx()
	{

	}
	SliderbarEx::~SliderbarEx()
	{

	}
	/*--------创建组件_滑块条EX--------*/
	SliderbarEx_P SliderbarEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, int Position, int Progress, int MiniPosition, int MaxiPosition, int RowChangeValue, int PageChangeValue, BOOL DragTrace, int Style, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_SliderbarEx);
		CreateControl_SliderbarEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, Position, Progress, MiniPosition, MaxiPosition, RowChangeValue, PageChangeValue, DragTrace, Style, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_滑块条Ex--------*/
	void SliderbarEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_SliderbarEx(control, index, attribute);
	}
	/*--------取属性_滑块条Ex--------*/
	AutoInt SliderbarEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_SliderbarEx(control, index);
	}

	/*----------组件滑块条EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx SliderbarEx::Skin()
	{
		return (BinEx_P)GetAttribute_SliderbarEx(control, SliderbarEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void SliderbarEx::Skin(BindataEx Skin)
	{
		SetAttribute_SliderbarEx(control, SliderbarEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取位置--------*/
	int SliderbarEx::Position()
	{
		return (int)GetAttribute_SliderbarEx(control, SliderbarEx_Attr_Position);
	}
	/*--------置位置--------*/
	void SliderbarEx::Position(int Position)
	{
		SetAttribute_SliderbarEx(control, SliderbarEx_Attr_Position, (AutoInt)Position);
	}
	/*--------取进度位置--------*/
	int SliderbarEx::Progress()
	{
		return (int)GetAttribute_SliderbarEx(control, SliderbarEx_Attr_Progress);
	}
	/*--------置进度位置--------*/
	void SliderbarEx::Progress(int Progress)
	{
		SetAttribute_SliderbarEx(control, SliderbarEx_Attr_Progress, (AutoInt)Progress);
	}
	/*--------取最小位置--------*/
	int SliderbarEx::MiniPosition()
	{
		return (int)GetAttribute_SliderbarEx(control, SliderbarEx_Attr_MiniPosition);
	}
	/*--------置最小位置--------*/
	void SliderbarEx::MiniPosition(int MiniPosition)
	{
		SetAttribute_SliderbarEx(control, SliderbarEx_Attr_MiniPosition, (AutoInt)MiniPosition);
	}
	/*--------取最大位置--------*/
	int SliderbarEx::MaxiPosition()
	{
		return (int)GetAttribute_SliderbarEx(control, SliderbarEx_Attr_MaxiPosition);
	}
	/*--------置最大位置--------*/
	void SliderbarEx::MaxiPosition(int MaxiPosition)
	{
		SetAttribute_SliderbarEx(control, SliderbarEx_Attr_MaxiPosition, (AutoInt)MaxiPosition);
	}
	/*--------取滚动改变值--------*/
	int SliderbarEx::ScrollChangeValue()
	{
		return (int)GetAttribute_SliderbarEx(control, SliderbarEx_Attr_ScrollChangeValue);
	}
	/*--------置滚动改变值--------*/
	void SliderbarEx::ScrollChangeValue(int ScrollChangeValue)
	{
		SetAttribute_SliderbarEx(control, SliderbarEx_Attr_ScrollChangeValue, (AutoInt)ScrollChangeValue);
	}
	/*--------取页改变值--------*/
	int SliderbarEx::PageChangeValue()
	{
		return (int)GetAttribute_SliderbarEx(control, SliderbarEx_Attr_PageChangeValue);
	}
	/*--------置页改变值--------*/
	void SliderbarEx::PageChangeValue(int PageChangeValue)
	{
		SetAttribute_SliderbarEx(control, SliderbarEx_Attr_PageChangeValue, (AutoInt)PageChangeValue);
	}
	/*--------取容许拖动跟踪--------*/
	BOOL SliderbarEx::DragTrace()
	{
		return (BOOL)GetAttribute_SliderbarEx(control, SliderbarEx_Attr_DragTrace);
	}
	/*--------置容许拖动跟踪--------*/
	void SliderbarEx::DragTrace(BOOL DragTrace)
	{
		SetAttribute_SliderbarEx(control, SliderbarEx_Attr_DragTrace, (AutoInt)DragTrace);
	}
	/*--------取样式--------*/
	int SliderbarEx::Style()
	{
		return (int)GetAttribute_SliderbarEx(control, SliderbarEx_Attr_Style);
	}
	/*--------置样式--------*/
	void SliderbarEx::Style(int Style)
	{
		SetAttribute_SliderbarEx(control, SliderbarEx_Attr_Style, (AutoInt)Style);
	}
	/*--------取扩展元素--------*/
	BindataEx SliderbarEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_SliderbarEx(control, SliderbarEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void SliderbarEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_SliderbarEx(control, SliderbarEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx SliderbarEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_SliderbarEx(control, SliderbarEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void SliderbarEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_SliderbarEx(control, SliderbarEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件滑块条EX属性读写函数结束--------*/


}
//SelectthefolderEx
namespace exui
{
	SelectthefolderEx::SelectthefolderEx()
	{

	}
	SelectthefolderEx::~SelectthefolderEx()
	{

	}
	/*--------创建组件_选择夹EX--------*/
	SelectthefolderEx_P SelectthefolderEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, AutoInt CurrentTable, int TableSize, int Direction, int Spacing, int TableStyle, int Retain, int IconWidth, int IconHeight, BindataEx Font, BindataEx TableData, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_SelectthefolderEx);
		CreateControl_SelectthefolderEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, CurrentTable, TableSize, Direction, Spacing, TableStyle, Retain, IconWidth, IconHeight, Font, TableData, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_选择夹Ex--------*/
	void SelectthefolderEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_SelectthefolderEx(control, index, attribute);
	}
	/*--------取属性_选择夹Ex--------*/
	AutoInt SelectthefolderEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_SelectthefolderEx(control, index);
	}
	/*--------插入_选择夹Ex--------*/
	AutoInt SelectthefolderEx::InsertTab(AutoInt index, AutoInt Data, BitmapEx Ico, StringEx Title, int FontColor, int Size)
	{
		return InsertTab_SelectthefolderEx(control, index, Data, Ico, Title, FontColor, Size);
	}
	/*--------删除_选择夹Ex--------*/
	void SelectthefolderEx::DeleteTab(AutoInt ctrlindex)
	{
		DeleteTab_SelectthefolderEx(control, ctrlindex);
	}
	/*--------取数量_选择夹Ex--------*/
	AutoInt SelectthefolderEx::GetTabCount()
	{
		return GetTabCount_SelectthefolderEx(control);
	}
	/*--------取关联数值_选择夹Ex--------*/
	AutoInt SelectthefolderEx::GetTabData(AutoInt index)
	{
		return GetTabData_SelectthefolderEx(control, index);
	}
	/*--------置关联数值_选择夹Ex--------*/
	void SelectthefolderEx::SetTabData(AutoInt index, AutoInt Data)
	{
		SetTabData_SelectthefolderEx(control, index, Data);
	}
	/*--------置图标_选择夹Ex--------*/
	void SelectthefolderEx::SetTabIco(AutoInt index, BitmapEx Ico)
	{
		SetTabIco_SelectthefolderEx(control, index, Ico);
	}
	/*--------取图标_选择夹Ex--------*/
	BitmapEx SelectthefolderEx::GetTabIco(AutoInt index)
	{
		return GetTabIco_SelectthefolderEx(control, index);
	}
	/*--------置文本_选择夹Ex--------*/
	void SelectthefolderEx::SetTabTitle(AutoInt index, StringEx Title)
	{
		SetTabTitle_SelectthefolderEx(control, index, Title);
	}
	/*--------取文本_选择夹Ex--------*/
	StringEx SelectthefolderEx::GetTabTitle(AutoInt index)
	{
		return GetTabTitle_SelectthefolderEx(control, index);
	}
	/*--------置字体色_选择夹Ex--------*/
	void SelectthefolderEx::SetTabFontColor(AutoInt index, int FontColor)
	{
		SetTabFontColor_SelectthefolderEx(control, index, FontColor);
	}
	/*--------取字体色_选择夹Ex--------*/
	int SelectthefolderEx::GetTabFontColor(AutoInt index)
	{
		return GetTabFontColor_SelectthefolderEx(control, index);
	}
	/*--------置尺寸_选择夹Ex--------*/
	void SelectthefolderEx::SetTabSize(AutoInt index, int Size)
	{
		SetTabSize_SelectthefolderEx(control, index, Size);
	}
	/*--------取尺寸_选择夹Ex--------*/
	int SelectthefolderEx::GetTabSize(AutoInt index)
	{
		return GetTabSize_SelectthefolderEx(control, index);
	}
	/*--------保证显示_选择夹Ex--------*/
	void SelectthefolderEx::GuaranteeVisible(AutoInt index, int DisplayMode)
	{
		GuaranteeVisible_SelectthefolderEx(control, index, DisplayMode);
	}
	/*--------添加组件_选择夹Ex--------*/
	BOOL SelectthefolderEx::InsertControl(AutoInt Table, Control_P ControlHand, int left, int top)
	{
		return InsertControl_SelectthefolderEx(control, Table, ControlHand, left, top);
	}
	/*--------移除组件_选择夹Ex--------*/
	BOOL SelectthefolderEx::RemoveControl(AutoInt Table, Control_P ControlHand, Control_P Parentcontrol, int left, int top)
	{
		return RemoveControl_SelectthefolderEx(control, Table, ControlHand, Parentcontrol, left, top);
	}

	/*----------组件选择夹EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx SelectthefolderEx::Skin()
	{
		return (BinEx_P)GetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void SelectthefolderEx::Skin(BindataEx Skin)
	{
		SetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取现行子夹--------*/
	int SelectthefolderEx::CurrentTable()
	{
		return (int)GetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_CurrentTable);
	}
	/*--------置现行子夹--------*/
	void SelectthefolderEx::CurrentTable(int CurrentTable)
	{
		SetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_CurrentTable, (AutoInt)CurrentTable);
	}
	/*--------取子夹头尺寸--------*/
	int SelectthefolderEx::TableSize()
	{
		return (int)GetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_TableSize);
	}
	/*--------置子夹头尺寸--------*/
	void SelectthefolderEx::TableSize(int TableSize)
	{
		SetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_TableSize, (AutoInt)TableSize);
	}
	/*--------取子夹头方向--------*/
	int SelectthefolderEx::Direction()
	{
		return (int)GetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_Direction);
	}
	/*--------置子夹头方向--------*/
	void SelectthefolderEx::Direction(int Direction)
	{
		SetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_Direction, (AutoInt)Direction);
	}
	/*--------取间距--------*/
	int SelectthefolderEx::Spacing()
	{
		return (int)GetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_Spacing);
	}
	/*--------置间距--------*/
	void SelectthefolderEx::Spacing(int Spacing)
	{
		SetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_Spacing, (AutoInt)Spacing);
	}
	/*--------取子夹头样式--------*/
	int SelectthefolderEx::TableStyle()
	{
		return (int)GetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_TableStyle);
	}
	/*--------置子夹头样式--------*/
	void SelectthefolderEx::TableStyle(int TableStyle)
	{
		SetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_TableStyle, (AutoInt)TableStyle);
	}
	/*--------取保留属性--------*/
	int SelectthefolderEx::Reserve()
	{
		return (int)GetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_Reserve);
	}
	/*--------置保留属性--------*/
	void SelectthefolderEx::Reserve(int Reserve)
	{
		SetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_Reserve, (AutoInt)Reserve);
	}
	/*--------取图标宽--------*/
	int SelectthefolderEx::IconWidth()
	{
		return (int)GetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_IconWidth);
	}
	/*--------置图标宽--------*/
	void SelectthefolderEx::IconWidth(int IconWidth)
	{
		SetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_IconWidth, (AutoInt)IconWidth);
	}
	/*--------取图标高--------*/
	int SelectthefolderEx::IconHeight()
	{
		return (int)GetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_IconHeight);
	}
	/*--------置图标高--------*/
	void SelectthefolderEx::IconHeight(int IconHeight)
	{
		SetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_IconHeight, (AutoInt)IconHeight);
	}
	/*--------取字体--------*/
	BindataEx SelectthefolderEx::Font()
	{
		return (BinEx_P)GetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_Font);
	}
	/*--------置字体--------*/
	void SelectthefolderEx::Font(BindataEx Font)
	{
		SetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_Font, (AutoInt)(BinEx_P)Font);
	}
	/*--------取子夹管理--------*/
	BindataEx SelectthefolderEx::TableData()
	{
		return (BinEx_P)GetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_TableData);
	}
	/*--------置子夹管理--------*/
	void SelectthefolderEx::TableData(BindataEx TableData)
	{
		SetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_TableData, (AutoInt)(BinEx_P)TableData);
	}
	/*--------取扩展元素--------*/
	BindataEx SelectthefolderEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void SelectthefolderEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx SelectthefolderEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void SelectthefolderEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_SelectthefolderEx(control, SelectthefolderEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件选择夹EX属性读写函数结束--------*/

}
//ToolbarEx
namespace exui
{
	ToolbarEx::ToolbarEx()
	{

	}
	ToolbarEx::~ToolbarEx()
	{

	}
	/*--------创建组件_工具条EX--------*/
	ToolbarEx_P ToolbarEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, int BackMode, BOOL schedule, int Spacing, int IconWidth, int IconHeight, BindataEx Font, BindataEx ButtonData, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_ToolbarEx);
		CreateControl_ToolbarEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, BackMode, schedule, Spacing, IconWidth, IconHeight, Font, ButtonData, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_工具条Ex--------*/
	void ToolbarEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_ToolbarEx(control, index, attribute);
	}
	/*--------取属性_工具条Ex--------*/
	AutoInt ToolbarEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_ToolbarEx(control, index);
	}
	/*--------插入_工具条Ex--------*/
	AutoInt ToolbarEx::InsertButton(AutoInt index, AutoInt Data, BitmapEx Ico, StringEx Title, int FontColor, int Size, int Type, int Align, BOOL Disabled, BOOL Selected)
	{
		return InsertButton_ToolbarEx(control, index, Data, Ico, Title, FontColor, Size, Type, Align, Disabled, Selected);
	}
	/*--------删除_工具条Ex--------*/
	void ToolbarEx::DeleteButton(AutoInt index)
	{
		DeleteButton_ToolbarEx(control, index);
	}
	/*--------取数量_工具条Ex--------*/
	AutoInt ToolbarEx::GetButtonCount()
	{
		return GetButtonCount_ToolbarEx(control);
	}
	/*--------置关联数值_工具条Ex--------*/
	void ToolbarEx::SetButtonData(AutoInt index, AutoInt Data)
	{
		SetButtonData_ToolbarEx(control, index, Data);
	}
	/*--------取关联数值_工具条Ex--------*/
	AutoInt ToolbarEx::GetButtonData(AutoInt index)
	{
		return GetButtonData_ToolbarEx(control, index);
	}
	/*--------置图标_工具条Ex--------*/
	void ToolbarEx::SetButtonIco(AutoInt index, BitmapEx Ico)
	{
		SetButtonIco_ToolbarEx(control, index, Ico);
	}
	/*--------取图标_工具条Ex--------*/
	BitmapEx ToolbarEx::GetButtonIco(AutoInt index)
	{
		return GetButtonIco_ToolbarEx(control, index);
	}
	/*--------置文本_工具条Ex--------*/
	void ToolbarEx::SetButtonTitle(AutoInt index, StringEx Title)
	{
		SetButtonTitle_ToolbarEx(control, index, Title);
	}
	/*--------取文本_工具条Ex--------*/
	StringEx ToolbarEx::GetButtonTitle(AutoInt index)
	{
		return GetButtonTitle_ToolbarEx(control, index);
	}
	/*--------置字体色_工具条Ex--------*/
	void ToolbarEx::SetButtonFontColor(AutoInt index, int FontColor)
	{
		SetButtonFontColor_ToolbarEx(control, index, FontColor);
	}
	/*--------取字体色_工具条Ex--------*/
	int ToolbarEx::GetButtonFontColor(AutoInt index)
	{
		return GetButtonFontColor_ToolbarEx(control, index);
	}
	/*--------置尺寸_工具条Ex--------*/
	void ToolbarEx::SetButtonSize(AutoInt index, int Size)
	{
		SetButtonSize_ToolbarEx(control, index, Size);
	}
	/*--------取尺寸_工具条Ex--------*/
	int ToolbarEx::GetButtonSize(AutoInt index)
	{
		return GetButtonSize_ToolbarEx(control, index);
	}
	/*--------置类型_工具条Ex--------*/
	void ToolbarEx::SetButtonType(AutoInt index, int Type)
	{
		SetButtonType_ToolbarEx(control, index, Type);
	}
	/*--------取类型_工具条Ex--------*/
	int ToolbarEx::GetButtonType(AutoInt index)
	{
		return GetButtonType_ToolbarEx(control, index);
	}
	/*--------置对齐方式_工具条Ex--------*/
	void ToolbarEx::SetButtonAlign(AutoInt index, int Align)
	{
		SetButtonAlign_ToolbarEx(control, index, Align);
	}
	/*--------取对齐方式_工具条Ex--------*/
	int ToolbarEx::GetButtonAlign(AutoInt index)
	{
		return GetButtonAlign_ToolbarEx(control, index);
	}
	/*--------置选中_工具条Ex--------*/
	void ToolbarEx::SetButtonSelected(AutoInt index, BOOL Selected)
	{
		SetButtonSelected_ToolbarEx(control, index, Selected);
	}
	/*--------取选中_工具条Ex--------*/
	BOOL ToolbarEx::GetButtonSelected(AutoInt index)
	{
		return GetButtonSelected_ToolbarEx(control, index);
	}
	/*--------置禁止_工具条Ex--------*/
	void ToolbarEx::SetButtonDisabled(AutoInt index, BOOL Disabled)
	{
		SetButtonDisabled_ToolbarEx(control, index, Disabled);
	}
	/*--------取禁止_工具条Ex--------*/
	BOOL ToolbarEx::GetButtonDisabled(AutoInt index)
	{
		return GetButtonDisabled_ToolbarEx(control, index);
	}
	/*--------置字体_工具条Ex--------*/
	void ToolbarEx::SetButtonFont(AutoInt index, BindataEx Font)
	{
		SetButtonFont_ToolbarEx(control, index, Font);
	}
	/*--------取字体_工具条Ex--------*/
	BindataEx ToolbarEx::GetButtonFont(AutoInt index)
	{
		return GetButtonFont_ToolbarEx(control, index);
	}
	/*--------置皮肤_工具条Ex--------*/
	void ToolbarEx::SetButtonSkin(AutoInt index, BindataEx Skin)
	{
		SetButtonSkin_ToolbarEx(control, index, Skin);
	}
	/*--------取皮肤_工具条Ex--------*/
	BindataEx ToolbarEx::GetButtonSkin(AutoInt index)
	{
		return GetButtonSkin_ToolbarEx(control, index);
	}
	/*--------保证显示_工具条Ex--------*/
	void ToolbarEx::GuaranteeVisible(AutoInt index, int DisplayMode)
	{
		GuaranteeVisible_ToolbarEx(control, index, DisplayMode);
	}

	/*----------组件工具条EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx ToolbarEx::Skin()
	{
		return (BinEx_P)GetAttribute_ToolbarEx(control, ToolbarEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void ToolbarEx::Skin(BindataEx Skin)
	{
		SetAttribute_ToolbarEx(control, ToolbarEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取背景样式--------*/
	int ToolbarEx::BackMode()
	{
		return (int)GetAttribute_ToolbarEx(control, ToolbarEx_Attr_BackMode);
	}
	/*--------置背景样式--------*/
	void ToolbarEx::BackMode(int BackMode)
	{
		SetAttribute_ToolbarEx(control, ToolbarEx_Attr_BackMode, (AutoInt)BackMode);
	}
	/*--------取纵向--------*/
	BOOL ToolbarEx::schedule()
	{
		return (BOOL)GetAttribute_ToolbarEx(control, ToolbarEx_Attr_schedule);
	}
	/*--------置纵向--------*/
	void ToolbarEx::schedule(BOOL schedule)
	{
		SetAttribute_ToolbarEx(control, ToolbarEx_Attr_schedule, (AutoInt)schedule);
	}
	/*--------取间距--------*/
	int ToolbarEx::Spacing()
	{
		return (int)GetAttribute_ToolbarEx(control, ToolbarEx_Attr_Spacing);
	}
	/*--------置间距--------*/
	void ToolbarEx::Spacing(int Spacing)
	{
		SetAttribute_ToolbarEx(control, ToolbarEx_Attr_Spacing, (AutoInt)Spacing);
	}
	/*--------取图标宽--------*/
	int ToolbarEx::IconWidth()
	{
		return (int)GetAttribute_ToolbarEx(control, ToolbarEx_Attr_IconWidth);
	}
	/*--------置图标宽--------*/
	void ToolbarEx::IconWidth(int IconWidth)
	{
		SetAttribute_ToolbarEx(control, ToolbarEx_Attr_IconWidth, (AutoInt)IconWidth);
	}
	/*--------取图标高--------*/
	int ToolbarEx::IconHeight()
	{
		return (int)GetAttribute_ToolbarEx(control, ToolbarEx_Attr_IconHeight);
	}
	/*--------置图标高--------*/
	void ToolbarEx::IconHeight(int IconHeight)
	{
		SetAttribute_ToolbarEx(control, ToolbarEx_Attr_IconHeight, (AutoInt)IconHeight);
	}
	/*--------取字体--------*/
	BindataEx ToolbarEx::Font()
	{
		return (BinEx_P)GetAttribute_ToolbarEx(control, ToolbarEx_Attr_Font);
	}
	/*--------置字体--------*/
	void ToolbarEx::Font(BindataEx Font)
	{
		SetAttribute_ToolbarEx(control, ToolbarEx_Attr_Font, (AutoInt)(BinEx_P)Font);
	}
	/*--------取按钮管理--------*/
	BindataEx ToolbarEx::ButtonData()
	{
		return (BinEx_P)GetAttribute_ToolbarEx(control, ToolbarEx_Attr_ButtonData);
	}
	/*--------置按钮管理--------*/
	void ToolbarEx::ButtonData(BindataEx ButtonData)
	{
		SetAttribute_ToolbarEx(control, ToolbarEx_Attr_ButtonData, (AutoInt)(BinEx_P)ButtonData);
	}
	/*--------取扩展元素--------*/
	BindataEx ToolbarEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_ToolbarEx(control, ToolbarEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void ToolbarEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_ToolbarEx(control, ToolbarEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx ToolbarEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_ToolbarEx(control, ToolbarEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void ToolbarEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_ToolbarEx(control, ToolbarEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件工具条EX属性读写函数结束--------*/


}
//ListboxEx
namespace exui
{
	ListboxEx::ListboxEx()
	{

	}
	ListboxEx::~ListboxEx()
	{

	}
	/*--------创建组件_列表框EX--------*/
	ListboxEx_P ListboxEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, AutoInt CurrentItem, int IconWidth, int IconHeight, BindataEx Font, BOOL AlternateColor, int SelectMode, int ScrollBarMode, BindataEx ItemData, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_ListboxEx);
		CreateControl_ListboxEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, CurrentItem, IconWidth, IconHeight, Font, AlternateColor, SelectMode, ScrollBarMode, ItemData, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_列表框Ex--------*/
	void ListboxEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_ListboxEx(control, index, attribute);
	}
	/*--------取属性_列表框Ex--------*/
	AutoInt ListboxEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_ListboxEx(control, index);
	}
	/*--------插入_列表框Ex--------*/
	AutoInt ListboxEx::InsertItem(AutoInt index, AutoInt addnum, AutoInt Data, BitmapEx Ico, StringEx Title, int FontColor, int Size)
	{
		return InsertItem_ListboxEx(control, index, addnum, Data, Ico, Title, FontColor, Size);
	}
	/*--------删除_列表框Ex--------*/
	void ListboxEx::DeleteItem(AutoInt index, AutoInt deletenum)
	{
		DeleteItem_ListboxEx(control, index, deletenum);
	}
	/*--------取数量_列表框Ex--------*/
	AutoInt ListboxEx::GetItemCount()
	{
		return GetItemCount_ListboxEx(control);
	}
	/*--------置附加值_列表框Ex--------*/
	void ListboxEx::SetItemData(AutoInt index, AutoInt Data)
	{
		SetItemData_ListboxEx(control, index, Data);
	}
	/*--------取附加值_列表框Ex--------*/
	AutoInt ListboxEx::GetItemData(AutoInt index)
	{
		return GetItemData_ListboxEx(control, index);
	}
	/*--------置图标_列表框Ex--------*/
	void ListboxEx::SetItemIco(AutoInt index, BitmapEx Ico)
	{
		SetItemIco_ListboxEx(control, index, Ico);
	}
	/*--------取图标_列表框Ex--------*/
	BitmapEx ListboxEx::GetItemIco(AutoInt index)
	{
		return GetItemIco_ListboxEx(control, index);
	}
	/*--------置标题_列表框Ex--------*/
	void ListboxEx::SetItemTitle(AutoInt index, StringEx Title)
	{
		SetItemTitle_ListboxEx(control, index, Title);
	}
	/*--------取标题_列表框Ex--------*/
	StringEx ListboxEx::GetItemTitle(AutoInt index)
	{
		return GetItemTitle_ListboxEx(control, index);
	}
	/*--------取字体色_列表框Ex--------*/
	int ListboxEx::GetItemFontColor(AutoInt index)
	{
		return GetItemFontColor_ListboxEx(control, index);
	}
	/*--------置字体色_列表框Ex--------*/
	void ListboxEx::SetItemFontColor(AutoInt index, int FontColor)
	{
		SetItemFontColor_ListboxEx(control, index, FontColor);
	}
	/*--------置项目高度_列表框Ex--------*/
	void ListboxEx::SetItemSize(AutoInt index, int Size)
	{
		SetItemSize_ListboxEx(control, index, Size);
	}
	/*--------取项目高度_列表框Ex--------*/
	int ListboxEx::GetItemSize(AutoInt index)
	{
		return GetItemSize_ListboxEx(control, index);
	}
	/*--------置字体_列表框Ex--------*/
	void ListboxEx::SetItemFont(AutoInt index, BindataEx Font)
	{
		SetItemFont_ListboxEx(control, index, Font);
	}
	/*--------取字体_列表框Ex--------*/
	BindataEx ListboxEx::GetItemFont(AutoInt index)
	{
		return GetItemFont_ListboxEx(control, index);
	}
	/*--------置皮肤_列表框Ex--------*/
	void ListboxEx::SetItemSkin(AutoInt index, BindataEx Skin)
	{
		SetItemSkin_ListboxEx(control, index, Skin);
	}
	/*--------取皮肤_列表框Ex--------*/
	BindataEx ListboxEx::GetItemSkin(AutoInt index)
	{
		return GetItemSkin_ListboxEx(control, index);
	}
	/*--------置选中_列表框Ex--------*/
	void ListboxEx::SetItemSelected(AutoInt index, BOOL Selected)
	{
		SetItemSelected_ListboxEx(control, index, Selected);
	}
	/*--------取选中_列表框Ex--------*/
	BOOL ListboxEx::GetItemSelected(AutoInt index)
	{
		return GetItemSelected_ListboxEx(control, index);
	}
	/*--------置滚动回调_列表框Ex--------*/
	void ListboxEx::SetScrollCallBack(ExuiCallback Callback)
	{
		SetScrollCallBack_ListboxEx(control, Callback);
	}
	/*--------取滚动回调_列表框Ex--------*/
	ExuiCallback ListboxEx::GetScrollCallBack()
	{
		return GetScrollCallBack_ListboxEx(control);
	}
	/*--------置虚表回调_列表框Ex--------*/
	void ListboxEx::SetVirtualTableCallBack(ExuiCallback Callback)
	{
		SetVirtualTableCallBack_ListboxEx(control, Callback);
	}
	/*--------取虚表回调_列表框Ex--------*/
	ExuiCallback ListboxEx::GetVirtualTableCallBack()
	{
		return GetVirtualTableCallBack_ListboxEx(control);
	}
	/*--------保证显示_列表框Ex--------*/
	void ListboxEx::GuaranteeVisible(AutoInt index, int mode)
	{
		GuaranteeVisible_ListboxEx(control, index, mode);
	}

	/*----------组件列表框EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx ListboxEx::Skin()
	{
		return (BinEx_P)GetAttribute_ListboxEx(control, ListboxEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void ListboxEx::Skin(BindataEx Skin)
	{
		SetAttribute_ListboxEx(control, ListboxEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取现行选中项--------*/
	int ListboxEx::CurrentItem()
	{
		return (int)GetAttribute_ListboxEx(control, ListboxEx_Attr_CurrentItem);
	}
	/*--------置现行选中项--------*/
	void ListboxEx::CurrentItem(int CurrentItem)
	{
		SetAttribute_ListboxEx(control, ListboxEx_Attr_CurrentItem, (AutoInt)CurrentItem);
	}
	/*--------取图标宽--------*/
	int ListboxEx::IconWidth()
	{
		return (int)GetAttribute_ListboxEx(control, ListboxEx_Attr_IconWidth);
	}
	/*--------置图标宽--------*/
	void ListboxEx::IconWidth(int IconWidth)
	{
		SetAttribute_ListboxEx(control, ListboxEx_Attr_IconWidth, (AutoInt)IconWidth);
	}
	/*--------取图标高--------*/
	int ListboxEx::IconHeight()
	{
		return (int)GetAttribute_ListboxEx(control, ListboxEx_Attr_IconHeight);
	}
	/*--------置图标高--------*/
	void ListboxEx::IconHeight(int IconHeight)
	{
		SetAttribute_ListboxEx(control, ListboxEx_Attr_IconHeight, (AutoInt)IconHeight);
	}
	/*--------取字体--------*/
	BindataEx ListboxEx::Font()
	{
		return (BinEx_P)GetAttribute_ListboxEx(control, ListboxEx_Attr_Font);
	}
	/*--------置字体--------*/
	void ListboxEx::Font(BindataEx Font)
	{
		SetAttribute_ListboxEx(control, ListboxEx_Attr_Font, (AutoInt)(BinEx_P)Font);
	}
	/*--------取隔行换色--------*/
	BOOL ListboxEx::AlternateColor()
	{
		return (BOOL)GetAttribute_ListboxEx(control, ListboxEx_Attr_AlternateColor);
	}
	/*--------置隔行换色--------*/
	void ListboxEx::AlternateColor(BOOL AlternateColor)
	{
		SetAttribute_ListboxEx(control, ListboxEx_Attr_AlternateColor, (AutoInt)AlternateColor);
	}
	/*--------取选中模式--------*/
	int ListboxEx::SelectMode()
	{
		return (int)GetAttribute_ListboxEx(control, ListboxEx_Attr_SelectMode);
	}
	/*--------置选中模式--------*/
	void ListboxEx::SelectMode(int SelectMode)
	{
		SetAttribute_ListboxEx(control, ListboxEx_Attr_SelectMode, (AutoInt)SelectMode);
	}
	/*--------取滚动条方式--------*/
	int ListboxEx::ScrollBarMode()
	{
		return (int)GetAttribute_ListboxEx(control, ListboxEx_Attr_ScrollBarMode);
	}
	/*--------置滚动条方式--------*/
	void ListboxEx::ScrollBarMode(int ScrollBarMode)
	{
		SetAttribute_ListboxEx(control, ListboxEx_Attr_ScrollBarMode, (AutoInt)ScrollBarMode);
	}
	/*--------取项目数据--------*/
	BindataEx ListboxEx::ItemData()
	{
		return (BinEx_P)GetAttribute_ListboxEx(control, ListboxEx_Attr_ItemData);
	}
	/*--------置项目数据--------*/
	void ListboxEx::ItemData(BindataEx ItemData)
	{
		SetAttribute_ListboxEx(control, ListboxEx_Attr_ItemData, (AutoInt)(BinEx_P)ItemData);
	}
	/*--------取扩展元素--------*/
	BindataEx ListboxEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_ListboxEx(control, ListboxEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void ListboxEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_ListboxEx(control, ListboxEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx ListboxEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_ListboxEx(control, ListboxEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void ListboxEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_ListboxEx(control, ListboxEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件列表框EX属性读写函数结束--------*/


}
//SuperListboxEx
namespace exui
{
	SuperListboxEx::SuperListboxEx()
	{

	}
	SuperListboxEx::~SuperListboxEx()
	{

	}
	/*--------创建组件_超级列表框EX--------*/
	SuperListboxEx_P SuperListboxEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, AutoInt CurrentItem, AutoInt CurrentColumn, int HeadHeight, int HeadMode, BOOL AlternateColor, int SelectMode, BOOL EntireLine, int EventMode, int EditMode, int LineMode, int ScrollBarMode, BindataEx ItemData, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_SuperListboxEx);
		CreateControl_SuperListboxEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, CurrentItem, CurrentColumn, HeadHeight, HeadMode, AlternateColor, SelectMode, EntireLine, EventMode, EditMode, LineMode, ScrollBarMode, ItemData, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_超级列表框Ex--------*/
	void SuperListboxEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_SuperListboxEx(control, index, attribute);
	}
	/*--------取属性_超级列表框Ex--------*/
	AutoInt SuperListboxEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_SuperListboxEx(control, index);
	}
	/*--------插入列_超级列表框Ex--------*/
	AutoInt SuperListboxEx::InsertColumn(AutoInt cloid, AutoInt addnum, BitmapEx Ico, StringEx Title, int FontColor, int wight, int min, int max, BindataEx Font, int IcoW, int IcoH, int Align, BindataEx ItemFont, int ItemIcoW, int ItemIcoH, int ItemAlign)
	{
		return InsertColumn_SuperListboxEx(control, cloid, addnum, Ico, Title, FontColor, wight, min, max, Font, IcoW, IcoH, Align, ItemFont, ItemIcoW, ItemIcoH, ItemAlign);
	}
	/*--------删除列_超级列表框Ex--------*/
	void SuperListboxEx::DeleteColumn(AutoInt delindex, AutoInt deletenum)
	{
		DeleteColumn_SuperListboxEx(control, delindex, deletenum);
	}
	/*--------取列数量_超级列表框Ex--------*/
	AutoInt SuperListboxEx::GetColumnCount()
	{
		return GetColumnCount_SuperListboxEx(control);
	}
	/*--------置列属性_超级列表框Ex--------*/
	void SuperListboxEx::SetColumnAttribute(AutoInt index, AutoInt AttributeId, BitmapEx Ico, StringEx Title, int FontColor, int wight, int min, int max, BindataEx Font, int IcoW, int IcoH, int Align, BindataEx ItemFont, int ItemIcoW, int ItemIcoH, int ItemAlign)
	{
		SetColumnAttribute_SuperListboxEx(control, index, AttributeId, Ico, Title, FontColor, wight, min, max, Font, IcoW, IcoH, Align, ItemFont, ItemIcoW, ItemIcoH, ItemAlign);
	}
	/*--------取列属性_超级列表框Ex--------*/
	AutoInt SuperListboxEx::GetColumnAttribute(AutoInt index, AutoInt AttributeId)
	{
		return GetColumnAttribute_SuperListboxEx(control, index, AttributeId);
	}
	/*--------插入项目_超级列表框Ex--------*/
	AutoInt SuperListboxEx::InsertItem(AutoInt index, AutoInt addnum, AutoInt Data, int Size)
	{
		return InsertItem_SuperListboxEx(control, index, addnum, Data, Size);
	}
	/*--------删除项目_超级列表框Ex--------*/
	void SuperListboxEx::DeleteItem(AutoInt delindex, AutoInt deletenum)
	{
		DeleteItem_SuperListboxEx(control, delindex, deletenum);
	}
	/*--------取数量_超级列表框Ex--------*/
	AutoInt SuperListboxEx::GetItemCount()
	{
		return GetItemCount_SuperListboxEx(control);
	}
	/*--------置附加值_超级列表框Ex--------*/
	void SuperListboxEx::SetItemData(AutoInt index, AutoInt Data)
	{
		SetItemData_SuperListboxEx(control, index, Data);
	}
	/*--------取附加值_超级列表框Ex--------*/
	AutoInt SuperListboxEx::GetItemData(AutoInt index)
	{
		return GetItemData_SuperListboxEx(control, index);
	}
	/*--------置图标_超级列表框Ex--------*/
	void SuperListboxEx::SetItemIco(AutoInt index, AutoInt cloid, BitmapEx Ico)
	{
		SetItemIco_SuperListboxEx(control, index, cloid, Ico);
	}
	/*--------取图标_超级列表框Ex--------*/
	BitmapEx SuperListboxEx::GetItemIco(AutoInt index, AutoInt cloid)
	{
		return GetItemIco_SuperListboxEx(control, index, cloid);
	}
	/*--------置文本_超级列表框Ex--------*/
	void SuperListboxEx::SetItemTitle(AutoInt index, AutoInt cloid, StringEx Title)
	{
		SetItemTitle_SuperListboxEx(control, index, cloid, Title);
	}
	/*--------取文本_超级列表框Ex--------*/
	StringEx SuperListboxEx::GetItemTitle(AutoInt index, AutoInt cloid)
	{
		return GetItemTitle_SuperListboxEx(control, index, cloid);
	}
	/*--------置字体色_超级列表框Ex--------*/
	void SuperListboxEx::SetItemFontColor(AutoInt index, AutoInt cloid, int FontColor)
	{
		SetItemFontColor_SuperListboxEx(control, index, cloid, FontColor);
	}
	/*--------取字体色_超级列表框Ex--------*/
	int SuperListboxEx::GetItemFontColor(AutoInt index, AutoInt cloid)
	{
		return GetItemFontColor_SuperListboxEx(control, index, cloid);
	}
	/*--------置项目高度_超级列表框Ex--------*/
	void SuperListboxEx::SetItemSize(AutoInt index, int Size)
	{
		SetItemSize_SuperListboxEx(control, index, Size);
	}
	/*--------取项目高度_超级列表框Ex--------*/
	int SuperListboxEx::GetItemSize(AutoInt index)
	{
		return GetItemSize_SuperListboxEx(control, index);
	}
	/*--------置字体_超级列表框Ex--------*/
	void SuperListboxEx::SetItemFont(AutoInt index, AutoInt cloid, BindataEx Font)
	{
		SetItemFont_SuperListboxEx(control, index, cloid, Font);
	}
	/*--------取字体_超级列表框Ex--------*/
	BindataEx SuperListboxEx::GetItemFont(AutoInt index, AutoInt cloid)
	{
		return GetItemFont_SuperListboxEx(control, index, cloid);
	}
	/*--------置皮肤_超级列表框Ex--------*/
	void SuperListboxEx::SetItemSkin(AutoInt index, AutoInt cloid, BindataEx Skin)
	{
		SetItemSkin_SuperListboxEx(control, index, cloid, Skin);
	}
	/*--------取皮肤_超级列表框Ex--------*/
	BindataEx SuperListboxEx::GetItemSkin(AutoInt index, AutoInt cloid)
	{
		return GetItemSkin_SuperListboxEx(control, index, cloid);
	}
	/*--------置选中_超级列表框Ex--------*/
	void SuperListboxEx::SetItemSelected(AutoInt index, AutoInt cloid, BOOL Selected)
	{
		SetItemSelected_SuperListboxEx(control, index, cloid, Selected);
	}
	/*--------取选中_超级列表框Ex--------*/
	BOOL SuperListboxEx::GetItemSelected(AutoInt index, AutoInt cloid)
	{
		return GetItemSelected_SuperListboxEx(control, index, cloid);
	}
	/*--------置项目编辑配置_超级列表框Ex--------*/
	void SuperListboxEx::SetItemEditConfig(int AttributeId, int InputMode, int MaxInput, StringEx PasswordSubstitution, int Align, int Multiline, int Wrapped, BindataEx Skin, BindataEx Cursor, BindataEx Font, int FontClour, int SelectedFontColor, int SelectedColor, int CursorColor, int ScrollBarMode, int MenuTableWidth, int MenuTableHeight, BindataEx MenuFont, int MenuFontClour, int MenuDisabledFontClour, int MenuTransparency, int MenuLanguage)
	{
		SetItemEditConfig_SuperListboxEx(control, AttributeId, InputMode, MaxInput, PasswordSubstitution, Align, Multiline, Wrapped, Skin, Cursor, Font, FontClour, SelectedFontColor, SelectedColor, CursorColor, ScrollBarMode, MenuTableWidth, MenuTableHeight, MenuFont, MenuFontClour, MenuDisabledFontClour, MenuTransparency, MenuLanguage);
	}
	/*--------取项目编辑配置_超级列表框Ex--------*/
	AutoInt SuperListboxEx::GetItemEditConfig(int AttributeId)
	{
		return GetItemEditConfig_SuperListboxEx(control, AttributeId);
	}
	/*--------进入编辑_超级列表框Ex--------*/
	void SuperListboxEx::EnterEdit(AutoInt index, AutoInt cloid, AutoInt mode)
	{
		EnterEdit_SuperListboxEx(control, index, cloid, mode);
	}
	/*--------退出编辑_超级列表框Ex--------*/
	void SuperListboxEx::ExitEdit(AutoInt index, AutoInt cloid, AutoInt mode)
	{
		ExitEdit_SuperListboxEx(control, index, cloid, mode);
	}
	/*--------置滚动回调_超级列表框Ex--------*/
	void SuperListboxEx::SetScrollCallBack(ExuiCallback Callback)
	{
		SetScrollCallBack_SuperListboxEx(control, Callback);
	}
	/*--------取滚动回调_超级列表框Ex--------*/
	ExuiCallback SuperListboxEx::GetScrollCallBack()
	{
		return GetScrollCallBack_SuperListboxEx(control);
	}
	/*--------置虚表回调_超级列表框Ex--------*/
	void SuperListboxEx::SetVirtualTableCallBack(ExuiCallback Callback)
	{
		SetVirtualTableCallBack_SuperListboxEx(control, Callback);
	}
	/*--------取虚表回调_超级列表框Ex--------*/
	ExuiCallback SuperListboxEx::GetVirtualTableCallBack()
	{
		return GetVirtualTableCallBack_SuperListboxEx(control);
	}
	/*--------保证显示_超级列表框Ex--------*/
	void SuperListboxEx::GuaranteeVisible(AutoInt index, int cid, int mode)
	{
		GuaranteeVisible_SuperListboxEx(control, index, cid, mode);
	}

	/*----------组件超级列表框EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx SuperListboxEx::Skin()
	{
		return (BinEx_P)GetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void SuperListboxEx::Skin(BindataEx Skin)
	{
		SetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取现行选中项--------*/
	int SuperListboxEx::CurrentItem()
	{
		return (int)GetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_CurrentItem);
	}
	/*--------置现行选中项--------*/
	void SuperListboxEx::CurrentItem(int CurrentItem)
	{
		SetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_CurrentItem, (AutoInt)CurrentItem);
	}
	/*--------取现行选中列--------*/
	int SuperListboxEx::CurrentColumn()
	{
		return (int)GetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_CurrentColumn);
	}
	/*--------置现行选中列--------*/
	void SuperListboxEx::CurrentColumn(int CurrentColumn)
	{
		SetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_CurrentColumn, (AutoInt)CurrentColumn);
	}
	/*--------取表头高度--------*/
	int SuperListboxEx::HeadHeight()
	{
		return (int)GetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_HeadHeight);
	}
	/*--------置表头高度--------*/
	void SuperListboxEx::HeadHeight(int HeadHeight)
	{
		SetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_HeadHeight, (AutoInt)HeadHeight);
	}
	/*--------取表头模式--------*/
	int SuperListboxEx::HeadMode()
	{
		return (int)GetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_HeadMode);
	}
	/*--------置表头模式--------*/
	void SuperListboxEx::HeadMode(int HeadMode)
	{
		SetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_HeadMode, (AutoInt)HeadMode);
	}
	/*--------取隔行换色--------*/
	BOOL SuperListboxEx::AlternateColor()
	{
		return (BOOL)GetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_AlternateColor);
	}
	/*--------置隔行换色--------*/
	void SuperListboxEx::AlternateColor(BOOL AlternateColor)
	{
		SetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_AlternateColor, (AutoInt)AlternateColor);
	}
	/*--------取选中模式--------*/
	int SuperListboxEx::SelectMode()
	{
		return (int)GetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_SelectMode);
	}
	/*--------置选中模式--------*/
	void SuperListboxEx::SelectMode(int SelectMode)
	{
		SetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_SelectMode, (AutoInt)SelectMode);
	}
	/*--------取整行选择--------*/
	BOOL SuperListboxEx::EntireLine()
	{
		return (BOOL)GetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_EntireLine);
	}
	/*--------置整行选择--------*/
	void SuperListboxEx::EntireLine(BOOL EntireLine)
	{
		SetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_EntireLine, (AutoInt)EntireLine);
	}
	/*--------取事件模式--------*/
	int SuperListboxEx::EventMode()
	{
		return (int)GetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_EventMode);
	}
	/*--------置事件模式--------*/
	void SuperListboxEx::EventMode(int EventMode)
	{
		SetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_EventMode, (AutoInt)EventMode);
	}
	/*--------取编辑模式--------*/
	int SuperListboxEx::EditMode()
	{
		return (int)GetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_EditMode);
	}
	/*--------置编辑模式--------*/
	void SuperListboxEx::EditMode(int EditMode)
	{
		SetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_EditMode, (AutoInt)EditMode);
	}
	/*--------取表格线模式--------*/
	int SuperListboxEx::LineMode()
	{
		return (int)GetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_LineMode);
	}
	/*--------置表格线模式--------*/
	void SuperListboxEx::LineMode(int LineMode)
	{
		SetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_LineMode, (AutoInt)LineMode);
	}
	/*--------取滚动条方式--------*/
	int SuperListboxEx::ScrollBarMode()
	{
		return (int)GetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_ScrollBarMode);
	}
	/*--------置滚动条方式--------*/
	void SuperListboxEx::ScrollBarMode(int ScrollBarMode)
	{
		SetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_ScrollBarMode, (AutoInt)ScrollBarMode);
	}
	/*--------取项目数据--------*/
	BindataEx SuperListboxEx::ItemData()
	{
		return (BinEx_P)GetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_ItemData);
	}
	/*--------置项目数据--------*/
	void SuperListboxEx::ItemData(BindataEx ItemData)
	{
		SetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_ItemData, (AutoInt)(BinEx_P)ItemData);
	}
	/*--------取扩展元素--------*/
	BindataEx SuperListboxEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void SuperListboxEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx SuperListboxEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void SuperListboxEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_SuperListboxEx(control, SuperListboxEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件超级列表框EX属性读写函数结束--------*/

}
//IcoListboxEx
namespace exui
{
	IcoListboxEx::IcoListboxEx()
	{

	}
	IcoListboxEx::~IcoListboxEx()
	{

	}
	/*--------创建组件_图标列表框EX--------*/
	IcoListboxEx_P IcoListboxEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, AutoInt CurrentItem, int TableWidth, int TableHeight, int HSpacing, int LSpacing, int IconWidth, int IconHeight, BindataEx Font, int PageLayout, int Align, int SelectMode, int ScrollBarMode, BindataEx ItemData, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_IcoListboxEx);
		CreateControl_IcoListboxEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, CurrentItem, TableWidth, TableHeight, HSpacing, LSpacing, IconWidth, IconHeight, Font, PageLayout, Align, SelectMode, ScrollBarMode, ItemData, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_图标列表框Ex--------*/
	void IcoListboxEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_IcoListboxEx(control, index, attribute);
	}
	/*--------取属性_图标列表框Ex--------*/
	AutoInt IcoListboxEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_IcoListboxEx(control, index);
	}
	/*--------插入项目_图标列表框Ex--------*/
	AutoInt IcoListboxEx::InsertItem(AutoInt index, AutoInt addnum, AutoInt Data, BitmapEx Ico, StringEx Title, int FontColor)
	{
		return InsertItem_IcoListboxEx(control, index, addnum, Data, Ico, Title, FontColor);
	}
	/*--------删除项目_图标列表框Ex--------*/
	void IcoListboxEx::DeleteItem(AutoInt delindex, AutoInt deletenum)
	{
		DeleteItem_IcoListboxEx(control, delindex, deletenum);
	}
	/*--------取项目数量_图标列表框Ex--------*/
	AutoInt IcoListboxEx::GetItemCount()
	{
		return GetItemCount_IcoListboxEx(control);
	}
	/*--------置附加值_图标列表框Ex--------*/
	void IcoListboxEx::SetItemData(AutoInt index, AutoInt Data)
	{
		SetItemData_IcoListboxEx(control, index, Data);
	}
	/*--------取附加值_图标列表框Ex--------*/
	AutoInt IcoListboxEx::GetItemData(AutoInt index)
	{
		return GetItemData_IcoListboxEx(control, index);
	}
	/*--------置图标_图标列表框Ex--------*/
	void IcoListboxEx::SetItemIco(AutoInt index, BitmapEx Ico)
	{
		SetItemIco_IcoListboxEx(control, index, Ico);
	}
	/*--------取图标_图标列表框Ex--------*/
	BitmapEx IcoListboxEx::GetItemIco(AutoInt index)
	{
		return GetItemIco_IcoListboxEx(control, index);
	}
	/*--------置文本_图标列表框Ex--------*/
	void IcoListboxEx::SetItemTitle(AutoInt index, StringEx Title)
	{
		SetItemTitle_IcoListboxEx(control, index, Title);
	}
	/*--------取文本_图标列表框Ex--------*/
	StringEx IcoListboxEx::GetItemTitle(AutoInt index)
	{
		return GetItemTitle_IcoListboxEx(control, index);
	}
	/*--------置字体色_图标列表框Ex--------*/
	void IcoListboxEx::SetItemFontColor(AutoInt index, int FontColor)
	{
		SetItemFontColor_IcoListboxEx(control, index, FontColor);
	}
	/*--------取字体色_图标列表框Ex--------*/
	int IcoListboxEx::GetItemFontColor(AutoInt index)
	{
		return GetItemFontColor_IcoListboxEx(control, index);
	}
	/*--------置字体_图标列表框Ex--------*/
	void IcoListboxEx::SetItemFont(AutoInt index, BindataEx Font)
	{
		SetItemFont_IcoListboxEx(control, index, Font);
	}
	/*--------取字体_图标列表框Ex--------*/
	BindataEx IcoListboxEx::GetItemFont(AutoInt index)
	{
		return GetItemFont_IcoListboxEx(control, index);
	}
	/*--------置皮肤_图标列表框Ex--------*/
	void IcoListboxEx::SetItemSkin(AutoInt index, BindataEx Skin)
	{
		SetItemSkin_IcoListboxEx(control, index, Skin);
	}
	/*--------取皮肤_图标列表框Ex--------*/
	BindataEx IcoListboxEx::GetItemSkin(AutoInt index)
	{
		return GetItemSkin_IcoListboxEx(control, index);
	}
	/*--------置选中_图标列表框Ex--------*/
	void IcoListboxEx::SetItemSelected(AutoInt index, BOOL Selected)
	{
		SetItemSelected_IcoListboxEx(control, index, Selected);
	}
	/*--------取选中_图标列表框Ex--------*/
	BOOL IcoListboxEx::GetItemSelected(AutoInt index)
	{
		return GetItemSelected_IcoListboxEx(control, index);
	}
	/*--------置布局配置_图标列表框Ex--------*/
	void IcoListboxEx::SetItemLayoutConfig(AutoInt index, int ConfigId, int Reserve1, int Reserve2, int Reserve3, int Width, int Height, int Align, int IconWidth, int IconHeight)
	{
		SetItemLayoutConfig_IcoListboxEx(control, index,  ConfigId,  Reserve1,  Reserve2,  Reserve3,  Width,  Height,  Align,  IconWidth,  IconHeight);
	}
	/*--------取布局配置_图标列表框Ex--------*/
	int IcoListboxEx::GetItemLayoutConfig(AutoInt index, int ConfigId)
	{
		return GetItemLayoutConfig_IcoListboxEx(control, index,  ConfigId);
	}
	/*--------置滚动回调_图标列表框Ex--------*/
	void IcoListboxEx::SetScrollCallBack(ExuiCallback Callback)
	{
		SetScrollCallBack_IcoListboxEx(control, Callback);
	}
	/*--------取滚动回调_图标列表框Ex--------*/
	ExuiCallback IcoListboxEx::GetScrollCallBack()
	{
		return GetScrollCallBack_IcoListboxEx(control);
	}
	/*--------置虚表回调_图标列表框Ex--------*/
	void IcoListboxEx::SetVirtualTableCallBack(ExuiCallback Callback)
	{
		SetVirtualTableCallBack_IcoListboxEx(control, Callback);
	}
	/*--------取虚表回调_图标列表框Ex--------*/
	ExuiCallback IcoListboxEx::GetVirtualTableCallBack()
	{
		return GetVirtualTableCallBack_IcoListboxEx(control);
	}
	/*--------保证显示_图标列表框Ex--------*/
	void IcoListboxEx::GuaranteeVisible(AutoInt index, int mode)
	{
		GuaranteeVisible_IcoListboxEx(control, index, mode);
	}

	/*----------组件图标列表框EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx IcoListboxEx::Skin()
	{
		return (BinEx_P)GetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void IcoListboxEx::Skin(BindataEx Skin)
	{
		SetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取现行选中项--------*/
	int IcoListboxEx::CurrentItem()
	{
		return (int)GetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_CurrentItem);
	}
	/*--------置现行选中项--------*/
	void IcoListboxEx::CurrentItem(int CurrentItem)
	{
		SetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_CurrentItem, (AutoInt)CurrentItem);
	}
	/*--------取表项宽度--------*/
	int IcoListboxEx::TableWidth()
	{
		return (int)GetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_TableWidth);
	}
	/*--------置表项宽度--------*/
	void IcoListboxEx::TableWidth(int TableWidth)
	{
		SetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_TableWidth, (AutoInt)TableWidth);
	}
	/*--------取表项高度--------*/
	int IcoListboxEx::TableHeight()
	{
		return (int)GetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_TableHeight);
	}
	/*--------置表项高度--------*/
	void IcoListboxEx::TableHeight(int TableHeight)
	{
		SetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_TableHeight, (AutoInt)TableHeight);
	}
	/*--------取横间距--------*/
	int IcoListboxEx::HSpacing()
	{
		return (int)GetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_HSpacing);
	}
	/*--------置横间距--------*/
	void IcoListboxEx::HSpacing(int HSpacing)
	{
		SetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_HSpacing, (AutoInt)HSpacing);
	}
	/*--------取纵间距--------*/
	int IcoListboxEx::LSpacing()
	{
		return (int)GetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_LSpacing);
	}
	/*--------置纵间距--------*/
	void IcoListboxEx::LSpacing(int LSpacing)
	{
		SetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_LSpacing, (AutoInt)LSpacing);
	}
	/*--------取图标宽--------*/
	int IcoListboxEx::IconWidth()
	{
		return (int)GetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_IconWidth);
	}
	/*--------置图标宽--------*/
	void IcoListboxEx::IconWidth(int IconWidth)
	{
		SetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_IconWidth, (AutoInt)IconWidth);
	}
	/*--------取图标高--------*/
	int IcoListboxEx::IconHeight()
	{
		return (int)GetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_IconHeight);
	}
	/*--------置图标高--------*/
	void IcoListboxEx::IconHeight(int IconHeight)
	{
		SetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_IconHeight, (AutoInt)IconHeight);
	}
	/*--------取字体--------*/
	BindataEx IcoListboxEx::Font()
	{
		return (BinEx_P)GetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_Font);
	}
	/*--------置字体--------*/
	void IcoListboxEx::Font(BindataEx Font)
	{
		SetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_Font, (AutoInt)(BinEx_P)Font);
	}
	/*--------取布局模式--------*/
	int IcoListboxEx::PageLayout()
	{
		return (int)GetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_PageLayout);
	}
	/*--------置布局模式--------*/
	void IcoListboxEx::PageLayout(int PageLayout)
	{
		SetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_PageLayout, (AutoInt)PageLayout);
	}
	/*--------取对齐方式--------*/
	int IcoListboxEx::Align()
	{
		return (int)GetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_Align);
	}
	/*--------置对齐方式--------*/
	void IcoListboxEx::Align(int Align)
	{
		SetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_Align, (AutoInt)Align);
	}
	/*--------取选中模式--------*/
	int IcoListboxEx::SelectMode()
	{
		return (int)GetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_SelectMode);
	}
	/*--------置选中模式--------*/
	void IcoListboxEx::SelectMode(int SelectMode)
	{
		SetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_SelectMode, (AutoInt)SelectMode);
	}
	/*--------取滚动条方式--------*/
	int IcoListboxEx::ScrollBarMode()
	{
		return (int)GetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_ScrollBarMode);
	}
	/*--------置滚动条方式--------*/
	void IcoListboxEx::ScrollBarMode(int ScrollBarMode)
	{
		SetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_ScrollBarMode, (AutoInt)ScrollBarMode);
	}
	/*--------取项目数据--------*/
	BindataEx IcoListboxEx::ItemData()
	{
		return (BinEx_P)GetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_ItemData);
	}
	/*--------置项目数据--------*/
	void IcoListboxEx::ItemData(BindataEx ItemData)
	{
		SetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_ItemData, (AutoInt)(BinEx_P)ItemData);
	}
	/*--------取扩展元素--------*/
	BindataEx IcoListboxEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void IcoListboxEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx IcoListboxEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void IcoListboxEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_IcoListboxEx(control, IcoListboxEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件图标列表框EX属性读写函数结束--------*/

}
//TreeListEx
namespace exui
{
	TreeListEx::TreeListEx()
	{

	}
	TreeListEx::~TreeListEx()
	{

	}
	/*--------创建组件_树形列表框EX--------*/
	TreeListEx_P TreeListEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, AutoInt CurrentItem, BOOL AutoSize, int FoldBtnWidth, int FoldBtnHeight, int IconWidth, int IconHeight, BindataEx Font, int ChildMigration, int BackMigration, int ContentMigration, int SelectMode, int LineMode, int ScrollBarMode, BindataEx ItemData, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_TreeListEx);
		CreateControl_TreeListEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, CurrentItem, AutoSize, FoldBtnWidth, FoldBtnHeight, IconWidth, IconHeight, Font, ChildMigration, BackMigration, ContentMigration, SelectMode, LineMode, ScrollBarMode, ItemData, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_树形框Ex--------*/
	void TreeListEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_TreeListEx(control, index, attribute);
	}
	/*--------取属性_树形框Ex--------*/
	AutoInt TreeListEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_TreeListEx(control, index);
	}
	/*--------插入_树形框Ex--------*/
	AutoInt TreeListEx::InsertItem(AutoInt index, AutoInt addnum, AutoInt Data, BitmapEx Ico, StringEx Title, int FontColor, int Width, int Size, int ItemType, BOOL SetFold, int InsMode)
	{
		return InsertItem_TreeListEx(control, index, addnum, Data, Ico, Title, FontColor, Width, Size, ItemType, SetFold, InsMode);
	}
	/*--------删除_树形框Ex--------*/
	void TreeListEx::DeleteItem(AutoInt delindex, AutoInt deletenum)
	{
		DeleteItem_TreeListEx(control, delindex, deletenum);
	}
	/*--------取数量_树形框Ex--------*/
	AutoInt TreeListEx::GetItemCount()
	{
		return GetItemCount_TreeListEx(control);
	}
	/*--------置附加值_树形框Ex--------*/
	void TreeListEx::SetItemData(AutoInt index, AutoInt Data)
	{
		SetItemData_TreeListEx(control, index, Data);
	}
	/*--------取附加值_树形框Ex--------*/
	AutoInt TreeListEx::GetItemData(AutoInt index)
	{
		return GetItemData_TreeListEx(control, index);
	}
	/*--------置图标_树形框Ex--------*/
	void TreeListEx::SetItemIco(AutoInt index, BitmapEx Ico)
	{
		SetItemIco_TreeListEx(control, index, Ico);
	}
	/*--------取图标_树形框Ex--------*/
	BitmapEx TreeListEx::GetItemIco(AutoInt index)
	{
		return GetItemIco_TreeListEx(control, index);
	}
	/*--------置文本_树形框Ex--------*/
	void TreeListEx::SetItemTitle(AutoInt index, StringEx Title)
	{
		SetItemTitle_TreeListEx(control, index, Title);
	}
	/*--------取文本_树形框Ex--------*/
	StringEx TreeListEx::GetItemTitle(AutoInt index)
	{
		return GetItemTitle_TreeListEx(control, index);
	}
	/*--------置字体色_树形框Ex--------*/
	void TreeListEx::SetItemFontColor(AutoInt index, int FontColor)
	{
		SetItemFontColor_TreeListEx(control, index, FontColor);
	}
	/*--------取字体色_树形框Ex--------*/
	int TreeListEx::GetItemFontColor(AutoInt index)
	{
		return GetItemFontColor_TreeListEx(control, index);
	}
	/*--------置项目宽度_树形框Ex--------*/
	void TreeListEx::SetItemWidth(AutoInt index, int Width)
	{
		SetItemWidth_TreeListEx(control, index, Width);
	}
	/*--------取项目宽度_树形框Ex--------*/
	int TreeListEx::GetItemWidth(AutoInt index)
	{
		return GetItemWidth_TreeListEx(control, index);
	}
	/*--------置项目高度_树形框Ex--------*/
	void TreeListEx::SetItemSize(AutoInt index, int Size)
	{
		SetItemSize_TreeListEx(control, index, Size);
	}
	/*--------取项目高度_树形框Ex--------*/
	int TreeListEx::GetItemSize(AutoInt index)
	{
		return GetItemSize_TreeListEx(control, index);
	}
	/*--------置项目风格_树形框Ex--------*/
	void TreeListEx::SetItemType(AutoInt index, int style)
	{
		SetItemType_TreeListEx(control, index, style);
	}
	/*--------取项目风格_树形框Ex--------*/
	int TreeListEx::GetItemType(AutoInt index)
	{
		return GetItemType_TreeListEx(control, index);
	}
	/*--------置字体_树形框Ex--------*/
	void TreeListEx::SetItemFont(AutoInt index, BindataEx Font)
	{
		SetItemFont_TreeListEx(control, index, Font);
	}
	/*--------取字体_树形框Ex--------*/
	BindataEx TreeListEx::GetItemFont(AutoInt index)
	{
		return GetItemFont_TreeListEx(control, index);
	}
	/*--------置皮肤_树形框Ex--------*/
	void TreeListEx::SetItemSkin(AutoInt index, BindataEx Skin)
	{
		SetItemSkin_TreeListEx(control, index, Skin);
	}
	/*--------取皮肤_树形框Ex--------*/
	BindataEx TreeListEx::GetItemSkin(AutoInt index)
	{
		return GetItemSkin_TreeListEx(control, index);
	}
	/*--------置选中_树形框Ex--------*/
	void TreeListEx::SetItemSelected(AutoInt index, BOOL Selected)
	{
		SetItemSelected_TreeListEx(control, index, Selected);
	}
	/*--------取选中_树形框Ex--------*/
	BOOL TreeListEx::GetItemSelected(AutoInt index)
	{
		return GetItemSelected_TreeListEx(control, index);
	}
	/*--------置折叠状态_树形框Ex--------*/
	void TreeListEx::SetItemFold(AutoInt index, BOOL Fold)
	{
		SetItemFold_TreeListEx(control, index, Fold);
	}
	/*--------取折叠状态_树形框Ex--------*/
	BOOL TreeListEx::GetItemFold(AutoInt index)
	{
		return GetItemFold_TreeListEx(control, index);
	}
	/*--------取缩进层次_树形框Ex--------*/
	int TreeListEx::GetItemLevel(AutoInt index)
	{
		return GetItemLevel_TreeListEx(control, index);
	}
	/*--------取子项目数_树形框Ex--------*/
	AutoInt TreeListEx::GetSubItemCount(AutoInt index)
	{
		return GetSubItemCount_TreeListEx(control, index);
	}
	/*--------取下一级子项目数_树形框Ex--------*/
	AutoInt TreeListEx::GetNextSubItemCount(AutoInt index)
	{
		return GetNextSubItemCount_TreeListEx(control, index);
	}
	/*--------取祖宗项目_树形框Ex--------*/
	AutoInt TreeListEx::GetAncestorItem(AutoInt index)
	{
		return GetAncestorItem_TreeListEx(control, index);
	}
	/*--------取父项目_树形框Ex--------*/
	AutoInt TreeListEx::GetFatherItem(AutoInt index)
	{
		return GetFatherItem_TreeListEx(control, index);
	}
	/*--------取上一同层项目_树形框Ex--------*/
	AutoInt TreeListEx::GetLastItem(AutoInt index)
	{
		return GetLastItem_TreeListEx(control, index);
	}
	/*--------取下一同层项目_树形框Ex--------*/
	AutoInt TreeListEx::GetNextItem(AutoInt index)
	{
		return GetNextItem_TreeListEx(control, index);
	}
	/*--------置滚动回调_树形框Ex--------*/
	void TreeListEx::SetScrollCallBack(ExuiCallback Callback)
	{
		SetScrollCallBack_TreeListEx(control, Callback);
	}
	/*--------取滚动回调_树形框Ex--------*/
	ExuiCallback TreeListEx::GetScrollCallBack()
	{
		return GetScrollCallBack_TreeListEx(control);
	}
	/*--------置虚表回调_树形框Ex--------*/
	void TreeListEx::SetVirtualTableCallBack(ExuiCallback Callback)
	{
		SetVirtualTableCallBack_TreeListEx(control, Callback);
	}
	/*--------取虚表回调_树形框Ex--------*/
	ExuiCallback TreeListEx::GetVirtualTableCallBack()
	{
		return GetVirtualTableCallBack_TreeListEx(control);
	}
	/*--------保证显示_树形框Ex--------*/
	void TreeListEx::GuaranteeVisible(AutoInt index, int mode)
	{
		GuaranteeVisible_TreeListEx(control, index, mode);
	}

	/*----------组件树形列表框EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx TreeListEx::Skin()
	{
		return (BinEx_P)GetAttribute_TreeListEx(control, TreeListEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void TreeListEx::Skin(BindataEx Skin)
	{
		SetAttribute_TreeListEx(control, TreeListEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取现行选中项--------*/
	int TreeListEx::CurrentItem()
	{
		return (int)GetAttribute_TreeListEx(control, TreeListEx_Attr_CurrentItem);
	}
	/*--------置现行选中项--------*/
	void TreeListEx::CurrentItem(int CurrentItem)
	{
		SetAttribute_TreeListEx(control, TreeListEx_Attr_CurrentItem, (AutoInt)CurrentItem);
	}
	/*--------取项目自动扩展--------*/
	BOOL TreeListEx::AutoSize()
	{
		return (BOOL)GetAttribute_TreeListEx(control, TreeListEx_Attr_AutoSize);
	}
	/*--------置项目自动扩展--------*/
	void TreeListEx::AutoSize(BOOL AutoSize)
	{
		SetAttribute_TreeListEx(control, TreeListEx_Attr_AutoSize, (AutoInt)AutoSize);
	}
	/*--------取折叠钮宽度--------*/
	int TreeListEx::FoldBtnWidth()
	{
		return (int)GetAttribute_TreeListEx(control, TreeListEx_Attr_FoldBtnWidth);
	}
	/*--------置折叠钮宽度--------*/
	void TreeListEx::FoldBtnWidth(int FoldBtnWidth)
	{
		SetAttribute_TreeListEx(control, TreeListEx_Attr_FoldBtnWidth, (AutoInt)FoldBtnWidth);
	}
	/*--------取折叠钮高度--------*/
	int TreeListEx::FoldBtnHeight()
	{
		return (int)GetAttribute_TreeListEx(control, TreeListEx_Attr_FoldBtnHeight);
	}
	/*--------置折叠钮高度--------*/
	void TreeListEx::FoldBtnHeight(int FoldBtnHeight)
	{
		SetAttribute_TreeListEx(control, TreeListEx_Attr_FoldBtnHeight, (AutoInt)FoldBtnHeight);
	}
	/*--------取图标宽--------*/
	int TreeListEx::IconWidth()
	{
		return (int)GetAttribute_TreeListEx(control, TreeListEx_Attr_IconWidth);
	}
	/*--------置图标宽--------*/
	void TreeListEx::IconWidth(int IconWidth)
	{
		SetAttribute_TreeListEx(control, TreeListEx_Attr_IconWidth, (AutoInt)IconWidth);
	}
	/*--------取图标高--------*/
	int TreeListEx::IconHeight()
	{
		return (int)GetAttribute_TreeListEx(control, TreeListEx_Attr_IconHeight);
	}
	/*--------置图标高--------*/
	void TreeListEx::IconHeight(int IconHeight)
	{
		SetAttribute_TreeListEx(control, TreeListEx_Attr_IconHeight, (AutoInt)IconHeight);
	}
	/*--------取字体--------*/
	BindataEx TreeListEx::Font()
	{
		return (BinEx_P)GetAttribute_TreeListEx(control, TreeListEx_Attr_Font);
	}
	/*--------置字体--------*/
	void TreeListEx::Font(BindataEx Font)
	{
		SetAttribute_TreeListEx(control, TreeListEx_Attr_Font, (AutoInt)(BinEx_P)Font);
	}
	/*--------取子级缩进--------*/
	int TreeListEx::ChildMigration()
	{
		return (int)GetAttribute_TreeListEx(control, TreeListEx_Attr_ChildMigration);
	}
	/*--------置子级缩进--------*/
	void TreeListEx::ChildMigration(int ChildMigration)
	{
		SetAttribute_TreeListEx(control, TreeListEx_Attr_ChildMigration, (AutoInt)ChildMigration);
	}
	/*--------取背景缩进--------*/
	int TreeListEx::BackMigration()
	{
		return (int)GetAttribute_TreeListEx(control, TreeListEx_Attr_BackMigration);
	}
	/*--------置背景缩进--------*/
	void TreeListEx::BackMigration(int BackMigration)
	{
		SetAttribute_TreeListEx(control, TreeListEx_Attr_BackMigration, (AutoInt)BackMigration);
	}
	/*--------取内容缩进--------*/
	int TreeListEx::ContentMigration()
	{
		return (int)GetAttribute_TreeListEx(control, TreeListEx_Attr_ContentMigration);
	}
	/*--------置内容缩进--------*/
	void TreeListEx::ContentMigration(int ContentMigration)
	{
		SetAttribute_TreeListEx(control, TreeListEx_Attr_ContentMigration, (AutoInt)ContentMigration);
	}
	/*--------取选中模式--------*/
	int TreeListEx::SelectMode()
	{
		return (int)GetAttribute_TreeListEx(control, TreeListEx_Attr_SelectMode);
	}
	/*--------置选中模式--------*/
	void TreeListEx::SelectMode(int SelectMode)
	{
		SetAttribute_TreeListEx(control, TreeListEx_Attr_SelectMode, (AutoInt)SelectMode);
	}
	/*--------取表格线模式--------*/
	int TreeListEx::LineMode()
	{
		return (int)GetAttribute_TreeListEx(control, TreeListEx_Attr_LineMode);
	}
	/*--------置表格线模式--------*/
	void TreeListEx::LineMode(int LineMode)
	{
		SetAttribute_TreeListEx(control, TreeListEx_Attr_LineMode, (AutoInt)LineMode);
	}
	/*--------取滚动条方式--------*/
	int TreeListEx::ScrollBarMode()
	{
		return (int)GetAttribute_TreeListEx(control, TreeListEx_Attr_ScrollBarMode);
	}
	/*--------置滚动条方式--------*/
	void TreeListEx::ScrollBarMode(int ScrollBarMode)
	{
		SetAttribute_TreeListEx(control, TreeListEx_Attr_ScrollBarMode, (AutoInt)ScrollBarMode);
	}
	/*--------取项目数据--------*/
	BindataEx TreeListEx::ItemData()
	{
		return (BinEx_P)GetAttribute_TreeListEx(control, TreeListEx_Attr_ItemData);
	}
	/*--------置项目数据--------*/
	void TreeListEx::ItemData(BindataEx ItemData)
	{
		SetAttribute_TreeListEx(control, TreeListEx_Attr_ItemData, (AutoInt)(BinEx_P)ItemData);
	}
	/*--------取扩展元素--------*/
	BindataEx TreeListEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_TreeListEx(control, TreeListEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void TreeListEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_TreeListEx(control, TreeListEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx TreeListEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_TreeListEx(control, TreeListEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void TreeListEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_TreeListEx(control, TreeListEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件树形列表框EX属性读写函数结束--------*/

}
//WebBrowserEx
namespace exui
{
	WebBrowserEx::WebBrowserEx()
	{

	}
	WebBrowserEx::~WebBrowserEx()
	{

	}
	/*--------创建组件_浏览框EX--------*/
	WebBrowserEx_P WebBrowserEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BOOL BackgroundTransparent, int MediaVolume, int ZoomFactor, BOOL NavigationToNewWind, BOOL CookieEnabled, BOOL NpapiEnabled, BOOL MenuEnabled, int DragEnable, StringEx StartPage, StringEx UserAgent, int Version, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_WebBrowserEx);
		CreateControl_WebBrowserEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, BackgroundTransparent, MediaVolume, ZoomFactor, NavigationToNewWind, CookieEnabled, NpapiEnabled, MenuEnabled, DragEnable, StartPage, UserAgent, Version, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_浏览框Ex--------*/
	void WebBrowserEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_WebBrowserEx(control, index, attribute);
	}
	/*--------取属性_浏览框Ex--------*/
	AutoInt WebBrowserEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_WebBrowserEx(control, index);
	}
	/*--------取内核句柄_浏览框Ex--------*/
	AutoInt WebBrowserEx::GetWebView()
	{
		return GetWebView_WebBrowserEx(control);
	}
	/*--------载入_浏览框Ex--------*/
	void WebBrowserEx::Load(StringEx url, StringEx baseUrl, int Loadmode)
	{
		Load_WebBrowserEx(control, url, baseUrl, Loadmode);
	}
	/*--------后退_浏览框Ex--------*/
	BOOL WebBrowserEx::GoBack()
	{
		return GoBack_WebBrowserEx(control);
	}
	/*--------前进_浏览框Ex--------*/
	BOOL WebBrowserEx::GoForward()
	{
		return GoForward_WebBrowserEx(control);
	}
	/*--------可否后退_浏览框Ex--------*/
	BOOL WebBrowserEx::GoCanBack()
	{
		return GoCanBack_WebBrowserEx(control);
	}
	/*--------可否前进_浏览框Ex--------*/
	BOOL WebBrowserEx::GoCanForward()
	{
		return GoCanForward_WebBrowserEx(control);
	}
	/*--------重载_浏览框Ex--------*/
	void WebBrowserEx::ReLoad()
	{
		ReLoad_WebBrowserEx(control);
	}
	/*--------停止_浏览框Ex--------*/
	void WebBrowserEx::StopLoading()
	{
		StopLoading_WebBrowserEx(control);
	}
	/*--------取地址_浏览框Ex--------*/
	StringEx WebBrowserEx::GetUrl(StringEx Param1)
	{
		return GetUrl_WebBrowserEx(control, Param1);
	}
	/*--------取标题_浏览框Ex--------*/
	StringEx WebBrowserEx::GetTitle(StringEx Param1)
	{
		return GetTitle_WebBrowserEx(control, Param1);
	}
	/*--------取源码_浏览框Ex--------*/
	StringEx WebBrowserEx::GetSource(StringEx Param1)
	{
		return GetSource_WebBrowserEx(control, Param1);
	}
	/*--------执行命令_浏览框Ex--------*/
	AutoInt WebBrowserEx::RunCmd(AutoInt Cmd, AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4)
	{
		return RunCmd_WebBrowserEx(control, Cmd, Parameter1, Parameter2, Parameter3, Parameter4);
	}
	/*--------取Cookie_浏览框Ex--------*/
	StringEx WebBrowserEx::GetCookie(StringEx Param1)
	{
		return GetCookie_WebBrowserEx(control, Param1);
	}
	/*--------置Cookie_浏览框Ex--------*/
	void WebBrowserEx::SetCookie(StringEx url, StringEx cookie)
	{
		SetCookie_WebBrowserEx(control, url, cookie);
	}
	/*--------执行Js命令_浏览框Ex--------*/
	StringEx WebBrowserEx::RunJsCmd( StringEx Cmd, StringEx  Param1, StringEx  Param2, StringEx  Param3)
	{
		return RunJsCmd_WebBrowserEx(control,  Cmd, Param1, Param2, Param3);
	}
	/*--------绑定Js命令_浏览框Ex--------*/
	void WebBrowserEx::BindJsCmd(StringEx Cmd, void* Function, AutoInt Param1, AutoInt Param2)
	{
		BindJsCmd_WebBrowserEx(control, Cmd, Function, Param1, Param2);
	}
	/*--------否正在加载_浏览框Ex--------*/
	BOOL WebBrowserEx::IsLoading()
	{
		return IsLoading_WebBrowserEx(control);
	}
	/*--------是否载入失败_浏览框Ex--------*/
	BOOL WebBrowserEx::IsLoadingSucceeded()
	{
		return IsLoadingSucceeded_WebBrowserEx(control);
	}
	/*--------是否载入完毕_浏览框Ex--------*/
	BOOL WebBrowserEx::IsLoadingCompleted()
	{
		return IsLoadingCompleted_WebBrowserEx(control);
	}
	/*--------是否加载完成_浏览框Ex--------*/
	BOOL WebBrowserEx::IsDocumentReady()
	{
		return IsDocumentReady_WebBrowserEx(control);
	}
	/*--------启用开发者工具_浏览框Ex--------*/
	void WebBrowserEx::ShowDevtools(StringEx path)
	{
		ShowDevtools_WebBrowserEx(control, path);
	}

	/*----------组件浏览框EX属性读写函数开始--------*/


/*--------取背景透明--------*/
	BOOL WebBrowserEx::BackgroundTransparent()
	{
		return (BOOL)GetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_BackgroundTransparent);
	}
	/*--------置背景透明--------*/
	void WebBrowserEx::BackgroundTransparent(BOOL BackgroundTransparent)
	{
		SetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_BackgroundTransparent, (AutoInt)BackgroundTransparent);
	}
	/*--------取媒体音量--------*/
	int WebBrowserEx::MediaVolume()
	{
		return (int)GetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_MediaVolume);
	}
	/*--------置媒体音量--------*/
	void WebBrowserEx::MediaVolume(int MediaVolume)
	{
		SetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_MediaVolume, (AutoInt)MediaVolume);
	}
	/*--------取缩放度--------*/
	int WebBrowserEx::ZoomFactor()
	{
		return (int)GetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_ZoomFactor);
	}
	/*--------置缩放度--------*/
	void WebBrowserEx::ZoomFactor(int ZoomFactor)
	{
		SetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_ZoomFactor, (AutoInt)ZoomFactor);
	}
	/*--------取容许新窗口跳转--------*/
	BOOL WebBrowserEx::NavigationToNewWind()
	{
		return (BOOL)GetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_NavigationToNewWind);
	}
	/*--------置容许新窗口跳转--------*/
	void WebBrowserEx::NavigationToNewWind(BOOL NavigationToNewWind)
	{
		SetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_NavigationToNewWind, (AutoInt)NavigationToNewWind);
	}
	/*--------取启用Cookie--------*/
	BOOL WebBrowserEx::CookieEnabled()
	{
		return (BOOL)GetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_CookieEnabled);
	}
	/*--------置启用Cookie--------*/
	void WebBrowserEx::CookieEnabled(BOOL CookieEnabled)
	{
		SetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_CookieEnabled, (AutoInt)CookieEnabled);
	}
	/*--------取启用插件--------*/
	BOOL WebBrowserEx::NpapiEnabled()
	{
		return (BOOL)GetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_NpapiEnabled);
	}
	/*--------置启用插件--------*/
	void WebBrowserEx::NpapiEnabled(BOOL NpapiEnabled)
	{
		SetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_NpapiEnabled, (AutoInt)NpapiEnabled);
	}
	/*--------取启用菜单--------*/
	BOOL WebBrowserEx::MenuEnabled()
	{
		return (BOOL)GetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_MenuEnabled);
	}
	/*--------置启用菜单--------*/
	void WebBrowserEx::MenuEnabled(BOOL MenuEnabled)
	{
		SetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_MenuEnabled, (AutoInt)MenuEnabled);
	}
	/*--------取拖拽模式--------*/
	int WebBrowserEx::DragEnable()
	{
		return (int)GetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_DragEnable);
	}
	/*--------置拖拽模式--------*/
	void WebBrowserEx::DragEnable(int DragEnable)
	{
		SetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_DragEnable, (AutoInt)DragEnable);
	}
	/*--------取初始页面--------*/
	BindataEx WebBrowserEx::StartPage()
	{
		return (BinEx_P)GetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_StartPage);
	}
	/*--------置初始页面--------*/
	void WebBrowserEx::StartPage(BindataEx StartPage)
	{
		SetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_StartPage, (AutoInt)(BinEx_P)StartPage);
	}
	/*--------取用户代理--------*/
	BindataEx WebBrowserEx::UserAgent()
	{
		return (BinEx_P)GetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_UserAgent);
	}
	/*--------置用户代理--------*/
	void WebBrowserEx::UserAgent(BindataEx UserAgent)
	{
		SetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_UserAgent, (AutoInt)(BinEx_P)UserAgent);
	}
	/*--------取版本--------*/
	int WebBrowserEx::Version()
	{
		return (int)GetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_Version);
	}
	/*--------置版本--------*/
	void WebBrowserEx::Version(int Version)
	{
		SetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_Version, (AutoInt)Version);
	}
	/*--------取扩展元素--------*/
	BindataEx WebBrowserEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void WebBrowserEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx WebBrowserEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void WebBrowserEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_WebBrowserEx(control, WebBrowserEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件浏览框EX属性读写函数结束--------*/

}
//CalendarBoxEx
namespace exui
{
	CalendarBoxEx::CalendarBoxEx()
	{

	}
	CalendarBoxEx::~CalendarBoxEx()
	{

	}
	/*--------创建组件_日历框EX--------*/
	CalendarBoxEx_P CalendarBoxEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, StringEx MiniDate, StringEx MaxDate, StringEx SelectionDate, BindataEx TitleFont, BindataEx WeekFont, BindataEx DayFont, BindataEx TimeFont, int TitleClour, int WeekClour, int WeekendWeekClour, int DayClour, int WeekendClour, int OtherMonthClour, int TimeFontClour, BOOL OnlyThisMonth, int TimeMode, BOOL AutoSelect, int TitleHeight, int WeekHeight, int TimeRegulatorHeight, int PartnerHeight, int Language, BindataEx ItemData, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_CalendarBoxEx);
		CreateControl_CalendarBoxEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, MiniDate, MaxDate, SelectionDate, TitleFont, WeekFont, DayFont, TimeFont, TitleClour, WeekClour, WeekendWeekClour, DayClour, WeekendClour, OtherMonthClour, TimeFontClour, OnlyThisMonth, TimeMode, AutoSelect, TitleHeight, WeekHeight, TimeRegulatorHeight, PartnerHeight, Language, ItemData, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_日历框Ex--------*/
	void CalendarBoxEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_CalendarBoxEx(control, index, attribute);
	}
	/*--------取属性_日历框Ex--------*/
	AutoInt CalendarBoxEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_CalendarBoxEx(control, index);
	}
	/*--------置选中_日历框Ex--------*/
	void CalendarBoxEx::SetItemSelected(AutoInt index, BOOL Selected)
	{
		SetItemSelected_CalendarBoxEx(control, index, Selected);
	}
	/*--------取选中_日历框Ex--------*/
	BOOL CalendarBoxEx::GetItemSelected(AutoInt index)
	{
		return GetItemSelected_CalendarBoxEx(control, index);
	}
	/*--------置皮肤_日历框Ex--------*/
	void CalendarBoxEx::SetItemSkin(AutoInt index, BindataEx Skin)
	{
		SetItemSkin_CalendarBoxEx(control, index, Skin);
	}
	/*--------取皮肤_日历框Ex--------*/
	BindataEx CalendarBoxEx::GetItemSkin(AutoInt index)
	{
		return GetItemSkin_CalendarBoxEx(control, index);
	}

	/*----------组件日历框EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx CalendarBoxEx::Skin()
	{
		return (BinEx_P)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void CalendarBoxEx::Skin(BindataEx Skin)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取最小日期--------*/
	StringEx CalendarBoxEx::MiniDate()
	{
		return (StrEx_P)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_MiniDate);
	}
	/*--------置最小日期--------*/
	void CalendarBoxEx::MiniDate(StringEx MiniDate)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_MiniDate, (AutoInt)(StrEx_P)MiniDate);
	}
	/*--------取最大日期--------*/
	StringEx CalendarBoxEx::MaxDate()
	{
		return (StrEx_P)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_MaxDate);
	}
	/*--------置最大日期--------*/
	void CalendarBoxEx::MaxDate(StringEx MaxDate)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_MaxDate, (AutoInt)(StrEx_P)MaxDate);
	}
	/*--------取选中日期--------*/
	StringEx CalendarBoxEx::SelectionDate()
	{
		return (StrEx_P)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_SelectionDate);
	}
	/*--------置选中日期--------*/
	void CalendarBoxEx::SelectionDate(StringEx SelectionDate)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_SelectionDate, (AutoInt)(StrEx_P)SelectionDate);
	}
	/*--------取标题字体--------*/
	BindataEx CalendarBoxEx::TitleFont()
	{
		return (BinEx_P)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_TitleFont);
	}
	/*--------置标题字体--------*/
	void CalendarBoxEx::TitleFont(BindataEx TitleFont)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_TitleFont, (AutoInt)(BinEx_P)TitleFont);
	}
	/*--------取星期字体--------*/
	BindataEx CalendarBoxEx::WeekFont()
	{
		return (BinEx_P)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_WeekFont);
	}
	/*--------置星期字体--------*/
	void CalendarBoxEx::WeekFont(BindataEx WeekFont)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_WeekFont, (AutoInt)(BinEx_P)WeekFont);
	}
	/*--------取日期字体--------*/
	BindataEx CalendarBoxEx::DayFont()
	{
		return (BinEx_P)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_DayFont);
	}
	/*--------置日期字体--------*/
	void CalendarBoxEx::DayFont(BindataEx DayFont)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_DayFont, (AutoInt)(BinEx_P)DayFont);
	}
	/*--------取时间字体--------*/
	BindataEx CalendarBoxEx::TimeFont()
	{
		return (BinEx_P)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_TimeFont);
	}
	/*--------置时间字体--------*/
	void CalendarBoxEx::TimeFont(BindataEx TimeFont)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_TimeFont, (AutoInt)(BinEx_P)TimeFont);
	}
	/*--------取标题色--------*/
	int CalendarBoxEx::TitleClour()
	{
		return (int)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_TitleClour);
	}
	/*--------置标题色--------*/
	void CalendarBoxEx::TitleClour(int TitleClour)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_TitleClour, (AutoInt)TitleClour);
	}
	/*--------取星期色--------*/
	int CalendarBoxEx::WeekClour()
	{
		return (int)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_WeekClour);
	}
	/*--------置星期色--------*/
	void CalendarBoxEx::WeekClour(int WeekClour)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_WeekClour, (AutoInt)WeekClour);
	}
	/*--------取双休星期色--------*/
	int CalendarBoxEx::WeekendWeekClour()
	{
		return (int)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_WeekendWeekClour);
	}
	/*--------置双休星期色--------*/
	void CalendarBoxEx::WeekendWeekClour(int WeekendWeekClour)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_WeekendWeekClour, (AutoInt)WeekendWeekClour);
	}
	/*--------取日期色--------*/
	int CalendarBoxEx::DayClour()
	{
		return (int)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_DayClour);
	}
	/*--------置日期色--------*/
	void CalendarBoxEx::DayClour(int DayClour)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_DayClour, (AutoInt)DayClour);
	}
	/*--------取双休日期色--------*/
	int CalendarBoxEx::WeekendClour()
	{
		return (int)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_WeekendClour);
	}
	/*--------置双休日期色--------*/
	void CalendarBoxEx::WeekendClour(int WeekendClour)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_WeekendClour, (AutoInt)WeekendClour);
	}
	/*--------取非本月色--------*/
	int CalendarBoxEx::OtherMonthClour()
	{
		return (int)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_OtherMonthClour);
	}
	/*--------置非本月色--------*/
	void CalendarBoxEx::OtherMonthClour(int OtherMonthClour)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_OtherMonthClour, (AutoInt)OtherMonthClour);
	}
	/*--------取时间字体色--------*/
	int CalendarBoxEx::TimeFontClour()
	{
		return (int)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_TimeFontClour);
	}
	/*--------置时间字体色--------*/
	void CalendarBoxEx::TimeFontClour(int TimeFontClour)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_TimeFontClour, (AutoInt)TimeFontClour);
	}
	/*--------取只显示本月--------*/
	BOOL CalendarBoxEx::OnlyThisMonth()
	{
		return (BOOL)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_OnlyThisMonth);
	}
	/*--------置只显示本月--------*/
	void CalendarBoxEx::OnlyThisMonth(BOOL OnlyThisMonth)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_OnlyThisMonth, (AutoInt)OnlyThisMonth);
	}
	/*--------取时间调整模式--------*/
	int CalendarBoxEx::TimeMode()
	{
		return (int)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_TimeMode);
	}
	/*--------置时间调整模式--------*/
	void CalendarBoxEx::TimeMode(int TimeMode)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_TimeMode, (AutoInt)TimeMode);
	}
	/*--------取自动选中--------*/
	BOOL CalendarBoxEx::AutoSelect()
	{
		return (BOOL)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_AutoSelect);
	}
	/*--------置自动选中--------*/
	void CalendarBoxEx::AutoSelect(BOOL AutoSelect)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_AutoSelect, (AutoInt)AutoSelect);
	}
	/*--------取标题高度--------*/
	int CalendarBoxEx::TitleHeight()
	{
		return (int)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_TitleHeight);
	}
	/*--------置标题高度--------*/
	void CalendarBoxEx::TitleHeight(int TitleHeight)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_TitleHeight, (AutoInt)TitleHeight);
	}
	/*--------取星期高度--------*/
	int CalendarBoxEx::WeekHeight()
	{
		return (int)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_WeekHeight);
	}
	/*--------置星期高度--------*/
	void CalendarBoxEx::WeekHeight(int WeekHeight)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_WeekHeight, (AutoInt)WeekHeight);
	}
	/*--------取时间调节器高度--------*/
	int CalendarBoxEx::TimeRegulatorHeight()
	{
		return (int)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_TimeRegulatorHeight);
	}
	/*--------置时间调节器高度--------*/
	void CalendarBoxEx::TimeRegulatorHeight(int TimeRegulatorHeight)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_TimeRegulatorHeight, (AutoInt)TimeRegulatorHeight);
	}
	/*--------取伙伴高度--------*/
	int CalendarBoxEx::PartnerHeight()
	{
		return (int)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_PartnerHeight);
	}
	/*--------置伙伴高度--------*/
	void CalendarBoxEx::PartnerHeight(int PartnerHeight)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_PartnerHeight, (AutoInt)PartnerHeight);
	}
	/*--------取语言--------*/
	int CalendarBoxEx::Language()
	{
		return (int)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_Language);
	}
	/*--------置语言--------*/
	void CalendarBoxEx::Language(int Language)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_Language, (AutoInt)Language);
	}
	/*--------取项目数据--------*/
	BindataEx CalendarBoxEx::ItemData()
	{
		return (BinEx_P)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_ItemData);
	}
	/*--------置项目数据--------*/
	void CalendarBoxEx::ItemData(BindataEx ItemData)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_ItemData, (AutoInt)(BinEx_P)ItemData);
	}
	/*--------取扩展元素--------*/
	BindataEx CalendarBoxEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void CalendarBoxEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx CalendarBoxEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void CalendarBoxEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_CalendarBoxEx(control, CalendarBoxEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件日历框EX属性读写函数结束--------*/
}
//ColorPickEx
namespace exui
{
	ColorPickEx::ColorPickEx()
	{

	}
	ColorPickEx::~ColorPickEx()
	{

	}
	/*--------创建组件_选色板EX--------*/
	ColorPickEx_P ColorPickEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, BindataEx Font, int FontClour, int NowClour, BOOL DragTrace, int ClourMode, BindataEx QuickClours, int Style, int RegulatorHeight, int QuickClourSize, int PartnerHeight, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_ColorPickEx);
		CreateControl_ColorPickEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, Font, FontClour, NowClour, DragTrace, ClourMode, QuickClours, Style, RegulatorHeight, QuickClourSize, PartnerHeight, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_选色板EX--------*/
	void ColorPickEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_ColorPickEx(control, index, attribute);
	}
	/*--------取属性_选色板EX--------*/
	AutoInt ColorPickEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_ColorPickEx(control, index);
	}

	/*----------组件选色板EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx ColorPickEx::Skin()
	{
		return (BinEx_P)GetAttribute_ColorPickEx(control, ColorPickEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void ColorPickEx::Skin(BindataEx Skin)
	{
		SetAttribute_ColorPickEx(control, ColorPickEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取字体--------*/
	BindataEx ColorPickEx::Font()
	{
		return (BinEx_P)GetAttribute_ColorPickEx(control, ColorPickEx_Attr_Font);
	}
	/*--------置字体--------*/
	void ColorPickEx::Font(BindataEx Font)
	{
		SetAttribute_ColorPickEx(control, ColorPickEx_Attr_Font, (AutoInt)(BinEx_P)Font);
	}
	/*--------取字体色--------*/
	int ColorPickEx::FontClour()
	{
		return (int)GetAttribute_ColorPickEx(control, ColorPickEx_Attr_FontClour);
	}
	/*--------置字体色--------*/
	void ColorPickEx::FontClour(int FontClour)
	{
		SetAttribute_ColorPickEx(control, ColorPickEx_Attr_FontClour, (AutoInt)FontClour);
	}
	/*--------取当前色--------*/
	int ColorPickEx::NowClour()
	{
		return (int)GetAttribute_ColorPickEx(control, ColorPickEx_Attr_NowClour);
	}
	/*--------置当前色--------*/
	void ColorPickEx::NowClour(int NowClour)
	{
		SetAttribute_ColorPickEx(control, ColorPickEx_Attr_NowClour, (AutoInt)NowClour);
	}
	/*--------取调整跟踪--------*/
	BOOL ColorPickEx::DragTrace()
	{
		return (BOOL)GetAttribute_ColorPickEx(control, ColorPickEx_Attr_DragTrace);
	}
	/*--------置调整跟踪--------*/
	void ColorPickEx::DragTrace(BOOL DragTrace)
	{
		SetAttribute_ColorPickEx(control, ColorPickEx_Attr_DragTrace, (AutoInt)DragTrace);
	}
	/*--------取颜色模式--------*/
	int ColorPickEx::ClourMode()
	{
		return (int)GetAttribute_ColorPickEx(control, ColorPickEx_Attr_ClourMode);
	}
	/*--------置颜色模式--------*/
	void ColorPickEx::ClourMode(int ClourMode)
	{
		SetAttribute_ColorPickEx(control, ColorPickEx_Attr_ClourMode, (AutoInt)ClourMode);
	}
	/*--------取备选色--------*/
	BindataEx ColorPickEx::QuickClours()
	{
		return (BinEx_P)GetAttribute_ColorPickEx(control, ColorPickEx_Attr_QuickClours);
	}
	/*--------置备选色--------*/
	void ColorPickEx::QuickClours(BindataEx QuickClours)
	{
		SetAttribute_ColorPickEx(control, ColorPickEx_Attr_QuickClours, (AutoInt)(BinEx_P)QuickClours);
	}
	/*--------取样式--------*/
	int ColorPickEx::Style()
	{
		return (int)GetAttribute_ColorPickEx(control, ColorPickEx_Attr_Style);
	}
	/*--------置样式--------*/
	void ColorPickEx::Style(int Style)
	{
		SetAttribute_ColorPickEx(control, ColorPickEx_Attr_Style, (AutoInt)Style);
	}
	/*--------取调节器高度--------*/
	int ColorPickEx::RegulatorHeight()
	{
		return (int)GetAttribute_ColorPickEx(control, ColorPickEx_Attr_RegulatorHeight);
	}
	/*--------置调节器高度--------*/
	void ColorPickEx::RegulatorHeight(int RegulatorHeight)
	{
		SetAttribute_ColorPickEx(control, ColorPickEx_Attr_RegulatorHeight, (AutoInt)RegulatorHeight);
	}
	/*--------取备选色直径--------*/
	int ColorPickEx::QuickClourSize()
	{
		return (int)GetAttribute_ColorPickEx(control, ColorPickEx_Attr_QuickClourSize);
	}
	/*--------置备选色直径--------*/
	void ColorPickEx::QuickClourSize(int QuickClourSize)
	{
		SetAttribute_ColorPickEx(control, ColorPickEx_Attr_QuickClourSize, (AutoInt)QuickClourSize);
	}
	/*--------取伙伴高度--------*/
	int ColorPickEx::PartnerHeight()
	{
		return (int)GetAttribute_ColorPickEx(control, ColorPickEx_Attr_PartnerHeight);
	}
	/*--------置伙伴高度--------*/
	void ColorPickEx::PartnerHeight(int PartnerHeight)
	{
		SetAttribute_ColorPickEx(control, ColorPickEx_Attr_PartnerHeight, (AutoInt)PartnerHeight);
	}
	/*--------取扩展元素--------*/
	BindataEx ColorPickEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_ColorPickEx(control, ColorPickEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void ColorPickEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_ColorPickEx(control, ColorPickEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx ColorPickEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_ColorPickEx(control, ColorPickEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void ColorPickEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_ColorPickEx(control, ColorPickEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件选色板EX属性读写函数结束--------*/

}
//RichEditEx
namespace exui
{
	RichEditEx::RichEditEx()
	{

	}
	RichEditEx::~RichEditEx()
	{

	}
	/*--------创建组件_富文本框EX--------*/
	RichEditEx_P RichEditEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, StringEx Content, int Type, BOOL BackTransparent, int BackClour, int CursorColor, BindataEx DefaultCharFormat, BindataEx DefaultParaFormat, int InputMode, int MaxInput, StringEx PasswordSubstitution, BOOL Multiline, BOOL Wrapped, int ScrollBarMode, int MenuTableWidth, int MenuTableHeight, BindataEx MenuFont, int MenuFontClour, int MenuDisabledFontClour, int MenuTransparency, int MenuLanguage, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_RichEditEx);
		CreateControl_RichEditEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, Content, Type, BackTransparent, BackClour, CursorColor, DefaultCharFormat, DefaultParaFormat, InputMode, MaxInput, PasswordSubstitution, Multiline, Wrapped, ScrollBarMode, MenuTableWidth, MenuTableHeight, MenuFont, MenuFontClour, MenuDisabledFontClour, MenuTransparency, MenuLanguage, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_富文本框EX--------*/
	void RichEditEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_RichEditEx(control, index, attribute);
	}
	/*--------取属性_富文本框EX--------*/
	AutoInt RichEditEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_RichEditEx(control, index);
	}
	/*--------插入文本_编辑框Ex--------*/
	void RichEditEx::InsertText(AutoInt index, StringEx text, int UseRich, StringEx FontName, int Size, int style, int TextColor, int Alignment, int LineHeight)
	{
		InsertText_RichEditEx(control, index, text, UseRich, FontName, Size, style, TextColor, Alignment, LineHeight);
	}
	/*--------删除文本_编辑框Ex--------*/
	void RichEditEx::DeleteText(AutoInt index, AutoInt dellen)
	{
		DeleteText_RichEditEx(control, index, dellen);
	}
	/*--------置光标位置_编辑框Ex--------*/
	void RichEditEx::SetInsertCursor(AutoInt index)
	{
		SetInsertCursor_RichEditEx(control, index);
	}
	/*--------取光标位置_编辑框Ex--------*/
	AutoInt RichEditEx::GetInsertCursor()
	{
		return GetInsertCursor_RichEditEx(control);
	}
	/*--------置选择文本长度_编辑框Ex--------*/
	void RichEditEx::SetSelectLeng(AutoInt SelectLeng)
	{
		SetSelectLeng_RichEditEx(control, SelectLeng);
	}
	/*--------取选择文本长度_编辑框Ex--------*/
	AutoInt RichEditEx::GetSelectLeng()
	{
		return GetSelectLeng_RichEditEx(control);
	}
	/*--------保证显示字符_编辑框Ex--------*/
	void RichEditEx::GuaranteeVisibleText(AutoInt index, int mode)
	{
		GuaranteeVisibleText_RichEditEx(control, index, mode);
	}
	/*--------置选择区字符格式--------*/
	BOOL RichEditEx::SetSelCharFormat(BindataEx CharFormat)
	{
		return SetSelCharFormat_RichEditEx(control, CharFormat);
	}
	/*--------取选择区字符格式--------*/
	BindataEx RichEditEx::GetSelCharFormat()
	{
		return GetSelCharFormat_RichEditEx(control);
	}
	/*--------置选择区段落方式--------*/
	BOOL RichEditEx::SetSelParaFormat(BindataEx CharFormat)
	{
		return SetSelParaFormat_RichEditEx(control, CharFormat);
	}
	/*--------取选择区段落方式--------*/
	BindataEx RichEditEx::GetSelParaFormat()
	{
		return GetSelParaFormat_RichEditEx(control);
	}
	/*--------执行接口命令--------*/
	AutoInt RichEditEx::RunServicesCmd(UINT msg, WPARAM wparam, LPARAM lparam, LRESULT* plresult)
	{
		return RunServicesCmd_RichEditEx(control, msg, wparam, lparam, plresult);
	}

	/*----------组件富文本框EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx RichEditEx::Skin()
	{
		return (BinEx_P)GetAttribute_RichEditEx(control, RichEditEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void RichEditEx::Skin(BindataEx Skin)
	{
		SetAttribute_RichEditEx(control, RichEditEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取内容--------*/
	StringEx RichEditEx::Content()
	{
		return (StrEx_P)GetAttribute_RichEditEx(control, RichEditEx_Attr_Content);
	}
	/*--------置内容--------*/
	void RichEditEx::Content(StringEx Content)
	{
		SetAttribute_RichEditEx(control, RichEditEx_Attr_Content, (AutoInt)(StrEx_P)Content);
	}
	/*--------取类型--------*/
	int RichEditEx::Type()
	{
		return (int)GetAttribute_RichEditEx(control, RichEditEx_Attr_Type);
	}
	/*--------置类型--------*/
	void RichEditEx::Type(int Type)
	{
		SetAttribute_RichEditEx(control, RichEditEx_Attr_Type, (AutoInt)Type);
	}
	/*--------取背景透明--------*/
	BOOL RichEditEx::BackTransparent()
	{
		return (BOOL)GetAttribute_RichEditEx(control, RichEditEx_Attr_BackTransparent);
	}
	/*--------置背景透明--------*/
	void RichEditEx::BackTransparent(BOOL BackTransparent)
	{
		SetAttribute_RichEditEx(control, RichEditEx_Attr_BackTransparent, (AutoInt)BackTransparent);
	}
	/*--------取背景色--------*/
	int RichEditEx::BackClour()
	{
		return (int)GetAttribute_RichEditEx(control, RichEditEx_Attr_BackClour);
	}
	/*--------置背景色--------*/
	void RichEditEx::BackClour(int BackClour)
	{
		SetAttribute_RichEditEx(control, RichEditEx_Attr_BackClour, (AutoInt)BackClour);
	}
	/*--------取光标色--------*/
	int RichEditEx::CursorColor()
	{
		return (int)GetAttribute_RichEditEx(control, RichEditEx_Attr_CursorColor);
	}
	/*--------置光标色--------*/
	void RichEditEx::CursorColor(int CursorColor)
	{
		SetAttribute_RichEditEx(control, RichEditEx_Attr_CursorColor, (AutoInt)CursorColor);
	}
	/*--------取默认字符格式--------*/
	BindataEx RichEditEx::DefaultCharFormat()
	{
		return (BinEx_P)GetAttribute_RichEditEx(control, RichEditEx_Attr_DefaultCharFormat);
	}
	/*--------置默认字符格式--------*/
	void RichEditEx::DefaultCharFormat(BindataEx DefaultCharFormat)
	{
		SetAttribute_RichEditEx(control, RichEditEx_Attr_DefaultCharFormat, (AutoInt)(BinEx_P)DefaultCharFormat);
	}
	/*--------取默认段落格式--------*/
	BindataEx RichEditEx::DefaultParaFormat()
	{
		return (BinEx_P)GetAttribute_RichEditEx(control, RichEditEx_Attr_DefaultParaFormat);
	}
	/*--------置默认段落格式--------*/
	void RichEditEx::DefaultParaFormat(BindataEx DefaultParaFormat)
	{
		SetAttribute_RichEditEx(control, RichEditEx_Attr_DefaultParaFormat, (AutoInt)(BinEx_P)DefaultParaFormat);
	}
	/*--------取输入方式--------*/
	int RichEditEx::InputMode()
	{
		return (int)GetAttribute_RichEditEx(control, RichEditEx_Attr_InputMode);
	}
	/*--------置输入方式--------*/
	void RichEditEx::InputMode(int InputMode)
	{
		SetAttribute_RichEditEx(control, RichEditEx_Attr_InputMode, (AutoInt)InputMode);
	}
	/*--------取最大容许长度--------*/
	int RichEditEx::MaxInput()
	{
		return (int)GetAttribute_RichEditEx(control, RichEditEx_Attr_MaxInput);
	}
	/*--------置最大容许长度--------*/
	void RichEditEx::MaxInput(int MaxInput)
	{
		SetAttribute_RichEditEx(control, RichEditEx_Attr_MaxInput, (AutoInt)MaxInput);
	}
	/*--------取密码替换符--------*/
	StringEx RichEditEx::PasswordSubstitution()
	{
		return (StrEx_P)GetAttribute_RichEditEx(control, RichEditEx_Attr_PasswordSubstitution);
	}
	/*--------置密码替换符--------*/
	void RichEditEx::PasswordSubstitution(StringEx PasswordSubstitution)
	{
		SetAttribute_RichEditEx(control, RichEditEx_Attr_PasswordSubstitution, (AutoInt)(StrEx_P)PasswordSubstitution);
	}
	/*--------取是否容许多行--------*/
	BOOL RichEditEx::Multiline()
	{
		return (BOOL)GetAttribute_RichEditEx(control, RichEditEx_Attr_Multiline);
	}
	/*--------置是否容许多行--------*/
	void RichEditEx::Multiline(BOOL Multiline)
	{
		SetAttribute_RichEditEx(control, RichEditEx_Attr_Multiline, (AutoInt)Multiline);
	}
	/*--------取自动换行--------*/
	BOOL RichEditEx::Wrapped()
	{
		return (BOOL)GetAttribute_RichEditEx(control, RichEditEx_Attr_Wrapped);
	}
	/*--------置自动换行--------*/
	void RichEditEx::Wrapped(BOOL Wrapped)
	{
		SetAttribute_RichEditEx(control, RichEditEx_Attr_Wrapped, (AutoInt)Wrapped);
	}
	/*--------取滚动条方式--------*/
	int RichEditEx::ScrollBarMode()
	{
		return (int)GetAttribute_RichEditEx(control, RichEditEx_Attr_ScrollBarMode);
	}
	/*--------置滚动条方式--------*/
	void RichEditEx::ScrollBarMode(int ScrollBarMode)
	{
		SetAttribute_RichEditEx(control, RichEditEx_Attr_ScrollBarMode, (AutoInt)ScrollBarMode);
	}
	/*--------取菜单项目宽--------*/
	int RichEditEx::MenuTableWidth()
	{
		return (int)GetAttribute_RichEditEx(control, RichEditEx_Attr_MenuTableWidth);
	}
	/*--------置菜单项目宽--------*/
	void RichEditEx::MenuTableWidth(int MenuTableWidth)
	{
		SetAttribute_RichEditEx(control, RichEditEx_Attr_MenuTableWidth, (AutoInt)MenuTableWidth);
	}
	/*--------取菜单项目高--------*/
	int RichEditEx::MenuTableHeight()
	{
		return (int)GetAttribute_RichEditEx(control, RichEditEx_Attr_MenuTableHeight);
	}
	/*--------置菜单项目高--------*/
	void RichEditEx::MenuTableHeight(int MenuTableHeight)
	{
		SetAttribute_RichEditEx(control, RichEditEx_Attr_MenuTableHeight, (AutoInt)MenuTableHeight);
	}
	/*--------取菜单字体--------*/
	BindataEx RichEditEx::MenuFont()
	{
		return (BinEx_P)GetAttribute_RichEditEx(control, RichEditEx_Attr_MenuFont);
	}
	/*--------置菜单字体--------*/
	void RichEditEx::MenuFont(BindataEx MenuFont)
	{
		SetAttribute_RichEditEx(control, RichEditEx_Attr_MenuFont, (AutoInt)(BinEx_P)MenuFont);
	}
	/*--------取菜单字体色--------*/
	int RichEditEx::MenuFontClour()
	{
		return (int)GetAttribute_RichEditEx(control, RichEditEx_Attr_MenuFontClour);
	}
	/*--------置菜单字体色--------*/
	void RichEditEx::MenuFontClour(int MenuFontClour)
	{
		SetAttribute_RichEditEx(control, RichEditEx_Attr_MenuFontClour, (AutoInt)MenuFontClour);
	}
	/*--------取菜单禁止字体色--------*/
	int RichEditEx::MenuDisabledFontClour()
	{
		return (int)GetAttribute_RichEditEx(control, RichEditEx_Attr_MenuDisabledFontClour);
	}
	/*--------置菜单禁止字体色--------*/
	void RichEditEx::MenuDisabledFontClour(int MenuDisabledFontClour)
	{
		SetAttribute_RichEditEx(control, RichEditEx_Attr_MenuDisabledFontClour, (AutoInt)MenuDisabledFontClour);
	}
	/*--------取菜单透明度--------*/
	int RichEditEx::MenuTransparency()
	{
		return (int)GetAttribute_RichEditEx(control, RichEditEx_Attr_MenuTransparency);
	}
	/*--------置菜单透明度--------*/
	void RichEditEx::MenuTransparency(int MenuTransparency)
	{
		SetAttribute_RichEditEx(control, RichEditEx_Attr_MenuTransparency, (AutoInt)MenuTransparency);
	}
	/*--------取菜单语言--------*/
	int RichEditEx::MenuLanguage()
	{
		return (int)GetAttribute_RichEditEx(control, RichEditEx_Attr_MenuLanguage);
	}
	/*--------置菜单语言--------*/
	void RichEditEx::MenuLanguage(int MenuLanguage)
	{
		SetAttribute_RichEditEx(control, RichEditEx_Attr_MenuLanguage, (AutoInt)MenuLanguage);
	}
	/*--------取扩展元素--------*/
	BindataEx RichEditEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_RichEditEx(control, RichEditEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void RichEditEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_RichEditEx(control, RichEditEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx RichEditEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_RichEditEx(control, RichEditEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void RichEditEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_RichEditEx(control, RichEditEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件富文本框EX属性读写函数结束--------*/


}
//ExtendEx
namespace exui
{
	ExtendEx::ExtendEx()
	{

	}
	ExtendEx::~ExtendEx()
	{

	}
	/*--------创建组件_扩展组件EX--------*/
	ExtendEx_P ExtendEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx AttrEx, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_ExtendEx);
		CreateControl_ExtendEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, AttrEx, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_扩展组件Ex--------*/
	void ExtendEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_ExtendEx(control, index, attribute);
	}
	/*--------取属性_扩展组件Ex--------*/
	AutoInt ExtendEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_ExtendEx(control, index);
	}
	/*--------启用开发者工具_扩展组件Ex--------*/
	int ExtendEx::ShowDevtools(StringEx toolspath)
	{
		return ShowDevtools_ExtendEx(control, toolspath);
	}
	/*--------执行扩展命令_扩展组件Ex--------*/
	StringEx ExtendEx::RunExtendCmd(StringEx Cmd, StringEx  Param1, StringEx  Param2, StringEx  Param3)
	{
		return RunExtendCmd_ExtendEx(control, Cmd, Param1, Param2, Param3);
	}

	/*----------组件扩展组件EX属性读写函数开始--------*/


/*--------取扩展属性表--------*/
	BindataEx ExtendEx::ExtendAttr()
	{
		return (BinEx_P)GetAttribute_ExtendEx(control, ExtendEx_Attr_ExtendAttr);
	}
	/*--------置扩展属性表--------*/
	void ExtendEx::ExtendAttr(BindataEx ExtendAttr)
	{
		SetAttribute_ExtendEx(control, ExtendEx_Attr_ExtendAttr, (AutoInt)(BinEx_P)ExtendAttr);
	}
	/*--------取扩展元素--------*/
	BindataEx ExtendEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_ExtendEx(control, ExtendEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void ExtendEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_ExtendEx(control, ExtendEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx ExtendEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_ExtendEx(control, ExtendEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void ExtendEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_ExtendEx(control, ExtendEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件扩展组件EX属性读写函数结束--------*/

}
//AnimationbuttonEx
namespace exui
{
	AnimationbuttonEx::AnimationbuttonEx()
	{

	}
	AnimationbuttonEx::~AnimationbuttonEx()
	{

	}
	/*--------创建组件_动画按钮EX--------*/
	AnimationbuttonEx_P AnimationbuttonEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, BOOL Selected, BindataEx Icon, int IconWidth, int IconHeight, int Align, StringEx Text, BindataEx Font, int FontClour, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_AnimationbuttonEx);
		CreateControl_AnimationbuttonEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, Selected, Icon, IconWidth, IconHeight, Align, Text, Font, FontClour, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_动画按钮EX--------*/
	void AnimationbuttonEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_AnimationbuttonEx(control, index, attribute);
	}
	/*--------取属性_动画按钮EX--------*/
	AutoInt AnimationbuttonEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_AnimationbuttonEx(control, index);
	}

	/*----------组件动画按钮EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx AnimationbuttonEx::Skin()
	{
		return (BinEx_P)GetAttribute_AnimationbuttonEx(control, AnimationbuttonEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void AnimationbuttonEx::Skin(BindataEx Skin)
	{
		SetAttribute_AnimationbuttonEx(control, AnimationbuttonEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取启用动画--------*/
	BOOL AnimationbuttonEx::EnableAnimation()
	{
		return (BOOL)GetAttribute_AnimationbuttonEx(control, AnimationbuttonEx_Attr_EnableAnimation);
	}
	/*--------置启用动画--------*/
	void AnimationbuttonEx::EnableAnimation(BOOL EnableAnimation)
	{
		SetAttribute_AnimationbuttonEx(control, AnimationbuttonEx_Attr_EnableAnimation, (AutoInt)EnableAnimation);
	}
	/*--------取图标--------*/
	BindataEx AnimationbuttonEx::Icon()
	{
		return (BinEx_P)GetAttribute_AnimationbuttonEx(control, AnimationbuttonEx_Attr_Icon);
	}
	/*--------置图标--------*/
	void AnimationbuttonEx::Icon(BindataEx Icon)
	{
		SetAttribute_AnimationbuttonEx(control, AnimationbuttonEx_Attr_Icon, (AutoInt)(BinEx_P)Icon);
	}
	/*--------取图标宽--------*/
	int AnimationbuttonEx::IconWidth()
	{
		return (int)GetAttribute_AnimationbuttonEx(control, AnimationbuttonEx_Attr_IconWidth);
	}
	/*--------置图标宽--------*/
	void AnimationbuttonEx::IconWidth(int IconWidth)
	{
		SetAttribute_AnimationbuttonEx(control, AnimationbuttonEx_Attr_IconWidth, (AutoInt)IconWidth);
	}
	/*--------取图标高--------*/
	int AnimationbuttonEx::IconHeight()
	{
		return (int)GetAttribute_AnimationbuttonEx(control, AnimationbuttonEx_Attr_IconHeight);
	}
	/*--------置图标高--------*/
	void AnimationbuttonEx::IconHeight(int IconHeight)
	{
		SetAttribute_AnimationbuttonEx(control, AnimationbuttonEx_Attr_IconHeight, (AutoInt)IconHeight);
	}
	/*--------取对齐方式--------*/
	int AnimationbuttonEx::Align()
	{
		return (int)GetAttribute_AnimationbuttonEx(control, AnimationbuttonEx_Attr_Align);
	}
	/*--------置对齐方式--------*/
	void AnimationbuttonEx::Align(int Align)
	{
		SetAttribute_AnimationbuttonEx(control, AnimationbuttonEx_Attr_Align, (AutoInt)Align);
	}
	/*--------取标题--------*/
	StringEx AnimationbuttonEx::Title()
	{
		return (StrEx_P)GetAttribute_AnimationbuttonEx(control, AnimationbuttonEx_Attr_Title);
	}
	/*--------置标题--------*/
	void AnimationbuttonEx::Title(StringEx Title)
	{
		SetAttribute_AnimationbuttonEx(control, AnimationbuttonEx_Attr_Title, (AutoInt)(StrEx_P)Title);
	}
	/*--------取字体--------*/
	BindataEx AnimationbuttonEx::Font()
	{
		return (BinEx_P)GetAttribute_AnimationbuttonEx(control, AnimationbuttonEx_Attr_Font);
	}
	/*--------置字体--------*/
	void AnimationbuttonEx::Font(BindataEx Font)
	{
		SetAttribute_AnimationbuttonEx(control, AnimationbuttonEx_Attr_Font, (AutoInt)(BinEx_P)Font);
	}
	/*--------取字体色--------*/
	int AnimationbuttonEx::FontClour()
	{
		return (int)GetAttribute_AnimationbuttonEx(control, AnimationbuttonEx_Attr_FontClour);
	}
	/*--------置字体色--------*/
	void AnimationbuttonEx::FontClour(int FontClour)
	{
		SetAttribute_AnimationbuttonEx(control, AnimationbuttonEx_Attr_FontClour, (AutoInt)FontClour);
	}
	/*--------取扩展元素--------*/
	BindataEx AnimationbuttonEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_AnimationbuttonEx(control, AnimationbuttonEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void AnimationbuttonEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_AnimationbuttonEx(control, AnimationbuttonEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx AnimationbuttonEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_AnimationbuttonEx(control, AnimationbuttonEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void AnimationbuttonEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_AnimationbuttonEx(control, AnimationbuttonEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件动画按钮EX属性读写函数结束--------*/

}
//AdvancedFormEx
namespace exui
{
	AdvancedFormEx::AdvancedFormEx()
	{

	}
	AdvancedFormEx::~AdvancedFormEx()
	{

	}
	/*--------创建组件_高级表格EX--------*/
	AdvancedFormEx_P AdvancedFormEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, AutoInt CurrentItem, AutoInt CurrentColumn, AutoInt HeaderRowNum, AutoInt HeaderColumnNum, AutoInt BottomFixedRow, AutoInt RightFixedColumn, int AdjustmentMode, int SelectMode, BOOL EntireLine, int EventMode, int EditMode, BOOL TreeType, int LineMode, int ScrollBarMode, BindataEx ItemData, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_AdvancedFormEx);
		CreateControl_AdvancedFormEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, CurrentItem, CurrentColumn, HeaderRowNum, HeaderColumnNum, BottomFixedRow, RightFixedColumn, AdjustmentMode, SelectMode, EntireLine, EventMode, EditMode, TreeType, LineMode, ScrollBarMode, ItemData, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_高级表格Ex--------*/
	void AdvancedFormEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_AdvancedFormEx(control, index, attribute);
	}
	/*--------取属性_高级表格Ex--------*/
	AutoInt AdvancedFormEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_AdvancedFormEx(control, index);
	}
	/*--------插入列_高级表格Ex--------*/
	AutoInt AdvancedFormEx::InsertColumn(AutoInt cloid, AutoInt addnum, int wight, int min, int max, BindataEx HeaderFont, int HeaderIcoW, int HeaderIcoH, int HeaderAlign, BindataEx ItemFont, int ItemIcoW, int ItemIcoH, int ItemAlign, BindataEx FixedItemFont, int FixedItemIcoW, int FixedItemIcoH, int FixedItemAlign, int HeaderTreeType, int TreeType, int BottomFixedTreeType, int ChildMigration, int ContentMigration, int LineMode, BindataEx Skin)
	{
		return InsertColumn_AdvancedFormEx(control, cloid, addnum, wight, min, max, HeaderFont, HeaderIcoW, HeaderIcoH, HeaderAlign, ItemFont, ItemIcoW, ItemIcoH, ItemAlign, FixedItemFont, FixedItemIcoW, FixedItemIcoH, FixedItemAlign, HeaderTreeType, TreeType, BottomFixedTreeType, ChildMigration, ContentMigration, LineMode, Skin);
	}
	/*--------删除列_高级表格Ex--------*/
	void AdvancedFormEx::DeleteColumn(AutoInt delindex, AutoInt deletenum)
	{
		DeleteColumn_AdvancedFormEx(control, delindex, deletenum);
	}
	/*--------取列数量_高级表格Ex--------*/
	int AdvancedFormEx::GetColumnCount()
	{
		return GetColumnCount_AdvancedFormEx(control);
	}
	/*--------置列属性_高级表格Ex--------*/
	void AdvancedFormEx::SetColumnAttribute(AutoInt index, AutoInt Endindex, AutoInt AttributeId, int wight, int min, int max, BindataEx HeaderFont, int HeaderIcoW, int HeaderIcoH, int HeaderAlign, BindataEx ItemFont, int ItemIcoW, int ItemIcoH, int ItemAlign, BindataEx FixedItemFont, int FixedItemIcoW, int FixedItemIcoH, int FixedItemAlign, int HeaderTreeType, int TreeType, int BottomFixedTreeType, int ChildMigration, int ContentMigration, int LineMode, BindataEx Skin)
	{
		SetColumnAttribute_AdvancedFormEx(control, index, Endindex, AttributeId, wight, min, max, HeaderFont, HeaderIcoW, HeaderIcoH, HeaderAlign, ItemFont, ItemIcoW, ItemIcoH, ItemAlign, FixedItemFont, FixedItemIcoW, FixedItemIcoH, FixedItemAlign, HeaderTreeType, TreeType, BottomFixedTreeType, ChildMigration, ContentMigration, LineMode, Skin);
	}
	/*--------取列属性_高级表格Ex--------*/
	int AdvancedFormEx::GetColumnAttribute(AutoInt index, AutoInt AttributeId)
	{
		return GetColumnAttribute_AdvancedFormEx(control, index, AttributeId);
	}
	/*--------插入项目_高级表格Ex--------*/
	AutoInt AdvancedFormEx::InsertItem(AutoInt index, AutoInt addnum, AutoInt Data, int Height, int MinHeight, int MaxHeight, int Style, BOOL SetFold, int InsMode)
	{
		return InsertItem_AdvancedFormEx(control, index, addnum, Data, Height, MinHeight, MaxHeight, Style, SetFold, InsMode);
	}
	/*--------删除项目_高级表格Ex--------*/
	void AdvancedFormEx::DeleteItem(AutoInt delindex, AutoInt deletenum)
	{
		DeleteItem_AdvancedFormEx(control, delindex, deletenum);
	}
	/*--------取数量_高级表格Ex--------*/
	AutoInt AdvancedFormEx::GetItemCount()
	{
		return GetItemCount_AdvancedFormEx(control);
	}
	/*--------置附加值_高级表格Ex--------*/
	void AdvancedFormEx::SetItemData(AutoInt StarRow, AutoInt EndRow, AutoInt Data)
	{
		SetItemData_AdvancedFormEx(control, StarRow, EndRow, Data);
	}
	/*--------取附加值_高级表格Ex--------*/
	AutoInt AdvancedFormEx::GetItemData(AutoInt index)
	{
		return GetItemData_AdvancedFormEx(control, index);
	}
	/*--------置图标_高级表格Ex--------*/
	void AdvancedFormEx::SetItemIco(AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn, BitmapEx Ico)
	{
		SetItemIco_AdvancedFormEx(control, StarRow, StarColumn, EndRow, EndColumn, Ico);
	}
	/*--------取图标_高级表格Ex--------*/
	BitmapEx AdvancedFormEx::GetItemIco(AutoInt index, AutoInt cloid)
	{
		return GetItemIco_AdvancedFormEx(control, index, cloid);
	}
	/*--------置文本_高级表格Ex--------*/
	void AdvancedFormEx::SetItemTitle(AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn, StringEx Title)
	{
		SetItemTitle_AdvancedFormEx(control, StarRow, StarColumn, EndRow, EndColumn, Title);
	}
	/*--------取文本_高级表格Ex--------*/
	StringEx AdvancedFormEx::GetItemTitle(AutoInt index, AutoInt cloid)
	{
		return GetItemTitle_AdvancedFormEx(control, index, cloid);
	}
	/*--------置字体色_高级表格Ex--------*/
	void AdvancedFormEx::SetItemFontColor(AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn, int FontColor)
	{
		SetItemFontColor_AdvancedFormEx(control, StarRow, StarColumn, EndRow, EndColumn, FontColor);
	}
	/*--------取字体色_高级表格Ex--------*/
	int AdvancedFormEx::GetItemFontColor(AutoInt index, AutoInt cloid)
	{
		return GetItemFontColor_AdvancedFormEx(control, index, cloid);
	}
	/*--------置项目高度_高级表格Ex--------*/
	void AdvancedFormEx::SetItemHeight(AutoInt StarRow, AutoInt EndRow, int Height)
	{
		SetItemHeight_AdvancedFormEx(control, StarRow, EndRow, Height);
	}
	/*--------取项目高度_高级表格Ex--------*/
	int AdvancedFormEx::GetItemHeight(AutoInt index)
	{
		return GetItemHeight_AdvancedFormEx(control, index);
	}
	/*--------置项目最大高度_高级表格Ex--------*/
	void AdvancedFormEx::SetItemMaxHeight(AutoInt StarRow, AutoInt EndRow, int Height)
	{
		SetItemMaxHeight_AdvancedFormEx(control, StarRow, EndRow, Height);
	}
	/*--------取项目最大高度_高级表格Ex--------*/
	int AdvancedFormEx::GetItemMaxHeight(AutoInt index)
	{
		return GetItemMaxHeight_AdvancedFormEx(control, index);
	}
	/*--------置项目最小高度_高级表格Ex--------*/
	void AdvancedFormEx::SetItemMinHeight(AutoInt StarRow, AutoInt EndRow, int Height)
	{
		SetItemMinHeight_AdvancedFormEx(control, StarRow, EndRow, Height);
	}
	/*--------取项目最小高度_高级表格Ex--------*/
	int AdvancedFormEx::GetItemMinHeight(AutoInt index)
	{
		return GetItemMinHeight_AdvancedFormEx(control, index);
	}
	/*--------合并单元格_高级表格Ex--------*/
	void AdvancedFormEx::MergeCell(AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn)
	{
		MergeCell_AdvancedFormEx(control, StarRow, StarColumn, EndRow, EndColumn);
	}
	/*--------分解单元格_高级表格Ex--------*/
	void AdvancedFormEx::BreakCell(AutoInt StarRow, AutoInt StarColumn)
	{
		BreakCell_AdvancedFormEx(control, StarRow, StarColumn);
	}
	/*--------取单元格合并信息_高级表格Ex--------*/
	AutoInt AdvancedFormEx::GetMergeInfo(AutoInt StarRow, AutoInt StarColumn, int Type)
	{
		return GetMergeInfo_AdvancedFormEx(control, StarRow, StarColumn, Type);
	}
	/*--------置字体_高级表格Ex--------*/
	void AdvancedFormEx::SetItemFont(AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn, BindataEx Font)
	{
		SetItemFont_AdvancedFormEx(control, StarRow, StarColumn, EndRow, EndColumn, Font);
	}
	/*--------取字体_高级表格Ex--------*/
	BindataEx AdvancedFormEx::GetItemFont(AutoInt index, AutoInt cloid)
	{
		return GetItemFont_AdvancedFormEx(control, index, cloid);
	}
	/*--------置皮肤_高级表格Ex--------*/
	void AdvancedFormEx::SetItemSkin(AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn, BindataEx Skin)
	{
		SetItemSkin_AdvancedFormEx(control, StarRow, StarColumn, EndRow, EndColumn, Skin);
	}
	/*--------取皮肤_高级表格Ex--------*/
	BindataEx AdvancedFormEx::GetItemSkin(AutoInt index, AutoInt cloid)
	{
		return GetItemSkin_AdvancedFormEx(control, index, cloid);
	}
	/*--------置对齐方式_高级表格Ex--------*/
	void AdvancedFormEx::SetItemAlign(AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn, int Align)
	{
		SetItemAlign_AdvancedFormEx(control, StarRow, StarColumn, EndRow, EndColumn, Align);
	}
	/*--------取对齐方式_高级表格Ex--------*/
	int AdvancedFormEx::GetItemAlign(AutoInt index, AutoInt cloid)
	{
		return GetItemAlign_AdvancedFormEx(control, index, cloid);
	}
	/*--------置选中状态_高级表格Ex--------*/
	void AdvancedFormEx::SetItemSelected(AutoInt StarRow, AutoInt StarColumn, AutoInt EndRow, AutoInt EndColumn, BOOL Select)
	{
		SetItemSelected_AdvancedFormEx(control, StarRow, StarColumn, EndRow, EndColumn, Select);
	}
	/*--------取选中状态_高级表格Ex--------*/
	BOOL AdvancedFormEx::GetItemSelected(AutoInt index, AutoInt cloid)
	{
		return GetItemSelected_AdvancedFormEx(control, index, cloid);
	}
	/*--------置项目风格_高级表格Ex--------*/
	void AdvancedFormEx::SetItemType(AutoInt StarRow, AutoInt EndRow, int style)
	{
		SetItemType_AdvancedFormEx(control, StarRow, EndRow, style);
	}
	/*--------取项目风格_高级表格Ex--------*/
	int AdvancedFormEx::GetItemType(AutoInt index)
	{
		return GetItemType_AdvancedFormEx(control, index);
	}
	/*--------置折叠状态_高级表格Ex--------*/
	void AdvancedFormEx::SetItemFold(AutoInt StarRow, AutoInt EndRow, BOOL Fold)
	{
		SetItemFold_AdvancedFormEx(control, StarRow, EndRow, Fold);
	}
	/*--------取折叠状态_高级表格Ex--------*/
	BOOL AdvancedFormEx::GetItemFold(AutoInt index)
	{
		return GetItemFold_AdvancedFormEx(control, index);
	}
	/*--------取缩进层次_高级表格Ex--------*/
	int AdvancedFormEx::GetItemLevel(AutoInt index)
	{
		return GetItemLevel_AdvancedFormEx(control, index);
	}
	/*--------取子项目数_高级表格Ex--------*/
	int AdvancedFormEx::GetSubItemCount(AutoInt index)
	{
		return GetSubItemCount_AdvancedFormEx(control, index);
	}
	/*--------取下一级子项目数_高级表格Ex--------*/
	int AdvancedFormEx::GetNextSubItemCount(AutoInt index)
	{
		return GetNextSubItemCount_AdvancedFormEx(control, index);
	}
	/*--------取祖宗项目_高级表格Ex--------*/
	int AdvancedFormEx::GetAncestorItem(AutoInt index)
	{
		return GetAncestorItem_AdvancedFormEx(control, index);
	}
	/*--------取父项目_高级表格Ex--------*/
	int AdvancedFormEx::GetFatherItem(AutoInt index)
	{
		return GetFatherItem_AdvancedFormEx(control, index);
	}
	/*--------取上一同层项目_高级表格Ex--------*/
	int AdvancedFormEx::GetLastItem(AutoInt index)
	{
		return GetLastItem_AdvancedFormEx(control, index);
	}
	/*--------取下一同层项目_高级表格Ex--------*/
	int AdvancedFormEx::GetNextItem(AutoInt index)
	{
		return GetNextItem_AdvancedFormEx(control, index);
	}
	/*--------置项目编辑配置_高级表格Ex--------*/
	void AdvancedFormEx::SetItemEditConfig(int AttributeId, int InputMode, int MaxInput, StringEx PasswordSubstitution, int Align, int Multiline, int Wrapped, BindataEx Skin, BindataEx Cursor, BindataEx Font, int FontClour, int SelectedFontColor, int SelectedColor, int CursorColor, int ScrollBarMode, int MenuTableWidth, int MenuTableHeight, BindataEx MenuFont, int MenuFontClour, int MenuDisabledFontClour, int MenuTransparency, int MenuLanguage)
	{
		SetItemEditConfig_AdvancedFormEx(control, AttributeId, InputMode, MaxInput, PasswordSubstitution, Align, Multiline, Wrapped, Skin, Cursor, Font, FontClour, SelectedFontColor, SelectedColor, CursorColor, ScrollBarMode, MenuTableWidth, MenuTableHeight, MenuFont, MenuFontClour, MenuDisabledFontClour, MenuTransparency, MenuLanguage);
	}
	/*--------取项目编辑配置_高级表格Ex--------*/
	AutoInt AdvancedFormEx::GetItemEditConfig(int AttributeId)
	{
		return GetItemEditConfig_AdvancedFormEx(control, AttributeId);
	}
	/*--------进入编辑_高级表格Ex--------*/
	void AdvancedFormEx::EnterEdit(AutoInt index, AutoInt cloid, AutoInt mode)
	{
		EnterEdit_AdvancedFormEx(control, index, cloid, mode);
	}
	/*--------退出编辑_高级表格Ex--------*/
	void AdvancedFormEx::ExitEdit(AutoInt index, AutoInt cloid, AutoInt mode)
	{
		ExitEdit_AdvancedFormEx(control, index, cloid, mode);
	}
	/*--------置滚动回调_高级表格Ex--------*/
	void AdvancedFormEx::SetScrollCallBack(ExuiCallback Callback)
	{
		SetScrollCallBack_AdvancedFormEx(control, Callback);
	}
	/*--------取滚动回调_高级表格Ex--------*/
	ExuiCallback AdvancedFormEx::GetScrollCallBack()
	{
		return GetScrollCallBack_AdvancedFormEx(control);
	}
	/*--------置虚表回调_高级表格Ex--------*/
	void AdvancedFormEx::SetVirtualTableCallBack(ExuiCallback Callback)
	{
		SetVirtualTableCallBack_AdvancedFormEx(control, Callback);
	}
	/*--------取虚表回调_高级表格Ex--------*/
	ExuiCallback AdvancedFormEx::GetVirtualTableCallBack()
	{
		return GetVirtualTableCallBack_AdvancedFormEx(control);
	}
	/*--------保证显示_高级表格Ex--------*/
	void AdvancedFormEx::GuaranteeVisible(AutoInt index, AutoInt cid, int mode)
	{
		GuaranteeVisible_AdvancedFormEx(control, index, cid, mode);
	}

	/*----------组件高级表格EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx AdvancedFormEx::Skin()
	{
		return (BinEx_P)GetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void AdvancedFormEx::Skin(BindataEx Skin)
	{
		SetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取现行选中项--------*/
	int AdvancedFormEx::CurrentItem()
	{
		return (int)GetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_CurrentItem);
	}
	/*--------置现行选中项--------*/
	void AdvancedFormEx::CurrentItem(int CurrentItem)
	{
		SetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_CurrentItem, (AutoInt)CurrentItem);
	}
	/*--------取现行选中列--------*/
	int AdvancedFormEx::CurrentColumn()
	{
		return (int)GetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_CurrentColumn);
	}
	/*--------置现行选中列--------*/
	void AdvancedFormEx::CurrentColumn(int CurrentColumn)
	{
		SetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_CurrentColumn, (AutoInt)CurrentColumn);
	}
	/*--------取表头行数--------*/
	int AdvancedFormEx::HeaderRowNum()
	{
		return (int)GetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_HeaderRowNum);
	}
	/*--------置表头行数--------*/
	void AdvancedFormEx::HeaderRowNum(int HeaderRowNum)
	{
		SetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_HeaderRowNum, (AutoInt)HeaderRowNum);
	}
	/*--------取表头列数--------*/
	int AdvancedFormEx::HeaderColumnNum()
	{
		return (int)GetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_HeaderColumnNum);
	}
	/*--------置表头列数--------*/
	void AdvancedFormEx::HeaderColumnNum(int HeaderColumnNum)
	{
		SetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_HeaderColumnNum, (AutoInt)HeaderColumnNum);
	}
	/*--------取底悬浮行数--------*/
	int AdvancedFormEx::BottomFixedRow()
	{
		return (int)GetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_BottomFixedRow);
	}
	/*--------置底悬浮行数--------*/
	void AdvancedFormEx::BottomFixedRow(int BottomFixedRow)
	{
		SetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_BottomFixedRow, (AutoInt)BottomFixedRow);
	}
	/*--------取右悬浮列数--------*/
	int AdvancedFormEx::RightFixedColumn()
	{
		return (int)GetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_RightFixedColumn);
	}
	/*--------置右悬浮列数--------*/
	void AdvancedFormEx::RightFixedColumn(int RightFixedColumn)
	{
		SetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_RightFixedColumn, (AutoInt)RightFixedColumn);
	}
	/*--------取调整模式--------*/
	int AdvancedFormEx::AdjustmentMode()
	{
		return (int)GetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_AdjustmentMode);
	}
	/*--------置调整模式--------*/
	void AdvancedFormEx::AdjustmentMode(int AdjustmentMode)
	{
		SetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_AdjustmentMode, (AutoInt)AdjustmentMode);
	}
	/*--------取选中模式--------*/
	int AdvancedFormEx::SelectMode()
	{
		return (int)GetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_SelectMode);
	}
	/*--------置选中模式--------*/
	void AdvancedFormEx::SelectMode(int SelectMode)
	{
		SetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_SelectMode, (AutoInt)SelectMode);
	}
	/*--------取整行选择--------*/
	BOOL AdvancedFormEx::EntireLine()
	{
		return (BOOL)GetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_EntireLine);
	}
	/*--------置整行选择--------*/
	void AdvancedFormEx::EntireLine(BOOL EntireLine)
	{
		SetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_EntireLine, (AutoInt)EntireLine);
	}
	/*--------取事件模式--------*/
	int AdvancedFormEx::EventMode()
	{
		return (int)GetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_EventMode);
	}
	/*--------置事件模式--------*/
	void AdvancedFormEx::EventMode(int EventMode)
	{
		SetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_EventMode, (AutoInt)EventMode);
	}
	/*--------取编辑模式--------*/
	int AdvancedFormEx::EditMode()
	{
		return (int)GetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_EditMode);
	}
	/*--------置编辑模式--------*/
	void AdvancedFormEx::EditMode(int EditMode)
	{
		SetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_EditMode, (AutoInt)EditMode);
	}
	/*--------取树形表格--------*/
	BOOL AdvancedFormEx::TreeType()
	{
		return (BOOL)GetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_TreeType);
	}
	/*--------置树形表格--------*/
	void AdvancedFormEx::TreeType(BOOL TreeType)
	{
		SetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_TreeType, (AutoInt)TreeType);
	}
	/*--------取表格线模式--------*/
	int AdvancedFormEx::LineMode()
	{
		return (int)GetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_LineMode);
	}
	/*--------置表格线模式--------*/
	void AdvancedFormEx::LineMode(int LineMode)
	{
		SetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_LineMode, (AutoInt)LineMode);
	}
	/*--------取滚动条方式--------*/
	int AdvancedFormEx::ScrollBarMode()
	{
		return (int)GetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_ScrollBarMode);
	}
	/*--------置滚动条方式--------*/
	void AdvancedFormEx::ScrollBarMode(int ScrollBarMode)
	{
		SetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_ScrollBarMode, (AutoInt)ScrollBarMode);
	}
	/*--------取项目数据--------*/
	BindataEx AdvancedFormEx::ItemData()
	{
		return (BinEx_P)GetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_ItemData);
	}
	/*--------置项目数据--------*/
	void AdvancedFormEx::ItemData(BindataEx ItemData)
	{
		SetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_ItemData, (AutoInt)(BinEx_P)ItemData);
	}
	/*--------取扩展元素--------*/
	BindataEx AdvancedFormEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void AdvancedFormEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx AdvancedFormEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void AdvancedFormEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_AdvancedFormEx(control, AdvancedFormEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件高级表格EX属性读写函数结束--------*/

}
//BarChartEx
namespace exui
{
	BarChartEx::BarChartEx()
	{

	}
	BarChartEx::~BarChartEx()
	{

	}
	/*--------创建组件_柱状图Ex--------*/
	BarChartEx_P BarChartEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, int ChartStyle, StringEx TemplateH, int ColorHA, int GridStyleH, int GridColorH, BindataEx FontH, int FontColorH, StringEx TemplateV, int MiniV, int MaxV, int ColorVA, int GridStyleV, int GridColorV, BindataEx FontV, int FontColorV, int Leftretain, int Topretain, int Rightretain, int bottomretain, AutoInt hotcolumn, int GraphWidth, int GraphSpace, BindataEx GraphData, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_BarChartEx);
		CreateControl_BarChartEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, ChartStyle, TemplateH, ColorHA, GridStyleH, GridColorH, FontH, FontColorH, TemplateV, MiniV, MaxV, ColorVA, GridStyleV, GridColorV, FontV, FontColorV, Leftretain, Topretain, Rightretain, bottomretain, hotcolumn, GraphWidth, GraphSpace, GraphData, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_柱状图Ex--------*/
	void BarChartEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_BarChartEx(control, index, attribute);
	}
	/*--------取属性_柱状图Ex--------*/
	AutoInt BarChartEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_BarChartEx(control, index);
	}
	/*--------置图例数量_柱状图Ex--------*/
	void BarChartEx::SetGraphCount(AutoInt Count)
	{
		SetGraphCount_BarChartEx(control, Count);
	}
	/*--------取图例数量_柱状图Ex--------*/
	AutoInt BarChartEx::GetGraphCount()
	{
		return GetGraphCount_BarChartEx(control);
	}
	/*--------置图例属性_柱状图Ex--------*/
	void BarChartEx::SetGraphAttr(AutoInt index, AutoInt AttrId, int Colour, int Rgn)
	{
		SetGraphAttr_BarChartEx(control, index, AttrId, Colour, Rgn);
	}
	/*--------取图例属性_柱状图Ex--------*/
	AutoInt BarChartEx::GetGraphAttr(AutoInt index, AutoInt AttrId)
	{
		return GetGraphAttr_BarChartEx(control, index, AttrId);
	}
	/*--------置图例值_柱状图Ex--------*/
	void BarChartEx::SetGraphValue(AutoInt index, AutoInt Cid, int Value)
	{
		SetGraphValue_BarChartEx(control, index, Cid, Value);
	}
	/*--------取图例值_柱状图Ex--------*/
	int BarChartEx::GetGraphValue(AutoInt index, AutoInt Cid)
	{
		return GetGraphValue_BarChartEx(control, index, Cid);
	}

	/*----------组件柱状图EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx BarChartEx::Skin()
	{
		return (BinEx_P)GetAttribute_BarChartEx(control, BarChartEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void BarChartEx::Skin(BindataEx Skin)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取图表风格--------*/
	int BarChartEx::ChartStyle()
	{
		return (int)GetAttribute_BarChartEx(control, BarChartEx_Attr_ChartStyle);
	}
	/*--------置图表风格--------*/
	void BarChartEx::ChartStyle(int ChartStyle)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_ChartStyle, (AutoInt)ChartStyle);
	}
	/*--------取横轴标题模版--------*/
	StringEx BarChartEx::TemplateH()
	{
		return (StrEx_P)GetAttribute_BarChartEx(control, BarChartEx_Attr_TemplateH);
	}
	/*--------置横轴标题模版--------*/
	void BarChartEx::TemplateH(StringEx TemplateH)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_TemplateH, (AutoInt)(StrEx_P)TemplateH);
	}
	/*--------取横轴颜色--------*/
	int BarChartEx::ColorHA()
	{
		return (int)GetAttribute_BarChartEx(control, BarChartEx_Attr_ColorHA);
	}
	/*--------置横轴颜色--------*/
	void BarChartEx::ColorHA(int ColorHA)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_ColorHA, (AutoInt)ColorHA);
	}
	/*--------取横轴网格类型--------*/
	int BarChartEx::GridStyleH()
	{
		return (int)GetAttribute_BarChartEx(control, BarChartEx_Attr_GridStyleH);
	}
	/*--------置横轴网格类型--------*/
	void BarChartEx::GridStyleH(int GridStyleH)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_GridStyleH, (AutoInt)GridStyleH);
	}
	/*--------取横轴网格色--------*/
	int BarChartEx::GridColorH()
	{
		return (int)GetAttribute_BarChartEx(control, BarChartEx_Attr_GridColorH);
	}
	/*--------置横轴网格色--------*/
	void BarChartEx::GridColorH(int GridColorH)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_GridColorH, (AutoInt)GridColorH);
	}
	/*--------取横轴字体--------*/
	BindataEx BarChartEx::FontH()
	{
		return (BinEx_P)GetAttribute_BarChartEx(control, BarChartEx_Attr_FontH);
	}
	/*--------置横轴字体--------*/
	void BarChartEx::FontH(BindataEx FontH)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_FontH, (AutoInt)(BinEx_P)(BinEx_P)FontH);
	}
	/*--------取横轴字体色--------*/
	int BarChartEx::FontColorH()
	{
		return (int)GetAttribute_BarChartEx(control, BarChartEx_Attr_FontColorH);
	}
	/*--------置横轴字体色--------*/
	void BarChartEx::FontColorH(int FontColorH)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_FontColorH, (AutoInt)FontColorH);
	}
	/*--------取纵轴标题模版--------*/
	StringEx BarChartEx::TemplateV()
	{
		return (StrEx_P)GetAttribute_BarChartEx(control, BarChartEx_Attr_TemplateV);
	}
	/*--------置纵轴标题模版--------*/
	void BarChartEx::TemplateV(StringEx TemplateV)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_TemplateV, (AutoInt)(StrEx_P)TemplateV);
	}
	/*--------取纵轴最小值--------*/
	int BarChartEx::MiniV()
	{
		return (int)GetAttribute_BarChartEx(control, BarChartEx_Attr_MiniV);
	}
	/*--------置纵轴最小值--------*/
	void BarChartEx::MiniV(int MiniV)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_MiniV, (AutoInt)MiniV);
	}
	/*--------取纵轴最大值--------*/
	int BarChartEx::MaxV()
	{
		return (int)GetAttribute_BarChartEx(control, BarChartEx_Attr_MaxV);
	}
	/*--------置纵轴最大值--------*/
	void BarChartEx::MaxV(int MaxV)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_MaxV, (AutoInt)MaxV);
	}
	/*--------取纵轴颜色--------*/
	int BarChartEx::ColorVA()
	{
		return (int)GetAttribute_BarChartEx(control, BarChartEx_Attr_ColorVA);
	}
	/*--------置纵轴颜色--------*/
	void BarChartEx::ColorVA(int ColorVA)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_ColorVA, (AutoInt)ColorVA);
	}
	/*--------取纵轴网格类型--------*/
	int BarChartEx::GridStyleV()
	{
		return (int)GetAttribute_BarChartEx(control, BarChartEx_Attr_GridStyleV);
	}
	/*--------置纵轴网格类型--------*/
	void BarChartEx::GridStyleV(int GridStyleV)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_GridStyleV, (AutoInt)GridStyleV);
	}
	/*--------取纵轴网格色--------*/
	int BarChartEx::GridColorV()
	{
		return (int)GetAttribute_BarChartEx(control, BarChartEx_Attr_GridColorV);
	}
	/*--------置纵轴网格色--------*/
	void BarChartEx::GridColorV(int GridColorV)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_GridColorV, (AutoInt)GridColorV);
	}
	/*--------取纵轴字体--------*/
	BindataEx BarChartEx::FontV()
	{
		return (BinEx_P)GetAttribute_BarChartEx(control, BarChartEx_Attr_FontV);
	}
	/*--------置纵轴字体--------*/
	void BarChartEx::FontV(BindataEx FontV)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_FontV, (AutoInt)(BinEx_P)FontV);
	}
	/*--------取纵轴字体色--------*/
	int BarChartEx::FontColorV()
	{
		return (int)GetAttribute_BarChartEx(control, BarChartEx_Attr_FontColorV);
	}
	/*--------置纵轴字体色--------*/
	void BarChartEx::FontColorV(int FontColorV)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_FontColorV, (AutoInt)FontColorV);
	}
	/*--------取左预留--------*/
	int BarChartEx::LeftReserve()
	{
		return (int)GetAttribute_BarChartEx(control, BarChartEx_Attr_LeftReserve);
	}
	/*--------置左预留--------*/
	void BarChartEx::LeftReserve(int LeftReserve)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_LeftReserve, (AutoInt)LeftReserve);
	}
	/*--------取顶预留--------*/
	int BarChartEx::TopReserve()
	{
		return (int)GetAttribute_BarChartEx(control, BarChartEx_Attr_TopReserve);
	}
	/*--------置顶预留--------*/
	void BarChartEx::TopReserve(int TopReserve)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_TopReserve, (AutoInt)TopReserve);
	}
	/*--------取右预留--------*/
	int BarChartEx::RightReserve()
	{
		return (int)GetAttribute_BarChartEx(control, BarChartEx_Attr_RightReserve);
	}
	/*--------置右预留--------*/
	void BarChartEx::RightReserve(int RightReserve)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_RightReserve, (AutoInt)RightReserve);
	}
	/*--------取底预留--------*/
	int BarChartEx::BottomReserve()
	{
		return (int)GetAttribute_BarChartEx(control, BarChartEx_Attr_BottomReserve);
	}
	/*--------置底预留--------*/
	void BarChartEx::BottomReserve(int BottomReserve)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_BottomReserve, (AutoInt)BottomReserve);
	}
	/*--------取热点图列--------*/
	int BarChartEx::HotColumn()
	{
		return (int)GetAttribute_BarChartEx(control, BarChartEx_Attr_HotColumn);
	}
	/*--------置热点图列--------*/
	void BarChartEx::HotColumn(int HotColumn)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_HotColumn, (AutoInt)HotColumn);
	}
	/*--------取图例宽度--------*/
	int BarChartEx::GraphWidth()
	{
		return (int)GetAttribute_BarChartEx(control, BarChartEx_Attr_GraphWidth);
	}
	/*--------置图例宽度--------*/
	void BarChartEx::GraphWidth(int GraphWidth)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_GraphWidth, (AutoInt)GraphWidth);
	}
	/*--------取图例间距--------*/
	int BarChartEx::GraphSpace()
	{
		return (int)GetAttribute_BarChartEx(control, BarChartEx_Attr_GraphSpace);
	}
	/*--------置图例间距--------*/
	void BarChartEx::GraphSpace(int GraphSpace)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_GraphSpace, (AutoInt)GraphSpace);
	}
	/*--------取图例管理--------*/
	BindataEx BarChartEx::GraphData()
	{
		return (BinEx_P)GetAttribute_BarChartEx(control, BarChartEx_Attr_GraphData);
	}
	/*--------置图例管理--------*/
	void BarChartEx::GraphData(BindataEx GraphData)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_GraphData, (AutoInt)(BinEx_P)GraphData);
	}
	/*--------取扩展元素--------*/
	BindataEx BarChartEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_BarChartEx(control, BarChartEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void BarChartEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx BarChartEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_BarChartEx(control, BarChartEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void BarChartEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_BarChartEx(control, BarChartEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件柱状图EX属性读写函数结束--------*/

}
//CurveChartEx
namespace exui
{
	CurveChartEx::CurveChartEx()
	{

	}
	CurveChartEx::~CurveChartEx()
	{

	}
	/*--------创建组件_曲线图Ex--------*/
	CurveChartEx_P CurveChartEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, int ChartStyle, StringEx TemplateH, int ColorHA, int GridStyleH, int GridColorH, BindataEx FontH, int FontColorH, StringEx TemplateV, int MiniV, int MaxV, int ColorVA, int GridStyleV, int GridColorV, BindataEx FontV, int FontColorV, int Leftretain, int Topretain, int Rightretain, int bottomretain, AutoInt hotcolumn, BindataEx GraphData, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_CurveChartEx);
		CreateControl_CurveChartEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, ChartStyle, TemplateH, ColorHA, GridStyleH, GridColorH, FontH, FontColorH, TemplateV, MiniV, MaxV, ColorVA, GridStyleV, GridColorV, FontV, FontColorV, Leftretain, Topretain, Rightretain, bottomretain, hotcolumn, GraphData, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_曲线图Ex--------*/
	void CurveChartEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_CurveChartEx(control, index, attribute);
	}
	/*--------取属性_曲线图Ex--------*/
	AutoInt CurveChartEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_CurveChartEx(control, index);
	}
	/*--------置图例数量_曲线图Ex--------*/
	void CurveChartEx::SetGraphCount(AutoInt Count)
	{
		SetGraphCount_CurveChartEx(control, Count);
	}
	/*--------取图例数量_曲线图Ex--------*/
	AutoInt CurveChartEx::GetGraphCount()
	{
		return GetGraphCount_CurveChartEx(control);
	}
	/*--------置图例属性_曲线图Ex--------*/
	void CurveChartEx::SetGraphAttr(AutoInt index, AutoInt AttrId, int Colour, int Tension, int width, int style, int PointSize, int HotPointSize)
	{
		SetGraphAttr_CurveChartEx(control, index, AttrId, Colour, Tension, width, style, PointSize, HotPointSize);
	}
	/*--------取图例属性_曲线图Ex--------*/
	AutoInt CurveChartEx::GetGraphAttr(AutoInt index, AutoInt AttrId)
	{
		return GetGraphAttr_CurveChartEx(control, index, AttrId);
	}
	/*--------置图例值_曲线图Ex--------*/
	void CurveChartEx::SetGraphValue(AutoInt index, AutoInt Cid, int Value)
	{
		SetGraphValue_CurveChartEx(control, index, Cid, Value);
	}
	/*--------取图例值_曲线图Ex--------*/
	int CurveChartEx::GetGraphValue(AutoInt index, AutoInt Cid)
	{
		return GetGraphValue_CurveChartEx(control, index, Cid);
	}

	/*----------组件曲线图Ex属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx CurveChartEx::Skin()
	{
		return (BinEx_P)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void CurveChartEx::Skin(BindataEx Skin)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取图表风格--------*/
	int CurveChartEx::ChartStyle()
	{
		return (int)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_ChartStyle);
	}
	/*--------置图表风格--------*/
	void CurveChartEx::ChartStyle(int ChartStyle)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_ChartStyle, (AutoInt)ChartStyle);
	}
	/*--------取横轴标题模版--------*/
	StringEx CurveChartEx::TemplateH()
	{
		return (StrEx_P)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_TemplateH);
	}
	/*--------置横轴标题模版--------*/
	void CurveChartEx::TemplateH(StringEx TemplateH)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_TemplateH, (AutoInt)(StrEx_P)TemplateH);
	}
	/*--------取横轴颜色--------*/
	int CurveChartEx::ColorHA()
	{
		return (int)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_ColorHA);
	}
	/*--------置横轴颜色--------*/
	void CurveChartEx::ColorHA(int ColorHA)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_ColorHA, (AutoInt)ColorHA);
	}
	/*--------取横轴网格类型--------*/
	int CurveChartEx::GridStyleH()
	{
		return (int)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_GridStyleH);
	}
	/*--------置横轴网格类型--------*/
	void CurveChartEx::GridStyleH(int GridStyleH)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_GridStyleH, (AutoInt)GridStyleH);
	}
	/*--------取横轴网格色--------*/
	int CurveChartEx::GridColorH()
	{
		return (int)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_GridColorH);
	}
	/*--------置横轴网格色--------*/
	void CurveChartEx::GridColorH(int GridColorH)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_GridColorH, (AutoInt)GridColorH);
	}
	/*--------取横轴字体--------*/
	BindataEx CurveChartEx::FontH()
	{
		return (BinEx_P)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_FontH);
	}
	/*--------置横轴字体--------*/
	void CurveChartEx::FontH(BindataEx FontH)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_FontH, (AutoInt)(BinEx_P)FontH);
	}
	/*--------取横轴字体色--------*/
	int CurveChartEx::FontColorH()
	{
		return (int)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_FontColorH);
	}
	/*--------置横轴字体色--------*/
	void CurveChartEx::FontColorH(int FontColorH)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_FontColorH, (AutoInt)FontColorH);
	}
	/*--------取纵轴标题模版--------*/
	StringEx CurveChartEx::TemplateV()
	{
		return (StrEx_P)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_TemplateV);
	}
	/*--------置纵轴标题模版--------*/
	void CurveChartEx::TemplateV(StringEx TemplateV)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_TemplateV, (AutoInt)(StrEx_P)TemplateV);
	}
	/*--------取纵轴最小值--------*/
	int CurveChartEx::MiniV()
	{
		return (int)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_MiniV);
	}
	/*--------置纵轴最小值--------*/
	void CurveChartEx::MiniV(int MiniV)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_MiniV, (AutoInt)MiniV);
	}
	/*--------取纵轴最大值--------*/
	int CurveChartEx::MaxV()
	{
		return (int)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_MaxV);
	}
	/*--------置纵轴最大值--------*/
	void CurveChartEx::MaxV(int MaxV)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_MaxV, (AutoInt)MaxV);
	}
	/*--------取纵轴颜色--------*/
	int CurveChartEx::ColorVA()
	{
		return (int)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_ColorVA);
	}
	/*--------置纵轴颜色--------*/
	void CurveChartEx::ColorVA(int ColorVA)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_ColorVA, (AutoInt)ColorVA);
	}
	/*--------取纵轴网格类型--------*/
	int CurveChartEx::GridStyleV()
	{
		return (int)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_GridStyleV);
	}
	/*--------置纵轴网格类型--------*/
	void CurveChartEx::GridStyleV(int GridStyleV)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_GridStyleV, (AutoInt)GridStyleV);
	}
	/*--------取纵轴网格色--------*/
	int CurveChartEx::GridColorV()
	{
		return (int)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_GridColorV);
	}
	/*--------置纵轴网格色--------*/
	void CurveChartEx::GridColorV(int GridColorV)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_GridColorV, (AutoInt)GridColorV);
	}
	/*--------取纵轴字体--------*/
	BindataEx CurveChartEx::FontV()
	{
		return (BinEx_P)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_FontV);
	}
	/*--------置纵轴字体--------*/
	void CurveChartEx::FontV(BindataEx FontV)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_FontV, (AutoInt)(BinEx_P)FontV);
	}
	/*--------取纵轴字体色--------*/
	int CurveChartEx::FontColorV()
	{
		return (int)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_FontColorV);
	}
	/*--------置纵轴字体色--------*/
	void CurveChartEx::FontColorV(int FontColorV)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_FontColorV, (AutoInt)FontColorV);
	}
	/*--------取左预留--------*/
	int CurveChartEx::LeftReserve()
	{
		return (int)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_LeftReserve);
	}
	/*--------置左预留--------*/
	void CurveChartEx::LeftReserve(int LeftReserve)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_LeftReserve, (AutoInt)LeftReserve);
	}
	/*--------取顶预留--------*/
	int CurveChartEx::TopReserve()
	{
		return (int)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_TopReserve);
	}
	/*--------置顶预留--------*/
	void CurveChartEx::TopReserve(int TopReserve)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_TopReserve, (AutoInt)TopReserve);
	}
	/*--------取右预留--------*/
	int CurveChartEx::RightReserve()
	{
		return (int)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_RightReserve);
	}
	/*--------置右预留--------*/
	void CurveChartEx::RightReserve(int RightReserve)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_RightReserve, (AutoInt)RightReserve);
	}
	/*--------取底预留--------*/
	int CurveChartEx::BottomReserve()
	{
		return (int)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_BottomReserve);
	}
	/*--------置底预留--------*/
	void CurveChartEx::BottomReserve(int BottomReserve)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_BottomReserve, (AutoInt)BottomReserve);
	}
	/*--------取热点图列--------*/
	int CurveChartEx::HotColumn()
	{
		return (int)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_HotColumn);
	}
	/*--------置热点图列--------*/
	void CurveChartEx::HotColumn(int HotColumn)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_HotColumn, (AutoInt)HotColumn);
	}
	/*--------取图例管理--------*/
	BindataEx CurveChartEx::GraphData()
	{
		return (BinEx_P)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_GraphData);
	}
	/*--------置图例管理--------*/
	void CurveChartEx::GraphData(BindataEx GraphData)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_GraphData, (AutoInt)(BinEx_P)GraphData);
	}
	/*--------取扩展元素--------*/
	BindataEx CurveChartEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void CurveChartEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx CurveChartEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_CurveChartEx(control, CurveChartEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void CurveChartEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_CurveChartEx(control, CurveChartEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件曲线图Ex属性读写函数结束--------*/

}
//PieChartEx
namespace exui
{
	PieChartEx::PieChartEx()
	{

	}
	PieChartEx::~PieChartEx()
	{

	}
	/*--------创建组件_饼形图Ex--------*/
	PieChartEx_P PieChartEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, int Leftretain, int Topretain, int Rightretain, int bottomretain, AutoInt hotGraph, int GraphStar, int GraphWidth, BOOL Zoom, BindataEx ItemData, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_PieChartEx);
		CreateControl_PieChartEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, Leftretain, Topretain, Rightretain, bottomretain, hotGraph, GraphStar, GraphWidth, Zoom, ItemData, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_饼形图Ex--------*/
	void PieChartEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_PieChartEx(control, index, attribute);
	}
	/*--------取属性_饼形图Ex--------*/
	AutoInt PieChartEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_PieChartEx(control, index);
	}
	/*--------置图例数量_饼形图Ex--------*/
	void PieChartEx::SetGraphCount(AutoInt Count)
	{
		SetGraphCount_PieChartEx(control, Count);
	}
	/*--------取图例数量_饼形图Ex--------*/
	AutoInt PieChartEx::GetGraphCount()
	{
		return GetGraphCount_PieChartEx(control);
	}
	/*--------置图例属性_饼形图Ex--------*/
	void PieChartEx::SetGraphAttr(AutoInt index, AutoInt AttrId, int color)
	{
		SetGraphAttr_PieChartEx(control, index, AttrId, color);
	}
	/*--------取图例属性_饼形图Ex--------*/
	AutoInt PieChartEx::GetGraphAttr(AutoInt index, AutoInt AttrId)
	{
		return GetGraphAttr_PieChartEx(control, index, AttrId);
	}
	/*--------置图例值_饼形图Ex--------*/
	void PieChartEx::SetGraphValue(AutoInt index, int Value)
	{
		SetGraphValue_PieChartEx(control, index, Value);
	}
	/*--------取图例值_饼形图Ex--------*/
	int PieChartEx::GetGraphValue(AutoInt index)
	{
		return GetGraphValue_PieChartEx(control, index);
	}

	/*----------组件饼形图EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx PieChartEx::Skin()
	{
		return (BinEx_P)GetAttribute_PieChartEx(control, PieChartEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void PieChartEx::Skin(BindataEx Skin)
	{
		SetAttribute_PieChartEx(control, PieChartEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取左预留--------*/
	int PieChartEx::LeftReserve()
	{
		return (int)GetAttribute_PieChartEx(control, PieChartEx_Attr_LeftReserve);
	}
	/*--------置左预留--------*/
	void PieChartEx::LeftReserve(int LeftReserve)
	{
		SetAttribute_PieChartEx(control, PieChartEx_Attr_LeftReserve, (AutoInt)LeftReserve);
	}
	/*--------取顶预留--------*/
	int PieChartEx::TopReserve()
	{
		return (int)GetAttribute_PieChartEx(control, PieChartEx_Attr_TopReserve);
	}
	/*--------置顶预留--------*/
	void PieChartEx::TopReserve(int TopReserve)
	{
		SetAttribute_PieChartEx(control, PieChartEx_Attr_TopReserve, (AutoInt)TopReserve);
	}
	/*--------取右预留--------*/
	int PieChartEx::RightReserve()
	{
		return (int)GetAttribute_PieChartEx(control, PieChartEx_Attr_RightReserve);
	}
	/*--------置右预留--------*/
	void PieChartEx::RightReserve(int RightReserve)
	{
		SetAttribute_PieChartEx(control, PieChartEx_Attr_RightReserve, (AutoInt)RightReserve);
	}
	/*--------取底预留--------*/
	int PieChartEx::BottomReserve()
	{
		return (int)GetAttribute_PieChartEx(control, PieChartEx_Attr_BottomReserve);
	}
	/*--------置底预留--------*/
	void PieChartEx::BottomReserve(int BottomReserve)
	{
		SetAttribute_PieChartEx(control, PieChartEx_Attr_BottomReserve, (AutoInt)BottomReserve);
	}
	/*--------取热点图例--------*/
	int PieChartEx::HotGraph()
	{
		return (int)GetAttribute_PieChartEx(control, PieChartEx_Attr_HotGraph);
	}
	/*--------置热点图例--------*/
	void PieChartEx::HotGraph(int HotGraph)
	{
		SetAttribute_PieChartEx(control, PieChartEx_Attr_HotGraph, (AutoInt)HotGraph);
	}
	/*--------取图例起始度--------*/
	int PieChartEx::GraphStar()
	{
		return (int)GetAttribute_PieChartEx(control, PieChartEx_Attr_GraphStar);
	}
	/*--------置图例起始度--------*/
	void PieChartEx::GraphStar(int GraphStar)
	{
		SetAttribute_PieChartEx(control, PieChartEx_Attr_GraphStar, (AutoInt)GraphStar);
	}
	/*--------取图例宽度--------*/
	int PieChartEx::GraphWidth()
	{
		return (int)GetAttribute_PieChartEx(control, PieChartEx_Attr_GraphWidth);
	}
	/*--------置图例宽度--------*/
	void PieChartEx::GraphWidth(int GraphWidth)
	{
		SetAttribute_PieChartEx(control, PieChartEx_Attr_GraphWidth, (AutoInt)GraphWidth);
	}
	/*--------取缩放--------*/
	BOOL PieChartEx::Zoom()
	{
		return (BOOL)GetAttribute_PieChartEx(control, PieChartEx_Attr_Zoom);
	}
	/*--------置缩放--------*/
	void PieChartEx::Zoom(BOOL Zoom)
	{
		SetAttribute_PieChartEx(control, PieChartEx_Attr_Zoom, (AutoInt)Zoom);
	}
	/*--------取图例管理--------*/
	BindataEx PieChartEx::GraphData()
	{
		return (BinEx_P)GetAttribute_PieChartEx(control, PieChartEx_Attr_GraphData);
	}
	/*--------置图例管理--------*/
	void PieChartEx::GraphData(BindataEx GraphData)
	{
		SetAttribute_PieChartEx(control, PieChartEx_Attr_GraphData, (AutoInt)(BinEx_P)GraphData);
	}
	/*--------取扩展元素--------*/
	BindataEx PieChartEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_PieChartEx(control, PieChartEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void PieChartEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_PieChartEx(control, PieChartEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx PieChartEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_PieChartEx(control, PieChartEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void PieChartEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_PieChartEx(control, PieChartEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件饼形图EX属性读写函数结束--------*/


}
//SlideButtonEx
namespace exui
{
	SlideButtonEx::SlideButtonEx()
	{

	}
	SlideButtonEx::~SlideButtonEx()
	{

	}
	/*--------创建组件_滑动按钮EX--------*/
	SlideButtonEx_P SlideButtonEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, BOOL Selected, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_SlideButtonEx);
		CreateControl_SlideButtonEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, Selected, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_滑动按钮Ex--------*/
	void SlideButtonEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_SlideButtonEx(control, index, attribute);
	}
	/*--------取属性_滑动按钮Ex--------*/
	AutoInt SlideButtonEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_SlideButtonEx(control, index);
	}

	/*----------组件滑动按钮EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx SlideButtonEx::Skin()
	{
		return (BinEx_P)GetAttribute_SlideButtonEx(control, SlideButtonEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void SlideButtonEx::Skin(BindataEx Skin)
	{
		SetAttribute_SlideButtonEx(control, SlideButtonEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取选中--------*/
	BOOL SlideButtonEx::Selected()
	{
		return (BOOL)GetAttribute_SlideButtonEx(control, SlideButtonEx_Attr_Selected);
	}
	/*--------置选中--------*/
	void SlideButtonEx::Selected(BOOL Selected)
	{
		SetAttribute_SlideButtonEx(control, SlideButtonEx_Attr_Selected, (AutoInt)Selected);
	}
	/*--------取扩展元素--------*/
	BindataEx SlideButtonEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_SlideButtonEx(control, SlideButtonEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void SlideButtonEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_SlideButtonEx(control, SlideButtonEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx SlideButtonEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_SlideButtonEx(control, SlideButtonEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void SlideButtonEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_SlideButtonEx(control, SlideButtonEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件滑动按钮EX属性读写函数结束--------*/

}
//CandleChartEx
namespace exui
{
	CandleChartEx::CandleChartEx()
	{

	}
	CandleChartEx::~CandleChartEx()
	{

	}
	/*--------创建组件_烛线图Ex--------*/
	CandleChartEx_P CandleChartEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, int ChartStyle, StringEx TemplateH, int ColorHA, int GridStyleH, int GridColorH, BindataEx FontH, int FontColorH, StringEx TemplateV, int MiniV, int MaxV, int ColorVA, int GridStyleV, int GridColorV, BindataEx FontV, int FontColorV, int Leftretain, int Topretain, int Rightretain, int bottomretain, AutoInt hotcolumn, int GraphWidth, BOOL NegativeHollow, int NegativeColor, BOOL PositiveHollow, int PositiveColor, BindataEx ItemData, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_CandleChartEx);
		CreateControl_CandleChartEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, ChartStyle, TemplateH, ColorHA, GridStyleH, GridColorH, FontH, FontColorH, TemplateV, MiniV, MaxV, ColorVA, GridStyleV, GridColorV, FontV, FontColorV, Leftretain, Topretain, Rightretain, bottomretain, hotcolumn, GraphWidth, NegativeHollow, NegativeColor, PositiveHollow, PositiveColor, ItemData, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_烛线图Ex--------*/
	void CandleChartEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_CandleChartEx(control, index, attribute);
	}
	/*--------取属性_烛线图Ex--------*/
	AutoInt CandleChartEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_CandleChartEx(control, index);
	}
	/*--------置图例值_烛线图Ex--------*/
	void CandleChartEx::SetGraphValue(AutoInt index, AutoInt DataId, int openVal, int closeVal, int lowVal, int heighVal)
	{
		SetGraphValue_CandleChartEx(control, index, DataId, openVal, closeVal, lowVal, heighVal);
	}
	/*--------取图例值_烛线图Ex--------*/
	int CandleChartEx::GetGraphValue(AutoInt index, AutoInt DataId)
	{
		return GetGraphValue_CandleChartEx(control, index, DataId);
	}

	/*----------组件烛线图Ex属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx CandleChartEx::Skin()
	{
		return (BinEx_P)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void CandleChartEx::Skin(BindataEx Skin)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取图表风格--------*/
	int CandleChartEx::ChartStyle()
	{
		return (int)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_ChartStyle);
	}
	/*--------置图表风格--------*/
	void CandleChartEx::ChartStyle(int ChartStyle)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_ChartStyle, (AutoInt)ChartStyle);
	}
	/*--------取横轴标题模版--------*/
	StringEx CandleChartEx::TemplateH()
	{
		return (StrEx_P)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_TemplateH);
	}
	/*--------置横轴标题模版--------*/
	void CandleChartEx::TemplateH(StringEx TemplateH)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_TemplateH, (AutoInt)(StrEx_P)TemplateH);
	}
	/*--------取横轴颜色--------*/
	int CandleChartEx::ColorHA()
	{
		return (int)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_ColorHA);
	}
	/*--------置横轴颜色--------*/
	void CandleChartEx::ColorHA(int ColorHA)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_ColorHA, (AutoInt)ColorHA);
	}
	/*--------取横轴网格类型--------*/
	int CandleChartEx::GridStyleH()
	{
		return (int)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_GridStyleH);
	}
	/*--------置横轴网格类型--------*/
	void CandleChartEx::GridStyleH(int GridStyleH)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_GridStyleH, (AutoInt)GridStyleH);
	}
	/*--------取横轴网格色--------*/
	int CandleChartEx::GridColorH()
	{
		return (int)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_GridColorH);
	}
	/*--------置横轴网格色--------*/
	void CandleChartEx::GridColorH(int GridColorH)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_GridColorH, (AutoInt)GridColorH);
	}
	/*--------取横轴字体--------*/
	BindataEx CandleChartEx::FontH()
	{
		return (BinEx_P)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_FontH);
	}
	/*--------置横轴字体--------*/
	void CandleChartEx::FontH(BindataEx FontH)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_FontH, (AutoInt)(BinEx_P)FontH);
	}
	/*--------取横轴字体色--------*/
	int CandleChartEx::FontColorH()
	{
		return (int)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_FontColorH);
	}
	/*--------置横轴字体色--------*/
	void CandleChartEx::FontColorH(int FontColorH)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_FontColorH, (AutoInt)FontColorH);
	}
	/*--------取纵轴标题模版--------*/
	StringEx CandleChartEx::TemplateV()
	{
		return (StrEx_P)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_TemplateV);
	}
	/*--------置纵轴标题模版--------*/
	void CandleChartEx::TemplateV(StringEx TemplateV)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_TemplateV, (AutoInt)(StrEx_P)TemplateV);
	}
	/*--------取纵轴最小值--------*/
	int CandleChartEx::MiniV()
	{
		return (int)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_MiniV);
	}
	/*--------置纵轴最小值--------*/
	void CandleChartEx::MiniV(int MiniV)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_MiniV, (AutoInt)MiniV);
	}
	/*--------取纵轴最大值--------*/
	int CandleChartEx::MaxV()
	{
		return (int)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_MaxV);
	}
	/*--------置纵轴最大值--------*/
	void CandleChartEx::MaxV(int MaxV)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_MaxV, (AutoInt)MaxV);
	}
	/*--------取纵轴颜色--------*/
	int CandleChartEx::ColorVA()
	{
		return (int)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_ColorVA);
	}
	/*--------置纵轴颜色--------*/
	void CandleChartEx::ColorVA(int ColorVA)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_ColorVA, (AutoInt)ColorVA);
	}
	/*--------取纵轴网格类型--------*/
	int CandleChartEx::GridStyleV()
	{
		return (int)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_GridStyleV);
	}
	/*--------置纵轴网格类型--------*/
	void CandleChartEx::GridStyleV(int GridStyleV)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_GridStyleV, (AutoInt)GridStyleV);
	}
	/*--------取纵轴网格色--------*/
	int CandleChartEx::GridColorV()
	{
		return (int)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_GridColorV);
	}
	/*--------置纵轴网格色--------*/
	void CandleChartEx::GridColorV(int GridColorV)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_GridColorV, (AutoInt)GridColorV);
	}
	/*--------取纵轴字体--------*/
	BindataEx CandleChartEx::FontV()
	{
		return (BinEx_P)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_FontV);
	}
	/*--------置纵轴字体--------*/
	void CandleChartEx::FontV(BindataEx FontV)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_FontV, (AutoInt)(BinEx_P)FontV);
	}
	/*--------取纵轴字体色--------*/
	int CandleChartEx::FontColorV()
	{
		return (int)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_FontColorV);
	}
	/*--------置纵轴字体色--------*/
	void CandleChartEx::FontColorV(int FontColorV)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_FontColorV, (AutoInt)FontColorV);
	}
	/*--------取左预留--------*/
	int CandleChartEx::LeftReserve()
	{
		return (int)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_LeftReserve);
	}
	/*--------置左预留--------*/
	void CandleChartEx::LeftReserve(int LeftReserve)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_LeftReserve, (AutoInt)LeftReserve);
	}
	/*--------取顶预留--------*/
	int CandleChartEx::TopReserve()
	{
		return (int)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_TopReserve);
	}
	/*--------置顶预留--------*/
	void CandleChartEx::TopReserve(int TopReserve)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_TopReserve, (AutoInt)TopReserve);
	}
	/*--------取右预留--------*/
	int CandleChartEx::RightReserve()
	{
		return (int)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_RightReserve);
	}
	/*--------置右预留--------*/
	void CandleChartEx::RightReserve(int RightReserve)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_RightReserve, (AutoInt)RightReserve);
	}
	/*--------取底预留--------*/
	int CandleChartEx::BottomReserve()
	{
		return (int)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_BottomReserve);
	}
	/*--------置底预留--------*/
	void CandleChartEx::BottomReserve(int BottomReserve)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_BottomReserve, (AutoInt)BottomReserve);
	}
	/*--------取热点图列--------*/
	int CandleChartEx::HotColumn()
	{
		return (int)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_HotColumn);
	}
	/*--------置热点图列--------*/
	void CandleChartEx::HotColumn(int HotColumn)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_HotColumn, (AutoInt)HotColumn);
	}
	/*--------取图例宽度--------*/
	int CandleChartEx::GraphWidth()
	{
		return (int)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_GraphWidth);
	}
	/*--------置图例宽度--------*/
	void CandleChartEx::GraphWidth(int GraphWidth)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_GraphWidth, (AutoInt)GraphWidth);
	}
	/*--------取阴线镂空--------*/
	BOOL CandleChartEx::NegativeHollow()
	{
		return (BOOL)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_NegativeHollow);
	}
	/*--------置阴线镂空--------*/
	void CandleChartEx::NegativeHollow(BOOL NegativeHollow)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_NegativeHollow, (AutoInt)NegativeHollow);
	}
	/*--------取阴线颜色--------*/
	int CandleChartEx::NegativeColor()
	{
		return (int)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_NegativeColor);
	}
	/*--------置阴线颜色--------*/
	void CandleChartEx::NegativeColor(int NegativeColor)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_NegativeColor, (AutoInt)NegativeColor);
	}
	/*--------取阳线镂空--------*/
	BOOL CandleChartEx::PositiveHollow()
	{
		return (BOOL)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_PositiveHollow);
	}
	/*--------置阳线镂空--------*/
	void CandleChartEx::PositiveHollow(BOOL PositiveHollow)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_PositiveHollow, (AutoInt)PositiveHollow);
	}
	/*--------取阳线颜色--------*/
	int CandleChartEx::PositiveColor()
	{
		return (int)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_PositiveColor);
	}
	/*--------置阳线颜色--------*/
	void CandleChartEx::PositiveColor(int PositiveColor)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_PositiveColor, (AutoInt)PositiveColor);
	}
	/*--------取图例管理--------*/
	BindataEx CandleChartEx::GraphData()
	{
		return (BinEx_P)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_GraphData);
	}
	/*--------置图例管理--------*/
	void CandleChartEx::GraphData(BindataEx GraphData)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_GraphData, (AutoInt)(BinEx_P)GraphData);
	}
	/*--------取扩展元素--------*/
	BindataEx CandleChartEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void CandleChartEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx CandleChartEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_CandleChartEx(control, CandleChartEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void CandleChartEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_CandleChartEx(control, CandleChartEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件烛线图Ex属性读写函数结束--------*/

}
//DrawPanelEx
namespace exui
{
	DrawPanelEx::DrawPanelEx()
	{

	}
	DrawPanelEx::~DrawPanelEx()
	{

	}
	/*--------创建组件_画板EX--------*/
	DrawPanelEx_P DrawPanelEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, int BackColor, int LineColor, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_DrawPanelEx);
		CreateControl_DrawPanelEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, BackColor, LineColor, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;

	}
	/*--------置属性_画板EX--------*/
	void DrawPanelEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_DrawPanelEx(control, index, attribute);
	}
	/*--------取属性_画板EX--------*/
	AutoInt DrawPanelEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_DrawPanelEx(control, index);
	}
	/*--------画图片_画板EX--------*/
	void DrawPanelEx::DrawImage(BitmapEx Image, float dstX, float dstY, float dstWidth, float dstHeight, float srcx, float srcy, float srcwidth, float srcheight, int Alpha)
	{
		DrawImage_DrawPanelEx(control, Image, dstX, dstY, dstWidth, dstHeight, srcx, srcy, srcwidth, srcheight, Alpha);
	}
	/*--------画文本_画板EX--------*/
	void DrawPanelEx::DrawString(StringEx string, float dstX, float dstY, float dstWidth, float dstHeight, BindataEx font, AutoInt StringFormat, int DrawMode, int DrawParame, int color, int auxcolor)
	{
		DrawString_DrawPanelEx(control, string, dstX, dstY, dstWidth, dstHeight, font, StringFormat, DrawMode, DrawParame, color, auxcolor);
	}
	/*--------画多边形_画板EX--------*/
	void DrawPanelEx::DrawPolygon(float* Points, int count, int DrawMode, int DrawParame, int color, int auxcolor)
	{
		DrawPolygon_DrawPanelEx(control, Points, count, DrawMode, DrawParame, color, auxcolor);
	}
	/*--------画矩形_画板EX--------*/
	void DrawPanelEx::DrawRect(float X, float Y, float Width, float Height, int DrawMode, int DrawParame, int color, int auxcolor)
	{
		DrawRect_DrawPanelEx(control, X, Y, Width, Height, DrawMode, DrawParame, color, auxcolor);
	}
	/*--------画椭圆_画板EX--------*/
	void DrawPanelEx::DrawEllipse(float X, float Y, float Width, float Height, float Rgn, int DrawMode, int DrawParame, int color, int auxcolor)
	{
		DrawEllipse_DrawPanelEx(control, X, Y, Width, Height, Rgn, DrawMode, DrawParame, color, auxcolor);
	}
	/*--------画饼_画板EX--------*/
	void DrawPanelEx::DrawPie(float X, float Y, float Width, float Height, float starAngle, float sweepAngle, int DrawMode, int DrawParame, int color, int auxcolor)
	{
		DrawPie_DrawPanelEx(control, X, Y, Width, Height, starAngle, sweepAngle, DrawMode, DrawParame, color, auxcolor);
	}
	/*--------画曲线_画板EX--------*/
	void DrawPanelEx::DrawCurve(float* Points, int count, float tension, int Mode, int DrawMode, int DrawParame, int color, int auxcolor)
	{
		DrawCurve_DrawPanelEx(control, Points, count, tension, Mode, DrawMode, DrawParame, color, auxcolor);
	}
	/*--------画弧线_画板EX--------*/
	void DrawPanelEx::DrawArc(float X, float Y, float Width, float Height, float starAngle, float sweepAngle, int DrawMode, int DrawParame, int color, int auxcolor)
	{
		DrawArc_DrawPanelEx(control, X, Y, Width, Height, starAngle, sweepAngle, DrawMode, DrawParame, color, auxcolor);
	}
	/*--------画直线_画板EX--------*/
	void DrawPanelEx::DrawLine(float X, float Y, float Width, float Height, int DrawMode, int DrawParame, int color, int auxcolor)
	{
		DrawLine_DrawPanelEx(control, X, Y, Width, Height, DrawMode, DrawParame, color, auxcolor);
	}
	/*--------清除_画板EX--------*/
	void DrawPanelEx::Clear(int colour, float X, float Y, float Width, float Height, BOOL mode)
	{
		Clear_DrawPanelEx(control, colour, X, Y, Width, Height, mode);
	}
	/*--------更新_画板EX--------*/
	void DrawPanelEx::Update(float X, float Y, float Width, float Height, BOOL mode)
	{
		Update_DrawPanelEx(control, X, Y, Width, Height, mode);
	}
	/*--------置剪辑区_画板EX--------*/
	void DrawPanelEx::SetClip(int CombineMd, AutoInt Clip, int cliptype, float X, float Y, float Width, float Height, float Rgn)
	{
		SetClip_DrawPanelEx(control, CombineMd, Clip, cliptype, X, Y, Width, Height, Rgn);
	}
	/*--------置绘图质量_画板EX--------*/
	void DrawPanelEx::SetGraphicsquality(int Smoothing, int Interpolation, int PixelOffset)
	{
		SetGraphicsquality_DrawPanelEx(control, Smoothing, Interpolation, PixelOffset);
	}
	/*--------取绘图数据_画板EX--------*/
	AutoInt DrawPanelEx::GetGraphicsData(int Type, float X, float Y, float Width, float Height)
	{
		return GetGraphicsData_DrawPanelEx(control, Type, X, Y, Width, Height);
	}

	/*----------组件画板EX属性读写函数开始--------*/


/*--------取背景颜色--------*/
	int DrawPanelEx::BackColor()
	{
		return (int)GetAttribute_DrawPanelEx(control, DrawPanelEx_Attr_BackColor);
	}
	/*--------置背景颜色--------*/
	void DrawPanelEx::BackColor(int BackColor)
	{
		SetAttribute_DrawPanelEx(control, DrawPanelEx_Attr_BackColor, (AutoInt)BackColor);
	}
	/*--------取边线色--------*/
	int DrawPanelEx::LineColor()
	{
		return (int)GetAttribute_DrawPanelEx(control, DrawPanelEx_Attr_LineColor);
	}
	/*--------置边线色--------*/
	void DrawPanelEx::LineColor(int LineColor)
	{
		SetAttribute_DrawPanelEx(control, DrawPanelEx_Attr_LineColor, (AutoInt)LineColor);
	}
	/*--------取扩展元素--------*/
	BindataEx DrawPanelEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_DrawPanelEx(control, DrawPanelEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void DrawPanelEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_DrawPanelEx(control, DrawPanelEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx DrawPanelEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_DrawPanelEx(control, DrawPanelEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void DrawPanelEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_DrawPanelEx(control, DrawPanelEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件画板EX属性读写函数结束--------*/

}
//ScrollLayoutBoxEx
namespace exui
{
	ScrollLayoutBoxEx::ScrollLayoutBoxEx()
	{

	}
	ScrollLayoutBoxEx::~ScrollLayoutBoxEx()
	{

	}
	/*--------创建组件_滚动布局框EX--------*/
	ScrollLayoutBoxEx_P ScrollLayoutBoxEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, int Mode, int HorizontalPosition, int VerticalPosition, int MaxHorizontalPosition, int MaxVerticalPosition, int retain1, int retain2, int retain3, int retain4, int retain5, int retain6, int ScrollBarMode, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_ScrollLayoutBoxEx);
		CreateControl_ScrollLayoutBoxEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, Mode, HorizontalPosition, VerticalPosition, MaxHorizontalPosition, MaxVerticalPosition, retain1, retain2, retain3, retain4, retain5, retain6, ScrollBarMode, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_滚动布局框EX--------*/
	void ScrollLayoutBoxEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_ScrollLayoutBoxEx(control, index, attribute);
	}
	/*--------取属性_滚动布局框EX--------*/
	AutoInt ScrollLayoutBoxEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_ScrollLayoutBoxEx(control, index);
	}
	/*--------添加组件_滚动布局框EX--------*/
	BOOL ScrollLayoutBoxEx::InsertControl(Control_P ControlHand, int left, int top)
	{
		return InsertControl_ScrollLayoutBoxEx(control, ControlHand, left, top);
	}
	/*--------移除组件_滚动布局框EX--------*/
	BOOL ScrollLayoutBoxEx::RemoveControl(Control_P ControlHand, Control_P Parentcontrol, int left, int top)
	{
		return RemoveControl_ScrollLayoutBoxEx(control, ControlHand, Parentcontrol, left, top);
	}

	/*----------组件滚动布局框EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx ScrollLayoutBoxEx::Skin()
	{
		return (BinEx_P)GetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void ScrollLayoutBoxEx::Skin(BindataEx Skin)
	{
		SetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取模式--------*/
	int ScrollLayoutBoxEx::Mode()
	{
		return (int)GetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_Mode);
	}
	/*--------置模式--------*/
	void ScrollLayoutBoxEx::Mode(int Mode)
	{
		SetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_Mode, (AutoInt)Mode);
	}
	/*--------取横向位置--------*/
	int ScrollLayoutBoxEx::HorizontalPosition()
	{
		return (int)GetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_HorizontalPosition);
	}
	/*--------置横向位置--------*/
	void ScrollLayoutBoxEx::HorizontalPosition(int HorizontalPosition)
	{
		SetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_HorizontalPosition, (AutoInt)HorizontalPosition);
	}
	/*--------取纵向位置--------*/
	int ScrollLayoutBoxEx::VerticalPosition()
	{
		return (int)GetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_VerticalPosition);
	}
	/*--------置纵向位置--------*/
	void ScrollLayoutBoxEx::VerticalPosition(int VerticalPosition)
	{
		SetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_VerticalPosition, (AutoInt)VerticalPosition);
	}
	/*--------取横向最大位置--------*/
	int ScrollLayoutBoxEx::MaxHorizontalPosition()
	{
		return (int)GetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_MaxHorizontalPosition);
	}
	/*--------置横向最大位置--------*/
	void ScrollLayoutBoxEx::MaxHorizontalPosition(int MaxHorizontalPosition)
	{
		SetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_MaxHorizontalPosition, (AutoInt)MaxHorizontalPosition);
	}
	/*--------取纵向最大位置--------*/
	int ScrollLayoutBoxEx::MaxVerticalPosition()
	{
		return (int)GetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_MaxVerticalPosition);
	}
	/*--------置纵向最大位置--------*/
	void ScrollLayoutBoxEx::MaxVerticalPosition(int MaxVerticalPosition)
	{
		SetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_MaxVerticalPosition, (AutoInt)MaxVerticalPosition);
	}
	/*--------取保留属性1--------*/
	int ScrollLayoutBoxEx::Reserve1()
	{
		return (int)GetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_Reserve1);
	}
	/*--------置保留属性1--------*/
	void ScrollLayoutBoxEx::Reserve1(int Reserve1)
	{
		SetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_Reserve1, (AutoInt)Reserve1);
	}
	/*--------取保留属性2--------*/
	int ScrollLayoutBoxEx::Reserve2()
	{
		return (int)GetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_Reserve2);
	}
	/*--------置保留属性2--------*/
	void ScrollLayoutBoxEx::Reserve2(int Reserve2)
	{
		SetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_Reserve2, (AutoInt)Reserve2);
	}
	/*--------取保留属性3--------*/
	int ScrollLayoutBoxEx::Reserve3()
	{
		return (int)GetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_Reserve3);
	}
	/*--------置保留属性3--------*/
	void ScrollLayoutBoxEx::Reserve3(int Reserve3)
	{
		SetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_Reserve3, (AutoInt)Reserve3);
	}
	/*--------取保留属性4--------*/
	int ScrollLayoutBoxEx::Reserve4()
	{
		return (int)GetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_Reserve4);
	}
	/*--------置保留属性4--------*/
	void ScrollLayoutBoxEx::Reserve4(int Reserve4)
	{
		SetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_Reserve4, (AutoInt)Reserve4);
	}
	/*--------取保留属性5--------*/
	int ScrollLayoutBoxEx::Reserve5()
	{
		return (int)GetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_Reserve5);
	}
	/*--------置保留属性5--------*/
	void ScrollLayoutBoxEx::Reserve5(int Reserve5)
	{
		SetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_Reserve5, (AutoInt)Reserve5);
	}
	/*--------取保留属性6--------*/
	int ScrollLayoutBoxEx::Reserve6()
	{
		return (int)GetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_Reserve6);
	}
	/*--------置保留属性6--------*/
	void ScrollLayoutBoxEx::Reserve6(int Reserve6)
	{
		SetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_Reserve6, (AutoInt)Reserve6);
	}
	/*--------取滚动条方式--------*/
	int ScrollLayoutBoxEx::ScrollBarMode()
	{
		return (int)GetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_ScrollBarMode);
	}
	/*--------置滚动条方式--------*/
	void ScrollLayoutBoxEx::ScrollBarMode(int ScrollBarMode)
	{
		SetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_ScrollBarMode, (AutoInt)ScrollBarMode);
	}
	/*--------取扩展元素--------*/
	BindataEx ScrollLayoutBoxEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void ScrollLayoutBoxEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx ScrollLayoutBoxEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void ScrollLayoutBoxEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_ScrollLayoutBoxEx(control, ScrollLayoutBoxEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件滚动布局框EX属性读写函数结束--------*/

}
//MediaboxEx
namespace exui
{
	MediaboxEx::MediaboxEx()
	{

	}
	MediaboxEx::~MediaboxEx()
	{

	}
	/*--------创建组件_影像框EX--------*/
	MediaboxEx_P MediaboxEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, StringEx MediaFile, int Align, int PlayState, int PlayMode, int TotalProgress, int CurrentPosition, int volume, int PlaySpeed, int Reserve1, int Reserve2, int Reserve3, int Reserve4, int Reserve5, int Reserve6, int Reserve7, int Reserve8, int Reserve9, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_MediaboxEx);
		CreateControl_MediaboxEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, MediaFile, Align, PlayState, PlayMode, TotalProgress, CurrentPosition, volume, PlaySpeed, Reserve1, Reserve2, Reserve3, Reserve4, Reserve5, Reserve6, Reserve7, Reserve8, Reserve9, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_影像框EX--------*/
	void MediaboxEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_MediaboxEx(control, index, attribute);
	}
	/*--------取属性_影像框EX--------*/
	AutoInt MediaboxEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_MediaboxEx(control, index);
	}
	/*--------执行命令_影像框EX--------*/
	AutoInt MediaboxEx::RunCmd(AutoInt Parameter1, AutoInt Parameter2, AutoInt Parameter3, AutoInt Parameter4)
	{
		return RunCmd_MediaboxEx(control, Parameter1, Parameter2, Parameter3, Parameter4);
	}

	/*----------组件影像框EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx MediaboxEx::Skin()
	{
		return (BinEx_P)GetAttribute_MediaboxEx(control, MediaboxEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void MediaboxEx::Skin(BindataEx Skin)
	{
		SetAttribute_MediaboxEx(control, MediaboxEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取影像文件--------*/
	StringEx MediaboxEx::MediaFile()
	{
		return (StrEx_P)GetAttribute_MediaboxEx(control, MediaboxEx_Attr_MediaFile);
	}
	/*--------置影像文件--------*/
	void MediaboxEx::MediaFile(StringEx MediaFile)
	{
		SetAttribute_MediaboxEx(control, MediaboxEx_Attr_MediaFile, (AutoInt)(StrEx_P)MediaFile);
	}
	/*--------取对齐方式--------*/
	int MediaboxEx::Align()
	{
		return (int)GetAttribute_MediaboxEx(control, MediaboxEx_Attr_Align);
	}
	/*--------置对齐方式--------*/
	void MediaboxEx::Align(int Align)
	{
		SetAttribute_MediaboxEx(control, MediaboxEx_Attr_Align, (AutoInt)Align);
	}
	/*--------取播放状态--------*/
	int MediaboxEx::PlayState()
	{
		return (int)GetAttribute_MediaboxEx(control, MediaboxEx_Attr_PlayState);
	}
	/*--------置播放状态--------*/
	void MediaboxEx::PlayState(int PlayState)
	{
		SetAttribute_MediaboxEx(control, MediaboxEx_Attr_PlayState, (AutoInt)PlayState);
	}
	/*--------取播放模式--------*/
	int MediaboxEx::PlayMode()
	{
		return (int)GetAttribute_MediaboxEx(control, MediaboxEx_Attr_PlayMode);
	}
	/*--------置播放模式--------*/
	void MediaboxEx::PlayMode(int PlayMode)
	{
		SetAttribute_MediaboxEx(control, MediaboxEx_Attr_PlayMode, (AutoInt)PlayMode);
	}
	/*--------取总进度--------*/
	int MediaboxEx::TotalProgress()
	{
		return (int)GetAttribute_MediaboxEx(control, MediaboxEx_Attr_TotalProgress);
	}
	/*--------置总进度--------*/
	void MediaboxEx::TotalProgress(int TotalProgress)
	{
		SetAttribute_MediaboxEx(control, MediaboxEx_Attr_TotalProgress, (AutoInt)TotalProgress);
	}
	/*--------取当前进度--------*/
	int MediaboxEx::CurrentPosition()
	{
		return (int)GetAttribute_MediaboxEx(control, MediaboxEx_Attr_CurrentPosition);
	}
	/*--------置当前进度--------*/
	void MediaboxEx::CurrentPosition(int CurrentPosition)
	{
		SetAttribute_MediaboxEx(control, MediaboxEx_Attr_CurrentPosition, (AutoInt)CurrentPosition);
	}
	/*--------取音量--------*/
	int MediaboxEx::volume()
	{
		return (int)GetAttribute_MediaboxEx(control, MediaboxEx_Attr_volume);
	}
	/*--------置音量--------*/
	void MediaboxEx::volume(int volume)
	{
		SetAttribute_MediaboxEx(control, MediaboxEx_Attr_volume, (AutoInt)volume);
	}
	/*--------取播放速度--------*/
	int MediaboxEx::PlaySpeed()
	{
		return (int)GetAttribute_MediaboxEx(control, MediaboxEx_Attr_PlaySpeed);
	}
	/*--------置播放速度--------*/
	void MediaboxEx::PlaySpeed(int PlaySpeed)
	{
		SetAttribute_MediaboxEx(control, MediaboxEx_Attr_PlaySpeed, (AutoInt)PlaySpeed);
	}
	/*--------取保留1--------*/
	int MediaboxEx::Reserve1()
	{
		return (int)GetAttribute_MediaboxEx(control, MediaboxEx_Attr_Reserve1);
	}
	/*--------置保留1--------*/
	void MediaboxEx::Reserve1(int Reserve1)
	{
		SetAttribute_MediaboxEx(control, MediaboxEx_Attr_Reserve1, (AutoInt)Reserve1);
	}
	/*--------取保留2--------*/
	int MediaboxEx::Reserve2()
	{
		return (int)GetAttribute_MediaboxEx(control, MediaboxEx_Attr_Reserve2);
	}
	/*--------置保留2--------*/
	void MediaboxEx::Reserve2(int Reserve2)
	{
		SetAttribute_MediaboxEx(control, MediaboxEx_Attr_Reserve2, (AutoInt)Reserve2);
	}
	/*--------取保留3--------*/
	int MediaboxEx::Reserve3()
	{
		return (int)GetAttribute_MediaboxEx(control, MediaboxEx_Attr_Reserve3);
	}
	/*--------置保留3--------*/
	void MediaboxEx::Reserve3(int Reserve3)
	{
		SetAttribute_MediaboxEx(control, MediaboxEx_Attr_Reserve3, (AutoInt)Reserve3);
	}
	/*--------取保留4--------*/
	int MediaboxEx::Reserve4()
	{
		return (int)GetAttribute_MediaboxEx(control, MediaboxEx_Attr_Reserve4);
	}
	/*--------置保留4--------*/
	void MediaboxEx::Reserve4(int Reserve4)
	{
		SetAttribute_MediaboxEx(control, MediaboxEx_Attr_Reserve4, (AutoInt)Reserve4);
	}
	/*--------取保留5--------*/
	int MediaboxEx::Reserve5()
	{
		return (int)GetAttribute_MediaboxEx(control, MediaboxEx_Attr_Reserve5);
	}
	/*--------置保留5--------*/
	void MediaboxEx::Reserve5(int Reserve5)
	{
		SetAttribute_MediaboxEx(control, MediaboxEx_Attr_Reserve5, (AutoInt)Reserve5);
	}
	/*--------取保留6--------*/
	int MediaboxEx::Reserve6()
	{
		return (int)GetAttribute_MediaboxEx(control, MediaboxEx_Attr_Reserve6);
	}
	/*--------置保留6--------*/
	void MediaboxEx::Reserve6(int Reserve6)
	{
		SetAttribute_MediaboxEx(control, MediaboxEx_Attr_Reserve6, (AutoInt)Reserve6);
	}
	/*--------取保留7--------*/
	int MediaboxEx::Reserve7()
	{
		return (int)GetAttribute_MediaboxEx(control, MediaboxEx_Attr_Reserve7);
	}
	/*--------置保留7--------*/
	void MediaboxEx::Reserve7(int Reserve7)
	{
		SetAttribute_MediaboxEx(control, MediaboxEx_Attr_Reserve7, (AutoInt)Reserve7);
	}
	/*--------取保留8--------*/
	int MediaboxEx::Reserve8()
	{
		return (int)GetAttribute_MediaboxEx(control, MediaboxEx_Attr_Reserve8);
	}
	/*--------置保留8--------*/
	void MediaboxEx::Reserve8(int Reserve8)
	{
		SetAttribute_MediaboxEx(control, MediaboxEx_Attr_Reserve8, (AutoInt)Reserve8);
	}
	/*--------取保留9--------*/
	int MediaboxEx::Reserve9()
	{
		return (int)GetAttribute_MediaboxEx(control, MediaboxEx_Attr_Reserve9);
	}
	/*--------置保留9--------*/
	void MediaboxEx::Reserve9(int Reserve9)
	{
		SetAttribute_MediaboxEx(control, MediaboxEx_Attr_Reserve9, (AutoInt)Reserve9);
	}
	/*--------取扩展元素--------*/
	BindataEx MediaboxEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_MediaboxEx(control, MediaboxEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void MediaboxEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_MediaboxEx(control, MediaboxEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx MediaboxEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_MediaboxEx(control, MediaboxEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void MediaboxEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_MediaboxEx(control, MediaboxEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件影像框EX属性读写函数结束--------*/


}
//CarouselBoxEx
namespace exui
{
	CarouselBoxEx::CarouselBoxEx()
	{

	}
	CarouselBoxEx::~CarouselBoxEx()
	{

	}
	/*--------创建组件_轮播框EX--------*/
	CarouselBoxEx_P CarouselBoxEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, AutoInt CurrentPosition, int Schedule, int Spacing, int LayoutStyle, int LayoutParam, int CtrlModel, int CarouselMode, int ScrollDirection, int AutoPlay, int DurationTime, int FrameDelay, int Step, int AlgorithmMode, BindataEx ItemData, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_CarouselBoxEx);
		CreateControl_CarouselBoxEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, CurrentPosition, Schedule, Spacing, LayoutStyle, LayoutParam, CtrlModel, CarouselMode, ScrollDirection, AutoPlay, DurationTime, FrameDelay, Step, AlgorithmMode, ItemData, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_轮播框Ex--------*/
	void CarouselBoxEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_CarouselBoxEx(control, index, attribute);
	}
	/*--------取属性_轮播框Ex--------*/
	AutoInt CarouselBoxEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_CarouselBoxEx(control, index);
	}
	/*--------插入_轮播框Ex--------*/
	AutoInt CarouselBoxEx::InsertItem(AutoInt index, AutoInt Data, BitmapEx Skin, BitmapEx Ico, int IconWidth, int IconHeight, StringEx Title, BitmapEx Font, int FontColor, int HotFontColor, int Type, int Align, int reserve, int Size)
	{
		return InsertItem_CarouselBoxEx(control, index, Data, Skin, Ico, IconWidth, IconHeight, Title, Font, FontColor, HotFontColor, Type, Align, reserve, Size);
	}
	/*--------删除_轮播框Ex--------*/
	void CarouselBoxEx::DeleteItem(AutoInt index)
	{
		DeleteItem_CarouselBoxEx(control, index);
	}
	/*--------取数量_轮播框Ex--------*/
	AutoInt CarouselBoxEx::GetItemCount()
	{
		return GetItemCount_CarouselBoxEx(control);
	}
	/*--------置项目属性_轮播框Ex--------*/
	void CarouselBoxEx::SetItemAttribute(AutoInt index, int AttributeId, AutoInt Data, BitmapEx Skin, BitmapEx Ico, int IconWidth, int IconHeight, StringEx Title, BitmapEx Font, int FontColor, int HotFontColor, int Type, int Align, int reserve, int Size)
	{
		SetItemAttribute_CarouselBoxEx(control, index, AttributeId, Data, Skin, Ico, IconWidth, IconHeight, Title, Font, FontColor, HotFontColor, Type, Align, reserve, Size);
	}
	/*--------取项目属性_轮播框Ex--------*/
	AutoInt CarouselBoxEx::GetItemAttribute(AutoInt index, int AttributeId)
	{
		return GetItemAttribute_CarouselBoxEx(control, index, AttributeId);
	}
	/*--------执行命令_轮播框Ex--------*/
	AutoInt CarouselBoxEx::RunCmd(AutoInt Cmd, AutoInt Param1, AutoInt Param2, AutoInt Param3, AutoInt Param4)
	{
		return  RunCmd_CarouselBoxEx(control, Cmd, Param1, Param2, Param3, Param4);
	}

	/*----------组件轮播框EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx CarouselBoxEx::Skin()
	{
		return (BinEx_P)GetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void CarouselBoxEx::Skin(BindataEx Skin)
	{
		SetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------当前进度--------*/
	AutoInt CarouselBoxEx::CurrentPosition()
	{
		return (AutoInt)GetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_CurrentPosition);
	}
	/*--------当前进度--------*/
	void CarouselBoxEx::CurrentPosition(AutoInt CurrentPosition)
	{
		SetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_CurrentPosition, (AutoInt)CurrentPosition);
	}


	/*--------纵向--------*/
	int CarouselBoxEx::Schedule()
	{
		return (AutoInt)GetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_Schedule);
	}
	/*--------纵向--------*/
	void CarouselBoxEx::Schedule(int Schedule)
	{
		SetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_Schedule, (int)Schedule);
	}

	/*--------间距--------*/
	int CarouselBoxEx::Spacing()
	{
		return (AutoInt)GetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_Spacing);
	}
	/*--------间距--------*/
	void CarouselBoxEx::Spacing(int Spacing)
	{
		SetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_Spacing, (int)Spacing);
	}

	/*--------布局样式--------*/
	int CarouselBoxEx::LayoutStyle()
	{
		return (AutoInt)GetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_LayoutStyle);
	}
	/*--------布局样式--------*/
	void CarouselBoxEx::LayoutStyle(int LayoutStyle)
	{
		SetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_LayoutStyle, (int)LayoutStyle);
	}

	/*--------布局参数--------*/
	int CarouselBoxEx::LayoutParam()
	{
		return (AutoInt)GetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_LayoutParam);
	}
	/*--------布局参数--------*/
	void CarouselBoxEx::LayoutParam(int LayoutParam)
	{
		SetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_LayoutParam, (int)LayoutParam);
	}

	/*--------控制模式--------*/
	int CarouselBoxEx::CtrlModel()
	{
		return (AutoInt)GetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_CtrlModel);
	}
	/*--------控制模式--------*/
	void CarouselBoxEx::CtrlModel(int CtrlModel)
	{
		SetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_CtrlModel, (int)CtrlModel);
	}

	/*--------轮播模式--------*/
	int CarouselBoxEx::CarouselMode()
	{
		return (AutoInt)GetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_CarouselMode);
	}
	/*--------轮播模式--------*/
	void CarouselBoxEx::CarouselMode(int CarouselMode)
	{
		SetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_CarouselMode, (int)CarouselMode);
	}

	/*--------滚动方向--------*/
	int CarouselBoxEx::ScrollDirection()
	{
		return (AutoInt)GetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_ScrollDirection);
	}
	/*--------滚动方向--------*/
	void CarouselBoxEx::ScrollDirection(int ScrollDirection)
	{
		SetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_ScrollDirection, (int)ScrollDirection);
	}

	/*--------自动播放--------*/
	int CarouselBoxEx::AutoPlay()
	{
		return (AutoInt)GetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_AutoPlay);
	}
	/*--------自动播放--------*/
	void CarouselBoxEx::AutoPlay(int AutoPlay)
	{
		SetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_AutoPlay, (int)AutoPlay);
	}

	/*--------停留时长--------*/
	int CarouselBoxEx::DurationTime()
	{
		return (AutoInt)GetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_DurationTime);
	}
	/*--------停留时长--------*/
	void CarouselBoxEx::DurationTime(int DurationTime)
	{
		SetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_DurationTime, (int)DurationTime);
	}

	/*--------帧延迟--------*/
	int CarouselBoxEx::FrameDelay()
	{
		return (AutoInt)GetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_FrameDelay);
	}
	/*--------帧延迟--------*/
	void CarouselBoxEx::FrameDelay(int FrameDelay)
	{
		SetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_FrameDelay, (int)FrameDelay);
	}

	/*--------帧步进--------*/
	int CarouselBoxEx::Step()
	{
		return (AutoInt)GetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_Step);
	}
	/*--------帧步进--------*/
	void CarouselBoxEx::Step(int Step)
	{
		SetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_Step, (int)Step);
	}

	/*--------滚动算法--------*/
	int CarouselBoxEx::AlgorithmMode()
	{
		return (AutoInt)GetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_AlgorithmMode);
	}
	/*--------滚动算法--------*/
	void CarouselBoxEx::AlgorithmMode(int AlgorithmMode)
	{
		SetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_AlgorithmMode, (int)AlgorithmMode);
	}

	/*--------取项目数据--------*/
	BindataEx CarouselBoxEx::ItemData()
	{
		return (BinEx_P)GetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_ItemData);
	}
	/*--------置项目数据--------*/
	void CarouselBoxEx::ItemData(BindataEx ItemData)
	{
		SetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_ItemData, (AutoInt)(BinEx_P)ItemData);
	}
	/*--------取扩展元素--------*/
	BindataEx CarouselBoxEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void CarouselBoxEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx CarouselBoxEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void CarouselBoxEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_CarouselBoxEx(control, CarouselBoxEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}



	/*----------组件轮播框EX属性读写函数结束--------*/


}
//SuperEditEx
namespace exui
{
	SuperEditEx::SuperEditEx()
	{

	}
	SuperEditEx::~SuperEditEx()
	{

	}
	/*--------创建组件_超级编辑框EX--------*/
	SuperEditEx_P SuperEditEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, StringEx Kernel, StringEx Attribute, StringEx Data, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_SuperEditEx);
		CreateControl_SuperEditEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, Kernel, Attribute, Data, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_超级编辑框EX--------*/
	void SuperEditEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_SuperEditEx(control, index, attribute);
	}
	/*--------取属性_超级编辑框EX--------*/
	AutoInt SuperEditEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_SuperEditEx(control, index);
	}
	/*--------执行命令_超级编辑框EX--------*/
	StringEx SuperEditEx::RunCmd(StringEx Cmd, StringEx Parameter1, StringEx Parameter2, StringEx Parameter3, StringEx Parameter4, StringEx Parameter5, StringEx Parameter6, StringEx Parameter7, StringEx Parameter8, StringEx Parameter9)
	{
		return RunCmd_SuperEditEx(control, Cmd, Parameter1, Parameter2, Parameter3, Parameter4, Parameter5, Parameter6, Parameter7, Parameter8, Parameter9);
	}

	/*----------组件超级编辑框EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx SuperEditEx::Skin()
	{
		return (BinEx_P)GetAttribute_SuperEditEx(control, SuperEditEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void SuperEditEx::Skin(BindataEx Skin)
	{
		SetAttribute_SuperEditEx(control, SuperEditEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取核心--------*/
	StringEx SuperEditEx::Kernel()
	{
		return (StrEx_P)GetAttribute_SuperEditEx(control, SuperEditEx_Attr_Kernel);
	}
	/*--------置核心--------*/
	void SuperEditEx::Kernel(StringEx Kernel)
	{
		SetAttribute_SuperEditEx(control, SuperEditEx_Attr_Kernel, (AutoInt)(StrEx_P)Kernel);
	}
	/*--------取属性--------*/
	StringEx SuperEditEx::Attribute()
	{
		return (StrEx_P)GetAttribute_SuperEditEx(control, SuperEditEx_Attr_Attribute);
	}
	/*--------置属性--------*/
	void SuperEditEx::Attribute(StringEx Attribute)
	{
		SetAttribute_SuperEditEx(control, SuperEditEx_Attr_Attribute, (AutoInt)(StrEx_P)Attribute);
	}
	/*--------取数据--------*/
	StringEx SuperEditEx::Data()
	{
		return (StrEx_P)GetAttribute_SuperEditEx(control, SuperEditEx_Attr_Data);
	}
	/*--------置数据--------*/
	void SuperEditEx::Data(StringEx Data)
	{
		SetAttribute_SuperEditEx(control, SuperEditEx_Attr_Data, (AutoInt)(StrEx_P)Data);
	}


	/*--------取扩展元素--------*/
	BindataEx SuperEditEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_SuperEditEx(control, SuperEditEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void SuperEditEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_SuperEditEx(control, SuperEditEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx SuperEditEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_SuperEditEx(control, SuperEditEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void SuperEditEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_SuperEditEx(control, SuperEditEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}


	/*----------组件超级编辑框EX属性读写函数结束--------*/


}
//ChatBoxEx
namespace exui
{
	ChatBoxEx::ChatBoxEx()
	{

	}
	ChatBoxEx::~ChatBoxEx()
	{

	}
	/*--------创建组件_聊天框EX--------*/
	ChatBoxEx_P ChatBoxEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, StringEx Kernel, StringEx Attribute, StringEx Data, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_ChatBoxEx);
		CreateControl_ChatBoxEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, Kernel, Attribute, Data, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_聊天框EX--------*/
	void ChatBoxEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_ChatBoxEx(control, index, attribute);
	}
	/*--------取属性_聊天框EX--------*/
	AutoInt ChatBoxEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_ChatBoxEx(control, index);
	}
	/*--------执行命令_聊天框EX--------*/
	StringEx ChatBoxEx::RunCmd(StringEx Cmd, StringEx Parameter1, StringEx Parameter2, StringEx Parameter3, StringEx Parameter4, StringEx Parameter5, StringEx Parameter6, StringEx Parameter7, StringEx Parameter8, StringEx Parameter9)
	{
		return RunCmd_ChatBoxEx(control, Cmd, Parameter1, Parameter2, Parameter3, Parameter4, Parameter5, Parameter6, Parameter7, Parameter8, Parameter9);
	}

	/*----------组件聊天框EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx ChatBoxEx::Skin()
	{
		return (BinEx_P)GetAttribute_ChatBoxEx(control, ChatBoxEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void ChatBoxEx::Skin(BindataEx Skin)
	{
		SetAttribute_ChatBoxEx(control, ChatBoxEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取核心--------*/
	StringEx ChatBoxEx::Kernel()
	{
		return (StrEx_P)GetAttribute_ChatBoxEx(control, ChatBoxEx_Attr_Kernel);
	}
	/*--------置核心--------*/
	void ChatBoxEx::Kernel(StringEx Kernel)
	{
		SetAttribute_ChatBoxEx(control, ChatBoxEx_Attr_Kernel, (AutoInt)(StrEx_P)Kernel);
	}
	/*--------取属性--------*/
	StringEx ChatBoxEx::Attribute()
	{
		return (StrEx_P)GetAttribute_ChatBoxEx(control, ChatBoxEx_Attr_Attribute);
	}
	/*--------置属性--------*/
	void ChatBoxEx::Attribute(StringEx Attribute)
	{
		SetAttribute_ChatBoxEx(control, ChatBoxEx_Attr_Attribute, (AutoInt)(StrEx_P)Attribute);
	}
	/*--------取数据--------*/
	StringEx ChatBoxEx::Data()
	{
		return (StrEx_P)GetAttribute_ChatBoxEx(control, ChatBoxEx_Attr_Data);
	}
	/*--------置数据--------*/
	void ChatBoxEx::Data(StringEx Data)
	{
		SetAttribute_ChatBoxEx(control, ChatBoxEx_Attr_Data, (AutoInt)(StrEx_P)Data);
	}


	/*--------取扩展元素--------*/
	BindataEx ChatBoxEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_ChatBoxEx(control, ChatBoxEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void ChatBoxEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_ChatBoxEx(control, ChatBoxEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx ChatBoxEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_ChatBoxEx(control, ChatBoxEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void ChatBoxEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_ChatBoxEx(control, ChatBoxEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}


	/*----------组件聊天框EX属性读写函数结束--------*/


}
//SuperChartEx
namespace exui
{
	SuperChartEx::SuperChartEx()
	{

	}
	SuperChartEx::~SuperChartEx()
	{

	}
	/*--------创建组件_超级图表框EX--------*/
	SuperChartEx_P SuperChartEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, StringEx Kernel, StringEx Attribute, StringEx Data, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_SuperChartEx);
		CreateControl_SuperChartEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, Kernel, Attribute, Data, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_超级图表框EX--------*/
	void SuperChartEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_SuperChartEx(control, index, attribute);
	}
	/*--------取属性_超级图表框EX--------*/
	AutoInt SuperChartEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_SuperChartEx(control, index);
	}
	/*--------执行命令_超级图表框EX--------*/
	StringEx SuperChartEx::RunCmd(StringEx Cmd, StringEx Parameter1, StringEx Parameter2, StringEx Parameter3, StringEx Parameter4, StringEx Parameter5, StringEx Parameter6, StringEx Parameter7, StringEx Parameter8, StringEx Parameter9)
	{
		return RunCmd_SuperChartEx(control, Cmd, Parameter1, Parameter2, Parameter3, Parameter4, Parameter5, Parameter6, Parameter7, Parameter8, Parameter9);
	}

	/*----------组件超级图表框EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx SuperChartEx::Skin()
	{
		return (BinEx_P)GetAttribute_SuperChartEx(control, SuperChartEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void SuperChartEx::Skin(BindataEx Skin)
	{
		SetAttribute_SuperChartEx(control, SuperChartEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取核心--------*/
	StringEx SuperChartEx::Kernel()
	{
		return (StrEx_P)GetAttribute_SuperChartEx(control, SuperChartEx_Attr_Kernel);
	}
	/*--------置核心--------*/
	void SuperChartEx::Kernel(StringEx Kernel)
	{
		SetAttribute_SuperChartEx(control, SuperChartEx_Attr_Kernel, (AutoInt)(StrEx_P)Kernel);
	}
	/*--------取属性--------*/
	StringEx SuperChartEx::Attribute()
	{
		return (StrEx_P)GetAttribute_SuperChartEx(control, SuperChartEx_Attr_Attribute);
	}
	/*--------置属性--------*/
	void SuperChartEx::Attribute(StringEx Attribute)
	{
		SetAttribute_SuperChartEx(control, SuperChartEx_Attr_Attribute, (AutoInt)(StrEx_P)Attribute);
	}
	/*--------取数据--------*/
	StringEx SuperChartEx::Data()
	{
		return (StrEx_P)GetAttribute_SuperChartEx(control, SuperChartEx_Attr_Data);
	}
	/*--------置数据--------*/
	void SuperChartEx::Data(StringEx Data)
	{
		SetAttribute_SuperChartEx(control, SuperChartEx_Attr_Data, (AutoInt)(StrEx_P)Data);
	}


	/*--------取扩展元素--------*/
	BindataEx SuperChartEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_SuperChartEx(control, SuperChartEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void SuperChartEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_SuperChartEx(control, SuperChartEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx SuperChartEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_SuperChartEx(control, SuperChartEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void SuperChartEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_SuperChartEx(control, SuperChartEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}


	/*----------组件超级图表框EX属性读写函数结束--------*/


}
//DataReportBoxEx
namespace exui
{
	DataReportBoxEx::DataReportBoxEx()
	{

	}
	DataReportBoxEx::~DataReportBoxEx()
	{

	}
	/*--------创建组件_数据报表框EX--------*/
	DataReportBoxEx_P DataReportBoxEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, StringEx Kernel, StringEx Attribute, StringEx Data, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_DataReportBoxEx);
		CreateControl_DataReportBoxEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, Kernel, Attribute, Data, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_数据报表框EX--------*/
	void DataReportBoxEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_DataReportBoxEx(control, index, attribute);
	}
	/*--------取属性_数据报表框EX--------*/
	AutoInt DataReportBoxEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_DataReportBoxEx(control, index);
	}
	/*--------执行命令_数据报表框EX--------*/
	StringEx DataReportBoxEx::RunCmd(StringEx Cmd, StringEx Parameter1, StringEx Parameter2, StringEx Parameter3, StringEx Parameter4, StringEx Parameter5, StringEx Parameter6, StringEx Parameter7, StringEx Parameter8, StringEx Parameter9)
	{
		return RunCmd_DataReportBoxEx(control, Cmd, Parameter1, Parameter2, Parameter3, Parameter4, Parameter5, Parameter6, Parameter7, Parameter8, Parameter9);
	}

	/*----------组件数据报表框EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx DataReportBoxEx::Skin()
	{
		return (BinEx_P)GetAttribute_DataReportBoxEx(control, DataReportBoxEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void DataReportBoxEx::Skin(BindataEx Skin)
	{
		SetAttribute_DataReportBoxEx(control, DataReportBoxEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------取核心--------*/
	StringEx DataReportBoxEx::Kernel()
	{
		return (StrEx_P)GetAttribute_DataReportBoxEx(control, DataReportBoxEx_Attr_Kernel);
	}
	/*--------置核心--------*/
	void DataReportBoxEx::Kernel(StringEx Kernel)
	{
		SetAttribute_DataReportBoxEx(control, DataReportBoxEx_Attr_Kernel, (AutoInt)(StrEx_P)Kernel);
	}
	/*--------取属性--------*/
	StringEx DataReportBoxEx::Attribute()
	{
		return (StrEx_P)GetAttribute_DataReportBoxEx(control, DataReportBoxEx_Attr_Attribute);
	}
	/*--------置属性--------*/
	void DataReportBoxEx::Attribute(StringEx Attribute)
	{
		SetAttribute_DataReportBoxEx(control, DataReportBoxEx_Attr_Attribute, (AutoInt)(StrEx_P)Attribute);
	}
	/*--------取数据--------*/
	StringEx DataReportBoxEx::Data()
	{
		return (StrEx_P)GetAttribute_DataReportBoxEx(control, DataReportBoxEx_Attr_Data);
	}
	/*--------置数据--------*/
	void DataReportBoxEx::Data(StringEx Data)
	{
		SetAttribute_DataReportBoxEx(control, DataReportBoxEx_Attr_Data, (AutoInt)(StrEx_P)Data);
	}


	/*--------取扩展元素--------*/
	BindataEx DataReportBoxEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_DataReportBoxEx(control, DataReportBoxEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void DataReportBoxEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_DataReportBoxEx(control, DataReportBoxEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx DataReportBoxEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_DataReportBoxEx(control, DataReportBoxEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void DataReportBoxEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_DataReportBoxEx(control, DataReportBoxEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}


	/*----------组件数据报表框EX属性读写函数结束--------*/


}
//PageNavigBarEx
namespace exui
{
	PageNavigBarEx::PageNavigBarEx()
	{

	}
	PageNavigBarEx::~PageNavigBarEx()
	{

	}
	/*--------创建组件_分页导航条EX--------*/
	PageNavigBarEx_P PageNavigBarEx::Create(BaseEx& Parent, int LEFT, int Top, int Width, int Height, BOOL Visual, BOOL Disabled, int Transparency, int Pierced, int FocusWeight, BindataEx Cursor, AutoInt Tag, BindataEx Skin, int PageCount, int CurrentPage, int NavigBtnCount, int Align, BindataEx Font, int FontColor, int CurrentFontClour, int LeftReservation, int RightReservation, BindataEx ElemeData, BindataEx Layout)
	{
		InitCreateAttrStar(Exui_EventPro_PageNavigBarEx);
		CreateControl_PageNavigBarEx(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), LEFT, Top, Width, Height, (ExuiCallback)-1, InitAttr, Visual, Disabled, Transparency, Pierced, FocusWeight, Cursor, Tag, Skin, PageCount, CurrentPage, NavigBtnCount, Align, Font, FontColor, CurrentFontClour, LeftReservation, RightReservation, ElemeData, Layout);

		InitCreateAttrEnd(false);
		return control;
	}
	/*--------置属性_分页导航条EX--------*/
	void PageNavigBarEx::SetAttribute(AutoInt index, AutoInt attribute)
	{
		SetAttribute_PageNavigBarEx(control, index, attribute);
	}
	/*--------取属性_分页导航条EX--------*/
	AutoInt PageNavigBarEx::GetAttribute(AutoInt index)
	{
		return GetAttribute_PageNavigBarEx(control, index);
	}
	/*----------组件分页导航条EX属性读写函数开始--------*/


/*--------取皮肤--------*/
	BindataEx PageNavigBarEx::Skin()
	{
		return (BinEx_P)GetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_Skin);
	}
	/*--------置皮肤--------*/
	void PageNavigBarEx::Skin(BindataEx Skin)
	{
		SetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_Skin, (AutoInt)(BinEx_P)Skin);
	}
	/*--------模式--------*/
	int PageNavigBarEx::Mode()
	{
		return (AutoInt)GetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_Mode);
	}
	/*--------模式--------*/
	void PageNavigBarEx::Mode(int Mode)
	{
		SetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_Mode, (int)Mode);
	}
	/*--------总页数--------*/
	int PageNavigBarEx::PageCount()
	{
		return (AutoInt)GetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_PageCount);
	}
	/*--------总页数--------*/
	void PageNavigBarEx::PageCount(int PageCount)
	{
		SetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_PageCount, (int)PageCount);
	}
	/*--------当前页--------*/
	int PageNavigBarEx::CurrentPage()
	{
		return (AutoInt)GetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_CurrentPage);
	}
	/*--------当前页--------*/
	void PageNavigBarEx::CurrentPage(int CurrentPage)
	{
		SetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_CurrentPage, (int)CurrentPage);
	}
	/*--------页码数量--------*/
	int PageNavigBarEx::NavigBtnCount()
	{
		return (AutoInt)GetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_NavigBtnCount);
	}
	/*--------页码数量--------*/
	void PageNavigBarEx::NavigBtnCount(int NavigBtnCount)
	{
		SetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_NavigBtnCount, (int)NavigBtnCount);
	}
	/*--------对齐方式--------*/
	int PageNavigBarEx::Align()
	{
		return (AutoInt)GetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_Align);
	}
	/*--------对齐方式--------*/
	void PageNavigBarEx::Align(int Align)
	{
		SetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_Align, (int)Align);
	}
	/*--------取字体--------*/
	BindataEx PageNavigBarEx::Font()
	{
		return (StrEx_P)GetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_Font);
	}
	/*--------置字体--------*/
	void PageNavigBarEx::Font(BindataEx Data)
	{
		SetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_Font, (AutoInt)(BinEx_P)Data);
	}
	/*--------字体色--------*/
	int PageNavigBarEx::FontColor()
	{
		return (AutoInt)GetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_FontColor);
	}
	/*--------字体色--------*/
	void PageNavigBarEx::FontColor(int FontColor)
	{
		SetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_FontColor, (int)FontColor);
	}
	/*--------选中字体色--------*/
	int PageNavigBarEx::CurrentFontClour()
	{
		return (AutoInt)GetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_CurrentFontClour);
	}
	/*--------选中字体色--------*/
	void PageNavigBarEx::CurrentFontClour(int CurrentFontClour)
	{
		SetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_CurrentFontClour, (int)CurrentFontClour);
	}
	/*--------左预留--------*/
	int PageNavigBarEx::LeftReservation()
	{
		return (AutoInt)GetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_LeftReservation);
	}
	/*--------左预留--------*/
	void PageNavigBarEx::LeftReservation(int LeftReservation)
	{
		SetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_LeftReservation, (int)LeftReservation);
	}
	/*--------右预留--------*/
	int PageNavigBarEx::RightReservation()
	{
		return (AutoInt)GetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_RightReservation);
	}
	/*--------右预留--------*/
	void PageNavigBarEx::RightReservation(int RightReservation)
	{
		SetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_RightReservation, (int)RightReservation);
	}
	/*--------取扩展元素--------*/
	BindataEx PageNavigBarEx::ElemeData()
	{
		return (BinEx_P)GetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_ElemeData);
	}
	/*--------置扩展元素--------*/
	void PageNavigBarEx::ElemeData(BindataEx ElemeData)
	{
		SetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_ElemeData, (AutoInt)(BinEx_P)ElemeData);
	}
	/*--------取布局器--------*/
	BindataEx PageNavigBarEx::LayoutData()
	{
		return (BinEx_P)GetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_LayoutData);
	}
	/*--------置布局器--------*/
	void PageNavigBarEx::LayoutData(BindataEx LayoutData)
	{
		SetAttribute_PageNavigBarEx(control, PageNavigBarEx_Attr_LayoutData, (AutoInt)(BinEx_P)LayoutData);
	}


	/*----------组件分页导航条EX属性读写函数结束--------*/


}

//ExuiProgramEx
namespace exui
{

	ExuiProgramEx::ExuiProgramEx()
	{
		InitExui(NULL, NULL);
		controltype = ControlType_ExuiProgramEx;
	}

	ExuiProgramEx::~ExuiProgramEx()
	{


	}

	AutoInt ExuiProgramEx::Load(Control_P Parent, HWND Window, AutoInt mode, AutoInt Parameter1, AutoInt Parameter2)
	{
		AutoInt controlinfo[2] = { (AutoInt)this ,((AutoInt)this) + (AutoInt)sizeof(ExuiProgramEx) + (AutoInt)sizeof(AutoInt) };
		BinEx_P Exuidata = ReadRcDataEx(TEXT("ExuiProgramEx"), *(AutoInt*)((AutoInt)(this) + (AutoInt)sizeof(ExuiProgramEx)));
		AutoInt result = LoadControlEx(Parent, Window, mode, Parameter1, Parameter2, Exuidata, (AutoInt)Exui_EventPro_LoadControlEx, (AutoInt)controlinfo);
		DeleteBinEx(Exuidata);
		return result;
	}
	//创建主窗口
	AutoInt ExuiProgramEx::LoadMainWindow()
	{
		return	Load(NULL, NULL, 1, 9, -1);
	}
	//创建窗口
	AutoInt ExuiProgramEx::LoadWindow(BaseEx& Parent)
	{
		return	Load(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), 1, 0, -1);
	}
	//创建子窗口
	AutoInt ExuiProgramEx::LoadChildWindow(BaseEx& Parent)
	{
		return	Load(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), 1, 4, -1);
	}
	//创建弹出子窗口
	AutoInt ExuiProgramEx::LoadPopupChildWindow(BaseEx& Parent)
	{
		return	Load(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), 1, 3, -1);
	}
	//创建对话框窗口
	AutoInt ExuiProgramEx::LoadDialogWindow(BaseEx& Parent)
	{
		return	Load(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), 1, 1, -1);
	}
	//创建到组件
	AutoInt ExuiProgramEx::LoadToControl(BaseEx& Parent)
	{
		return	Load(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), 0, 0, -1);
	}
	//创建到现有窗口
	AutoInt ExuiProgramEx::LoadToWindow(BaseEx& Parent)
	{
		return	Load(GetCreateParentControl(Parent), GetCreateParentHwnd(Parent), 0, 0, -1);
	}




	/*--------信息框Ex--------*/
	int ExuiProgramEx::MsgBoxEx(BindataEx Ico, StringEx Title, BindataEx TipIco, StringEx TipTitle, StringEx Button, int DefaultBtn, HWND hWndParent, int Style, BindataEx Skin, ExuiCallback callback, AutoInt Parameter)
	{
		return 	::MsgBox_Ex(Ico, Title, TipIco, TipTitle, Button, DefaultBtn, hWndParent, Style, Skin, callback, Parameter);
	}

	/*--------输入框Ex--------*/
	StringEx ExuiProgramEx::InputBoxEx(BindataEx Ico, StringEx Title, BindataEx TipIco, StringEx TipTitle, StringEx Button, int DefaultBtn, StringEx Conten, int InputMode, StringEx PasswordSubstitution, int MaxInput, BOOL Multiline, AutoInt* SelectResult, HWND hWndParent, int Style, BindataEx Skin, ExuiCallback callback, AutoInt Parameter_)
	{
		return 	::InputBox_Ex(Ico, Title, TipIco, TipTitle, Button, DefaultBtn, Conten, InputMode, PasswordSubstitution, MaxInput, Multiline, SelectResult, hWndParent, Style, Skin, callback, Parameter_);

	}

	/*--------弹出提示框Ex--------*/
	int ExuiProgramEx::PopUpTipBoxEx(BindataEx Ico, StringEx TipTitle, int PopUpMode, int x, int y, int width, int height, int AutoCloseTime, int Angle, int BackColor, int BorderColor, int LineWidth, BindataEx Font, int FontColor, int Align, int Transparency, int ReferType, Control_P ReferControl)
	{
		return 	::PopUpTipBoxEx(Ico, TipTitle, PopUpMode, x, y, width, height, AutoCloseTime, Angle, BackColor, BorderColor, LineWidth, Font, FontColor, Align, Transparency, ReferType, ReferControl);

	}

	/*--------关闭提示框Ex--------*/
	void ExuiProgramEx::CloseTipBoxEx()
	{
		return	::CloseTipBoxEx();
	}


	/*--------时间选择框--------*/
	StringEx ExuiProgramEx::TimePickBoxEx(BindataEx Ico, StringEx TipTitle, StringEx Button, int DefaultBtn, int Width, int Height, StringEx SelectionDate, StringEx MiniDate, StringEx MaxDate, BOOL OtherMonthClour, int TimeMode, int Language, AutoInt* SelectResult, HWND hWndParent, int Style, BindataEx Skin, ExuiCallback Callback, AutoInt Parameter_)
	{
		return	 ::TimePickBox_Ex(Ico, TipTitle, Button, DefaultBtn, Width, Height, SelectionDate, MiniDate, MaxDate, OtherMonthClour, TimeMode, Language, SelectResult, hWndParent, Style, Skin, Callback, Parameter_);
	}

	/*--------颜色选择框--------*/
	int ExuiProgramEx::ColorPickExBoxEx(BindataEx Ico, StringEx TipTitle, StringEx Button, int DefaultBtn, int Width, int Height, int NowClour, BindataEx QuickClours, int ClourMode, int ColorPickStyle, AutoInt* SelectResult, HWND hWndParent, int Style, BindataEx Skin, ExuiCallback Callback, AutoInt Parameter_)
	{
		return	 ::ColorPickExBox_Ex(Ico, TipTitle, Button, DefaultBtn, Width, Height, NowClour, QuickClours, ClourMode, ColorPickStyle, SelectResult, hWndParent, Style, Skin, Callback, Parameter_);
	}

	/*--------弹出时间选择器--------*/
	StringEx ExuiProgramEx::PopUpTimePickEx(int LEFT, int Top, int Width, int Height, StringEx Button, int DefaultBtn, StringEx SelectionDate, StringEx MiniDate, StringEx MaxDate, BOOL OnlyThisMonth, int TimeMode, int Language, AutoInt* SelectResult, HWND hWndParent, int Style, BindataEx Skin, ExuiCallback Callback, AutoInt Parameter_, int ReferType, Control_P ReferControl)
	{
		return ::PopUpTimePick_Ex(LEFT, Top, Width, Height, Button, DefaultBtn, SelectionDate, MiniDate, MaxDate, OnlyThisMonth, TimeMode, Language, SelectResult, hWndParent, Style, Skin, Callback, Parameter_, ReferType, ReferControl);
	}

	/*--------弹出颜色选择器--------*/
	int ExuiProgramEx::PopUpColorPickEx(int LEFT, int Top, int Width, int Height, StringEx Button, int DefaultBtn, int NowClour, BindataEx QuickClours, int ClourMode, int ColorPickStyle, AutoInt* SelectResult, HWND hWndParent, int Style, BindataEx Skin, ExuiCallback Callback, AutoInt Parameter_, int ReferType, Control_P ReferControl)
	{
		return ::PopUpColorPick_Ex(LEFT, Top, Width, Height, Button, DefaultBtn, NowClour, QuickClours, ClourMode, ColorPickStyle, SelectResult, hWndParent, Style, Skin, Callback, Parameter_, ReferType, ReferControl);
	}


	/*--------弹出通知框Ex--------*/
	AutoInt ExuiProgramEx::PopUpNotificationBoxEx(BindataEx Ico, StringEx Title, BindataEx TipIco, StringEx TipTitle, StringEx Button, int Style, int PopUpMode, Control_P Parent, int AutoCloseTime, int Reserve, BindataEx Skin, ExuiCallback callback, AutoInt Parameter)
	{
		return 	::PopUpNotificationBoxEx(Ico, Title, TipIco, TipTitle, Button, Style, PopUpMode, Parent, AutoCloseTime, Reserve, Skin, callback, Parameter);
	}

	/*--------关闭通知框Ex--------*/
	void ExuiProgramEx::CloseNotificationBoxEx(AutoInt NotificationBoxHandle)
	{
		return	::CloseNotificationBoxEx(NotificationBoxHandle);
	}

	/*--------测量文本--------*/
	BindataEx ExuiProgramEx::RexMeasureStringEx(StringEx string, BindataEx Fontex, AutoInt StringFormat, float x, float y, float width, float height, int mode)
	{
		return ::RexMeasureStringEx(string, Fontex, StringFormat, x, y, width, height, mode);
	}
	
	/*--------读事件参数数据Ex--------*/
	BindataEx ExuiProgramEx::ReadEventParamDataEx(AutoInt DataPtr, AutoInt DataType)
	{
		return ::ReadEventParamDataEx(DataPtr, DataType);
	}
	/*--------写事件参数数据Ex--------*/
	void  ExuiProgramEx::WriteEventParamDataEx(AutoInt DataPtr, BindataEx Data, AutoInt DataType)
	{
		::WriteEventParamDataEx(DataPtr, Data, DataType);
	}

	/*--------打包事件返回数据Ex--------*/
	AutoInt  ExuiProgramEx::PackEventReturnDataEx(BindataEx Data, AutoInt DataType)
	{
		return ::PackEventReturnDataEx(Data, DataType);
	}
	/*--------解析事件返回数据Ex--------*/
	BindataEx  ExuiProgramEx::UnpackEventReturnDataEx(AutoInt DataPtr, AutoInt DataType)
	{
		return ::UnpackEventReturnDataEx(DataPtr, DataType);
	}

	/*--------读事件参数文本Ex--------*/
	StringEx ExuiProgramEx::ReadEventParamTextEx(AutoInt TextPtr, AutoInt DataType)
	{
		return ::ReadEventParamTextEx(TextPtr, DataType);
	}
	/*--------写事件参数文本Ex--------*/
	void  ExuiProgramEx::WriteEventParamTextEx(AutoInt TextPtr, StringEx Text, AutoInt DataType)
	{
		 ::WriteEventParamTextEx(TextPtr, Text, DataType);
	}

	/*--------打包事件返回文本Ex--------*/
	AutoInt  ExuiProgramEx::PackEventReturnTextEx(StringEx Text, AutoInt DataType)
	{
		return ::PackEventReturnTextEx(Text, DataType);
	}
	/*--------解析事件返回文本Ex--------*/
	StringEx  ExuiProgramEx::UnpackEventReturnTextEx(AutoInt TextPtr, AutoInt DataType)
	{
		return ::UnpackEventReturnTextEx(TextPtr, DataType);
	}



}