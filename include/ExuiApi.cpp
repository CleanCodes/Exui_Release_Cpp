#include "ExuiApi.h"

typedef void* HMEMORYRSRC;

typedef void* HCUSTOMMODULE;

typedef LPVOID(*CustomAllocFunc)(LPVOID, SIZE_T, DWORD, DWORD, void*);
typedef BOOL(*CustomFreeFunc)(LPVOID, SIZE_T, DWORD, void*);
typedef HCUSTOMMODULE(*CustomLoadLibraryFunc)(LPCSTR, void*);
typedef FARPROC(*CustomGetProcAddressFunc)(HCUSTOMMODULE, LPCSTR, void*);
typedef void (*CustomFreeLibraryFunc)(HCUSTOMMODULE, void*);


HINSTANCE  __stdcall  MemoryLoadLibrary_Exui(const void*, size_t);
HINSTANCE MemoryLoadLibraryEx_Exui(const void*, size_t, CustomAllocFunc, CustomFreeFunc, CustomLoadLibraryFunc, CustomGetProcAddressFunc, CustomFreeLibraryFunc, void*);
FARPROC  __stdcall MemoryGetProcAddress_Exui(HINSTANCE, LPCSTR);
BOOL  __stdcall MemoryFreeLibrary_Exui(HINSTANCE);

#ifdef _WIN64
typedef unsigned __int64  uintptr_t;
#else
typedef unsigned int uintptr_t;
#endif

#ifdef _WIN64
typedef unsigned __int64 size_t;
typedef __int64          ptrdiff_t;
typedef __int64          intptr_t;
#else
typedef unsigned int     size_t;
typedef int              ptrdiff_t;
typedef int              intptr_t;
#endif

#ifndef IMAGE_SIZEOF_BASE_RELOCATION
// Vista SDKs no longer define IMAGE_SIZEOF_BASE_RELOCATION!?
#define IMAGE_SIZEOF_BASE_RELOCATION (sizeof(IMAGE_BASE_RELOCATION))
#endif

#ifdef _WIN64
#define HOST_MACHINE IMAGE_FILE_MACHINE_AMD64
#else
#define HOST_MACHINE IMAGE_FILE_MACHINE_I386
#endif

struct ExportNameEntry_Exui {
	LPCSTR name;
	WORD idx;
};

typedef BOOL(WINAPI* DllEntryProc_Exui)(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpReserved);
typedef int (WINAPI* ExeEntryProc_Exui)(void);

#ifdef _WIN64
typedef struct POINTER_LIST_Exui {
	struct POINTER_LIST_Exui* next;
	void* address;
} POINTER_LIST_Exui;
#endif

typedef struct {
	PIMAGE_NT_HEADERS headers;
	unsigned char* codeBase;
	HCUSTOMMODULE* modules;
	int numModules;
	BOOL initialized;
	BOOL isDLL;
	BOOL isRelocated;
	CustomAllocFunc CustomAlloc;
	CustomFreeFunc CustomFree;
	CustomLoadLibraryFunc loadLibrary;
	CustomGetProcAddressFunc getProcAddress;
	CustomFreeLibraryFunc freeLibrary;
	struct ExportNameEntry_Exui* nameExportsTable;
	void* userdata;
	ExeEntryProc_Exui exeEntry;
	DWORD pageSize;
#ifdef _WIN64
	POINTER_LIST_Exui* blockedMemory;
#endif
} MEMORYMODULE_Exui, * PMEMORYMODULE_Exui;

typedef struct {
	LPVOID address;
	LPVOID alignedAddress;
	SIZE_T size;
	DWORD characteristics;
	BOOL last;
} SECTIONFINALIZEDATA_Exui, * PSECTIONFINALIZEDATA_Exui;

#define GET_HEADER_DICTIONARY_Exui(module, idx)  &(module)->headers->OptionalHeader.DataDirectory[idx]


static  uintptr_t AlignValueDown_Exui(uintptr_t value, uintptr_t alignment) {
	return value & ~(alignment - 1);
}

static inline LPVOID AlignAddressDown_Exui(LPVOID address, uintptr_t alignment) {
	return (LPVOID)AlignValueDown_Exui((uintptr_t)address, alignment);
}

static inline size_t AlignValueUp_Exui(size_t value, size_t alignment) {
	return (value + alignment - 1) & ~(alignment - 1);
}

static inline void* OffsetPointer_Exui(void* data, ptrdiff_t offset) {
	return (void*)((uintptr_t)data + offset);
}

static inline void OutputLastError_Exui(const char* msg)
{

}

#ifdef _WIN64
static void FreePointerList_Exui(POINTER_LIST_Exui* head, CustomFreeFunc freeMemory, void* userdata)
{
	POINTER_LIST_Exui* node = head;
	while (node) {
		POINTER_LIST_Exui* next;
		freeMemory(node->address, 0, MEM_RELEASE, userdata);
		next = node->next;
		free(node);
		node = next;
	}
}
#endif

static BOOL CheckSize_Exui(size_t size, size_t expected) {
	if (size < expected) {
		SetLastError(ERROR_INVALID_DATA);
		return FALSE;
	}

	return TRUE;
}

static BOOL CopySections_Exui(const unsigned char* data, size_t size, PIMAGE_NT_HEADERS old_headers, PMEMORYMODULE_Exui module)
{
	int i, section_size;
	unsigned char* codeBase = module->codeBase;
	unsigned char* dest;
	PIMAGE_SECTION_HEADER section = IMAGE_FIRST_SECTION(module->headers);
	for (i = 0; i < module->headers->FileHeader.NumberOfSections; i++, section++) {
		if (section->SizeOfRawData == 0) {
			// section doesn't contain data in the dll itself, but may define
			// uninitialized data
			section_size = old_headers->OptionalHeader.SectionAlignment;
			if (section_size > 0) {
				dest = (unsigned char*)module->CustomAlloc(codeBase + section->VirtualAddress,
					section_size,
					MEM_COMMIT,
					PAGE_READWRITE,
					module->userdata);
				if (dest == NULL) {
					return FALSE;
				}

				// Always use position from file to support alignments smaller
				// than page size (allocation above will align to page size).
				dest = codeBase + section->VirtualAddress;
				// NOTE: On 64bit systems we truncate to 32bit here but expand
				// again later when "PhysicalAddress" is used.
				section->Misc.PhysicalAddress = (DWORD)((uintptr_t)dest & 0xffffffff);
				memset(dest, 0, section_size);
			}

			// section is empty
			continue;
		}

		if (!CheckSize_Exui(size, section->PointerToRawData + section->SizeOfRawData)) {
			return FALSE;
		}

		// commit memory block and copy data from dll
		dest = (unsigned char*)module->CustomAlloc(codeBase + section->VirtualAddress,
			section->SizeOfRawData,
			MEM_COMMIT,
			PAGE_READWRITE,
			module->userdata);
		if (dest == NULL) {
			return FALSE;
		}

		// Always use position from file to support alignments smaller
		// than page size (allocation above will align to page size).
		dest = codeBase + section->VirtualAddress;
		memcpy(dest, data + section->PointerToRawData, section->SizeOfRawData);
		// NOTE: On 64bit systems we truncate to 32bit here but expand
		// again later when "PhysicalAddress" is used.
		section->Misc.PhysicalAddress = (DWORD)((uintptr_t)dest & 0xffffffff);
	}

	return TRUE;
}

// Protection flags for memory pages (Executable, Readable, Writeable)
static int ProtectionFlags_Exui[2][2][2] = {
	{
		// not executable
		{PAGE_NOACCESS, PAGE_WRITECOPY},
		{PAGE_READONLY, PAGE_READWRITE},
	}, {
		// executable
		{PAGE_EXECUTE, PAGE_EXECUTE_WRITECOPY},
		{PAGE_EXECUTE_READ, PAGE_EXECUTE_READWRITE},
	},
};

static SIZE_T GetRealSectionSize_Exui(PMEMORYMODULE_Exui module, PIMAGE_SECTION_HEADER section) {
	DWORD size = section->SizeOfRawData;
	if (size == 0) {
		if (section->Characteristics & IMAGE_SCN_CNT_INITIALIZED_DATA) {
			size = module->headers->OptionalHeader.SizeOfInitializedData;
		}
		else if (section->Characteristics & IMAGE_SCN_CNT_UNINITIALIZED_DATA) {
			size = module->headers->OptionalHeader.SizeOfUninitializedData;
		}
	}
	return (SIZE_T)size;
}

static BOOL FinalizeSection_Exui(PMEMORYMODULE_Exui module, PSECTIONFINALIZEDATA_Exui sectionData) {
	DWORD protect, oldProtect;
	BOOL executable;
	BOOL readable;
	BOOL writeable;

	if (sectionData->size == 0) {
		return TRUE;
	}

	if (sectionData->characteristics & IMAGE_SCN_MEM_DISCARDABLE) {
		// section is not needed any more and can safely be freed
		if (sectionData->address == sectionData->alignedAddress &&
			(sectionData->last ||
				module->headers->OptionalHeader.SectionAlignment == module->pageSize ||
				(sectionData->size % module->pageSize) == 0)
			) {
			// Only allowed to decommit whole pages
			module->CustomFree(sectionData->address, sectionData->size, MEM_DECOMMIT, module->userdata);
		}
		return TRUE;
	}

	// determine protection flags based on characteristics
	executable = (sectionData->characteristics & IMAGE_SCN_MEM_EXECUTE) != 0;
	readable = (sectionData->characteristics & IMAGE_SCN_MEM_READ) != 0;
	writeable = (sectionData->characteristics & IMAGE_SCN_MEM_WRITE) != 0;
	protect = ProtectionFlags_Exui[executable][readable][writeable];
	if (sectionData->characteristics & IMAGE_SCN_MEM_NOT_CACHED) {
		protect |= PAGE_NOCACHE;
	}

	// change memory access flags
	if (VirtualProtect(sectionData->address, sectionData->size, protect, &oldProtect) == 0) {
		OutputLastError_Exui("Error protecting memory page");
		return FALSE;
	}

	return TRUE;
}

static BOOL FinalizeSections_Exui(PMEMORYMODULE_Exui module)
{
	int i;
	PIMAGE_SECTION_HEADER section = IMAGE_FIRST_SECTION(module->headers);
#ifdef _WIN64
	// "PhysicalAddress" might have been truncated to 32bit above, expand to
	// 64bits again.
	uintptr_t imageOffset = ((uintptr_t)module->headers->OptionalHeader.ImageBase & 0xffffffff00000000);
#else
	static const uintptr_t imageOffset = 0;
#endif
	SECTIONFINALIZEDATA_Exui sectionData;
	sectionData.address = (LPVOID)((uintptr_t)section->Misc.PhysicalAddress | imageOffset);
	sectionData.alignedAddress = AlignAddressDown_Exui(sectionData.address, module->pageSize);
	sectionData.size = GetRealSectionSize_Exui(module, section);
	sectionData.characteristics = section->Characteristics;
	sectionData.last = FALSE;
	section++;

	// loop through all sections and change access flags
	for (i = 1; i < module->headers->FileHeader.NumberOfSections; i++, section++) {
		LPVOID sectionAddress = (LPVOID)((uintptr_t)section->Misc.PhysicalAddress | imageOffset);
		LPVOID alignedAddress = AlignAddressDown_Exui(sectionAddress, module->pageSize);
		SIZE_T sectionSize = GetRealSectionSize_Exui(module, section);
		// Combine access flags of all sections that share a page
		// TODO(fancycode): We currently share flags of a trailing large section
		//   with the page of a first small section. This should be optimized.
		if (sectionData.alignedAddress == alignedAddress || (uintptr_t)sectionData.address + sectionData.size > (uintptr_t) alignedAddress) {
			// Section shares page with previous
			if ((section->Characteristics & IMAGE_SCN_MEM_DISCARDABLE) == 0 || (sectionData.characteristics & IMAGE_SCN_MEM_DISCARDABLE) == 0) {
				sectionData.characteristics = (sectionData.characteristics | section->Characteristics) & ~IMAGE_SCN_MEM_DISCARDABLE;
			}
			else {
				sectionData.characteristics |= section->Characteristics;
			}
			sectionData.size = (((uintptr_t)sectionAddress) + ((uintptr_t)sectionSize)) - (uintptr_t)sectionData.address;
			continue;
		}

		if (!FinalizeSection_Exui(module, &sectionData)) {
			return FALSE;
		}
		sectionData.address = sectionAddress;
		sectionData.alignedAddress = alignedAddress;
		sectionData.size = sectionSize;
		sectionData.characteristics = section->Characteristics;
	}
	sectionData.last = TRUE;
	if (!FinalizeSection_Exui(module, &sectionData)) {
		return FALSE;
	}
	return TRUE;
}

static BOOL ExecuteTLS_Exui(PMEMORYMODULE_Exui module)
{
	unsigned char* codeBase = module->codeBase;
	PIMAGE_TLS_DIRECTORY tls;
	PIMAGE_TLS_CALLBACK* callback;

	PIMAGE_DATA_DIRECTORY directory = GET_HEADER_DICTIONARY_Exui(module, IMAGE_DIRECTORY_ENTRY_TLS);
	if (directory->VirtualAddress == 0) {
		return TRUE;
	}

	tls = (PIMAGE_TLS_DIRECTORY)(codeBase + directory->VirtualAddress);
	callback = (PIMAGE_TLS_CALLBACK*)tls->AddressOfCallBacks;
	if (callback) {
		while (*callback) {
			(*callback)((LPVOID)codeBase, DLL_PROCESS_ATTACH, NULL);
			callback++;
		}
	}
	return TRUE;
}

static BOOL PerformBaseRelocation_Exui(PMEMORYMODULE_Exui module, ptrdiff_t delta)
{
	unsigned char* codeBase = module->codeBase;
	PIMAGE_BASE_RELOCATION relocation;

	PIMAGE_DATA_DIRECTORY directory = GET_HEADER_DICTIONARY_Exui(module, IMAGE_DIRECTORY_ENTRY_BASERELOC);
	if (directory->Size == 0) {
		return (delta == 0);
	}

	relocation = (PIMAGE_BASE_RELOCATION)(codeBase + directory->VirtualAddress);
	for (; relocation->VirtualAddress > 0; ) {
		DWORD i;
		unsigned char* dest = codeBase + relocation->VirtualAddress;
		unsigned short* relInfo = (unsigned short*)OffsetPointer_Exui(relocation, IMAGE_SIZEOF_BASE_RELOCATION);
		for (i = 0; i < ((relocation->SizeOfBlock - IMAGE_SIZEOF_BASE_RELOCATION) / 2); i++, relInfo++) {
			// the upper 4 bits define the type of relocation
			int type = *relInfo >> 12;
			// the lower 12 bits define the offset
			int offset = *relInfo & 0xfff;

			switch (type)
			{
			case IMAGE_REL_BASED_ABSOLUTE:
				// skip relocation
				break;

			case IMAGE_REL_BASED_HIGHLOW:
				// change complete 32 bit address
			{
				DWORD* patchAddrHL = (DWORD*)(dest + offset);
				*patchAddrHL += (DWORD)delta;
			}
			break;

#ifdef _WIN64
			case IMAGE_REL_BASED_DIR64:
			{
				ULONGLONG* patchAddr64 = (ULONGLONG*)(dest + offset);
				*patchAddr64 += (ULONGLONG)delta;
			}
			break;
#endif

			default:
				//printf("Unknown relocation: %d\n", type);
				break;
			}
		}

		// advance to next relocation block
		relocation = (PIMAGE_BASE_RELOCATION)OffsetPointer_Exui(relocation, relocation->SizeOfBlock);
	}
	return TRUE;
}

static BOOL BuildImportTable_Exui(PMEMORYMODULE_Exui module)
{
	unsigned char* codeBase = module->codeBase;
	PIMAGE_IMPORT_DESCRIPTOR importDesc;
	BOOL result = TRUE;

	PIMAGE_DATA_DIRECTORY directory = GET_HEADER_DICTIONARY_Exui(module, IMAGE_DIRECTORY_ENTRY_IMPORT);
	if (directory->Size == 0) {
		return TRUE;
	}

	importDesc = (PIMAGE_IMPORT_DESCRIPTOR)(codeBase + directory->VirtualAddress);
	for (; !IsBadReadPtr(importDesc, sizeof(IMAGE_IMPORT_DESCRIPTOR)) && importDesc->Name; importDesc++) {
		uintptr_t* thunkRef;
		FARPROC* funcRef;
		HCUSTOMMODULE* tmp;
		HCUSTOMMODULE handle = module->loadLibrary((LPCSTR)(codeBase + importDesc->Name), module->userdata);
		if (handle == NULL) {
			SetLastError(ERROR_MOD_NOT_FOUND);
			result = FALSE;
			break;
		}

		tmp = (HCUSTOMMODULE*)realloc(module->modules, (module->numModules + 1) * (sizeof(HCUSTOMMODULE)));
		if (tmp == NULL) {
			module->freeLibrary(handle, module->userdata);
			SetLastError(ERROR_OUTOFMEMORY);
			result = FALSE;
			break;
		}
		module->modules = tmp;

		module->modules[module->numModules++] = handle;
		if (importDesc->OriginalFirstThunk) {
			thunkRef = (uintptr_t*)(codeBase + importDesc->OriginalFirstThunk);
			funcRef = (FARPROC*)(codeBase + importDesc->FirstThunk);
		}
		else {
			// no hint table
			thunkRef = (uintptr_t*)(codeBase + importDesc->FirstThunk);
			funcRef = (FARPROC*)(codeBase + importDesc->FirstThunk);
		}
		for (; *thunkRef; thunkRef++, funcRef++) {
			if (IMAGE_SNAP_BY_ORDINAL(*thunkRef)) {
				*funcRef = module->getProcAddress(handle, (LPCSTR)IMAGE_ORDINAL(*thunkRef), module->userdata);
			}
			else {
				PIMAGE_IMPORT_BY_NAME thunkData = (PIMAGE_IMPORT_BY_NAME)(codeBase + (*thunkRef));
				*funcRef = module->getProcAddress(handle, (LPCSTR)&thunkData->Name, module->userdata);
			}
			if (*funcRef == 0) {
				result = FALSE;
				break;
			}
		}

		if (!result) {
			module->freeLibrary(handle, module->userdata);
			SetLastError(ERROR_PROC_NOT_FOUND);
			break;
		}
	}

	return result;
}

LPVOID MemoryDefaultAlloc_Exui(LPVOID address, SIZE_T size, DWORD allocationType, DWORD protect, void* userdata)
{
	UNREFERENCED_PARAMETER(userdata);
	return VirtualAlloc(address, size, allocationType, protect);
}

BOOL MemoryDefaultFree_Exui(LPVOID lpAddress, SIZE_T dwSize, DWORD dwFreeType, void* userdata)
{
	UNREFERENCED_PARAMETER(userdata);
	return VirtualFree(lpAddress, dwSize, dwFreeType);
}

HCUSTOMMODULE MemoryDefaultLoadLibrary_Exui(LPCSTR filename, void* userdata)
{
	HMODULE result;
	UNREFERENCED_PARAMETER(userdata);
	result = LoadLibraryA(filename);
	if (result == NULL) {
		return NULL;
	}

	return (HCUSTOMMODULE)result;
}

FARPROC MemoryDefaultGetProcAddress_Exui(HCUSTOMMODULE module, LPCSTR name, void* userdata)
{
	UNREFERENCED_PARAMETER(userdata);
	return (FARPROC)GetProcAddress((HMODULE)module, name);
}

void MemoryDefaultFreeLibrary_Exui(HCUSTOMMODULE module, void* userdata)
{
	UNREFERENCED_PARAMETER(userdata);
	FreeLibrary((HMODULE)module);
}

HINSTANCE  __stdcall  MemoryLoadLibrary_Exui(const void* data, size_t size)
{
	return MemoryLoadLibraryEx_Exui(data, size, MemoryDefaultAlloc_Exui, MemoryDefaultFree_Exui, MemoryDefaultLoadLibrary_Exui, MemoryDefaultGetProcAddress_Exui, MemoryDefaultFreeLibrary_Exui, NULL);
}

HINSTANCE    MemoryLoadLibraryEx_Exui(const void* data, size_t size, CustomAllocFunc allocMemory, CustomFreeFunc freeMemory, CustomLoadLibraryFunc loadLibrary, CustomGetProcAddressFunc getProcAddress, CustomFreeLibraryFunc freeLibrary, void* userdata)
{
	PMEMORYMODULE_Exui result = NULL;
	PIMAGE_DOS_HEADER dos_header;
	PIMAGE_NT_HEADERS old_header;
	unsigned char* code, * headers;
	ptrdiff_t locationDelta;
	SYSTEM_INFO sysInfo;
	PIMAGE_SECTION_HEADER section;
	DWORD i;
	size_t optionalSectionSize;
	size_t lastSectionEnd = 0;
	size_t alignedImageSize;
#ifdef _WIN64
	POINTER_LIST_Exui* blockedMemory = NULL;
#endif

	if (!CheckSize_Exui(size, sizeof(IMAGE_DOS_HEADER))) {
		return NULL;
	}
	dos_header = (PIMAGE_DOS_HEADER)data;
	if (dos_header->e_magic != IMAGE_DOS_SIGNATURE) {
		SetLastError(ERROR_BAD_EXE_FORMAT);
		return NULL;
	}

	if (!CheckSize_Exui(size, dos_header->e_lfanew + sizeof(IMAGE_NT_HEADERS))) {
		return NULL;
	}
	old_header = (PIMAGE_NT_HEADERS) & ((const unsigned char*)(data))[dos_header->e_lfanew];
	if (old_header->Signature != IMAGE_NT_SIGNATURE) {
		SetLastError(ERROR_BAD_EXE_FORMAT);
		return NULL;
	}

	if (old_header->FileHeader.Machine != HOST_MACHINE) {
		SetLastError(ERROR_BAD_EXE_FORMAT);
		return NULL;
	}

	if (old_header->OptionalHeader.SectionAlignment & 1) {
		// Only support section alignments that are a multiple of 2
		SetLastError(ERROR_BAD_EXE_FORMAT);
		return NULL;
	}

	section = IMAGE_FIRST_SECTION(old_header);
	optionalSectionSize = old_header->OptionalHeader.SectionAlignment;
	for (i = 0; i < old_header->FileHeader.NumberOfSections; i++, section++) {
		size_t endOfSection;
		if (section->SizeOfRawData == 0) {
			// Section without data in the DLL
			endOfSection = section->VirtualAddress + optionalSectionSize;
		}
		else {
			endOfSection = section->VirtualAddress + section->SizeOfRawData;
		}

		if (endOfSection > lastSectionEnd) {
			lastSectionEnd = endOfSection;
		}
	}

#if _WIN32_WINNT >= 0x0501
	//	GetNativeSystemInfo(&sysInfo);
#else
	//	GetSystemInfo(&sysInfo);
#endif

	GetSystemInfo(&sysInfo);
	alignedImageSize = AlignValueUp_Exui(old_header->OptionalHeader.SizeOfImage, sysInfo.dwPageSize);
	if (alignedImageSize != AlignValueUp_Exui(lastSectionEnd, sysInfo.dwPageSize)) {
		SetLastError(ERROR_BAD_EXE_FORMAT);
		return NULL;
	}

	// reserve memory for image of library
	// XXX: is it correct to commit the complete memory region at once?
	// calling DllEntry raises an exception if we don't...
	code = (unsigned char*)allocMemory((LPVOID)(old_header->OptionalHeader.ImageBase), alignedImageSize, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE, userdata);

	if (code == NULL) {
		// try to allocate memory at arbitrary position
		code = (unsigned char*)allocMemory(NULL,
			alignedImageSize,
			MEM_RESERVE | MEM_COMMIT,
			PAGE_READWRITE,
			userdata);
		if (code == NULL) {
			SetLastError(ERROR_OUTOFMEMORY);
			return NULL;
		}
	}

#ifdef _WIN64
	// Memory block may not span 4 GB boundaries.
	while ((((uintptr_t)code) >> 32) < (((uintptr_t)(code + alignedImageSize)) >> 32)) {
		POINTER_LIST_Exui* node = (POINTER_LIST_Exui*)malloc(sizeof(POINTER_LIST_Exui));
		if (!node) {
			freeMemory(code, 0, MEM_RELEASE, userdata);
			FreePointerList_Exui(blockedMemory, freeMemory, userdata);
			SetLastError(ERROR_OUTOFMEMORY);
			return NULL;
		}

		node->next = blockedMemory;
		node->address = code;
		blockedMemory = node;

		code = (unsigned char*)allocMemory(NULL,
			alignedImageSize,
			MEM_RESERVE | MEM_COMMIT,
			PAGE_READWRITE,
			userdata);
		if (code == NULL) {
			FreePointerList_Exui(blockedMemory, freeMemory, userdata);
			SetLastError(ERROR_OUTOFMEMORY);
			return NULL;
		}
	}
#endif

	result = (PMEMORYMODULE_Exui)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(MEMORYMODULE_Exui));
	if (result == NULL) {
		freeMemory(code, 0, MEM_RELEASE, userdata);
#ifdef _WIN64
		FreePointerList_Exui(blockedMemory, freeMemory, userdata);
#endif
		SetLastError(ERROR_OUTOFMEMORY);
		return NULL;
	}

	result->codeBase = code;
	result->isDLL = (old_header->FileHeader.Characteristics & IMAGE_FILE_DLL) != 0;
	result->CustomAlloc = allocMemory;
	result->CustomFree = freeMemory;
	result->loadLibrary = loadLibrary;
	result->getProcAddress = getProcAddress;
	result->freeLibrary = freeLibrary;
	result->userdata = userdata;
	result->pageSize = sysInfo.dwPageSize;
#ifdef _WIN64
	result->blockedMemory = blockedMemory;
#endif

	if (!CheckSize_Exui(size, old_header->OptionalHeader.SizeOfHeaders)) {
		goto error;
	}

	// commit memory for headers
	headers = (unsigned char*)allocMemory(code,
		old_header->OptionalHeader.SizeOfHeaders,
		MEM_COMMIT,
		PAGE_READWRITE,
		userdata);

	// copy PE header to code
	memcpy(headers, dos_header, old_header->OptionalHeader.SizeOfHeaders);
	result->headers = (PIMAGE_NT_HEADERS) & ((const unsigned char*)(headers))[dos_header->e_lfanew];

	// update position
	result->headers->OptionalHeader.ImageBase = (uintptr_t)code;

	// copy sections from DLL file block to new memory location
	if (!CopySections_Exui((const unsigned char*)data, size, old_header, result)) {
		goto error;
	}

	// adjust base address of imported data
	locationDelta = (ptrdiff_t)(result->headers->OptionalHeader.ImageBase - old_header->OptionalHeader.ImageBase);
	if (locationDelta != 0) {
		result->isRelocated = PerformBaseRelocation_Exui(result, locationDelta);
	}
	else {
		result->isRelocated = TRUE;
	}

	// load required dlls and adjust function table of imports
	if (!BuildImportTable_Exui(result)) {
		goto error;
	}

	// mark memory pages depending on section headers and release
	// sections that are marked as "discardable"
	if (!FinalizeSections_Exui(result)) {
		goto error;
	}

	// TLS callbacks are executed BEFORE the main loading
	if (!ExecuteTLS_Exui(result)) {
		goto error;
	}

	// get entry point of loaded library
	if (result->headers->OptionalHeader.AddressOfEntryPoint != 0) {
		if (result->isDLL) {
			DllEntryProc_Exui DllEntry = (DllEntryProc_Exui)(LPVOID)(code + result->headers->OptionalHeader.AddressOfEntryPoint);
			// notify library about attaching to process
			BOOL successfull = (*DllEntry)((HINSTANCE)code, DLL_PROCESS_ATTACH, 0);
			if (!successfull) {
				SetLastError(ERROR_DLL_INIT_FAILED);
				goto error;
			}
			result->initialized = TRUE;
		}
		else {
			result->exeEntry = (ExeEntryProc_Exui)(LPVOID)(code + result->headers->OptionalHeader.AddressOfEntryPoint);
		}
	}
	else {
		result->exeEntry = NULL;
	}

	return (HINSTANCE)result;

error:
	// cleanup
	MemoryFreeLibrary_Exui((HINSTANCE)result);
	return NULL;
}

static int _compare_Exui(const void* a, const void* b)
{
	const struct ExportNameEntry_Exui* p1 = (const struct ExportNameEntry_Exui*)a;
	const struct ExportNameEntry_Exui* p2 = (const struct ExportNameEntry_Exui*)b;
	return strcmp(p1->name, p2->name);
}

static int _find_Exui(const void* a, const void* b)
{
	LPCSTR* name = (LPCSTR*)a;
	const struct ExportNameEntry_Exui* p = (const struct ExportNameEntry_Exui*)b;
	return strcmp(*name, p->name);
}

FARPROC  __stdcall  MemoryGetProcAddress_Exui(HINSTANCE mod, LPCSTR name)
{
	PMEMORYMODULE_Exui module = (PMEMORYMODULE_Exui)mod;
	unsigned char* codeBase = module->codeBase;
	DWORD idx = 0;
	PIMAGE_EXPORT_DIRECTORY exports;
	PIMAGE_DATA_DIRECTORY directory = GET_HEADER_DICTIONARY_Exui(module, IMAGE_DIRECTORY_ENTRY_EXPORT);
	if (directory->Size == 0) {
		// no export table found
		SetLastError(ERROR_PROC_NOT_FOUND);
		return NULL;
	}

	exports = (PIMAGE_EXPORT_DIRECTORY)(codeBase + directory->VirtualAddress);
	if (exports->NumberOfNames == 0 || exports->NumberOfFunctions == 0) {
		// DLL doesn't export anything
		SetLastError(ERROR_PROC_NOT_FOUND);
		return NULL;
	}

	if (HIWORD(name) == 0) {
		// load function by ordinal value
		if (LOWORD(name) < exports->Base) {
			SetLastError(ERROR_PROC_NOT_FOUND);
			return NULL;
		}

		idx = LOWORD(name) - exports->Base;
	}
	else if (!exports->NumberOfNames) {
		SetLastError(ERROR_PROC_NOT_FOUND);
		return NULL;
	}
	else {
		const struct ExportNameEntry_Exui* found;

		// Lazily build name table and sort it by names
		if (!module->nameExportsTable) {
			DWORD i;
			DWORD* nameRef = (DWORD*)(codeBase + exports->AddressOfNames);
			WORD* ordinal = (WORD*)(codeBase + exports->AddressOfNameOrdinals);
			struct ExportNameEntry_Exui* entry = (struct ExportNameEntry_Exui*)malloc(exports->NumberOfNames * sizeof(struct ExportNameEntry_Exui));
			module->nameExportsTable = entry;
			if (!entry) {
				SetLastError(ERROR_OUTOFMEMORY);
				return NULL;
			}
			for (i = 0; i < exports->NumberOfNames; i++, nameRef++, ordinal++, entry++) {
				entry->name = (const char*)(codeBase + (*nameRef));
				entry->idx = *ordinal;
			}
			qsort(module->nameExportsTable, exports->NumberOfNames, sizeof(struct ExportNameEntry_Exui), _compare_Exui);
		}

		// search function name in list of exported names with binary search
		found = (const struct ExportNameEntry_Exui*)bsearch(&name,
			module->nameExportsTable,
			exports->NumberOfNames,
			sizeof(struct ExportNameEntry_Exui), _find_Exui);
		if (!found) {
			// exported symbol not found
			SetLastError(ERROR_PROC_NOT_FOUND);
			return NULL;
		}

		idx = found->idx;
	}

	if (idx > exports->NumberOfFunctions) {
		// name <-> ordinal number don't match
		SetLastError(ERROR_PROC_NOT_FOUND);
		return NULL;
	}

	// AddressOfFunctions contains the RVAs to the "real" functions
	return (FARPROC)(LPVOID)(codeBase + (*(DWORD*)(codeBase + exports->AddressOfFunctions + (idx * 4))));
}

BOOL  __stdcall  MemoryFreeLibrary_Exui(HINSTANCE mod)
{
	PMEMORYMODULE_Exui module = (PMEMORYMODULE_Exui)mod;

	if (module == NULL) {		return false;	}
	if (module->initialized) {
		// notify library about detaching from process
		DllEntryProc_Exui DllEntry = (DllEntryProc_Exui)(LPVOID)(module->codeBase + module->headers->OptionalHeader.AddressOfEntryPoint);
		(*DllEntry)((HINSTANCE)module->codeBase, DLL_PROCESS_DETACH, 0);
	}

	free(module->nameExportsTable);
	if (module->modules != NULL) {
		// free previously opened libraries
		int i;
		for (i = 0; i < module->numModules; i++) {
			if (module->modules[i] != NULL) {
				module->freeLibrary(module->modules[i], module->userdata);
			}
		}

		free(module->modules);
	}

	if (module->codeBase != NULL) {
		// release memory of library
		module->CustomFree(module->codeBase, 0, MEM_RELEASE, module->userdata);
	}

#ifdef _WIN64
	FreePointerList_Exui(module->blockedMemory, module->CustomFree, module->userdata);
#endif
	HeapFree(GetProcessHeap(), 0, module);

	return true;
}


EX_UI_CMDINFO

